<?
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

if( !isset($_GET['page']) ) {
	echo '============== НЕ УКАЗАНА СТРАНИЦА ===============';
	return;
}

return false;

define('TASK_IBLOCK_ID', 4);
define('BITRIX_24_USER_ID', 788);

echo '========== НАЧАЛО ЭКСПОРТА ===========';
echo '<br>';

//Стадии сделок на стороне B24
define('DEAL_STAGES', [
	'NEW'         => 'NEW', // Обращение
	'FREE'        => '7', // Свободная заявка
	'IN_PROGRESS' => '8', // Заявка в работе
	'DONE'        => '9', // Выполненная заявка
	'WON'         => 'WON', // Заявка закрыта (выполнено)
	'LOSE'        => 'LOSE' // Заявка провалена
]);

Loader::includeModule('iblock');
global $USER;

// Соответствия менеджеров из Б24 и CMS
$managersCMStoB24 = [
	"89"  => "30", //Александр Артемьев
	"501" => "32", //Лоенид Гордеев
	"533" => "34", //Алия Дрофейчик
	"673" => "36", //Ринат Ганиев
	"122" => "1", //Евгений Антипов
	"88"  => "14", //Артем Родин
	"1"   => "40", //Влад Чистаков
];

// Список городв по контакту из Б24
$ufContactCityUrl = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
$arCityContact    = makePostQuery($ufContactCityUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_5C70359B5EB25']]);
$arCityContact24  = remapB24List($arCityContact['result'][0]['LIST']);

echo '========== ГОРОДА КОНТАКТА ===========';
//PR($arCityContact24);
echo '<br>';

// Список городв по Сделке из Б24
$ufDealCityUrl = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.deal.userfield.list';
$arCityDeal    = makePostQuery($ufDealCityUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_5C70359B72613']]);
$arCityDeal24  = remapB24List($arCityDeal['result'][0]['LIST']);

echo '========== ГОРОДА СДЕЛКИ ===========';
//PR($arCityContact24);
echo '<br>';

// Сопоставляем идетификторы CMS и Б24
$arDealCitiesMap    = [];
$arContactCitiesMap = [];
$res                = CIBlockElement::GetList([], ["IBLOCK_ID" => 1, "ACTIVE" => "Y"], false, [], ['*']);
while ($arFields = $res->GetNext()) {
	if ($arCityDeal24[ $arFields['NAME'] ]) {
		$arDealCitiesMap[ $arFields['ID'] ] = $arCityDeal24[ $arFields['NAME'] ];
	}
	if ($arCityContact24[ $arFields['NAME'] ]) {
		$arContactCitiesMap[ $arFields['ID'] ] = $arCityContact24[ $arFields['NAME'] ];
	}
}

echo '========== МАППИНГ ГОРОДОВ СДЕЛКИ ===========';
//PR($arDealCitiesMap);
echo '<br>';
echo '========== МАППИНГ ГОРОДОВ КОНТАКТА ===========';
//PR($arContactCitiesMap);
echo '<br>';

// Список видов деятельности из Б24
$ufContactDirectionUrl  = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
$arContactDirectionDeal = makePostQuery($ufContactDirectionUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_1551197597']]);
$arContactDirection24   = remapB24List($arContactDirectionDeal['result'][0]['LIST']);

echo '========== ВИДЫ ДЕЯТЕЛЬНОСТИ ===========';
//PR($arContactDirection24);
echo '<br>';

// Сопоставляем идетификторы CMS и Б24
$arDirectionsMap = [];
$rsSections      = CIBlockSection::GetList(["left_margin" => "asc"], ["IBLOCK_ID" => 4]);
while ($arSection = $rsSections->GetNext()) {
	if ($arContactDirection24[ $arSection['NAME'] ]) {
		$arDirectionsMap[ $arSection['ID'] ] = $arContactDirection24[ $arSection['NAME'] ];
	}
}

echo '========== МАППИНГ ВИДОВ ДЕЯТЕЛЬНОСТИ ===========';
//PR($arDirectionsMap);
echo '<br>';


$arSelectTask = Array("*", "PROPERTY_*");
$arFilterTask = Array("IBLOCK_ID" => TASK_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_STATUS_ID" => "F");
//$arFilterTask = Array("IBLOCK_ID" => TASK_IBLOCK_ID, "ACTIVE" => "Y", "ID" => 133912);
$resTask      = \CIBlockElement::GetList(Array("[DATE_CREATE"=>"ASC"), $arFilterTask, false, Array("nPageSize"=>50, "iNumPage" => $_GET['page'], 'checkOutOfRange' => true), $arSelectTask);

while ($obTask = $resTask->GetNextElement()) {
	$arFieldsTask = $obTask->GetFields();
	$arPropsTask  = $obTask->GetProperties();

	// Проверяем что сделка с таким идентификатором заявки уже существует, если существует, тогда пропускаем
	$dealListUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.deal.list';
	$arDeals     = makePostQuery($dealListUrl, array('filter' => array("UF_CRM_1552426988" => $arFieldsTask['ID']), 'select' => ["ID"]));
	if ( ! empty($arDeals['result'])) {
		echo "========== ПРОПУСКАЕМ: ID - {$arFieldsTask['ID']}  ===========";
		echo '<br>';
		continue;
	}
	sleep(1);


	// Сделка
	$dealManager     = $arFieldsTask["CREATED_BY"]; //Менеджер
	$dealName        = $arPropsTask["NAME"]["VALUE"]; //Имя сделки
	$dealDateCreated = $arFieldsTask["DATE_CREATE"]; //Дата создания
	$dealDescription = $arPropsTask["DESCRIPTION"]["VALUE"]; //Описание сделки
	$dealCost        = $arPropsTask["COST"]["VALUE"]; //Стоимость сделки
	$dealStatus      = $arPropsTask["STATUS_ID"]["VALUE"]; //Статус сделки
	$dealExecutor    = $arPropsTask["EXECUTOR_NAME"]["VALUE"]; //ФИО исполнителя
	$dealProps       = array(
		"sectionId" => $arFieldsTask["IBLOCK_SECTION_ID"], // Вид деятельности
		"partners"  => $arPropsTask["PARTNERS"]["VALUE"], //Массив id партнеров
		"city"      => $arPropsTask["CITY"]["VALUE"] // Город исполнителя
	);
	$dealCity        = $arDealCitiesMap[ $arPropsTask["CITY"]["VALUE"] || 6726 ]; // Город сделки
	$dealBeginDate   = getB24DateFormat($arFieldsTask['DATE_CREATE']); // Дата создания сделки
	$dealCloseDate   = getB24DateFormat($arPropsTask['STATUS_ID']['TIMESTAMP_X']); // Дата закрытия сделки

	// Клиент
	$clientName   = $arPropsTask[27]["VALUE"]; //Имя клиента
	$clientCity   = $arContactCitiesMap[ $arPropsTask["CITY"]["VALUE"] || 6726 ]; //Город клиента
	$clientAddres = $arPropsTask["ADDRESS"]["VALUE"]; //Адрес клиента
	$clientPhone  = $arPropsTask["PHONE"]["VALUE"]; //Телефон клиента
	$clientType   = "CLIENT"; // Тип "Клиент"


	// Исполнитель
	if ($arPropsTask["PROFILE_ID"]["VALUE"]) {
		// Получаем идентификаторы видов деятельности для профиля
		$arProfileDirectionIds = [];
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 2, 'CREATED_BY' => $arPropsTask["PROFILE_ID"]["VALUE"]));
		if ($arProfile = $rsProfile->getNext()) {
			$dbPropsUserDirections = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'PROFILE_WORK')
			);
			while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
				$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
			}
		}

		// Виды детяльности для передачи в Б24
		$executorDirections = [];
		foreach ($arProfileDirectionIds as $dirId) {
			if ($arDirectionsMap[ $dirId ]) {
				$executorDirections[] = $arDirectionsMap[ $dirId ];
			}
		}

		$rsUser             = CUser::GetByID($arPropsTask["PROFILE_ID"]["VALUE"]);
		$arUser             = $rsUser->Fetch();
		$executorName       = $arUser["NAME"]; // Имя
		$executorLastName   = $arUser["LAST_NAME"]; // Фамилия
		$executorSecondName = $arUser["SECOND_NAME"]; // Отчетство
		$executorPhone      = $arUser["LOGIN"]; // Телефоно
		$executorCity       = $arContactCitiesMap[ $arUser["UF_CITY"] ]; // Город
		$executorDirection  = $executorDirections; // Виды деятельности
		$excutorType        = "SUPPLIER"; // Тип "Исполнитель"
	}

	/***============ СДЕЛКА ==============**/
	$dealAddUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.deal.add';
	$dealData   = array(
		'fields' => array(
			'UF_CRM_1552426988'    => $arFieldsTask['ID'], // ID заявки
			'TITLE'                => $dealName,
			'UF_CRM_1548512221'    => $dealDescription, //Описание
			'UF_CRM_1548512107'    => $dealCost, //Стоимость работ
			'UF_CRM_1548513476'    => $clientAddres, //Адрес работ
			'UF_CRM_1549782914'    => $dealExecutor, //Назначенный исполнитель
			'UF_CRM_TASK_SECTIONS' => json_encode($dealProps), //Назначение исполнителя
			'UF_CRM_5C70359B72613' => $dealCity, //Город сделки
			'STAGE_ID'             => 'WON',
			'ASSIGNED_BY_ID'       => $managersCMStoB24[ $dealManager ],
			'SOURCE_ID'            => 'WEB',
			'UF_CRM_1552309134'    => 'Y', // Призанак Сделка из CMS
			'BEGINDATE'            => $dealBeginDate, // Дата создания сделки
			'CLOSEDATE'            => $dealBeginDate, // Дата закрытия сделки
			'OPENED'               => 'Y', // Доступна для всех
		)
	);

	echo '========== ПОЛЯ СДЕЛКИ ===========';
//	PR($dealData);
	echo '<br>';

	$arDeal = makePostQuery($dealAddUrl, $dealData);
	echo '========== ДОБАВЛЕННАЯ СДЕЛКА ===========';
//	PR($arDeal);
	echo '<br>';


	$dealId = $arDeal['result']; //Идентификатор новой сделки

	echo '========== ID СДЕЛКИ ===========';
//	PR($dealId);
	echo '<br>';


	/***============ КЛИЕНТ ==============**/
	// Получение клиента по телефону
	$contactListUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.list';
	$arContacts     = makePostQuery($contactListUrl, array('filter' => array("PHONE" => $clientPhone), 'select' => ["ID"]));

	$contactAddData = array(
		'fields' => array(
			'NAME'                 => empty($clientName) ? 'Имя не указано' : $clientName, //Имя
			'TYPE_ID'              => $clientType, //тип "Клиента"
			'PHONE'                => array(array("VALUE" => $clientPhone, "VALUE_TYPE" => "WORK")), //Телефон
			'ASSIGNED_BY_ID'       => $managersCMStoB24[ $dealManager ], //Менеджер
			'UF_CRM_5C70359B5EB25' => $clientCity, //Город
			'UF_CRM_1552309160'    => 'Y',// Признак "Контакт из CMS"
			'OPENED'               => 'Y' // Доступна для всех
		)
	);
	if ($arContacts['total'] === 0) { // Если контакты не найдены, тогда добавляем
		$contactUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.add';
		$arContact   = makePostQuery($contactUrl, $contactAddData);
		$arContactId = $arContact['result'];
		echo '========== КЛИЕНТ - НОВЫЙ ===========';
		echo '<br>';
	} else {
		$arContactId = $arContacts['result'][0]['ID'];
		$contactAddData['id'] = $arContactId;
		$contactUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.update';
		$arContact   = makePostQuery($contactUrl, $contactAddData);
		echo '========== КЛИЕНТ - СУЩЕСТВУЮЩИЙ ===========';
		echo '<br>';
	}

	echo '========== ПОЛЯ КЛИЕНТА ===========';
//	PR($contactAddData);
	echo '<br>';

	echo '========== ID КЛИЕНТА ===========';
//	PR($arContactId);
	echo '<br>';


	//Привязка контакта к сделке
	$dealContactAddUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.deal.contact.add';
	$dealContactAddData = array(
		'id'     => $dealId,
		'fields' => array('CONTACT_ID' => $arContactId, 'IS_PRIMARY' => 'Y')
	);
	$arUpdatedDeal      = makePostQuery($dealContactAddUrl, $dealContactAddData);

	echo '========== ПОЛЯ ПРИВЗЯКИ КЛИЕНТА К СДЕЛКЕ ===========';
//	PR($dealContactAddData);
	echo '<br>';


	/***============ ИСПОЛНИТЕЛЬ ==============**/
	if ($arPropsTask["PROFILE_ID"]["VALUE"]) {
		// Получение исполнителя по телефону
		$executorListUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.list';
		$arExecutors     = makePostQuery($executorListUrl, array('filter' => array("PHONE" => $executorPhone), 'select' => ["ID"]));

		$executorAddData = array(
			'fields' => array(
				'NAME'                 => $executorName, //Имя
				'TYPE_ID'              => $excutorType, //тип
				'PHONE'                => array(array("VALUE" => $executorPhone, "VALUE_TYPE" => "WORK")), //Телефон
				'ASSIGNED_BY_ID'       => $managersCMStoB24[ $dealManager ], //Менеджер
				'UF_CRM_5C70359B5EB25' => $executorCity,//Город
				'UF_CRM_1551197597'    => $executorDirection,//Вид деятельности исполнителя
				'UF_CRM_1552309160'    => 'Y',// Признак "Контакт из CMS"
				'OPENED'               => 'Y' // Доступна для всех
			)
		);

		if ($arExecutors['total'] === 0) { // Если исполнители не найдены, тогда добавляем
			$executorUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.add';
			echo '========== ИСПОЛНИТЕЛЬ - НОВЫЙ ===========';
			echo '<br>';

		} else {
			$executorUrl           = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.update';
			$executorAddData['id'] = $arExecutors['result'][0]['ID'];
			echo '========== ИСПОЛНИТЕЛЬ - СУЩЕСТВУЮЩИЙ ===========';
			echo '<br>';
		}

		echo '========== ПОЛЯ ИСПОЛНИТЕЛЯ ===========';
//		PR($executorAddData);
		echo '<br>';

		makePostQuery($executorUrl, $executorAddData);

		unset($arFieldsTask, $arPropsTask, $arProfile, $arDeals, $dealData, $arDeal, $arContacts, $contactAddData, $arContact, $dealContactAddData, $arUpdatedDeal, $arExecutors, $executorAddData);
	}

//	PR($arUpdatedDeal);
}
