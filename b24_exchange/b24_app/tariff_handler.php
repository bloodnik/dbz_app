<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

// Настройки виджета
$placementOptions = isset($_REQUEST['PLACEMENT_OPTIONS']) ? json_decode($_REQUEST['PLACEMENT_OPTIONS'], true) : array();

$placementOptionsObj = CUtil::PhpToJSObject($placementOptions);

if ( ! is_array($placementOptions)) {
	$placementOptions = array();
}

$tariffName = "";
if ( ! empty($placementOptions["ENTITY_VALUE_ID"])) {
	$arTask = CAppforsaleTask::GetByExternalId($placementOptions["ENTITY_VALUE_ID"]);
	$userId = $arTask["PROPERTY_PROFILE_ID_VALUE"];

	if ( ! empty($userId)) {
		$tariff = new CAppforsaleTariff($userId);
		$userTariff = $tariff->getUserTariff();
		$tariffName = $userTariff["TARIFF_NAME"];
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap-vue.min.css"/>
	<style>
		body.view {
			height: 80px;
		}
	</style>
</head>
<body class="<?=$placementOptions['MODE']?>" style="margin: 0; padding: 0; overflow:hidden; background: #fafcfe">
<div id="app" style="padding: 15px;">
	<p class="h4">Тариф: <span>{{ tariffName }}</span></p>
</div>
<script src="//api.bitrix24.com/api/v1/dev/"></script>
<script src="/js_vendors/vue.min.js"></script>
<script src="/js_vendors/vue-resource.js"></script>
<script src="/js_vendors/polyfill.min.js"></script>
<script src="/js_vendors/bootstrap-vue.min.js"></script>

<script>
    var app = new Vue({
        el: '#app',
        data: {
            userTariff: "<?=$tariffName?>",
            tariffName: "",
            placementOption: <?=$placementOptionsObj?>,
            // Режим редактирования
            editMode: <?= $placementOptions['MODE'] === 'edit' ? 'true' : 'false';?>,
        },
        created() {
            BX24.resizeWindow(50, this.editMode ? 50 : 50);

            const curValue = '<?=$placementOptions['VALUE']?>';
            if (curValue === '' && this.userTariff !== '') {
                this.setValue(this.userTariff);
            }

            this.tariffName = curValue || this.userTariff;
        },
        methods: {
            // Установка занчения для B24
            setValue(value) {
                const newValue = value;
                BX24.placement.call('setValue', '' + newValue, function () {
                    console.log('value set')
                });
            },
        }
    })
</script>
</body>
</html>