<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

// Настройки виджета
$placementOptions = isset($_REQUEST['PLACEMENT_OPTIONS']) ? json_decode($_REQUEST['PLACEMENT_OPTIONS'], true) : array();

if ( ! is_array($placementOptions)) {
	$placementOptions = array();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap-vue.min.css"/>
	<style>
		.crm-entity-widget-content-input {
			display: block;
			box-sizing: border-box;
			padding: 10px 9px;
			min-height: 39px;
			max-width: 100%;
			width: 100%;
			outline: 0;
			border: 1px solid #c4c7cc;
			border-radius: 1px;
			background-color: #fff;
			color: #424956;
			font: 15px/17px "Helvetica Neue", Helvetica, Arial, sans-serif;
			transition: 220ms all ease;
		}

		.crm-entity-widget-content-block-inner-text {
			color: #424956;
			word-break: break-all;
			font: 15px/17px "Helvetica Neue", Helvetica, Arial, sans-serif;
		}

		.scrollable-menu {
			height: auto;
			max-height: 160px;
			overflow-x: hidden;
		}

		.scrollable-menu li {
			cursor: pointer;
			position: relative;
		}

		.scrollable-menu .online:after {
			bottom: 10px;
			right: 10px;
			border: 2px solid #fff;
			height: 11px;
			width: 11px;
			content: '';
			position: absolute;
			background-color: #8bc34a;
			border-radius: 50%;
		}


		body.edit {
			height: 450px;
		}

		body.view {
			height: 100px;
		}
	</style>
</head>
<body class="<?=$placementOptions['MODE']?>" style="margin: 0; padding: 0; overflow:hidden; background: #fafcfe">
<div id="app" style="padding: 15px;">


		<b-form-group>
			<b-form-checkbox-group
					v-model="selected"
					:options="options"
					name="buttons-1"
					button-variant="primary"
					size="sm"
					switches
					:disabled="!editMode"
					@change="setValue"
			></b-form-checkbox-group>
<!--			<b-button size="sm" @click="setValue([])">Очистить все</b-button>-->
		</b-form-group>

	</div>

</div>
<script src="//api.bitrix24.com/api/v1/dev/"></script>
<script src="/js_vendors/vue.min.js"></script>
<script src="/js_vendors/vue-resource.js"></script>
<script src="/js_vendors/polyfill.min.js"></script>
<script src="/js_vendors/bootstrap-vue.min.js"></script>

<script>
    var app = new Vue({
        el: '#app',
        data: {
            // Режим редактирования
            editMode: <?= $placementOptions['MODE'] === 'edit' ? 'true' : 'false';?>,
            selected: [], // Must be an array reference!
            options: [
                {text: 'Обращение', value: 'Обращение, клиент интересуется. У Вас есть возможность договориться о цене.'},
                {text: 'Подъехать сегодня!', value: 'Подъехать сегодня!'},
                {text: 'Ориентировочная стоимость', value: 'Стоимость работ указана ориентировочно. Итоговую определяет мастер на месте'},
                {text: 'Материалы закуплены', value: 'Все материалы закуплены. Ждут мастера'},
                {text: 'Договориться о времени!', value: 'Договориться о времени!'},
                {text: 'Срочно!', value: 'Срочно!'},
                {text: 'Маска', value: 'Заказчик просит мастера быть в медицинской маске'},
                {text: 'Требуется помощь с закупкой', value: 'Требуется помощь с закупкой'},
                {text: 'Договор/Акт', value: 'Для выполнения заявки потребуется заключить Договор/составить Акт выполненных работ с заказчиком. Образец можно скачать в сети интернет'}
            ]
        },
        created() {
            BX24.resizeWindow(50, this.editMode ? 180 : 180);
            const curValue = '<?=$placementOptions['VALUE']?>';
	        
            if (curValue !== '') {
                this.selected = JSON.parse(curValue) || [];
            }
        },
        methods: {
            // Установка занчения для B24
            setValue(value) {
                const newValue = JSON.stringify(value);
                BX24.placement.call('setValue', '' + newValue, function () {
                    console.log('value set')
                });
            },
        }
    })
</script>
</body>
</html>