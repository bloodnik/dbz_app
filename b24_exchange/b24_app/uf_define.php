﻿<?
/**
 *  Скрипт приложения для Битрикс 24, реистрирующий пользовательские типы поля
 *  Виды деятельности и исполнители
 */

//echo '<pre>'; print_r($_REQUEST); echo '</pre>';

// Расположение виджета
$placement = $_REQUEST['PLACEMENT'];

// Настройки виджета
$placementOptions = isset($_REQUEST['PLACEMENT_OPTIONS']) ? json_decode($_REQUEST['PLACEMENT_OPTIONS'], true) : array();

if(!is_array($placementOptions))
{
	$placementOptions = array();
}

if($placement === 'DEFAULT')
{
	$placementOptions['MODE'] = 'edit';
}
?>

<!DOCTYPE html>
<html>
<head>
	<script src="//api.bitrix24.com/api/v1/dev/"></script>
</head>
<body>
<div class="workarea">

<? if($placement === 'DEFAULT'): ?>
    <ul>
        <li><a href="javascript:void(0)" onclick="user_fields_app.add1()">Добавить тип поля Исполнители и виды деятельности</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addDescrUFType()">Добавить тип поля расширенное описание</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addBalanceUFType()">Добавить тип поля баланс мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addTariffUFType()">Добавить тип поля тариф мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.list()">Список добавленных полей</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.del()">Удалить поля</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delDescrFieldType()">Удалить поля расширенное описание</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delBalanceFieldType()">Удалить поле баланс мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delTariffFieldType()">Удалить поле тариф мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addField1()">Добавить поле Исполнители и виды деятельности</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addField2()">Добавить поле Исполнители и виды деятельности для лида</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addDescrField()">Добавить поле расширенное описание</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addLeadDescrField()">Добавить поле расширенное описание для лида</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addBalanceField()">Добавить поле баланс мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.addTariffField()">Добавить поле тариф мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.listField()">Список полей</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delField1()">Удалить поле Исполнители и виды деятельности</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delField2()">Удалить поле Исполнители и виды деятельности (лид)</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delDescrField()">Удалить поле Расширенное описание</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delLeadDescrField()">Удалить поле Расширенное описание для лида</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delBalanceField()">Удалить поле баланс мастера</a></li>
        <li><a href="javascript:void(0)" onclick="user_fields_app.delTariffField()">Удалить поле тариф мастера</a></li>
    </ul>
    <pre id="debug" style="border: solid 1px #aaa; padding: 10px; background-color: #eee">&nbsp;</pre>

	<script>

		var user_fields_app = {
			call: function(method, param) {
				BX24.callMethod(method, param, user_fields_app.debug);
				BX24.installFinish();
			},

			debug: function(result) {
				var s = '';

				s += '<b>' + result.query.method + '</b>\n';
				s += JSON.stringify(result.query.data, null, '  ') + '\n\n';

				if(result.error())
				{
					s += '<span style="color: red">Error! ' + result.error().getStatus() + ': ' + result.error().toString() + '</span>\n';
				}
				else
				{
					s += '<span>' + JSON.stringify(result.data(), null, '  ') + '</span>\n';
				}

				document.getElementById('debug').innerHTML = s;
			},

			add1: function() {
				user_fields_app.call('userfieldtype.add', {
					USER_TYPE_ID: 'task_section',
					HANDLER: 'https://dom-bez-zabot.online/b24_exchange/b24_app/sections_handler.php',
					TITLE: 'Виды деятельности Битрикс',
					DESCRIPTION: 'Список видов деятельности'
				});
			},

            addDescrUFType: function () {
                user_fields_app.call('userfieldtype.add', {
                    USER_TYPE_ID: 'descr_type',
                    HANDLER: 'https://dom-bez-zabot.online/b24_exchange/b24_app/descr_handler.php',
                    TITLE: 'Расширенное описание',
                    DESCRIPTION: 'Выбор расширенного описания'
                });
            },

            addBalanceUFType: function () {
                user_fields_app.call('userfieldtype.add', {
                    USER_TYPE_ID: 'balance_type',
                    HANDLER: 'https://dom-bez-zabot.online/b24_exchange/b24_app/balance_handler.php',
                    TITLE: 'Баланс мастера',
                    DESCRIPTION: 'Баланс мастера'
                });
            },

            addTariffUFType: function () {
                user_fields_app.call('userfieldtype.add', {
                    USER_TYPE_ID: 'tariff_type',
                    HANDLER: 'https://dom-bez-zabot.online/b24_exchange/b24_app/tariff_handler.php',
                    TITLE: 'Тариф мастера',
                    DESCRIPTION: 'Тариф мастера'
                });
            },

			list: function() {
				user_fields_app.call('userfieldtype.list', {});
			},

			del: function() {
				user_fields_app.call('userfieldtype.delete', {
					USER_TYPE_ID: 'task_section'
				});
				// user_fields_app.call('userfieldtype.delete', {
				// 	USER_TYPE_ID: 'task_users'
				// });
			},

            delDescrFieldType: function () {
                user_fields_app.call('userfieldtype.delete', {
                    USER_TYPE_ID: 'descr_type'
                });
            },
            delBalanceFieldType: function () {
                user_fields_app.call('userfieldtype.delete', {
                    USER_TYPE_ID: 'balance_type'
                });
            },

			delTariffFieldType: function () {
                user_fields_app.call('userfieldtype.delete', {
                    USER_TYPE_ID: 'tariff_type'
                });
            },

			addField1: function() {
				user_fields_app.call('crm.deal.userfield.add', {
					fields: {
						FIELD_NAME: "TASK_SECTIONS",
						EDIT_FORM_LABEL: "Виды деятельности",
						LIST_COLUMN_LABEL: "Виды деятельности",
						USER_TYPE_ID: "task_section",
						XML_ID: "TASK_SECTIONS"
					}
				});
			},

            addField2: function() {
                user_fields_app.call('crm.lead.userfield.add', {
                    fields: {
                        FIELD_NAME: "TASK_SECTIONS",
                        EDIT_FORM_LABEL: "Виды деятельности",
                        LIST_COLUMN_LABEL: "Виды деятельности",
                        USER_TYPE_ID: "task_section",
                        XML_ID: "TASK_SECTIONS"
                    }
                });
            },

            addDescrField: function () {
                user_fields_app.call('crm.deal.userfield.add', {
                    fields: {
                        FIELD_NAME: "descr",
                        EDIT_FORM_LABEL: "Расширенное описание",
                        LIST_COLUMN_LABEL: "Расширенное описание",
                        USER_TYPE_ID: "descr_type",
                        XML_ID: "descr"
                    }
                });
            },
            addLeadDescrField: function () {
                user_fields_app.call('crm.lead.userfield.add', {
                    fields: {
                        FIELD_NAME: "descr",
                        EDIT_FORM_LABEL: "Расширенное описание",
                        LIST_COLUMN_LABEL: "Расширенное описание",
                        USER_TYPE_ID: "descr_type",
                        XML_ID: "descr"
                    }
                });
            },
            addBalanceField: function () {
                user_fields_app.call('crm.deal.userfield.add', {
                    fields: {
                        FIELD_NAME: "balance",
                        EDIT_FORM_LABEL: "Баланс мастера",
                        LIST_COLUMN_LABEL: "Баланс мастера",
                        USER_TYPE_ID: "balance_type",
                        XML_ID: "balance"
                    }
                });
            },
            addTariffField: function () {
                user_fields_app.call('crm.deal.userfield.add', {
                    fields: {
                        FIELD_NAME: "tariff",
                        EDIT_FORM_LABEL: "Тариф мастера",
                        LIST_COLUMN_LABEL: "Тариф мастера",
                        USER_TYPE_ID: "tariff_type",
                        XML_ID: "tariff"
                    }
                });
            },

			delField1: function() {
				user_fields_app.call('crm.deal.userfield.delete', {
					id: "task_section"
				});
			},
			delField2: function() {
				user_fields_app.call('crm.lead.userfield.delete', {
					id: "task_section"
				});
			},

            delDescrField: function () {
                user_fields_app.call('crm.deal.userfield.delete', {
                    id: "584"
                });
            },
            delLeadDescrField: function () {
                user_fields_app.call('crm.lead.userfield.delete', {
                    id: "538"
                });
            },
            delBalanceField: function () {
                user_fields_app.call('crm.deal.userfield.delete', {
                    id: "618"
                });
            },
			delTariffField: function () {
                user_fields_app.call('crm.deal.userfield.delete', {
                    id: "713"
                });
            },

            listField: function() {
				user_fields_app.call('crm.lead.userfield.list', {});
			}


		}
	</script>
<? endif; ?>

</div>

</body>
</html>
