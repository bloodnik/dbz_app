<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

// Настройки виджета
$placementOptions = isset($_REQUEST['PLACEMENT_OPTIONS']) ? json_decode($_REQUEST['PLACEMENT_OPTIONS'], true) : array();


if ( ! is_array($placementOptions)) {
	$placementOptions = array();
}

function getAllSections() {
	// Получаем разделы инфоблока Заданий
	$rsSections    = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => 4), false, array("ID", "NAME", "IBLOCK_SECTION_ID", "UF_FEE_PRICE"));
	$arSections    = [];
	$arSections[0] = ['ID' => 011, 'NAME' => 'Выберите вид деятельности', 'IBLOCK_SECTION_ID' => ''];
	while ($arSection = $rsSections->GetNext()) {

		// Проверяем, платный ли родитель
		$paydParent = false;
		if(!empty($arSection['IBLOCK_SECTION_ID'])) {
			$rsParentSections = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => 4, "ID" => $arSection['IBLOCK_SECTION_ID']), false, array("ID", "UF_FEE_PRICE"));
			while ($arParentSection = $rsParentSections->GetNext()) {
				$paydParent =  intval($arParentSection['UF_FEE_PRICE']) > 0;
			}
		}

		$arItem                      = array();
		$arItem['ID']                = $arSection['ID'];
		$arItem['NAME']              = $arSection['NAME'];
		$arItem['IBLOCK_SECTION_ID'] = $arSection['IBLOCK_SECTION_ID'];
		$arItem['FEE_PRICE']         = intval($arSection['UF_FEE_PRICE']);
		$arItem['IS_PAYD']           = intval($arSection['UF_FEE_PRICE']) > 0 || $paydParent;
		$arItem['CHILDREN']          = [];
		$arSections[]                = $arItem;
	}

	return $arSections;
}

$cachedSections = returnResultCache(86400, 'dbzSectionsId5', 'getAllSections');

// Список разделов
$allSections = json_encode($cachedSections);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="/js_vendors/bootstrap-vue.min.css"/>
	<style>
		.crm-entity-widget-content-input {
			display: block;
			box-sizing: border-box;
			padding: 10px 9px;
			min-height: 39px;
			max-width: 100%;
			width: 100%;
			outline: 0;
			border: 1px solid #c4c7cc;
			border-radius: 1px;
			background-color: #fff;
			color: #424956;
			font: 15px/17px "Helvetica Neue", Helvetica, Arial, sans-serif;
			transition: 220ms all ease;
		}

		.crm-entity-widget-content-block-inner-text {
			color: #424956;
			word-break: break-all;
			font: 15px/17px "Helvetica Neue", Helvetica, Arial, sans-serif;
		}

		.scrollable-menu {
			height: auto;
			max-height: 160px;
			overflow-x: hidden;
		}

		.scrollable-menu li {
			cursor: pointer;
			position: relative;
		}

		.scrollable-menu .online:after {
			bottom: 10px;
			right: 10px;
			border: 2px solid #fff;
			height: 11px;
			width: 11px;
			content: '';
			position: absolute;
			background-color: #8bc34a;
			border-radius: 50%;
		}


		body.edit {
			height: 450px;
		}

		body.view {
			height: 100px;
		}
	</style>
</head>
<body class="<?=$placementOptions['MODE']?>" style="margin: 0; padding: 0; overflow:hidden; background: #fafcfe">
<div id="app" style="padding: 15px;">

	<div v-if="editMode">
		<b-label name="city">Город</b-label>
		<b-form-select class="crm-entity-widget-content-input mb-3" v-model="selectedCity" :options="sortedCities" value-field="ID" text-field="NAME" @input="setCity"></b-form-select>

		<b-label name="activity">Вид деятельности</b-label>
		<b-form-select class="crm-entity-widget-content-input mb-3" v-model="selectedSection" @input="setValue" @change="selectedPartners = []" multiple :select-size="5">
			<option :value="sectParent.ID" v-for="sectParent in sectionsTree">
				{{sectParent.IS_PAYD ? '+' : ''}}{{sectParent.NAME }}

				{{ getChildDirtections(sectParent) }}
			</option>
		</b-form-select>

		<b-label name="partners">Партнеры</b-label>
		<div>
			<b-form-checkbox v-model="allParnters"
			                 :value="true"
			                 :unchecked-value="false">
				Выбрать всех партнеров
			</b-form-checkbox>

			<div v-if="allowedToShow" class="parnersSelect">
				<input class="crm-entity-widget-content-input mb-3" type="text" v-model="partnerSearchQuery" placeholder="Поиск партнера...">

				<ul class="scrollable-menu list-unstyled">
					<li @click="setPartner(item)" class="p-1" :class="{'bg-primary text-white' : selectedPartners.includes(item.ID), 'online': item.IS_ONLINE}" v-for="item in filteredPartners">
						{{item.NAME}}
					</li>
				</ul>
			</div>
			<div class="small">
				Всего: {{ parntersList.length }} | Выбрано: {{ selectedPartners.length }}
			</div>

		</div>

	</div>
	<div v-else>
		<h6>Город:</h6>
		<p class="crm-entity-widget-content-block-inner-text">{{ cityName }}</p>
		<h6>Направление:</h6>
		<p class="crm-entity-widget-content-block-inner-text">{{ sectionName }}</p>
		<h6>Партнеры:</h6>
		<p class="crm-entity-widget-content-block-inner-text">{{ partnersName }}</p>
	</div>

</div>
<script src="//api.bitrix24.com/api/v1/dev/"></script>
<script src="/js_vendors/vue.min.js"></script>
<script src="/js_vendors/vue-resource.js"></script>
<script src="/js_vendors/polyfill.min.js"></script>
<script src="/js_vendors/bootstrap-vue.min.js"></script>


<script>
    var app = new Vue({
        el: '#app',
        data: {
            // Режим редактирования
            editMode: <?= $placementOptions['MODE'] === 'edit' ? 'true' : 'false';?>,

            selectedCity: null,
            selectedSection: ['1'],
            selectedPartners: [],
            allParnters: false,

            //Плоский список разделов
            allSectionsList: JSON.parse('<?=$allSections?>'),
            //Список партнеров
            parntersList: [],
            //Список городов
            citiesList: [],

            partnerSearchQuery: '',
            currentUser: {},
            adminUsers: [1, 36, 32, 40, 864]
        },
        async created() {
            BX24.resizeWindow(300, this.editMode ? 570 : 250);

            var _this = this;

            const curValue = '<?=$placementOptions['VALUE']?>';


            const value = curValue ? JSON.parse(curValue) : null;
            if (value) {
                this.selectedSection = value.sectionId;
                this.selectedCity = value.city || '6726';
                this.selectedPartners = value.partners;
            }

            let sections = Array.isArray(this.selectedSection) ? this.selectedSection.join(',') : this.selectedSection;
            const {body} = await this.$http.get('/youdo/api/get_data.php', {params: {type: 'partners2', sectionId: sections, cityId: this.selectedCity}});
            this.parntersList = body;

            if (value) this.allParnters = value.allParnters;

            await this.getCities();
            await this.getPartners();

        },
        computed: {
            // Полский список разделов в древовидный
            sectionsTree() {
                return this.list_to_tree(this.allSectionsList)
            },

            //Имя городв по его ID
            cityName() {
                if (this.citiesList.length > 0 && this.selectedCity) {
                    return this.citiesList.filter(item => item.ID == this.selectedCity)[0]['NAME'];
                }
            },

            //Имя раздела по его ID
            sectionName() {
                if (Array.isArray(this.selectedSection)) {
                    return this.allSectionsList.filter(function (item) {
                        return this.indexOf(item.ID) >= 0
                    }, this.selectedSection).map(item => item.NAME).join(', ');

                } else if (this.selectedSection) {
                    return this.allSectionsList.filter(item => item.ID == this.selectedSection)[0]['NAME'];
                } else {
                    return 'Не выбрано'
                }
            },

            //Список имен партнеров
            partnersName() {
                if (this.parntersList.length > 0 && this.selectedPartners.length > 0) {
                    return this.selectedPartners.map(item => {
                            let partnerName = this.parntersList.filter(partner => partner.ID == item);
                            return partnerName[0] ? partnerName[0]['NAME'] : '';
                        }
                    ).join(', ');
                }
                return 'Не выбраны';
            },

            filteredPartners: function () {
                if (this.parntersList !== null) {
                    return this.parntersList.filter(partner => partner.NAME.toLowerCase().indexOf(this.partnerSearchQuery.toLowerCase()) !== -1);
                }
            },

            sortedCities: function () {
                if (this.citiesList !== null) {
                    return this.citiesList.sort(function (a, b) {
                        var nameA = a.NAME.toLowerCase(), nameB = b.NAME.toLowerCase();
                        if (nameA < nameB) //сортируем строки по возрастанию
                            return -1;
                        if (nameA > nameB)
                            return 1;
                        return 0
                    });
                }
            },

            //Условие показа блока с партнерами
            allowedToShow() {
                // return this.adminUsers.includes(parseInt(this.currentUser.id, 10));
                return true;
            }
        },
        methods: {
            // Установка занчения для B24
            setValue() {
                const newValue = {sectionId: this.selectedSection, partners: this.selectedPartners, city: this.selectedCity, allParnters: this.allParnters};
                BX24.placement.call('setValue', JSON.stringify(newValue), function () {
                    console.log('value set')
                });
            },

            //Выбираем город, и перезапрашиваем партнеров
            setCity() {
                this.selectedPartners = [];
                this.getPartners();
                this.setValue();
            },

            //Заполняем массив выбранных партнеров
            setPartner(item) {
                if (this.selectedPartners.includes(item.ID)) {
                    this.selectedPartners.splice(this.selectedPartners.findIndex(arrItem => arrItem === item.ID), 1);
                } else {
                    this.selectedPartners.push(item.ID);
                }

                this.setValue();
            },

            // Получаем список партнеров с сервера
            async getPartners() {

                let sections = Array.isArray(this.selectedSection) ? this.selectedSection.join(',') : this.selectedSection;

                const {body} = await this.$http.get('/youdo/api/get_data.php', {params: {type: 'partners2', sectionId: sections, cityId: this.selectedCity}})
                this.parntersList = body;

                return true;
            },

            // Получаем список городов с сервера
            getCities() {
                this.$http.get('/youdo/api/get_data.php', {params: {type: 'cities'}}).then(response => {
                    this.citiesList = response.body;
                });
            },

            getChildDirtections(parentDirection) {
                if (!parentDirection.CHILDREN || !parentDirection.CHILDREN.length) return "";
                return `(${parentDirection.CHILDREN.map(child => child.NAME).join(", ")})`;
            },


            // Функция геренрации древовидного массива
            list_to_tree(list) {
                var map = {}, node, roots = [], i;
                for (i = 0; i < list.length; i += 1) {
                    map[list[i].ID] = i; // initialize the map
                    list[i].CHILDREN = []; // initialize the children
                }
                for (i = 0; i < list.length; i += 1) {
                    node = list[i];
                    if (node.IBLOCK_SECTION_ID) {
                        list[map[node.IBLOCK_SECTION_ID]].CHILDREN.push(node);
                    } else {
                        roots.push(node);
                    }
                }

                return roots;
            }
        },
        watch: {
            selectedSection(val) {
                this.getPartners();
            },
            allParnters(val) {
                if (val) {
                    this.selectedPartners = this.parntersList.map(item => item.ID);
                } else {
                    this.selectedPartners = [];
                }

                this.setValue();
            }
        }
    })
</script>
</body>
</html>