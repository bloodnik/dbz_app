﻿﻿<?php
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

use Bitrix\Main\Config\Option;
use \Bitrix\MAin\Loader;
use Bitrix\Main\Type\DateTime;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

define('B24_DOMAIN', 'dombezzabot.bitrix24.ru');
// Код авторизациии исходящего вебхука для события обновления сделки
define('DEAL_UPDATE_SECRET', 'gsaonqu113avuaegg3g34dj7l8gjgqw2');
// Код авторизациии исходящего вебхука для события удаления сделки
define('DEAL_DELETE_SECRET', '9dn00c706dp1flm116azdqzuyxwxby3w');

// Коды авторизации
define('DEAL_SECRETS', array('gsaonqu113avuaegg3g34dj7l8gjgqw2_', '9dn00c706dp1flm116azdqzuyxwxby3w_'));

// Секретный ключ входящего вебхука Bitrix24
define('OUT_SECRET', 'mhylug065v6c458h');

define('TASK_IBLOCK_ID', 4);
define('BITRIX_24_USER_ID', 788);
define('DEFAULT_COMISSION', '25'); // Комиссия по умолчания, если не заполнено в сделке

//Стадии сделок на стороне B24
$arDealStage = [
	'NEW'               => 'NEW', // Качественный лид
	'FULL_COMISSION'    => '32', // Полный возврат
	'PARTIAL_COMISSION' => '11', // Частичный возврат
	'REFUSE_COMISSION'  => '31', // Отказ по возврату
	'MINUS_50'          => '16', // Возврат за минусом 50 рублей
	'RELOCATE'          => '12', // Автопереразмещение
	'FREE'              => '7', // Свободная заявка
	'IN_PROGRESS'       => '8', // Заявка в работе
	'DONE'              => '9', // Выполненная заявка
	'WON'               => 'WON', // Заявка закрыта (выполнено)
];

// Стадии при которых надо удалить заявку
$arDeleteStages = [
	'LOSE', // Некачеств-й лид
	'10', // Цена
	'1', // НЕТ ИСПОЛНИТЕЛЕЙ
	'5', // По вине исполнителя
	'6', // По вине клиента
	'15', // Возврат комиссии
	'17', // Возврат комиссии -50 рублей
	'20', // По вине менеджера
	'21', // ПО ВИНЕ ТП
	'34', // ОТМЕНА ЧЕРЕЗ ЧАТ"
	'35', // Принята в НП
];

// Статусы сделок B24
$arAllDealStage = array(
	"NEW"     => "Обращение",
	"7"       => "Свободная заявка",
	"8"       => "Заявка в работе",
	"9"       => "Выполненная заявка",
	"WON"     => "Заявка закрыта (выполнено)",
	"LOSE"    => "Заявка провалена",
	"APOLOGY" => "Не устроила цена",
	"4"       => "Нашли дешевле",
	"1"       => "Не устроил срок",
	"2"       => "Не профильное",
	"5"       => "По вине исполнителя",
	"3"       => "По вине оператора",
	"6"       => "По вине клиента"
);


// Пользовательские поля сделки B24
$arDealUserFields = [
	'COMISSION'            => 'UF_CRM_1548512107', //Стоимость заявки. Комиссия
	'DESCRIPTION'          => 'UF_CRM_1548512221', //Описание работы
	'ADDRESS'              => 'UF_CRM_1548513476', // Адрес работы
	'TASK_DATA'            => 'UF_CRM_TASK_SECTIONS', // Раздел, партнеры, город
	'COMMITMENT'           => 'UF_CRM_1551013902', // Обязательства исполнителя
	'PHOTO'                => 'UF_CRM_1551801303', // Фотография
	'MORE_PHOTO'           => 'UF_CRM_1554024213', // Фотография(множественное добавление)
	'EXECUTOR_ID'          => 'UF_CRM_1552120642', // Id выбранного исполнителя
	'COMMENT_FOR_EXECUTOR' => 'UF_CRM_1552224786', // Комментарий для исполнителя
	'EXTRA_DESCRIPTION'    => 'UF_CRM_DESCR' // Расширенное описание
];


// Соответсвия статусов CRM и CMS
$arStatusMapping = array(
	"7"   => "N", // "Открыто",
	"8"   => "P", // "Выполняется",
	"9"   => "D", // "Исполнено",
	"WON" => "F", // "Закрыто для преложений",
);

// Соответствия менеджеров из Б24 и CMS
$managersB24toCMS = [
	"30"   => "89", //Александр Артемьев
	"32"   => "501", //Лоенид Гордеев
	"34"   => "533", //Алия Дрофейчик
	"36"   => "673", //Ринат Ганиев
	"1"    => "122", //Евгений Антипов
	"14"   => "88", //Артем Родин
	"40"   => "1", //В. Чистаков
	"868"  => "2403", //Руслан Аеткулов
	"1050" => "242", //Максим Башлыков
	"864"  => "2383", //Роман Кузнецов
];

global $DB;

//Проверка, что запрос пришел от https://dombezzabot.bitrix24.ru/
if (isset($_REQUEST['auth']) && in_array($_REQUEST['auth']['application_token'], DEAL_SECRETS) && $_REQUEST["auth"]["domain"] == B24_DOMAIN) {
	if ($_REQUEST['event'] === 'ONCRMDEALUPDATE') {
		$dealID = $_REQUEST["data"]['FIELDS']['ID'];

		// Endpoint получения сделки по ID
		$dealUrl = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.deal.get';
		// Сделка
		$arDeal = makePostQuery($dealUrl, array('ID' => $dealID));


		if (is_array($arDeal) && ! empty($arDeal) && $arDeal['result']['MODIFY_BY_ID'] !== '52') {
			// Получение данных по контакту сделки
			$arContact = [];
			if ( ! empty($arDeal['result']['CONTACT_ID']) && $arDeal['result']['CONTACT_ID'] > 0) {
				// Endpoint получения контакта по ID
				$contactUrl = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.contact.get';
				// Контакт
				sleep(3);
				$arContact = makePostQuery($contactUrl, array('ID' => $arDeal['result']['CONTACT_ID']));
			}

			//-- Добавление заявки --//
			$taskDescription = $arDeal['result'][ $arDealUserFields['DESCRIPTION'] ] . "\n \n" . $arDeal['result'][ $arDealUserFields['COMMITMENT'] ];
			$taskData        = json_decode($arDeal['result'][ $arDealUserFields['TASK_DATA'] ], true);

			if (empty($taskData['partners'])) {
				$partners = CAppforsaleProfile::GetByCityAndDSection($taskData['city'], $taskData["sectionId"]);
			} else {
				$partners = $taskData['partners'];
			}

			$adressArray = explode('|', $arDeal['result'][ $arDealUserFields['ADDRESS'] ]);
			$addres      = array_shift($adressArray);

			$arExtraDescription = json_decode($arDeal['result'][ $arDealUserFields['EXTRA_DESCRIPTION'] ], true);
			$extraDescription   = implode("\n", $arExtraDescription);
			if (strlen($extraDescription) > 0) {
				$taskDescription = $extraDescription . "\n \n" . $taskDescription;
			}

			$comission = DEFAULT_COMISSION;

			if (strlen($arDeal['result']['OPPORTUNITY']) > 0) {
				if (intval($arDeal['result']['OPPORTUNITY']) > 400) {
					$comissionSumm = $arDeal['result']['OPPORTUNITY'] * 0.1;
					$comission     = $comissionSumm;
				} else {
					$comission = 0;
				}
			}

			if (strlen($arDeal['result'][ $arDealUserFields['COMISSION'] ]) > 0) {
				$comission = $arDeal['result'][ $arDealUserFields['COMISSION'] ];
			}


			// Свойства заявки
			$arTaskProps = [
				24 => $arDeal['result']['TITLE'], // Заголовок
				25 => $taskDescription, // Описание заявки
				27 => $arContact['result']['NAME'] . ' ' . $arContact['result']['LAST_NAME'], // ФИО клиента
				28 => $addres, // Адрес
				29 => $arContact['result']['PHONE'][0]['VALUE'], // Телефон
				30 => "23", // Тип оплаты
				42 => $arDeal['result'][ $arDealUserFields['EXECUTOR_ID'] ], // Исполнитель
				51 => $partners, // Партнеры
				54 => $arDeal['result'][ $arDealUserFields['COMMENT_FOR_EXECUTOR'] ], // Комментарий для партнера
				60 => $comission, // Стоимость/комиссия
				61 => $taskData['city'], // Город
				62 => "Y", // Признак Заявка из CRM
				71 => $arDeal['result']['OPPORTUNITY'], // Стоимость работы
				79 => new DateTime($arDeal['result']['DATE_CREATE'], "Y-m-d\TH:i:s") // Дата создания сделки
			];

			//Скачиваем файл на сервер
			if ( ! empty($arDeal['result'][ $arDealUserFields['PHOTO'] ])) {
				requestRefreshToken(); //обновляем access_token TODO: Неверно, исправить на проверку акутальности токена, только потом обновлять
				$b24_access_token = Option::get("mlab.appforsale", "b24_access_token", "", 's1');
				downloadFileFromB24('https://dombezzabot.bitrix24.ru' . $arDeal['result'][ $arDealUserFields['PHOTO'] ]['showUrl'] . "&auth=" . $b24_access_token, 'b24_' . $arDeal['result'][ $arDealUserFields['PHOTO'] ]['id'] . '.jpg');
				$arTaskProps[26] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/b24_images/' . 'b24_' . $arDeal['result'][ $arDealUserFields['PHOTO'] ]['id'] . '.jpg'); // Фото
			}

			//Скачиваем файл на сервер
			if ( ! empty($arDeal['result'][ $arDealUserFields['MORE_PHOTO'] ])) {
				requestRefreshToken(); //обновляем access_token TODO: Неверно, исправить на проверку акутальности токена, только потом обновлять
				$b24_access_token = Option::get("mlab.appforsale", "b24_access_token", "", 's1');
				if (is_array($arDeal['result'][ $arDealUserFields['MORE_PHOTO'] ])) {
					foreach ($arDeal['result'][ $arDealUserFields['MORE_PHOTO'] ] as $photoKey => $arPhoto) {
						downloadFileFromB24('https://dombezzabot.bitrix24.ru' . $arPhoto['showUrl'] . "&auth=" . $b24_access_token, 'b24_' . $arPhoto['id'] . '_' . $photoKey . '.jpg');
						$arTaskProps[26][] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/b24_images/' . 'b24_' . $arPhoto['id'] . '_' . $photoKey . '.jpg'); // Фото
					}
				}
			}
			//Свободная заявка Заявка в работе или Закрытая заявка или Закрыто для предложний
			if ($arDeal['result']['STAGE_ID'] == $arDealStage['FREE'] || $arDeal['result']['STAGE_ID'] == $arDealStage['IN_PROGRESS'] || $arDeal['result']['STAGE_ID'] == $arDealStage['WON']) {
				$arTaskProps[43] = $arStatusMapping[ $arDeal['result']['STAGE_ID'] ];

				// Если статус "Свободная заявка", то очищаем исполнителя
				if ($arDeal['result']['STAGE_ID'] == $arDealStage['FREE']) {

					$arTaskProps[42] = ''; // Исполнитель
					$arTaskProps[59] = ''; // ФИО исполнителя
					$arTaskProps[74] = array("VALUE" => 33); // Требуется списание
					$arTaskProps[76] = array("VALUE" => false); // Не списывать комиссию
					$arTaskProps[83] = array("VALUE" => false); // Первая заявка
					$arTaskProps[84] = ''; // Фактическая цена работ
					$arTaskProps[85] = ''; // Тип проблемы с заявкой
				}

				// Поля ИБ заявки
				$arTaskFields = [
					"NAME"            => $arDeal['result']['TITLE'],
					"IBLOCK_ID"       => TASK_IBLOCK_ID,
					//"IBLOCK_SECTION_ID" => is_array($taskData['sectionId']) ? $taskData['sectionId'],
					"MODIFIED_BY"     => BITRIX_24_USER_ID,
					"CREATED_BY"      => $managersB24toCMS[ $arDeal['result']['ASSIGNED_BY_ID'] ], // менеждер
					"XML_ID"          => $dealID,
					"PROPERTY_VALUES" => $arTaskProps
				];

				if (is_array($taskData['sectionId'])) {
					$arTaskFields['IBLOCK_SECTION'] = $taskData['sectionId'];
				} else {
					$arTaskFields['IBLOCK_SECTION_ID'] = $taskData['sectionId'];
				}

				AddMessage2Log($arTaskFields, 'arTaskFields');

				Loader::includeModule('iblock');

				// Спим рандомное время
				$randDelay = rand(0, 1000000);
				usleep($randDelay);

				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $dealID), false, false, array("ID", "PROPERTY_WAIT_COMISSION", "PROPERTY_NO_COMISSION", "PROPERTY_FIRST_TASK"));
				if ($arTask = $rsTask->getNext()) {
					$obElement = new CIBlockElement();

					// Если статус "Свободная заявка", то удаляем предложение исполнителя, если оно есть, без возврата денежных средств
					if ($arDeal['result']['STAGE_ID'] == $arDealStage['FREE']) {
						executorOfferDelete($arTask['ID']);
					}

					/*Предварительно очищаем свойство PHOTO, чтобы не множить фотографии*/
					CIBlockElement::SetPropertyValuesEx($arTask['ID'], TASK_IBLOCK_ID, array('PHOTO' => array('VALUE' => array("del" => "Y"))));

					//Проверяем свойство "Требуется списание"
					if ( ! empty($arTask["PROPERTY_WAIT_COMISSION_VALUE"])) {
						$arTaskFields["PROPERTY_VALUES"][74] = array("VALUE" => 33); // Требуется списание
					}

					//Проверяем свойство "Не списывать комиссию"
					if ( ! empty($arTask["PROPERTY_NO_COMISSION_VALUE"])) {
						$arTaskFields["PROPERTY_VALUES"][76] = array("VALUE" => 35); // Не списывать комиссию
					}

					//Проверяем свойство "Первая заявка"
					if ( ! empty($arTask["PROPERTY_FIRST_TASK_VALUE"])) {
						$arTaskFields["PROPERTY_VALUES"][83] = array("VALUE" => 37); // Не списывать комиссию
					}


					// Обновляем
					$DB->StartTransaction();
					if ( ! $obElement->Update($arTask['ID'], $arTaskFields)) {
						AddMessage2Log($obElement->LAST_ERROR, 'UPDATE_ERROR'); // log
						$DB->Rollback();
					} else {
						$DB->Commit();
						CIBlock::clearIblockTagCache(4);
						AddMessage2Log("Сделка с ID {$dealID} обновлена. ID задания {$arTask['ID']}. Стадия - {$arTaskProps[43]}", 'UPDATE_TASK');

					}
				} else {
					if ($arDeal['result']['STAGE_ID'] == $arDealStage['FREE']) {
						// Добавляем
						$obElement = new CIBlockElement();
						if ( ! $taskId = $obElement->Add($arTaskFields)) {
							AddMessage2Log($obElement->LAST_ERROR, 'ADD_ERROR'); // log
						} else {
							AddMessage2Log("Сделка с ID {$dealID} добавлена. Стадия {$arTaskProps[43]}", 'ADD_TASK');
							CIBlock::clearIblockTagCache(4);
						}
					}
				}
			} elseif ($arDeal['result']['STAGE_ID'] === $arDealStage['FULL_COMISSION']) { // Если сделка перешла в стадию "Полный возврат", удаляем заявку и возвращаем деньги исполнителю в полном объеме
				Loader::includeModule('iblock');
				// Проверяем на существование данной заявки в системе
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
				if ($arTask = $rsTask->getNext()) {
					$userId     = CAppforsaleTask::GetProfileId($arTask['ID']);
					$returnSumm = CAppforsaleTask::GetTaskCost($arTask['ID']);
					$message    = "Комиссия по заявке № " . $arTask['ID'] . " возвращена на бонусный счёт в полном объёме";

					// Создаем запись в реестре возвратов
					$bonusReturn = new CAppforsaleBonusReturns($arTask['ID'], $userId, 'full', $returnSumm, $dealID);
					$bonusReturn->add();

					// Создаем транзакцию возврата на бонусный счет
					$bonusTransact = new CAppforsaleTaskBonusTransact($userId, $returnSumm, 1, $message);
					$bonusTransact->addTransact();

					// Отправляем пуш
					bonusReturnPush($userId, $message);
					taskDelete($arTask['ID']);

					// Пересчет рейтинга возваратов
					$rating = new CAppforsaleReturnsRating($userId);
					$rating->setReturnsRating();

					AddMessage2Log("Сделка с ID {$dealID} удалена с полным возмещением денег", 'DELETE_TASK_FULL_REFUND');
				}

			} elseif ($arDeal['result']['STAGE_ID'] === $arDealStage['REFUSE_COMISSION']) { // Если сделка перешла в стадию "Отказ по возврату", удаляем заявку
				Loader::includeModule('iblock');
				// Проверяем на существование данной заявки в системе
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
				if ($arTask = $rsTask->getNext()) {
					$userId     = CAppforsaleTask::GetProfileId($arTask['ID']);
					$returnSumm = CAppforsaleTask::GetTaskCost($arTask['ID']);
					$message    = "В возврате по заявке №" . $arTask['ID'] . " отказано на основании п. 9 договора оказания услуг";

					// Создаем запись в реестре возвратов
					$bonusReturn = new CAppforsaleBonusReturns($arTask['ID'], $userId, 'refuse', $returnSumm, $dealID);
					$bonusReturn->add();

					// Отправляем пуш
					bonusReturnPush($userId, $message);
					taskDelete($arTask['ID']);
					AddMessage2Log("Сделка с ID {$dealID} удалена с оказом в возмещении", 'DELETE_TASK_REFUSE_REFUND');
				}

			} elseif ($arDeal['result']['STAGE_ID'] === $arDealStage['PARTIAL_COMISSION']) { // Если сделка перешла в стадию "Частичный возврат", удаляем заявку и возвращаем часть денег исполнителю
				Loader::includeModule('iblock');
				// Проверяем на существование данной заявки в системе
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
				if ($arTask = $rsTask->getNext()) {
					$userId     = CAppforsaleTask::GetProfileId($arTask['ID']);
					$returnSumm = CAppforsaleTask::GetPartialRefundSum($arTask['ID']);
					$message    = "Комиссия по заявке №" . $arTask['ID'] . " возвращена на бонусный счёт частично согласно п. 9 договора оказания услуг.";

					// Создаем запись в реестре возвратов
					$bonusReturn = new CAppforsaleBonusReturns($arTask['ID'], $userId, 'partial', $returnSumm, $dealID);
					$bonusReturn->add();

					// Создаем транзакцию возврата на бонусный счет
					$bonusTransact = new CAppforsaleTaskBonusTransact($userId, $returnSumm, 1, $message);
					$bonusTransact->addTransact();

					// Отправляем пуш
					bonusReturnPush($userId, $message);
					taskDelete($arTask['ID']);

					// Пересчет рейтинга возваратов
					$rating = new CAppforsaleReturnsRating($userId);
					$rating->setReturnsRating();

					AddMessage2Log("Сделка с ID {$dealID} удалена с возмещением денег", 'DELETE_TASK_PARTIAL_REFUND');
				}

			} elseif ($arDeal['result']['STAGE_ID'] === $arDealStage['MINUS_50']) {
				Loader::includeModule('iblock');
				// Проверяем на существование данной заявки в системе
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
				if ($arTask = $rsTask->getNext()) {
					taskDelete($arTask['ID'], true, 50);
					AddMessage2Log("Сделка {$dealID} ушла в стадию - 50 рублей", 'MINUS_50_STAGE');
				}

			} elseif ($arDeal['result']['STAGE_ID'] === $arDealStage['RELOCATE']) { // Если сделка перешла в стадию "Авто переразмещение", удаляем заявку и возвращаем деньги исполнителю
				Loader::includeModule('iblock');
				// Проверяем на существование данной заявки в системе
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
				if ($arTask = $rsTask->getNext()) {
					taskDelete($arTask['ID'], false);
					AddMessage2Log("Сделка {$dealID} ушла в переразмещение", 'TASK_RELOCATE');
				}

			} elseif (in_array($arDeal['result']['STAGE_ID'], $arDeleteStages)) { //Меняем стадию сделки на "Закрыто для предложений" неуспешно


				Loader::includeModule('iblock');
				$DB->StartTransaction();
				$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $dealID));
				if ($arTask = $rsTask->getNext()) { // Обновляем
					// Если заявка принята в новом приложении.Отменяем в старом и возвращаем средства на бонусный счет мастера
					if ($arDeal['result']['STAGE_ID'] == '35') {
						$arTaskProps[28] = ''; // Адрес
						$arTaskProps[29] = ''; // Телефон
						$arTaskProps[42] = ''; // Исполнитель
						$arTaskProps[59] = ''; // ФИО исполнителя
						$arTaskProps[74] = array("VALUE" => 33); // Требуется списание
						$arTaskProps[76] = array("VALUE" => false); // Не списывать комиссию
						$arTaskProps[83] = array("VALUE" => false); // Первая заявка
						$arTaskProps[84] = ''; // Фактическая цена работ
						CAppforsaleTask::chargeBackTaskCostToBonus($arTask['ID']);
					}

					$arTaskProps[43] = 'E';
					$arTaskFields    = [
						"NAME"              => $arDeal['result']['TITLE'],
						"IBLOCK_ID"         => TASK_IBLOCK_ID,
						"IBLOCK_SECTION_ID" => $taskData['sectionId'],
						"MODIFIED_BY"       => BITRIX_24_USER_ID,
						"XML_ID"            => $dealID,
						"PROPERTY_VALUES"   => $arTaskProps
					];

					$obElement = new CIBlockElement();
					if ( ! $obElement->Update($arTask['ID'], $arTaskFields)) {
						AddMessage2Log($obElement->LAST_ERROR, 'UPDATE_ERROR'); // log
						$DB->Rollback();
					} else {


						$DB->Commit();
						AddMessage2Log("Сделка с ID {$dealID} обновлена (закрыто для предложений)", 'UPDATE_TASK_CLOSE');
						CIBlock::clearIblockTagCache(4);
					}
				}
			}
		}

		if (array_key_exists('error', $arDeal)) {
			echo "Ошибка при получении сделки: " . $arDeal['error_description'] . "<br/>";
		}

	} elseif ($_REQUEST['event'] === 'ONCRMDEALDELETE') {
		Loader::includeModule('iblock');
		// Проверяем на существование данной заявки в системе
		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => TASK_IBLOCK_ID, 'XML_ID' => $_REQUEST["data"]['FIELDS']['ID']));
		if ($arTask = $rsTask->getNext()) {
			taskDelete($arTask['ID'], true);
			AddMessage2Log("Сделка с ID {$_REQUEST["data"]['FIELDS']['ID']} удалена с возмещением денег", 'DELETE_TASK_REFUND');
			CIBlock::clearIblockTagCache(4);
		}
	}
}

