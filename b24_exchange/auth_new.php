<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\Config\Option;

define('APP_ID', 'local.5c7bf1f9937e25.80343879'); // take it from Bitrix24 after adding a new application
define('APP_SECRET_CODE', 'GULqhKSFxGbZTSQVQVf2OnKrOjvFd4DoMvntvI4treig9M62wi'); // take it from Bitrix24 after adding a new application
define('DOMAIN', 'dombezzabot.net'); // the same URL you should set when adding a new application in Bitrix24

function executeHTTPRequest ($queryUrl, array $params = array()) {
    $result = array();
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $curlResult = curl_exec($curl);
    curl_close($curl);

    if ($curlResult != '') $result = json_decode($curlResult, true);

    return $result;
}

function requestRefreshToken1 () {
	$refresh_token = Option::get("mlab.appforsale", "b24_refresh_token", "11", 's1');
    $url = 'https://dombezzabot.net/oauth/token/?' .
        'grant_type=refresh_token'.
        '&client_id='.urlencode(APP_ID).
        '&client_secret='.urlencode(APP_SECRET_CODE).
        '&refresh_token='.urlencode($refresh_token);

	$arNewAccessParams = executeHTTPRequest($url);

	if(is_array($arNewAccessParams) && isset($arNewAccessParams['refresh_token'])) {
		Option::set("mlab.appforsale", "b24_refresh_token", $arNewAccessParams['refresh_token'], "s1");
		Option::set("mlab.appforsale", "b24_access_token", $arNewAccessParams['access_token'], "s1");
	}

//	PR($arNewAccessParams);
}