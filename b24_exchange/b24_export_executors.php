<?
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

if ( ! isset($_GET['page'])) {
	echo '============== НЕ УКАЗАНА СТРАНИЦА ===============';

	return;
}

//return false;

echo '========== НАЧАЛО ЭКСПОРТА ===========';
echo '<br>';

global $USER;

// Соответствия менеджеров из Б24 и CMS
$managersCMStoB24 = [
	"30" => "89", //Александр Артемьев
	"32" => "501", //Лоенид Гордеев
	"34" => "533", //Алия Дрофейчик
	"36" => "673", //Ринат Ганиев
	"1"  => "122", //Евгений Антипов
	"14" => "88", //Артем Родин
	"40" => "1", //В. Чистаков
	"868" => "2403", //Руслан Аеткулов
	"1050" => "242", //Максим Башлыков
];

// Список городов по контакту из Б24
$ufContactCityUrl = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
$arCityContact    = makePostQuery($ufContactCityUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_5C70359B5EB25']]);
$arCityContact24  = remapB24List($arCityContact['result'][0]['LIST']);

echo '========== ГОРОДА КОНТАКТА ===========';
//PR($arCityContact24);
echo '<br>';

// Сопоставляем идетификторы CMS и Б24
$arContactCitiesMap = [];
$res                = CIBlockElement::GetList([], ["IBLOCK_ID" => 1, "ACTIVE" => "Y"], false, [], ['*']);
while ($arFields = $res->GetNext()) {
	if ($arCityContact24[ $arFields['NAME'] ]) {
		$arContactCitiesMap[ $arFields['ID'] ] = $arCityContact24[ $arFields['NAME'] ];
	}
}
echo '========== МАППИНГ ГОРОДОВ КОНТАКТА ===========';
//PR($arContactCitiesMap);
echo '<br>';

// Список видов деятельности из Б24
$ufContactDirectionUrl  = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
$arContactDirectionDeal = makePostQuery($ufContactDirectionUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_1551197597']]);
$arContactDirection24   = remapB24List($arContactDirectionDeal['result'][0]['LIST']);

echo '========== ВИДЫ ДЕЯТЕЛЬНОСТИ ===========';
//PR($arContactDirection24);
echo '<br>';

// Сопоставляем идетификторы CMS и Б24
$arDirectionsMap = [];
$rsSections      = CIBlockSection::GetList(["left_margin" => "asc"], ["IBLOCK_ID" => 4]);
while ($arSection = $rsSections->GetNext()) {
	if ($arContactDirection24[ $arSection['NAME'] ]) {
		$arDirectionsMap[ $arSection['ID'] ] = $arContactDirection24[ $arSection['NAME'] ];
	}
}

echo '========== МАППИНГ ВИДОВ ДЕЯТЕЛЬНОСТИ ===========';
//PR($arDirectionsMap);
echo '<br>';

$order      = array('sort' => 'asc');
$tmp        = 'sort'; // параметр проигнорируется методом, но обязан быть
$userParams = array(
	'SELECT'     => array("NAME", "LOGIN", "UF_CITY"),
	'NAV_PARAMS' => array("nPageSize" => 50, "iNumPage" => $_GET['page'], 'checkOutOfRange' => true)
);
$filter     = array("NAME" => "_%");
$rsUsers    = CUser::GetList($order, $tmp, $filter, $userParams); // выбираем пользователей
while ($arUser = $rsUsers->GetNext()) {
	sleep(0.5);
	// Получаем идентификаторы видов деятельности для профиля
	$arProfileDirectionIds = [];
	$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 2, 'CREATED_BY' => $arUser['ID']));
	if ($arProfile = $rsProfile->getNext()) {
		$dbPropsUserDirections = CIBlockElement::GetProperty(
			$arProfile['IBLOCK_ID'],
			$arProfile['ID'],
			array(),
			array('CODE' => 'PROFILE_WORK')
		);
		while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
			$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
		}
	}

	// Виды детяльности для передачи в Б24
	$executorDirections = [];
	foreach ($arProfileDirectionIds as $dirId) {
		if ($arDirectionsMap[ $dirId ]) {
			$executorDirections[] = $arDirectionsMap[ $dirId ];
		}
	}

	$executorName       = $arUser["NAME"]; // Имя
	$executorLastName   = $arUser["LAST_NAME"]; // Фамилия
	$executorSecondName = $arUser["SECOND_NAME"]; // Отчетство
	$executorPhone      = $arUser["LOGIN"]; // Телефоно
	$executorCity       = $arContactCitiesMap[ $arUser["UF_CITY"] ]; // Город
	$executorDirection  = $executorDirections; // Виды деятельности
	$excutorType        = "SUPPLIER"; // Тип "Исполнитель"

	// Получение исполнителя по телефону
	$executorListUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.list';
	$arExecutors     = makePostQuery($executorListUrl, array('filter' => array("PHONE" => $executorPhone), 'select' => ["ID"]));

	$executorAddData = array(
		'fields' => array(
			'NAME'                 => $executorName, //Имя
			'LAST_NAME'            => $executorLastName, //Фамилия
			'SECOND_NAME'          => $executorSecondName, //Отчество
			'TYPE_ID'              => $excutorType, //тип
			'PHONE'                => array(array("VALUE" => $executorPhone, "VALUE_TYPE" => "WORK")), //Телефон
			'ASSIGNED_BY_ID'       => $managersCMStoB24[ $dealManager ], //Менеджер
			'UF_CRM_5C70359B5EB25' => $executorCity,//Город
			'UF_CRM_1551197597'    => $executorDirection,//Вид деятельности исполнителя
			'UF_CRM_1552309160'    => 'Y',// Признак "Контакт из CMS"
			'OPENED'               => 'Y' // Доступна для всех
		)
	);

	if ($arExecutors['total'] === 0) { // Если исполнители не найдены, тогда добавляем
		$executorUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.add';
		echo '========== ИСПОЛНИТЕЛЬ - НОВЫЙ ===========';
		echo '<br>';

	} else {
		$executorUrl           = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.update';
		$executorAddData['id'] = $arExecutors['result'][0]['ID'];
		echo '========== ИСПОЛНИТЕЛЬ - СУЩЕСТВУЮЩИЙ ===========';
		echo '<br>';
	}

	echo '========== ПОЛЯ ИСПОЛНИТЕЛЯ ===========';
//	PR($executorAddData);
	echo '<br>';

	makePostQuery($executorUrl, $executorAddData);

	unset($arProfile, $arExecutors, $executorAddData);

//	PR($arUpdatedDeal);
}
