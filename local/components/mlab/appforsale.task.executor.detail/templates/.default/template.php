<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

?>


<div class="afs-task-detail">
	<? if ( ! empty($arResult['DISPLAY_PROPERTIES'])): ?>
		<div class="top-line">
			<div class="flex-row no-gutters py-3">
				<div class="col-xs-3">
					<span class="afs-badge <?=$arResult["BADGE_COLOR"]?>"><?=$arResult["STATUS_ID"]['DISPLAY_VALUE']?></span>
				</div>
				<div class="col-xs-9 text-right">
					<span class="text">#SHOW_COUNTER# <?=GetMessage('SHOW_COUNTER')?> | <?=GetMessage('CREATE')?> <?=$arResult["DATE_CREATED"]?></span>
				</div>
			</div>

			<div class="afs-list-divider"></div>
		</div>

		<div class="task-info row">
			<div class="col-12">
				<?=$arResult['IBLOCK_SECTION']['NAME']?> | <?=GetMessage('TASK')?><?=$arResult['ID']?>
			</div>
		</div>

		<div class="afs-task-customer-detail-rows">
			<div class="row d-none d-sm-block d-sm-none d-md-block">
				<div class="col-md-12">
					<h1 style="font-size: 26px"><?=$arResult['NAME']?></h1>
				</div>
			</div>
			<? foreach ($arResult['DISPLAY_PROPERTIES'] as $property): ?>
				<? if ($property['CODE'] === 'FROM_CRM' || $property['CODE'] === 'EXECUTOR_COMMENT') {
					continue;
				} ?>
				<div class="afs-task-customer-detail-row row">
					<div class="col-md-3"><b><?=$property['NAME']?></b></div>
					<? if ($property['CODE'] == 'PHONE'): ?>
						<div class="col-md-9"><a href="tel:<?=$property['VALUE']?>"><?=$property['VALUE']?></a></div>
					<? else: ?>
						<div class="col-md-9"><?=(
							is_array($property['DISPLAY_VALUE'])
								? implode(' / ', $property['DISPLAY_VALUE'])
								: $property['DISPLAY_VALUE']
							)?>
						</div>
					<? endif; ?>
				</div>
			<? endforeach; ?>
		</div>

		<? if ($arResult["HAS_PROBLEM"]): ?>
			<div class="alert alert-warning">
				Ваш запрос <? if ( ! empty($arResult["PROBLEM_TYPE"])): ?>(<?=$arResult["PROBLEM_TYPE"]?>)<? endif; ?> отправлен на рассмотрение в отдел поддержки. <br>Заявка будет рассмотрена в течение пяти рабочих дней.
			</div>
		<? endif; ?>
	<? endif; ?>
</div>
