<?php
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$arResult["STATUS_ID"]   = CIBlockFormatProperties::GetDisplayValue(null, $arResult['PROPERTIES']['STATUS_ID'], "");
$arResult["BADGE_COLOR"] = "";
switch ($arResult["STATUS_ID"]["VALUE"]) {
	case "N":
		$arResult["BADGE_COLOR"] = "afs-badge-success";
		break;
	case "P":
		$arResult["BADGE_COLOR"] = "afs-badge-warning";
		break;
	case "D":
		$arResult["BADGE_COLOR"] = "afs-badge-info";
		break;
	case "F":
		$arResult["BADGE_COLOR"] = "afs-badge-danger";
		break;
}

$arResult["HAS_PROBLEM"]  = ! empty($arResult["PROPERTIES"]["HAS_PROBLEM"]) ? ! empty($arResult["PROPERTIES"]["HAS_PROBLEM"]["VALUE"]) : false;
$arResult["PROBLEM_TYPE"] = ! empty($arResult["PROPERTIES"]["PROBLEM_TYPE"]) ? $arResult["PROPERTIES"]["PROBLEM_TYPE"]["VALUE"] : "";


$arResult["DATE_CREATED"] = ! empty($arResult["PROPERTIES"]["DEAL_CREATED_DATE"]["VALUE"]) ? $arResult["PROPERTIES"]["DEAL_CREATED_DATE"]["VALUE"] : $arResult["DATE_CREATE"];
