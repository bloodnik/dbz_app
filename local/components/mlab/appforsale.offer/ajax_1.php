<?
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);


use Bitrix\Main\Application;
use Bitrix\Main\Loader;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if ( ! check_bitrix_sessid()) {
	echo "Некорректная сессия";

	return;
}

$server  = Application::getInstance()->getContext()->getServer();
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);


$action = $request->getPost('action');
if (empty($action)) {
	echo "Некорректная действие";

	return;
}

if ( ! Loader::includeModule('iblock')) {
	return;
}

$dbElement = CIBlockElement::GetList(
	array(),
	array(
		'ID'     => $request->getPost('id'),
		'ACTIVE' => 'Y'
	),
	false,
	false,
	array(
		'ID',
		'IBLOCK_ID',
		'CREATED_BY',
		"XML_ID"
	)
);
if ($arElement = $dbElement->GetNext()) {
	global $USER;

	// Проблема с заявкой
	if ($action == 'taskProblem') {
		$problemType = $request->getPost('problemType');
		$COMMENT     = htmlspecialcharsbx($request->getPost('comment'));
		$PHOTO       = $request->getFile('photo');

		$DEAL_STATUS_ID = '22'; // Пробема с заявкой

		$returnRequest = new CAppforsaleReturnRequests($arElement['ID'], $arElement["XML_ID"], $USER->GetID(), $problemType, $COMMENT);

		$masterSign = "\n Мастер: " . $USER->GetLastName() . " " . $USER->GetFirstName() . " " . $USER->GetSecondName() . "\n Телефон: " . $USER->GetLogin();

		$problemTypeDescription = "";

		if ( ! empty($problemType)) {
			if ($problemType == "contacts") {
				$COMMENT = "Проблема с заявкой: номер-город \n " . $COMMENT . $masterSign;
				$problemTypeDescription = "Неверный номер телефона или город";
			} elseif ($problemType == "need_quickly") {
				$COMMENT = "Проблема с заявкой: Cрочная заявка \n " . $COMMENT . $masterSign;
				$problemTypeDescription = "Требует срочного исполнения";
			} elseif ($problemType == "change_master") {
				$COMMENT = "Проблема с заявкой: Заявка актуальна. Передать другому мастеру \n " . $COMMENT . $masterSign;
				$problemTypeDescription = "Заявка актуальна. Передать другому мастеру";
			} elseif ($problemType == "price") {
				$DEAL_STATUS_ID = '23'; // Не договорились по цене
				$COMMENT        = "Проблема с заявкой: Не договрились по цене \n " . $COMMENT . $masterSign;
				$problemTypeDescription = "Не договорились по цене";
				$returnRequest->add(); //Добавляем запись в реестр запросов на возврат
			} elseif ($problemType == "back_comission") {
				$DEAL_STATUS_ID = '24'; // Запрос на возврат
				$COMMENT        = "Проблема с заявкой: Возврат комиссии \n " . $COMMENT . $masterSign;
				$problemTypeDescription = "Возврат комиссии";
				$returnRequest->add(); //Добавляем запись в реестр запросов на возврат
			} elseif ($problemType == "task_question") {
				$COMMENT = "Вопрос по заявке: \n " . $COMMENT . $masterSign;
			}

			// Очищаем фотографию от мастера, и загружаем новую
			if ( ! empty($PHOTO['tmp_name'])) {
				if ($arProp = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID'], 'ID', 'DESC', array('CODE' => 'EXECUTOR_FILE'))->fetch()) {
					$XXX = $arProp['PROPERTY_VALUE_ID'];
					CIBlockElement::SetPropertyValueCode($arElement['ID'], 'EXECUTOR_FILE', array($XXX => array('del' => 'Y')));
				}

				// сохраняем файлы в Битрикс
				$fileUploadDir = $server->getDocumentRoot() . '/upload/executorPhotos';

				$arFileBx = array();

				foreach ($PHOTO['name'] as $photoName) {
					$PHOTO['save_to'][] = $fileUploadDir . $arElement['ID'] . '_' . date('Ymd_His_') . $photoName;
				}


				foreach ($PHOTO['tmp_name'] as $key => $tmpName) {
					if ( ! move_uploaded_file($tmpName, $PHOTO['save_to'][ $key ])) {
						$arResult['msg'] .= "Ошибка загрузки файла " . $PHOTO['name'][ $key ] . "\n";
					}
				}

				foreach ($PHOTO['save_to'] as $saveTo) {
					$arFileBx[] = array("VALUE" => CFile::MakeFileArray($saveTo), "DESCRIPTION" => "");
				}


				if (count($arFileBx) > 0) {
					CIBlockElement::SetPropertyValuesEx($arElement['ID'], $arElement['IBLOCK_ID'], array('EXECUTOR_FILE' => $arFileBx));
					foreach ($PHOTO['save_to'] as $saveTo) {
						unlink($saveTo);
					}
				}
			}

			CIBlockElement::SetPropertyValuesEx(
				$arElement['ID'],
				$arElement['IBLOCK_ID'],
				array(
					'EXECUTOR_COMMENT' => $COMMENT
				)
			);

			CIBlockElement::SetPropertyValuesEx(
				$arElement['ID'],
				$arElement['IBLOCK_ID'],
				array(
					'HAS_PROBLEM' => array("VALUE" => 34), //Проблема с заявкой
					"PROBLEM_TYPE" => $problemTypeDescription
				)
			);


			changeDealStage($arElement['ID'], $DEAL_STATUS_ID);
			CIBlock::clearIblockTagCache($arElement['IBLOCK_ID']);
			echo 'true';
		}

	}


	if ($action == 'executorConfirm') {
		global $USER;

		$masterSign = "\n Мастер: " . $USER->GetLastName() . " " . $USER->GetFirstName() . " " . $USER->GetSecondName() . "\n Телефон: " . $USER->GetLogin();

		$COMMENT   = htmlspecialcharsbx($request->getPost('comment'));
		$FACT_SUMM = htmlspecialcharsbx($request->getPost('factSumm'));
		$PHOTO     = $request->getFile('photo');

		// Комментарий
		if ( ! empty($COMMENT) and strlen($COMMENT) > 0) {
			CIBlockElement::SetPropertyValuesEx(
				$arElement['ID'],
				$arElement['IBLOCK_ID'],
				array(
					'EXECUTOR_COMMENT' => $COMMENT . $masterSign,
					'FACT_SUMM'        => $FACT_SUMM
				)
			);
		}

		// Очищаем фотографию от мастера, и загружаем новую
		if ( ! empty($PHOTO['tmp_name'])) {
			if ($arProp = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID'], 'ID', 'DESC', array('CODE' => 'EXECUTOR_FILE'))->fetch()) {
				$XXX = $arProp['PROPERTY_VALUE_ID'];
				CIBlockElement::SetPropertyValueCode($arElement['ID'], 'EXECUTOR_FILE', array($XXX => array('del' => 'Y')));
			}

			// сохраняем файлы в Битрикс
			$fileUploadDir = $server->getDocumentRoot() . '/upload/executorPhotos';

			$arFileBx = array();

			$PHOTO['save_to'] = $fileUploadDir . $arElement['ID'] . '_' . date('Ymd_His_') . $PHOTO['name'];

			if ( ! move_uploaded_file($PHOTO['tmp_name'], $PHOTO['save_to'])) {
				$arResult['msg'] .= "Ошибка загрузки файла " . $PHOTO['name'] . "\n";
			}

			$arFileBx[] = array("VALUE" => CFile::MakeFileArray($PHOTO['save_to']), "DESCRIPTION" => "");

			if (count($arFileBx) > 0) {
				CIBlockElement::SetPropertyValuesEx($arElement['ID'], $arElement['IBLOCK_ID'], array('EXECUTOR_FILE' => $arFileBx));
				unlink($PHOTO['save_to']);
			}
		}

		$tariff     = new CAppforsaleTariff($USER->GetID());
		$userTariff = $tariff->getUserTariff();
		$byTariff   = CAppforsaleTask::byTariff($arElement['ID']);

		//Смена стадии сделки на "Заявка в работе"
		$arStages = [
			"1" => "9", //- Заявка выполнена
			"2" => "26", // Выполнена по тарифу Оптимум
			"3" => "27", // Выполнена по тарифу Макси
			"4" => "28", // // Выполнена по тарифу Макси+
		];

		CAppforsaleComission::writeOfComissionOfTask($arElement["ID"],
			! empty($userTariff["TARIFF_ID"]) ?
				$byTariff ? $arStages[ $userTariff["TARIFF_ID"] ] : "9"
				: "9");

		if (CAppforsaleTask::GetTaskCost($arElement['ID']) > 0) {
			$taskBonus = new CAppforsaleTaskBonus($USER->GetID());
			$taskBonus->incrementClosedTaskCount();
		}

		// Пересчитываем рейтинг возвратов
		if ( ! $byTariff) {
			$rating = new CAppforsaleReturnsRating($USER->GetID());
			$rating->setReturnsRating();
		}

		echo "true";
	}
}
?>