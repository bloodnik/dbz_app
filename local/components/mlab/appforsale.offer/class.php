<?

use Bitrix\Main\Localization\Loc;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

Loc::loadMessages(__FILE__);

class Offer extends \Mlab\Appforsale\Component\ElementList {
	public function onPrepareComponentParams($arParams) {
		$arParams = parent::onPrepareComponentParams($arParams);

		$arParams['MESS_OFFER']       = $arParams['MESS_OFFER'] ?: Loc::getMessage('OFFER');
		$arParams['MESS_BTN_REJECT']  = $arParams['MESS_BTN_REJECT'] ?: Loc::getMessage('BTN_REJECT');
		$arParams['MESS_BTN_CONFIRM'] = $arParams['MESS_BTN_CONFIRM'] ?: Loc::getMessage('BTN_CONFIRM');
		$arParams['MESS_BTN_CANCEL']  = $arParams['MESS_BTN_CANCEL'] ?: Loc::getMessage('BTN_CANCEL');
		$arParams['MESS_BTN_DONE']    = $arParams['MESS_BTN_DONE'] ?: Loc::getMessage('BTN_DONE');
		$arParams['MESS_BTN_COMMENT'] = $arParams['MESS_BTN_COMMENT'] ?: Loc::getMessage('BTN_COMMENT');

		$arParams['COMMENT_URL'] = trim($arParams['COMMENT_URL']);

		return $arParams;
	}

	public function executeComponent() {
		global $USER;

		//$this->arParams['PROFILE_ID'] = 0;
		$this->arParams['STATUS_ID'] = 'N';
		$dbElement                   = CIBlockElement::GetList(
			array(),
			array(
				'ID'     => $this->arParams['TASK_ID'],
				'ACTIVE' => 'Y'
			),
			false,
			false,
			array(
				'PROPERTY_PROFILE_ID',
				'PROPERTY_STATUS_ID',
				'DETAIL_PAGE_URL',
				'PROPERTY_COST',
				'PROPERTY_WORK_COST',
				'PROPERTY_HAS_PROBLEM',
				'PROPERTY_NO_COMISSION'
			)
		);
		if ($arElement = $dbElement->GetNext()) {
			if ($this->arParams['PROFILE_ID'] == 0) {
				$this->arParams['PROFILE_ID'] = intval($arElement['PROPERTY_PROFILE_ID_VALUE']);
			}
			$this->arParams['STATUS_ID']       = $arElement['PROPERTY_STATUS_ID_VALUE'];
			$this->arParams['DETAIL_PAGE_URL'] = $arElement['DETAIL_PAGE_URL'];
			$this->arParams['COST']            = $arElement['PROPERTY_COST_VALUE'];
			$this->arParams['WORK_COST']       = $arElement['PROPERTY_WORK_COST_VALUE'];
			$this->arParams['HAS_PROBLEM']     = $arElement['PROPERTY_HAS_PROBLEM_VALUE'];
			$this->arParams['NO_COMISSION']    = $arElement['PROPERTY_NO_COMISSION_VALUE'];
		}


		//Рейтинг возвратов
		$obUserReturnRating               = new CAppforsaleReturnsRating($USER->GetID());
		$this->arParams["RETURNS_RATING"] = $obUserReturnRating->getReturnsRating();


		$this->arParams['COMMENT_LEFT'] = 'N';

		if ($this->arParams['STATUS_ID'] == 'F') {

			$dbElement = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_CODE'         => 'comment',
					'CREATED_BY'          => $USER->GetID(),
					'PROPERTY_TASK_ID'    => $this->arParams['TASK_ID'],
					'PROPERTY_PROFILE_ID' => $this->arParams['PROFILE_ID']
				)
			);
			if ($arElement = $dbElement->GetNext()) {
				$this->arParams['COMMENT_LEFT'] = 'Y';
			}
		}

		parent::executeComponent();
	}

	protected function getFilter() {
		global $USER;
		$arFilter                     = parent::getFilter();
		$arFilter['PROPERTY_TASK_ID'] = $this->arParams['TASK_ID'];
		if ($this->arParams['PROFILE_ID'] > 0) {
			$arFilter['CREATED_BY'] = $this->arParams['PROFILE_ID'];
		} else {
			$arFilter['!CREATED_BY'] = $USER->GetID();
		}

		return $arFilter;
	}
}

?>