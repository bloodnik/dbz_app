<?

use Bitrix\Main\Loader;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$signer = new \Bitrix\Main\Security\Sign\Signer;
try {
	$params = $signer->unsign($request->get('signedParamsString'), 'appforsale.offer');
	$params = unserialize(base64_decode($params));
} catch (\Bitrix\Main\Security\Sign\BadSignatureException $e) {
	die();
}

$action = $request->get('action');
if (empty($action)) {
	return;
}

if ( ! Loader::includeModule('iblock')) {
	return;
}

$dbElement = CIBlockElement::GetList(
	array(),
	array(
		'ID'     => $request->get('id'),
		'ACTIVE' => 'Y'
	),
	false,
	false,
	array(
		'ID',
		'CREATED_BY',
		'PROPERTY_TASK_ID.ID',
		'PROPERTY_TASK_ID.SECTION_ID',
		'PROPERTY_TASK_ID.IBLOCK_ID',
		'PROPERTY_TASK_ID.CREATED_BY',
		'PROPERTY_PRICE'
	)
);
if ($arElement = $dbElement->GetNext()) {
	if ($arElement['PROPERTY_TASK_ID_CREATED_BY'] == $USER->GetID()) {
		if ($action == 'reject') {
			$obElement = new CIBlockElement;
			$obElement->Update(
				$arElement['ID'],
				array(
					'ACTIVE' => 'N'
				)
			);
			CIBlockElement::Delete($arElement['ID']);

			$dbPropsPartners = CIBlockElement::GetProperty($arElement['PROPERTY_TASK_ID_IBLOCK_ID'], $arElement['PROPERTY_TASK_ID_ID'], array(), array('CODE' => 'PARTNERS'));
			$Partners        = '';
			while ($arPropsPartners = $dbPropsPartners->Fetch()) {
				$profile = CIBlockElement::GetByID($arPropsPartners['VALUE']);
				if ($arProfile = $profile->GetNext()) {
					if ($arProfile['CREATED_BY'] !== $arElement['CREATED_BY']) {
						$Partners[] = $arPropsPartners['VALUE'];
					}
				}
			}

			$arFields = array(
				'PROFILE_ID'    => '',
				'STATUS_ID'     => 'N',
				'PARTNERS'      => $Partners,
				'EXECUTOR_NAME' => ''
			);
			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				$arFields
			);
			/*
			$dbSection = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $arElement['PROPERTY_TASK_ID_IBLOCK_ID'], 'ID' => $arElement['PROPERTY_TASK_ID_SECTION_ID']), false, array('UF_OFFER_PRICE'));
			if($arSection = $dbSection -> GetNext())
			{
				$offer_price = 0;
				if ($arSection['UF_OFFER_PRICE'] > 0){
					$offer_price = $arSection['UF_OFFER_PRICE'];
				}
			}	

			$dbPropsFree = CIBlockElement::GetProperty($arElement['PROPERTY_TASK_ID_IBLOCK_ID'],$arElement['PROPERTY_TASK_ID_ID'],array(),array('CODE' => 'FREE'));	
			if ($arPropsFree = $dbPropsFree->Fetch()){
				if ($arPropsFree['VALUE_XML_ID'] == 'y')
					$free = true;
				else 
					$free = false;
			}	
			*/
			$dbPropsFree = CIBlockElement::GetProperty(
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				$arElement['PROPERTY_TASK_ID_ID'],
				array(),
				array(
					'CODE' => 'COST'
				)
			);
			if ($arPropsFree = $dbPropsFree->Fetch()) {

				if ($arPropsFree['VALUE'] == 0) {
					$free = true;
				} else {
					$free        = false;
					$offer_price = $arPropsFree['VALUE'];
				}
			}
			if ($free == false) {
				\CAppforsaleUserAccount::UpdateAccount($arElement['CREATED_BY'], $offer_price, "ORDER_PART_RETURN");
			}

			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);

			//Меняем стадию сделки на "Свободная заявка"
			changeDealStage($arElement['PROPERTY_TASK_ID_ID'], '7');
		}

		if ($action == 'confirm') {
			$arFields = array(
				'PROFILE_ID' => $arElement['CREATED_BY'],
				'STATUS_ID'  => 'P'
			);

			if (intval($arElement['PROPERTY_PRICE_VALUE']) > 0) {
				$arFields['PRICE'] = $arElement['PROPERTY_PRICE_VALUE'];
			}

			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				$arFields
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
		}

		if ($action == 'cancel') {
			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				array(
					'STATUS_ID' => 'P'
				)
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);

			//Меняем стадию сделки на "Заявка в работе"
			changeDealStage($arElement['PROPERTY_TASK_ID_ID'], '8');
		}

		/*if ($action == 'cancel')
		{
			$obElement = new CIBlockElement;
			$obElement->Update(
				$arElement['ID'],
				array(
					'ACTIVE' => 'N'
				)
			);
			
			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				array(
					'PROFILE_ID' => false,
					'STATUS_ID' => 'N'
				)
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
		}*/

		if ($action == 'finish') {
// 			$obElement = new CIBlockElement;
// 			$obElement->Update(
// 				$arElement['ID'],
// 				array(
// 					'ACTIVE' => 'N'
// 				)
// 			);

			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				array(
					'STATUS_ID' => 'F'
				)
			);


			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);

			//Меняем стадию сделки на "Закрыто для предложений"
			changeDealStage($arElement['PROPERTY_TASK_ID_ID'], 'WON');
		}
		// Отправить push-уведомление $arElement['CREATED_BY']
	}


	if ($action == 'done') {
		CIBlockElement::SetPropertyValuesEx(
			$arElement['PROPERTY_TASK_ID_ID'],
			$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
			array(
				'STATUS_ID' => 'D'
			)
		);
		CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);

		//Меняем стадию сделки на "Заявка выполнена исполнителем"
		changeDealStage($arElement['PROPERTY_TASK_ID_ID'], '9');
	}
}
?>