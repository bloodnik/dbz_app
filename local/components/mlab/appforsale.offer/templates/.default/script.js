/* mlab:appforsale.offer */
(function (window) {

    if (!!window.JCOffer) {
        return;
    }

    window.JCOffer = function (arParams) {
        this.id = 0;
        this.ajaxUrl = null;
        this.commentUrl = null;
        this.signedParamsString = null;
        this.visual = {
            ID: '',
            WRAP: '',
            REJECT: '',
            CONFIRM: '',
            DONE: '',
            CANCEL: '',
            FINISH: '',
            COMMENT: ''
        };

        this.obWrap = null;
        this.obRow = null;
        this.obReject = null;
        this.obDone = null;
        this.obConfirm = null;
        this.obCancel = null;
        this.obFinish = null;
        this.obComment = null;

        if ('object' === typeof arParams) {
            this.id = arParams.ID;
            this.ajaxUrl = arParams.ajaxUrl;
            this.commentUrl = arParams.commentUrl;
            this.signedParamsString = arParams.signedParamsString;
            this.visual = arParams.VISUAL;
        }

        BX.ready(BX.delegate(this.Init, this));
    }

    window.JCOffer.prototype.Init = function () {
        this.obWrap = BX(this.visual.WRAP);
        this.obRow = BX(this.visual.ID);

        this.obReject = BX(this.visual.REJECT);
        this.obConfirm = BX(this.visual.CONFIRM);
        this.obDone = BX(this.visual.DONE);
        this.obCancel = BX(this.visual.CANCEL);
        this.obFinish = BX(this.visual.FINISH);
        this.obComment = BX(this.visual.COMMENT);

        if (!!this.obReject) {
            BX.bind(this.obReject, 'click', BX.delegate(this.Reject, this));
        }

        if (!!this.obDone) {
            BX.bind(this.obDone, 'click', BX.delegate(this.Done, this));
        }

        if (!!this.obConfirm) {
            BX.bind(this.obConfirm, 'click', BX.delegate(this.Confirm, this));
        }

        if (!!this.obCancel) {
            BX.bind(this.obCancel, 'click', BX.delegate(this.Cancel, this));
        }

        if (!!this.obFinish) {
            BX.bind(this.obFinish, 'click', BX.delegate(this.Finish, this));
        }

        if (!!this.obComment) {
            BX.bind(this.obComment, 'click', BX.delegate(this.Comment, this));
        }
    }

    window.JCOffer.prototype.Reject = function () {
        this.ajax('reject', function (html) {
            (fx = new BX.easing({
                duration: 100,
                start: {height: BX.height(this.obWrap), opacity: 100, marginTop: 0},
                finish: {height: 0, opacity: 0, marginTop: -(BX.height(this.obWrap) / 2)},
                transition: BX.easing.transitions.linear,
                step: BX.delegate(function (state) {
                    this.obWrap.style.height = state.height + 'px';
                    this.obWrap.style.opacity = state.opacity / 100;
                    this.obRow.style.marginTop = state.marginTop + 'px';
                }, this),
                complete: BX.delegate(function () {
                    BX.remove(this.obOffer);
                }, this)
            })).animate();
        });
    }

    window.JCOffer.prototype.Confirm = function () {
        this.ajax('confirm', function (html) {
            app.reload();
        });
    }

    window.JCOffer.prototype.Cancel = function () {
        this.ajax('cancel', function (html) {
            app.reload();
        });
    }

    window.JCOffer.prototype.Finish = function () {
        this.ajax('finish', function (html) {
            app.reload();
        });
    }

    window.JCOffer.prototype.Done = function () {
        this.ajax('done', function (html) {
            app.reload();
        });
    }

    window.JCOffer.prototype.Comment = function () {
        document.location = this.commentUrl;
    }

    window.JCOffer.prototype.ajax = function (action, onsuccess) {
        if (!!this.obReject)
            this.obReject.setAttribute('disabled', 'disabled');
        if (!!this.obConfirm)
            this.obConfirm.setAttribute('disabled', 'disabled');
        if (!!this.obDone)
            this.obDone.setAttribute('disabled', 'disabled');
        if (!!this.obCancel)
            this.obCancel.setAttribute('disabled', 'disabled');
        if (!!this.obFinish)
            this.obFinish.setAttribute('disabled', 'disabled');
        if (!!this.obComment)
            this.obComment.setAttribute('disabled', 'disabled');
        BX.ajax({
            url: this.ajaxUrl,
            method: 'POST',
            data: {
                id: this.id,
                action: action,
                signedParamsString: this.signedParamsString
            },
            onprogress: function () {

            },
            onsuccess: BX.delegate(onsuccess, this),
            onfailure: BX.delegate(function () {
                if (!!this.obReject)
                    this.obReject.removeAttribute('disabled');
                if (!!this.obConfirm)
                    this.obConfirm.removeAttribute('disabled');
                if (!!this.obDone)
                    this.obDone.removeAttribute('disabled');
                if (!!this.obCancel)
                    this.obCancel.removeAttribute('disabled');
                if (!!this.obFinish)
                    this.obFinish.removeAttribute('disabled');
                if (!!this.obComment)
                    this.obComment.removeAttribute('disabled');
            }, this)
        });
    }

})(window);