<?

use Bitrix\Main;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<style>
	#afsTaskConfirmPanel .custom-file-input:lang(ru) ~ .custom-file-label::after {
		content: 'Выбрать';
	}

	.custom-file-input.is-invalid ~ .custom-file-label, .was-validated .custom-file-input:invalid ~ .custom-file-label {
		border-color: #8bc34a;
	}
</style>
<div id="afsTaskConfirmPanel2" v-cloak>
	<? if ( ! empty($arResult['ITEMS'])): ?>
		<? foreach ($arResult['ITEMS'] as $arItem): ?>
			<? if ($arItem['CREATED_BY'] == $USER->GetID()): ?>
				<? if ($arParams['STATUS_ID'] == 'P' && empty($arParams["HAS_PROBLEM"])): ?>

					<div class="afs-task-confirm-panel" style="z-index: 999999">
						<div>
							<button class="btn afs-btn-success btn-block" @click="confirmSheetShow()" type="button">Исполнено</button>

							<p class="mt-3 text-muted">Проблема с заявкой</p>

							<b-alert v-if="returnsRating >= 35" show variant="warning">
								Процент возвратов превышен, оформить запрос по заявке будет невозможно
							</b-alert>

							<template v-for="problemType in problemTypesActive">
								<button class="btn btn-sm btn-block" :class="problemType.color" @click="problemSheetShow(problemType)" :key="'problem_' + problemType.type" type="button">
									{{problemType.name}}
								</button>
								<hr class="my-1">
							</template>
						</div>

						<div class="overlay d-flex justify-content-center my-auto" :class="{'loading': sending}" v-if="confirmSheetState || problemSheetState">
							<b-spinner v-if="sending" class="loader-spinner" variant="success" label="Подождите..."></b-spinner>
						</div>

						<transition name="slide">
							<div v-if="confirmSheetState" class="afs-bottom-sheet__panel">

								<div class="afs-bottom-sheet__content">
									<div class="h6 text-center">
										Подтвердите исполнение
										<a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
									</div>

									<div class="p-3">
										<b-form-group label="Комментарий">
											<textarea style="width: 100%;" v-model="executorComment" :tab-index="1" rows="4" placeholder="Оставьте комментарий по работе"></textarea>

										</b-form-group>

										<b-form-group label="Фактическая цена работ">
											<b-form-input v-model="factSumm" type="number" autocomplete="off" placeholder="Введите цену"></b-form-input>
										</b-form-group>

										<b-form-file v-model="executorFile" :state="Boolean(executorFile)" accept=".jpg, .png, .gif" placeholder="Выберите файл"></b-form-file>

										<div class="text-right mt-3">
											<b-button @click="sendConfirm()" variant="link" :disabled="sending">
												<b-spinner v-if="sending" small type="grow"></b-spinner>
												Подтвердить
											</b-button>
										</div>
									</div>
								</div>
							</div>

							<div v-if="problemSheetState && selectedProblem" class="afs-bottom-sheet__panel">
								<div class="h6 text-center">
									{{ selectedProblem.name }}
									<a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
								</div>

								<div class="p-3">
									<b-form-group label="Заполните комментарий по проблеме *">
									</b-form-group>
									<textarea style="width: 100%;" v-model="executorComment" :tab-index="1" rows="4" :placeholder="selectedProblem.placeholder"></textarea>
									<small v-if="errors.commentRequired" class="text-danger"> Коментарий обязетелен для заполнения </small>


									<b-form-file class="mb-2" v-for="(executorFile, index) in executorFiles" v-model="executorFiles[index]" :state="Boolean(executorFiles[index])" accept=".jpg, .png, .gif" placeholder="Выберите файл"></b-form-file>
									<b-link @click="executorFiles.push([])"> Добавить еще файл</b-link>

									<p class="small">
										Для ускорения рассмотрения Вашего запроса на возврат, пожалуйста опишите ситуацию как можно подробнее, при наличии, прикрепите к запросу скриншот журнала звонков заказчику и запись разговора с ним
									</p>

									<div class="text-right mt-1">
										<b-button @click="sendTaskProblem()" variant="link" :disabled="sending">
											<b-spinner v-if="sending" small type="grow"></b-spinner>
											Отправить
										</b-button>
									</div>
								</div>


							</div>
						</transition>
					</div>
				<? endif; ?>
			<? endif; ?>
		<? endforeach; ?>
	<? endif; ?>
</div>

<script>
	var taskConfirmPanel2 = new Vue({
		el: '#afsTaskConfirmPanel2',
		data: function () {
			return {
				taskId: "<?=$arParams["TASK_ID"]?>",
				returnsRating: <?=$arParams["RETURNS_RATING"]?>,
				executorComment: "",
				factSumm: null,
				executorFile: null,
				executorFiles: [[]],
				confirmSheetState: false,
				problemSheetState: false,
				sessid: "<?=bitrix_sessid()?>",
				sending: false,
				hasError: false,
				message: "",
				ajaxUrl: "<?=CUtil::JSEscape($component->getPath() . '/ajax_1.php')?>",
				problemTypes: [
					{type: "back_comission", name: "Возврат комиссии", color: "afs-btn-danger", placeholder: "Опишите причину возврата детально", active: !!<?=empty($arParams["NO_COMISSION"]) && $arParams["RETURNS_RATING"] < 35 ? 1 : 0?>},
					{type: "contacts", name: "Неверный номер телефона или город", color: "afs-btn-warning", placeholder: "Что именно указано не верно?", active: true},
					{type: "price", name: "Не договорились по цене", color: "afs-btn-warning", placeholder: "Какая сумма озвучена клиенту, какая сумма его устроит?", active: !!<?=empty($arParams["NO_COMISSION"]) ? 1 : 0?>},
					{type: "need_quickly", name: "Требует срочного исполнения", color: "afs-btn-warning", placeholder: "Когда подъехать к клиенту, или в течение какого времени?", active: !!<?=empty($arParams["NO_COMISSION"]) ? 1 : 0?>},
					{type: "change_master", name: "Заявка актуальна. Передать другому мастеру", color: "afs-btn-warning", placeholder: "Опишите причину детально", active: !!<?=empty($arParams["NO_COMISSION"]) ? 0 : 1?>},
				],
				errors: {
					commentRequired: false
				},
				selectedProblem: null
			}
		},
		computed: {
			problemTypesActive() {
				return this.problemTypes.filter(function (problem) {
					return problem.active;
				})
			}
		},
		methods: {
			sendConfirm: function () {
				var _this = this;

				_this.sending = true;
				_this.hasError = false;
				_this.message = "";

				var fd = new FormData();
				fd.append('id', _this.taskId);
				fd.append('comment', _this.executorComment);
				fd.append('factSumm', _this.factSumm);
				fd.append('photo', _this.executorFile);
				fd.append('sessid', _this.sessid);
				fd.append('action', "executorConfirm");

				try {
					axios.post(_this.ajaxUrl, fd, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					}).then(function (response) {
						var data = response.data;

						if (data) {
							_this.confirmSheetState = false;
							_this.sending = false;
							_this.executorComment = "";
							_this.factSumm = null;
							_this.executorFile = null;
							app.reload();
						} else {
							_this.hasError = true;
							_this.message = "Ошибка";
							_this.sending = false;
						}

					})

				} catch (e) {
					console.log(e);
				}
			}
			,

			sendTaskProblem: function () {
				var _this = this;

				if (!_this.selectedProblem) return;

				_this.errors.commentRequired = false;
				if (_this.executorComment.length === 0) {
					_this.errors.commentRequired = true;
					return;
				}

				_this.sending = true;
				_this.hasError = false;
				_this.message = "";

				var fd = new FormData();
				fd.append('id', _this.taskId);
				fd.append('problemType', _this.selectedProblem.type);
				fd.append('comment', _this.executorComment);

				if (!!_this.executorFiles.length) {
					_this.executorFiles.forEach(function (file, index) {
						if (!file) return;
						fd.append('photo[' + index + ']', file);
					})
				}

				fd.append('sessid', _this.sessid);
				fd.append('action', "taskProblem");


				try {
					axios.post(_this.ajaxUrl, fd, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					}).then(function (response) {
						var data = response.data;

						if (data) {
							_this.problemSheetState = false;
							_this.sending = false;
							_this.executorFiles = [[]];
							app.reload();
						} else {
							_this.hasError = true;
							_this.message = "Ошибка";
							_this.sending = false;
						}
					})
				} catch (e) {
					console.log(e);
				}
			},

			confirmSheetShow: function () {
				this.closeBtnHandler();
				this.confirmSheetState = true;
			},

			problemSheetShow: function (problem) {
				this.closeBtnHandler();
				this.selectedProblem = problem;

				if (this.selectedProblem) {
					this.problemSheetState = true;
				}
			}
			,
			closeBtnHandler: function () {
				this.executorComment = "";
				this.selectedProblem = null;
				this.confirmSheetState = false;
				this.problemSheetState = false;
			}
			,
		}
		,
	});

</script>