<?

use Bitrix\Main;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

?>
<style>
	#afsTaskConfirmPanel .custom-file-input:lang(ru) ~ .custom-file-label::after {
		content: 'Выбрать';
	}

	.custom-file-input.is-invalid ~ .custom-file-label, .was-validated .custom-file-input:invalid ~ .custom-file-label {
		border-color: #8bc34a;
	}
</style>
<div id="afsTaskConfirmPanel" class="mt-5" v-cloak>
	<? if ( ! empty($arResult['ITEMS'])): ?>
		<? foreach ($arResult['ITEMS'] as $arItem): ?>
			<? if ($arItem['CREATED_BY'] == $USER->GetID()): ?>
				<? if ($arParams['STATUS_ID'] == 'P'): ?>

					<hr>
					<div class="afs-task-confirm-panel" style="z-index: 999999">
						<div class="text-right">
							<button class="btn btn-success btn-sm" @click="confirmSheetShow()" type="button">Исполнено</button>
							<button class="btn btn-warning btn-sm" @click="problemSheetShow()" type="button">Проблема с заявкой</button>
						</div>

						<div class="overlay d-flex justify-content-center my-auto" :class="{'loading': sending}" v-if="confirmSheetState || problemSheetState">
							<b-spinner v-if="sending" class="loader-spinner" variant="success" label="Подождите..."></b-spinner>
						</div>

						<transition name="slide">
							<div v-if="confirmSheetState" class="afs-bottom-sheet__panel">

								<div class="afs-bottom-sheet__content">
									<div class="h6 text-center">
										Подтвердите исполнение
										<a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
									</div>

									<div class="p-3">
										<b-form-group label="Комментарий">
											<textarea style="width: 100%;" v-model="executorComment" :tab-index="1" rows="4" placeholder="Оставьте комментарий по выполненной работе..."></textarea>
										</b-form-group>

										<b-form-file v-model="executorFile" :state="Boolean(executorFile)" accept=".jpg, .png, .gif" placeholder="Выберите файл"></b-form-file>

										<div class="text-right mt-3">
											<b-button @click="sendConfirm()" variant="link" :disabled="sending">
												<b-spinner v-if="sending" small type="grow"></b-spinner>
												Подтвердить
											</b-button>
										</div>
									</div>
								</div>
							</div>

							<div v-if="problemSheetState" class="afs-bottom-sheet__panel">
								<div class="h6 text-center">
									Укажите проблему
									<a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
								</div>

								<div class="p-3">
									<b-list-group>
										<b-list-group-item :diabled="sending" @click="sendTaskProplem('contacts')">Не верный номер телефона или город</b-list-group-item>
										<b-list-group-item :diabled="sending" @click="sendTaskProplem('need_quickly')">Клиенту нужно срочно, не могу выполнить</b-list-group-item>
									</b-list-group>
								</div>
							</div>
						</transition>
					</div>
				<? endif; ?>
			<? endif; ?>
		<? endforeach; ?>
	<? endif; ?>
</div>

<script>
    var taskConfirmPanel = new Vue({
        el: '#afsTaskConfirmPanel',
        data: function () {
            return {
                taskId: "<?=$arParams["TASK_ID"]?>",
                executorComment: "",
                executorFile: null,
                confirmSheetState: false,
                problemSheetState: false,
                sessid: "<?=bitrix_sessid()?>",
                sending: false,
                hasError: false,
                message: "",
                ajaxUrl: "<?=CUtil::JSEscape($component->getPath() . '/ajax_1.php')?>",
            }
        },
        methods: {
            sendConfirm: function () {
                var _this = this;

                _this.sending = true;
                _this.hasError = false;
                _this.message = "";

                var fd = new FormData();
                fd.append('id', _this.taskId);
                fd.append('comment', _this.executorComment);
                fd.append('photo', _this.executorFile);
                fd.append('sessid', _this.sessid);
                fd.append('action', "executorConfirm");

                try {
                    axios.post(_this.ajaxUrl, fd, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        var data = response.data;

                        if (data) {
                            _this.confirmSheetState = false;
                            _this.sending = false;
                            _this.executorComment = "";
                            _this.executorFile = null;
                            app.reload();
                        } else {
                            _this.hasError = true;
                            _this.message = "Ошибка";
                            _this.sending = false;
                        }

                    })

                } catch (e) {
                    console.log(e);
                }
            },

            sendTaskProplem: function (problemType) {
                var _this = this;

                _this.sending = true;
                _this.hasError = false;
                _this.message = "";

                var fd = new FormData();
                fd.append('id', _this.taskId);
                fd.append('problemType', problemType);
                fd.append('sessid', _this.sessid);
                fd.append('action', "taskProblem");

                try {
                    axios.post(_this.ajaxUrl, fd, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(function (response) {
                        var data = response.data;

                        if (data) {
                            _this.problemSheetState = false;
                            _this.sending = false;
                            app.reload();
                        } else {
                            _this.hasError = true;
                            _this.message = "Ошибка";
                            _this.sending = false;
                        }
                    })
                } catch (e) {
                    console.log(e);
                }
            },

            confirmSheetShow: function () {
                this.closeBtnHandler();
                this.confirmSheetState = true;
            },
            problemSheetShow: function () {
                this.closeBtnHandler();
                this.problemSheetState = true;
            },
            closeBtnHandler: function () {
                this.confirmSheetState = false;
                this.problemSheetState = false;
            },
        },
    });

</script>