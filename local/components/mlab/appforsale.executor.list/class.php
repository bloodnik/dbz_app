<? 
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class ExecutorList extends \Mlab\Appforsale\Component\ElementList
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['HIDE_NONAME'] = $arParams['HIDE_NONAME'] == 'Y' ? 'Y' : 'N';
		return $arParams;
	}
	
	protected function getFilter()
	{

		$arFilter = parent::getFilter();
		
		if (!empty($_GET['sections']))
		{
			$arFilter['SECTION_ID'] = explode(',', $_GET['sections']);
			$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		}
		$arFilter['%NAME'] = $_GET['name'];
		
		return $arFilter;
	}
	
	protected function getAdditionalCacheId()
	{
		$res = parent::getAdditionalCacheId();
		$res[] = $_GET['sections'];

		return $res;
	}
	
	protected function getSort()
	{
		return array();
	}
	
	protected function getGroup()
	{
		return array('CREATED_BY');
	}
}
?>