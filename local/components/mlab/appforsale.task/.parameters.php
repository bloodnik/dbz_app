<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arSort = CIBlockParameters::GetElementSortFields(
	array('TIMESTAMP_X', 'SORT', 'ID'),
	array('KEY_LOWERCASE' => 'Y')
);

$arAscDesc = array(
	'asc' => Loc::getMessage('SORT_ASC'),
	'desc' => Loc::getMessage('SORT_DESC')
);

$arComponentParameters = array(
	'GROUPS' => array(
		'LIST_SETTINGS' => array(
			'NAME' => Loc::getMessage('LIST_SETTINGS')
		),
		'DETAIL_SETTINGS' => array(
			'NAME' => Loc::getMessage('DETAIL_SETTINGS')
		),
		'OFFER_SETTINGS' => array(
			'NAME' => Loc::getMessage('OFFER_SETTINGS')
		),
		'OFFER_FORM_SETTINGS' => array(
			'NAME' => Loc::getMessage('OFFER_FORM_SETTINGS')
		),
		'FILTER_SETTINGS' => array(
			'NAME' => Loc::getMessage('FILTER_SETTINGS')
		),
		'SORT_SETTINGS' => array(
			'NAME' => Loc::getMessage('SORT_SETTINGS')
		),
		'MAP_SETTINGS' => array(
			'NAME' => Loc::getMessage('MAP_SETTINGS')
		)
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'OFFER_IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('OFFER_IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'OFFER_IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('OFFER_IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['OFFER_IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'CACHE_TIME' => array('DEFAULT' => 36000000),
		'SEF_MODE' => array(
			'list' => array(
				'NAME' => Loc::getMessage('SEF_MODE_LIST'),
				'DEFAULT' => '',
				'VARIABLES' => array()
			),
			'detail' => array(
				'NAME' => Loc::getMessage('SEF_MODE_DETAIL'),
				'DEFAULT' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH',
					'ELEMENT_ID',
					'ELEMENT_CODE',
				)
			),
			'offer' => array(
				'NAME' => Loc::getMessage('SEF_MODE_OFFER'),
				'DEFAULT' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/offer/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH',
					'ELEMENT_ID',
					'ELEMENT_CODE',
				)
			)
		),
		'OWNER' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('OWNER'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N'
		),
		'ELEMENT_SORT_FIELD' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'timestamp_x',
		),
		'ELEMENT_SORT_ORDER' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'ELEMENT_SORT_FIELD2' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'id',
		),
		'ELEMENT_SORT_ORDER2' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
			
			
			
			
			
			
			
			
		'LIST_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('LIST_FIELD_CODE'), 'LIST_SETTINGS'),
		'LIST_PROPERTY_CODE' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('LIST_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'DETAIL_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('DETAIL_FIELD_CODE'), 'DETAIL_SETTINGS'),
		'DETAIL_PROPERTY_CODE' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'OFFER_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('OFFER_FIELD_CODE'), 'DETAIL_SETTINGS'),
		'OFFER_PROPERTY_CODE' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['OFFER_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'OFFER_MESS_BTN_ADD' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_MESS_BTN_ADD'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('OFFER_MESS_BTN_ADD_DEFAULT')
		),
		'DETAIL_MESS_CUSTOMER' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_MESS_CUSTOMER'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DETAIL_MESS_CUSTOMER_DEFAULT')
		),
		'DETAIL_MESS_CLAIM' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_MESS_CLAIM'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DETAIL_MESS_CLAIM_DEFAULT')
		),
		'DETAIL_MESS_YOUR_OFFER' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_MESS_YOUR_OFFER'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DETAIL_MESS_YOUR_OFFER_DEFAULT')
		),	
		'OFFER_FORM_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('OFFER_FORM_FIELD_CODE'), 'OFFER_FORM_SETTINGS'),
		'OFFER_FORM_PROPERTY_CODE' => array(
			'PARENT' => 'OFFER_FORM_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_FORM_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['OFFER_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),	
		'OFFER_FORM_PROPERTY_CODE_REQUIRED' => array(
			'PARENT' => 'OFFER_FORM_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_FORM_PROPERTY_CODE_REQUIRED'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['OFFER_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),			
		'OFFER_FORM_MESS_PROPERTY_VALUE_NA' => array(
			'PARENT' => 'OFFER_FORM_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_FORM_MESS_PROPERTY_VALUE_NA'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('OFFER_FORM_MESS_PROPERTY_VALUE_NA_DEFAULT')
		),
		'OFFER_FORM_MESS_BTN_ADD' => array(
			'PARENT' => 'OFFER_FORM_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_FORM_MESS_BTN_ADD'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('OFFER_FORM_MESS_BTN_ADD_DEFAULT')
		),
		'USE_FILTER' => array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('USE_FILTER'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
			'REFRESH' => 'Y'
		),
		'USE_SORT' => array(
			'PARENT' => 'SORT_SETTINGS',
			'NAME' => Loc::getMessage('USE_SORT'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y'
		),
		'USE_MAP' => array(
			'PARENT' => 'MAP_SETTINGS',
			'NAME' => Loc::getMessage('USE_MAP'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y'
		)
				
	)
);

if ($arCurrentValues['USE_FILTER'] == 'Y')
{

	
	$arComponentParameters['PARAMETERS']['CNT'] = array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('FILTER_SETTINGS_CNT'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N'
	);
	
	$arComponentParameters['PARAMETERS']['FILTER_SORT_FIELD'] = array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('FILTER_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'timestamp_x',
	);
	$arComponentParameters['PARAMETERS']['FILTER_SORT_ORDER'] = array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('FILTER_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
	);
	$arComponentParameters['PARAMETERS']['FILTER_SORT_FIELD2'] = array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('FILTER_SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'id',
	);
	$arComponentParameters['PARAMETERS']['FILTER_SORT_ORDER2'] = array(
			'PARENT' => 'FILTER_SETTINGS',
			'NAME' => Loc::getMessage('FILTER_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
	);
}

if ($arCurrentValues['USE_SORT'] == 'Y')
{
	$arComponentParameters['PARAMETERS']['SORT_FIELD'] = array(
			'PARENT' => 'SORT_SETTINGS',
			'NAME' => Loc::getMessage('SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => array(),
		//	'DEFAULT' => 'NAME',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'ADDITIONAL_VALUES' => 'Y'
	);	
}

if ($arCurrentValues['SEF_MODE'] == 'Y')
{
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES'] = array();
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_ID'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_ID'),
		'TEMPLATE' => '#SECTION_ID#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE'),
		'TEMPLATE' => '#SECTION_CODE#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE_PATH'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE_PATH'),
		'TEMPLATE' => '#SECTION_CODE_PATH#'
	);
}
?>