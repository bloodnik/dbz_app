<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
if ($arParams['USE_FILTER'] == 'Y')
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.filter',
		'',
		array(
			'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
 			'CACHE_TYPE' => "A",
 			'CACHE_TIME' => $arParams['CACHE_TIME'],
			'SORT_FIELD' => $arParams['FILTER_SORT_FIELD'],
			'SORT_ORDER' => $arParams['FILTER_SORT_ORDER'],
			'SORT_FIELD2' => $arParams['FILTER_SORT_FIELD2'],
			'SORT_ORDER2' => $arParams['FILTER_SORT_ORDER2'],
			'CNT' => $arParams['CNT']
		),
		$component
	);
?>
<?
if ($arParams['USE_SORT'] == 'Y')
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.sort',
		'',
		array(
			'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'FIELD_CODE' => $arParams['SORT_FIELD']
		),
		$component
	);
?>
<div id="list_for_filter">
<?
if (array_key_exists('sections', $_GET))
	$APPLICATION->RestartBuffer();

if (array_key_exists('sort', $_GET))
	$APPLICATION->RestartBuffer();

if ($arParams['USE_MAP'] == 'Y')
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.map',
		'',
		array(
			'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'CACHE_TYPE' => $arParams['CACHE_TYPE'],
			'CACHE_TIME' => $arParams['CACHE_TIME'],
			'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail'],
			'OWNER' => $arParams['OWNER']	
		),
		$component
	);
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.task.list',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'FIELD_CODE' => $arParams['LIST_FIELD_CODE'],
		'PROPERTY_CODE' => $arParams['LIST_PROPERTY_CODE'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail'],
		'SORT_FIELD' => $arParams['ELEMENT_SORT_FIELD'],
		'SORT_ORDER' => $arParams['ELEMENT_SORT_ORDER'],
		'SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
		'SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
		'OWNER' => $arParams['OWNER']	
	),
	$component
);
?>
</div>