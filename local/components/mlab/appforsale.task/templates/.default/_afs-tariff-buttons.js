Vue.component('afs-tariff-buttons', {
    name: "afsTariffButtons",
    props: {
        availabaleOptions: {
            type: Array,
            default() {
                return [];
            }
        },
        backurl: {
            type: String,
            default: ""
        },
        comission: {
            type: Number,
            default: 0
        },
        comissionTypeId: {
            type: String,
            default: ""
        },
        sessid: {
            type: String,
            default: ""
        },
        onlyPackageButtons: {
            type: Boolean,
            default: false
        }
    },

    data: function () {
        return {
            ajaxUrl: "/local/components/siril/app.tariffs/ajax.php",
            confirmOptions: {
                html: true
            },
            hasError: false
        }
    },

    computed: {
        filteredOptions() {
            var _this = this;

            if (!onlyPackageButtons) return _this.availabaleOptions;

            _this.availabaleOptions.filter(function (option) {
                return option.UF_TYPE != _this.comissionTypeId
            })
        }
    },

    methods: {
        clickTariffButtonHandler(availabaleOption) {
            var _this = this;

            if (availabaleOption.UF_TYPE === this.comissionTypeId) {
                _this.setTariff(availabaleOption, true);
            } else {
                _this.setTariff(availabaleOption, false);
            }

        },

        setTariff: function setTariff(tariff, getTask) {
            var _this = this;

            let confirmMessage = {
                html: true,
                title: 'Применить изменения?',
                body: "Применить тариф " + tariff.UF_NAME + "<br><br> Количество заявок в тарифе: " + tariff.UF_TASK_COUNT + "<br> Тариф действует: " + tariff.UF_ACTIVE_DAYS + " дн. <br> Цена тарифа: " + tariff.UF_PRICE + " руб. <br> Цена заявки по" +
                    " тарифу: " + parseInt(tariff.UF_PRICE, 10) / parseInt(tariff.UF_TASK_COUNT, 10) + " руб. (без дополнительных комиссий)"
            };

            if (getTask) {
                confirmMessage.body = "Применить тариф " + tariff.UF_NAME + " и взять заявку в работу?";
            }

            Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
                _this.sending = true;
                _this.hasError = false;
                _this.message = "";
                var fd = new FormData();
                fd.append('sessid', _this.sessid);
                fd.append('type', "setTariff");
                fd.append('tariffId', tariff.ID);

                try {
                    axios.post(_this.ajaxUrl, fd).then(function (response) {
                        var data = response.data;

                        if (data && !data.hasError) {
                            if (getTask) {
                                _this.$emit("success", tariff);
                            } else {
                                app.reload();
                            }
                            _this.sending = false;
                        } else {
                            _this.hasError = true;
                            _this.message = data.msg;
                            _this.sending = false;
                        }
                    });
                } catch (e) {
                    console.log(e);
                    _this.sending = false;
                    _this.hasError = true;
                    _this.message = e;
                }
            });

        },

        buttonText: function (availabaleOption) {
            switch (availabaleOption.UF_TYPE) {
                case this.comissionTypeId:
                    return "Принять за 10% = " + this.comission + " руб."
                default:
                    return "Принять за " + parseInt(availabaleOption.UF_PRICE, 10) / parseInt(availabaleOption.UF_TASK_COUNT, 10) + " руб.";
            }
        }
    },

    template: `
        <div>
            <div v-if="hasError" class="alert alert-danger">{{ message }}</div>
            <a class="btn btn-afs afs-btn-outline-success btn-block" @click="clickTariffButtonHandler(availabaleOption)" v-for="availabaleOption in filteredOptions" :key="availabaleOption.ID">
                {{ buttonText(availabaleOption) }}
            </a>
        </div>
    `
})