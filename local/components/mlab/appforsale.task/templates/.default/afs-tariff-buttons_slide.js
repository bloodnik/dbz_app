Vue.component('afs-tariff-buttons', {
	name: "afsTariffButtons",
	props: {
		availabaleOptions: {
			type: Array,
			default() {
				return [];
			}
		},
		backurl: {
			type: String,
			default: ""
		},
		comission: {
			type: Number,
			default: 0
		},
		workCost: {
			type: Number,
			default: 0
		},
		comissionTypeId: {
			type: String,
			default: ""
		},
		sessid: {
			type: String,
			default: ""
		},
		onlyPackageButtons: {
			type: Boolean,
			default: false
		},
		hasFirstTask: {
			type: Boolean,
			default: false
		}
	},

	data: function () {
		return {
			ajaxUrl: "/local/components/siril/app.tariffs/ajax.php",
			confirmOptions: {
				html: true,
				customClass: "dbz-dialog"
			},
			hasError: false,
			bottomSheetState: false
		}
	},

	computed: {
		filteredOptions() {
			var _this = this;

			let options = _this.availabaleOptions.filter(function (option) {
				const costFrom = option.UF_COST_FROM || 0;
				const costTo = option.UF_COST_TO || 5000000;

				return parseInt(costFrom, 10) <= _this.workCost && parseInt(costTo, 10) >= _this.workCost

			})

			if (!_this.onlyPackageButtons && (this.workCost <= 5000 || this.comission === 0)) return options;

			return options.filter(function (option) {
				return option.UF_TYPE !== _this.comissionTypeId
			})
		},

		sheetHeight: function () {
			return {
				height: window.innerHeight - (BM.MobileApp.isDevice() ? 56 : 108) + "px"
			}
		},

		comissionTariff: function () {
			return this.availabaleOptions.find(function (option) {
				return option.ID === "1";
			})
		}


	},


	methods: {
		clickButtonHandler() {
			this.bottomSheetState = true
		},
		closeBtnHandler: function () {
			this.bottomSheetState = false;
		},

		clickTariffButtonHandler(availabaleOption) {
			var _this = this;

			if (availabaleOption.UF_TYPE === this.comissionTypeId) {
				_this.setTariff(availabaleOption, true);
			} else {
				_this.setTariff(availabaleOption, false);
			}
		},

		buyTask() {
			var _this = this;

			Vue.dialog.confirm("Взять заявку в работу?", _this.confirmOptions).then(function (dialog) {
				_this.$emit("success", _this.comissionTariff);
			});
		},

		setTariff: function setTariff(tariff, getTask) {
			var _this = this;

			let firstTaskConfirm = _this.hasFirstTask ? "<div class='mt-3 alert alert-info'><p>Вы уверены, что хотите принять заявку или активировать пакетный тариф? Если у Вас ещё остались вопросы по условиям сервиса, задайте вопрос в" +
				" чат <a href='https://wa.me/79871051800' >\"Сложный вопрос\"</a> </p></div>" : "";

			let confirmMessage = {
				html: true,
				title: 'Применить изменения?',
				body: "Применить тариф " + tariff.UF_NAME + "<br><br> Количество заявок в тарифе: " + tariff.UF_TASK_COUNT + "<br> Тариф действует: " + tariff.UF_ACTIVE_DAYS + " дн. <br> Цена тарифа: " + tariff.UF_PRICE + " руб. <br> Цена заявки по" +
					" тарифу: " + parseInt(tariff.UF_PRICE, 10) / parseInt(tariff.UF_TASK_COUNT, 10) + " руб. (без дополнительных комиссий) <br>" +
					"Доступны заявки " + (!!tariff.UF_COST_FROM ? "от " + tariff.UF_COST_FROM + " руб." : "") + (!!tariff.UF_COST_TO ? "до " + tariff.UF_COST_TO + " руб." : "") + firstTaskConfirm
			};

			if (getTask) {
				confirmMessage.body = "Применить тариф " + tariff.UF_NAME + " и взять заявку в работу?" + firstTaskConfirm;
			}

			Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
				_this.sending = true;
				_this.hasError = false;
				_this.message = "";
				var fd = new FormData();
				fd.append('sessid', _this.sessid);
				fd.append('type', "setTariff");
				fd.append('tariffId', tariff.ID);

				try {
					axios.post(_this.ajaxUrl, fd).then(function (response) {
						var data = response.data;

						if (data && !data.hasError) {
							if (getTask) {
								_this.$emit("success", tariff);
							} else {
								app.reload();
							}
							_this.sending = false;
						} else {
							_this.hasError = true;
							_this.message = data.msg;
							_this.sending = false;
						}
					});
				} catch (e) {
					console.log(e);
					_this.sending = false;
					_this.hasError = true;
					_this.message = e;
				}
			});

		},

		formattedTariffPrice: function (option) {
			switch (option.UF_TYPE) {
				case this.comissionTypeId:
					if (this.hasFirstTask) {
						return this.comission / 2 + " ₽ (скидка 50%)";
					}
					return this.comission + " ₽ (10% от заказа)";
				default:
					return option.UF_PRICE + " ₽";
			}
		},

		formattedTariffBtnText: function (option) {
			switch (option.UF_TYPE) {
				case this.comissionTypeId:
					return "Выбрать тариф";
				default:
					const profitPersent = this.calcTariffProfit(option);
					const profitText = profitPersent >= 5 ? " с выгодой " + profitPersent + "%" : "";
					return "Купить " + option.UF_TASK_COUNT + " заявок " + profitText;
			}
		},

		formattedTariffTaskPrice: function (option) {
			switch (option.UF_TYPE) {
				case this.comissionTypeId:
					if (this.hasFirstTask) {
						return "<span class=\"afs-tariff-card__header_price\">" + this.comission / 2 + " ₽ (50% скидка) </span>";
					}
					return "<span class=\"afs-tariff-card__header_price\">" + this.comission + " ₽ (10% от заказа) </span>";
				default:
					return "цена заявки по тарифу:  <span class=\"afs-tariff-card__header_price\">" + option.UF_PRICE / option.UF_TASK_COUNT + " ₽</span>";
			}
		},

		formattedTariffTaskCount: function (option) {
			switch (option.UF_TYPE) {
				case this.comissionTypeId:
					return "Неограничено"
				default:
					return option.UF_TASK_COUNT + " заявок"
			}
		},

		formattedTariffPeriod: function (option) {
			switch (option.UF_TYPE) {
				case this.comissionTypeId:
					return "Действует для выбранной заявки"
				default:
					return "Действует " + option.UF_ACTIVE_DAYS + " дн."
			}
		},

		profitText: function (availabaleOption) {
			switch (availabaleOption.UF_TYPE) {
				case this.comissionTypeId:
					return ""
				default:
					const profitPersent = this.calcTariffProfit(availabaleOption);
					let text = profitPersent >= 5 ? "Экономия до " + profitPersent + "%" : ""
					return text
			}
		},

		calcTariffProfit: function (tariff) {
			const profitSumm = this.comission - parseInt(tariff.UF_PRICE, 10) / parseInt(tariff.UF_TASK_COUNT, 10);
			const profitPersent = (profitSumm * 100) / this.comission;
			return Math.round(profitPersent);
		}
	},

	template: `
        <div>
            <a class="btn afs-btn-outline-success btn-block mb-3" @click="clickButtonHandler()">
                Выбрать тариф принятия заявки
            </a>
            
            <transition name="slide">
               <div v-if="bottomSheetState" class="afs-bottom-sheet__panel">
                    <div class="afs-bottom-sheet__header">
                       Выберите тариф
                       <a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
                    </div>
                    
                    <div v-if="hasError" class="alert alert-danger">{{ message }}</div>
                    
                    <div class="afs-tariff-cards" :style="sheetHeight">
                    
						<!--Принятие за комиссию-->
                        <div class="afs-tariff-card" v-if="onlyPackageButtons && (workCost <= 5000 || this.comission === 0)">	
                            <div class="afs-tariff-card__header">
								<div class="afs-tariff-card__header_title">
									{{ comissionTariff.UF_NAME }} <br>
									<span>{{ formattedTariffTaskCount(comissionTariff) }}</span>
								</div>
								<div class="afs-tariff-card__header_price">{{ formattedTariffPrice(comissionTariff) }}</div>
							</div>
							<div class="afs-tariff-card__text">
								<div class="afs-text-success mb-3" style="font-size: 15px"> {{ formattedTariffPeriod(comissionTariff) }} </div>
								<div v-html="comissionTariff.UF_DESCRIPTION"></div>
								<div class="afs-text-success">{{ profitText(comissionTariff) }}</div>
								
							</div>                        					
		                    <a class="btn btn-afs afs-btn-success btn-block" style="white-space: normal" @click="buyTask()">
		                        Принять заявку за {{ hasFirstTask ? comission / 2 : comission  }} руб.
		                    </a>
						</div>
                    
						<!--Кнопки тарифов-->
                        <div class="afs-tariff-card" v-for="availabaleOption in filteredOptions" :key="availabaleOption.ID">
                            <div class="afs-tariff-card__header">
								<div class="afs-tariff-card__header_title">
									{{ availabaleOption.UF_NAME }} <br>
									<span>{{ formattedTariffTaskCount(availabaleOption) }}</span>
								</div>
								<div class="afs-tariff-card__header_price_wrap" v-html="formattedTariffTaskPrice(availabaleOption)"></div>
							</div>
							<div class="afs-tariff-card__text">
								<div class="afs-text-success mb-3" style="font-size: 15px"> {{ formattedTariffPeriod(availabaleOption) }} </div>
								<div v-html="availabaleOption.UF_DESCRIPTION"></div>
								<div class="afs-text-success">{{ profitText(availabaleOption) }}</div>
								
							</div>
							
		                    <a class="btn btn-afs afs-btn-success btn-block" style="white-space: normal" @click="clickTariffButtonHandler(availabaleOption)">
		                        {{ formattedTariffBtnText(availabaleOption) }}
		                    </a>
						</div>
                    </div>                  
                </div>
            </transition>
        </div>
    `
})