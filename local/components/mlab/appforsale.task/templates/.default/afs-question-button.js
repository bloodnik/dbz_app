Vue.component('afs-question-button', {
	name: "AfsQuestionButton",
	props: {
		sessid: {
			type: String,
			default: ""
		},
		taskId: {
			type: Number,
			default: null
		},

	},

	data: function () {
		return {
			ajaxUrl: "/local/components/mlab/appforsale.offer/ajax_1.php",
			problemSheetState: false,
			executorComment: "",
			errors: {
				commentRequired: false
			},
			sending: false
		}
	},

	methods: {
		clickButtonHandler() {
			this.problemSheetState = true
		},
		closeBtnHandler: function () {
			this.executorComment = "";
			this.problemSheetState = false;
		},

		sendTaskProblem() {
			var _this = this;

			_this.errors.commentRequired = false;
			if (_this.executorComment.length === 0) {
				_this.errors.commentRequired = true;
				return;
			}

			_this.sending = true;
			_this.hasError = false;
			_this.message = "";

			var fd = new FormData();
			fd.append('id', _this.taskId);
			fd.append('problemType', "task_question");
			fd.append('comment', _this.executorComment);
			fd.append('sessid', _this.sessid);
			fd.append('action', "taskProblem");

			try {
				axios.post(_this.ajaxUrl, fd, {
					headers: {
						'Content-Type': 'multipart/form-data'
					}
				}).then(function (response) {
					var data = response.data;

					if (data) {
						_this.problemSheetState = false;
						_this.sending = false;
						app.reload();
					} else {
						_this.hasError = true;
						_this.message = "Ошибка";
						_this.sending = false;
					}
				})
			} catch (e) {
				console.log(e);
			}
		}
	},

	template: `
        <div class="container-fluid">
            <a class="btn  afs-btn-warning btn-block btn-sm mb-3" @click="clickButtonHandler()">
                Вопрос по заявке менеджеру / Ваша цена
            </a>
            
            <transition name="slide">
               <div v-if="problemSheetState" class="afs-bottom-sheet__panel">
                    <div class="h6 text-center">
                        Вопрос заявке
                        <a class="afs-btn__close" href="javascript:void(0)" @click.prevent="closeBtnHandler()"></a>
                    </div>
                    
                    <div class="p-3">                   
                        <b-form-group label="Заполните комментарий по проблеме *"></b-form-group>
                        <textarea style="width: 100%;" v-model="executorComment" :tab-index="1" rows="4" placeholder="Введите свой вопрос"></textarea>
                        <small v-if="errors.commentRequired" class="text-danger"> Коментарий обязетелен для заполнения </small>
                    
                    
                        <p class="alert alert-info">Если у Вас вопрос по принятию заявки, напишите в чат <a href="https://wa.me/79871051800">"Сложный вопрос"</a></p>
                    
                        <div class="text-right mt-1">
                            <b-button @click="sendTaskProblem()" variant="link" :disabled="sending">
                                <b-spinner v-if="sending" small type="grow"></b-spinner>
                                Отправить
                            </b-button>
                        </div>
                                           
                    </div>
                </div>
            </transition>
        </div>
    `
})