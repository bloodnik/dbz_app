<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
global $USER;


$this->addExternalJS($this->GetFolder() . "/afs-tariff-buttons_slide.js");
$this->addExternalJS($this->GetFolder() . "/afs-question-button.js");

\Bitrix\Main\Loader::includeModule("mlab.appforsale");

$tariff         = new CAppforsaleTariff($USER->GetID());
$userTariff     = $tariff->getUserTariff();
$tariffIsActive = $tariff->isTariffActive();


//Списоок доступных тарифов, за исключением Комиссии
$availableTariffOptions = CAppforsaleTariffOption::getAvailableOptions();

//Признак наличия бесплатной заявки у пользователя
$hasFirstTask = CAppforsaleProfile::hasFirstTask();

//Статус заявки
$arStatus          = CAppforsaleTask::GetTaskStatus($arResult['VARIABLES']['ELEMENT_ID']);
$status            = $arStatus[ $arResult['VARIABLES']['ELEMENT_ID'] ]['CODE'];
$arAvailableStatus = ["N"];

//Стоимость работы по заявке
$workCost = CAppforsaleTask::GetTaskWorkCost($arResult['VARIABLES']['ELEMENT_ID']);

//Возможность принятия заявки по тарифу
$taskDisableByTariff = false;
if ($userTariff["TARIFF_COST_FROM"] >= 0 && $userTariff["TARIFF_COST_TO"] > 0) {
	if ($workCost <= $userTariff["TARIFF_COST_FROM"] || $workCost > $userTariff["TARIFF_COST_TO"]) {
		$taskDisableByTariff = true;
	}
}

$obUserReturnRating = new CAppforsaleReturnsRating($USER->GetID());
$returnsRating      = $obUserReturnRating->getReturnsRating();

?>
<div id="taskDetail">
	<?
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.detail',
		'',
		array(
			'IBLOCK_TYPE'   => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID'     => $arParams['IBLOCK_ID'],
			'CACHE_TYPE'    => $arParams['CACHE_TYPE'],
			'CACHE_TIME'    => $arParams['CACHE_TIME'],
			'ELEMENT_ID'    => $arResult['VARIABLES']['ELEMENT_ID'],
			'ELEMENT_CODE'  => $arResult['VARIABLES']['ELEMENT_CODE'],
			'SECTION_ID'    => $arResult['VARIABLES']['SECTION_ID'],
			'SECTION_CODE'  => $arResult['VARIABLES']['SECTION_CODE'],
			'FIELD_CODE'    => $arParams['DETAIL_FIELD_CODE'],
			'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
			'MESS_BTN_ADD'  => $arParams['OFFER_MESS_BTN_ADD']
		),
		$component
	);
	?>

	<div class="col-md-12">

		<div class="row">
			<?
			$bAccess = false;
			if ($USER->IsAuthorized()) {
				CModule::IncludeModule('iblock');
				$dbElement = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_ID'        => $arParams['OFFER_IBLOCK_ID'],
						'PROPERTY_TASK_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
						'CREATED_BY'       => $USER->GetID()
					)
				);
				if ($arElement = $dbElement->GetNext()) {
					?>
					<div class="col-md-12">
						<?
						if ( ! is_array($arParams['OFFER_FORM_FIELD_CODE'])) {
							$arParams['OFFER_FORM_FIELD_CODE'] = array();
						}
						foreach ($arParams['OFFER_FORM_FIELD_CODE'] as $key => $val) {
							if ( ! $val) {
								unset($arParams['OFFER_FORM_FIELD_CODE'][ $key ]);
							}
						}

						if ( ! is_array($arParams['OFFER_FORM_PROPERTY_CODE'])) {
							$arParams['OFFER_FORM_PROPERTY_CODE'] = array();
						}
						foreach ($arParams['OFFER_FORM_PROPERTY_CODE'] as $key => $val) {
							if ($val === '') {
								unset($arParams['OFFER_FORM_PROPERTY_CODE'][ $key ]);
							}
						}

						$text = CIBlockProperty::GetPropertyArray('TEXT', $arParams['OFFER_IBLOCK_ID']);
						$APPLICATION->IncludeComponent(
							'mlab:appforsale.offer',
							'',
							array(
								'IBLOCK_TYPE'   => $arParams['OFFER_IBLOCK_TYPE'],
								'IBLOCK_ID'     => $arParams['OFFER_IBLOCK_ID'],
								'TASK_ID'       => $arResult['VARIABLES']['ELEMENT_ID'],
								'FIELD_CODE'    => $arParams['OFFER_FORM_FIELD_CODE'],
								'PROPERTY_CODE' => $arParams['OFFER_FORM_PROPERTY_CODE'] ?: array($text['ORIG_ID']),
								'CACHE_TYPE'    => $arParams['CACHE_TYPE'],
								'CACHE_TIME'    => $arParams['CACHE_TIME'],
								'PROFILE_ID'    => $USER->GetID(),
								'MESS_OFFER'    => $arParams['DETAIL_MESS_YOUR_OFFER']
							),
							$component
						);
						?>
					</div>
					<?
				} else {
					$dbProps = CIBlockElement::GetProperty(
						$arParams['IBLOCK_ID'],
						$arResult['VARIABLES']['ELEMENT_ID'],
						array(),
						array(
							'CODE' => 'STATUS_ID'
						)
					);
					if ($arProps = $dbProps->Fetch()) {
						if ($arProps['VALUE'] == 'N' || $arProps['VALUE'] == '') {
							$section_id = CIBlockFindTools::GetSectionID(
								$arResult['VARIABLES']['SECTION_ID'],
								$arResult['VARIABLES']['SECTION_CODE'],
								array(
									'GLOBAL_ACTIVE' => 'Y',
									'IBLOCK_ID'     => $arParams['IBLOCK_ID']
								)
							);

							$dbNav      = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $section_id, array('ID', 'CODE'));
							$path       = '';
							$arNavChain = array();
							while ($arNav = $dbNav->GetNext()) {
								$arNavChain[] = $arNav;
// 							$path .= $arNav['CODE'].'/';
							}

							$arNavChain = array_reverse($arNavChain);

							$comission = 0;
							foreach ($arNavChain as $arNav) {
								$euf = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_' . $arParams['IBLOCK_ID'] . '_SECTION', $arNav['ID']);
								if ($comission == 0 && $euf['UF_COMISSION']['VALUE'] > 0) {
									$comission = $euf['UF_COMISSION']['VALUE'];
								}
							}

							if ($comission > 0) {
								$dbProps = CIBlockElement::GetProperty(
									$arParams['IBLOCK_ID'],
									$arResult['VARIABLES']['ELEMENT_ID'],
									array(),
									array(
										'CODE' => 'PRICE'
									)
								);
								if ($arProps = $dbProps->Fetch()) {
									if ($arProps['VALUE'] > 0) {
										$comissionText = '<br />' . AppforsaleFormatCurrency($arProps['VALUE'] / 100 * $comission) . ' (' . $comission . '% ' . GetMessage('END') . ')';
									}
								}
							}

							$bAccess = true;
							if ((intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_LENGTH_TEST', 0)) > 0 && intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_PRICE_TEST', 0)) > 0)
							    || (intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_LENGTH', 0)) > 0 && intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_PRICE', 0)) > 0)) {
								$dbRecurring = CAppforsaleRecurring::GetList(
									array(),
									array(
										'PRODUCT_ID' => '',
										'USER_ID'    => $USER->GetID(),
									)
								);
								if ($arRecurring = $dbRecurring->Fetch()) {
									if (time() > MakeTimeStamp($arRecurring['NEXT_DATE'], "DD.MM.YYYY HH:MI:SS") || $arRecurring['SUCCESS_PAYMENT'] == 'N') {
										$bAccess  = false;
										$eMessage = GetMessage('WARNING_SUBSCRIPTION_GLOBAL');
									}
								} else {
									$bAccess  = false;
									$eMessage = GetMessage('WARNING_PROFILE_GLOBAL');
								}
							} else {
								$uf         = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_' . $arParams['IBLOCK_ID'] . '_SECTION', $section_id);
								$uf_profile = $uf['UF_PROFILE'];
								$profiles   = array();
								foreach ($uf_profile['VALUE'] as $sid) {
									$dbNav = CIBlockSection::GetNavChain($uf_profile['SETTINGS']['IBLOCK_ID'], $sid, array('ID', 'CODE'));
									$path  = '';
									while ($arNav = $dbNav->GetNext()) {
										$path .= $arNav['CODE'] . '/';

										$profiles[] = $arNav['ID'];
										$price      = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_' . $uf_profile['SETTINGS']['IBLOCK_ID'] . '_SECTION', $arNav['ID']);
										if (intval($price['UF_RECUR_PRICE']['VALUE']) > 0 || intval($price['UF_RECUR_PRICE_TEST']['VALUE']) > 0) {
											$bAccess = false;
										}
									}
								}

								if ( ! $bAccess) {
									$arProfile = CIBlockElement::GetList(
										array(),
										array(
											'IBLOCK_ID'         => $uf_profile['SETTINGS']['IBLOCK_ID'],
											'ACTIVE'            => 'Y',
											'IBLOCK_SECTION_ID' => $profiles,
											'CREATED_BY'        => $USER->GetID()
										),
										array()
									);
									if ($arProfile['CNT'] > 0) {
										$arProfile = CIBlockElement::GetList(
											array(),
											array(
												'IBLOCK_ID'         => $uf_profile['SETTINGS']['IBLOCK_ID'],
												'ACTIVE'            => 'Y',
												'ACTIVE_DATE'       => 'Y',
												'IBLOCK_SECTION_ID' => $profiles,
												'CREATED_BY'        => $USER->GetID()
											),
											array()
										);
										if ($arProfile['CNT'] > 0) {
											$bAccess = true;
										} else {
											$eMessage = GetMessage('WARNING_SUBSCRIPTION');
										}
									} else {
										$eMessage = GetMessage('WARNING_PROFILE', array('#PATH#' => $path));
									}
								}
							}

							$uf             = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_' . $arParams['IBLOCK_ID'] . '_SECTION', $section_id);
							$uf_offer_price = $uf['UF_OFFER_PRICE']['VALUE'];
						}
					}
				}
			} else {
				$eMessage = GetMessage('WARNING_LOGIN');
			}
			?>

			<?

			$dbPropsFree = CIBlockElement::GetProperty(
				$arParams['IBLOCK_ID'],
				$arResult['VARIABLES']['ELEMENT_ID'],
				array(),
				array(
					'CODE' => 'COST'
				)
			);
			if ($arPropsFree = $dbPropsFree->Fetch()) {

				if ($arPropsFree['VALUE'] == 0) {
					$free = true;
				} else {
					$free = false;
				}
			}
			?>

			<? if (in_array($status, $arAvailableStatus)): ?>

				<div class="afs-fixed-bottom">
					<? if ( ! $taskDisableByTariff): ?>
						<div class="afs-buttons-group pb-2">
							<? if ($tariffIsActive): ?>
								<? if ($bAccess): ?>

									<? if ($tariff->isPackageTariff() || $tariff->isUnlimTariff()): ?>
										<? if ($workCost <= 5000 || $arPropsFree['VALUE'] == 0): ?>
											<a class="btn btn-afs afs-btn-success btn-block sendBtn" data-free="0" title="<?=$arParams['OFFER_MESS_BTN_ADD']?>">
												Принять заявку за <?=(AppforsaleFormatCurrency($arPropsFree['VALUE']/*$uf_offer_price*/)) . $comissionText?>
											</a>
										<? endif; ?>
										<!-- Взятие за тариф -->
										<a class="btn btn-afs afs-btn-success btn-block sendBtn" data-free="1" title="<?=$arParams['OFFER_MESS_BTN_ADD']?>">Принять заявку по оплаченному тарифу</a>
									<? endif; ?>

									<? if ($tariff->isComisssionTariff()): ?>
										<afs-tariff-buttons class="mt-3"
										                    :availabale-options="<?=CUtil::PhpToJSObject($availableTariffOptions)?>"
										                    backurl="<?=urlencode($APPLICATION->GetCurPage(false))?>"
										                    comission-type-id="<?=CAppforsaleTariff::COMISSION_TARIFF_ID?>"
										                    :comission="<?=$arPropsFree['VALUE'] ? $arPropsFree['VALUE'] : 0?>"
										                    :work-cost="<?=$workCost ? $workCost : 0?>"
										                    :has-first-task="<?=$hasFirstTask ? "true" : "false"?>"
										                    sessid="<?=bitrix_sessid()?>"
										                    :only-package-buttons="true"
										                    @success="onTariffSucccess"/>
									<? endif; ?>
								<? endif; ?>

							<? else: ?>

								<afs-tariff-buttons :availabale-options="<?=CUtil::PhpToJSObject($availableTariffOptions)?>"
								                    backurl="<?=urlencode($APPLICATION->GetCurPage(false))?>"
								                    comission-type-id="<?=CAppforsaleTariff::COMISSION_TARIFF_ID?>"
								                    :comission="<?=$arPropsFree['VALUE'] ? $arPropsFree['VALUE'] : 0?>"
								                    :work-cost="<?=$workCost ? $workCost : 0?>"
								                    :has-first-task="<?=$hasFirstTask ? "true" : "false"?>"
								                    sessid="<?=bitrix_sessid()?>"
								                    @success="onTariffSucccess"/>
							<? endif; ?>


							<? if ( ! empty($eMessage)): ?>
								<div class="bg-warning" style="padding: 15px;"><?=$eMessage?></div>
							<? endif; ?>
						</div>
					<? else: ?>
						<div class="afs-buttons-group">
							<!-- Взятие за комиссию -->
							<? if ($userReturnRating < 35 && ($workCost <= 5000 || $arPropsFree['VALUE'] == 0)): ?>
								<a class="btn btn-afs afs-btn-success btn-block sendBtn mb-2" data-free="0" title="<?=$arParams['OFFER_MESS_BTN_ADD']?>">
									Принять заявку за <?=(AppforsaleFormatCurrency($arPropsFree['VALUE'])) . $comissionText?>
								</a>
							<? endif; ?>

							<div class="alert alert-warning">
								Приём данной заявки по Вашему тарифу <?=$userTariff["TARIFF_NAME"]?> не доступен. В выбранном Вами тарифе доступны заявки со стоимостью
								работ <? if ($userTariff["TARIFF_COST_FROM"]): ?>от <?=$userTariff["TARIFF_COST_FROM"]?><? endif; ?> до
								<?=$userTariff["TARIFF_COST_TO"]?> рублей
							</div>
						</div>
					<? endif; ?>

					<? if ($returnsRating >= 35): ?>
						<div class="container">
							<div class="alert alert-warning">
								Ваш процент возвратов превышен. Запросить возврат комиссии по данной заявке будет невозможно. Снизить процент можно принимая заявки по пакетным тарифам либо за10% не запрашивая возврат комиссии
							</div>
						</div>
					<? endif; ?>

					<? if ( ! $taskDisableByTariff): ?>
						<afs-question-button :task-id="<?=$arResult['VARIABLES']['ELEMENT_ID']?>" sessid="<?=bitrix_sessid()?>"/>
					<? endif; ?>


				</div>
			<? endif; ?>
		</div>
		<script type="text/javascript">BX.message({'CLAIM_SUCCESS': '<?=GetMessage('CLAIM_SUCCESS')?>'});</script>
	</div>

</div>


<script>

	var taskDetail = new Vue({
		el: '#taskDetail',

		data: function () {
			return {
				userId: <?=$USER->GetID()?>,
				comissionTypeId: "<?=CAppforsaleTariff::COMISSION_TARIFF_ID?>",
				taskId: "<?=$arResult['VARIABLES']['ELEMENT_ID']?>",
				sendUrl: "/youdo/api/send.php",
				executionUrl: "/youdo/tasks-executor/<?=$arResult['VARIABLES']['SECTION_CODE_PATH'] . '/' . $arResult['VARIABLES']['ELEMENT_ID']?>/"
			}
		},

		methods: {
			onTariffSucccess: function (tariff) {
				var _this = this;

				var dataToSend = {
					task: _this.taskId,
					user: _this.userId,
					free: _this.isFree(tariff) ? 1 : 0,
				}

				try {
					axios.get(_this.sendUrl, {params: dataToSend}).then(function (response) {
						var data = response.data;

						if (data === 'executor') {
							alert('Задание принято другим исполнителем');
						} else if (data === false) {
							alert('Пополните баланс');
						} else if (data === 'no_tariff') {
							alert('Нет активного тарифа');
						} else if (data === true) {
							app.loadPageStart({url: _this.executionUrl, title: ""});
						}
					});
				} catch (e) {
					console.log(e);
				}
			},

			isFree: function isFree(tariff) {
				return tariff.UF_TYPE > this.comissionTypeId;
			}
		},


	});

	$(function () {
		$('.sendBtn').click(function (e) {

			var free = e.target.dataset.free ? e.target.dataset.free : "0";

			$.ajax({
				url: "/youdo/api/send.php",
				data: {task: <?=$arResult['VARIABLES']['ELEMENT_ID']?>, user: <?=$USER->GetID()?>, free: free},
				success: function (data) {
					if (data === 'executor') {
						alert('Задание принято другим исполнителем');
					} else if (data === 'false') {
						alert('Пополните баланс');
					} else if (data === 'no_tariff') {
						alert('Нет активного тарифа');
					} else if (data === 'true') {
						app.loadPageStart({url: "/youdo/tasks-executor/<?=$arResult['VARIABLES']['SECTION_CODE_PATH'] . '/' . $arResult['VARIABLES']['ELEMENT_ID']?>/", title: ""});
					}
				}
			});
		});
	});
</script>