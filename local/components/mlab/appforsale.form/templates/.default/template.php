<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$strMainID = $this->GetEditAreaId('');
$arItemIDs = array(
	'ID'     => $strMainID,
	'DELETE' => $strMainID . '_delete'
);
?>
<form class="afs-form form-horizontal" id="form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" onsubmit="
		this.submit.setAttribute('disabled', 'disabled');
		BX.ajax({
		url: form.action,
		data: BX.ajax.prepareForm(this).data,
		method: 'POST',
		dataType: 'json',
		onsuccess: BX.delegate(function(data) {
		if (data.response)
		{
<? if (empty($arParams['REDIRECT_URL'])): ?>
		app.onCustomEvent('OnIblock');
		app.closeController();
<? else: ?>
		app.loadPageBlank({url: '<?=$arParams['REDIRECT_URL']?>', title: ''});
<? endif; ?>
<? if ($arParams['IBLOCK_TYPE'] == 'profile'): ?>
		app.onCustomEvent('OnAfterUserLogin');
		app.reload();
<? endif; ?>
		}
		else if (data.error)
		{
		this.submit.removeAttribute('disabled');
		alert(data.error.error_msg);
		}
		}, this)
		});
		return false;
		">
	<?=bitrix_sessid_post()?>
	
	<? if (is_array($arResult['PROPERTY_LIST']) && ! empty($arResult['PROPERTY_LIST'])): ?>
		<? foreach ($arResult['PROPERTY_LIST'] as $propertyID): ?>
			<? if ( ! empty($arResult["ELEMENT"]) && $arResult["ELEMENT"]['CREATED_BY'] !== $USER->GetID()): ?>
				<? if (strpos($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'], 'EXECUTOR') !== false): ?>
					<div class="form-group">
						<label class="col-sm-4 control-label"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['NAME']?><? if (in_array($propertyID, $arResult['PROPERTY_REQUIRED'])): ?> <span class="afs-required">*</span><? endif ?></label>
						<div class="col-sm-8">
							<?
							if (intval($propertyID) > 0) {
								if (
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "T"
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] == "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "S";
								} elseif (
									(
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "S"
										||
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "N"
									)
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] > "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "T";
								}
							}

							if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y") {
								$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][ $propertyID ]) : 0;
								$inputNum += $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] : 1;
							} else {
								$inputNum = 1;
							}


							if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['USER_TYPE'] === 'Date') {
								$INPUT_TYPE = 'USER_TYPE';
							} else if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['GetPublicEditHTML']) {
								$INPUT_TYPE = 'USER_TYPE';
							} else {
								$INPUT_TYPE = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['PROPERTY_TYPE'];
							}

							switch ($INPUT_TYPE):
								case 'USER_TYPE':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value       = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["~VALUE"] : $arResult["ELEMENT"][ $propertyID ];
											$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["DESCRIPTION"] : "";
										} elseif ($i == 0) {
											$value       = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"];
											$description = "";
										} else {
											$value       = "";
											$description = "";
										}
										echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["GetPublicEditHTML"],
											array(
												$arResult["PROPERTY_LIST_FULL"][ $propertyID ],
												array(
													"VALUE"       => $value,
													"DESCRIPTION" => $description,
												),
												array(
													"VALUE"       => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][VALUE]",
													"DESCRIPTION" => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][DESCRIPTION]",
													"FORM_NAME"   => "iblock_add",
												),
											));
										?><?
									}
									break;
								case "TAGS":
									break;
								case "DATE":
									?>
									<input class="form-control datepicker-here" required name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>"/>
									<?
									break;
								case "HTML":
									break;
								case 'E':
								case 'G':
									$bDisable = false;
									if ($arParams["ID"] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'CITY_ID') {
										foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
											if (intval($arEnum['PROPERTY_RATIO_VALUE']) > 0) {
												$bDisable = true;
												break;
											}
										}
									}
									?>
									<?
									if ($propertyID == 51):?>
										<?
										$arSelect1 = Array("*");
										$arFilter1 = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $arParams['SECTION_ID'], "PROPERTY_CITY" => "");
										$res       = CIBlockElement::GetList(Array(), $arFilter1, false, Array(), $arSelect1);
										while ($ob = $res->GetNextElement()) {
											$arFields   = $ob->GetFields();
											$partners[] = $arFields['ID'];
										}
										?>
										<input type="checkbox" id="select_all_partners"/> Выбрать всех партнеров
									<? endif; ?>
									<select <?=($propertyID == 51 ? ' id="partners"' : '')?> class="form-control"
									                                                         name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? '[]" size="' . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . '" multiple="multiple' : ''?>"<?=($bDisable ? ' disabled="disabled"' : '')?>>
										<?
										if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
											<optgroup disabled hidden></optgroup>';
										<? endif; ?>
										<?
										$selectedPartners = []; // Выбранные партнеры
										foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
											$checked = false;
											if ($arParams['ID'] > 0) {
												if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
													foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
														if ($arElEnum['VALUE'] == $key) {
															if ($propertyID == 51) {
																$selectedPartners[] = $arElEnum['VALUE'];
															}
															$checked = true;
															break;
														}
													}
												}
											}
											?>
											<? if ($propertyID == 51): ?>
												<? if (in_array($key, $partners)): ?>
													<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
												<? endif; ?>
											<? else: ?>
												<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
											<? endif; ?>
											<?
										}
										?>
									</select>
									<?
									break;
								case "T":
									for ($i = 0; $i < $inputNum; $i++) {

										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["VALUE"] : $arResult["ELEMENT"][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"] : "";
										} else {
											$value = "";
										}
										?>
										<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"]?>"
										          name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
										<?
									}
									break;

								case 'S':
								case 'N':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											$value = intval($propertyID) > 0 ? $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'] : $arResult['ELEMENT'][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) <= 0 ? '' : $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['DEFAULT_VALUE'];
										} else {
											$value = "";
										}
										?>
										<input<?=($arParams['ID'] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'PRICE' ? ' disabled="disabled"' : '')?>
										class="form-control" <?=($INPUT_TYPE == 'N' ? 'type="number" pattern="[0-9]*"' : 'type="text"')?> required
										name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
									}
									break;
								case 'L':
									if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST_TYPE'] == 'C') {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'checkbox' : 'radio';
									} else {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'multiselect' : 'dropdown';
									}

									switch ($type):
										case 'checkbox':
										case 'radio':
											if (count($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM']) <= 1) {
												$type = 'checkbox';
											}

											foreach ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key => $arEnum) {
												$checked = false;
												if ($arParams['ID'] > 0) {
													if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
														foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
															if ($arElEnum['VALUE'] == $key) {
																$checked = true;
																break;
															}
														}
													}
												} else {
													if ($arEnum['DEF'] == 'Y') {
														$checked = true;
													}
												}
												?>
												<div class="<?=$type?>"><label for="property_<?=$key?>"><input type="<?=$type?>"
												                                                               name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "checkbox" ? "[" . $key . "]" : ""?>"
												                                                               value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?=$arEnum["VALUE"]?></label></div>
												<?
											}

											break;

										case "dropdown":
										case "multiselect":
											?>
											<select class="form-control"
											        name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . "\" multiple=\"multiple" : ""?>">
												<?
												if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
													<optgroup disabled hidden></optgroup>';
												<? endif; ?>
												<option value=""><?=$arParams['MESS_PROPERTY_VALUE_NA']?></option>
												<?
												if (intval($propertyID) > 0) {
													$sKey = "ELEMENT_PROPERTIES";
												} else {
													$sKey = "ELEMENT";
												}

												foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM'] as $key => $arEnum) {
													$checked = false;
													if ($arParams['ID'] > 0) {
														foreach ($arResult[ $sKey ][ $propertyID ] as $elKey => $arElEnum) {
															if ($key == $arElEnum["VALUE"]) {
																$checked = true;
																break;
															}
														}
													} else {
														if ($arEnum['DEF'] == 'Y') {
															$checked = true;
														}
													}
													?>
													<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
													<?
												}
												?>
											</select>
											<?
											break;
									endswitch;

									break;

								case 'F':

									$value = array();
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											if ( ! empty($arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'])) {
												$value[] = $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'];
											}
										}


									}
									$APPLICATION->IncludeComponent(
										'mlab:appforsale.interface.file',
										'',
										array_merge($arResult['PROPERTY_LIST_FULL'][ $propertyID ], array('VALUE' => $value)),
										false
									);
									break;
							endswitch;

							if ( ! empty($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT'])):
								?>
								<p class="help-block"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT']?></p>
							<? endif; ?>
						</div>
					</div>
				<? else: ?>
					<div style="display: none" class="form-group">
						<label class="col-sm-4 control-label"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['NAME']?><? if (in_array($propertyID, $arResult['PROPERTY_REQUIRED'])): ?> <span class="afs-required">*</span><? endif ?></label>
						<div class="col-sm-8">
							<?
							if (intval($propertyID) > 0) {
								if (
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "T"
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] == "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "S";
								} elseif (
									(
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "S"
										||
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "N"
									)
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] > "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "T";
								}
							}

							if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y") {
								$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][ $propertyID ]) : 0;
								$inputNum += $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] : 1;
							} else {
								$inputNum = 1;
							}


							if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['USER_TYPE'] === 'Date') {
								$INPUT_TYPE = 'USER_TYPE';
							} else if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['GetPublicEditHTML']) {
								$INPUT_TYPE = 'USER_TYPE';
							} else {
								$INPUT_TYPE = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['PROPERTY_TYPE'];
							}

							switch ($INPUT_TYPE):
								case 'USER_TYPE':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value       = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["~VALUE"] : $arResult["ELEMENT"][ $propertyID ];
											$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["DESCRIPTION"] : "";
										} elseif ($i == 0) {
											$value       = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"];
											$description = "";
										} else {
											$value       = "";
											$description = "";
										}
										echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["GetPublicEditHTML"],
											array(
												$arResult["PROPERTY_LIST_FULL"][ $propertyID ],
												array(
													"VALUE"       => $value,
													"DESCRIPTION" => $description,
												),
												array(
													"VALUE"       => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][VALUE]",
													"DESCRIPTION" => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][DESCRIPTION]",
													"FORM_NAME"   => "iblock_add",
												),
											));
										?><?
									}
									break;
								case "TAGS":
									break;
								case "DATE":
									?>
									<input class="form-control datepicker-here" required name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>"/>
									<?
									break;
								case "HTML":
									break;
								case 'E':
								case 'G':
									$bDisable = false;
									if ($arParams["ID"] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'CITY_ID') {
										foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
											if (intval($arEnum['PROPERTY_RATIO_VALUE']) > 0) {
												$bDisable = true;
												break;
											}
										}
									}
									?>
									<?
									if ($propertyID == 51):?>
										<?
										$arSelect1 = Array("*");
										$arFilter1 = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $arParams['SECTION_ID'], "PROPERTY_CITY" => "");
										$res       = CIBlockElement::GetList(Array(), $arFilter1, false, Array(), $arSelect1);
										while ($ob = $res->GetNextElement()) {
											$arFields   = $ob->GetFields();
											$partners[] = $arFields['ID'];
										}
										?>
										<input type="checkbox" id="select_all_partners"/> Выбрать всех партнеров
									<? endif; ?>

									<select <?=($propertyID == 51 ? ' id="partners"' : '')?> class="form-control"
									                                                         name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? '[]" size="' . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . '" multiple="multiple' : ''?>"<?=($bDisable ? ' disabled="disabled"' : '')?>>
										<?
										if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
											<optgroup disabled hidden></optgroup>';
										<? endif; ?>
										<?
										$selectedPartners = []; // Выбранные партнеры
										foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
											$checked = false;
											if ($arParams['ID'] > 0) {
												if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
													foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
														if ($arElEnum['VALUE'] == $key) {
															if ($propertyID == 51) {
																$selectedPartners[] = $arElEnum['VALUE'];
															}
															$checked = true;
															break;
														}
													}
												}
											}
											?>
											<?

											if ($propertyID == 51):?>
												<? if (in_array($key, $partners)): ?>
													<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
												<? endif; ?>
											<? else: ?>
												<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
											<? endif; ?>
											<?
										}
										?>
									</select>
									<?
									break;
								case "T":
									for ($i = 0; $i < $inputNum; $i++) {

										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["VALUE"] : $arResult["ELEMENT"][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"] : "";
										} else {
											$value = "";
										}
										?>
										<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"]?>"
										          name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
										<?
									}
									break;

								case 'S':
								case 'N':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											$value = intval($propertyID) > 0 ? $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'] : $arResult['ELEMENT'][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) <= 0 ? '' : $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['DEFAULT_VALUE'];
										} else {
											$value = "";
										}
										?>
										<input<?=($arParams['ID'] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'PRICE' ? ' disabled="disabled"' : '')?>
										class="form-control" <?=($INPUT_TYPE == 'N' ? 'type="number" pattern="[0-9]*"' : 'type="text"')?> name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"
										size="25" value="<?=$value?>" /><?
									}
									break;
								case 'L':
									if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST_TYPE'] == 'C') {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'checkbox' : 'radio';
									} else {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'multiselect' : 'dropdown';
									}

									switch ($type):
										case 'checkbox':
										case 'radio':
											if (count($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM']) <= 1) {
												$type = 'checkbox';
											}

											foreach ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key => $arEnum) {
												$checked = false;
												if ($arParams['ID'] > 0) {
													if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
														foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
															if ($arElEnum['VALUE'] == $key) {
																$checked = true;
																break;
															}
														}
													}
												} else {
													if ($arEnum['DEF'] == 'Y') {
														$checked = true;
													}
												}
												?>
												<div class="<?=$type?>"><label for="property_<?=$key?>"><input type="<?=$type?>"
												                                                               name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "checkbox" ? "[" . $key . "]" : ""?>"
												                                                               value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?=$arEnum["VALUE"]?></label></div>
												<?
											}

											break;

										case "dropdown":
										case "multiselect":
											?>
											<select class="form-control"
											        name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . "\" multiple=\"multiple" : ""?>">
												<?
												if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
													<optgroup disabled hidden></optgroup>';
												<? endif; ?>
												<option value=""><?=$arParams['MESS_PROPERTY_VALUE_NA']?></option>
												<?
												if (intval($propertyID) > 0) {
													$sKey = "ELEMENT_PROPERTIES";
												} else {
													$sKey = "ELEMENT";
												}

												foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM'] as $key => $arEnum) {
													$checked = false;
													if ($arParams['ID'] > 0) {
														foreach ($arResult[ $sKey ][ $propertyID ] as $elKey => $arElEnum) {
															if ($key == $arElEnum["VALUE"]) {
																$checked = true;
																break;
															}
														}
													} else {
														if ($arEnum['DEF'] == 'Y') {
															$checked = true;
														}
													}
													?>
													<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
													<?
												}
												?>
											</select>
											<?
											break;
									endswitch;

									break;

								case 'F':

									$value = array();
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											if ( ! empty($arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'])) {
												$value[] = $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'];
											}
										}


									}
									$APPLICATION->IncludeComponent(
										'mlab:appforsale.interface.file',
										'',
										array_merge($arResult['PROPERTY_LIST_FULL'][ $propertyID ], array('VALUE' => $value)),
										false
									);
									break;
							endswitch;

							if ( ! empty($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT'])):
								?>
								<p class="help-block"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT']?></p>
							<? endif; ?>
						</div>
					</div>
				<? endif; ?>
			<? else: ?>
				<? if (strpos($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'], 'EXECUTOR') !== false): ?>

				<? else: ?>
					<div class="form-group">
						<label class="col-sm-4 control-label"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['NAME']?><? if (in_array($propertyID, $arResult['PROPERTY_REQUIRED'])): ?> <span class="afs-required">*</span><? endif ?></label>
						<div class="col-sm-8">
							<?
							if (intval($propertyID) > 0) {
								if (
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "T"
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] == "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "S";
								} elseif (
									(
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "S"
										||
										$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] == "N"
									)
									&&
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] > "1"
								) {
									$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["PROPERTY_TYPE"] = "T";
								}
							}

							if ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE"] == "Y") {
								$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][ $propertyID ]) : 0;
								$inputNum += $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["MULTIPLE_CNT"] : 1;
							} else {
								$inputNum = 1;
							}


							if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['USER_TYPE'] === 'Date') {
								$INPUT_TYPE = 'DATE';
							} else if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['GetPublicEditHTML']) {
								$INPUT_TYPE = 'USER_TYPE';
							} else {
								$INPUT_TYPE = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['PROPERTY_TYPE'];
							}


							switch ($INPUT_TYPE):
								case 'USER_TYPE':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value       = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["~VALUE"] : $arResult["ELEMENT"][ $propertyID ];
											$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["DESCRIPTION"] : "";
										} elseif ($i == 0) {
											$value       = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"];
											$description = "";
										} else {
											$value       = "";
											$description = "";
										}
										echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["GetPublicEditHTML"],
											array(
												$arResult["PROPERTY_LIST_FULL"][ $propertyID ],
												array(
													"VALUE"       => $value,
													"DESCRIPTION" => $description,
												),
												array(
													"VALUE"       => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][VALUE]",
													"DESCRIPTION" => $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . "[" . $propertyID . "][" . $i . "][DESCRIPTION]",
													"FORM_NAME"   => "iblock_add",
												),
											));
										?><?
									}
									break;
								case "TAGS":
									break;
								case "DATE":
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value       = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["~VALUE"] : $arResult["ELEMENT"][ $propertyID ];
											$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["DESCRIPTION"] : "";
										} elseif ($i == 0) {
											$value       = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"];
											$description = "";
										} else {
											$value       = "";
											$description = "";
										} ?>

										<input class="form-control datepicker-here"
										       readonly
										       data-auto-close="true"
										       required
										       name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName'] . '[' . $propertyID . '][' . $i . '][VALUE]'?>"
										       size="25"
										       value="<?=$value?>"/>
									<?
									}
									break;
								case "HTML":
									break;
								case 'E':
								case 'G':
									$bDisable = false;
									if ($arParams["ID"] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'CITY_ID') {
										foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
											if (intval($arEnum['PROPERTY_RATIO_VALUE']) > 0) {
												$bDisable = true;
												break;
											}
										}
									}
									?>
									<?
									if ($propertyID == 51):?>
										<?
										$arSelect1 = Array("*");
										$arFilter1 = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $arParams['SECTION_ID'], "PROPERTY_CITY" => 6726);
										$res       = CIBlockElement::GetList(Array(), $arFilter1, false, Array(), $arSelect1);
										while ($ob = $res->GetNextElement()) {
											$arFields   = $ob->GetFields();
											$partners[] = $arFields['ID'];
										}
										?>
										<input type="checkbox" id="select_all_partners"/> Выбрать всех партнеров
										<input class="form-control" type="text" id="partner_search" placeholder="Поиск партнера">
									<? endif; ?>
									<select <?=($propertyID == 51 ? ' id="partners"' : '')?> <?=($propertyID == 61 ? ' id="id_city"' : '')?> class="form-control"
									                                                                                                         name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? '[]" size="' . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . '" multiple="multiple' : ''?>"<?=($bDisable ? ' disabled="disabled"' : '')?>>
										<?
										if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
											<optgroup disabled hidden></optgroup>';
										<? endif; ?>
										<?
										if ($propertyID == 46):?>
											<? foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
												$checked = false;
												if ($arParams['ID'] > 0) {
													if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
														foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
															if ($arElEnum['VALUE'] == $key) {
																$categories[] = $arElEnum['VALUE'];
																break;
															}
														}
													}
												}
											}
											$arSelect = Array('ID', 'NAME', 'DEPTH_LEVEL');
											$arFilter = Array('IBLOCK_ID' => 4, 'ACTIVE' => 'Y');
											$res      = CIBlockSection::GetList(Array('left_margin' => 'ASC'), $arFilter, true, $arSelect);
											while ($ob = $res->GetNext()):?>
												<?
												if ($ob['DEPTH_LEVEL'] == 1):?>
													<optgroup label="<?=$ob['NAME']?>"></optgroup>
												<? else: ?>
													<option value='<?=$ob['ID']?>'<?=in_array($ob['ID'], $categories) ? ' selected="selected"' : ''?>><?
														for ($i = 1; $i < $ob['DEPTH_LEVEL']; $i++) {
															echo '-';
														} ?><?=$ob['DEPTH_LEVEL'] > 1 ? ' ' . $ob['NAME'] : $ob['NAME']?></option>
												<? endif; ?>
											<? endwhile; ?>
										<? else: ?>
											<?
											$selectedPartners = []; // Выбранные партнеры
											foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST'] as $key => $arEnum) {
												$checked = false;
												if ($arParams['ID'] > 0) {
													if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
														foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
															if ($arElEnum['VALUE'] == $key) {
																if ($propertyID == 51) {
																	$selectedPartners[] = $arElEnum['VALUE'];
																}
																$checked = true;
																break;
															}
														}
													}
												}
												?>
												<?
												if ($propertyID == 51):?>
													<? if (in_array($key, $partners)): ?>
														<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
													<? endif; ?>
												<? else: ?>
													<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
												<? endif; ?>
												<?
											}
											?>
										<? endif; ?>
									</select>
									<?
									break;
								case "T":
									for ($i = 0; $i < $inputNum; $i++) {

										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][ $propertyID ][ $i ]["VALUE"] : $arResult["ELEMENT"][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["DEFAULT_VALUE"] : "";
										} else {
											$value = "";
										}
										?>
										<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"]?>"
										          name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
										<?
									}
									break;

								case 'S':
								case 'N':
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											$value = intval($propertyID) > 0 ? $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'] : $arResult['ELEMENT'][ $propertyID ];
										} elseif ($i == 0) {
											$value = intval($propertyID) <= 0 ? '' : $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['DEFAULT_VALUE'];
										} else {
											$value = "";
										}
										?>
										<input<?=($arParams['ID'] > 0 && $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['CODE'] == 'PRICE' ? ' disabled="disabled"' : '')?>
										class="form-control" <?=($INPUT_TYPE == 'N' ? 'type="number" pattern="[0-9]*"' : 'type="text"')?> name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"
										size="25" value="<?=$value?>" /><?
									}
									break;
								case 'L':
									if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['LIST_TYPE'] == 'C') {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'checkbox' : 'radio';
									} else {
										$type = $arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' ? 'multiselect' : 'dropdown';
									}

									switch ($type):
										case 'checkbox':
										case 'radio':
											if (count($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM']) <= 1) {
												$type = 'checkbox';
											}

											foreach ($arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ENUM"] as $key => $arEnum) {
												$checked = false;
												if ($arParams['ID'] > 0) {
													if (is_array($arResult['ELEMENT_PROPERTIES'][ $propertyID ])) {
														foreach ($arResult['ELEMENT_PROPERTIES'][ $propertyID ] as $arElEnum) {
															if ($arElEnum['VALUE'] == $key) {
																$checked = true;
																break;
															}
														}
													}
												} else {
													if ($arEnum['DEF'] == 'Y') {
														$checked = true;
													}
												}
												?>
												<div class="<?=$type?>"><label for="property_<?=$key?>"><input type="<?=$type?>"
												                                                               name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "checkbox" ? "[" . $key . "]" : ""?>"
												                                                               value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?=$arEnum["VALUE"]?></label></div>
												<?
											}

											break;

										case "dropdown":
										case "multiselect":
											?>
											<select class="form-control"
											        name="<?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][ $propertyID ]["ROW_COUNT"] . "\" multiple=\"multiple" : ""?>">
												<?
												if ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
													<optgroup disabled hidden></optgroup>';
												<? endif; ?>
												<option value=""><?=$arParams['MESS_PROPERTY_VALUE_NA']?></option>
												<?
												if (intval($propertyID) > 0) {
													$sKey = "ELEMENT_PROPERTIES";
												} else {
													$sKey = "ELEMENT";
												}

												foreach ($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['ENUM'] as $key => $arEnum) {
													$checked = false;
													if ($arParams['ID'] > 0) {
														foreach ($arResult[ $sKey ][ $propertyID ] as $elKey => $arElEnum) {
															if ($key == $arElEnum["VALUE"]) {
																$checked = true;
																break;
															}
														}
													} else {
														if ($arEnum['DEF'] == 'Y') {
															$checked = true;
														}
													}
													?>
													<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
													<?
												}
												?>
											</select>
											<?
											break;
									endswitch;

									break;

								case 'F':

									$value = array();
									for ($i = 0; $i < $inputNum; $i++) {
										if ($arParams['ID'] > 0) {
											if ( ! empty($arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'])) {
												$value[] = $arResult['ELEMENT_PROPERTIES'][ $propertyID ][ $i ]['VALUE'];
											}
										}


									}
									$APPLICATION->IncludeComponent(
										'mlab:appforsale.interface.file',
										'',
										array_merge($arResult['PROPERTY_LIST_FULL'][ $propertyID ], array('VALUE' => $value)),
										false
									);
									break;
							endswitch;

							if ( ! empty($arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT'])):
								?>
								<p class="help-block"><?=$arResult['PROPERTY_LIST_FULL'][ $propertyID ]['HINT']?></p>
							<? endif; ?>
						</div>
					</div>
				<? endif; ?>
			<? endif; ?>
		<? endforeach; ?>
	<? endif; ?>
	<input class="btn btn-primary" type="submit" name="submit" value="<?=$arParams['MESS_BTN_ADD']?>"/> <? if ($arParams['ID'] > 0 && $arResult["ELEMENT"]['CREATED_BY'] == $USER->GetID() && $arParams['IBLOCK_TYPE'] !== 'profile'): ?><span
		class="btn btn-danger" id="<?=$arItemIDs['DELETE']?>"><?=$arParams['MESS_BTN_DELETE']?></span>
		<?
		$arJSParams = array(
			'ID'      => $arParams['ID'],
			'ajaxUrl' => CUtil::JSEscape($component->getPath() . '/delete.php'),
			'VISUAL'  => array(
				'ID'     => $arItemIDs['ID'],
				'DELETE' => $arItemIDs['DELETE']
			)
		);
		echo '<script type="text/javascript">new JCForm(' . CUtil::PhpToJSObject($arJSParams, false, true) . ');</script>';
		?>
	<? endif; ?>
</form>

<script>
    //Список партнеров
    let partners = [];
    let filteredPartners = [];

    $(function () {
        getPartners();

        $('#partner_search').on('keyup', function () {
            let searchString = $(this).val();
            filteredPartners = partners.filter(partner => partner.NAME.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
            fillPartnersSelect();
        });

        // Собыите смены города.
        // Отправляется запрос на получение партнеров с привязкой к городу
        $('#id_city').change(function (value) {
            getPartners($(this).val());
        });

        function fillPartnersSelect() {
            $('#partners').empty();
            var selectedPartners = JSON.parse('<?=json_encode($selectedPartners)?>');
            filteredPartners.forEach(function (value) {
                var selected = '';
                if (selectedPartners.includes(value.ID)) {
                    selected = 'selected="selected"';
                }
                $('#partners').append('<option ' + selected + ' value="' + value.ID + '">' + value.NAME + '</option>');
            });
        }

        function getPartners(cityId) {
            $.get('/youdo/api/get_data.php', {type: 'partners', sectionId: '<?=$arParams['SECTION_ID']?>', cityId: cityId || 6726}, function (response) {
                $('#partners').empty();
                const data = JSON.parse(response);
                partners = filteredPartners = data;
                fillPartnersSelect();
            })
        }


        function urlico() {
            if ($('#property_28').is(':checked')) {
                $('[name="PROPERTY[48][0]"]').parent().parent().show();
                $('[name="PROPERTY[49][0]"]').parent().parent().show();
                $('[name="PROPERTY[50][0]"]').parent().parent().show();
            }
            if ($('#property_29').is(':checked')) {
                $('[name="PROPERTY[48][0]"]').parent().parent().hide();
                $('[name="PROPERTY[49][0]"]').parent().parent().hide();
                $('[name="PROPERTY[50][0]"]').parent().parent().hide();
            }
        }

        setTimeout(urlico, 100);

        $("#property_28").click(function () {
            urlico();
        });

        $("#property_29").click(function () {
            urlico();
        });

        $('#select_all_partners').click(function () {
            if ($('#select_all_partners').is(':checked')) {
                $('#partners option').prop('selected', true);
            }
            else {
                $('#partners option').prop('selected', false);
            }
        });

    });
</script>