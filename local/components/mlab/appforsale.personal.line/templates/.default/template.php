<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
?>
<div class="afs-personal-line">
<?if ($USER->IsAuthorized()):?>
	<div class="dropdown">
		<a class="afs-personal" data-toggle="dropdown" href="#" onclick="BX.PreventDefault(e)"><img src="<?=$arResult['USER']['PERSONAL_PHOTO']['src'] ?: $templateFolder.'/images/user_no_photo.png'?>" /><span><?=$arResult['USER']['FORMAT_NAME']?> <b class="caret"></b></span></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="/youdo/personal/"><?=Loc::getMessage('PROFILE')?></a></li>
			<?if (!CSite::InGroup([5])):?>
				<li role="presentation"><a role="menuitem" tabindex="0" href="/youdo/subscription/"><?=Loc::getMessage('SERVICE')?></a></li>
			<?endif;?>
		<li role="presentation"><a role="menuitem" tabindex="1" href="/youdo/notification/"><?=Loc::getMessage('NOTIFICATION')?></a></li>
		
			<li class="divider"></li>
			<li role="presentation"><a role="menuitem" tabindex="2" href="/?exit=Y" style="color: red"><?=Loc::getMessage('EXIT')?></a></li>
		</ul>
	</div>
<?else:?>
	<a class="afs-login" href="/youdo/login/"><span><?=Loc::getMessage('REGISTRATION')?></span></a>
<?endif;?>
</div>