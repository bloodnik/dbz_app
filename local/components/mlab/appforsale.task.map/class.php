<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
	
class TaskMap extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		
		CModule::IncludeModule('iblock');
		
		$arFilter = array(
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			'!PROPERTY_ADDRESS' => false
		);
		
		if ($USER->IsAuthorized())
		{
			if ($this->arParams['OWNER'] == 'N')
				$arFilter['!CREATED_BY'] = $USER->GetID();
			
			$arFilter['PROPERTY_STATUS_ID'] = array(false, 'N');
			
			$arProperty = CIBlockProperty::GetPropertyArray('CITY_ID', $this->arParams['IBLOCK_ID']);
			if ($arProperty)
			{
				$UF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', $USER->GetID());
				$city_id = intval($UF['UF_CITY']['VALUE']);
				if ($city_id > 0)
				{
					if (!empty($_SESSION['city_id']))
					{
						$city_id = $_SESSION['city_id'];
					}
						
					if (!empty($_GET['city_id']))
					{
						$city_id = $_GET['city_id'];
					}
						
					$this->arParams['CITY_ID'] = $city_id;
					$arFilter['PROPERTY_CITY_ID'] = $city_id;
				}
			}
			$arSelect1 = Array("*");
			$arFilter1 = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "CREATED_BY"=>$USER->GetID());
			$res = CIBlockElement::GetList(Array(), $arFilter1, false, Array(), $arSelect1);
			while($ob = $res->GetNextElement())
			{
				 $arFields = $ob->GetFields();
				 $partners[] = $arFields['ID'];
			}
			if (empty($partners))
				$partners = 0;
			
			$arFilter['PROPERTY_PARTNERS'] = $partners;			
		}
		else
		{
			$arFilter['!PROPERTY_STATUS_ID'] = 'F';
		}
		
		if (!empty($_GET['sections']))
		{
			$arFilter['SECTION_ID'] = explode(',', $_GET['sections']);
			$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		}
		
		$dbElement = CIBlockElement::GetList(
			array(),
			$arFilter,
			false,
			false,
			array(
				'ID',
				'IBLOCK_ID',
				'NAME',
					'CREATED_BY',
				'DETAIL_PAGE_URL',
				'PROPERTY_*'
			)
		);
		$dbElement->SetUrlTemplates($this->arParams['DETAIL_URL']);
		$this->arResult['OBJECTS'] = array();
		while($obElement = $dbElement->GetNextElement())
		{
			$arElement = $obElement->GetFields();
			$arElement['PROPERTIES'] = $obElement->GetProperties();


			if ($USER->IsAuthorized() && $this->arParams['OWNER'] == 'Y')
			{
				if ($arElement['CREATED_BY'] == $USER->GetID())
				{
					$arElement['DETAIL_PAGE_URL'] = str_replace('/youdo/', '/youdo/tasks-customer/', $arElement['DETAIL_PAGE_URL']);
				}
			}
			
			if (!is_array($arElement['PROPERTIES']['ADDRESS']['VALUE']))
				$arElement['PROPERTIES']['ADDRESS']['VALUE'] = array($arElement['PROPERTIES']['ADDRESS']['VALUE']);
			
			$arAddress = explode('|', $arElement['PROPERTIES']['ADDRESS']['VALUE'][0]);
			$this->arResult['OBJECTS'][] = array(
				explode(',', $arAddress[1]),
				array(
			//		'clusterCaption' => $arElement['NAME'],
					'balloonContent' => '<p>'.$arElement['NAME'] .'</p><p><a class="btn btn-info btn-xs" title="'.$arElement['NAME'] .'" href="'.$arElement['DETAIL_PAGE_URL'].'">'.Loc::getMessage("DETAIL_PAGE_URL").'</a></p>',
				)
			);
		}

		$this->IncludeComponentTemplate();
	}
}
?>