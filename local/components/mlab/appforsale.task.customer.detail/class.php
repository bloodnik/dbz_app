<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskCustomerDetail extends \Mlab\Appforsale\Component\Element
{
	const TASK_VISIBLE_GROUPS = [1, 5]; //Группы видящие все задания

	protected function getFilter()
	{
		global $USER;
		$arUserGroups = CUser::GetUserGroup($USER->GetID());

		$arFilter = parent::getFilter();
		unset($arFilter['ACTIVE']);

		// START/SIRIL 03.02.2019 - Фильтр по городу пользователя
		if (count(array_intersect(self::TASK_VISIBLE_GROUPS, $arUserGroups)) === 0) {
			$arFilter['CREATED_BY'] = $USER->GetID();
		}
		// END/SIRIL 03.02.2019 - Фильтр по городу пользователя

		return $arFilter;
	}
}
?>