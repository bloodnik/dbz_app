<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class SubscriptionList extends \Mlab\Appforsale\Component\ElementList
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams = parent::onPrepareComponentParams($arParams);
	
		$arParams['MESS_BTN_RESTORE'] = $arParams['MESS_BTN_RESTORE'] ?: Loc::getMessage('BTN_RESTORE');
		$arParams['MESS_BTN_CANCEL'] = $arParams['MESS_BTN_CANCEL'] ?: Loc::getMessage('BTN_CANCEL');

		return $arParams;
	}
	
	protected function getSelect()
	{
		$arSelect = parent::getSelect();
		$arSelect[] = 'ACTIVE_TO';
		$arSelect[] = 'ACTIVE';
		return $arSelect;
	}
	
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		$arFilter['CREATED_BY'] = $USER->GetID();
		unset($arFilter['ACTIVE_DATE']);
		unset($arFilter['ACTIVE']);
		return $arFilter;
	}
	
	protected function getCacheKeys()
	{
		global $USER;
		$resultCacheKeys = parent::getCacheKeys();
		
		$dbRecurring = CAppforsaleRecurring::GetList(
			array(), 
			array(
			//	'CANCELED' => 'N',
				'PRODUCT_ID' => $this->arResult['ELEMENTS'],
				'USER_ID' => $USER->GetID()
			)
		);
		while ($arRecurring = $dbRecurring->Fetch())
		{
			$this->arResult['RECURRING'][$arRecurring['PRODUCT_ID']] = $arRecurring;
		}
		
		$sid = array();
		foreach ($this->arResult['ITEMS'] as &$arItem)
		{
		    if ($arItem["NAME"] == "unknown")
		        $arItem["NAME"] = "&nbsp;";
			$sid[] = $arItem['IBLOCK_SECTION_ID'];
		}
		
		$rsSection = CIBlockSection::GetList(
				array(),
				array(
						"IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
						"ID" => $sid
				),
				true,
				array(
						"ID",
						"NAME",
						"UF_FORMAT_NAME",
						"UF_RECUR_TYPE_TEST",
						"UF_RECUR_LENGTH_TEST",
						"UF_RECUR_PRICE_TEST",
						"UF_RECUR_TYPE",
						"UF_RECUR_LENGTH",
						"UF_RECUR_PRICE",
						"UF_COUNT"
				)
		);
		
		while ($arSection = $rsSection->GetNext())
		{
			// HTMLToTxt
			$this->arResult['SECTIONS'][$arSection['ID']] = CAppforsaleRecurring::ToString($arSection);
		}
		
		return $resultCacheKeys;
	}
}
?>