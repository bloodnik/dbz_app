<?

use Bitrix\Main\Localization\Loc;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

Loc::loadMessages(__FILE__);

$arUser   = $arResult['arUser'];
$arGroups = CUser::GetUserGroup($arUser['ID']);

?>

<div class="row afs-executor-count">
	<div class="col-xs-6"><?=$arResult['LAST_ACTIVITY_DATE']?></div>
	<div class="col-xs-6 text-right"><?=$arResult['SHOW_COUNTER']?> <?=CAppforsaleRecurring::pluralForm($arResult['SHOW_COUNTER'], Loc::getMessage('EXECUTOR_SHOW_COUNTER1'), Loc::getMessage('EXECUTOR_SHOW_COUNTER2'), Loc::getMessage('EXECUTOR_SHOW_COUNTER5'))?></div>
</div>

<div class="row afs-executor">
	<div class="afs-executor-image">
		<img class="afs-executor-img" src="<?=($arUser['PERSONAL_PHOTO'] ? $arUser['PERSONAL_PHOTO']['src'] : $templateFolder . '/images/no_photo.png')?>"/>
	</div>
	<div class="afs-executor-info">
		<div class="afs-executor-name">
			<? if ($arResult['IS_MANAGER']): ?>
				Менеджер
			<? else: ?>
				<?=$item['FORMAT_NAME']?>
			<? endif; ?>
		</div>
		<div class="afs-executor-desc"><?=Loc::getMessage('EXECUTOR_DONE')?> <?=$arResult['DONE']?> <?=CAppforsaleRecurring::pluralForm($arResult['DONE'], Loc::getMessage('EXECUTOR_TASK1'), Loc::getMessage('EXECUTOR_TASK2'), Loc::getMessage('EXECUTOR_TASK5'))?>
			, <?=Loc::getMessage('EXECUTOR_CREATE')?> <?=$arResult['CREATE']?> <?=CAppforsaleRecurring::pluralForm($arResult['CREATE'], Loc::getMessage('EXECUTOR_TASK1'), Loc::getMessage('EXECUTOR_TASK2'), Loc::getMessage('EXECUTOR_TASK5'))?></div>
		<div>
			<?
			$APPLICATION->IncludeComponent(
				'bitrix:system.field.edit',
				'rating',
				array(
					'arUserField' => $arUser['UF_RATING']
				),
				null,
				array('HIDE_ICONS' => 'Y')
			);
			?>
		</div>
		<div style="margin-top: 6px;">
			<? if ($arUser['PERSONAL_MOBILE'] == '+7 (XXX) XXX-XX-XX'): ?>
				<span class="btn btn-primary btn-sm disabled" onclick="alert('<?=Loc::getMessage('CALL_DISABLED')?>')"><?=$arUser['PERSONAL_MOBILE']?></span>
				<? if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/youdo/im/')): ?>
					<span class="btn btn-primary btn-sm disabled" onclick="alert('<?=Loc::getMessage('SEND_DISABLED')?>')"><?=Loc::getMessage('SEND')?></span>
				<? endif; ?>
			<? else: ?>
				<? if ($arResult['IS_MANAGER']): //пользователь состоит в группе менеджер?>
					<a href="tel:+79871051800" class="btn btn-primary btn-sm">+7(987)105-18-00</a>
				<? else: ?>
					<a href="tel:+<?=$arUser['PERSONAL_MOBILE']?>" class="btn btn-primary btn-sm"><?=$arUser['PERSONAL_MOBILE']?></a>
					<? if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/youdo/im/')): ?>
						<a class="btn btn-primary btn-sm" href="/youdo/im/?sel=<?=$arResult['arUser']['ID']?>"><?=Loc::getMessage('SEND')?></a>
					<? endif; ?>
				<? endif; ?>

			<? endif; ?>
		</div>

		<? /*if ($USER->IsAuthorized() && $arUser['ID'] != $USER->GetID()):?>
				<div style="margin-top: 6px;">
					<div><a class="btn btn-warning btn-sm" onclick="showBox('<?=$templateFolder?>/tasks.php?USER_ID=<?=$arUser['ID']?>');"><?=Loc::getMessage('OFFER_BUTTON')?></a></div>
					<div style="color: #eea236; padding: 5px 0px; font-size: 12px; line-height: 1.5;"><?=Loc::getMessage('OFFER_INFO')?></div>
				</div>
				<?endif;*/ ?>
	</div>
</div>