<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if ($USER->IsAuthorized() && CModule::IncludeModule('iblock'))
{
	$context = Bitrix\Main\Application::getInstance()->getContext();
	$request = $context->getRequest();
	$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);
	
	$USER_ID = $request->get('USER_ID');
	$TASK_ID = $request->get('TASK_ID');
	
	if ($USER_ID > 0 && $TASK_ID > 0)
	{
		$dbElement = CIBlockElement::GetByID($TASK_ID);
		$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
		if($arElement = $dbElement->GetNext())
		{
			if ($arElement['CREATED_BY'] == $USER->GetID())
			{
				global $push_id;
				$push_id = 1;
				CModule::IncludeModule('mlab.appforsale');
				CAppforsale::SendMessage(
					array($USER_ID),
					array(
						'body' => Loc::getMessage('NOTIFICATION_TASK_BODY', array('#NAME#' => $arElement['NAME'])),
						'icon' => 'ic_stat_name',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => 'http://'.$_SERVER['SERVER_NAME'].'/youdo/'.$arElement['DETAIL_PAGE_URL'],
						'id' => $arElement['ID']
					)
				);
			}
		}
	}
	else
	{
		$dbElement = CIBlockElement::GetList(
			array(
				'TIMESTAMP_X' => 'DESC'
			),
			array(
				'IBLOCK_CODE' => 'task',
				'ACTIVE' => 'Y',
				'CREATED_BY' => $USER->GetID(),
				'!PROPERTY_STATUS_ID' => 'F'
			),
			false,
			false,
			array(
				'ID',
				'IBLOCK_ID',
				'IBLOCK_SECTION_ID',
				'NAME'
			)
		);
		echo '<div>';
		$cnt = 0;
		while($arElement = $dbElement->GetNext())
		{
			$dbSection = CIBlockSection::GetNavChain($arElement['IBLOCK_ID'], $arElement['IBLOCK_SECTION_ID']);
			if ($arSection = $dbSection->Fetch())
			{
				$arElement['IBLOCK_SECTION'] = $arSection;
				echo '<div onclick="BX.ajax({url: \'/bitrix/components/mlab/appforsale.executor.detail/templates/.default/tasks.php\', method: \'POST\', data: { TASK_ID: '.$arElement['ID'].', USER_ID: '.$USER_ID.'}, onsuccess: function() {alert(\''.Loc::getMessage('SUCCESS').'\');}}); __bq.hideAll(true)" style="padding: 8px 15px 8px 15px; border-bottom: 1px solid #f3f3f3; min-height: 56px; background-color: #ffffff"><div style="font-size: 16px; line-height: 24px; color: rgba(0, 0, 0, .87)">'.$arElement['NAME'].'</div><div style="font-size: 14px; line-height: 20px; color: rgba(0, 0, 0, .54)">'.$arElement['IBLOCK_SECTION']['NAME'].'</div></div>';
				$cnt++;
			}
		}
		
		if ($cnt == 0)
		{
			echo "<div style=\"padding: 16px;\">".Loc::getMessage("ERROR")."</div>";
		}
		
		echo '</div>';
	}
}
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>