<?
$MESS['LIST_SETTINGS'] = 'ParamГЁtres de la liste';
$MESS['DETAIL_SETTINGS'] = 'Configuration dГ©taillГ©e de la navigation';
$MESS['OFFER_SETTINGS'] = 'ParamГЁtres d\'offres';
$MESS['OFFER_FORM_SETTINGS'] = 'Ajouter une clause';
$MESS['FILTER_SETTINGS'] = 'ParamГЁtres du filtre';
$MESS['SORT_SETTINGS'] = 'ParamГЁtres de tri';
$MESS['MAP_SETTINGS'] = 'Configuration de la carte';

$MESS['IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР°';
$MESS['IBLOCK'] = 'un jeu de donnГ©es';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР° offres';
$MESS['OFFER_IBLOCK'] = 'un jeu de donnГ©es d\'offres';
$MESS['SEF_MODE_LIST'] = 'Liste des tГўches';
$MESS['SEF_MODE_DETAIL'] = 'DГ©tail';
$MESS['SEF_MODE_OFFER'] = 'Proposition';
$MESS['LIST_FIELD_CODE'] = 'Champs';
$MESS['LIST_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['DETAIL_FIELD_CODE'] = 'Champs';
$MESS['DETAIL_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['OFFER_FIELD_CODE'] = 'le Champ de la proposition';
$MESS['OFFER_PROPERTY_CODE'] = 'PropriГ©tГ©s de la proposition';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР° offres';
$MESS['OFFER_IBLOCK'] = 'un jeu de donnГ©es d\'offres';
$MESS['OFFER_MESS_BTN_ADD'] = 'le Texte du bouton "Envoyer l\'offre"';
$MESS['OFFER_MESS_BTN_ADD_DEFAULT'] = 'Envoyer une proposition';
$MESS['OFFER_FORM_FIELD_CODE'] = 'Champs';
$MESS['OFFER_FORM_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['OFFER_FORM_PROPERTY_CODE_REQUIRED'] = 'PropriГ©tГ©s obligatoires';
$MESS['OFFER_FORM_MESS_BTN_ADD'] = 'le Texte du bouton "Ajouter une annonce"';
$MESS['OFFER_FORM_MESS_BTN_ADD_DEFAULT'] = 'Ajouter une clause';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA'] = 'Inscription vide propriГ©tГ©s';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(non dГ©fini)';

$MESS['ELEMENT_SORT_FIELD'] = 'le Premier champ de tri';
$MESS['ELEMENT_SORT_ORDER'] = 'la Direction du premier tri';
$MESS['ELEMENT_SORT_FIELD2'] = 'le DeuxiГЁme champ de tri';
$MESS['ELEMENT_SORT_ORDER2'] = 'la Direction de la deuxiГЁme tri';
$MESS['SORT_ASC'] = 'croissant';
$MESS['SORT_DESC'] = 'desc';

$MESS['DETAIL_MESS_CUSTOMER'] = 'le Texte "le Client de cette tГўche"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'le Client de cette tГўche';
$MESS['DETAIL_MESS_CLAIM'] = 'le Texte de "l\'Arnaque sur le travail"';
$MESS['DETAIL_MESS_CLAIM_DEFAULT'] = 'Arnaque sur le travail';
$MESS['DETAIL_MESS_YOUR_OFFER'] = 'le Texte "Votre offre"';
$MESS['DETAIL_MESS_YOUR_OFFER_DEFAULT'] = 'Votre offre';


$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Id de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'le code de CaractГЁre de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Chemin de caractГЁres des codes de la section';

$MESS['USE_FILTER'] = 'Utiliser le filtre';
$MESS['FILTER_SORT_FIELD'] = 'le Premier champ de tri';
$MESS['FILTER_SORT_ORDER'] = 'la Direction du premier tri';
$MESS['FILTER_SORT_FIELD2'] = 'le DeuxiГЁme champ de tri';
$MESS['FILTER_SORT_ORDER2'] = 'la Direction de la deuxiГЁme tri';

$MESS['USE_SORT'] = 'tri';
$MESS['SORT_FIELD'] = 'Champ de tri';

$MESS['USE_MAP'] = 'Utiliser la carte';

$MESS['OWNER'] = 'Afficher les tГўches de l\'utilisateur';
$MESS['FILTER_SETTINGS_CNT'] = 'Afficher le nombre dans СЃРєРѕР±СЂРєР°С…';
?>