<?
$MESS['LIST_SETTINGS'] = 'Настройки списка';
$MESS['DETAIL_SETTINGS'] = 'Настройки детального просмотра';
$MESS['OFFER_SETTINGS'] = 'Настройки предложений';
$MESS['OFFER_FORM_SETTINGS'] = 'Добавить предложение';
$MESS['FILTER_SETTINGS'] = 'Настройки фильтра';
$MESS['SORT_SETTINGS'] = 'Настройки сортировки';
$MESS['MAP_SETTINGS'] = 'Настройки карты';

$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['OFFER_IBLOCK_TYPE'] = 'Тип инфоблока предложений';
$MESS['OFFER_IBLOCK'] = 'Инфоблок предложений';
$MESS['SEF_MODE_LIST'] = 'Список заданий';
$MESS['SEF_MODE_DETAIL'] = 'Детально';
$MESS['SEF_MODE_OFFER'] = 'Предложение';
$MESS['LIST_FIELD_CODE'] = 'Поля';
$MESS['LIST_PROPERTY_CODE'] = 'Свойства';
$MESS['DETAIL_FIELD_CODE'] = 'Поля';
$MESS['DETAIL_PROPERTY_CODE'] = 'Свойства';
$MESS['OFFER_FIELD_CODE'] = 'Поля предложения';
$MESS['OFFER_PROPERTY_CODE'] = 'Свойства предложения';
$MESS['OFFER_IBLOCK_TYPE'] = 'Тип инфоблока предложений';
$MESS['OFFER_IBLOCK'] = 'Инфоблок предложений';
$MESS['OFFER_MESS_BTN_ADD'] = 'Текст кнопки "Отправить предложение"';
$MESS['OFFER_MESS_BTN_ADD_DEFAULT'] = 'Отправить предложение';
$MESS['OFFER_FORM_FIELD_CODE'] = 'Поля';
$MESS['OFFER_FORM_PROPERTY_CODE'] = 'Свойства';
$MESS['OFFER_FORM_PROPERTY_CODE_REQUIRED'] = 'Свойства обязательные для заполнения';
$MESS['OFFER_FORM_MESS_BTN_ADD'] = 'Текст кнопки "Добавить предложение"';
$MESS['OFFER_FORM_MESS_BTN_ADD_DEFAULT'] = 'Добавить предложение';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA'] = 'Надпись незаполненного свойства';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(не установлено)';

$MESS['ELEMENT_SORT_FIELD'] = 'Первое поле для сортировки';
$MESS['ELEMENT_SORT_ORDER'] = 'Направление первой сортировки';
$MESS['ELEMENT_SORT_FIELD2'] = 'Второе поле для сортировки';
$MESS['ELEMENT_SORT_ORDER2'] = 'Направление второй сортировки';
$MESS['SORT_ASC'] = 'по возрастанию';
$MESS['SORT_DESC'] = 'по убыванию';

$MESS['DETAIL_MESS_CUSTOMER'] = 'Текст "Заказчик этого задания"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'Заказчик этого задания';
$MESS['DETAIL_MESS_CLAIM'] = 'Текст "Пожаловаться на задание"';
$MESS['DETAIL_MESS_CLAIM_DEFAULT'] = 'Пожаловаться на задание';
$MESS['DETAIL_MESS_YOUR_OFFER'] = 'Текст "Ваше предложение"';
$MESS['DETAIL_MESS_YOUR_OFFER_DEFAULT'] = 'Ваше предложение';


$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Идентификатор раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Символьный код раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Путь из символьных кодов раздела';

$MESS['USE_FILTER'] = 'Использовать фильтр';
$MESS['FILTER_SORT_FIELD'] = 'Первое поле для сортировки';
$MESS['FILTER_SORT_ORDER'] = 'Направление первой сортировки';
$MESS['FILTER_SORT_FIELD2'] = 'Второе поле для сортировки';
$MESS['FILTER_SORT_ORDER2'] = 'Направление второй сортировки';

$MESS['USE_SORT'] = 'Использовать сортировку';
$MESS['SORT_FIELD'] = 'Поля для сортировки';

$MESS['USE_MAP'] = 'Использовать карту';

$MESS['OWNER'] = 'Отображать задания пользователя';
$MESS['FILTER_SETTINGS_CNT'] = 'Отображать количество в скобрках';
?>