<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$status_id = CIBlockFormatProperties::GetDisplayValue(null, $arResult['PROPERTIES']['STATUS_ID'], "");

$badgeColor = "";
switch ($status_id["VALUE"]) {
	case "N":
		$badgeColor = "afs-badge-success";
		break;
	case "P":
		$badgeColor = "afs-badge-warning";
		break;
	case "D":
		$badgeColor = "afs-badge-info";
		break;
	case "F":
		$badgeColor = "afs-badge-danger";
		break;
}
$createdDate = ! empty($arResult["PROPERTIES"]["DEAL_CREATED_DATE"]["VALUE"]) ? $arResult["PROPERTIES"]["DEAL_CREATED_DATE"]["VALUE"] : $arResult["DATE_CREATE"]

?>
<div class="afs-task-detail">
	<? if ( ! empty($arResult['DISPLAY_PROPERTIES'])): ?>
		<div class="top-line">
			<div class="flex-row no-gutters py-3">
				<div class="col-xs-3">
					<span class="afs-badge <?=$badgeColor?>"><?=$status_id['DISPLAY_VALUE']?></span>
				</div>
				<div class="col-xs-9 text-right">
					<span class="text">#SHOW_COUNTER# <?=GetMessage('SHOW_COUNTER')?> | <?=GetMessage('CREATE')?> <?=$createdDate?></span>
				</div>
			</div>

			<div class="afs-list-divider"></div>
		</div>

		<div class="task-info row">
			<div class="col-12">
				<?=$arResult['IBLOCK_SECTION']['NAME']?> | <?=GetMessage('TASK')?><?=$arResult['ID']?>
			</div>
		</div>

		<div class="afs-task-customer-detail-rows">
			<div class="row  d-none d-sm-block d-sm-none d-md-block">
				<div class="col-md-12">
					<h1 style="font-size: 26px"><?=$arResult['NAME']?></h1>
				</div>
			</div>
			<? foreach ($arResult['DISPLAY_PROPERTIES'] as $property): ?>
				<? if ($property['CODE'] === 'FROM_CRM') {
					continue;
				} ?>
				<? if ($property['CODE'] == 'NAME' || $property['CODE'] == 'DESCRIPTION' || $property['CODE'] == 'PHOTO' || $property['CODE'] == 'ADDRESS'): ?>
					<div class="afs-task-customer-detail-row row">
						<div class="col-md-3"><b><?=$property['NAME']?></b></div>
						<div class="col-md-9">
							<?=(
							is_array($property['DISPLAY_VALUE'])
								? implode(' / ', $property['DISPLAY_VALUE'])
								: $property['DISPLAY_VALUE']
							)?>
						</div>
					</div>
				<? endif; ?>

				<? if ($property['CODE'] == 'WORK_COST'): ?>
					<div class="afs-task-customer-detail-row row">
						<div class="col-md-3"><b><?=$property['NAME']?></b></div>
						<div class="col-md-9">
							<?=round($property['DISPLAY_VALUE'])?>
						</div>
					</div>
				<? endif; ?>

<!--				--><?// if ($property['CODE'] == 'EXECUTOR_COMMENT'): ?>
<!--					<div class="afs-task-customer-detail-row row">-->
<!--						<div class="col-md-3"><b>--><?//=$property['NAME']?><!--</b></div>-->
<!--						<div class="col-md-9">-->
<!--							--><?//=$property['DISPLAY_VALUE']?>
<!--						</div>-->
<!--					</div>-->
<!--				--><?// endif; ?>

			<? endforeach; ?>
		</div>
	<? endif; ?>
</div>

