<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskDetail extends \Mlab\Appforsale\Component\Element
{
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		if ($USER->IsAuthorized())
			$arFilter['!CREATED_BY'] = $USER->GetID();
		return $arFilter;
	}

	protected function getAdditionalCacheId() {
		$res = parent::getAdditionalCacheId();
		$res[] = $_GET['sections'];

		return $res;

	}
}
?>