<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$areaId = $this->GetEditAreaId('');
$itemIds = array(
	'ID' => $areaId,
	'CITY' => $areaId.'_city',
	'SELECT' => $areaId.'_select'
);

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.js"></script>

<div class="afs-task-filter form-horizontal">
	<?
	$arFilter = array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CREATED_BY' => $USER->GetID(),
	);
	?>
	
	<select class="form-control" id="status" multiple="multiple">
			<option value="N">Открыто</option>
			<option value="P">Выполняется</option>
			<option value="D">Исполнено</option>
			<option value="F">Закрыто</option>
	</select>
	<input placeholder="Номер телефона" class="form-control" id="phone" type="text">
	<input placeholder="Адрес" class="form-control" id="address" type="text">
	<input placeholder="ФИО исполнителя" class="form-control" id="name" type="text">
	<p class="date-text" >Дата cоздания: </p><input class="form-control" id="date" type="date">
	<input type="button" class="form-control" id="find" value="Найти">
</div>
<script>
$('select').selectize({
	closeAfterSelect: true,
	placeholder: 'Статус'
});
$( "#find" ).on( "click", function() {
	city_id = 0;
	var values = [];
	this.obSelect = $("#status")[0];
	for (var i=0; i < this.obSelect.options.length; i++)
	{
		if (this.obSelect.options[i].selected)
			values.push(this.obSelect.options[i].value);
	} 
	
	path = '/youdo/tasks-customer/' + "?status=" + values.join();
	
	if ($("#phone").val().length > 0)
		path = path + "&phone=" + $("#phone").val();	
	
	if ($("#address").val().length > 0)
		path = path + "&address=" + $("#address").val();

	if ($("#name").val().length > 0)
		path = path + "&name=" + $("#name").val();	
	
	if ($("#date").val().length > 0)
		path = path + "&date=" + $("#date").val();
	
	BX.ajax.insertToNode(
		path,
		BX('list_for_filter')
	);
});
</script>
<? 
$arJSParams = array(
	'pagePath' => GetPagePath(false, false),
	'VISUAL' => $itemIds
);
echo '<script type="text/javascript">new JCTaskFilter('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>