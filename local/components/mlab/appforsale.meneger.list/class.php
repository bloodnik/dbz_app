<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use CUser;

class TaskList extends \Mlab\Appforsale\Component\ElementList
{

	const DEFAULT_CITY = 6726; //Город по умолчанию (Уфа)
	const TASK_VISIBLE_GROUPS = [1, 5]; //Группы видящие все задания

	public function onPrepareComponentParams($arParams)
	{
		$arParams = parent::onPrepareComponentParams($arParams);
	
		$arParams['OWNER'] = ($arParams['OWNER'] == 'Y' ? 'Y' : 'N');
	
		return $arParams;
	}
	
	protected function makeOutputResult()
	{
		global $USER;
		
		parent::makeOutputResult();
		if ($USER->IsAuthorized() && $this->arParams['OWNER'] == 'Y')
		{
			foreach ($this->arResult['ITEMS'] as &$arItem)
			{
				if ($arItem['CREATED_BY'] == $USER->GetID())
				{
					$arItem['DETAIL_PAGE_URL'] = str_replace('/youdo/', '/youdo/tasks-customer/', $arItem['DETAIL_PAGE_URL']);
				}
			}
		}
	}
	
	protected function getFilter()
	{
		global $USER;

		// START/SIRIL 31.01.2019 - Фильтр по городу пользователя
		$order = array('sort' => 'asc');
		$tmp = 'sort';
		$filter = array('ID' => $USER->GetID() );
		$select = array('SELECT' => array('UF_*'));
		$rsUsers = CUser::GetList($order, $tmp, $filter, $select);

		$userCity = self::DEFAULT_CITY; //По умолчанию город Уфа
		while ($arUser = $rsUsers->Fetch()) {
			$userCity = $arUser['UF_CITY'];
		}
		$arUserGroups = CUser::GetUserGroup($USER->GetID());
		// END/SIRIL 31.01.2019 - Фильтр по городу пользователя

		$arFilter = parent::getFilter();
		unset($arFilter['ACTIVE']);

		// START/SIRIL 03.02.2019 - Фильтр по городу пользователя
		if (count(array_intersect(self::TASK_VISIBLE_GROUPS, $arUserGroups)) === 0) {
			$arFilter['CREATED_BY'] = $USER->GetID();
		}
		// END/SIRIL 03.02.2019 - Фильтр по городу пользователя

		if (!empty($_GET['status']))
		{
			$arFilter['PROPERTY_STATUS_ID'] = explode(',', $_GET['status']);
		}
		if (!empty($_GET['phone']))
		{
			$arFilter['%PROPERTY_PHONE'] = $_GET['phone'];
		}
		if (!empty($_GET['address']))
		{
			$arFilter['%PROPERTY_ADDRESS'] = $_GET['address'];
		}			
		if (!empty($_GET['name']))
		{
			$arFilter['%PROPERTY_EXECUTOR_NAME'] = $_GET['name'];
		}

		// START/SIRIL 31.01.2019 - Фильтр по городу пользователя
		if (count(array_intersect(self::TASK_VISIBLE_GROUPS, $arUserGroups)) === 0) {
			if ($userCity == self::DEFAULT_CITY) { // Если город Уфа проказываем задания также с незаполненным свойством город
				$arFilter[] = array(
					"LOGIC" => "OR",
					array("PROPERTY_CITY" => $userCity),
					array("%PROPERTY_CITY" => false),
				);
			} else {
				$arFilter['%PROPERTY_CITY'] = $userCity;
			}
		}
		// END/SIRIL 31.01.2019 - Фильтр по городу пользователя
		
		if (!empty($_GET['date']))
		{
			$date = new DateTime($_GET['date']);
			$date1 = $date->format('d.m.Y 00:00:00');
			$date2 = $date->format('d.m.Y 23:59:59');
			$arFilter['>=DATE_CREATE'] = ConvertTimeStamp(strtotime($date1),"FULL");
			$arFilter['<=DATE_CREATE'] = ConvertTimeStamp(strtotime($date2),"FULL");
		}		

		return $arFilter;
	}
	
	protected function getSort()
	{
		if (!empty($_GET['sort']))
		{
			$sort = trim($_GET['sort']);
			$newSort = str_replace('High', '', $sort);
			if ($newSort != $sort)
			{
				return array(
					$newSort => 'ASC'
				);
			}
			
			$newSort = str_replace('Low', '', $sort);
			if ($newSort != $sort)
			{
				return array(
					$newSort => 'DESC'
				);
			}
		}
		
		return parent::getSort();
	}
	
	protected function getAdditionalCacheId()
	{
		global $USER;
		$UF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', $USER->GetID());
		$city_id = intval($UF['UF_CITY']['VALUE']);
		
		if (!empty($_SESSION['city_id']))
			$city_id = $_SESSION['city_id'];
		if (!empty($_GET['city_id']))
		{
			$city_id = $_GET['city_id'];
			$_SESSION['city_id'] = $city_id;
		}
		
		if (!empty($_SESSION['sort']))
			$sort = $_SESSION['sort'];
		if (!empty($_GET['sort']))
		{
			$sort = $_GET['sort'];
			$_SESSION['sort'] = $sort;
		}

				
		$res = parent::getAdditionalCacheId();
		$res[] = $_GET['sections'];
		$res[] = $_GET['status'];
		$res[] = $_GET['phone'];
		$res[] = $_GET['address'];
		$res[] = $_GET['name'];
		$res[] = $_GET['date'];
		$res[] = $city_id;
		$res[] = $sort;
		
		return $res;
	}
}
?>