<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class ExecutorFilter extends \Mlab\Appforsale\Component\SectionList
{
	protected function getFilter()
	{
		$arFilter = parent::getFilter();
		unset($arFilter['SECTION_ID']);
		return $arFilter;
	}
}
?>
