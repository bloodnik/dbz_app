<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<?
$areaId   = $this->GetEditAreaId('');
$itemIds  = array(
	'ID'     => $areaId,
	'CITY'   => $areaId . '_city',
	'SELECT' => $areaId . '_select',
	'NAME' => $areaId . '_name'
);
$sections = explode(',', $_GET['sections']);

$arFilter  = array(
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'ACTIVE'    => 'Y'
);
$arSection = Mlab\Appforsale\Cnt::get($arFilter);
?>
	<div class="afs-task-filter form-horizontal">
		<select class="form-control" id="<?=$itemIds['SELECT']?>" multiple="multiple">
			<? if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')): ?>
				<optgroup disabled hidden></optgroup>';
			<? endif; ?>

			<?
			$arSelect = Array('ID', 'NAME', 'DEPTH_LEVEL');
			$arFilter = Array('IBLOCK_ID' => 4, 'ACTIVE' => 'Y');
			$res      = CIBlockSection::GetList(Array('left_margin' => 'ASC'), $arFilter, true, $arSelect);
			while ($ob = $res->GetNext()):?>
				<? if ($ob['DEPTH_LEVEL'] == 1):?>
					<optgroup label="<?=$ob['NAME']?>"></optgroup>
				<? else:?>
					<option value='<?=$ob['ID']?>'><? for ($i = 1; $i < $ob['DEPTH_LEVEL']; $i++) {
							echo '-';
						} ?><?=$ob['DEPTH_LEVEL'] > 1 ? ' ' . $ob['NAME'] : $ob['NAME']?></option>
				<? endif; ?>
			<? endwhile; ?>

			<? /*foreach ($arResult['ITEMS'] as $arItem):?>
			<option value="<?=$arItem['ID']?>"<?=(in_array($arItem['ID'], $sections) ? ' selected="selected"' : '')?>><?for($i = 1; $i < $arItem['DEPTH_LEVEL']; $i++) { echo '-'; }?><?=$arItem['DEPTH_LEVEL'] > 1 ? ' '.$arItem['NAME'] : $arItem['NAME']?><?=$arParams['CNT'] == 'Y' ? ' ('.intval($arSection[$arItem['ID']]). ')' : ''?></option>
		<?endforeach;*/ ?>

		</select>
		<br>
		<input class="form-control" id="<?=$itemIds['NAME']?>" type="text" placeholder="Поиск по ФИО">
	</div>
<?
$arJSParams = array(
	'pagePath' => GetPagePath(false, false),
	'VISUAL'   => $itemIds
);
echo '<script type="text/javascript">new JCExecutorFilter(' . CUtil::PhpToJSObject($arJSParams, false, true) . ')</script>';
?>