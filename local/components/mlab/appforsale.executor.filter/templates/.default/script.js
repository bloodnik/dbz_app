/* mlab:appforsale.task.filter */
(function (window) {

    if (!!window.JCExecutorFilter) {
        return;
    }

    window.JCExecutorFilter = function (arParams) {
        this.pagePath = '';
        this.visual = {
            CITY: '',
            SELECT: '',
            NAME: ''
        };

        this.obCity = null;
        this.obSelect = null;
        this.obName = null;

        if ('object' === typeof arParams) {
            this.pagePath = arParams.pagePath;
            this.visual = arParams.VISUAL;
        }

        BX.ready(BX.delegate(this.Init, this));
    }

    window.JCExecutorFilter.prototype.Init = function () {
        this.obCity = BX(this.visual.CITY);

        if (!!this.obCity) {
            BX.bind(this.obCity, 'change', BX.delegate(this.OnChange, this));
        }

        this.obSelect = BX(this.visual.SELECT);

        if (!!this.obSelect) {
            BX.bind(this.obSelect, 'change', BX.delegate(this.OnChange, this));
        }

        this.obName = BX(this.visual.NAME);

        if (!!this.obName) {
            BX.bind(this.obName, 'change', BX.delegate(this.OnChange, this));
        }


    };

    window.JCExecutorFilter.prototype.OnChange = function (e) {
        var city_id = 0;
        if (!!this.obCity) {
            for (var i = 0; i < this.obCity.options.length; i++) {
                if (this.obCity.options[i].selected)
                    city_id = this.obCity.options[i].value;
            }
        }

        var values = [];
        for (var i = 0; i < this.obSelect.options.length; i++) {
            if (this.obSelect.options[i].selected)
                values.push(this.obSelect.options[i].value);
        }

        var name = '';

        if (this.obName) {
            name = this.obName.value;
        }

//		BX.ajax.Setup(
//			{
//				denyShowWait: true
//			},
//			true
//		);

        path = this.pagePath + "?sections=" + values.join();
        if (city_id > 0)
            path = path + "&city_id=" + city_id;

        if (name.length > 0)
            path = path + "&name=" + name;

        BX.ajax.insertToNode(
            path,
            BX('list_for_filter')
        );
    }

})(window);