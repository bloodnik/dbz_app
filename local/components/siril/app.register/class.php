<?

use Bitrix\Main\Loader;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class AppRegister extends CBitrixComponent {

	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	public function executeComponent() {
		global $USER, $USER_FIELD_MANAGER;
		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}
//
//		if (!empty($USER->GetFirstName()) || !empty($USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_CITY", $USER->GetID())))
//			LocalRedirect("/youdo/notification/");


		// Проверка на существование профиля
		Loader::includeModule('iblock');
		$dbItem                    = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('*'),
			'filter' => array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, "CREATED_BY" => $USER->GetID()),
			'limit'  => 1,
		));
		$arProfileDirectionIds     = [];
		$arFreeSections            = [];
		$arPaidSections            = [];
		$this->arResult["PROFILE"] = [];
		if ($arItem = $dbItem->fetch()) {
			$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID']);
			while ($arProperty = $dbProperty->Fetch()) {
				if (empty($arProperty["CODE"])) {
					continue;
				}

				if ($arProperty["CODE"] == 'PROFILE_WORK') {
					$arProfileDirectionIds[] = $arProperty["VALUE"];
				} else {
					$arItem['PROPERTIES'][ $arProperty["CODE"] ] = $arProperty;
				}

				$this->arResult["PROFILE"] = $arItem;
			}

			// Направления деятельности
			$arFilter       = array("IBLOCK_ID" => self::TASKS_IBLOCK_ID, "ID" => $arProfileDirectionIds);
			$rsTaskSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "UF_FEE_PRICE", "IBLOCK_SECTION_ID"));

			while ($arTaskSections = $rsTaskSections->getNext()) {
				// Проверяем, платный ли родитель
				$paydParent = false;
				if ( ! empty($arTaskSections['IBLOCK_SECTION_ID'])) {
					$rsParentSections = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID, "ID" => $arTaskSections['IBLOCK_SECTION_ID']), false, array("ID", "UF_FEE_PRICE"));
					while ($arParentSection = $rsParentSections->GetNext()) {
						$paydParent = intval($arParentSection['UF_FEE_PRICE']) > 0;
					}
				}

				if ($arTaskSections["UF_FEE_PRICE"] > 0 || $paydParent) {
					$arPaidSections[] = $arTaskSections['ID'];
				} else {
					$arFreeSections[] = $arTaskSections['ID'];
				}
			}
		} else {
			// Направления деятельности
			$arFilter       = array("IBLOCK_ID" => self::TASKS_IBLOCK_ID);
			$rsTaskSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "UF_FEE_PRICE", "IBLOCK_SECTION_ID"));

			while ($arTaskSections = $rsTaskSections->getNext()) {
				// Проверяем, платный ли родитель
				$paydParent = false;
				if ( ! empty($arTaskSections['IBLOCK_SECTION_ID'])) {
					$rsParentSections = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID, "ID" => $arTaskSections['IBLOCK_SECTION_ID']), false, array("ID", "UF_FEE_PRICE"));
					while ($arParentSection = $rsParentSections->GetNext()) {
						$paydParent = intval($arParentSection['UF_FEE_PRICE']) > 0;
					}
				}

				if ($arTaskSections["UF_FEE_PRICE"] > 0 || $paydParent) {
					$arPaidSections[] = $arTaskSections['ID'];
				} else {
					$arFreeSections[] = $arTaskSections['ID'];
				}
			}
		}

		$this->arResult["FREE_SECTIONS"] = $arFreeSections;
		$this->arResult["PAID_SECTIONS"] = $arPaidSections;

		$this->includeComponentTemplate();

	}
}

?>