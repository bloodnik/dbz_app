<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->addExternalCss("https://cdn.jsdelivr.net/npm/@riophae/vue-treeselect@^0.4.0/dist/vue-treeselect.min.css");
$this->addExternalCss("https://unpkg.com/vue-select@latest/dist/vue-select.css");

$this->addExternalJs("https://cdn.jsdelivr.net/npm/@riophae/vue-treeselect@^0.4.0/dist/vue-treeselect.umd.min.js");
$this->addExternalJs("https://unpkg.com/vue-select@latest");
$this->addExternalJs("https://unpkg.com/vuejs-datepicker");
$this->addExternalJs("https://unpkg.com/vuejs-datepicker/dist/locale/translations/ru.js");

?>

<div class="w-100" id="afs-register" v-cloak>

	<b-form @submit="onSubmit" ref="form">

		<b-card-group deck class="my-3">
			<b-card bg-variant="light" :header="form.id ? 'Изменение профиля' : 'Регистрация профиля'">
				<b-alert v-if="form.id && form.active==='N'" variant="warning" show>Профиль на модерации</b-alert>
				<b-form-group label="Фамилия *">
					<b-form-input v-model="form.lastname" :tab-index="1" size="lg" type="text" required></b-form-input>
				</b-form-group>
				<b-form-group label="Имя *">
					<b-form-input v-model="form.name" :tab-index="2" size="lg" type="text" required></b-form-input>
				</b-form-group>
				<b-form-group label="Отчество *">
					<b-form-input v-model="form.secondname" :tab-index="3" size="lg" type="text" required></b-form-input>
				</b-form-group>
				<b-form-group label="Дата рождения *">
					<vuejs-datepicker v-model="form.birthday" :tab-index="4" required input-class="form-control" format="dd.MM.yyyy" :language="ru"></vuejs-datepicker>
				</b-form-group>

				<b-form-group label="Город *">
					<v-select class="style-chooser " v-model="form.city" :reduce="city => parseInt(city.ID, 10)" :options="cities" label="NAME" required/>
				</b-form-group>


				<b-form-group label="Адрес *">
					<b-form-input v-model="form.address" :tab-index="8" type="text" required></b-form-input>
				</b-form-group>

				<b-form-group label="Email">
					<b-form-input v-model="form.email" :tab-index="9" type="email" required>
				</b-form-group>
			</b-card>

		</b-card-group>

		<b-card-group v-if="!form.id" deck class="mb-3">
			<b-card bg-variant="light" header="Виды деятельности ">
				<b-form-group>
					<treeselect placeholder="Введите вид деятельности" :tab-index="11" size="lg"
					            v-model="form.selectedSections"
					            :normalizer="normalizer"
					            :multiple="true"
					            :options="sectionsTree"
					            :searchable="false"
					            :auto-select-descendants="true"
					            :auto-deselect-descendants="true"
					            :auto-select-ancestors="true"
					            :flat="true">
						>
					</treeselect>
				</b-form-group>

				<b-alert v-if="!form.selectedSections.length" variant="warning" show> Выберите необходимые направления деятельности для получения уведомлений по новым заявкам</b-alert>


			</b-card>
		</b-card-group>


		<b-alert show variant="danger" v-if="hasError">{{ message }}</b-alert>
		<b-button type="submit" :disabled="sending" :tab-index="13" block variant="warning">Сохранить</b-button>
	</b-form>
</div>

<script>
	Vue.component('vue-bootstrap-typeahead', VueBootstrapTypeahead);
	Vue.component('treeselect', VueTreeselect.Treeselect);
	Vue.component('v-select', VueSelect.VueSelect);


	new Vue({
		el: '#afs-register',
		components: {
			VueBootstrapTypeahead: VueBootstrapTypeahead,
			vuejsDatepicker: vuejsDatepicker
		},
		computed: {
			// Полский список разделов в древовидный. Бесплатные категории
			sectionsTreeFree: function () {
				return this.list_to_tree(JSON.parse(JSON.stringify(this.directions))).filter(function (direction) {
					// return (direction.CHILDREN && direction.CHILDREN.length > 0)
					return true;
				});
			},

			// Сумма списания в день
			totalFeePrice: function () {
				var _this = this, totalPrice = 0;

				if (!_this.direction.length) return 0;

				_this.form.selectedSections.forEach(function (section) {
					var findedSection = _this.direction.find(function (paidSection) {
						return paidSection.ID === section;
					});

					totalPrice += findedSection.FEE_PRICE;
				});

				return totalPrice;
			},

			// Полский список разделов в древовидный
			sectionsTree: function sectionsTree() {
				return this.list_to_tree(this.directions);
			}
		},
		data: function () {
			return {
				hasError: false,
				message: "",
				sending: false,
				ru: vdp_translation_ru.js,
				query: null,
				value: null,
				passportDescriction: "Чтобы подтвердить личность и показать клиентам вашу надежность. Предоставляя копию документа. вы заявляете. что не мошенник и вам нечего скрывать. Процедура проверки документа строго конфиденциальна. копия " +
					"документа без заверения нотариуса не имеет юридической силы и не может быть использована без вашего согласия.",
				form: {
					id: <?=$arResult['PROFILE'] ? $arResult['PROFILE']['ID'] : 0?>,
					active: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['ACTIVE'] : "N"?>',
					lastname: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['LAST_NAME']['VALUE'] : ""?>',
					name: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['NAME']['VALUE'] : ""?>',
					secondname: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['SECOND_NAME']['VALUE'] : ""?>',
					email: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['EMAIL']['VALUE'] : ""?>',
					birthday: moment('<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['BIRTHDAY']['VALUE'] : ""?>', 'DD.MM.YYYY').toString(),
					city: <?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['CITY']['VALUE'] : 0?>,
					address: '<?=$arResult['PROFILE'] ? $arResult['PROFILE']['PROPERTIES']['ADDRESS']['VALUE'] : ""?>',
					selectedSections:  <?=$arResult['PROFILE'] ? CUtil::PhpToJSObject(array_merge($arResult['FREE_SECTIONS'], $arResult['PAID_SECTIONS'])) : "[]"?>,
					sessid: "<?=bitrix_sessid()?>"
				},
				directions: [],
				cities: [],
				normalizer: function (node) {
					return {
						id: node.ID,
						label: node.NAME,
						children: node.CHILDREN
					};
				},
				confirmOptions: {
					html: true
				}
			};
		},
		mounted: function () {
			this.getCities();
			this.getDirections();
		},
		methods: {
			onSubmit: function onSubmit(evt) {
				evt.preventDefault();
				var _this = this;

				if (!_this.validateDirections()) {
					return false;
				}

				let confirmMessage = {
					html: true,
					title: 'Применить изменения?',
					body: _this.form.id ? "" : "Убедитесь в правильности выбора направлений. <br> Вы будете видеть заявки только по выбранным направлениям."
				};

				Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
					_this.sending = true;
					_this.hasError = false;
					_this.message = "";

					var fd = new FormData();
					fd.append('LAST_NAME', _this.form.lastname);
					fd.append('NAME', _this.form.name);
					fd.append('SECOND_NAME', _this.form.secondname);
					fd.append('BIRTHDAY', moment(_this.form.birthday).format('DD.MM.YYYY'));
					fd.append('CITY', _this.form.city);
					fd.append('ADDRESS', _this.form.address);
					fd.append('EMAIL', _this.form.email);
					fd.append('PROFILE_WORK', _this.form.selectedSections);
					fd.append('sessid', _this.form.sessid);


					try {
						axios.post('/youdo/api/register_profile.php', fd, {
							headers: {
								'Content-Type': 'multipart/form-data'
							}
						}).then(function (response) {
							var data = response.data;

							if (data && !data.hasError) {
								app.onCustomEvent('OnIblock');
								app.closeController();
								app.onCustomEvent('OnAfterUserLogin');
								// app.reload();
								app.loadPageStart({url: "/youdo/notification/", title: "Новые заявки"});
							} else {
								_this.hasError = true;
								_this.message = data.msg;
								_this.sending = false;
							}

						})

					} catch (e) {
						console.log(e);
					}
				});
			},

			validateDirections: function () {
				var _this = this;
				if (!_this.form.selectedSections.length && !_this.form.id) {
					const alertMessage = {
						html: true,
						okText: "Закрыть",
						title: 'Внимание!',
						body: "Не выбранно ни одного напрвления деятельности. <br> <br> Необходимо выбрать хотя бы одно направление для получения уведомлений по новым заявкам"
					};
					Vue.dialog.alert(alertMessage, _this.confirmOptions);

					return false;
				}

				return true;
			},

			getCities: function () {
				var _this = this;

				axios.get('/youdo/api/get_data.php', {params: {type: 'cities'}}).then(function (response) {
					_this.cities = response.data;
				})
			},
			getDirections: function () {
				var _this = this;

				axios.get('/youdo/api/get_data.php', {params: {type: 'direction'}}).then(function (response) {
					_this.directions = response.data;
				});
			},
			// Функция геренрации древовидного массива
			list_to_tree: function (list) {
				var map = {},
					node,
					roots = [],
					i;

				for (i = 0; i < list.length; i += 1) {
					map[list[i].ID] = i; // initialize the map

					list[i].CHILDREN = []; // initialize the children
				}


				for (i = 0; i < list.length; i += 1) {
					node = list[i];

					if (node.IBLOCK_SECTION_ID) {
						list[map[node.IBLOCK_SECTION_ID]].CHILDREN.push({
							ID: node.ID,
							NAME: node.NAME,
							PRICE: node.FEE_PRICE,
							IS_PAYD: node.IS_PAYD
						});
					} else {
						roots.push(node);
					}
				}

				for (i = 0; i < roots.length; i += 1) {
					if (!roots[i].CHILDREN.length) {
						roots[i].CHILDREN = null;
					}
				}
				return roots;
			}
		}
	});
</script>