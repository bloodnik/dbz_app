<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
global $USER;
?>

<div class="w-100 mt-3" id="afs-balance" v-cloak>
	<b-card-group deck class="mb-3">
		<b-card bg-variant="light" header="Баланс">
			<div class="row">
				<div class="col d-flex align-items-center mb-0 justify-content-center lead">{{ currentBudget }}
					РУБ
				</div>
				<div class="col">
					<b-button @click="goToPayPage()" :disabled="!profileIsActive" size="sm" block class="afs-btn-success btn">Пополнить</b-button>
				</div>
			</div>
		</b-card>
	</b-card-group>

	<b-card-group deck class="mb-3" v-if="!!bonusAccount">
		<b-card bg-variant="light" header="Бонусный счет">
			<div class="row">
				<div class="col d-flex align-items-center lead mb-0">{{ bonusAccount.UF_SUMM }}
					Бонусов
				</div>
			</div>
		</b-card>
	</b-card-group>

	<b-card-group deck class="mb-3">
		<b-card bg-variant="light" header="Процент возвратов">
			<div class="row">
				<div class="col d-flex align-items-center mb-0 lead">{{ returnsRating }}
					%
				</div>
			</div>
		</b-card>
	</b-card-group>

	<div class="row">
		<div class="col">
			<b-alert v-if="returnsRating >= 35" show variant="warning">
				Вы превысили % возвратов. Возможность запроса возврата отключена до снижения 30%. Снизить процент можно одним из описанных в инструкции для мастеров способом
			</b-alert>
			<b-alert v-if="returnsRating >= 30 && returnsRating < 35" show variant="warning">
				Запрос возвратов по тарифу 10% доступен. По достижению 35% возможность будет недоступна
			</b-alert>
		</div>
	</div>
</div>

<script>
	new Vue({
		el: '#afs-balance',
		data: function data() {
			return {
				profileIsActive: !!<?=$arResult['PROFILE_ACTIVE'] ? 1 : 0?>,
				currentBudget: <?=$arResult['CURRENT_BUDGET'] ? $arResult['CURRENT_BUDGET'] : 0 ?> ,
				returnsRating: <?=$arResult["RETURNS_RATING"] > 0 ? $arResult["RETURNS_RATING"] : 0 ?> ,
				bonusAccount: <?=! empty($arResult['BONUS_ACCOUNT']) ? CUtil::PhpToJSObject($arResult['BONUS_ACCOUNT'], false, false, true) : "null" ?> ,
			};
		},
		methods: {
			goToPayPage: function goToPayPage() {
				app.loadPageBlank({
					url: "/youdo/personal/index.php?bitrix_include_areas=N&ITEM=account",
					title: "Баланс"
				});
			}
		}
	});
</script>