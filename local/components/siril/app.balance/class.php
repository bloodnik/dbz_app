<?

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class AppBalance extends CBitrixComponent {

	const PROFILE_IBLOCK_ID = 2;


	public function executeComponent() {
		global $USER;

		Loader::includeModule('mlab.appforsale');


		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}

		// Получаем список видов деятельности на которые подписан профиль мастера
		Loader::includeModule('iblock');
		$arFreeSections        = array();
		$arPaidSections        = array();
		$arProfileDirectionIds = [];
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, 'CREATED_BY' => $USER->GetId()));

		if ($arProfile = $rsProfile->getNext()) {
			$this->arResult["PROFILE_ACTIVE"] = $arProfile['ACTIVE'] == 'Y';
		}

		// Текущий бюджет
		$dbUserAccount = CAppforsaleUserAccount::GetList(array(), array("USER_ID" => $USER->GetId()));
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$this->arResult["CURRENT_BUDGET"] = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		// Бонусный счет
		$obBonusAccount = new CAppforsaleTaskBonusAccount($USER->GetId());
		$this->arResult["BONUS_ACCOUNT"] = $obBonusAccount->getAccount();

		// Процент возвратов
		$returnRating = new CAppforsaleReturnsRating($USER->GetID());
		$this->arResult["RETURNS_RATING"] = $returnRating->getReturnsRating();

		$this->includeComponentTemplate();

	}
}

?>