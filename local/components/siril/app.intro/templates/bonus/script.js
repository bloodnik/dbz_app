Vue.component('boardal', {
    template: '<transition name="boardal"><div class="boardal"><div class="boardal__mask" v-if="hasMask" @click="clickMask"></div><div class="boardal__wrapper"><slot></slot><div class="boardal__x" v-if="hasX" @click="clickX">&times;</div></div></transition>',
    props: ['hasX', 'hasMask', 'canClickMask'],
    methods: {
        clickX: function clickX() {
            this.$emit('toggle');
        },
        clickMask: function clickMask() {
            if (this.canClickMask) {
                this.$emit('toggle');
            }
        }
    }
});
var vm = new Vue({
    el: 'main',
    data: {
        modal: {
            isOpen: false,
            hasMask: true,
            canClickMask: false,
            hasX: false
        },
        step: 1,
        max: 1,
        showDots: true,
        orientation: 'row' // xray: 'hidden'

    },
    computed: {
        isFirstStep: function isFirstStep() {
            return this.step === 1;
        },
        isLastStep: function isLastStep() {
            return this.step === this.max;
        },
        hasDots: function hasDots() {
            return this.max > 1 && this.showDots;
        },
        x_multiplier: function x_multiplier() {
            return this.orientation === 'row' ? -1 : 0;
        },
        y_multiplier: function y_multiplier() {
            return this.orientation === 'row' ? 0 : -1;
        },
        axis: function axis() {
            return this.orientation === 'row' ? 'row' : 'column';
        },
        axisReverse: function axisReverse() {
            return this.orientation === 'row' ? 'row-reverse' : 'column-reverse';
        },
        cross: function cross() {
            return this.orientation === 'row' ? 'column' : 'row';
        },
        crossReverse: function crossReverse() {
            return this.orientation === 'row' ? 'column-reverse' : 'row-reverse';
        },
        nextIcon: function nextIcon() {
            return this.orientation === 'row' ? 'fa-arrow-right' : 'fa-arrow-down';
        },
        backIcon: function backIcon() {
            return this.orientation === 'row' ? 'fa-arrow-left' : 'fa-arrow-up';
        }
    },
    watch: {
        orientation: 'setCssVars' // xray: 'setCssVars'

    },
    mounted: function mounted() {
        this.toggleModal();
    },
    methods: {
        toggleModal: function toggleModal(step) {
            step = step || 1;
            this.modal.isOpen = !this.modal.isOpen;

            if (this.modal.isOpen) {
                var self = this;
                setTimeout(function () {
                    self.$sections = self.$el.querySelectorAll('section');
                    self.max = self.$sections.length;
                    self.goToStep(step);
                }, 1);
            }
        },
        setCssVars: function setCssVars() {
            this.$el.style.setProperty('--x', (this.step * 100 - 100) * this.x_multiplier + '%');
            this.$el.style.setProperty('--y', (this.step * 100 - 100) * this.y_multiplier + '%');
            this.$el.style.setProperty('--axis', this.axis);
            this.$el.style.setProperty('--axis-reverse', this.axisReverse);
            this.$el.style.setProperty('--cross', this.cross);
            this.$el.style.setProperty('--cross-reverse', this.crossReverse); // this.$el.style.setProperty('--vision', this.xray)
        },
        goToStep: function goToStep(step) {
            this.step = step > this.max ? this.max : step < 1 ? 1 : step;
            this.currentSection = this.$sections[this.step - 1];
            Array.prototype.slice.call(this.$sections).forEach(function (section) {
                section.classList.remove('current');
            });

            this.currentSection.classList.add('current');
            this.currentSection.scrollTop = 0;
            this.setCssVars();
        },
        skip: function skip(step) {
            this.step += step;
            this.goToStep(this.step);
        },
        reset: function reset() {
            this.goToStep(1);
        },
        finish: function finish() {
            app.loadPageBlank({
                url: "/youdo/",
                title: ""
            });
        },
        swipeLeftHandler: function swipeLeftHandler() {
            this.skip(1);
        },
        swipeRightHandler: function swipeRightHandler() {
            this.skip(-1);
        }
    }
});