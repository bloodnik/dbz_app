<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->addExternalJs("https://cdn.jsdelivr.net/npm/intersection-observer@0.5.1/intersection-observer.min.js");
$this->addExternalJs("https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js");
$this->addExternalJs($this->getComponent()->getTemplate()->GetFolder() . "/vue-touch.js");

\Bitrix\Main\Loader::includeModule('mlab.appforsale');

global $USER;

?>
<script>
	var lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy"
		// ... more custom settings?
	});
</script>

<main id="afs-app-intro" class="afs-app-intro" data-email="<?=$USER->GetEmail()?>" v-cloak>
	<!--  The Modal -->
	<boardal v-if="modal.isOpen" :has-mask="modal.hasMask" :can-click-mask="modal.canClickMask" :has-x="modal.hasX" @toggle="toggleModal">
		<article v-cloak v-touch:swipe.left="swipeLeftHandler" v-touch:swipe.right="swipeRightHandler">
			<? $APPLICATION->IncludeFile(SITE_DIR . "include/instruction.php", array(), array(
					"MODE"     => "html",
					"NAME"     => "Text in title",
					"TEMPLATE" => "include_area",
				)
			); ?>

		</article>
		<footer <?if($arParams["FOR_NEW_APP"] == 'Y'):?>style="position: absolute; bottom: 50%; left: 0; right: 0;" <?endif;?>>
			<div class="actions">
				<div class="forward-actions">
					<!--         <button class="secondary skip" :disabled="isLastStep" v-show="!isLastStep" @click="skip(2)">Skip</button> -->
					<button class="primary next" :disabled="isLastStep" v-show="!isLastStep" @click="skip(1)"><i class="fa fa-fw fa-lg" :class="nextIcon"></i></button>
					<? if ($arParams["FOR_NEW_APP"] == 'N'): ?>
						<button class="accent save" :disabled="!isLastStep" v-show="isLastStep" @click="finish"><i class="fa fa-fw fa-lg fa-check"></i></button>
					<? endif; ?>
				</div>
<!--				<div class="step-dots" v-if="hasDots">-->
<!--					<div class="step-dot" v-for="n in max" :class="{active: n == step}" @click="goToStep(n)"></div>-->
<!--				</div>-->
				<div class="back-actions">
					<button class="secondary cancel prev" :disabled="isFirstStep" xv-show="!isFirstStep" @click="skip(-1)"><i class="fa fa-fw fa-lg" :class="backIcon"></i></button>
				</div>
			</div>
		</footer>
	</boardal>
</main>