<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

?>

<? if ( ! empty($arResult['ITEMS'])): ?>
	<ul>
		<? foreach ($arResult['ITEMS'] as $arItem):
			$url = strip_tags($arItem['LINK']);

			$badgeColor   = "";
			$status_id    = "";
			$status_name  = "";
			$disableClass = "";
			if ( ! empty($arItem["TASK_ID"])) {
				$status_name = $arResult["TASK_STATUSES"][ $arItem["TASK_ID"] ]["NAME"];
				$status_id   = $arResult["TASK_STATUSES"][ $arItem["TASK_ID"] ]["CODE"];
				switch ($status_id) {
					case "N":
						$badgeColor = "afs-badge-success";
						break;
					case "P":
						$badgeColor = "afs-badge-warning";
						break;
					case "D":
						$badgeColor = "afs-badge-info";
						break;
					case "F":
						$badgeColor = "afs-badge-danger";
						break;
				}

				if (in_array($status_id, ["P", "D", "F", "E"])) {
					$disableClass = "afs-disabled";
				}
			}
			?>


			<li class="afs-notification-item <?=$disableClass?>" <? if (empty($disableClass)): ?>onclick="app.loadPageBlank({url: '<?=$url?>', title: '<?=htmlspecialchars($arItem['NAME'])?>'})"<? endif;
			?>>
				<div class="flex-row no-gutters mb-2">
					<div class="col-xs-6 d-flex align-items-center">
						<div class=" afs-notification-date"><?=$arItem['CREATED_DATE']?></div>
					</div>
					<div class="col-xs-6 d-flex align-items-center  justify-content-end">
						<span class="afs-badge <?=$badgeColor?>"><?=$status_name?></span>
					</div>
				</div>

				<div class="afs-notification-name"><?=$arItem['NAME']?></div>
			</li>
		<? endforeach; ?>
	</ul>
	<?=$arResult['NAV_STRING']?>
<? else: ?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>
<? endif; ?>
<script>
    app.onCustomEvent("OnNotificationCount");
</script>