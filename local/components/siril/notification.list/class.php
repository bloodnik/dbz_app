<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class NotificationList extends CBitrixComponent {


	protected function initPageNavigation() {
		$nav = new \Bitrix\Main\UI\PageNavigation("nav-rows");
		$nav->allowAllRecords(false)
		    ->setPageSize(150)
		    ->initFromUri();
		$this->navigation = array(
			"page_size"   => $nav->getPageSize(),
			"page_number" => $nav->getCurrentPage(),
			"allRecords"  => "N"
		);
		$this->nav        = $nav;
	}

	public function initResult() {
		if ($this->nav instanceof \Bitrix\Main\UI\PageNavigation) {
			$this->arResult['NAV_OBJECT'] = $this->nav;
		}
	}

	public function getNavString($nav) {
		$AJAX_ID = $this->GetEditAreaId('ajax');

		if ( ! ($nav instanceof \Bitrix\Main\UI\PageNavigation)) {
			return false;
		}
		global $APPLICATION;
		ob_start();
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.pagenavigation',
			'',
			array(
				'NAV_RESULT' => $nav,
				'AJAX_ID'    => $AJAX_ID
			),
			$this,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		$this->arResult['NAV_STRING'] = ob_get_contents();
		ob_end_clean();
	}


	public function executeComponent() {
		global $USER, $USER_FIELD_MANAGER;
		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}

		if (empty($USER->GetFirstName()) || empty($USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_CITY", $USER->GetID()))) {
			LocalRedirect("/youdo/personal/register/");
		}


		\Bitrix\Main\Loader::includeModule('mlab.appforsale');
		if ( ! CAppforsaleProfile::isIntroShown()) {
			LocalRedirect("/youdo/app_intro/");
		}


		$entity_data_class = getHighloadEntity(1);

		// Снимаем активность с уведомлений
		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"order"  => array("ID" => "DESC"),
			"filter" => array("UF_ACTIVE" => 1, "UF_CREATED_BY" => $USER->GetID())  // Задаем параметры фильтра выборки
		));
		while ($arData = $rsData->Fetch()) {
			$entity_data_class::update($arData["ID"], array("UF_ACTIVE" => 0));
		}

		$this->initPageNavigation();
		$this->initResult();

		$cnt = $entity_data_class::getCount(array("UF_CREATED_BY" => $USER->GetID()));
		$this->arResult["NAV_OBJECT"]->setRecordCount($cnt);
		$this->arResult["NAV_STRING"] = $this->getNavString($this->arResult["NAV_OBJECT"]);

		// Получаем все уведолмения
		$rsData = $entity_data_class::getList(array(
			"select"      => array("*"),
			"order"       => array("ID" => "DESC"),
			"filter"      => array("UF_CREATED_BY" => $USER->GetID()),  // Задаем параметры фильтра выборки
			"count_total" => true,
			'offset'      => $this->arResult["NAV_OBJECT"]->getOffset(),
			'limit'       => $this->arResult["NAV_OBJECT"]->getLimit(),
		));

		$arTaskIds = [];
		while ($arData = $rsData->Fetch()) {
			$arItem['ID']              = $arData["ID"];
			$arItem['LINK']            = $arData["UF_LINK"];
			$arItem['NAME']            = $arData["UF_NAME"];
			$arItem['TASK_ID']            = $arData["UF_TASK_ID"];
			$arItem['CREATED_DATE']    = $arData["UF_CREATED_DATE"]->toString();

			if ( ! empty($arData["UF_TASK_ID"])) {
				if(in_array($arData["UF_TASK_ID"], $arTaskIds)) continue;
				$arTaskIds[] = $arData["UF_TASK_ID"];
			}

			$this->arResult["ITEMS"][] = $arItem;
		}

		$this->arResult["TASK_STATUSES"] = CAppforsaleTask::GetTaskStatus(array_unique($arTaskIds));

		$this->includeComponentTemplate();

	}
}

?>