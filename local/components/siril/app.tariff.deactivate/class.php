<?

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class AppTariffDeactivate extends CBitrixComponent {

	public function executeComponent() {
		global $USER, $APPLICATION;

		Loader::includeModule('mlab.appforsale');


		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}

		if(! $USER->IsAdmin()) {
			ShowError("Доступ запрещен");
		}

		$this->includeComponentTemplate();

	}
}

?>