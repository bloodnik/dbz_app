<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

?>


<div class="w-100" id="afs-tariff-deactivate" v-cloak>
	<br>
	<h5>Деактивация тарифа</h5>
	<br>


	<b-card-group deck class="mb-3">
		<b-card bg-variant="light" header="Пользователи">
			<b-row>
				<b-col lg="12" class="my-1">
					<div class="row">
						<b-col sm="3">
							<label>Поиск пользователя:</label>
						</b-col>
						<b-col sm="9">
							<b-input-group>
								<b-form-input v-model="filter"
								              type="search"
								              id="filterInput" type="text"></b-form-input>

								<b-input-group-append>
									<b-button variant="outline-secondary">Поиск</b-button>
								</b-input-group-append>
							</b-input-group>
						</b-col>
					</div>
				</b-col>
			</b-row>

			<b-table
					class="table table-striped"
					:items="filteredItems"
					:fields="fields"
					:sort-by.sync="sortBy"
					:sort-desc.sync="sortDesc"
					responsive="sm"
					:per-page="perPage"
					:current-page="currentPage"
			>
				<template #cell(ACTIONS)="{item}">
					<b-button @click="deactivate(item)" size="sm" variant="danger">Деактивировать</b-button>
				</template>
			</b-table>


			<b-pagination
					class="m-0"
					v-model="currentPage"
					:total-rows="rows"
					:per-page="perPage"
					aria-controls="my-table"
					first-number
					last-number
			></b-pagination>

		</b-card>

	</b-card-group>

	<b-alert class="mt-3" show variant="danger" v-if="errorMessage">{{ message }}</b-alert>

</div>

<script>
	new Vue({
		el: '#afs-tariff-deactivate',
		computed: {},
		data: function data() {
			return {
				filter: "",
				perPage: 100,
				currentPage: 1,
				sortBy: 'FIO',
				sortDesc: false,
				rows: null,
				fields: [
					{key: "FIO", label: "ФИО"},
					{key: "LOGIN", label: "Телефон"},
					{key: "TARIFF_NAME", label: "Тариф"},
					{key: "UF_TASK_COUNT", label: "Количество заявок"},
					{key: "UF_ACTIVATE_DATE", label: "Дата активации"},
					{key: "UF_ACTIVATE_DATE", label: "Дата активации"},
					{key: "UF_ACTIVE_TO", label: "Активно до"},
					{key: "ACTIONS", label: "Действия"}
				],

				successMessage: "",
				errorMessage: "",
				currentTariff: null,
				userTarifs: [],
				form: {
					sessid: "<?=bitrix_sessid()?>"
				},
				confirmOptions: {
					html: true
				},
				ajaxUrl: "<?=$componentPath?>/ajax.php"
			};
		},

		computed: {
			filteredItems() {
				if (!this.userTarifs) {
					return []
				}
				let items = this.userTarifs;
				if (this.filter.length > 0 && items && items.length) {
					items = this.userTarifs.filter((item) => item.FIO.toUpperCase().includes(this.filter.toUpperCase()) || item.LOGIN.includes(this.filter))
				}

				this.rows = items.length
				return items;
			},

		},

		mounted: function mounted() {
			this.getUsersTarrifs()
		},

		methods: {
			getUsersTarrifs: function getUsersTarrifs() {
				var _this = this;


				axios.get(_this.ajaxUrl, {params: {type: "getUserTarrifs", 'sessid': _this.form.sessid}}).then(function (response) {
					var data = response.data;

					if (data && !data.hasError) {
						_this.successMessage = data.msg;
						_this.userTarifs = data.result;
					} else {
						_this.errorMessage = data.msg;
					}
				});
			},

			deactivate: function deactivate(tariff) {
				var _this = this;

				var refundSumm = (parseInt(tariff.TARIFF_PRICE, 10) / parseInt(tariff.TARIFF_TASK_COUNT, 10)) * parseInt(tariff.UF_TASK_COUNT, 10);

				let confirmMessage = {
					html: true,
					title: 'Деактивировать тариф?',
					body: "Деактивировать тариф и вернуть пользователю " + refundSumm + " бонусов?"
				};

				Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
					_this.sending = true;
					_this.successMessage = "";
					_this.errorMessage = "";
					var fd = new FormData();
					fd.append('sessid', _this.form.sessid);
					fd.append('type', "deactivate");
					fd.append('tariffId', tariff.ID);

					try {
						axios.post(_this.ajaxUrl, fd).then(function (response) {
							var data = response.data;
							console.log(data);
							if (data && !data.hasError) {
								_this.successMessage = data.msg;
								alert(data.msg);
								_this.sending = false;
								_this.getUsersTarrifs();
							} else {
								_this.errorMessage = data.msg;
								alert(data.msg);
								_this.sending = false;
							}
						});
					} catch (e) {
						console.log(e);
						_this.sending = false;
						_this.errorMessage = e;
					}
				});
			},


			unescape_html(string) {
				var optTemp = 0
				var i = 0
				var noquotes = false

				var quoteStyle = 2
				string = string.toString()
					.replace(/&lt;/g, '<')
					.replace(/&gt;/g, '>')
				var OPTS = {
					'ENT_NOQUOTES': 0,
					'ENT_HTML_QUOTE_SINGLE': 1,
					'ENT_HTML_QUOTE_DOUBLE': 2,
					'ENT_COMPAT': 2,
					'ENT_QUOTES': 3,
					'ENT_IGNORE': 4
				}
				if (quoteStyle === 0) {
					noquotes = true
				}
				if (typeof quoteStyle !== 'number') {
					// Allow for a single string or an array of string flags
					quoteStyle = [].concat(quoteStyle)
					for (i = 0; i < quoteStyle.length; i++) {
						// Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
						if (OPTS[quoteStyle[i]] === 0) {
							noquotes = true
						} else if (OPTS[quoteStyle[i]]) {
							optTemp = optTemp | OPTS[quoteStyle[i]]
						}
					}
					quoteStyle = optTemp
				}
				if (quoteStyle & OPTS.ENT_HTML_QUOTE_SINGLE) {
					// PHP doesn't currently escape if more than one 0, but it should:
					string = string.replace(/&#0*39;/g, "'")
					// This would also be useful here, but not a part of PHP:
					// string = string.replace(/&apos;|&#x0*27;/g, "'");
				}
				if (!noquotes) {
					string = string.replace(/&quot;/g, '"')
				}
				// Put this in last place to avoid escape being double-decoded
				string = string.replace(/&amp;/g, '&')

				return string
			}
		}
	});
</script>