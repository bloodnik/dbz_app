<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PARTNERS_IBLOCK_ID", 2);
define("CITIES_IBLOCK_ID", 1);
define("TASK_IBLOCK_ID", 4);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$response = array(
	'status'  => true,
	'message' => "",
	'result'  => array(),
);

global $USER;


if ( ! check_bitrix_sessid()) {
	return;
}


if ( ! $USER->IsAuthorized()) {
	$response["status"]  = false;
	$response["message"] = "Необходима авторизация";
	echo json_encode($response);

	return false;
}

$request = Application::getInstance()->getContext()->getRequest();

$requestType = $request->isPost() ? $request->getPost('type') : $request->get('type');

if (isset($requestType) && strlen($requestType) > 0) {

	if ($requestType === 'getUserTarrifs') {

		$entity_data_class  = getHighloadEntityByName("DBZUserTariffs");
		$entityTariffsClass = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select"  => array(
				"*",
				"LOGIN"             => "USER.LOGIN",
				"LAST_NAME"         => "USER.LAST_NAME",
				"NAME"              => "USER.NAME",
				"SECOND_NAME"       => "USER.SECOND_NAME",
				"TARIFF_TYPE"       => "TARIFF.UF_TYPE",
				"TARIFF_NAME"       => "TARIFF.UF_NAME",
				"TARIFF_ID"         => "TARIFF.ID",
				"TARIFF_TASK_COUNT" => "TARIFF.UF_TASK_COUNT",
				"TARIFF_PRICE"      => "TARIFF.UF_PRICE",
				"TARIFF_COST_FROM"  => "TARIFF.UF_COST_FROM",
				"TARIFF_COST_TO"    =>
					"TARIFF.UF_COST_TO"
			),
			"filter"  => array(
				"UF_ACTIVE"       => 1,
				"!TARIFF.UF_TYPE" => 232,
				"!UF_USER"        => false
			),
			'runtime' => array(
				'USER'   => array(
					'data_type' => '\Bitrix\Main\UserTable',
					'reference' => array(
						'=this.UF_USER' => 'ref.ID'
					),
					'join_type' => 'LEFT'
				),
				'TARIFF' => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					$entityTariffsClass,
					[    // СМОТРИ ДОКУ ПО ORM: JOIN
						'=this.UF_TARIFF_TYPE' => 'ref.ID',
					],
					['join_type' => 'INNER']
				),
			),
		));

		$arResult = [];
		while ($arData = $rsData->Fetch()) {
			$arData["UF_ACTIVE"]        = ! ! $arData["UF_ACTIVE"];
			$arData["FIO"]              = $arData["LAST_NAME"] . " " . $arData["NAME"] . " " . $arData["SECOND_NAME"];
			$arData["UF_ACTIVE_TO"]     = $arData["UF_ACTIVE_TO"] ? $arData["UF_ACTIVE_TO"]->toString() : "";
			$arData["UF_CHANGE_DATE"]   = $arData["UF_CHANGE_DATE"] ? $arData["UF_CHANGE_DATE"]->toString() : "";
			$arData["UF_ACTIVATE_DATE"] = $arData["UF_ACTIVATE_DATE"] ? $arData["UF_ACTIVATE_DATE"]->toString() : "";
			$arResult[]                 = $arData;
		}

		$response["result"] = $arResult;
		echo json_encode($response);

	}

	if ($requestType === 'deactivate') {
		$tariffId = $request->get('tariffId');

		$entity_data_class  = getHighloadEntityByName("DBZUserTariffs");
		$entityTariffsClass = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select"  => array(
				"*",
				"TARIFF_NAME"       => "TARIFF.UF_NAME",
				"TARIFF_TASK_COUNT" => "TARIFF.UF_TASK_COUNT",
				"TARIFF_PRICE"      => "TARIFF.UF_PRICE"
			),
			"filter"  => array(
				"UF_ACTIVE" => 1,
				"ID"        => $tariffId
			),
			'runtime' => array(
				'TARIFF' => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					$entityTariffsClass,
					[    // СМОТРИ ДОКУ ПО ORM: JOIN
						'=this.UF_TARIFF_TYPE' => 'ref.ID',
					],
					['join_type' => 'INNER']
				)
			),
		));

		$response["status"]  = false;
		$response["message"] = "Активный тариф не найден";
		while ($arData = $rsData->Fetch()) {
			$updateData                     = [];
			$updateData["UF_USER"]          = $arData["UF_USER"];
			$updateData["UF_TASK_COUNT"]    = $arData["UF_TASK_COUNT"];
			$updateData["UF_TARIFF_TYPE"]   = $arData["UF_TARIFF_TYPE"];
			$updateData["UF_ACTIVE"]        = false;
			$updateData["UF_ACTIVE_TO"]     = false;
			$updateData["UF_ACTIVATE_DATE"] = false;
			$updateData["UF_CHANGE_DATE"]   = new \Bitrix\Main\Type\DateTime();
			$entity_data_class::update($tariffId, $updateData);

			$userId = $arData["UF_USER"];

			$refundSumm = (intval($arData['TARIFF_PRICE']) / intval($arData['TARIFF_TASK_COUNT'])) * $arData['UF_TASK_COUNT'];


			if ($refundSumm > 0) {
				$bonusTransact = new CAppforsaleTaskBonusTransact($userId, $refundSumm / 2, 1, "Компенсация за деактивацию тарифа " . $arData['TARIFF_NAME']);
				$bonusTransact->addTransact();

				// Списываем раннее начисленный кэшбек за активацию тарифа
				$balancePercent = ($arData['UF_COST'] * 100) / $arData['TARIFF_PRICE'];
				$writeOfPercent = 0;
				if ($balancePercent == 100) {
					$cashbackPercent = 0.15;
				} elseif ($balancePercent >= 50 && $balancePercent <= 99) {
					$cashbackPercent = 0.1;
				} elseif ($balancePercent > 0 && $balancePercent <= 49) {
					$cashbackPercent = 0.05;
				}

				$cashbackSumm = $arData['UF_COST'] * $cashbackPercent;
				if ($cashbackSumm) {
					$bonusTransact = new CAppforsaleTaskBonusTransact($userId, abs($cashbackSumm), 0, "Списание кэшбека за активацию тарифа");
					$bonusTransact->addTransact();
				}

				// Удаляем запись из журнала применения тарифов
				$entity_data_class = getHighloadEntityByName("DBZTariffAcceptLog");
				$rsTariffLog       = $entity_data_class::getList([
					"filter" => [
						"UF_USER" => $userId
					],
					"order"  => ["UF_DATE" => "DESC"],
					"limit"  => 1
				]);

				if ($arTariffLog = $rsTariffLog->Fetch()) {
					$arTariffLog["UF_DEACTIVATED"] = 1;
					$arTariffLog["UF_TASK_REMAIN"] = $arData['UF_TASK_COUNT'];
					$entity_data_class::update($arTariffLog["ID"], $arTariffLog);
				}

				\CAppforsale::SendMessage(
					array((int) $userId),
					array(
						'body'         => "Вам произведена компенсация за деактивацию тарифа " . $arData['TARIFF_NAME'],
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => 'http://dom-bez-zabot.online/youdo/personal/subscription/'
					)
				);

				$response["status"]  = true;
				$response["result"]  = $refundSumm / 2;
				$response["message"] = "Тариф успешно деактивирован с возмещением суммы";
			} else {
				$response["status"]  = false;
				$response["message"] = "Сумма не возвращена";
			}

		}
		echo json_encode($response);
	}

}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

