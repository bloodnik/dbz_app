<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->addExternalCss($this->GetFolder() . "/vendors/vue-treeselect.min.css");
$this->addExternalJs($this->GetFolder() . "/vendors/vue-treeselect.umd.min.js");

?>


<div class="w-100" id="afs-subscribe" v-cloak>
	<br>
	<h5>Подписка</h5>
	<br>
	<b-alert :show="isDemoPeriod" variant="warning">Идет пробный период</b-alert>

	<b-form @submit="onSubmit" ref="form">
		<b-card-group deck class="mb-3">
			<b-card bg-variant="light" header="Виды деятельности ">

				<b-form-group>
					<treeselect placeholder="Введите виды деятельности" :tab-index="11" size="lg"
					            v-model="form.selectedSections"
					            :normalizer="normalizer"
					            :multiple="true"
					            :options="sectionsTreeFree"
					            :searchable="false"
					            :auto-select-descendants="true"
					            :auto-deselect-descendants="true"
					            :auto-select-ancestors="true"
					            :flat="true">

						>
					</treeselect>

				</b-form-group>

			</b-card>
		</b-card-group>

		<b-alert show variant="danger" v-if="hasError">{{ message }}</b-alert>
		<b-button type="submit" :disabled="sending" :tab-index="13" block variant="warning">Сохранить</b-button>
	</b-form>
</div>

<script>
    Vue.component('treeselect', VueTreeselect.Treeselect);
    new Vue({
        el: '#afs-subscribe',
        computed: {
            // Полский список разделов в древовидный.
            sectionsTreeFree: function sectionsTreeFree() {
                return this.list_to_tree(JSON.parse(JSON.stringify(this.directions)));
            },
        },
        data: function data() {
            return {
                hasError: false,
                message: "",
                sending: false,

                query: null,
                value: null,
                profileIsActive: !!<?=$arResult['PROFILE_ACTIVE'] == "Y" ? 1 : 0?>,
                isDemoPeriod: !!<?=$arResult['IS_DEMO_PERIOD'] ? 1 : 0?>,
                form: {
                    selectedSections: <?=CUtil::PhpToJSObject(array_merge($arResult['FREE_SECTIONS'], $arResult['PAID_SECTIONS']))?>,
                    sessid: "<?=bitrix_sessid()?>"
                },
                directions: [],
                cities: [],
                normalizer: function normalizer(node) {
                    return {
                        id: node.ID,
                        label: node.NAME,
                        children: node.CHILDREN
                    };
                },
                confirmOptions: {
                    html: true
                }
            };
        },
        mounted: function mounted() {
            this.getDirections();
        },
        methods: {
            onSubmit: function onSubmit(evt) {
                evt.preventDefault();

                var _this = this;

                let confirmMessage = {
                    html: true,
                    title: 'Применить изменения?',
                    body: "Убедитесь в правильности выбора направлений. <br> После подтверждения оформления подписки и выбора тарифа, Вы сможете принимать заявки по выбранным направлениям"
                };

                Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
                    _this.sending = true;
                    _this.hasError = false;
                    _this.message = "";
                    var fd = new FormData();
                    fd.append('PROFILE_WORK', _this.form.selectedSections);
                    fd.append('sessid', _this.form.sessid);

                    try {
                        axios.post('/youdo/api/update_subscribe.php', fd).then(function (response) {
                            var data = response.data;

                            if (data && !data.hasError) {
                                app.onCustomEvent('OnIblock');
                                app.closeController();
                                app.onCustomEvent('OnAfterUserLogin');
                                app.reload();
                            } else {
                                _this.hasError = true;
                                _this.message = data.msg;
                                _this.sending = false;
                            }
                        });
                    } catch (e) {
                        console.log(e);
                    }
                });

            },
            getDirections: function getDirections() {
                var _this = this;

                axios.get('/youdo/api/get_data.php', {
                    params: {
                        type: 'direction'
                    }
                }).then(function (response) {
                    _this.directions = response.data;
                });
            },
            goToPayPage: function goToPayPage() {
                app.loadPageBlank({
                    url: "/youdo/personal/index.php?bitrix_include_areas=N&ITEM=account",
                    title: "Баланс"
                });
            },
            // Функция геренрации древовидного массива
            list_to_tree: function list_to_tree(list) {
                var map = {},
                    node,
                    roots = [],
                    i;

                for (i = 0; i < list.length; i += 1) {
                    map[list[i].ID] = i; // initialize the map

                    list[i].CHILDREN = []; // initialize the children
                }

                for (i = 0; i < list.length; i += 1) {
                    node = list[i];

                    if (node.IBLOCK_SECTION_ID) {
                        list[map[node.IBLOCK_SECTION_ID]].CHILDREN.push({
                            ID: node.ID,
                            NAME: node.NAME,
                            PRICE: node.FEE_PRICE,
                            IS_PAYD: node.IS_PAYD
                        });
                    } else {
                        roots.push(node);
                    }
                }

                for (i = 0; i < roots.length; i += 1) {
                    if (!roots[i].CHILDREN.length) {
                        roots[i].CHILDREN = null;
                    }
                }

                return roots;
            }
        }
    });
</script>