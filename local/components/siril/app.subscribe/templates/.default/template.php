<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->addExternalCss($this->GetFolder() . "/vendors/vue-treeselect.min.css");
$this->addExternalJs($this->GetFolder() . "/vendors/vue-treeselect.umd.min.js");

?>


<div class="w-100" id="afs-subscribe" v-cloak>
	<br>
	<h5>Подписка</h5>
	<br>
	<b-alert :show="isDemoPeriod" variant="warning">Идет пробный период</b-alert>

	<b-form @submit="onSubmit" ref="form">
		<b-card-group deck class="mb-3">

			<b-card v-if="profileIsActive" bg-variant="light" :header="currentDaysCount > 0 ? 'Осталось дней подписки' : 'Выберите необходимое количество дней подписки'">
				<b-card-text>
					<div class="row">
						<div class="col-3 d-flex align-items-center justify-content-center">
							<b-button @click="decrementDays()" size="sm" variant="outline-secondary">-</b-button>
						</div>
						<div class="col-6 d-flex align-items-center justify-content-center">
							<div class="lead">{{ currentDaysCount }} дн. <span v-if="form.daysCount">(+ {{form.daysCount}})</span>
							</div>

						</div>
						<div class="col-3 d-flex align-items-center justify-content-center">
							<b-button @click="incrementDays()" size="sm" variant="outline-primary">+</b-button>
						</div>
					</div>
				</b-card-text>
				<template v-if=" currentDaysCount > 0" #footer>
					<small class="text-muted">Следующее списание дня: {{ form.nextWriteOfDate }}</small>
				</template>
			</b-card>

			<b-card v-if="form.daysCount > 0" bg-variant="light" header="Стоимость абон. платы">
				<b-card-text>
					<div class="row">
						<div class="col-12 d-flex align-items-center">
							<div class="lead">{{ totalFeePrice }} руб. в день</div>
						</div>
						<div v-if="currentDaysCount > 0" class="col-12 d-flex align-items-center">
							<div class="lead mb-0">{{ totalFeePrice * currentDaysCount }} руб за {{ currentDaysCount }} дн.
							</div>
						</div>
						<div v-if="form.daysCount > 0" class="col-12 d-flex align-items-center">
							<div v-if="discountSum > 0" class="lead mb-0">
								или {{ (totalFeePrice * form.daysCount) - discountSum }}
								<span style="text-decoration: line-through">({{ totalFeePrice * form.daysCount }})</span> руб. за {{ form.daysCount }} дн.
								<p class="text-danger mt-3"><strong>Скидка: {{ discountSum }} руб ({{ discountsList[form.daysCount] }}%)</strong></p>
							</div>

							<div v-else class="lead mb-0">или {{ totalFeePrice * form.daysCount }} руб. за {{ form.daysCount }} дн.</div>
						</div>


					</div>

				</b-card-text>
			</b-card>
		</b-card-group>

		<b-card-group deck class="mb-3">
			<b-card bg-variant="light" header="Виды деятельности ">

				<b-form-group>
					<treeselect placeholder="Введите виды деятельности" :tab-index="11" size="lg"
					            v-model="form.selectedSections"
					            :normalizer="normalizer"
					            :multiple="true"
					            :options="sectionsTreeFree"
					            :searchable="false"
					            :auto-select-descendants="true"
					            :auto-deselect-descendants="true"
					            :auto-select-ancestors="true"
					            :disabled="currentDaysCount > 0"
					            :flat="true">

						>
					</treeselect>
					<div class="alert alert-warning small mt-1">
						<p>В случае наличия у Вас платной подписки, не менее чем 15 дней и отсутствия заявок по
							выбранным платным направлениям в течение этого периода, обратитесь на почту
							(support@dom-bez-zabot.net) для компенсации оплаченных дней
							платной подписки по выбранным платным направлениям.</p>
						<p>Компенсация будет предоставлена в виде дополнительных дней подписки числом кратным числу
							оформленных дней подписки.</p>
						<p>В письме сообщите:</p>
						<ul class="list-unstyled">
							<li> 1. Номер телефона под которым зарегистрированы</li>
							<li> 2. Ваши Ф.И.О.</li>
							<li> 3. Дату оформления подписки по выбранным платным направлениям на срок не менее 15
								дней
							</li>
							<li> 4. Сумму оплаченную за подписку по выбранным платным направлениям</li>
							<li> 5. Перечень выбранных платных направлений</li>
							<li> 6. Ваш город</li>
						</ul>
						<p> Запрос на компенсацию будет рассмотрен в течение 10 рабочих дней с момента корректного
							заполнения обращения.</p>

					</div>
					<div class="alert alert-warning small mt-1">
						<p>Сервис оставляет за собой право изменять стоимость и состав платных направлений подписки.</p>
						<p>Действие новых условий вступает в силу с момента оформления нового срока подписки
							пользователем</p>
					</div>
				</b-form-group>

			</b-card>
		</b-card-group>

		<b-alert show variant="danger" v-if="hasError">{{ message }}</b-alert>
		<b-button type="submit" :disabled="sending" :tab-index="13" block variant="warning">Сохранить</b-button>
	</b-form>
</div>

<script>
    Vue.component('treeselect', VueTreeselect.Treeselect);
    new Vue({
        el: '#afs-subscribe',
        computed: {
            // Полский список разделов в древовидный.
            sectionsTreeFree: function sectionsTreeFree() {
                return this.list_to_tree(JSON.parse(JSON.stringify(this.directions)));
            },
            // Сумма списания в день
            totalFeePrice: function totalFeePrice() {
                var _this = this,
                    totalPrice = 0;

                if (!_this.directions.length) return 0;

                _this.form.selectedSections.forEach(function (section) {
                    var findedSection = _this.directions.find(function (paidSection) {
                        return paidSection.ID === section;
                    });
                    totalPrice += findedSection.FEE_PRICE;
                });
                return totalPrice;
            },

            discountSum: function () {
                if (this.form.daysCount > 1) {
                    var percent = this.discountsList[this.form.daysCount]
                    var discountSumm = (this.form.daysCount * this.totalFeePrice) * (percent / 100);
                    return discountSumm.toFixed(2)
                }
                return 0;
            }
        },
        data: function data() {
            return {
                hasError: false,
                message: "",
                sending: false,

                query: null,
                value: null,
                profileIsActive: !!<?=$arResult['PROFILE_ACTIVE'] == "Y" ? 1 : 0?>,
                isDemoPeriod: !!<?=$arResult['IS_DEMO_PERIOD'] ? 1 : 0?>,
                currentDaysCount: <?=$arResult['DAYS_COUNT'] > 0 ? $arResult['DAYS_COUNT'] : 0?>,
                discountsList: <?=$arResult['DISCOUNTS'] ? CUtil::PhpToJSObject($arResult['DISCOUNTS']) : []?>,
                form: {
                    daysCount: 0,
                    nextWriteOfDate: "<?=$arResult['NEXT_WRITEOF_DATE']?>",
                    lastWriteOfDate: "<?=$arResult['LAST_WRITEOF_DATE']?>",
                    selectedSections: <?=CUtil::PhpToJSObject(array_merge($arResult['FREE_SECTIONS'], $arResult['PAID_SECTIONS']))?>,
                    sessid: "<?=bitrix_sessid()?>"
                },
                directions: [],
                cities: [],
                normalizer: function normalizer(node) {
                    return {
                        id: node.ID,
                        label: node.NAME,
                        children: node.CHILDREN
                    };
                },
                confirmOptions: {
                    html: true
                }
            };
        },
        mounted: function mounted() {
            this.getDirections();
        },
        methods: {
            onSubmit: function onSubmit(evt) {
                evt.preventDefault();

                var _this = this;

                let confirmMessage = {
                    html: true,
                    title: 'Применить изменения?',
                    body: "Убедитесь в правильности выбора направлений. <br> После подтверждения оформления подписки и выбора тарифа, Вы сможете принимать заявки по выбранным направлениям"
                };

                Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
                    _this.sending = true;
                    _this.hasError = false;
                    _this.message = "";
                    var fd = new FormData();
                    fd.append('NEW_DAYS_COUNT', _this.form.daysCount);
                    fd.append('PROFILE_WORK', _this.form.selectedSections);
                    fd.append('sessid', _this.form.sessid);

                    try {
                        axios.post('/youdo/api/update_subscribe.php', fd).then(function (response) {
                            var data = response.data;

                            if (data && !data.hasError) {
                                app.onCustomEvent('OnIblock');
                                app.closeController();
                                app.onCustomEvent('OnAfterUserLogin');
                                app.reload();
                            } else {
                                _this.hasError = true;
                                _this.message = data.msg;
                                _this.sending = false;
                            }
                        });
                    } catch (e) {
                        console.log(e);
                    }
                });

            },
            getDirections: function getDirections() {
                var _this = this;

                axios.get('/youdo/api/get_data.php', {
                    params: {
                        type: 'direction'
                    }
                }).then(function (response) {
                    _this.directions = response.data;
                });
            },
            decrementDays: function decrementDays() {
                if (this.form.daysCount > 0) {
                    this.form.daysCount--;
                }
            },
            incrementDays: function incrementDays() {
                if (this.form.daysCount + this.currentDaysCount < 15) {
                    this.form.daysCount++;
                }
            },
            goToPayPage: function goToPayPage() {
                app.loadPageBlank({
                    url: "/youdo/personal/index.php?bitrix_include_areas=N&ITEM=account",
                    title: "Баланс"
                });
            },
            // Функция геренрации древовидного массива
            list_to_tree: function list_to_tree(list) {
                var map = {},
                    node,
                    roots = [],
                    i;

                for (i = 0; i < list.length; i += 1) {
                    map[list[i].ID] = i; // initialize the map

                    list[i].CHILDREN = []; // initialize the children
                }

                for (i = 0; i < list.length; i += 1) {
                    node = list[i];

                    if (node.IBLOCK_SECTION_ID) {
                        list[map[node.IBLOCK_SECTION_ID]].CHILDREN.push({
                            ID: node.ID,
                            NAME: node.NAME,
                            PRICE: node.FEE_PRICE,
                            IS_PAYD: node.IS_PAYD
                        });
                    } else {
                        roots.push(node);
                    }
                }

                for (i = 0; i < roots.length; i += 1) {
                    if (!roots[i].CHILDREN.length) {
                        roots[i].CHILDREN = null;
                    }
                }

                return roots;
            }
        }
    });
</script>