<?

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class AppSubscribe extends CBitrixComponent {

	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	public function executeComponent() {
		global $USER, $APPLICATION;

		Loader::includeModule('mlab.appforsale');


		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}


		// Получаем список видов деятельности на которые подписан профиль мастера
		Loader::includeModule('iblock');
		$arFreeSections        = array();
		$arPaidSections        = array();
		$arProfileDirectionIds = [];
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, 'CREATED_BY' => $USER->GetId()));

		if ($arProfile = $rsProfile->getNext()) {
			$this->arResult["PROFILE_ACTIVE"] = $arProfile['ACTIVE'] == 'Y';

			$dbPropsUserDirections = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'PROFILE_WORK')
			);
			while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
				$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
			}

			$arFilter       = array(
				"IBLOCK_ID" => self::TASKS_IBLOCK_ID,
				"ID"        => $arProfileDirectionIds,
			);
			$rsTaskSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "UF_FEE_PRICE", "IBLOCK_SECTION_ID"));

			while ($arTaskSections = $rsTaskSections->getNext()) {
				// Проверяем, платный ли родитель
				$paydParent = false;
				if ( ! empty($arTaskSections['IBLOCK_SECTION_ID'])) {
					$rsParentSections = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID, "ID" => $arTaskSections['IBLOCK_SECTION_ID']), false, array("ID", "UF_FEE_PRICE"));
					while ($arParentSection = $rsParentSections->GetNext()) {
						$paydParent = intval($arParentSection['UF_FEE_PRICE']) > 0;
					}
				}

				if ($arTaskSections["UF_FEE_PRICE"] > 0 || $paydParent) {
					$arPaidSections[] = $arTaskSections['ID'];
				} else {
					$arFreeSections[] = $arTaskSections['ID'];
				}
			}
		}

		$this->arResult["FREE_SECTIONS"] = $arFreeSections;
		$this->arResult["PAID_SECTIONS"] = $arPaidSections;

		$this->includeComponentTemplate();

	}
}

?>