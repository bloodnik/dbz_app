<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PARTNERS_IBLOCK_ID", 2);
define("CITIES_IBLOCK_ID", 1);
define("TASK_IBLOCK_ID", 4);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$response = array(
	'status'  => true,
	'message' => "",
	'result'  => array(),
);

global $USER;


if(!$USER->IsAuthorized()) {
	$response["status"] = false;
	$response["message"] = "Необходима авторизация";
	echo json_encode($response);
	return false;
}


$request = Application::getInstance()->getContext()->getRequest();
$requestType = $request->get('type');

if (isset($requestType) && strlen($requestType) > 0) {


	// Количество дней подписки
	if ($requestType === 'daysCount') {
		$response["result"] = intval(CAppforsaleSubscription::getUserSubscriptionDays($USER->GetId()));
		echo json_encode($response);
	}

}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

