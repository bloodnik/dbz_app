<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

?>


<div class="w-100" id="afs-tariffs" v-cloak>
	<br>
	<h5>Тарифы</h5>
	<br>

	<b-form ref="form">
		<b-card-group deck class="mb-3">
			<b-card bg-variant="light" header="Активный тариф">
				<div class="row">
					<div class="col" v-if="currentTariff.UF_ACTIVE">
						<p>{{ currentTariff.TARIFF_NAME }}</p>
						<p v-html="tariffTaskCount"></p>
						<p v-html="tariffActiveTo"></p>

						<b-button v-if="currentTariff.UF_TARIFF_TYPE > 1" @click="deactivate()" block variant="warning" size="sm">Деактивировать тариф</b-button>
					</div>

					<div class="col" v-else>
						<p>Тариф не выбран</p>
					</div>
				</div>
			</b-card>
		</b-card-group>

		<b-alert class="mt-3" show variant="danger" v-if="hasError">{{ message }}</b-alert>


		<b-card v-if="!isTariffActive || isTariffUnused || currentTariff.TARIFF_TYPE == 232" no-body>
			<b-alert show variant="info">
				"Тариф "Стандарт" не требует активации и доступен по умолчанию для заявок с суммой работ не дороже 5000 руб."
			</b-alert>
			<div v-for="(tariffType, tariffKey) in availableTariffs" :key="tariffKey">
				<b-card v-for="tariff in tariffType.ITEMS" :ref="'tariff_' + tariff.ID" :key="tariff.ID" no-body class="mb-1">
					<template v-if="tariffType.ID == 233">
						<b-card-header header-tag="header" class="p-1" role="tab">
							<b-button block v-b-toggle="['accordion-' + tariff.ID]" variant="secondary">Пакет {{tariff.UF_NAME}}, цена заявки {{ parseInt(tariff.UF_PRICE, 10) / parseInt(tariff.UF_TASK_COUNT,
								10) }} руб.
							</b-button>
						</b-card-header>
						<b-collapse :id="'accordion-' + tariff.ID" :visible="isTariffVisible(tariff.ID)" accordion="my-accordion" role="tabpanel">
							<b-card-body>
								<b-card-text v-html="unescape_html(tariff.UF_DESCRIPTION)"></b-card-text>
								<b-card-text>
									<b-button @click="setTariff(tariff)" type="button" :disabled="sending" block variant="success">Применить тариф {{ tariff.UF_NAME }}</b-button>
								</b-card-text>
							</b-card-body>
						</b-collapse>
					</template>
				</b-card>
			</div>
		</b-card>

	</b-form>
</div>

<script>
	new Vue({
		el: '#afs-tariffs',
		computed: {

			tariffId() {
				return new URL(location.href).searchParams.get('tariffId') || "2";
			},

			backurl() {
				return new URL(location.href).searchParams.get('backurl') || "";
			},

			tariffTaskCount() {
				var message = "";

				if (this.currentTariff.UF_TASK_COUNT >= 0) {
					message += "Осталось " + this.currentTariff.UF_TASK_COUNT + " заявок";
				} else {
					message = '';
				}

				return message;
			},
			tariffActiveTo() {
				var message = "";

				if (this.currentTariff.UF_ACTIVE_TO) {
					message += "Активен до <strong>" + this.currentTariff.UF_ACTIVE_TO + "</strong> (GMT+5)";
				} else {
					message += '';
				}

				return message;
			},
		},
		data: function data() {
			return {
				hasError: false,
				message: "",
				sending: false,

				query: null,
				value: null,
				isTariffActive: !!<?=$arResult['IS_TARIFF_ACTIVE'] ? 1 : 0?>,
				isTariffUnused: !!<?=$arResult["IS_TARIFF_UNUSED"] ? 1 : 0?>,
				profileIsActive: !!<?=$arResult['PROFILE_ACTIVE'] ? 1 : 0?>,
				currentTariff: <?=CUtil::PhpToJSObject($arResult["CURRENT_TARIFF"])?>,
				availableTariffs: <?=CUtil::PhpToJSObject($arResult["AVAILABLE_TARIFFS"])?>,
				form: {
					sessid: "<?=bitrix_sessid()?>"
				},
				confirmOptions: {
					html: true
				},
				ajaxUrl: "<?=$componentPath?>/ajax.php",
				dactivateAjaxUrl: "/local/components/siril/app.tariff.deactivate/ajax.php"

			};
		},
		mounted: function mounted() {
			var _this = this;

			if (new URL(location.href).searchParams.get('tariffId')) {
				setTimeout(function () {
					_this.scrollMeTo('tariff_' + _this.tariffId);
				}, 500)
			}

		},
		methods: {
			setTariff: function setTariff(tariff) {
				var _this = this;

				let confirmMessage = {
					html: true,
					title: 'Применить изменения?',
					body: "Применить тариф " + tariff.UF_NAME + "<br><br> Количество заявок в тарифе: " + tariff.UF_TASK_COUNT + "<br> Тариф действует: " + tariff.UF_ACTIVE_DAYS + " дн. <br> Цена тарифа: " + tariff.UF_PRICE + " руб. <br> Цена заявки по" +
						" тарифу: " + parseInt(tariff.UF_PRICE, 10) / parseInt(tariff.UF_TASK_COUNT, 10) + " руб. (без дополнительных комиссий) <br>" +
						"Доступны заявки " + (!!tariff.UF_COST_FROM ? "от " + tariff.UF_COST_FROM + " руб." : "") + (!!tariff.UF_COST_TO ? "до " + tariff.UF_COST_TO + " руб." : "")
				};

				Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
					_this.sending = true;
					_this.hasError = false;
					_this.message = "";
					var fd = new FormData();
					fd.append('sessid', _this.form.sessid);
					fd.append('type', "setTariff");
					fd.append('tariffId', tariff.ID);

					try {
						axios.post(_this.ajaxUrl, fd).then(function (response) {
							var data = response.data;

							if (data && !data.hasError) {
								if (_this.backurl) {
									app.loadPageStart({url: _this.backurl, title: "Заявка"});
								} else {
									app.reload();
								}
								_this.sending = false;
							} else {
								_this.hasError = true;
								_this.message = data.msg;
								_this.sending = false;
							}
						});
					} catch (e) {
						console.log(e);
						_this.sending = false;
						_this.hasError = true;
						_this.message = e;
					}
				});
			},

			deactivate: function deactivate() {
				var _this = this;

				console.log(this.currentTariff);

				var refundSumm = (parseInt(_this.currentTariff.TARIFF_PRICE, 10) / parseInt(_this.currentTariff.TARIFF_TASK_COUNT, 10)) * parseInt(_this.currentTariff.UF_TASK_COUNT, 10);


				let confirmMessage = {
					html: true,
					title: 'Деактивировать тариф?',
					body: "Деактивация тарифа будет произведена с удержанием 50% от суммы стоимостей оставшихся в тарифе заявок. <br> Возврат будет осуществлен в размере " + refundSumm / 2 + " бонусов за неиспользованных заявки - "  + _this
							.currentTariff.UF_TASK_COUNT +" шт" +
						". ?"
				};

				Vue.dialog.confirm(confirmMessage, _this.confirmOptions).then(function (dialog) {
					_this.sending = true;
					_this.hasError = false;
					_this.message = "";
					var fd = new FormData();
					fd.append('sessid', _this.form.sessid);
					fd.append('type', "deactivate");
					fd.append('tariffId', _this.currentTariff.ID);

					try {
						axios.post(_this.dactivateAjaxUrl, fd).then(function (response) {
							var data = response.data;
							console.log(data);
							if (data && data.status) {
								_this.sending = false;
								app.reload();
							} else {
								_this.message = data.message;
								_this.hasError = true;
								_this.sending = false;
							}
						});
					} catch (e) {
						console.log(e);
						_this.sending = false;
						_this.hasError = false;
						_this.message = e;
					}
				});
			},

			isTariffVisible(tariffId) {
				return this.tariffId === tariffId;
			},

			goToPayPage: function goToPayPage() {
				app.loadPageBlank({
					url: "/youdo/personal/index.php?bitrix_include_areas=N&ITEM=account",
					title: "Баланс"
				});
			},

			scrollMeTo(refName) {
				var element = this.$refs[refName];

				this.$scrollTo(element[0], 200, {
					container: 'body',
					easing: 'ease-in',
					offset: -60,
					force: true,
				});
			},

			unescape_html(string) {
				var optTemp = 0
				var i = 0
				var noquotes = false

				var quoteStyle = 2
				string = string.toString()
					.replace(/&lt;/g, '<')
					.replace(/&gt;/g, '>')
				var OPTS = {
					'ENT_NOQUOTES': 0,
					'ENT_HTML_QUOTE_SINGLE': 1,
					'ENT_HTML_QUOTE_DOUBLE': 2,
					'ENT_COMPAT': 2,
					'ENT_QUOTES': 3,
					'ENT_IGNORE': 4
				}
				if (quoteStyle === 0) {
					noquotes = true
				}
				if (typeof quoteStyle !== 'number') {
					// Allow for a single string or an array of string flags
					quoteStyle = [].concat(quoteStyle)
					for (i = 0; i < quoteStyle.length; i++) {
						// Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
						if (OPTS[quoteStyle[i]] === 0) {
							noquotes = true
						} else if (OPTS[quoteStyle[i]]) {
							optTemp = optTemp | OPTS[quoteStyle[i]]
						}
					}
					quoteStyle = optTemp
				}
				if (quoteStyle & OPTS.ENT_HTML_QUOTE_SINGLE) {
					// PHP doesn't currently escape if more than one 0, but it should:
					string = string.replace(/&#0*39;/g, "'")
					// This would also be useful here, but not a part of PHP:
					// string = string.replace(/&apos;|&#x0*27;/g, "'");
				}
				if (!noquotes) {
					string = string.replace(/&quot;/g, '"')
				}
				// Put this in last place to avoid escape being double-decoded
				string = string.replace(/&amp;/g, '&')

				return string
			}
		}
	});
</script>