<?

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

class AppTariffs extends CBitrixComponent {

	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	public function executeComponent() {
		global $USER, $APPLICATION;

		Loader::includeModule('mlab.appforsale');


		if ( ! $USER->IsAuthorized()) {
			CAppforsale::AuthForm();
		}


		$availableTariffOptions = CAppforsaleTariffOption::getAvailableOptions();


		$this->arResult["AVAILABLE_TARIFFS"] = [];
		foreach ($availableTariffOptions as $availableTariffOption) {
			$this->arResult["AVAILABLE_TARIFFS"][$availableTariffOption["UF_TYPE"]]["NAME"] = $availableTariffOption["TARIFF_NAME"];
			$this->arResult["AVAILABLE_TARIFFS"][$availableTariffOption["UF_TYPE"]]["ID"] = $availableTariffOption["UF_TYPE"];
			$this->arResult["AVAILABLE_TARIFFS"][$availableTariffOption["UF_TYPE"]]["ITEMS"][] = $availableTariffOption;
		}

		$tariff                             = new CAppforsaleTariff($USER->GetID());
		$this->arResult["IS_TARIFF_ACTIVE"] = $tariff->isTariffActive();
		$this->arResult["CURRENT_TARIFF"]   = $tariff->getUserTariff();

		$this->arResult["IS_TARIFF_UNUSED"] = $this->arResult["IS_TARIFF_ACTIVE"] && $tariff->isUnused();

		$this->includeComponentTemplate();

	}
}

?>