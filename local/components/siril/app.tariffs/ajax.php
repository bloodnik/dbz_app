<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PARTNERS_IBLOCK_ID", 2);
define("CITIES_IBLOCK_ID", 1);
define("TASK_IBLOCK_ID", 4);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$response = array(
	'status'  => true,
	'message' => "",
	'result'  => array(),
);

global $USER;


if ( ! check_bitrix_sessid()) {
	return;
}


if ( ! $USER->IsAuthorized()) {
	$response["status"]  = false;
	$response["message"] = "Необходима авторизация";
	echo json_encode($response);

	return false;
}


$request     = Application::getInstance()->getContext()->getRequest();
$requestType = $request->getPost('type');

if (isset($requestType) && strlen($requestType) > 0) {


	//
	if ($requestType === 'setTariff') {
		$tariffId = $request->getPost('tariffId');

		if (empty($tariffId)) {
			$arResult['msg']      = "Не выбран тариф";
			$arResult['hasError'] = true;
			echo json_encode($arResult);
			die();
		}

		$tariffOption = CAppforsaleTariffOption::getById($tariffId);

		$userTariff = new CAppforsaleTariff($USER->GetID());
		$userTariff->setOption($tariffOption);

		// Оплачиваем
		try {
			$userTariff->pay();
		} catch (\Bitrix\Main\SystemException $exception) {
			$arResult['msg']      = $exception->getMessage();
			$arResult['hasError'] = true;
			echo json_encode($arResult);
			die();
		}

		if ($userTariff->setUserTariff()) {
			$response["result"] = "Тариф успешно применен";
		}

		echo json_encode($response);
	}

}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

