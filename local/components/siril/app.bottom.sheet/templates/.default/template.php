<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>

<div id="afsBottomSheet">
	<afs-bottom-sheet ref="bottomSheet"></afs-bottom-sheet>
</div>

<script>
    Vue.component('afs-bottom-sheet', {
        data: function () {
            return {
                sheetState: false,
                links: [
                    {href: 'https://wa.me/79871051800', text: 'Сложный вопрос и корректировка автосписания', icon: 'whatsapp', blank: false},
                    {href: 'tel:+79174201418', text: '+7 (917) 420-14-18', icon: 'phone', blank: false},
                    {href: '/youdo/app_intro/', text: 'Справка', icon: 'question', blank: false},
                    {href: 'https://t.me/dombezzabot_news', text: 'Скидки на инструмент и советы по ремонту', icon: 'telegram', blank: false},
                ]
            }
        },

        methods: {
            fabClickHandler: function () {
                this.sheetState = true;
            },
            closeBtnHandler: function () {
                this.sheetState = false;
            },

            goToIntro: function () {
                app.loadPageBlank({url: '/youdo/app_intro/', title: 'Справка для мастеров'});
            }
        },

        template: "\n      <div class=\"afs-bottom-sheet\">\n        <button class=\"afs-fab__green question-btn\" @click=\"fabClickHandler()\" type=\"button\"><i class=\"fa fa-question\"></i></button>\n\n        <transition name=\"slide\">\n            <div v-if=\"sheetState\" class=\"afs-bottom-sheet__panel\">\n                <div class=\"afs-bottom-sheet__header\">\n                    \u0422\u0435\u0445\u043F\u043E\u0434\u0434\u0435\u0440\u0436\u043A\u0430\n                    <a class=\"afs-btn__close\" href=\"javascript:void(0)\" @click.prevent=\"closeBtnHandler()\"></a>\n                </div>\n                <ul>\n                    <li v-for=\"(link, index) in links\" :key=\"link.icon + index\"><a :href=\"link.href\" :target=\"link.blank ? '_blank' : ''\"><i :class=\"'fa-' + link.icon\" class=\"fa fa-2x\"></i> {{link.text}}</a></li>\n<!--                    <li><a href=\"#\" @click=\"goToIntro()\"><i class=\"fa fa-question fa-2x\"></i> \u0421\u043F\u0440\u0430\u0432\u043A\u0430</a></li>                    -->\n                </ul>\n            </div>\n        </transition>\n      </div>\n    "
    })


    var bottomSheet = new Vue({
        el: '#afsBottomSheet',
    })
</script>
