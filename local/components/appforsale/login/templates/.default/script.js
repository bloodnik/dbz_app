/* appforsale:login */
(function (window) {

    if (!!window.JCLogin) {
        return;
    }

    window.JCLogin = function (arParams) {
        this.mask = null;
        this.user_id = 0;
        this.visual = {
            PHONE: '',
            CONFIRM: '',
            CHECK_POLICY: '',
            TERM_OF_USE: '',
            CONTRACT: '',
            CONFIRM_CODE_DIV: '',
            CONFIRM_CODE: '',
            CAME: ''
        };

        this.mess = {
            INVALID_PHONE: '',
            ENTER: '',
            GET_CODE: ''
        }

        this.obPhone = null;
        this.obConfirm = null;
        this.obCheckPolicy = null;
        this.obTermsOfUse = null;
        this.obContract = null;

        this.obConfirmCodeDiv = null;
        this.obConfirmCode = null;

        this.obCame = null;

        this.obConfirmer = null;

        this.enabled = {props: {disabled: false}};
        this.disabled = {props: {disabled: true}};

        if ('object' === typeof arParams) {
            this.mask = arParams.mask;
            this.visual = arParams.VISUAL;
            this.mess = arParams.MESS;
        }

        BX.ready(BX.delegate(this.Init, this));
    }

    window.JCLogin.prototype.Init = function () {
        this.obPhone = BX(this.visual.PHONE);
        this.obConfirm = BX(this.visual.CONFIRM);
        this.obCheckPolicy = BX(this.visual.CHECK_POLICY);
        this.obTermsOfUse = BX(this.visual.TERM_OF_USE);
        this.obContract = BX(this.visual.CONTRACT);

        this.obConfirmCodeDiv = BX(this.visual.CONFIRM_CODE_DIV);
        this.obConfirmCode = BX(this.visual.CONFIRM_CODE);

        this.obCame = BX(this.visual.CAME);

        if (!!this.obPhone) {
            $(".phone").mask("+7-(999)-999-99-99");
        }

        if (!!this.obConfirmCode) {
            BX.bind(this.obConfirmCode, 'keydown', BX.delegate(this.keydown_code, this));
        }

        if (!!this.obConfirm) {
            BX.bind(this.obConfirm, 'click', BX.delegate(this.Confirm, this));
        }

        if (!!this.obCame) {
            BX.bind(this.obCame, 'click', BX.delegate(this.Came, this));
        }

        if (!!this.obCheckPolicy) {
            BX.bind(this.obCheckPolicy, 'change', function (e) {
                if (e.target.checked) {
                    e.target.classList.remove('is-invalid')
                }
            });
        }

        if (!!this.obTermsOfUse) {
            BX.bind(this.obTermsOfUse, 'change', function (e) {
                if (e.target.checked) {
                    e.target.classList.remove('is-invalid')
                }
            });
        }

        if (!!this.obContract) {
            BX.bind(this.obContract, 'change', function (e) {
                if (e.target.checked) {
                    e.target.classList.remove('is-invalid')
                }
            });
        }
        app.hideProgress();
    }

    window.JCLogin.prototype.keydown_code = function (e) {
        var target = BX.proxy_context;
        var k = e.which || e.keyCode
        var str = target.value;
        if (k != 8 && k != 229 && str.length >= 5) {
            e.preventDefault();
        }
    }

    window.JCLogin.prototype.ShowProgress = function () {
        BX.adjust(this.obPhone, this.disabled);
        BX.adjust(this.obConfirm, this.disabled);
    }

    window.JCLogin.prototype.HideProgress = function () {
        BX.adjust(this.obPhone, this.enabled);
        BX.adjust(this.obConfirm, this.enabled);
    }

    window.JCLogin.prototype.Came = function () {
        BX.adjust(this.obPhone, this.enabled);
        this.user_id = 0;
        BX.adjust(this.obConfirmCodeDiv, {style: {display: 'none'}});

        BX.adjust(this.obConfirm, {html: this.mess.GET_CODE});

        BX.unbindAll(this.obConfirm);
        BX.bind(this.obConfirm, 'click', BX.delegate(this.Confirm, this));
    }

    window.JCLogin.prototype.Confirm = function () {
        this.obCheckPolicy.classList.remove('is-invalid');
        this.obTermsOfUse.classList.remove('is-invalid');
        this.obContract.classList.remove('is-invalid');
        if (!this.obCheckPolicy.checked) {
            this.obCheckPolicy.classList.add('is-invalid')
            return;
        }
        if (!this.obTermsOfUse.checked) {
            this.obTermsOfUse.classList.add('is-invalid')
            return;
        }
        if (!this.obContract.checked) {
            this.obContract.classList.add('is-invalid')
            return;
        }


        phone = this.obPhone.value.replace(/\D+/g, "");

        if (phone[0] == 8)
            phone = '7' + phone.substr(1);

        if (!(new RegExp(this.mask).test(phone)))
            return alert(this.mess.INVALID_PHONE);


//	if (phone.length != 11 || (phone[0] != 7 && phone[0] != 8))
        //	return alert(this.mess.INVALID_PHONE);


        this.ShowProgress();

        BX.ajax({
            url: location.href,
            data: {
                ajax_call: 'y',
                action: 'register',
                phone: phone
            },
            method: 'POST',
            dataType: 'json',
            processData: true,
            scriptsRunFirst: true,
            emulateOnload: false,
            start: true,
            cache: false,
            onsuccess: BX.delegate(function (data) {
                if (data.response) {
                    this.user_id = data.response;
                    this.Fade(this.obConfirmCodeDiv);

                    BX.adjust(this.obConfirm, {props: {disabled: false}, html: this.mess.ENTER});

                    BX.unbindAll(this.obConfirm);
                    BX.bind(this.obConfirm, 'click', BX.delegate(this.Code, this));
                } else if (data.error) {
                    this.HideProgress();
                    alert(data.error.error_msg);
                }
            }, this)
        });
    }

    window.JCLogin.prototype.Code = function () {
        BX.adjust(this.obConfirm, this.disabled);
        BX.adjust(this.obConfirmCode, this.disabled);

        BX.ajax({
            url: location.href,
            data: {
                ajax_call: 'y',
                action: 'confirm',
                confirm_user_id: this.user_id,
                confirm_code: this.obConfirmCode.value
            },
            method: 'POST',
            dataType: 'json',
            processData: true,
            scriptsRunFirst: true,
            emulateOnload: false,
            start: true,
            cache: false,
            onsuccess: BX.delegate(function (data) {
                if (data.response) {
                    app.onCustomEvent("OnAfterUserLogin");
                    if (/\/youdo\/login\/$/.test(location.href))
                        location.href = '/youdo/';
                    else
                        app.reload();
                } else if (data.error) {
                    BX.adjust(this.obConfirm, this.enabled);
                    BX.adjust(this.obConfirmCode, this.enabled);
                    alert(data.error.error_msg);
                }
            }, this)
        });
    }

    window.JCLogin.prototype.Fade = function (elem, t, f) {
        var fps = f || 50;
        var time = t || 500;
        var steps = time / fps;

        elem.style.display = 'block';
        var d = elem.offsetHeight / steps;
        elem.style.height = 0;

        var h = 0;
        var timer = setInterval(function () {
            h += d;
            elem.style.height = h + 'px';
            steps--;
            if (steps == 0)
                clearInterval(timer);
        }, (1000 / fps));
    }

})(window);