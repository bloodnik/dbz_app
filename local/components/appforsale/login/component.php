<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if ($_REQUEST['ajax_call'] == 'y') {
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();

	if ($_REQUEST['action'] == 'register') {
		$sendSms      = false;
		$phone        = $_POST['phone'];
		$login        = preg_replace('/[^0-9]/', '', $_POST['phone']);
		$confirm_code = rand(10000, 99999);

		if ($login == COption::GetOptionString('mlab.appforsale', 'login_approved', '79270270273')) {
			$confirm_code = '12345';
		}

		$url_sms_service = COption::GetOptionString("mlab.appforsale", "url_sms_service", "");
		if ($url_sms_service != '') {
			$url_sms_service_replace = str_replace(
				array(
					"#PHONE#",
					"#CODE#",
					//" "
				),
				array(
					$login,
					$confirm_code,
					//"%20"
				),
				$url_sms_service
			);

			if ($url_sms_service != $url_sms_service_replace) {
				$url_sms_service = $url_sms_service_replace;
			} else {
				$url_sms_service = "";
			}

		}

		if ($url_sms_service == '') {
			$confirm_code = "12345";
		}

		$rsUser = $USER->GetList(
			($by = "id"),
			($order = "asc"),
			array(
				"LOGIN" => $login,
				"LID"   => SITE_ID
			),
			array(
				"FIELD" => array(
					"ID"
				)
			)
		);
		if ($arUser = $rsUser->Fetch()) //обновляем пользователя
		{
			if ($arUser['ACTIVE'] != 'Y' && $arUser['CONFIRM_CODE'] == '') {
				$arResult = array("error" => array("error_msg" => GetMessage('INACTIVE')));
			} else {
				if ($USER->Update(
						$arUser['ID'],
						array(
							"CONFIRM_CODE" => $confirm_code
						)
					) === true) {
					$sendSms  = true;
					$arResult = array("response" => (int) $arUser['ID']);
				} else {
					$arResult = array("error" => array("error_msg" => $USER->LAST_ERROR));
				}
			}
		} else //добавляем пользователя
		{
			$arFields = array(
				"LOGIN"           => $login,
				"ACTIVE"          => "N",
				"LID"             => SITE_ID,
				"PERSONAL_MOBILE" => $phone,
				"CONFIRM_CODE"    => $confirm_code
			);

			if ((COption::GetOptionString("main", "new_user_email_required", "Y") <> "N")) {
				$arFields['EMAIL'] = $arFields['LOGIN'] . '@' . $_SERVER['SERVER_NAME'];
			}

			$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
			if ($def_group != "") {
				$arFields["GROUP_ID"] = explode(",", $def_group);
				$arPolicy             = $USER->GetGroupPolicy($arFields["GROUP_ID"]);
			} else {
				$arPolicy = $USER->GetGroupPolicy(array());
			}

			$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
			if ($password_min_length <= 0) {
				$password_min_length = 6;
			}
			$password_chars = array(
				"abcdefghijklnmopqrstuvwxyz",
				"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
				"0123456789"
			);
			if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y") {
				$password_chars[] = ",.<>/?;:'\"[]{}\\|`~!@#\$%^&*()-_+=";
			}
			$arFields["PASSWORD"] = $arFields["CONFIRM_PASSWORD"] = randString($password_min_length, $password_chars);

			$ID = $USER->Add($arFields);
			if (intval($ID) > 0) {
				$sendSms  = true;
				$arResult = array("response" => intval($ID));

				$event = new CEvent;
				$event->SendImmediate('NEW_USER', SITE_ID, array(
					'USER_ID'     => $ID,
					'LOGIN'       => $arFields['LOGIN'],
					'ACTIVE'      => 'N',
					'SITE_ID'     => SITE_ID,
					'LANGUAGE_ID' => LANGUAGE_ID,
					"USER_IP"     => $_SERVER["REMOTE_ADDR"],
					"USER_HOST"   => @gethostbyaddr($_SERVER["REMOTE_ADDR"]),
				));
			} else {
				$arResult = array("error" => array("error_msg" => $USER->LAST_ERROR));
			}
		}

		if ($sendSms && $url_sms_service != '') {
			$request = new Bitrix\Main\Web\HttpClient();
			$request->get($url_sms_service);
		}
	}

	if ($_REQUEST['action'] == 'confirm') {
		$_POST['confirm_user_id'] = intval($_POST['confirm_user_id']);
		$_POST['confirm_code']    = htmlspecialcharsbx($_POST['confirm_code']);

		$rsUser = CUser::GetByID($_POST['confirm_user_id']);
		if ($arUser = $rsUser->GetNext()) {
			if ($_POST['confirm_code'] !== $arUser["CONFIRM_CODE"]) {
				$arResult = array("error" => array("error_msg" => GetMessage('INVALID_CODE')));
			} else {
				if ($USER->Update($arUser['ID'], array("ACTIVE" => "Y", "CONFIRM_CODE" => "")) === true) {
					$user = new CUser;
					$user->Authorize($arUser['ID'], true);
					$arResult = array("response" => 1);
				} else {
					$arResult = array("error" => array("error_msg" => GetMessage('INVALID_CODE')));
				}
			}
		} else {
			$arResult = array("error" => array("error_msg" => GetMessage('INVALID_CODE')));
		}
	}
	echo \Bitrix\Main\Web\Json::encode($arResult);
	require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_after.php");
	die();
}

$this->IncludeComponentTemplate();
?>