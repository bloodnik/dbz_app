<?
$MESS['EXIT'] = 'Выход';
$MESS['EDIT'] = 'Редактировать';
$MESS['PROFILE'] = 'Профиль';
$MESS['NOTICE'] = 'Уведомления';
$MESS['AGREEMENT'] = 'Пользовательское соглашение';
$MESS['BALANCE'] = 'Баланс';
$MESS['SUBSCRIPTION'] = 'Бонусы/Тарифы';
$MESS['REGISTER'] = 'Профиль мастера';
$MESS['CONTRACTOR'] = 'Исполнитель';
$MESS['PRIVACY_POLICY'] = 'Политика конфиденциальности';
$MESS['TERMS_OF_USE'] = 'Условия использования';
?>