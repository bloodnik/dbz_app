/* appforsale:main.personal */
(function (window) {
	
	if (!!window.JCPersonal)
	{
		return;
	}

	window.JCPersonal = function (arParams)
	{
		this.templateFolder = '';
		this.isContractor = false;
		this.ajax = false;
		this.visual = {
			HEADER: '',
			SWITCH: '',
			PROGRESS: ''
		};

		this.obHeader = null;
		this.obSwitch = null;
		this.obProgress = null;

		if ('object' === typeof arParams)
		{
			this.templateFolder = arParams.templateFolder;
			this.isContractor = arParams.isContractor;
			this.ajax = arParams.AJAX;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	

	window.JCPersonal.prototype.Init = function()
	{
		this.obHeader = BX(this.visual.HEADER);
		this.obSwitch = BX(this.visual.SWITCH);
		this.obProgress = BX(this.visual.PROGRESS);
		
		app.hideProgress();
		if (this.ajax == false)
		{
			BX.addCustomEvent("OnAfterUserUpdate", BX.delegate(this.updateUser, this));
		}
		
		if (!!this.obSwitch)
		{
			BX.bind(this.obSwitch, 'click', BX.delegate(this.Switch, this));
		}
	}
	
	window.JCPersonal.prototype.Switch = function()
	{
		this.ShowProgress();
		BX.ajax({
	        url: location.href,
	        data: {
				AJAX_CALL: 'Y',
				contractor: (this.isContractor ? 'N' : 'Y')
			},
	        method: 'POST',
	        dataType: 'json',
	        onsuccess: BX.delegate(this.onsuccess, this)
	    });
		event.stopPropagation();
	}
	
	window.JCPersonal.prototype.onsuccess = function(data)
	{
		if (data.response)
		{
			if (data.response == 1)
			{
				this.isContractor = true;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_on.png"}});
			}
			else
			{
				this.isContractor = false;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_off.png"}});
			}
			app.onCustomEvent("OnAfterUserUpdate");
		}
		else if (data.error)
		{
			alert(data.error.error_msg);
		}
		this.HideProgress();
	}
	
	window.JCPersonal.prototype.ShowProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "none"}});
		BX.adjust(this.obProgress, {style: {display: "block"}});
	}
	
	window.JCPersonal.prototype.HideProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "block"}});
		BX.adjust(this.obProgress, {style: {display: "none"}});
	}
	
	window.JCPersonal.prototype.updateUser = function()
	{
	//	if (BX.isNodeHidden(BX('menu')) || BX.isNodeHidden(BX('menu2')))
	//	{
			BX.ajax.Setup(
				{
					denyShowWait: true
				},
				true
			);  
			BX.ajax.insertToNode(
				location.href,
				document.body
			);
	//	}
	}

})(window);

function Logout(obj)
{
	BX.ajax({
        url: "/bitrix/components/appforsale/setting.main/templates/.default/logout.php",
        timeout: 15,
        async: true,
        processData: true,
        scriptsRunFirst: true,
        emulateOnload: false,
        start: true,
        cache: false,
        onsuccess: function(data){
        	app.onCustomEvent("OnAfterUserLogout");
        	app.loadPageStart({url: "/youdo/", title: ""});
        },
        onfailure: function() {
        	alert("������");
        }
    });
	return false;
}



function uploadfile2(e)
{
	result = e;
	var canvas = BX("canvas");
	var ctx = canvas.getContext('2d'); 
	var orientation = parse_exif(result, true);
	var pic = new Image();
	pic.onload = function() { 
		w=256; h=256;
		canvas.width=w;
		canvas.height=h;
		switch(orientation)	{
		case 8:
			ctx.translate(0,canvas.height);
			ctx.rotate(-90*Math.PI/180);
			break;
		case 3:
			ctx.translate(canvas.width,canvas.height);
			ctx.rotate(180*Math.PI/180);
			break;
		case 6:
			ctx.translate(canvas.width,0);
			ctx.rotate(90*Math.PI/180);
			break;
	} 
		if ((pic.width/(pic.height/canvas.height))>=canvas.width) { ctx.drawImage(pic, (pic.width/(pic.height/canvas.height)-canvas.width)/2, 0, canvas.width*(pic.height/canvas.height), pic.height, 0, 0, canvas.width, canvas.height); }
		else { ctx.drawImage(pic, 0, (pic.height/(pic.width/canvas.width)-canvas.height)/2, pic.width, canvas.height*(pic.width/canvas.width), 0, 0, canvas.width, canvas.height); } 

		// ЗАГРУЗКА
		var loader = getXmlHttp()  
		loader.open('POST', '/bitrix/components/appforsale/setting.main/templates/.default/upload.php', true);
		loader.onreadystatechange = function() { 
			if (loader.readyState == 4) {
				if (loader.status == 200) {
					BX.adjust(BX('progress'), {html: " "});
					
					var json = JSON.parse(loader.responseText);
					if (json.response)
					{
						app.onCustomEvent("OnAfterUserUpdate");
					}
					else if (json.error)
					{
						alert(json.error.error_msg);
					}
					
				}
		}};		
		
		loader.upload.onprogress = function(e) {
			if (e.lengthComputable) {  
				var p = e.total / 100;
				BX.adjust(BX('progress'), {html: Math.round(e.loaded / p) + "%"});

			   } 
		}
						
		loader.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');	
		loader.send('file='+BX("canvas").toDataURL('image/jpeg'));	
	};
	pic.src = result;
	
}

function uploadfile(e)
{
	var files = e.files;
	var reader = new FileReader();		
	reader.onload = function (oFREvent) {
		var canvas = BX("canvas");
		var ctx = canvas.getContext('2d'); 
		var orientation = parse_exif(oFREvent.target.result, true);
		var pic = new Image();
		pic.onload = function() { 
			w=256; h=256;
			canvas.width=w;
			canvas.height=h;
			switch(orientation)	{
			case 8:
				ctx.translate(0,canvas.height);
				ctx.rotate(-90*Math.PI/180);
				break;
			case 3:
				ctx.translate(canvas.width,canvas.height);
				ctx.rotate(180*Math.PI/180);
				break;
			case 6:
				ctx.translate(canvas.width,0);
				ctx.rotate(90*Math.PI/180);
				break;
		} 
			if ((pic.width/(pic.height/canvas.height))>=canvas.width) { ctx.drawImage(pic, (pic.width/(pic.height/canvas.height)-canvas.width)/2, 0, canvas.width*(pic.height/canvas.height), pic.height, 0, 0, canvas.width, canvas.height); }
			else { ctx.drawImage(pic, 0, (pic.height/(pic.width/canvas.width)-canvas.height)/2, pic.width, canvas.height*(pic.width/canvas.width), 0, 0, canvas.width, canvas.height); } 

			// ЗАГРУЗКА
			var loader = getXmlHttp()  
			loader.open('POST', '/bitrix/components/appforsale/setting.main/templates/.default/upload.php', true);
			loader.onreadystatechange = function() { 
				if (loader.readyState == 4) {
					if (loader.status == 200) {
						BX.adjust(BX('progress'), {html: " "});
						
						var json = JSON.parse(loader.responseText);
						if (json.response)
						{
							app.onCustomEvent("OnAfterUserUpdate");
						}
						else if (json.error)
						{
							alert(json.error.error_msg);
						}
						
					}
			}};		
			
			loader.upload.onprogress = function(e) {
				if (e.lengthComputable) {  
					var p = e.total / 100;
					BX.adjust(BX('progress'), {html: Math.round(e.loaded / p) + "%"});

				   } 
			}
							
			loader.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');	
			loader.send('file='+BX("canvas").toDataURL('image/jpeg'));	
		};
		pic.src = oFREvent.target.result;
	};
	reader.readAsDataURL(files[0]);	
}

function parse_exif(data,decode) {
	 var image_data=decode?base64decode(data.substr(data.indexOf(',')+1)):data;
	 var section_id, section_length, exif;
	 var offset_tags, num_of_tags, tag_id, tag_len, tag_value;

	 if (image_data.substr(0,2)=='\xFF\xD8') {
	     var idx=2;
	     while (true) {
	         if (idx>=image_data.length) { break; }

	         chr1=image_data.charCodeAt(idx++);
	         chr2=image_data.charCodeAt(idx++);
	         section_id=(chr1<<8)+chr2;

	         chr1=image_data.charCodeAt(idx++);
	         chr2=image_data.charCodeAt(idx++);
	         section_length=(chr1<<8)+chr2;

	         if (section_id==0xFFD8 || section_id==0xFFDB ||
	             section_id==0xFFC4 || section_id==0xFFDD ||
	             section_id==0xFFC0 || section_id==0xFFDA ||
	             section_id==0xFFD9) {
	             break;
	         }

	         if (section_id==0xFFE1) {
	             exif=image_data.substr(idx,(section_length-2));
	             if (exif.substr(0,4)=='Exif') {
	                  if (exif.substr(6,2)=='II') {
	                     this.mask4=function(str) {
	                         var chr1, chr2, chr3, chr4;
	                         chr1=str.charCodeAt(0);
	                         chr2=str.charCodeAt(1);
	                         chr3=str.charCodeAt(2);
	                         chr4=str.charCodeAt(3);
	                         return (chr4<<24)+(chr3<<16)+(chr2<<8)+chr1;
	                     }
	                     this.mask2=function(str) {
	                         var chr1, chr2;
	                         chr1=str.charCodeAt(0);
	                         chr2=str.charCodeAt(1);
	                         return (chr2<<8)+chr1;
	                     }
	                 }
	                 else if (exif.substr(6,2)=='MM') {
	                     this.mask4=function(str) {
	                         var chr1, chr2, chr3, chr4;
	                         chr1=str.charCodeAt(3);
	                         chr2=str.charCodeAt(2);
	                         chr3=str.charCodeAt(1);
	                         chr4=str.charCodeAt(0);
	                         return (chr4<<24)+(chr3<<16)+(chr2<<8)+chr1;
	                     }
	                     this.mask2=function(str) {
	                         var chr1, chr2;
	                         chr1=str.charCodeAt(1);
	                         chr2=str.charCodeAt(0);
	                         return (chr2<<8)+chr1;
	                     }
	                 }

	                 else {
	                     return false;
	                 }

	                 offset_tags=this.mask4(exif.substr(10,4));
	                 num_of_tags=this.mask2(exif.substr(14,2));

	                 if (num_of_tags>0) {
	                     offset=offset_tags+8;

	                     for(var i=0; i<num_of_tags; i++) {

	                         tag_id=this.mask2(exif.substr(offset,2));
	                         tag_len=this.mask4(exif.substr(offset+4,4));
	                         tag_value=this.mask4(exif.substr(offset+8,4));

	                         // Make
	                         if (tag_id==0x010f && tag_len>0 && tag_value>0) {
	                            // write_log('Make',exif.substr(tag_value+6,tag_len));
	                         }
	                         // Model
	                         else if (tag_id==0x0110 && tag_len>0 && tag_value>0) {
	                            // write_log('Model',exif.substr(tag_value+6,tag_len));
	                         }
	                         // ModifyDate
	                         else if (tag_id==0x0132 && tag_len>0 && tag_value>0) {
	                            // write_log('ModifyDate',exif.substr(tag_value+6,tag_len));
	                         }
	                         // Software
	                         else if (tag_id==0x0131 && tag_len>0 && tag_value>0) {
	                            // write_log('Software',exif.substr(tag_value+6,tag_len));
	                         }
	                         // ImageDescription
	                         else if (tag_id==0x010e && tag_len>0 && tag_value>0) {
	                            // write_log('ImageDescription',exif.substr(tag_value+6,tag_len));
	                         }
	                         // Artist
	                         else if (tag_id==0x013b && tag_len>0 && tag_value>0) {
	                            // write_log('Artist',exif.substr(tag_value+6,tag_len));
	                         }
	                         // Orientation
	                         else if (tag_id==0x0112 && tag_len>0) {
	                        	 return this.mask2(exif.substr(offset+8,2));
	                            // write_log('Orientation',this.mask2(exif.substr(offset+8,2)));
	                         }
	                         // Copyright
	                         else if (tag_id==0x8298 && tag_len>0 && tag_value>0) {
	                            // write_log('Copyright',exif.substr(tag_value+6,tag_len));
	                         }
	                         // GPSInfo
	                         else if (tag_id==0x8825 && tag_len>0) {
	                             // РђРґСЂРµСЃ GPSInfo РІ СЃРµРєС†РёРё
	                             var gps_offset=tag_value+6;
	                             // РљРѕР»РёС‡РµСЃС‚РІРѕ GPS-С‚РµРіРѕРІ
	                             var num_of_gps_tags=tag_id=this.mask2(exif.substr(gps_offset,2));

	                             if (num_of_gps_tags>0) {
	                                 gps_offset+=2;

	                                 // РћР±СЂР°Р±РѕС‚РєР° GPS-С‚РµРіРѕРІ
	                                 for (j=0; j<num_of_gps_tags; j++) {
	                                     tag_id=this.mask2(exif.substr(gps_offset,2));
	                                     tag_value=this.mask4(exif.substr(gps_offset+8,4));

	                                     // GPSLatitudeRef РёР»Рё GPSLongitudeRef
	                                     if (tag_id==0x0001 || tag_id==0x0003) {
	                                         if (tag_value!=0) {
	                                             if (tag_id==0x0001) {
	                                              //   write_log('GPSLatitudeRef',exif.substr(gps_offset+8,1));
	                                             }
	                                             else {
	                                               //  write_log('GPSLongitudeRef',exif.substr(gps_offset+8,1));
	                                             }
	                                         }
	                                     }
	                                     // GPSLatitude РёР»Рё GPSLongitude
	                                     else if (tag_id==0x0002 || tag_id==0x0004) {
	                                         var rational_offset=tag_value+6;
	                                         var val1=this.mask4(exif.substr(rational_offset+4*0,4));
	                                         var div1=this.mask4(exif.substr(rational_offset+4*1,4));
	                                         var val2=this.mask4(exif.substr(rational_offset+4*2,4));
	                                         var div2=this.mask4(exif.substr(rational_offset+4*3,4));
	                                         var val3=this.mask4(exif.substr(rational_offset+4*4,4));
	                                         var div3=this.mask4(exif.substr(rational_offset+4*5,4));
	                                         if (div1!=0 && div2!=0 && div3!=0) {
	                                             var tmp=Math.ceil(val1/div1)+'.';
	                                             tmp+=Math.floor((val2/div2/60+val3/div3/3600)*1000000).toString();
	                                             if (tag_id==0x0002) {
	                                               //  write_log('GPSLatitude',tmp);
	                                             }
	                                             else {
	                                               //  write_log('GPSLongitude',tmp);
	                                             }
	                                         }
	                                     }
	                                     gps_offset+=12;
	                                 }
	                             }
	                         }
	                         // ExifOffset
	                         else if (tag_id==0x8769 && tag_len>0) {
	                             // РђРґСЂРµСЃ СЂР°СЃС€РёСЂРµРЅРЅРѕР№ EXIF-СЃРµРєС†РёРё
	                             var exif_offset=tag_value+6;
	                             // РљРѕР»РёС‡РµСЃС‚РІРѕ EXIF-С‚РµРіРѕРІ
	                             var num_of_exif_tags=tag_id=this.mask2(exif.substr(exif_offset,2));

	                             if (num_of_exif_tags>0) {
	                                 exif_offset+=2;

	                                 // РћР±СЂР°Р±РѕС‚РєР° EXIF-С‚РµРіРѕРІ
	                                 for (j=0; j<num_of_exif_tags; j++) {
	                                     // ID С‚РµРіР°, СЂР°Р·РјРµСЂ РґР°РЅРЅС‹С… Рё Р·РЅР°С‡РµРЅРёРµ С‚РµРіР°
	                                     tag_id=this.mask2(exif.substr(exif_offset,2));
	                                     tag_len=this.mask4(exif.substr(exif_offset+4,4));
	                                     tag_value=this.mask4(exif.substr(exif_offset+8,4));

	                                     // DateTimeOriginal
	                                     if (tag_id==0x9003 && tag_len>0 && tag_value>0) {
	                                        // write_log('DateTimeOriginal',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // ExifVersion
	                                     else if (tag_id==0x9000) {
	                                         var tmp=exif.substr(exif_offset+9,1)+'.';
	                                         if (exif.substr(exif_offset+11,1)!='0') {
	                                             tmp+=exif.substr(exif_offset+10,2);
	                                         }
	                                         else {
	                                             tmp+=exif.substr(exif_offset+10,1);
	                                         }
	                                        // write_log('ExifVersion',tmp);
	                                     }
	                                     // ExifImageWidth
	                                     else if (tag_id==0xa002 && tag_len>0 && tag_value!=0) {
	                                         var tmp=this.mask2(exif.substr(exif_offset+8,2));
	                                         if (tmp==0) {
	                                             tmp=this.mask2(exif.substr(exif_offset+10,2));
	                                         }
	                                        // write_log('ExifImageWidth',tmp);
	                                     }
	                                     // ExifImageHeight
	                                     else if (tag_id==0xa003 && tag_len>0 && tag_value!=0) {
	                                         var tmp=this.mask2(exif.substr(exif_offset+8,2));
	                                         if (tmp==0) {
	                                             tmp=this.mask2(exif.substr(exif_offset+10,2));
	                                         }
	                                        // write_log('ExifImageHeight',tmp);
	                                     }
	                                     // CreateDate
	                                     else if (tag_id==0x9004 && tag_len>0 && tag_value>0) {
	                                       //  write_log('CreateDate',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // UserComment
	                                     else if (tag_id==0x9286 && tag_len>0 && tag_value>0) {
	                                         var tmp=exif.substr(tag_value+6,tag_len);
	                                         if (tmp.substr(0,5)=='ASCII') {
	                                             tmp=tmp.substr(6);
	                                         }
	                                         else if (tmp.substr(0,7)=='UNICODE') {
	                                             tmp=tmp.substr(8);
	                                         }
	                                       //  write_log('UserComment',tmp);
	                                     }
	                                     // OwnerName
	                                     else if (tag_id==0xa430 && tag_len>0 && tag_value>0) {
	                                        // write_log('OwnerName',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // SerialNumber
	                                     else if (tag_id==0xa431 && tag_len>0 && tag_value>0) {
	                                        // write_log('SerialNumber',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // LensMake
	                                     else if (tag_id==0xa433 && tag_len>0 && tag_value>0) {
	                                        // write_log('LensMake',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // LensModel
	                                     else if (tag_id==0xa434 && tag_len>0 && tag_value>0) {
	                                       //  write_log('LensModel',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // LensSerialNumber
	                                     else if (tag_id==0xa435 && tag_len>0 && tag_value>0) {
	                                       //  write_log('LensSerialNumber',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // OwnerName
	                                     else if (tag_id==0xfde8 && tag_len>0 && tag_value>0) {
	                                       //  write_log('OwnerName',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // SerialNumber
	                                     else if (tag_id==0xfde9 && tag_len>0 && tag_value>0) {
	                                      //   write_log('SerialNumber',exif.substr(tag_value+6,tag_len));
	                                     }
	                                     // Lens
	                                     else if (tag_id==0xfdea && tag_len>0 && tag_value>0) {
	                                       //  write_log('Lens',exif.substr(tag_value+6,tag_len));
	                                     }

	                                     exif_offset+=12;
	                                 }
	                             }


	                         }
	                         offset+=12;
	                     }
	                 }
	             }
	         }
	         idx+=(section_length-2);
	     }
	 }
	}

	//Р¤СѓРЅРєС†РёСЏ РґРµРєРѕРґРёСЂРѕРІР°РЅРёСЏ СЃС‚СЂРѕРєРё РёР· base64
	function base64decode(str) {
	 // РЎРёРјРІРѕР»С‹ РґР»СЏ base64-РїСЂРµРѕР±СЂР°Р·РѕРІР°РЅРёСЏ
	 var b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg'+
	                'hijklmnopqrstuvwxyz0123456789+/=';
	 var b64decoded = '';
	 var chr1, chr2, chr3;
	 var enc1, enc2, enc3, enc4;

	 str = str.replace(/[^a-z0-9\+\/\=]/gi, '');

	 for (var i=0; i<str.length;) {
	     enc1 = b64chars.indexOf(str.charAt(i++));
	     enc2 = b64chars.indexOf(str.charAt(i++));
	     enc3 = b64chars.indexOf(str.charAt(i++));
	     enc4 = b64chars.indexOf(str.charAt(i++));

	     chr1 = (enc1 << 2) | (enc2 >> 4);
	     chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	     chr3 = ((enc3 & 3) << 6) | enc4;

	     b64decoded = b64decoded + String.fromCharCode(chr1);

	     if (enc3 < 64) {
	         b64decoded += String.fromCharCode(chr2);
	     }
	     if (enc4 < 64) {
	         b64decoded += String.fromCharCode(chr3);
	     }
	 }
	 return b64decoded;
	}

function getXmlHttp(){ var xmlhttp; try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try {  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");  } catch (E) { xmlhttp = false; } } if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp = new XMLHttpRequest(); } return xmlhttp; }  
