<?
if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$arItemIDs = array(
	"HEADER"   => "header",
	"SWITCH"   => "switch",
	"PROGRESS" => "progress2"
);
?>

<div class="top_bg"></div>

<div class="header2" id="<?=$arItemIDs['HEADER']?>">
	<div style="cursor: pointer" class="personal_photo" onclick="app.showFileChooser(this, uploadfile2)">
		<div class="load" id="progress"></div>
		<canvas class="canvas" id="canvas"></canvas>
		<img src="<?=($arResult[" arUser"]['PERSONAL_PHOTO'] ? $arResult["arUser"]['PERSONAL_PHOTO']['src'] : $templateFolder . '/images/user_no_photo.png')?>" width="96" height="96"/>
	</div>
	<input class="personal_photo_file" type="file" onchange="uploadfile(this)"/>

	<div class="name"><?=$arResult['arUser']['FORMAT_NAME']?></div>
	<? if (CSite::InGroup([5])) : ?>
		<div style="cursor: pointer" class="profile" onclick="app.loadPageBlank({url: '/youdo/personal/profile/edit.php', title: '<?=GetMessage('PROFILE')?>'})"><?=GetMessage('EDIT')?></div>
	<? endif; ?>
</div>

<div class="afs-list">
	<ul>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/youdo/personal/?ITEM=account" title="<?=GetMessage('BALANCE')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_balance.svg">
				</div>
				<span>Баланс</span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/youdo/personal/subscription/" title="<?=GetMessage('SUBSCRIPTION')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_subscribe_2.svg">
				</div>
				<span><?=GetMessage('SUBSCRIPTION')?></span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/youdo/personal/register/" title="<?=GetMessage('REGISTER')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_user.svg">
				</div>
				<span><?=GetMessage('REGISTER')?></span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/youdo/personal/?ITEM=agreement" title="<?=GetMessage('AGREEMENT')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_subscribe.svg">
				</div>
				<span><?=GetMessage('AGREEMENT')?></span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/include/policy/privacy_policy_ru.php" title="<?=GetMessage('PRIVACY_POLICY')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_policy.svg">
				</div>
				<span><?=GetMessage('PRIVACY_POLICY')?></span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

		<li class="afs-menu-item hidden-md hidden-lg afs-selected">
			<a href="/include/term-of-use.php" title="<?=GetMessage('TERMS_OF_USE')?>">
				<div class="afs-list__image">
					<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_terms.svg">
				</div>
				<span><?=GetMessage('TERMS_OF_USE')?></span>
			</a>
		</li>
		<div class="afs-list-divider"></div>

	</ul>
</div>

<button class="btn btn-outline-danger btn-block btn-afs" onclick="return Logout(this)"><?=GetMessage('EXIT')?></button>

<?php
$arJSParams = array(
	"templateFolder" => $templateFolder,
	"isContractor"   => $isContractor,
	"AJAX"           => Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest(),
	"VISUAL"         => $arItemIDs
);
?>

<script type="text/javascript">new JCPersonal(' . CUtil::PhpToJSObject($arJSParams, false, true) . ')</script>
