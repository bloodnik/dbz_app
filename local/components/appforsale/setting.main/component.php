<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized())
{
	CAppforsale::AuthForm();
}

if ($_REQUEST['AJAX_CALL'] == 'Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	if ($_REQUEST['contractor'] == 'Y')
	{
		CUser::SetUserGroup(
			$USER->GetID(),
			array_merge(
				CUser::GetUserGroup($USER->GetID()),
				array($arParams['SWITCH_GROUP'])
			)
		);
		$arRes = array("response" => 1);
	}
	else
	{
		$arGroups = CUser::GetUserGroup($USER->GetID());
		foreach ($arGroups as $key=>$gid)
		{
			if($gid == $arParams['SWITCH_GROUP'])
			{
				unset($arGroups[$key]);
				break;
			}
		}
		CUser::SetUserGroup(
			$USER->GetID(),
			$arGroups
		);
		$arRes = array("response" => 2);
	}
	$USER->Authorize($USER->GetID());

	die(\Bitrix\Main\Web\Json::encode($arRes));
}


$arResult["ID"] = intval($USER->GetID());
$rsUser = CUser::GetByID($arResult["ID"]);
if(!$arResult["arUser"] = $rsUser->GetNext(false))
{
	$arResult["ID"] = 0;
}
else
{
	$nameFormat = CSite::GetNameFormat(true);
	$arResult["arUser"]['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arResult["arUser"]);
	$arResult["arUser"]['PERSONAL_PHOTO'] = (0 < $arResult["arUser"]['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array('width' => 192, 'height' => 192), BX_RESIZE_IMAGE_EXACT, true) : false);
}

$this->IncludeComponentTemplate();
?>