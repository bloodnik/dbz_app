<?

/*
* * * * * wget -O - -q -t 1 http://dom-bez-zabot.online /dev/null >/dev/null 2>1
*/

AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("PushActions", "OnAfterIBlockElementDeleteHandler"));

class PushActions {
	function OnAfterIBlockElementDeleteHandler($arFields) {

		// Удаляем все уведомления связанные с удаляемоой заявкой
		if ($arFields["IBLOCK_ID"] == 4) {
//			AddMessage2Log("Произошло удаление заявки с ID " . $arFields["ID"], "TASK_DELETED");

			$entity_data_class = getHighloadEntity(1);
			$rsData            = $entity_data_class::getList(array(
				"select" => array("*"),
				"order"  => array("ID" => "ASC"),
				"filter" => array("UF_TASK_ID" => $arFields["ID"])  // Задаем параметры фильтра выборки
			));

			while ($arData = $rsData->Fetch()) {
				$entity_data_class::delete($arData['ID']);
			}
		}
	}
}


// Пуш менеджеру, что заявка не взята в работу. Запускается из агентов
function Push() {
	CModule::IncludeModule('iblock');
	CModule::IncludeModule('mlab.appforsale');

	$arSelectTask = Array("*", "PROPERTY_*");
	$arFilterTask = Array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "PROPERTY_STATUS_ID" => array(false, 'N'), "PROPERTY_PUSHED" => '');
	$resTask      = \CIBlockElement::GetList(Array(), $arFilterTask, false, Array(), $arSelectTask);
	while ($obTask = $resTask->GetNextElement()) {
		$arFieldsTask = $obTask->GetFields();
		$arPropsTask  = $obTask->GetProperties();

		$date        = $arFieldsTask['DATE_CREATE'];
		$currentDate = strtotime($date);
		$futureDate  = $currentDate + (60 * 20);
		$formatDate  = date("Y-m-d H:i:s", $futureDate);

		if ($formatDate < date('Y-m-d H:i:s') && $arPropsTask["PUSHED"]["VALUE"] == '') {
			global $push_id;
			$push_id = 2;
			\CAppforsale::SendMessage(
				array((int) $arFieldsTask['CREATED_BY']),
				array(
					'body'         => 'Заявка "' . $arFieldsTask['NAME'] . '" еще не принята в работу',
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => 'http://dom-bez-zabot.online/youdo/tasks-customer/' . $arFieldsTask['DETAIL_PAGE_URL'],
					'id'  => $arFieldsTask['ID']
				)
			);

			\CIBlockElement::SetPropertyValuesEx($arFieldsTask['ID'], 4, array('PUSHED' => 'yes'));
		}
	}

	return 'Push();';
}

// Чистка пушов
function ClearPush() {
//	CModule::IncludeModule('iblock');
//	global $DB;

	//$deleteDate = date('d.m.Y', (strtotime('-1 day', strtotime(date('d.m.Y')))));


//	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
//	$arFilter = Array("IBLOCK_ID" => IntVal(6), "<DATE_CREATE" => $deleteDate);
//	$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 5000), $arSelect);
//	while ($ob = $res->GetNextElement()) {
//		$arFields = $ob->GetFields();
//		$DB->StartTransaction();
//		if ( ! CIBlockElement::Delete($arFields['ID'])) {
//			AddMessage2Log("PUSH_CLEAR_ERROR");
//			$DB->Rollback();
//		} else {
//			$DB->Commit();
//		}
//	}

	$deleteDate        = date('d.m.Y H:m', (strtotime('-10 hour', strtotime(date('d.m.Y H:m')))));
	$entity_data_class = getHighloadEntity(1);
	$rsData            = $entity_data_class::getList(array(
		"select" => array("*"),
		"order"  => array("ID" => "ASC"),
		"limit"  => 12000,
		"filter" => array("<UF_CREATED_DATE" => new \Bitrix\Main\Type\DateTime($deleteDate))  // Задаем параметры фильтра выборки
	));

	while ($arData = $rsData->Fetch()) {
		$entity_data_class::delete($arData['ID']);
	}

	return 'ClearPush();';
}


function sendNewTaskPush($taskId, $usersIds, $name) {
	CModule::IncludeModule('iblock');

	$dbElement = \CIBlockElement::GetByID($taskId);
	$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
	if ($arElement = $dbElement->GetNext()) {
		global $push_id;
		$push_id = 1;
		\CAppforsale::SendMessage(
			array_unique($usersIds),
			array(
				'body'         => 'Новое задание "' . $name . '"',
				'icon'         => 'ic_stat_name',
				'click_action' => 'appforsale.exec'
			),
			array(
				'url' => 'http://dom-bez-zabot.online/youdo/' . $arElement['DETAIL_PAGE_URL'],
				'id'  => $arElement['ID']
			)
		);
	}
}


AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array("UpdateNewTask", "OnAfterIBlockElementUpdate"));

//Отправляем пуши всем исполнителям задачи
class UpdateNewTask {

	function OnAfterIBlockElementUpdate($arFields) {
		if ($arFields['ID'] > 0) {
			if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task') {
				CModule::IncludeModule('iblock');

				$dbElement = \CIBlockElement::GetByID($arFields['ID']);
				$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
				if ($arElement = $dbElement->GetNext()) {
					$dbProperty = \CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
					while ($arProperty = $dbProperty->GetNext()) {
						$arElement['PROPERTY_VALUES'][ $arProperty['ID'] ] = $arProperty['VALUE'];
					}

					$arSelectTask = Array("*", "PROPERTY_*");
					$arFilterTask = Array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "ID" => $arFields['ID']);
					$dbTask       = \CIBlockElement::GetList(Array(), $arFilterTask, false, Array(), $arSelectTask);
					while ($arTask = $dbTask->GetNextElement()) {
						$arFieldsTask = $arTask->GetFields();
						$arPropsTask  = $arTask->GetProperties();
						$STATUS       = $arPropsTask['STATUS_ID']['VALUE'];
						$partners     = $arPropsTask['PARTNERS']['VALUE'];
					}

					$arSelectProfile = Array("*");
					$arFilterProfile = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "ID" => $partners);
					$dbProfile       = \CIBlockElement::GetList(Array(), $arFilterProfile, false, Array(), $arSelectProfile);
					while ($arProfile = $dbProfile->GetNextElement()) {
						$arFieldsProfile = $arProfile->GetFields();
						$profiles[] = $arFieldsProfile['CREATED_BY'];
					}


					if ( ! empty($profiles) && $arElement['NAME'] !== 'unknown' && $STATUS == 'N') {
						global $push_id;
						$push_id = 1;
						\CAppforsale::SendMessage(
							array_unique($profiles),
							array(
								'body'         => 'Новое задание "' . $arElement['NAME'] . '"',
								'icon'         => 'ic_stat_name',
								'click_action' => 'appforsale.exec'
							),
							array(
								'url' => 'http://dom-bez-zabot.online/youdo/tasks/' . $arElement['DETAIL_PAGE_URL'],
								'id'  => $arElement['ID']
							)
						);
					}
				}
			}
		}
	}
}

?>