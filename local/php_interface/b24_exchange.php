<?

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;

// Код авторизациии исходящего вебхука для события обновления сделки
define('DEAL_UPDATE_SECRET', 'gsaonqu113avuaegg3g34dj7l8gjgqw2');

// Секретный ключ входящего вебхука Bitrix24
define('OUT_SECRET', 'mhylug065v6c458h');

define('OFFER_IBLOCK_ID', 3);
define('TASK_IBLOCK_ID', 4);
define('BITRIX_24_USER_ID', 788);

/*Параметры приложения авторизации*/
define('APP_ID', 'local.5c7bf1f9937e25.80343879'); // take it from Bitrix24 after adding a new application
define('APP_SECRET_CODE', 'GULqhKSFxGbZTSQVQVf2OnKrOjvFd4DoMvntvI4treig9M62wi'); // take it from Bitrix24 after adding a new application
define('DOMAIN', 'dombezzabot.bitrix24.ru'); // the same URL you should set when adding a new application in Bitrix24

//Стадии сделок на стороне B24
define('DEAL_STAGES', [
	'NEW'         => 'NEW', // Обращение
	'FREE'        => '7', // Свободная заявка
	'IN_PROGRESS' => '8', // Заявка в работе
	'DONE'        => '9', // Выполненная заявка
	'WON'         => 'WON', // Заявка закрыта (выполнено)
	'LOSE'        => 'LOSE' // Заявка провалена
]);

/**
 * Меняет обновляет сделку на стороне Б24
 *
 * @param $taskID - ID заявки в системе
 * @param string $stage - стадия сделки
 */
function changeDealStage($taskID, $stage = "NEW") {
	Loader::includeModule('iblock');

	if (empty($taskID)) {
		return false;
	}

	$arSelectTask = array("*", "PROPERTY_*");
	$arFilterTask = array("IBLOCK_ID" => TASK_IBLOCK_ID, "ACTIVE" => "Y", "ID" => $taskID);
	$resTask      = \CIBlockElement::GetList(array(), $arFilterTask, false, array(), $arSelectTask);

	$executorName  = '';
	$executorPhone = '';
	$executorID    = '';

	$arInWorkStages = ["25", "8"];

	while ($obTask = $resTask->GetNextElement()) {
		$arFieldsTask = $obTask->GetFields();

		$arPropsTask = $obTask->GetProperties();
		if ( ! empty($arPropsTask['PROFILE_ID']['VALUE'])) {
			$arExecutor    = CUser::GetByID($arPropsTask['PROFILE_ID']['VALUE'])->GetNext();
			$executorName  = $arExecutor['NAME'] . ' ' . $arExecutor['LAST_NAME'];
			$executorPhone = $arExecutor['LOGIN'];
			$executorID    = $arPropsTask['PROFILE_ID']['VALUE'];
		}

		if ( ! empty($arFieldsTask['XML_ID']) && $arPropsTask['FROM_CRM']['VALUE'] === 'Y') {

			$userBudget = 0;
			$tariffName = "";
			if ($executorID) {
				$userBudget = CAppforsaleUserAccount::getCurrentUserBudget($executorID);

				$tariff     = new CAppforsaleTariff($executorID);
				$userTariff = $tariff->getUserTariff();
				$tariffName = $userTariff["TARIFF_NAME"];
			}

			// Endpoint обновления сделки по ID
			$dealUrl = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.deal.update';
			// Передаваемые параметры сделки
			$dealData = array(
				'id'     => $arFieldsTask['XML_ID'], // ID сделки
				'fields' => [
					'STAGE_ID'          => $stage,
					'UF_CRM_1549782914' => $executorName, //ФИО исполнителя
					'UF_CRM_1603386052' => $executorPhone, //Телефон исполнителя
					'UF_CRM_1552120642' => $executorID, //ID исполнителя
					'UF_CRM_1552376744' => $arFieldsTask['SHOW_COUNTER'], //Количество просмотров заявки
					'UF_CRM_1552426988' => $arFieldsTask['ID'], //ID заявки,
					"UF_CRM_BALANCE"    => $userBudget,
					"UF_CRM_1607014240" => $arPropsTask["FACT_SUMM"]["VALUE"] //Фактическая цена работ
				],
				'params' => [
					'REGISTER_SONET_EVENT' => 'Y'
				]
			);

			if (in_array($stage, $arInWorkStages)) {
				if (CAppforsaleProfile::hasFirstTask()) {
					$dealData["fields"]["UF_CRM_1602507997"] = "Y";  // Первая бесплатная заявка
				} else {
					$dealData["fields"]["UF_CRM_1602507997"] = "";
				}

				$dealData["fields"]["UF_CRM_TARIFF"] = $tariffName;
			}

			//Отправка комментария в таймленту с фио исполнителя
			if ($arPropsTask['STATUS_ID']['VALUE'] === 'P' && empty($arPropsTask['HAS_PROBLEM']['VALUE'])) {

				$dealCommentUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.livefeedmessage.add';
				$dealCommentData = array(
					'fields' => [
						'POST_TITLE'   => md5('Комментарий_' . $executorName),
						'MESSAGE'      => 'Работу принял исполнитель: ' . $executorName . "\n Телефон: " . $executorPhone,
						"ENTITYTYPEID" => 2,
						"ENTITYID"     => $arFieldsTask['XML_ID']
					]
				);
				makePostQuery($dealCommentUrl, $dealCommentData);
				sleep(2);
			}

			//Комментарий и фото от исполнителя. Добавляем в таймленту
			if ( ! empty($arPropsTask['EXECUTOR_FILE']['VALUE']) || ! empty($arPropsTask['EXECUTOR_COMMENT']['VALUE'])) {
				$partnerFiles = [];
				if ( ! empty($arPropsTask['EXECUTOR_FILE']['VALUE'])) {
					foreach ($arPropsTask['EXECUTOR_FILE']['VALUE'] as $fileID) {
						$arFile         = CFile::GetFileArray($fileID);
						$fileData       = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $arFile['SRC']);
						$partnerFiles[] = [$arFile['ORIGINAL_NAME'], base64_encode($fileData)];
					}
				}

				$dealCommentUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.livefeedmessage.add';
				$dealCommentData = array(
					'fields' => [
						'POST_TITLE'   => md5('Комментарий_' . rand(100, 999)),
						'MESSAGE'      => ! empty($arPropsTask['EXECUTOR_COMMENT']['VALUE']) ? $arPropsTask['EXECUTOR_COMMENT']['VALUE'] : "Заявка выполнена",
						"ENTITYTYPEID" => 2,
						"ENTITYID"     => $arFieldsTask['XML_ID'],
						"FILES"        => $partnerFiles
					]
				);
				makePostQuery($dealCommentUrl, $dealCommentData);
				sleep(2);
			}


			// Запрос
			makePostQuery($dealUrl, $dealData);
			AddMessage2Log("Сделка с ID {$arFieldsTask['XML_ID']} обновлена. ID задания {$taskID}. Стадия - {$stage}. Исполнитель - {$executorName}", 'UPDATE_DEAL');
		}
	}
}

/**
 * Удаление отклика(предложения) исполнителя
 * и убираем его из партнеров заявки, и обновляем заявку
 *
 * @param $taskId - ID заявки
 * @param $updateUserAccount - флаг возврата денег на счет пользователя (true - возвращает, false - нет)
 */
function executorOfferDelete($taskId, $updateUserAccount = false, $deduction = 0) {
	Loader::includeModule('iblock');

	$arFilterOffer = array("IBLOCK_ID" => OFFER_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_TASK_ID" => $taskId);
	$resOffer      = \CIBlockElement::GetList(array(), $arFilterOffer, false, array(), array("ID", "CREATED_BY"));

	try {
		// Удаляем предложение исполнителя, если оно есть
		while ($obOffer = $resOffer->GetNextElement()) {
			$arFieldsOffer = $obOffer->GetFields();
			CIBlockElement::Delete($arFieldsOffer['ID']);

			//Обновляем счет пользователя
			if ($updateUserAccount) {
				$totalComission = CAppforsaleComission::getTotalComissionSumm($arFieldsOffer['CREATED_BY'], $taskId);
				$totalComission = $totalComission + CAppforsaleTask::GetTaskCost($taskId);
				$totalComission = $totalComission - $deduction;
				$byTariff       = CAppforsaleTask::byTariff($taskId);


				if ($totalComission > 0 && ! $byTariff) {
					$note = "Возврат списанной комиссии по заявке №" . $taskId . " осуществлён";
					if ($deduction > 0) {
						$note = "Возврат по заявке №" . $taskId . " осуществлён за минусом " . $deduction . " рублей согласно п.п. 9.2.2";
					}

					\CAppforsaleUserAccount::UpdateAccount($arFieldsOffer['CREATED_BY'], $totalComission, "ORDER_PART_RETURN", $taskId, $note);

					$resTask = CIBlockElement::GetByID($taskId);
					if ($arTask = $resTask->GetNext()) {
						$dealCommentUrl  = 'https://dombezzabot.bitrix24.ru/rest/52/' . OUT_SECRET . '/crm.livefeedmessage.add';
						$dealCommentData = array(
							'fields' => [
								'POST_TITLE'   => md5('Комментарий_' . $arFieldsOffer['CREATED_BY']),
								'MESSAGE'      => 'Исполнителю возвращены денежные средства в размере: ' . $totalComission . ' рублей',
								"ENTITYTYPEID" => 2,
								"ENTITYID"     => $arTask['XML_ID'],
							]
						);
						makePostQuery($dealCommentUrl, $dealCommentData);
					}
				}
				AddMessage2Log(array(
					"TASK_ID"     => $arFieldsOffer['ID'],
					"OFFER_PRICE" => $totalComission,
					"USER_ID"     => $arFieldsOffer['CREATED_BY']
				), 'UPDATE_ACCOUNT');


				CIBlock::clearIblockTagCache(TASK_IBLOCK_ID);
			}
		}
	} catch (Exception $e) {
		AddMessage2Log('Поймано исключение ID : ' . $taskId, $e->getMessage(), "\n", 'DELETE_EXCEPTION');
	}
}

/**
 * Удаление заявки из системы
 *
 * @param $taskId - ID заявки
 * @param $updateUserAccount - флаг возврата денег на счет пользователя (true - возвращает, false - нет)
 * @param $deduction - сумма удержания из комиссии
 */
function taskDelete($taskId, $updateUserAccount = false, $deduction = 0) {
	global $DB;
	Loader::includeModule('iblock');
	$strWarning = '';
	$DB->StartTransaction();

	//Удаляем предложение исполнителя
	executorOfferDelete($taskId, $updateUserAccount, $deduction);

	// Удаляем заявку
	if ( ! CIBlockElement::Delete($taskId)) {
		$DB->Rollback();
	} else {
		$DB->Commit();
		CIBlock::clearIblockTagCache(4);
	}
}


function bonusReturnPush($userId, $message) {
	global $push_id;
	$push_id = 1;
	\CAppforsale::SendMessage(
		array((int) $userId),
		array(
			'body'         => $message,
			'click_action' => 'appforsale.exec'
		),
		array(
			'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/'
		)
	);
}

/**
 * Выполнение POST запроса
 *
 * @param $queryUrl string - Ссылка REST_API
 * @param $queryData array - Передаваемы параметры
 *
 * @return array
 */
function makePostQuery($queryUrl, $queryData) {
	$httpClient = new HttpClient();
	$httpClient->setHeader('Content-Type', 'application/json', true);
	$response = $httpClient->post($queryUrl, json_encode($queryData));

	AddMessage2Log(array($queryUrl, $queryData), 'MAKE_POST_QUERY');

	return json_decode($response, 1);
}

function makePost($queryUrl, $queryData) {
	$httpClient = new HttpClient();
	$httpClient->setHeader('Content-Type', 'application/x-www-form-urlencoded');
	$query    = http_build_query($queryData);
	$query    = preg_replace('/%5B[0-9]+%5D/simU', '', $query);
	$response = $httpClient->post($queryUrl, $query);

	AddMessage2Log(array($queryUrl, $queryData), 'MAKE_POST');

	return json_decode($response, 1);
}


/**
 * Проделние авторизации, получение access_token
 */
function requestRefreshToken() {
	$refresh_token = Option::get("mlab.appforsale", "b24_refresh_token", "11", 's1');
	$url           = 'https://oauth.bitrix.info/oauth/token/?' .
	                 'grant_type=refresh_token' .
	                 '&client_id=' . urlencode(APP_ID) .
	                 '&client_secret=' . urlencode(APP_SECRET_CODE) .
	                 '&refresh_token=' . urlencode($refresh_token);

	$httpClient = new HttpClient();
	$httpClient->setHeader('Content-Type', 'application/json', true);
	$response          = $httpClient->get($url);
	$arNewAccessParams = json_decode($response, true);;

	if (is_array($arNewAccessParams) && isset($arNewAccessParams['refresh_token'])) {
		Option::set("mlab.appforsale", "b24_refresh_token", $arNewAccessParams['refresh_token'], "s1");
		Option::set("mlab.appforsale", "b24_access_token", $arNewAccessParams['access_token'], "s1");
	}

//	PR($arNewAccessParams);
}

/**
 * Скачивание файла с Б24 и помещение его во временную папку
 * Нужно для прикрепления файла к заданию
 *
 * @param $fileUrl string - Ссылка на файл
 * @param $fileName string - Имя файла
 *
 */
function downloadFileFromB24($fileUrl, $fileName) {
	$httpClient = new HttpClient();
	$httpClient->download($fileUrl, $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/b24_images/' . $fileName);
}

/**
 * Преобразует массив в ключ => зна
 *
 * @param array $ar - массив вида [0=>['ID' => 1, 'VALUE' => 'Значение 1'], 1=>['ID' => 2, 'VALUE' => 'Значение 2'],]
 *
 * @return array - массив вида [[Значение 1] => 1, ['Значение 2'] => 2]
 */
function remapB24List($ar = array()) {
	return array_reduce($ar, function($result, $item) {
		$result[ $item['VALUE'] ] = $item['ID'];

		return $result;
	}, array());
}

/**
 * Функция для конрвертаци даты
 *
 * @param $datetime - дата в формате сайта
 *
 * @return string - дата в формате Б24
 */
function getB24DateFormat($datetime) {
	$format = "DD.MM.YYYY HH:MI:SS";
	if ($arr = ParseDateTime($datetime, $format)) {
		return "{$arr['YYYY']}-{$arr['MM']}-{$arr['DD']}T{$arr['HH']}:{$arr['MI']}+03:00";
	}

	return '';
}

// Проверка того, что сделка не принята в коробке (НП)
function isDealAvailableInCloud($taskId) {
	$arTask   = CAppforsaleTask::GetTaskById($taskId);
	$dealId = $arTask["XML_ID"];

	$queryUrl = 'https://dombezzabot.net/api/rest_api/rest_in.php';
	$httpClient = new HttpClient();
	$response = $httpClient->post($queryUrl, array(
		'token'  => 'fc1416079a3653dc7a4dc1a99c5bfe',
		'action' => 'CheckOrder',
		'id'     => $dealId,    //id сделки в облаке
	));

	$arResponse = json_decode($response, true);
	return $arResponse["is_available"];
}

?>