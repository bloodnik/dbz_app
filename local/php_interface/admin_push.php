<?
AddEventHandler("main", "OnBuildGlobalMenu", "PUSHFOREXECUTORS");

function PUSHFOREXECUTORS(&$adminMenu, &$moduleMenu){
    $moduleMenu[] = array(
         "parent_menu" => "global_menu_appforsale", // поместим в раздел "Сервис"
         "section" => "mlab.appforsale",
         "sort"        => 1000,
         "url"         => "pushforexecutors.php?lang=".LANG,
         "text"        => 'Push для исполнителей',       // текст пункта меню
         "title"       => 'Рассылка пушей', // текст всплывающей подсказки
         "icon"        => "form_menu_icon", // малая иконка
         "page_icon"   => "form_page_icon", // большая иконка
         "items_id"    => "menu_mlab.appforsale",  // идентификатор ветви
         "items"       => array()          // остальные уровни меню сформируем ниже.
    );
}

?>