<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/b24_exchange.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/b24_exchange_new.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/b24_executor_export.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/push.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/admin_push.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/highloadService.php');

AddEventHandler("iblock", "OnAfterIBlockElementAdd", array("ModerateProfiles", "OnAfterIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", array("ModerateProfiles", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array("ModerateProfiles", "OnAfterIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", array("CommentForManager", "OnAfterIBlockElementUpdateHandler1"));

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('', 'DBZBonusTransactOnAfterAdd', 'CAppforsaleTaskBonusTransact::onAfterAdd');


define('VUEJS_DEBUG', false);

//Модераторы
//define('MODERATORS_ID', ['1', '88', '17', '18827', '20156', '19168', '122', '13790', '14730', '2383', '31159', '31551', '32075', '32086']);

// For debug
function PR($item, $show_for = false) {
	global $USER;
	if ($USER->IsAdmin() || $show_for == 'all') {
		if ( ! $item) {
			echo ' <br />пусто <br />';
		} elseif (is_array($item) && empty($item)) {
			echo '<br />массив пуст <br />';
		} else {
			echo ' <pre>' . print_r($item, true) . ' </pre>';
		}
	}
}


// Автоматическое списание комисси за незакрытые заявки
function closeExpiredTаskAgent() {
	\Bitrix\Main\Loader::includeModule('mlab.appforsale');
	CAppforsaleTask::closeExpiredTask();

	return 'closeExpiredTаskAgent();';
}

// Автоматическое списание окончательной части комиссии
function writeOfLastComission() {
	\Bitrix\Main\Loader::includeModule('mlab.appforsale');
	CAppforsaleComission::writeOfLastComissionAndCloseTask();

	return 'writeOfLastComission();';
}

function returnResultCache($timeSeconds, $cacheId, $callback, $arCallbackParams = '') {
	$obCache   = new CPHPCache();
	$cachePath = '/' . SITE_ID . '/' . $cacheId;
	if ($obCache->InitCache($timeSeconds, $cacheId, $cachePath)) {
		$vars   = $obCache->GetVars();
		$result = $vars['result'];
	} elseif ($obCache->StartDataCache()) {
		$result = $callback($arCallbackParams);
		$obCache->EndDataCache(array('result' => $result));
	}

	return $result;
}

// Чистка заявок
function ClearTask() {
	CModule::IncludeModule('iblock');
	global $DB;

	$arSelect = array("ID", "NAME", "DATE_CREATE");
	$arFilter = array("IBLOCK_ID" => IntVal(4), "<DATE_CREATE" => '01.09.2019');
	$res      = CIBlockElement::GetList(array("DATE_CREATE" => "ASC"), $arFilter, false, array("nPageSize" => 300), $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$DB->StartTransaction();
		if ( ! CIBlockElement::Delete($arFields['ID'])) {
			AddMessage2Log("TASK_CLEAR_ERROR");
			$DB->Rollback();
		} else {
			$DB->Commit();
		}
	}


	return 'ClearTask();';
}

//Удаляем лог списании если удаляется заявка
AddEventHandler("iblock", "OnAfterIBlockElementDelete", array("ComissionLog", "OnAfterIBlockElementDeleteHandler"));

class ComissionLog {
	function OnAfterIBlockElementDeleteHandler($arFields) {

		// Удаляем все логи связанные с этой заявкой
		if ($arFields["IBLOCK_ID"] == 4) {
			$entity_data_class = getHighloadEntity(4);
			$rsData            = $entity_data_class::getList(array(
				"select" => array("*"),
				"order"  => array("ID" => "ASC"),
				"filter" => array("UF_TASK" => $arFields["ID"])  // Задаем параметры фильтра выборки
			));

			while ($arData = $rsData->Fetch()) {
				$entity_data_class::delete($arData['ID']);
			}
		}
	}
}

class ModerateProfiles {

	public static $inModaratePushText = "Вы зарегистрировались в сервисе для мастеров «Дом Без Забот». <br>
											Ваш профиль ожидает модерации. <br>
											Пока Ваши данные проверяются ознакомьтесь со всеми преимуществами сервиса:<br><br>
											
											1. Первая заявка со скидкой 50%<br>
											2. Возврат комиссии<br>
											3. Комиссия всего до 10%<br>
											4. Кэшбэк на остаточную сумму баланса<br>
											5. Бонусы за выполненные заявки<br>
											6. Удобные тарифы";

	//После добавления профиля, отправляем его на модерацию, деактивируем профиль
	function OnAfterIBlockElementAddHandler($arFields) {

		CModule::IncludeModule('iblock');

		if ($arFields['IBLOCK_ID'] == 2) {
			global $push_id;
			$push_id = 1;
			\CAppforsale::SendMessage(
				array((int) $arFields['CREATED_BY']),
				array(
					'body'         => COption::GetOptionString('mlab.appforsale', 'REGISTER_PUSH', self::$inModaratePushText),
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/register/'
				)
			);

			$el = new \CIBlockElement;
			$el->Update($arFields["ID"], array("ACTIVE" => "N"));
		}
	}

	//Перед обновление профиля, деактивирем его и отправляем его на модерацию, если пользователь не из группы модераторов
	function OnBeforeIBlockElementUpdateHandler(&$arFields) {
		if ($arFields['IBLOCK_ID'] == 2) {
			global $USER;

			if ( ! $USER->IsAdmin()) {
				if ($arFields['PROPERTY_VALUES']) {
					$arFields['ACTIVE'] = 'N';

					global $push_id;
					$push_id = 1;
					\CAppforsale::SendMessage(
						array((int) $arFields['MODIFIED_BY']),
						array(
							'body'         => self::$inModaratePushText,
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/personal/register/'
						)
					);
				}
			}
		}
	}

	public static $handlerDisallow = false;

	// После изменения профиля, обновляем данные пользователя.
	// И разрешаем активацию профиля группой модераторов
	function OnAfterIBlockElementUpdateHandler(&$arFields) {

		if (self::$handlerDisallow) {
			return;
		}


		CModule::IncludeModule('iblock');

		if ($arFields['IBLOCK_ID'] == 2) {
			$arSelect = array("*", "PROPERTY_*");
			$arFilter = array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']);
			$res      = \CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
			$USER_ID  = 0;
			while ($ob = $res->GetNextElement()) {
				$arFields1   = $ob->GetFields();
				$arProps1    = $ob->GetProperties();
				$USER_ID     = $arFields1['CREATED_BY'];
				$NAME        = $arProps1['NAME']['VALUE'];
				$SECOND_NAME = $arProps1['SECOND_NAME']['VALUE'];
				$LAST_NAME   = $arProps1['LAST_NAME']['VALUE'];
				$EMAIL       = $arProps1['EMAIL']['VALUE'];
				$CITY        = $arProps1['CITY']['VALUE'];
			}

			global $USER;

			if ($USER_ID > 0) {

				$dbUser = new \CUser;
				$fields = array(
					"NAME"        => $NAME,
					"LAST_NAME"   => $LAST_NAME,
					"SECOND_NAME" => $SECOND_NAME,
					"UF_CITY"     => $CITY,
				);
				$dbUser->Update($USER_ID, $fields);

				CModule::IncludeModule('mlab.appforsale');
				if ($USER->IsAdmin() && $arFields['ACTIVE'] == 'Y' && ! CAppforsaleProfile::getLastModerateDate($USER_ID)) {
					self::$handlerDisallow = true;  //обновлено
					global $push_id;
					$push_id = 1;
					\CAppforsale::SendMessage(
						array((int) $USER_ID),
						array(
							'body'         => COption::GetOptionString('mlab.appforsale', 'MODERATE_PUSH', "Ваш профиль одобрен"),
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
						)
					);

					\CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('MODERATE_PUSHED' => 'YES'));

					//номер телефона
					$rsUser = CUser::GetByID($USER_ID);
					$phone  = "";
					if ($arUser = $rsUser->GetNext()) {
						$phone = $arUser["LOGIN"];
					}
					CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('PHONE' => $phone));

					CAppforsaleProfile::setFirstTask($USER_ID);
					CAppforsaleProfile::setLastModeratateDate($USER_ID);
				}
			}
		}
	}
}

class CommentForManager {
	// После обновления свойства комментарий для менеджера, отправляем пуш менеджеру
	function OnAfterIBlockElementUpdateHandler1(&$arFields) {

		CModule::IncludeModule('iblock');

		if ($arFields['IBLOCK_ID'] == 4) {
			if ( ! empty($arFields['PROPERTY_VALUES']['53']) && $arFields['PROPERTY_VALUES']['43']['VALUE'] == 'P') {
				$arSelect = array("*", "PROPERTY_*");
				$arFilter = array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']);
				$res      = \CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields1 = $ob->GetFields();
					$arProps1  = $ob->GetProperties();
					$USER      = $arFields1['CREATED_BY'];
					$NAME      = $arProps1['NAME']['VALUE'];
				}

				global $push_id;
				$push_id = 1;
				\CAppforsale::SendMessage(
					array((int) $USER),
					array(
						'body'         => 'Новый коментарий по заявке "' . $NAME . '"',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/tasks-customer/' . $arFields1['DETAIL_PAGE_URL']
					)
				);
			}
		}
	}
}


function isUserDeactivated() {
	global $USER;

//	$uid = $userID ?? $USER->GetID();

	$rsUser = CUser::GetList(($by = "ID"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => array("UF_DEACTIVATED")));

	if ($arUser = $rsUser->Fetch()) {
		return $arUser["UF_DEACTIVATED"];
	}
}

?>