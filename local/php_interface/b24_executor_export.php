<?
use Bitrix\Main\Loader;

/**
 * Обработчик экспорта пользователя в Б24 после добавление
 */


// регистрируем обработчик
AddEventHandler("main", "OnAfterUserAdd", Array("DbzAfterUserAdd", "OnAfterUserAddHandler"));
AddEventHandler("main", "OnAfterUserUpdate", Array("DbzAfterUserAdd", "OnAfterUserAddHandler"));

class DbzAfterUserAdd {
	// создаем обработчик события "OnAfterUserAdd"
	function OnAfterUserAddHandler(&$arFields) {
		if ($arFields["ID"] > 0) {
			exportExecutorToB24($arFields['ID']);
		} else {
			AddMessage2Log("Ошибка добавления записи (" . $arFields["RESULT_MESSAGE"] . ").");
		}
	}
}

function exportExecutorToB24($userID) {
	Loader::includeModule('iblock');

    global $USER;

// Соответствия менеджеров из Б24 и CMS
	$managersCMStoB24 = [
		"89"  => "30", //Александр Артемьев
		"501" => "32", //Лоенид Гордеев
		"533" => "34", //Алия Дрофейчик
		"673" => "36", //Ринат Ганиев
		"122" => "1", //Евгений Антипов
		"88"  => "14", //Артем Родин
		"1"   => "40", //Влад Чистаков
	];

// Список городов по контакту из Б24
	$ufContactCityUrl = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
	$arCityContact    = makePostQuery($ufContactCityUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_5C70359B5EB25']]);
	$arCityContact24  = remapB24List($arCityContact['result'][0]['LIST']);

// Сопоставляем идетификторы CMS и Б24
	$arContactCitiesMap = [];
	$res                = CIBlockElement::GetList([], ["IBLOCK_ID" => 1, "ACTIVE" => "Y"], false, [], ['*']);
	while ($arFields = $res->GetNext()) {
		if ($arCityContact24[ $arFields['NAME'] ]) {
			$arContactCitiesMap[ $arFields['ID'] ] = $arCityContact24[ $arFields['NAME'] ];
		}
	}

// Список видов деятельности из Б24
	$ufContactDirectionUrl  = 'https://dombezzabot.bitrix24.ru/rest/1/g7t6hkcyeuje80gq/crm.contact.userfield.list';
	$arContactDirectionDeal = makePostQuery($ufContactDirectionUrl, ['filter' => ['FIELD_NAME' => 'UF_CRM_1551197597']]);
	$arContactDirection24   = remapB24List($arContactDirectionDeal['result'][0]['LIST']);

// Сопоставляем идетификторы CMS и Б24
	$arDirectionsMap = [];
	$rsSections      = CIBlockSection::GetList(["left_margin" => "asc"], ["IBLOCK_ID" => 4]);
	while ($arSection = $rsSections->GetNext()) {
		if ($arContactDirection24[ $arSection['NAME'] ]) {
			$arDirectionsMap[ $arSection['ID'] ] = $arContactDirection24[ $arSection['NAME'] ];
		}
	}

	$order      = array('sort' => 'asc');
	$tmp        = 'sort'; // параметр проигнорируется методом, но обязан быть
	$userParams = array('SELECT' => array("NAME", "LOGIN", "UF_CITY"));
	$filter     = array("ID" => $userID);
	$rsUsers    = CUser::GetList($order, $tmp, $filter, $userParams); // выбираем пользователей
	while ($arUser = $rsUsers->GetNext()) {
		sleep(0.5);
		// Получаем идентификаторы видов деятельности для профиля
		$arProfileDirectionIds = [];
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 2, 'CREATED_BY' => $arUser['ID']));
		if ($arProfile = $rsProfile->getNext()) {
			$dbPropsUserDirections = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'PROFILE_WORK')
			);
			while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
				$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
			}
		}

		// Виды детяльности для передачи в Б24
		$executorDirections = [];
		foreach ($arProfileDirectionIds as $dirId) {
			if ($arDirectionsMap[ $dirId ]) {
				$executorDirections[] = $arDirectionsMap[ $dirId ];
			}
		}

		$executorName       = $arUser["NAME"]; // Имя
		$executorLastName   = $arUser["LAST_NAME"]; // Фамилия
		$executorSecondName = $arUser["SECOND_NAME"]; // Отчетство
		$executorPhone      = $arUser["LOGIN"]; // Телефоно
		$executorCity       = $arContactCitiesMap[ $arUser["UF_CITY"] ]; // Город
		$executorDirection  = $executorDirections; // Виды деятельности
		$excutorType        = "SUPPLIER"; // Тип "Исполнитель"

		// Получение исполнителя по телефону
		$executorListUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.list';
		$arExecutors     = makePostQuery($executorListUrl, array('filter' => array("PHONE" => $executorPhone), 'select' => ["ID"]));

		$executorAddData = array(
			'fields' => array(
				'NAME'                 => $executorName, //Имя
				'LAST_NAME'            => $executorLastName, //Фамилия
				'SECOND_NAME'          => $executorSecondName, //Отчество
				'TYPE_ID'              => $excutorType, //тип
				'PHONE'                => array(array("VALUE" => $executorPhone, "VALUE_TYPE" => "WORK")), //Телефон
				'ASSIGNED_BY_ID'       => $managersCMStoB24[ $dealManager ], //Менеджер
				'UF_CRM_5C70359B5EB25' => $executorCity,//Город
				'UF_CRM_1551197597'    => $executorDirection,//Вид деятельности исполнителя
				'UF_CRM_1552309160'    => 'Y',// Признак "Контакт из CMS"
				'OPENED'               => 'Y', // Доступна для всех
				'UF_CRM_5D30062EA20C9'               => 'FROM_CRM' // ROISTAT
			)
		);

		if ($arExecutors['total'] === 0) { // Если исполнители не найдены, тогда добавляем
			$executorUrl = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.add';
		} else {
			$executorUrl           = 'https://dombezzabot.bitrix24.ru/rest/52/mhylug065v6c458h/crm.contact.update';
			$executorAddData['id'] = $arExecutors['result'][0]['ID'];
		}

		makePostQuery($executorUrl, $executorAddData);

		unset($arProfile, $arExecutors, $executorAddData);
	}
}