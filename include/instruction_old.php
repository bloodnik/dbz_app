<section>
	<h3>Как начать зарабатывать</h3>
	<ul>
		<li>1. Скачать наше приложение из GOOGLE PLAY или App Store по ссылкам:</li>
		<li><a href="https://itunes.apple.com/ru/app/%D0%B4%D0%BE%D0%BC%D0%B1%D0%B5%D0%B7%D0%B7%D0%B0%D0%B1%D0%BE%D1%82/id1440513938?mt=8" target="_blank">
				<img alt="app-store" style="width: 100px;" src="/youdo/images/app-store_2.svg">
			</a>
			<a href="/dombezzabot.apk" target="_blank">
				<img alt="google-play" style="width: 120px;" src="/youdo//images/google-play_2.svg">
			</a>
		</li>
		<li>2. Зарегистрироваться в приложении</li>
		<li>3. Ожидать подходящей заявки в ленте</li>
		<li>4. Принять заявку и выполнить работу</li>
	</ul>

	<br>

	<div class="executors__item-title executors__item-title_orange">
		<strong>Преимущества работы с нами:</strong>
	</div>
	<ul>
<!--		<li>- Оплата комиссии только за выполненный заказ;</li>-->
		<li>- Самостоятельный выбор подходящего заказа;</li>
		<li>- Возврат средств в случае отказа от заявки со стороны клиента.</li>
	</ul>
	<br>
	<div class="executors__item-subtitle">
		<strong>Присоединяйтесь к нашей дружной команде и зарабатывайте вместе с нами!</strong>
	</div>

</section>
<section>
	<h3>1. Как зарегистрироваться в приложении?</h3>
	<p>
		<strong>Установка:</strong>
	</p>
	<ul>
		<li>Открыть Google Play</li>
		<li>Найти Дом Без Забот</li>
		<li>Установить приложение (дать запрашиваемые разрешения)</li>
	</ul>
	<br>
	<p>
		<strong>Регистрация:</strong>
	</p>
	<ul>
		<li>Запустить приложение</li>
		<li>Ввести свой контактный номер телефона на которое должно прити смс сообщение с кодом активации</li>
		<li>Ввести полученый код</li>
		<li>Заполнить свой профиль (выбрать желаемые и смежные направления по которым хотите работать)</li>
		<li>Обязательно прикрепить фото разворота паспорта гражданина Российской Федерации (страница с фотографией и датой выдачи)</li>
		<li>Нажать кнопку "Сохранить"</li>
		<li>Ожидать прохождения модерации</li>
		<li>После проверки персональных данных Ваш аккаунт будет активирован</li>
	</ul>
	<div class="box_image_items">
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="/upload/medialibrary/c04/c04ebc8e01b0801f764541ae7b147fe9.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="/upload/medialibrary/0e5/0e501f9b0f16e0a322af3293c92c5c5b.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="/upload/medialibrary/797/7970ec3ea53eac92414959e6ad3d5afc.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/296/296d902efc9b889bf6d8b6f62b184dd4.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/4e8/4e8b0fd47a71beac02443ddd0a65f86a.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/882/882d297738e173660b50905beeb54311.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/191/191d8ec18abccdf769a99c901c7b2849.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/8b9/8b9e4fef5c9cbb2d73bf4ee9b20ea1ef.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/aac/aac2b74676e2b3b9c3f1dacb6f97b8b5.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/edf/edf34aec15ce1200c01fe50d4e621b58.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/dd9/dd920f5bbb8f02e55832dcd33fb01628.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/657/657062441915c5721411dd7154f79c80.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/81e/81e2a3937aeff4305fc4421bdc7ff06c.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/103/103c227ec640e29cc023ab3186625006.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/667/667518a86413c5edad6eaf1755cff8ba.jpg" alt="">
		</div>
	</div>
</section>
<section>
	<h3>2. Как пополнить баланс?</h3>
	<p>
		<strong>Как пополнить баланс:</strong>
	</p>
	<ul>
		<li>Перейти в свой профиль</li>
		<li>Перейти в настройки</li>
		<li>Перейти в баланс</li>
		<li>Выбрать способ оплаты Банковская карта/Яндекс кошелёк</li>
		<li>Выбрать сумму пополнения (рекомендуем удерживать баланс на 1500 рублеях)</li>
		<li>Нажать кнопку "Пополнить"</li>
		<li>Следовать инструкциям оператора приёма платежей</li>
		<li>Вернуться в раздел баланс и проверить зачисление средств</li>
	</ul>
	<br>
	<p>
		(Сумма внесённая на баланс остаётся недоступной для «Дом Без Забот» до момента оплаты принятия заявки)
	</p>
	<div class="box_image_items">
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/37a/37a5316d3772a94217a816034ff5df36.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/30b/30b92070c534901d2cb046dc4e314a44.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/d27/d27baba43e6ac2e14af94523e2ae6445.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/76a/76a8d50be3f9e60a53180aeee7ac4fe3.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/0f9/0f969a43a5d883c2219aa5c2f0f3d008.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/ded/ded3cfa1d44b2df38b7d2bca04672dd5.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/242/242e05ecd844ea914fdbe600938239e8.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/010/01076598382df7a8bc66766b7863d041.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/cfd/cfdfe514c604017c7891ad1e9f9d64a1.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/c2a/c2a20c782a9070b4add49b6392aa31c6.jpg" alt="">
		</div>
	</div>
</section>
<section>
	<h3>3. Как принять заявку?</h3>
	<ul>
		<li>Перейти в свой профиль</li>
		<li>Перейти в уведомления</li>
		<li>Выбрать понравившуюся заявку</li>
		<li>Прочитать детали заявки. Убедиться в том что сможете её выполнить</li>
		<li>Нажать кнопку "Взять заявку"</li>
		<li>Связаться с заказчиком, выяснить подробности, договориться об удобном времени, договориться о цене</li>
		<li>Выполнить работы</li>
		<li>Войти в приложение и закрыть заявку</li>
	</ul>
	<div class="box_image_items">
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/d44/d444d5748899a3a47aeda2a461520cc5.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/483/483ddd769947213769a6a07274c1119c.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/c1a/c1a85d23611052f90ab98bb95b3d7a69.jpg" alt="">
		</div>
	</div>
</section>
<section>
	<h3>4. Как отказаться от заявки и в каких случаях происходит возврат комиссии?</h3>
	<p>
		<strong>В том случае если у Вас возникла спорная ситуация:</strong>
	</p>
	<br>
	<ul>
		<li>клиент не согласен с ценой</li>
		<li>при выезде на объект выяснилось объем работ и цена не соответствуют друг-другу</li>
		<li>клиент ведет себя неадекватно</li>
	</ul>
	<br>
	Во всех вышеперечисленных случаях, но не ограничиваясь ими у Вас есть возможность произвести отмену заявки и вернуть комиссию. Для возврата средств необходимо связаться со службой поддержки и описать сложившуюся ситуацию <br>
	<br>
	<br>
	<p>
		<strong>Как вернуть комиссию по не актуальной заявке:</strong>
	</p>
	<ul>
		<li>Для Вашего удобства работает отдельный чат по возврату комиссии.</li>
		<li>Ссылку на чат можно найти в приложении нажав знак вопроса в нижней части экрана.</li>
		<li>Также при возникновении затруднений в работе с приложением существует возможность обратиться в чат технической поддержки, при сложном вопросе обратиться в чат whatsapp либо позвонить по указанному номеру.</li>
	</ul>
	<div class="box_image_items">
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="http://dom-bez-zabot.online/upload/medialibrary/10c/10c7a066c48d553dda98f1575cc108b3.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="http://dom-bez-zabot.online/upload/medialibrary/b2a/b2ab717592516d88bff44dbfc1261673.jpg" alt="">
		</div>
	</div>
</section>
<section>
	<h3>5. Как найти и закрыть заявку после выполнения работ?</h3>
	<ul>
		<li>Открыть профиль</li>
		<li>Перейти в историю заявок</li>
		<li>Выбрать заявку во вкладке "открыто"</li>
		<li>Нажать кнопку "Выполнено"</li>
	</ul>
	<p>
		Заявка будет перемещена в архив после подтверждения закрытия менеджером
	</p>
	<div class="box_image_items">
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/339/339c3e45ba8f187a3649a37175546159.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/bf2/bf27c02a64b28febf8313682a7429440.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/698/698a77b23ba9da3402624ca413231682.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/698/698a77b23ba9da3402624ca413231682.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/0dd/0dd92de409f54a1da0f39d094da0c187.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/ca2/ca2d3421d727e2fcf7c7a3a3d61a3a5f.jpg" alt="">
		</div>
		<div class="box_image_item">
			<img src="null" class="lazy" data-src="https://dom-bez-zabot.net/upload/medialibrary/7c0/7c03d8cb8b9217fe2e68d295a241acdc.jpg" alt="">
		</div>
	</div>
</section>
<section>
	<h3>6. Для чего нам необходим скан/фото Вашего паспорта?</h3>
	<p>
		Для того чтобы подтвердить личность и Вашу надёжность заказчику услуги.
	</p>
	<p>
		Предоставляя копию документа, Вы заявляете, факт того, что готовы нести ответственность за качество выполняемых работ.
	</p>
	<p>
		Процедура проверки документов строго конфиденциальна и соответствует действующему законодательству Российской Федерации в области требований по защите персональных данных.
	</p>
	<p>
		Помните! Копия документа без нотариального подтверждения не имеет юридической силы и не может быть использована без Вашего согласия
	</p>
</section>
<section>
	<h3>7. Кто отвечает за качество проведённых работ</h3>
	<p>
		Ответственность за результат лежит на мастере принявшего и выполняющего заявку. Заказчик может затребовать с мастера какие либо документы (договор, расписка, акт выполненных работ) подтверждающие факт выполнения и оплаты работ. Не
		стоит
		этого пугаться, расписка в свободной форме вполне может удовлетворить подобную просьбу.
	</p>
</section>
<section>
	<h3>8. Сколько стоит использование сервиса «Дом Без Забот»?</h3>
	<p>
		По умолчанию присутствие/регистрация и просмотр заявок в сервисе бесплатно. Только в случае если мастер захочет принять понравившуюся ему заявку потребуется пополнение баланса.
	</p>
	<p>
		Ценовая политика сервиса достаточно демократична: так принятие любой заявки стоит 10% от стоимости работ, если в полях цены
		принятия заявки не указано иное.
	</p>
</section>
<section>
	<h3>9. Как быть если выполнение принятой/оплаченной заявки сорвалось?</h3>
	<p>
		В случае возникновения подобных ситуаций наш сервис придерживается правила возврата денежных средств на баланс мастера в приложении в зависимости от обстоятельств повлекших за собой срыв выполнения заявки.
	</p>
	<p>
		В таком случае мастеру необходимо обратиться в техническую поддержку, по телефонам указанным в приложении в рабочее время и описать всю ситуацию детально. После обращения и выяснения обстоятельств, средства будут возвращены на баланс
		мастера в приложении
	</p>
</section>

<section>
	<h3>Приятного пользования приложеннием!</h3>

	<p>Снова воспользоваться данной справкой можно будет вызвав его из основного меню</p>


</section>