<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\UserTable;

class ImMessage extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['USER_ID'] = intval($arParams['USER_ID']);
		
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;	
			
		$dbUser = \CUser::GetList(
			$b,
			$o,
			array(
				'ID' => $USER->GetID() . '|' . $this->arParams['USER_ID']
			),
			array(
				'FIELDS' => array(
					'ID',
					'NAME',
					'LAST_NAME',
					'PERSONAL_PHOTO'
				)
			)
		);	
		$nameFormat =\CSite::GetNameFormat(true);
		while ($arUser = $dbUser->fetch())
		{
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : array('src' => '/bitrix/components/mlab/appforsale.im.message/templates/.default/images/no_photo.png'));
			$arUser['FORMAT_NAME'] = \CUser::FormatName($nameFormat, $arUser);
			$this->arResult['USERS'][$arUser['ID']] = $arUser;
		}
		
		$this->IncludeComponentTemplate();
	}
}
?>