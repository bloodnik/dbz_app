(function (window) {
	
	window.Im = function (arParams)
	{
	
		this.audio = {};
		this.audio.newMessage1 = null;
		this.audio.current = null;

		//this.desktop = params.desktopClass;
		
		this.start_message_id = 0;
		
		this.progress = true;
		
		this.user_id = 0;
		this.from_id = 0;
		
		this.pathToAjax = '/bitrix/components/mlab/appforsale.im.message/ajax.php?';
		
		this.users = null;
		
		this.visual = {
			HISTORY: '',
			TYPER: '',
			INPUT: '',
			CONTROL: '',
			BTN: ''
		};

		this.obHistory = null;
		this.obTyper = null;
		this.obInput = null;
		this.obControl = null;
		this.obBtn = null;
		
		if ('object' === typeof arParams)
		{
			this.user_id = arParams.user_id;
			this.from_id = arParams.from_id;
			this.users = arParams.USERS;
			this.visual = arParams.VISUAL;
		}
		
		this.timeout = null;
		this.lastActivity = null;
		

//		this.notify = new BX.Notify(this, {
//			'desktopClass': true,
//			//'webrtcClass': this.webrtc,
//			'domNode': BX(this.visual.HISTORY),
//			//'counters': params.counters || {},
//		//	'mailCount': params.mailCount || 0,
//		//	'notify': params.notify || {},
//		//	'unreadNotify' : params.unreadNotify || {},
//		//	'flashNotify' : params.flashNotify || {},
//			//'countNotify' : params.countNotify || 0,
//			//'loadNotify' : params.loadNotify
//		});
		
		
		
		
		
		
		
		
		
		BX.ready(BX.delegate(this.Init, this));
		
		
		
		
		this.queue = [];
		
		
		
	}
	
	
	window.Im.prototype.stopSound = function()
	{
		if (this.audio.current)
		{
			this.audio.current.pause();
		//	this.audio.current.currentTime = 0;
		}	
	}
	
	window.Im.prototype.playSound = function(sound)
	{
		try
		{
			this.stopSound();
			this.audio.current = this.audio[sound];
			var result = this.audio[sound].play();
			if(window.Promise && result instanceof Promise)
			{
				result.catch(function(e)
				{
					this.audio.current = null;
				});
			}
		}
		catch(e)
		{
			this.audio.current = null
		}	
	}
	
	window.Im.prototype.Scroll = function(e)
	{	
		if (BX.scrollTop(window) < 500 && this.progress == false)
		{
			this.getHistory(0, 50, this.user_id);
		}
	}
	
	window.Im.prototype.Init = function()
	{
		
		
		
		
		
		
		
		setTimeout(BX.delegate(function() {
			
			document.addEventListener('scroll', BX.delegate(this.Scroll, this));
		}, this), 300);
		//document.body.style.overflow = 'hidden';
		
		this.obHistory = BX(this.visual.HISTORY);
		this.obHistory.appendChild(this.audio.newMessage1 = BX.create('audio', { props : { className : 'bx-messenger-audio' }, children : [
	                                                                                                                               			BX.create("source", { attrs : { src : "/bitrix/components/mlab/appforsale.im.message/templates/.default/audio/new-message-1.ogg", type : "audio/ogg; codecs=vorbis" }}),
	                                                                                                                               			BX.create("source", { attrs : { src : "/bitrix/components/mlab/appforsale.im.message/templates/.default/audio/new-message-1.mp3", type : "audio/mpeg" }})
	                                                                                                                               		]}));
	
	
	
		if (!!this.obHistory)
		{
			this.getHistory(0, 20, this.user_id);
		}
		
		
		target = this.obHistory.parentNode;
		while (target && target != document.body)
        {
			target.style.height = '100%';
        	target = target.parentNode;
    	}

		this.obTyper = BX(this.visual.TYPER);
		
		this.obInput = BX(this.visual.INPUT);
		this.obControl = BX(this.visual.CONTROL);
		this.obBtn = BX(this.visual.BTN);
		
		if (!!this.obInput && !!this.obControl && !!this.obBtn)
		{
			this.obInput.style.visibility = 'visible';
			BX.bind(this.obInput, 'submit', BX.delegate(this.Submit, this));
			BX.bind(this.obControl, 'keyup', BX.delegate(this.CheckControl, this));
			BX.bind(this.obBtn, 'touchstart', BX.delegate(this.Submit, this));
		}
		
//		
//		if ('ontouchstart' in window)
//		{
//			BX.bind(this.obControl, 'focus', BX.delegate(function(e) {
//				BX.addClass(document.body, 'fixfixed');
//			}, this));
//			
//			BX.bind(this.obControl, 'blur', BX.delegate(function(e) {
//				BX.removeClass(document.body, 'fixfixed');
//			}, this));
//		}
//		
		

		BX.addCustomEvent('message', BX.delegate(function(data) {
			
			if (data.out == 1)
			{
				if (this.queue.length == 0 && data.user_id == this.user_id)
				{			
					this.addMessage(data);
				}
			}
			else if (data.user_id == this.user_id)
			{
				this.hideTyper();				
				this.addMessage(data);
				this.markAsRead();	
			}
		}, this));
		
		BX.addCustomEvent('activity', BX.delegate(function(data) {
			
			if (data.from_id == this.user_id)
			{
				this.showTyper();
			}
			
		}, this));
		
		
		BX.addCustomEvent('markAsRead', BX.delegate(function(data) {
			
			if (data.user_id == this.user_id)
			{
				this.markAsRead2();
			}
			
		}, this));
		
	}
	
	
	window.Im.prototype.CheckControl = function(e)
	{
		if (this.obControl.value.length <= 0)
		{
			this.obBtn.setAttribute('disabled', 'disabled');
		}
		else
		{
			this.obBtn.removeAttribute('disabled');
			this.setActivity();
		}
	}

	window.Im.prototype.Submit = function(e)
	{
		message = this.obControl.value;
		if (message.length > 0)
		{
			this.Enqueue(message);
			this.obInput.reset();
			this.lastActivity = null;
			this.CheckControl();
		}
		return BX.PreventDefault();
	}
	
	window.Im.prototype.Enqueue = function(body)
	{
		//body = BX.util.htmlspecialchars(body);
		message = this.addMessage({body: body})
		this.queue.push(message);
		
		if (this.queue.length == 1)
			this.Send();
	}
	
	window.Im.prototype.addMessageNode = function(arMessage, prepend)
	{
		if (!arMessage.date)
		{
			var date = new Date();
			arMessage.date = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();		
		}
		
		if (!arMessage.out)
		{
			arMessage.out = 1;
		}
		
		if (arMessage.out == 1)
		{
			arMessage.user_id = this.from_id;
		}
		else
		{
			arMessage.user_id = this.user_id;
		}
		
		if (!arMessage.read_state && arMessage.out == 1)
		{
			arMessage.read_state = 0;
		}
		
		// id
		// out
		// read_state
		
		var message = BX.create('div', {
				props: {
					className: 'im-message' + (arMessage.out == 1 && arMessage.read_state == 0 ? ' read_state' : '')
				},
				html: '<div class="im-message-image"><img class="im-message-img" src="' + this.users[arMessage.user_id].PERSONAL_PHOTO.src + '" /></div><div class="im-message-info"><div class="im-message-title">' + this.users[arMessage.user_id].FORMAT_NAME + '<span class="im-message-date">' + arMessage.date + '</span></div><div class="im-message-body">' + arMessage.body + '</div></div>'
		});
		
		
		
		//<div class="afs-im-message-image"><img class="afs-im-message-img" src="' + src + '" /></div><div class="afs-im-message-body"><div style="color: #ccc; font-size: 12px;">' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + '</div><div>' + body + '</div></div><span></span>
	//	alert(BX.height(window));
		
//		var image = BX.firstChild(message);
//		var body = BX.nextSibling(image);
//		var status = BX.nextSibling(body);
		
		//alert(BX.scrollTop(document.body));
	//	alert(BX.scrollTop(document.body));
		

			
		
		
		
		//alert(BX.scrollTop(document.body));
		
	//	alert(BX.scrollTop(document.body));
//		   setTimeout(function() {
//			   BX.scrollTop(document.body, 999999);
//	       
//	        }, 1000);
//		
	//	BX.scrollTop(document.body, 510);
	
		
		
		return {body: arMessage.body, node: message};
	}
	

	
	window.Im.prototype.addMessage = function(arMessage, prepend)
	{
		message = this.addMessageNode(arMessage, prepend);
		
		if (prepend)
			BX.prepend(message.node, this.obHistory);
		else
		{
			BX.append(message.node, this.obHistory);
		//	BX.scrollTop(document.body, 0);
			BX.scrollToNode(message.node);
			

		//	BX.focus(this.obBtn);
			//BX.focus(this.obControl);
	
//			var scrollTop = BX.scrollTop(window);
//			BX.append(message.node, this.obHistory);
//			scrollTop = scrollTop + message.node.offsetHeight;
//			BX.scrollTop(document.body, scrollTop);
		}
		
		return message.body;
	}
	
	
	
	window.Im.prototype.Send = function()
	{
//		console.log( {
//			'SEND_MESSAGE' : 'Y',
//			'sessid': BX.bitrix_sessid(),
//			'message' : this.queue[0],
//			'user_id': this.user_id
//		});
		var _ajax = BX.ajax({
				url: this.pathToAjax + 'SEND_MESSAGE',
				method: 'POST',
				dataType: 'json',
				data: {
					'SEND_MESSAGE' : 'Y',
					'sessid': BX.bitrix_sessid(),
					'message' : this.queue[0],
					'user_id': this.user_id
				},
				onsuccess: BX.delegate(function(data)
				{
					//alert(data)
					
					//this.queue[0].style.background = '#f3f3f3';
					this.queue.shift();
					
					if (this.queue.length > 0)
						this.Send();
					
				}, this),
				onfailure: BX.delegate(function(data)
				{
					//console.log(data);
					
				}, this)
		});
	}
	
	
	window.Im.prototype.typer = function()
	{
		
		
	}
	
	window.Im.prototype.showTyper = function()
	{
		clearTimeout(this.timeout);
		this.obTyper.style.visibility = 'visible';
		this.timeout = setTimeout(BX.delegate(this.hideTyper, this), 10000);
	}
	
	window.Im.prototype.markAsRead2 = function()
	{
		BX.findChildren(this.obHistory).forEach(function(element){
			element.style.background = '#fff';
		});
		
	}
	
	window.Im.prototype.markAsRead = function()
	{
		var _ajax = BX.ajax({
				url: this.pathToAjax + 'MARK_AS_READ',
				method: 'POST',
				dataType: 'json',
				timeout: 5,
				data: {
					'MARK_AS_READ' : 'Y',
					'sessid': BX.bitrix_sessid(),
					'user_id': this.user_id
				}	
		});
	}

	
	window.Im.prototype.hideTyper = function()
	{
		clearTimeout(this.timeout);
		this.obTyper.style.visibility = 'hidden';
	}
	
	

	
	window.Im.prototype.getHistory = function(offset, count, user_id)
	{		
		console.log('getHistory(' + offset + ', ' + count + ', ' + user_id + ')');
		this.progress = true;
		var _ajax = BX.ajax({
				url: this.pathToAjax + 'GET_HISTORY',
				method: 'POST',
				dataType: 'json',
				data: {
					'GET_HISTORY' : 'Y',
					'sessid': BX.bitrix_sessid(),
					'offset': offset,
					'count': count,
					'start_message_id': this.start_message_id,
					'user_id': user_id
				},
				onsuccess: BX.delegate(function(data)
				{		
					if (this.start_message_id == 0)
						this.markAsRead();
					
					if (data.length > 0)
					{		
						//var children = [];
						var scrollTop = BX.scrollTop(window);
						data.forEach(BX.delegate(function(message, i) {
							// message.offsetHeight
							m = this.addMessageNode(message, true);
							//children.push(m.node);
							
							BX.prepend(m.node, this.obHistory);
							scrollTop = scrollTop + m.node.offsetHeight;

							this.start_message_id = message.id;
							
						}, this));
					

						
//						block = BX.create('div', {
//								children: children.reverse()
//						});
//						var scrollTop = BX.scrollTop(window);
//						BX.prepend(block, this.obHistory);
						
//						m.node
//						var scrollTop = BX.scrollTop(window);
//						
//						//console.log('scrollTop start: ' + scrollTop);
//						
//						BX.create();
						
						
						//console.log('scrollTop end: ' + scrollTop);
						
						BX.scrollTop(document.body, scrollTop);
						
						setTimeout(BX.delegate(function() {
							
							this.progress = false;
						}, this), 5);
						
					}
					
				}, this),
				onfailure: BX.delegate(function(data)
				{
					this.progress = false;
					
				}, this),
	});
		
	}
	
	window.Im.prototype.setActivity = function()
	{
		//alert( this.from_id);
		var date = (+ new Date);
		if (date - this.lastActivity > 5000)
		{
			this.lastActivity = date;
			
			var _ajax = BX.ajax({
					url: this.pathToAjax + 'SET_ACTIVITY',
					method: 'POST',
					dataType: 'json',
					timeout: 5,
					data: {
						'SET_ACTIVITY' : 'Y',
						'sessid': BX.bitrix_sessid(),
						'user_id': this.user_id
					}	
			});
		}
	}

})(window);