<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
	<div class="row">
	<?foreach ($arResult['ITEMS'] as $arItem):?>
		<div class="col-md-4 afs-promo-item">
			<div class="afs-promo-image">
				<?if ($arItem['PREVIEW_PICTURE']):?>
					<img class="afs-promo-img" src="<?=$arItem['PREVIEW_PICTURE']['src']?>" />
				<?endif;?>
			</div>
			<div class="afs-promo-info">
				<div class="afs-promo-name"><?=$arItem['NAME']?></div>
				<div class="afs-promo-desc"><?=$arItem['PREVIEW_TEXT']?></div>
			</div>
		</div>
	<?endforeach;?>
	</div>
<?endif;?>