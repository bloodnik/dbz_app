<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class InviteOut extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['INVITE_LEN'] = $arParams['INVITE_LEN'] ?: 8;
		$arParams['MESS_LEAD'] = $arParams['MESS_LEAD'] ?: Loc::getMessage('INVITE_LEAD');
		$arParams['MESS_DESCRIPTION'] = $arParams['MESS_DESCRIPTION'] ?: Loc::getMessage('INVITE_DESCRIPTION');
		
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		
		if (Loader::includeModule('iblock'))
		{
			$invite_count = intval(\Bitrix\Main\Config\Option::get('mlab.appforsale', 'invite_count', 0));
			
			$dbElement = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
					'CREATED_BY' => $USER->GetID()
				),
				false,
				false,
				array(
					'NAME',
					'ACTIVE',
					'SHOW_COUNTER'
				)
			);
			if($arElement = $dbElement->GetNext())
			{
				$this->arResult['CODE'] = $arElement['NAME'];
				
				if ($invite_count > 0)
					$this->arResult['REST'] = $invite_count - intval($arElement['SHOW_COUNTER']);
			}
			else
			{
				$this->arResult['CODE'] = randString(intval($this->arParams['INVITE_LEN']), array(
						'ABCDEFGHIJKLNMOPQRSTUVWX­YZ',
						'0123456789'
				));
				
				$arFields = array(
					'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
					'NAME' => $this->arResult['CODE']
				);
				$obElement = new CIBlockElement;				
				if ($id = $obElement->Add($arFields))
				{
					if ($invite_count > 0)
						$this->arResult['REST'] = $invite_count;
				}
				else
				{
			
				}
			}
			if ($this->arResult['REST'])
			{
				$this->arResult['REST_TEXT'] = Loc::getMessage('INVITE_COUNT') . ' ' . $this->arResult['REST'] . ' ' . CAppforsaleRecurring::pluralForm(
					$this->arResult['REST'],
					Loc::getMessage('INVITE_PROMOCODE1'),
					Loc::getMessage('INVITE_PROMOCODE2'),
					Loc::getMessage('INVITE_PROMOCODE5')
				);
			}	
		}
		
		$this->includeComponentTemplate();
	}
}
?>