<?
use	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
?>
<div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3" style="padding-top: 15px; padding-bottom: 15px">
	<div class="afs-invite-code"><?=$arResult['CODE']?></div>
	<div class="afs-invite-lead"><?=$arParams['MESS_LEAD']?></div>
	<?if ($arResult['REST']):?>
		<p class="text-muted text-center"><?=$arResult['REST_TEXT']?></p>
	<?endif;?>
	<div class="afs-invite-control">
		<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
		<script src="//yastatic.net/share2/share.js"></script>
		<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter" data-title="<?=$arParams['MESS_DESCRIPTION']?>" data-url="<?='http://'.SITE_SERVER_NAME.'/youdo/invite/'.$arResult['CODE'].'/'?>" data-lang="<?=LANGUAGE_ID?>"></div>
	</div>
</div>