<?php
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$arResult["BADGE_COLOR"] = "";
switch ($arResult['ITEM']["DISPLAY_PROPERTIES"][0]["VALUE"]) {
	case "N":
		$arResult["BADGE_COLOR"] = "afs-badge-success";
		break;
	case "P":
		$arResult["BADGE_COLOR"] = "afs-badge-warning";
		break;
	case "D":
		$arResult["BADGE_COLOR"] = "afs-badge-info";
		break;
	case "F":
		$arResult["BADGE_COLOR"] = "afs-badge-danger";
		break;
}