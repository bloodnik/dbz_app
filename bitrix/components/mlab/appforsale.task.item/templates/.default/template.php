<?

use Bitrix\Main;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if (isset($arResult['ITEM'])) {
	$item    = $arResult['ITEM'];
	$areaId  = $arResult['AREA_ID'];
	$itemIds = array(
		'ID'     => $areaId,
		'WRAP'   => $areaId . '_wrap',
		'EDIT'   => $areaId . '_edit',
		'CANCEL' => $areaId . '_cancel'
	);
	$obName  = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);

	?>
	<li class="afs-notification-item" onclick="app.loadPageBlank({url: '<?=$item['DETAIL_PAGE_URL']?>', title: '<?=htmlspecialchars($item['NAME'])?>'})">
		<div class="row no-gutters mb-2">
			<div class="col-6 d-flex align-items-center">
				<div class=" afs-notification-date"><?=$item['DATE_CREATE']?></div>
			</div>
			<div class="col-6 d-flex align-items-center  justify-content-end">
				<span class="afs-badge <?=$arResult["BADGE_COLOR"]?>"><?=$item["DISPLAY_PROPERTIES"][0]["DISPLAY_VALUE"]?></span>
			</div>
		</div>

		<div class="afs-notification-name"><?=$item['NAME']?></div>
	</li>
<? }; ?>