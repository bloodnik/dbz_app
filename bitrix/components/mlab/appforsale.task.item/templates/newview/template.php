<?
use Bitrix\Main;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (isset($arResult['ITEM']))
{
	$item = $arResult['ITEM'];
	$areaId = $arResult['AREA_ID'];
	$itemIds = array(
		'ID' => $areaId,
		'WRAP' => $areaId.'_wrap',
		'EDIT' => $areaId.'_edit',
		'CANCEL' => $areaId.'_cancel'
	);
	$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);	
?>
<div id="<?=$itemIds['WRAP']?>" class="element_wrap">
	<a class="element" title="<?=$item['NAME']?>" href="<?=$item['DETAIL_PAGE_URL']?>">
		<div class="afs-task-item-row" id="<?=$areaId?>">
						<div class="element__name">
					<?=$item['NAME']?>
				</div>
			<div class="afs-task-item-info">
				<?if (!empty($item['PROPERTIES']['PRICE']['VALUE'])):
					foreach ($item['DISPLAY_PROPERTIES'] as $arProp)
					{
						if ($arProp['CODE'] == 'PRICE'):
						?>
					<span class="bg-warning afs-task-item-price"><?=$arProp['DISPLAY_VALUE']?></span>
					<?
					endif;
					}
				endif;?>
				<?if (!empty($item['DISPLAY_PROPERTIES'])):?>
				<div class="afs-task-item-prop">
					<?
					foreach ($item['DISPLAY_PROPERTIES'] as $arProp)
					{
						if ($arProp['CODE'] != 'PRICE')
							echo '<p>' . $arProp['NAME'] . ': ' . $arProp['DISPLAY_VALUE'] .'</p>';
					}
					?>
				</div>
				<?endif;?>
				
				
				<div class="row">
					<div class="col-xs-6">
					<?
				if ($item['CREATED_BY'] != $USER->GetID())
					$APPLICATION->IncludeComponent(
						'mlab:appforsale.user.item',
						'',
						array(
							'RESULT' => array(
								'ITEM' => $item['USER'],
								'AREA_ID' => $this->GetEditAreaId($item['CREATED_BY']),
								'TYPE' => 'LINE'
							)
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);
				?>
					</div>
					<div class="col-xs-6 text-right">
						<div class="element__section"><?=$item['IBLOCK_SECTION']['NAME']?></div>
						
					</div>
				</div>
				
			
				
			</div>
	
			
			<?if ($arResult['MORE'] == 'N'):?>
				<span class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" onclick="">
						<img class="afs-task-item-more" src="<?=$templateFolder?>/images/more.png" /></a><ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dLabel">
						<li><a id="<?=$itemIds['EDIT']?>" tabindex="-1" href="/tasks/new/">Редактировать</a></li>
		    			<li><a id="<?=$itemIds['CANCEL']?>" tabindex="-1" onclick="">Отменить</a></li>
		 			</ul>
				</span>	
				
				<? 
				$signer = new Main\Security\Sign\Signer;
				$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'appforsale.offer');
					
				$arJSParams = array(
					'ID' => $item['ID'],
					'signedParamsString' => CUtil::JSEscape($signedParams),
					'ajaxUrl' => CUtil::JSEscape($component->getPath().'/ajax.php'),
					'VISUAL' => $itemIds
				);
				echo '<script type="text/javascript">'.$obName.' = new JCTaskCustomer('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
				?>		
					
			<?endif;?>	
		</div>
	</a>
</div>
<?
// <!-- 		<div class="dropdown afs-task-item-more"> -->
// <img class="afs-task-item-more" src="<?=$templateFolder/images/more.png" />
// <!--   <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"> -->


// <!--   </ul> -->
// <!-- </div> -->
}
?>