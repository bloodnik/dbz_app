<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class Executor extends CBitrixComponent
{
	public function executeComponent()
	{
		$arDefaultUrlTemplates404 = array(
			'list' => '#SECTION_CODE_PATH#/',
			'detail' => 'u#USER_ID#/',
			'profile' => 'u#USER_ID#/#ELEMENT_ID#/'
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'ELEMENT_ID',
			'ELEMENT_CODE',
			'SECTION_ID',
			'SECTION_CODE',
			'USER_ID'
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
 			$arVariables = array();
			$engine = new CComponentEngine($this);
			if (\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$engine->addGreedyPart("#SECTION_CODE_PATH#");
				$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
			}
 			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'list';
		
			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);	
		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>
