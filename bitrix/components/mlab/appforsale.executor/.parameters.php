<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arComponentParameters = array(
	'GROUPS' => array(
		'COMMENT_SETTINGS' => array(
			'NAME' => Loc::getMessage('COMMENT_SETTINGS')
		),		
		'LIST_SETTINGS' => array(
			'NAME' => Loc::getMessage('LIST_SETTINGS')
		),
		'FILTER_SETTINGS' => array(
			'NAME' => Loc::getMessage('FILTER_SETTINGS')
		),
// 		'OFFER_SETTINGS' => array(
// 			'NAME' => Loc::getMessage('OFFER_SETTINGS')
// 		),
// 		'OFFER_FORM_SETTINGS' => array(
// 			'NAME' => Loc::getMessage('OFFER_FORM_SETTINGS')
		//)
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'COMMENT_IBLOCK_TYPE' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'COMMENT_IBLOCK_ID' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['COMMENT_IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'COMMENT_PROPERTY_CODE' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['COMMENT_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
			'CNT' => array(
					'PARENT' => 'FILTER_SETTINGS',
					'NAME' => Loc::getMessage('FILTER_SETTINGS_CNT'),
					'TYPE' => 'CHECKBOX',
					'DEFAULT' => 'N'
			),
			
			
			
		'CACHE_TIME' => array('DEFAULT' => 36000000),
		'SEF_MODE' => array(
			'list' => array(
				'NAME' => Loc::getMessage('SEF_MODE_LIST'),
				'DEFAULT' => '',
				'VARIABLES' => array()
			),
			'detail' => array(
				'NAME' => Loc::getMessage('SEF_MODE_DETAIL'),
				'DEFAULT' => '#USER_ID#/',
				'VARIABLES' => array(
					'USER_ID'
				)
			)
		)
	)
);

// if ($arCurrentValues['SEF_MODE'] == 'Y')
// {
// 	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES'] = array();
// 	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_ID'] = array(
// 		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_ID'),
// 		'TEMPLATE' => '#SECTION_ID#'
// 	);
// 	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE'] = array(
// 		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE'),
// 		'TEMPLATE' => '#SECTION_CODE#'
// 	);
// 	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE_PATH'] = array(
// 		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE_PATH'),
// 		'TEMPLATE' => '#SECTION_CODE_PATH#'
// 	);
// }
?>