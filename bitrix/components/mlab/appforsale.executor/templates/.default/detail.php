<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
?>
<div class="col-md-8">
	<div class="row">
		<div class="col-md-12">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.executor.detail',
			'',
			array(
				'ID' => $arResult['VARIABLES']['USER_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
			),
			$component
		);
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.executor.profile.list',
			'',
			array(
				'ID' => $arResult['VARIABLES']['USER_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['profile']
			),
			$component
		);
		?>
		</div>
	</div>
	<h5><?=Loc::getMessage("COMMENTS")?></h5>
	<div class="row">
		<div class="col-md-12">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.comment.list',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['COMMENT_IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['COMMENT_IBLOCK_ID'],
				'USER_ID' => $arResult['VARIABLES']['USER_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'PROPERTY_CODE' => $arParams['COMMENT_PROPERTY_CODE']
			),
			$component
		);
		?>
		</div>
	</div>
	
</div>
<div class="col-md-4">
</div>