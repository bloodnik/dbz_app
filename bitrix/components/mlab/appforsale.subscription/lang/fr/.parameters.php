<?
$MESS['SECTIONS_SETTINGS'] = 'ParamГЁtres de la liste des partitions';
$MESS['FORM_SETTINGS'] = "Personnalisation du formulaire d'Г©dition";
$MESS['IBLOCK_TYPE'] = "Type d'info bloc";
$MESS['IBLOCK'] = 'Info bloc';
$MESS['SEF_MODE_SECTIONS'] = 'Liste des sections';
$MESS['SEF_MODE_FORM'] = 'La forme';
$MESS['SECTIONS_FIELD_CODE'] = 'Les champs';
$MESS['FORM_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'PropriГ©tГ©s obligatoires';
$MESS['FORM_MESS_BTN_ADD'] = 'Le texte du bouton "Enregistrer"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Enregistrer';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = "L'inscription vide propriГ©tГ©s";
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(non dГ©fini)';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = "L'id de la partition";
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Code de caractГЁre de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Chemin de caractГЁres de codes de la section';
?>