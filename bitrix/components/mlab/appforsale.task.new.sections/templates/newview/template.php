<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?if (!empty($arResult["ITEMS"])):?>
<div class="iblock-section-list-item-wrap">
	<?foreach ($arResult["ITEMS"] as $arItem):?>
	<a class="iblock-section-list-item" href="<?=(($arItem['RIGHT_MARGIN'] - $arItem['LEFT_MARGIN']) == 1) ? $arItem['DETAIL_PAGE_URL']."?noHistory=Y" : $arItem['SECTION_PAGE_URL']?>">
		<img <?=$arItem['PICTURE'] ? "" : 'style="display: none" '?>src="<?=$arItem['PICTURE'] ? $arItem['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png'?>" class="iblock-section-list-item__picture" />
		<div class="iblock-section-list-item__info">
			<div class="iblock-section-list-item__name"><?=$arItem["NAME"]?></div>
		</div>
	</a> 
	<?endforeach;?>
</div>
<?endif;?>