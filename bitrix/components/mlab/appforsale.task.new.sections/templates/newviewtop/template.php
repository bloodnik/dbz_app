<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
	<div class="row">
		<?foreach ($arResult['ITEMS'] as $key=>$arItem):?>
		

		
				<?if ($key < 4):?>
					<a class="col-md-3 col-xs-6 afs-section" title="<?=$arItem['NAME']?>" href="<?=(($arItem['RIGHT_MARGIN'] - $arItem['LEFT_MARGIN']) == 1) ? $arItem['DETAIL_PAGE_URL'] : $arItem['SECTION_PAGE_URL']?>">
						<div class="fs-section afs-section-big">
						<div class="afs-section-info"><?=$arItem['NAME']?></div>
						<div class="afs-section-image">
							<img class="afs-section-img" src="<?=$arItem['PICTURE'] ? $arItem['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png'?>" />
						</div>
						</div>
					</a>	
				<?else:?> 
					<a class="col-md-3 afs-section" title="<?=$arItem['NAME']?>" href="<?=(($arItem['RIGHT_MARGIN'] - $arItem['LEFT_MARGIN']) == 1) ? $arItem['DETAIL_PAGE_URL'] : $arItem['SECTION_PAGE_URL']?>">
						<div class="afs-section afs-section-small">
						<div class="afs-section-image">
							<img class="afs-section-img" src="<?=$arItem['PICTURE'] ? $arItem['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png'?>" />
						</div>
						<div class="afs-section-info"><?=$arItem['NAME']?></div>
						</div>
					</a>
				<?endif;?>	
				
				<?if ($key > 0 && ($key + 1) % 4 == 0):?>
					<div class="clearfix"></div>
				<?endif;?>
		<?endforeach;?>
	</div>
<?endif;?>

<?
// http://vps.malahovsky.net/youdo/tasks-new/bytovoy-remont/santekhnicheskie-raboty/form/
?>