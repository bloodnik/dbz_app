<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
		foreach ($arResult['SECTIONS'] as $arSection)
		{
			if ($arSection['ACTIVE'] == 'Y')
			{
				if ($arSection['DEPTH_LEVEL'] == 1)
				{
					echo '<div class="afs-section-1"><b>'.$arSection['NAME'].'</b>';
				}
				else
				{
					echo '<div class="afs-section-2">'.$arSection['NAME'];
				}
				
				if (array_key_exists($arSection['ID'], $arResult['ELEMENTS']))
				{
					echo '<div class="afs-element">';
					foreach ($arResult['ELEMENTS'][$arSection['ID']] as $arElement)
					{
						echo '<a href="'.$arElement['ID'].'/" title="'.$arElement['NAME'].'">'.$arElement['NAME'].'</a>';
					}
					echo '</div>';
				}
				echo '</div>';
			}
		}
?>