<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$frame = $this->createFrame('mlab-pull-start')->begin('');
?>
	<script type="text/javascript">
		BX.ready(function() { MLab.pull.start(<?=(empty($arResult) ? '' : CUtil::PhpToJsObject($arResult))?>); });
	</script>
<?
$frame->end();
?>