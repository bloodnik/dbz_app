<?
use \Bitrix\Main\Loader,
	\Mlab\Appforsale\Pull\Channel;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class PullRequest extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		if (isset($_REQUEST['AJAX_CALL']) && $_REQUEST['AJAX_CALL'] == 'Y')
			return;
		
		if (!Loader::includeModule('mlab.appforsale'))
			return;
	
		if (defined('BX_PULL_SKIP_INIT'))
			return;
		
		$userId = 0;
		
		if ($GLOBALS['USER'] && intval($GLOBALS['USER']->GetID()) > 0)
		{
			$userId = intval($GLOBALS['USER']->GetID());
		}
		
		if ($userId == 0)
			return;
		
		CJSCore::Init(array('push'));
		$this->arResult = Channel::getConfig($userId);
			

		define("BX_PULL_SKIP_INIT", true);
		$this->includeComponentTemplate();
	}
}
?>