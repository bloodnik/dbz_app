<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

if (!CModule::IncludeModule('mlab.appforsale'))
{
	CMain::FinalActions();
	die();
}


if(!$USER->IsAuthorized())
{
	$USER->LoginByCookies();
}

$userId = intval($USER->GetID());
if ($userId <= 0)
{
	CMain::FinalActions();
	die();
}

if (check_bitrix_sessid())
{
	if ($_POST['PULL_GET_CHANNEL'] == 'Y')
	{
		session_write_close();

		$arConfig = Mlab\Appforsale\Pull\Channel::getConfig($userId);
		if (is_array($arConfig))
		{
			echo CUtil::PhpToJsObject($arConfig);
		}
		else
		{
			echo CUtil::PhpToJsObject(array('ERROR' => 'ERROR_OPEN_CHANNEL'));
		}
	}
}
CMain::FinalActions();
die();
?>