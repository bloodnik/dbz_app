<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arComponentParameters = array(
	'GROUPS' => array(
		'SECTIONS_SETTINGS' => array(
			'NAME' => Loc::getMessage('SECTIONS_SETTINGS')
		),
		'FORM_SETTINGS' => array(
			'NAME' => Loc::getMessage('FORM_SETTINGS')
		)
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'CACHE_TIME' => array('DEFAULT' => 36000000),
		'SEF_MODE' => array(
			'sections' => array(
				'NAME' => Loc::getMessage('SEF_MODE_SECTIONS'),
				'DEFAULT' => '#SECTION_CODE_PATH#/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH'
				)
			),
			'form' => array(
				'NAME' => Loc::getMessage('SEF_MODE_FORM'),
				'DEFAULT' => '#SECTION_CODE_PATH#/form/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH'
				)
			)
		),
		'SECTIONS_FIELD_CODE' => CIBlockParameters::GetSectionFieldCode(Loc::getMessage('SECTIONS_FIELD_CODE'), 'SECTIONS_SETTINGS'),
		'FORM_PROPERTY_CODE' => array(
			'PARENT' => 'FORM_SETTINGS',
			'NAME' => Loc::getMessage('FORM_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'FORM_PROPERTY_CODE_REQUIRED' => array(
			'PARENT' => 'FORM_SETTINGS',
			'NAME' => Loc::getMessage('FORM_PROPERTY_CODE_REQUIRED'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'FORM_MESS_PROPERTY_VALUE_NA' => array(
			'PARENT' => 'FORM_SETTINGS',
			'NAME' => Loc::getMessage('FORM_MESS_PROPERTY_VALUE_NA'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('FORM_MESS_PROPERTY_VALUE_NA_DEFAULT')
		),
		'FORM_MESS_BTN_ADD' => array(
			'PARENT' => 'FORM_SETTINGS',
			'NAME' => Loc::getMessage('FORM_MESS_BTN_ADD'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('FORM_MESS_BTN_ADD_DEFAULT')
		)
	)
);

if ($arCurrentValues['SEF_MODE'] == 'Y')
{
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES'] = array();
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_ID'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_ID'),
		'TEMPLATE' => '#SECTION_ID#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE'),
		'TEMPLATE' => '#SECTION_CODE#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE_PATH'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE_PATH'),
		'TEMPLATE' => '#SECTION_CODE_PATH#'
	);
}
?>