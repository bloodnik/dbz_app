<?
$MESS['SECTIONS_SETTINGS'] = 'set list of topics';
$MESS['FORM_SETTINGS'] = 'form Customization-editing';
$MESS['IBLOCK_TYPE'] = 'Type of the information block';
$MESS['IBLOCK'] = 'information block';
$MESS['SEF_MODE_SECTIONS'] = 'List of topics';
$MESS['SEF_MODE_FORM'] = 'Form';
$MESS['SECTIONS_FIELD_CODE'] = 'Fields';
$MESS['FORM_PROPERTY_CODE'] = 'Properties';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['FORM_MESS_BTN_ADD'] = 'the text of the button "Save"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Save';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'Inscription blank properties';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not set)';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'topic ID';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Character code of the section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'the Path of the symbolic codes section';
?>