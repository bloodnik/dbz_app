<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskNew extends CBitrixComponent
{
	public function executeComponent()
	{
		$arDefaultUrlTemplates404 = array(
			'form' => '#SECTION_CODE_PATH#/form/',
			'sections' => '#SECTION_CODE_PATH#/'
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'ID',
			'SECTION_ID',
			'SECTION_CODE'
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
			$arVariables = array();
			$engine = new CComponentEngine($this);
			if (\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$engine->addGreedyPart("#SECTION_CODE_PATH#");
				$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
			}
			
			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'sections';

		
			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);		

		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>
