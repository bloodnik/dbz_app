<?
use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class InterfaceIblock extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{		
		global $USER;
		Loader::includeModule('iblock');
		if (intval($arParams['PID']) > 0)
		{
			Loader::includeModule('iblock');
			$dbProperty = CIBlockProperty::GetByID($arParams['PID']);
			if($arProperty = $dbProperty->GetNext())
			{
				if ($arProperty['USER_TYPE'] == 'iblock')
				{
					$arParams['IBLOCK_ID'] = $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'];
					if ($arProperty['USER_TYPE_SETTINGS']['CREATED_BY'] == 'Y')
						$arParams['CREATED_BY'] = $USER->GetID();
				}
			}
		}
		else
		{
			global $USER_FIELD_MANAGER;
			$arFields = $USER_FIELD_MANAGER->GetUserFields('USER');
			$arParams['IBLOCK_ID'] = $arFields[$arParams['PID']]['SETTINGS']['IBLOCK_ID'];
		}


		$arParams['PARENT_SECTION'] = isset($arParams['PARENT_SECTION']) ? (int) $arParams['PARENT_SECTION'] : 0;
		
		return $arParams;
	}
	
	public function executeComponent()
	{					
		$dbElement = CIBlockSection::GetMixedList(
			array(
				'sort' => 'asc'
			),
			array(
				'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
				'ACTIVE' => 'Y',
				'SECTION_ID' => $this->arParams['PARENT_SECTION']
			),
			true,
			array(
				'ID',
				'NAME',
				'CREATED_BY'
			)
		);
		
		while($arElement = $dbElement->GetNext())
		{
			if ($this->arParams['CREATED_BY'] > 0 && $arElement['TYPE'] == 'E' && $arElement['CREATED_BY'] != $this->arParams['CREATED_BY'])
				continue;
			
			$this->arResult['ITEMS'][] = $arElement;
		}
		
		$this->IncludeComponentTemplate();
	}
}