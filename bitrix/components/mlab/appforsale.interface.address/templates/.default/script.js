/* mlab:appforsale.interface.address */
(function(window) {

	if (!!window.JCAddress)
	{
		return;
	}
	
	window.JCAddress = function(arParams)
	{
		this.myPosition = null;
		this.bPosition = false;
		
		this.obMap = null;
		this.map = null;
		
		
		this.obBlock = null;
		this.obProgress = null;
		
		this.timeout = null;
		
		this.bounds = null;
		this.value = [];
		this.placeholder = '';
		this.multiple = false;
		this.numbers = ['A','B','C','D','E','F','G','H','I','J'];

		this.id = 0;
		this.address = [];
		
		this.visual = {
			LIST: '',
			MAP: '',
			ADD: '',
			INDICATE: '',
			BLOCK: '',
			PROGRESS: ''
		};
	
		this.obList = null;
		this.obAdd = null;
		this.obIndicate = null;
		
		if ('object' === typeof arParams)
		{
			this.placeholder = arParams.PLACEHOLDER;
			this.id = arParams.ID;
			this.multiple = (arParams.MULTIPLE == 'N' ? false : true);
			this.visual = arParams.VISUAL;
		}
		
		
		BX.ready(BX.delegate(this.Check, this));
	}
	
	window.JCAddress.prototype.add = function(arParams)
	{
		this.value.push(arParams);
	}
	
	window.JCAddress.prototype.Check = function()
	{
		if (window.ymaps && window.ymaps.Map)
		{
			this.Init();
		}
		else
		{
			setTimeout(BX.delegate(this.Check, this), 50);
		}
		
		
//		function initMapEditor(config) {
//
//		    if (typeof window.ymaps !== "undefined") {
//
//		        window.ymaps.ready(function() {
//
//		            // понеслась
//
//		        });
//
//		    } else {
//
//		        // пока рано
//
//		        window.setTimeout(function () {initMapEditor(config)}, 100);
//
//		    }
//
//		};
		
		
		
		
		
		
		
	}
	

	
	window.JCAddress.prototype.showMyPosition = function(coords)
	{
		this.bPosition = true;
		if (this.myPosition != null)
		{
			this.addObject(this.myPosition)
		}
	}
	
	window.JCAddress.prototype.addObject = function(coords)
	{
		ymaps.geocode(coords, { results: 1 }).then(BX.delegate(function (res) {
			if (res.metaData.geocoder.results == 1)
			{
				var item = res.geoObjects.get(0);
				if (item.getPremiseNumber())
				{									
					for (var i = this.address.length - 1; i >= 0; i--)
					{
						if (!this.address[i].loc || !this.multiple)
						{
							this.address[i].remove();	
							this.address.splice(i, 1);
						}
					}

					var formatted = this.getFormatted(item);
					
					if (this.multiple || this.address.length == 0)
					{
						var result = this.addInput(null, {
							value: formatted + "|" + coords[0] + "," + coords[1],
							name: formatted
						});
						this.address[result.guid-1].loc = coords;
						
						this.refreshMap();
						this.refreshAddon();
					}
				}
			}
		}, this));
//	}
	}

	window.JCAddress.prototype.Init = function()
	{
		this.obMap = BX(this.visual.MAP);
		
		this.map = new ymaps.Map(this.obMap, {
			center: [55.76, 37.64],
			zoom: 7,
			margin: 20,
			controls: ['zoomControl', 'fullscreenControl', 'geolocationControl']
		}, { suppressMapOpenBlock: true });
		
		this.map.events.add('click', BX.delegate(function (e) {
			
			this.addObject(e.get('coords'));
			
		}, this));

		ymaps.geolocation.get({ mapStateAutoApply: true}).then(BX.delegate(function (res) {
		
			if (res.geoObjects)
			{
				this.myPosition = res.geoObjects.position
					
				if (this.bPosition)
				{
					this.addObject(this.myPosition)
				}
					
				this.map.setCenter(res.geoObjects.position, 14);
			}			
		}, this));
		

		
		
		
		this.obBlock = BX(this.visual.BLOCK);
		this.obProgress =  BX(this.visual.PROGRESS);
		this.obList = BX(this.visual.LIST);
		this.obAdd = BX(this.visual.ADD);
		this.obIndicate = BX(this.visual.INDICATE);
		
		if (!!this.obAdd)
		{
			BX.bind(this.obAdd, 'click', BX.delegate(this.addInput, this));
		}
		
		if (!!this.obIndicate)
		{
			BX.bind(this.obIndicate, 'click', BX.delegate(function() {
				this.showMyPosition();
			}, this));
		}
		
		if (this.value.length > 0)
		{
			for (var i = 0; i < this.value.length; i++)
			{
				var guid = this.addInput(null, this.value[i]);
				var latLng = this.value[i].loc.split(',');
				this.address[(guid.guid-1)].loc = [latLng[0],latLng[1]];
			}
			this.refreshMap();
		}
		else
		{
			if (this.multiple)
				this.addInput();
			this.addInput();
		}
		
		BX.show(this.obBlock);
		BX.hide(this.obProgress);
	}
	
	window.JCAddress.prototype.showMap = function()
	{
		BX.show(this.obMap);
		if (!!this.obIndicate)
			BX.hide(this.obIndicate);
	}
	
	window.JCAddress.prototype.hideMap = function()
	{
		BX.hide(this.obMap);
		if (!!this.obIndicate)
			BX.show(this.obIndicate);
	}
	
	window.JCAddress.prototype.getPoints = function()
	{
		var res = [];
		for (var i = 0; i < this.address.length; i++)
		{
			if (this.address[i].loc)
				res.push(this.address[i]);
		}
		return res;
	}
	
	window.JCAddress.prototype.refreshMap = function()
	{
		this.map.geoObjects.removeAll();

		var address = this.getPoints();

		if (address.length > 0)
		{
			this.bounds = [];
			for (var i = 0; i < address.length; i++)
			{
				this.bounds.push(address[i].loc);
			}
			
		
			var option = {};
			if (address.length == 1)
			{
				this.map.setCenter(this.bounds[0], 16);
			}
			else
			{
				option = {
						boundsAutoApply: true
				};
			}
			
			
			var route = new ymaps.multiRouter.MultiRoute(
					{
						referencePoints: this.bounds,
						params: { results: 2 }
					},
					option
			);
			
			
			
//			route.model.events.add("requestsuccess", function (event) {
//				var routes = event.get("target").getRoutes();
//				if (routes.length > 0)
//				{
//					var m = routes[0].properties.get("distance").value;
//					
//					var priceBoarding = 50;
//					var priceKilometer = 15;
//					
//					var price = priceBoarding + m * (priceKilometer / 1000);
//					console.log('price: ' + price);
//				}
//				
//		        
//		    });

			this.map.geoObjects.add(route);
			this.showMap();
		}
		else
		{
			this.hideMap();
		}	
	}
	
	window.JCAddress.prototype.refreshAddon = function(parent)
	{		
		for (var i = 0; i < this.address.length; i++)
		{
			BX.html(this.address[i].addon, (this.numbers[i] ? this.numbers[i] : '●'));
			if (i == 0)
			{
				this.address[i].addon.style.background = '#F23D30';
				this.address[i].addon.style.borderColor = '#F23D30';
			}
			else if (i == this.address.length - 1)
			{
				this.address[i].addon.style.background = '#4296EA';
				this.address[i].addon.style.borderColor = '#4296EA';
			}
			else
			{
				this.address[i].addon.style.background = '#71B732';	
				this.address[i].addon.style.borderColor = '#71B732';
			}
		}
		
	}
	
	window.JCAddress.prototype.Input = function(parent, arParams)
	{	
		if (arParams === undefined)
		{
			arParams = {
				value: '',
				name: ''
			};
		}
		
		var ret = BX.create('div', { props: { className: 'afs-address-item input-group' + (parent.address.length > 0 ? '' : '') }, html: '<span class="input-group-addon afs-addon"></span><input type="hidden" name="PROPERTY[' + parent.id + '][' + parent.address.length + ']" value="' + arParams.value + '" data-check="' + (arParams.value.length > 0 ? 'Y' : 'N') + '" /><input class="form-control" type="text" placeholder="' + parent.placeholder + '" autocomplete="off" value="' + arParams.name + '" /><ul class="afs-address-list"></ul><span class="input-group-btn"><button class="btn btn-default" type="button">x</button>' + (parent.address.length > 0 ? '<button class="btn btn-default" type="button">-</button>' : '') + "</span>"});
		
		var addon = BX.firstChild(ret);
		var val = BX.nextSibling(addon);
		var input = BX.nextSibling(val);
		var ul = BX.nextSibling(input);
		var group = BX.nextSibling(ul);
		var clear = BX.firstChild(group);
		var close = BX.nextSibling(clear);

		function _remove() {
			BX.remove(ret);
		}
		
		var guid = parent.address.push({
			input: input,
			ul: ul,
			addon: addon,
			remove: _remove
		});

		if (!!clear)
		{
			BX.bind(clear, 'click', BX.delegate(function() {
				input.value = "";
			}, this));
		}
		
		if (!!close)
		{			
			BX.bind(close, 'click', BX.delegate(function() {
				var index = Array.prototype.indexOf.call(BX.findParent(ret).childNodes, ret);
				BX.PreventDefault();
				BX.remove(ret);
				this.address.splice(index, 1);
				this.refreshAddon();
				this.refreshMap();
			}, parent));
		}

		BX.bind(input, 'blur', BX.delegate(function() {
			
			if (this.timeout)
				clearTimeout(this.timeout);
			
			val.setAttribute("data-check", "N");
			
			setTimeout(BX.delegate(function() {
				BX.hide(ul);
				if (val.value == '' && input.value.length > 0 && val.getAttribute('data-check') == "N")
				{
					input.style.background = '#f2dede';
					this.refreshMap();
				}
			}, this), 300);
		}, parent));
		
		BX.bind(input, 'keyup', BX.delegate(function() {
			
			input.style.background = '#fff';
			
			if (this.timeout)
				clearTimeout(this.timeout);
			
			if (input.value.length > 3)
			{	
				this.timeout = setTimeout(BX.delegate(function() {
			
					val.value = '';
					val.setAttribute('data-check', 'N');
					this.address[guid-1].loc = false;	
					ymaps.geocode(input.value, { boundedBy: this.map.getBounds(), results: 5}).then(BX.delegate(function (res) {
							
						if (res.metaData.geocoder.results > 0)
							BX.show(ul);
						else
							BX.hide(ul);
							
						var loc = [], formatted_address = [];
						BX.html(ul, '');
							
						i = 0;
						res.geoObjects.each(BX.delegate(function (item) {
							var formatted = this.getFormatted(item);
							loc[i] = item.geometry.getCoordinates();

							formatted_address[i] = formatted;
							var li = BX.create('li', {html: item.properties.get('balloonContent'), attrs: {'data-id': i}, events: { click: BX.delegate(function() { 
									
								var target = BX.proxy_context;
								var id = target.getAttribute('data-id');
								if (id == null)
									id = 0;
								
								input.value = formatted_address[id];
								BX.focus(input);
								
								val.setAttribute("data-check", "Y");
								if (item.getPremiseNumber())
								{
									this.address[guid-1].loc = loc[id];
									this.refreshMap();
									val.value = formatted_address[id] + "|" + loc[id][0] + "," + loc[id][1];
								}
							}, this)}});
							BX.append(li, ul);
								
							i++;	
						}, this));		
					}, this));
				}, this), 500);
			}
			else
			{
				BX.hide(ul);
			}
		}, parent));

		BX.append(ret, parent.obList);
		parent.refreshAddon();
		return {guid: guid};
	}
	
	window.JCAddress.prototype.getFormatted = function(item)
	{
	//	console.log(item.properties);
	//	getBounds
		return item.getLocalities()[0] + (item.getThoroughfare() ? ", " + item.getThoroughfare() : "") + " " + (item.getPremiseNumber() ? item.getPremiseNumber() : "");
	}

	
	window.JCAddress.prototype.addInput = function(e, arParams)
	{		
		return new this.Input(this, arParams);
	}
	
})(window);