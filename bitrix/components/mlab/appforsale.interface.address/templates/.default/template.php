<? 
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
$areaId = $this->GetEditAreaId('');
$itemIds = array(
		'ID' => $areaId,
		'LIST' => $areaId.'_list',
		'MAP' => $areaId.'_map',
		'ADD' => $areaId.'_add',
		'INDICATE' => $areaId.'_indicate',
		'BLOCK' => $areaId.'_block',
		'PROGRESS' => $areaId.'_progress'
);
?>
<div id="<?=$itemIds['PROGRESS']?>" style="min-height: <?=$arParams['MULTIPLE'] == 'Y' ? '108' : '71'?>px"><img class="afs-progress" src="<?=$templateFolder?>/images/progress.gif" /></div>
<div style="display: none" id="<?=$itemIds['BLOCK']?>">
<div id="<?=$itemIds['LIST']?>"></div>
<div>
<?if ($arParams['MULTIPLE'] == 'Y'):?>
	<span class="btn bnt-link" id="<?=$itemIds['ADD']?>"><?=Loc::getMessage('ADD_ADDRESS')?></span> 
<?endif;?>
<span class="btn bnt-link" id="<?=$itemIds['INDICATE']?>"><?=Loc::getMessage('INDICATE_ON_MAP')?></span>
</div>
<div class="map" id="<?=$itemIds['MAP']?>"></div></div>
<? 
$arJSParams = array(
	'ID' => $arParams['ID'],
	'MULTIPLE' => $arParams['MULTIPLE'],
	'PLACEHOLDER' => Loc::getMessage('PLACEHOLDER'),
	'VISUAL' => $itemIds
					);
echo '<script type="text/javascript">var tt = new JCAddress('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
if (!defined('AFS_MAP_SCRIPT_LOADED'))
{
?>
	<script>
		var script = document.createElement('script');
		script.src = 'https://api-maps.yandex.ru/2.1/?lang=<?=LANGUAGE_ID?>';
		(document.head || document.documentElement).appendChild(script);
		script.onload = function () {
			this.parentNode.removeChild(script);
		};
	</script>
<? 
	define('AFS_MAP_SCRIPT_LOADED', 1);
}	
?>