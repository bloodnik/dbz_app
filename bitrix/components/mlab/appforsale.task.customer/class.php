<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskCustomer extends CBitrixComponent
{	
	public function executeComponent()
	{
		global $USER, $USER_FIELD_MANAGER;
		
		if (!$USER->IsAuthorized())
			CAppforsale::AuthForm();
		
		if (empty($USER->GetFirstName()) || empty($USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_CITY", $USER->GetID())))
			LocalRedirect("/youdo/personal/profile/edit.php");
		
		$arDefaultUrlTemplates404 = array(
			'detail' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/',
			'list' => '#SECTION_CODE_PATH#/',
			'sections' => '',
			'form' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/edit/',
			'comment' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/comment/'
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'SECTION_ID',
			'SECTION_CODE',
			'ELEMENT_ID',
			'ELEMENT_CODE',
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
			$arVariables = array();
			$engine = new CComponentEngine($this);
			if (\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$engine->addGreedyPart("#SECTION_CODE_PATH#");
				$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
			}
			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'list';

		
			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);		
		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>
