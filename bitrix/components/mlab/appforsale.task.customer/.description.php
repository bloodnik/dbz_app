<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('TASK_CUSTOMER_NAME'),
	'SORT' => 30,
	'DESCRIPTION' => Loc::getMessage('TASK_CUSTOMER_DESCRIPTION'),
	'COMPLEX' => 'Y',
	'PATH' => array(
		'ID' => 'appforsale',
		'NAME' => Loc::getMessage('TASK_CUSTOMER_PATH_NAME'),
		'SORT' => 1,
		'CHILD' => array(
			'ID' => 'task',
			'NAME' => Loc::getMessage('TASK_CUSTOMER_PATH_CHILD_NAME'),
			'SORT' => 10
		)
	)
);
?>