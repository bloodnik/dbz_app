<?
$MESS['LIST_SETTINGS'] = 'list Settings';
$MESS['DETAIL_SETTINGS'] = 'Settings detailed view';
$MESS['OFFER_SETTINGS'] = 'set suggestions';
$MESS['COMMENT_SETTINGS'] = 'Settings for comments';
$MESS['FORM_SETTINGS'] = 'form Customization-editing';

$MESS['IBLOCK_TYPE'] = 'Type of the information block';
$MESS['IBLOCK'] = 'information block';
$MESS['SEF_MODE_LIST'] = 'jobs List';
$MESS['SEF_MODE_DETAIL'] = 'Details';
$MESS['SEF_MODE_COMMENT'] = 'Comment';
$MESS['ELEMENT_SORT_FIELD'] = 'First field to sort';
$MESS['ELEMENT_SORT_ORDER'] = 'the first sort';
$MESS['ELEMENT_SORT_FIELD2'] = 'the Second field to sort';
$MESS['ELEMENT_SORT_ORDER2'] = 'the Direction of the second sort';
$MESS['SORT_ASC'] = 'ascending';
$MESS['SORT_DESC'] = 'descending';
$MESS['LIST_FIELD_CODE'] = 'Fields';
$MESS['LIST_PROPERTY_CODE'] = 'Properties';
$MESS['DETAIL_FIELD_CODE'] = 'Fields';
$MESS['DETAIL_PROPERTY_CODE'] = 'Properties';
$MESS['OFFER_FIELD_CODE'] = 'Fields';
$MESS['OFFER_PROPERTY_CODE'] = 'Properties';
$MESS['OFFER_MESS_OFFER'] = 'the words "Offer"';
$MESS['OFFER_MESS_OFFER_DEFAULT'] = 'Suggestions';
$MESS['OFFER_MESS_BTN_REJECT'] = 'the text of the button "Decline"';
$MESS['OFFER_MESS_BTN_REJECT_DEFAULT'] = 'Reject';
$MESS['OFFER_MESS_BTN_CONFIRM'] = 'Text' Confirm 'button';
$MESS['OFFER_MESS_BTN_CONFIRM_DEFAULT'] = 'Confirm';
$MESS['OFFER_MESS_BTN_CANCEL'] = 'the button Text "Cancel"';
$MESS['OFFER_MESS_BTN_CANCEL_DEFAULT'] = 'Cancel';
$MESS['OFFER_MESS_BTN_DONE'] = 'the button Text "Done"';
$MESS['OFFER_MESS_BTN_DONE_DEFAULT'] = 'Executed';
$MESS['OFFER_MESS_BTN_COMMENT'] = 'the text of the button "Leave feedback"';
$MESS['OFFER_MESS_BTN_COMMENT_DEFAULT'] = 'Leave a comment';

$MESS['COMMENT_PROPERTY_CODE'] = 'Properties';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['COMMENT_MESS_BTN_ADD'] = 'the text of the button "Save"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Save';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Inscription blank properties';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not set)';

$MESS['FORM_PROPERTY_CODE'] = 'Properties';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['FORM_MESS_BTN_ADD'] = 'the text of the button "Save"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Save';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'Inscription blank properties';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not set)';

$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'topic ID';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Character code of the section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'the Path of the symbolic codes section';

$MESS['USE_SWITCH'] = 'Use switch';
?>