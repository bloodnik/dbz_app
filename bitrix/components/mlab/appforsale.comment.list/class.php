<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class CommentList extends \Mlab\Appforsale\Component\ElementList
{
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		
		if ($this->arParams['USER_ID'] > 0)
			$arFilter['PROPERTY_PROFILE_ID'] = $this->arParams['USER_ID'];
		
		return $arFilter;
	}
}
?>