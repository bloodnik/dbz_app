<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (isset($arResult['ITEM']))
{
	$item = $arResult['ITEM'];
	$areaId = $arResult['AREA_ID'];
	$itemIds = array(
		'ID' => $areaId
	);
	$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
?>
<?
	if (isset($arResult['TYPE']) && $arResult['TYPE'] === 'LINE')
	{
		include('line/template.php');
	}
	else
	{
		include('card/template.php');
	}
?>
<?
}
?>