<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arGroups = CUser::GetUserGroup($arResult['ITEM']['ID']);

$arResult['IS_MANAGER'] = in_array(5, $arGroups);
