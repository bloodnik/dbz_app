<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class UserItem extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		if (!empty($arParams['RESULT']))
		{
			$this->arResult = $arParams['RESULT'];
			unset($arParams['RESULT']);
		}

		if (!empty($arParams['PARAMS']))
		{
			$arParams += $arParams['PARAMS'];
			unset($arParams['PARAMS']);
		}
		
		return $arParams;
	}

	public function executeComponent()
	{
		$this->includeComponentTemplate();
	}
}