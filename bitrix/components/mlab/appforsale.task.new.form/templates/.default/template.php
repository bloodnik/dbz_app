<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.form',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'SECTION_ID' => $arParams['SECTION_ID'],
		'SECTION_CODE' => $arParams['SECTION_CODE'],
		'PROPERTY_CODE' =>$arParams['PROPERTY_CODE'],
		'PROPERTY_CODE_REQUIRED' =>$arParams['PROPERTY_CODE_REQUIRED'],
		'MESS_PROPERTY_VALUE_NA' => $arParams['MESS_PROPERTY_VALUE_NA'],
		'MESS_BTN_ADD' => $arParams['MESS_BTN_ADD'],
		'UPDATE_VALUES' => array(
			'IBLOCK_SECTION_ID' => $arParams['SECTION_ID']
		),
		'REDIRECT_URL' => '/youdo/tasks-customer/'
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
?>