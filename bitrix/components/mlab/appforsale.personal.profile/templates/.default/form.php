<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

?>
<?
CModule::IncludeModule('iblock');
$dbSection = CIBlockSection::GetList(
	array(),
	array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'ACTIVE' => 'Y',
		'GLOBAL_ACTIVE' => 'Y',
		'ID' => $arResult['VARIABLES']['SECTION_ID']
	),
	false,
	array(
		'ID',
		'UF_COUNT'
	)
);

if ($arSection = $dbSection->Fetch())
{

	$bAccess = true;
	if (intval($arSection['UF_COUNT']) > 0)
	{
		$dbElement = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'CREATED_BY' => $USER->GetID(),
				'SECTION_ID' => $arSection['ID'],
				'ACTIVE' => 'Y'
			),
			false,
			false,
			array(
				'ID'
			)
		);
		$cnt_element = 0;
		while($arElement = $dbElement->GetNextElement())
		{
			$cnt_element++;
		}
		
		if ($cnt_element >= intval($arSection['UF_COUNT']))
			$bAccess = false;
	}

	if ($bAccess)
	{
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.form',
			'',
			array(
				'IBLOCK_TYPE' => 'profile',
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'UPDATE_VALUES' => array(
					'IBLOCK_SECTION_ID' =>  $arResult['VARIABLES']['SECTION_ID']
				),
				'REDIRECT_URL' => '/youdo/subscription/'
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
	}
	else
	{
 		ShowMessage(GetMessage('EXCESS', array('#CNT_ELEMENT#' => $cnt_element, '#UF_COUNT#' => intval($arSection['UF_COUNT']))));
	}
}
?>