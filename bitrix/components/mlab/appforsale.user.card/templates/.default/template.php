<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.user.item',
	'',
	array(
		'RESULT' => array(
			'ITEM' => $arResult['USER'],
			'AREA_ID' => $this->GetEditAreaId($arResult['USER']['ID']),
			'TYPE' => 'CARD'
		)
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
?>