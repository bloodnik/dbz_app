<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<?foreach ($arResult['ITEMS'] as $arItem):
	$url = strip_tags($arItem['PREVIEW_TEXT']);?>
	<a class="afs-notification-item"<?=(!empty($url) ? ' href="'.$url.'"' : '')?>>
		<div class="afs-notification-date"><?=$arItem['DATE_CREATE']?></div>
		<div class="afs-notification-name"><?=$arItem['NAME']?></div>
	</a>	
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>
<script>
	app.onCustomEvent("OnNotificationCount");
</script>