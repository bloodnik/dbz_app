<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule('mlab.appforsale');
$context = Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$result = Mlab\Appforsale\MessageTable::add(array(
		'user_id' => $request->get('user_id'),
		'from_id' => $USER->GetID(),
		'out' => 1,
		'body' => $request->get('message')
));

$result = Mlab\Appforsale\MessageTable::add(array(
		'user_id' => $USER->GetID(),
		'from_id' => $request->get('user_id'),
		'out' => 0,
		'body' => $request->get('message')
));

$server = $context->getServer();

$request2 = new Bitrix\Main\Web\HttpClient();
$request2->post('http://127.0.0.1:8895/bitrix/pub/?CHANNEL_ID='.$request->get('user_id'), '"'.$APPLICATION->ConvertCharset($request->get('message'), LANG_CHARSET, 'UTF-8').'"');

CAppforsale::SendMessage(
		array($request->get('user_id')),
		array(
			'body' => CUser::GetFullName() .': '.$request->get('message'),
			'icon' => 'ic_stat_name',
			'click_action' => 'appforsale.exec'
		),
		array(
			'url' => 'http://'.$_SERVER['SERVER_NAME'].'/youdo/im/?sel='.$USER->GetID(),
			'archive' => 'N'
		)
);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>