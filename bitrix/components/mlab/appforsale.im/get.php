<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!Bitrix\Main\Loader::includeModule('mlab.appforsale'))
	return;

$items = array();

$filter = array(
	'=user_id' => $_POST['user_id'],
	'=from_id' => $USER->GetID()
);

$start_message_id = intval($_POST['start_message_id']);
if ($start_message_id > 0)
	$filter['<id'] = $start_message_id;

$dbMessage = Mlab\Appforsale\MessageTable::getList(array(
		'filter' => $filter,
		'order' => array(
			'id' => 'desc'
		),
		'limit' => 50
));


while($arMessage = $dbMessage->fetch())
{
	
	$items[] = array(
		'id' => $arMessage['id'],
		'date' => $arMessage['date']->getTimestamp(),
			//format("d.m.Y H:i:s"),
		'out' => $arMessage['out'],
		'user_id' => $arMessage['user_id'],
		'read_state' => $arMessage['read_state'],
		'body' => $arMessage['body']
	);
}
echo \Bitrix\Main\Web\Json::encode(array(
		'response' => array(
			'count' => $dbMessage->getSelectedRowsCount(),
			'items' => $items
		)
));
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>