<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskCustomerList extends \Mlab\Appforsale\Component\ElementList
{
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		unset($arFilter['ACTIVE']);
		$arFilter['CREATED_BY'] = $USER->GetID();
		if ($_REQUEST['archive'] == 'Y')
			$arFilter['PROPERTY_STATUS_ID'] = 'F';
		else 
			$arFilter['!PROPERTY_STATUS_ID'] = 'F';
		return $arFilter;
	}
}
?>