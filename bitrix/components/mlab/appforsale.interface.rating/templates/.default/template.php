<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$value = intval($arParams['value']['VALUE']);
?>
<input type="hidden" name="<?=$arParams['strHTMLControlName']['VALUE']?>" id="property_<?=$arParams['arProperty']['ID']?>_value" value="<?=$value?>" />
<div class="afs-property-rating-wrap" id="property_<?=$arParams['arProperty']['ID']?>">
<?for ($i=1; $i<= 5; $i++):
	if ($value >= $i):?>
		<img class="afs-property-rating-item" data-rating="<?=$i?>" src="<?=$templateFolder?>/images/star_on.png" />
	<?else:?>
		<img class="afs-property-rating-item" data-rating="<?=$i?>" src="<?=$templateFolder?>/images/star_off.png" />
<?
	endif;
endfor;?>
</div>
<script>
var items = BX('property_<?=$arParams['arProperty']['ID']?>');
BX.bind(items, 'click', BX.delegate(function(e) {
	var target = event.target;
	while (target != items)
	{
		if (target && target.nodeType && target.nodeType == 1 && BX.hasClass(target, "afs-property-rating-item"))
		{
			var rating = target.getAttribute("data-rating");
			BX('property_<?=$arParams['arProperty']['ID']?>_value').value = rating;
			for (var i=0, item; item=items.children[i]; i++)
			{
				if (i < rating)
					item.src = '<?=$templateFolder?>/images/star_on.png';
				else
					item.src = '<?=$templateFolder?>/images/star_off.png';
			}   
			return;
		}
		target = target.parentNode;
	}
	
}, this));




</script>