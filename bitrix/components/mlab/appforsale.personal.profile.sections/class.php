<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class PersonalProfileSectionList extends \Mlab\Appforsale\Component\SectionList
{	
	
	public function executeComponent()
	{
		global $USER;
		
		$dbRecurring = CAppforsaleRecurring::GetList(
				array(),
				array(
						'PRODUCT_ID' => '',
						'USER_ID' => $USER->GetID()
				)
		);
		
		if ($arRecurring = $dbRecurring->Fetch())
		{
			$this->arParams['SUBSCRIPTION_SERVICE'] = $arRecurring['NEXT_DATE'];
		}
		
		parent::executeComponent();
	}
	
	protected function getSelect()
	{
		$arSelect = parent::getSelect();
		
		$arSelect[] = 'UF_RECUR_TYPE_TEST';
		$arSelect[] = 'UF_RECUR_LENGTH_TEST';
		$arSelect[] = 'UF_RECUR_PRICE_TEST';
		
		$arSelect[] = 'UF_RECUR_TYPE';
		$arSelect[] = 'UF_RECUR_LENGTH';
		$arSelect[] = 'UF_RECUR_PRICE';
		
		return $arSelect;
	}	
}
?>