<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

<?if (!empty($arParams['SUBSCRIPTION_SERVICE'])):?>
	<p class="bg-warning" style="padding: 16px; margin: 16px;"><?=GetMessage('SUBSCRIPTION_SERVICE')?> <?=$arParams['SUBSCRIPTION_SERVICE']?></p>
<?endif;?>


<?if (!empty($arResult['ITEMS'])):?>
	<div class="afs-section-rows">
		<?foreach ($arResult['ITEMS'] as $arItem):?>
			<div id="<?=$this->GetEditAreaId($arItem['ID'])?>">
				<?$fields = is_array($arItem['FIELDS']) ? implode(' / ', $arItem['FIELDS']) : $arItem['FIELDS'];?>
				<a class="afs-section-wrap" title="<?=$arItem['NAME']?>" href="<?=(($arItem['RIGHT_MARGIN'] - $arItem['LEFT_MARGIN']) == 1) ? $arItem['DETAIL_PAGE_URL'] : $arItem['SECTION_PAGE_URL']?>">
					<div class="afs-section-row <?=($arItem['PICTURE'] ? ' afs-section-row-picture' : '')?><?=(strlen($fields) > 0 ? ' afs-section-row-desk' : '')?>">
						<div class="afs-section-image">
							<img class="afs-section-img afs-color" src="<?=$arItem['PICTURE'] ? $arItem['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png'?>" />
						</div>
						<div class="afs-section-info">
							<div class="afs-section-name"><?=$arItem['NAME']?><br /><small><?=strip_tags(CAppforsaleRecurring::ToString($arItem))?></small></div>
							<div class="afs-section-field"><?=(is_array($arItem['FIELDS'])? implode(' / ', $arItem['FIELDS']) : $arItem['FIELDS'])?></div>
						</div>
						<img class="afs-section-indicator" src="<?=$templateFolder?>/images/section_indicator.png" />
					</div>
				</a>
			</div>
		<?endforeach;?>
	</div>
<?endif;?>