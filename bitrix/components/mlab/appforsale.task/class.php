<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class Task extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['OFFER_MESS_BTN_ADD'] = $arParams['OFFER_MESS_BTN_ADD'] ?: Loc::getMessage('OFFER_MESS_BTN_ADD');
		$arParams['DETAIL_MESS_CUSTOMER'] = $arParams['DETAIL_MESS_CUSTOMER'] ?: Loc::getMessage('DETAIL_MESS_CUSTOMER');
		$arParams['DETAIL_MESS_CLAIM'] = $arParams['DETAIL_MESS_CLAIM'] ?: Loc::getMessage('DETAIL_MESS_CLAIM');
		$arParams['DETAIL_MESS_YOUR_OFFER'] = $arParams['DETAIL_MESS_YOUR_OFFER'] ?: Loc::getMessage('DETAIL_MESS_YOUR_OFFER');
		$arParams['OFFER_FORM_MESS_BTN_ADD'] = $arParams['OFFER_FORM_MESS_BTN_ADD'] ?: Loc::getMessage('OFFER_FORM_MESS_BTN_ADD');
		
		$arParams['USE_MAP'] = $arParams['USE_MAP'] ?: 'Y';

		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		if ($USER->IsAuthorized())
		{
			$rsUser = CUser::GetByID($USER->GetID());
			if($arUser = $rsUser->GetNext(false))
			{
				if (intval($arUser['UF_CITY']) == 0)
				{
					global $APPLICATION;
					$APPLICATION->IncludeComponent(
							"appforsale:main.profile",
							"",
							array(
									"NO_BACK" => "Y"
							),
							false
					);
					die("");
				}
			}
		}
		
		$arDefaultUrlTemplates404 = array(
			'detail' => '#SECTION_CODE#/#ELEMENT_ID#/',
			'list' => '#SECTION_CODE#/',
			'sections' => ''
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'SECTION_ID',
			'SECTION_CODE',
			'ELEMENT_ID',
			'ELEMENT_CODE',
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
			$arVariables = array();
			$engine = new CComponentEngine($this);
			if (\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$engine->addGreedyPart("#SECTION_CODE_PATH#");
				$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
			}
			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'list';

		
			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);	
		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>
