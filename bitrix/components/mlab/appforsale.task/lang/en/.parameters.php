<?
$MESS['LIST_SETTINGS'] = 'list Settings';
$MESS['DETAIL_SETTINGS'] = 'Settings detailed view';
$MESS['OFFER_SETTINGS'] = 'set suggestions';
$MESS['OFFER_FORM_SETTINGS'] = 'Add an offer';
$MESS['FILTER_SETTINGS'] = 'filter Settings';
$MESS['SORT_SETTINGS'] = 'sort Settings';
$MESS['MAP_SETTINGS'] = 'map Settings';

$MESS['IBLOCK_TYPE'] = 'Type of the information block';
$MESS['IBLOCK'] = 'information block';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type of the information block proposals';
$MESS['OFFER_IBLOCK'] = 'information block proposals';
$MESS['SEF_MODE_LIST'] = 'jobs List';
$MESS['SEF_MODE_DETAIL'] = 'Details';
$MESS['SEF_MODE_OFFER'] = 'Offer';
$MESS['LIST_FIELD_CODE'] = 'Fields';
$MESS['LIST_PROPERTY_CODE'] = 'Properties';
$MESS['DETAIL_FIELD_CODE'] = 'Fields';
$MESS['DETAIL_PROPERTY_CODE'] = 'Properties';
$MESS['OFFER_FIELD_CODE'] = 'Fields';
$MESS['OFFER_PROPERTY_CODE'] = 'Properties of sentences';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type of the information block proposals';
$MESS['OFFER_IBLOCK'] = 'information block proposals';
$MESS['OFFER_MESS_BTN_ADD'] = 'the text of the button "Send quotation"';
$MESS['OFFER_MESS_BTN_ADD_DEFAULT'] = 'Send';
$MESS['OFFER_FORM_FIELD_CODE'] = 'Fields';
$MESS['OFFER_FORM_PROPERTY_CODE'] = 'Properties';
$MESS['OFFER_FORM_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['OFFER_FORM_MESS_BTN_ADD'] = 'the button Text "Add the sentence"';
$MESS['OFFER_FORM_MESS_BTN_ADD_DEFAULT'] = 'Add an offer';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA'] = 'Inscription blank properties';
$MESS['OFFER_FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not set)';

$MESS['ELEMENT_SORT_FIELD'] = 'First field to sort';
$MESS['ELEMENT_SORT_ORDER'] = 'the first sort';
$MESS['ELEMENT_SORT_FIELD2'] = 'the Second field to sort';
$MESS['ELEMENT_SORT_ORDER2'] = 'the Direction of the second sort';
$MESS['SORT_ASC'] = 'ascending';
$MESS['SORT_DESC'] = 'descending';

$MESS['DETAIL_MESS_CUSTOMER'] = 'the Text "Customer job"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'the Client of this job';
$MESS['DETAIL_MESS_CLAIM'] = 'the Text "Report this job"';
$MESS['DETAIL_MESS_CLAIM_DEFAULT'] = 'Report job';
$MESS['DETAIL_MESS_YOUR_OFFER'] = 'the Text "Your proposal"';
$MESS['DETAIL_MESS_YOUR_OFFER_DEFAULT'] = 'Your offer';


$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'topic ID';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Character code of the section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'the Path of the symbolic codes section';

$MESS['USE_FILTER'] = 'Use filter';
$MESS['FILTER_SORT_FIELD'] = 'First field to sort';
$MESS['FILTER_SORT_ORDER'] = 'the first sort';
$MESS['FILTER_SORT_FIELD2'] = 'the Second field to sort';
$MESS['FILTER_SORT_ORDER2'] = 'the Direction of the second sort';

$MESS['USE_SORT'] = 'sort';
$MESS['SORT_FIELD'] = 'Field to sort';

$MESS['USE_MAP'] = 'Use the map';

$MESS['OWNER'] = 'Display job user';
$MESS['FILTER_SETTINGS_CNT'] = 'Display quantity in skobkah';
?>