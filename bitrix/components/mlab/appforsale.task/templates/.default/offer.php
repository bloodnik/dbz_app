<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.task.offer.new.form',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['OFFER_IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['OFFER_IBLOCK_ID'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'PROPERTY_CODE' => $arParams['OFFER_FORM_PROPERTY_CODE'],
		'PROPERTY_CODE_REQUIRED' => $arParams['OFFER_FORM_PROPERTY_CODE_REQUIRED'],
		'MESS_PROPERTY_VALUE_NA' => $arParams['OFFER_FORM_MESS_PROPERTY_VALUE_NA'],
		'MESS_BTN_ADD' => $arParams['OFFER_FORM_MESS_BTN_ADD'],
		'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
	),
	$component
);
?>