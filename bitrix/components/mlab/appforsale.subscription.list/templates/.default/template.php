<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CJSCore::Init(array('date'));

Loc::loadMessages(__FILE__);
?>
<?if (!empty($arResult['ITEMS'])):?>
	<div>
		<?foreach ($arResult['ITEMS'] as $arItem):
			$areaId = $this->GetEditAreaId($arItem['ID']);
			$itemIds = array(
				'ID' => $areaId,
				'INFO' => $areaId.'_info',
				'RESTORE' => $areaId.'_restore',
				'CANCEL' => $areaId.'_cancel'
			);
			$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
		?>
			<div class="afs-subscription-item">
				<a href="<?=$arItem['ID']?>/" title="<?=$arItem['IBLOCK_SECTION']['NAME']?>">
					<div class="afs-subscription-name"><?=$arItem['IBLOCK_SECTION']['NAME']?></div>
					<div class="afs-subscription-desc"><?=$arItem['NAME']?></div>
					<div class="afs-subscription-wrap">
						<div class="afs-subscription-info row">
							<div class="col-sm-6 col-xs-6">Цена<?=$arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']] ? $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']] : '<br /><small>Бесплатно</small>'?></div>
							<div class="col-sm-6 col-xs-6" id="<?=$itemIds['INFO']?>"></div>
						</div>
					</div>
					<div id="<?=$itemIds['ID']?>" class="afs-subscription-button"></div>
				</a>
			</div>
			<script type="text/javascript"><?=$obName?> = new JCSubscription(<?=CUtil::PhpToJSObject(array(
				'id' => $arResult['RECURRING'][$arItem['ID']]['ID'],
				//'product_id' => $arItem['ID'],
				
				'activeTo' => MakeTimeStamp($arItem['ACTIVE_TO'], "DD.MM.YYYY HH:MI:SS"),
				'time' => time(),
				'ajaxUrl' => CUtil::JSEscape($templateFolder.'/ajax.php'),
				'visual' => $itemIds,
			    "mess" => [
			        "restore" => Loc::getMessage("RESTORE"),
			        "cancel" => Loc::getMessage("CANCEL"),
			        "active_to" => Loc::getMessage("ACTIVE_TO"),
			        "expired" => Loc::getMessage("EXPIRED"),
			        "del" => Loc::getMessage("DEL"),
			        "info" => Loc::getMessage("INFO"),
			    ]
			), false, true)?>)</script>
		<?endforeach;?>
		<?=$arResult['NAV_STRING']?>
	</div>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>