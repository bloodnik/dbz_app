<?
$MESS["PRICE"] = "Цена";
$MESS["FREE"] = "Бесплатно";

$MESS["RESTORE"] = "Восстановить подписку";
$MESS["CANCEL"] = "Отменить подписку";
$MESS["ACTIVE_TO"] = "Активна до";
$MESS["EXPIRED"] = "Истекла";
$MESS["DEL"] = "Удалить";
$MESS["INFO"] = "Срок действия<br /><small>не ограничен</small>";
?>