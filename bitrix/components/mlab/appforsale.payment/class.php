<?
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

class PayvideocallsPayment extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{		
		global $USER;
		if (!Loader::includeModule("mlab.appforsale"))
			return;
		
		$user_id = intval($USER->GetID());

		if ($user_id <= 0)
			return;
		
		$sum = intval($_GET['sum']);
		if ($sum <= 0)
			return;
		
		Mlab\Appforsale\YandexMoney::pay($sum);
	}
}
?>