<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.invite.out',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'INVITE_LEN' => $arParams['INVITE_LEN'],
		'MESS_LEAD' => $arParams['MESS_LEAD'],
		'MESS_DESCRIPTION' => $arParams['MESS_DESCRIPTION']
	),
	$component
);
?>		