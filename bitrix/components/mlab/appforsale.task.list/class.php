<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskList extends \Mlab\Appforsale\Component\ElementList
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams = parent::onPrepareComponentParams($arParams);
	
		$arParams['OWNER'] = ($arParams['OWNER'] == 'Y' ? 'Y' : 'N');
	
		return $arParams;
	}
	
	protected function makeOutputResult()
	{
		global $USER;
		
		parent::makeOutputResult();
		if ($USER->IsAuthorized() && $this->arParams['OWNER'] == 'Y')
		{
			foreach ($this->arResult['ITEMS'] as &$arItem)
			{
				if ($arItem['CREATED_BY'] == $USER->GetID())
				{
					$arItem['DETAIL_PAGE_URL'] = str_replace('/youdo/', '/youdo/tasks-customer/', $arItem['DETAIL_PAGE_URL']);
				}
			}
		}
	}
	
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		if ($USER->IsAuthorized())
		{
			if ($this->arParams['OWNER'] == 'N')
				$arFilter['!CREATED_BY'] = $USER->GetID();
			$arFilter['PROPERTY_STATUS_ID'] = array(false, 'N');
			
			$arProperty = CIBlockProperty::GetPropertyArray('CITY_ID', $this->arParams['IBLOCK_ID']);
			if ($arProperty)
			{
				$UF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', $USER->GetID());
				$city_id = intval($UF['UF_CITY']['VALUE']);
				if ($city_id > 0)
				{
					if (!empty($_SESSION['city_id']))
					{
						$city_id = $_SESSION['city_id'];
					}
					
					if (!empty($_GET['city_id']))
					{
						$city_id = $_GET['city_id'];
					}
					
					$this->arParams['CITY_ID'] = $city_id;
					$arFilter['PROPERTY_CITY_ID'] = $city_id;
				}
			}
		}
		else
		{
			$arFilter['!PROPERTY_STATUS_ID'] = 'F';
		}
		
		if (!empty($_GET['sections']))
		{
			$arFilter['SECTION_ID'] = explode(',', $_GET['sections']);
			$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		}
		
		return $arFilter;
	}
	
	protected function getSort()
	{
		if (!empty($_GET['sort']))
		{
			$sort = trim($_GET['sort']);
			$newSort = str_replace('High', '', $sort);
			if ($newSort != $sort)
			{
				return array(
					$newSort => 'ASC'
				);
			}
			
			$newSort = str_replace('Low', '', $sort);
			if ($newSort != $sort)
			{
				return array(
					$newSort => 'DESC'
				);
			}
		}
		
		return parent::getSort();
	}
	
	protected function getAdditionalCacheId()
	{
		global $USER;
		$UF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', $USER->GetID());
		$city_id = intval($UF['UF_CITY']['VALUE']);
		
		if (!empty($_SESSION['city_id']))
			$city_id = $_SESSION['city_id'];
		if (!empty($_GET['city_id']))
		{
			$city_id = $_GET['city_id'];
			$_SESSION['city_id'] = $city_id;
		}
		
		if (!empty($_SESSION['sort']))
			$sort = $_SESSION['sort'];
		if (!empty($_GET['sort']))
		{
			$sort = $_GET['sort'];
			$_SESSION['sort'] = $sort;
		}
				
		$res = parent::getAdditionalCacheId();
		$res[] = $_GET['sections'];
		$res[] = $city_id;
		$res[] = $sort;
		
		return $res;
	}
}
?>