<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arSort = CIBlockParameters::GetElementSortFields(
	array('TIMESTAMP_X', 'SORT', 'ID'),
	array('KEY_LOWERCASE' => 'Y')
);

$arAscDesc = array(
	'asc' => Loc::getMessage('SORT_ASC'),
	'desc' => Loc::getMessage('SORT_DESC')
);

$arComponentParameters = array(
	'GROUPS' => array(
		'LIST_SETTINGS' => array(
			'NAME' => Loc::getMessage('LIST_SETTINGS')
		),
		'DETAIL_SETTINGS' => array(
			'NAME' => Loc::getMessage('DETAIL_SETTINGS')
		),
		'OFFER_SETTINGS' => array(
			'NAME' => Loc::getMessage('OFFER_SETTINGS')
		),
		'COMMENT_SETTINGS' => array(
			'NAME' => Loc::getMessage('COMMENT_SETTINGS')
		)
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'OFFER_IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('OFFER_IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'OFFER_IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('OFFER_IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['OFFER_IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'CACHE_TIME' => array('DEFAULT' => 36000000),
		'SEF_MODE' => array(
			'list' => array(
				'NAME' => Loc::getMessage('SEF_MODE_LIST'),
				'DEFAULT' => '',
				'VARIABLES' => array()
			),
			'detail' => array(
				'NAME' => Loc::getMessage('SEF_MODE_DETAIL'),
				'DEFAULT' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH',
					'ELEMENT_ID',
					'ELEMENT_CODE',
				)
			),
			'comment' => array(
				'NAME' => Loc::getMessage('SEF_MODE_COMMENT'),
				'DEFAULT' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/comment/',
				'VARIABLES' => array(
					'SECTION_ID',
					'SECTION_CODE',
					'SECTION_CODE_PATH',
					'ELEMENT_ID',
					'ELEMENT_CODE',
				)
			)
		),
		'USE_SWITCH' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('USE_SWITCH'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
		),
			
		'ELEMENT_SORT_FIELD' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'timestamp_x',
		),
		'ELEMENT_SORT_ORDER' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'ELEMENT_SORT_FIELD2' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'id',
		),
		'ELEMENT_SORT_ORDER2' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('ELEMENT_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
			
			
			
			
			
			
			
			
		'LIST_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('LIST_FIELD_CODE'), 'LIST_SETTINGS'),
		'LIST_PROPERTY_CODE' => array(
			'PARENT' => 'LIST_SETTINGS',
			'NAME' => Loc::getMessage('LIST_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'DETAIL_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('DETAIL_FIELD_CODE'), 'DETAIL_SETTINGS'),
		'DETAIL_PROPERTY_CODE' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'OFFER_FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('OFFER_FIELD_CODE'), 'DETAIL_SETTINGS'),
		'OFFER_PROPERTY_CODE' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('OFFER_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['OFFER_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),			
		'DETAIL_MESS_CUSTOMER' => array(
			'PARENT' => 'DETAIL_SETTINGS',
			'NAME' => Loc::getMessage('DETAIL_MESS_CUSTOMER'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DETAIL_MESS_CUSTOMER_DEFAULT')
		),
		'COMMENT_IBLOCK_TYPE' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'COMMENT_IBLOCK_ID' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['COMMENT_IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'COMMENT_PROPERTY_CODE' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_PROPERTY_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['COMMENT_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'COMMENT_PROPERTY_CODE_REQUIRED' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_PROPERTY_CODE_REQUIRED'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'SIZE' => 8,
			'VALUES' => Parameters::getIblockElementProperties($arCurrentValues['COMMENT_IBLOCK_ID']),
			'ADDITIONAL_VALUES' => 'Y'
		),
		'COMMENT_MESS_PROPERTY_VALUE_NA' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_MESS_PROPERTY_VALUE_NA'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT')
		),
		'COMMENT_MESS_BTN_ADD' => array(
			'PARENT' => 'COMMENT_SETTINGS',
			'NAME' => Loc::getMessage('COMMENT_MESS_BTN_ADD'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('COMMENT_MESS_BTN_ADD_DEFAULT')
		)		
	)
);

if ($arCurrentValues['SEF_MODE'] == 'Y')
{
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES'] = array();
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_ID'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_ID'),
		'TEMPLATE' => '#SECTION_ID#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE'),
		'TEMPLATE' => '#SECTION_CODE#'
	);
	$arComponentParameters['PARAMETERS']['VARIABLE_ALIASES']['SECTION_CODE_PATH'] = array(
		'NAME' => Loc::getMessage('VARIABLE_ALIASES_SECTION_CODE_PATH'),
		'TEMPLATE' => '#SECTION_CODE_PATH#'
	);
}
?>