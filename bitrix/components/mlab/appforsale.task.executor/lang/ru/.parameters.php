<?
$MESS['LIST_SETTINGS'] = 'Настройки списка';
$MESS['DETAIL_SETTINGS'] = 'Настройки детального просмотра';
$MESS['OFFER_SETTINGS'] = 'Настройки предложений';
$MESS['COMMENT_SETTINGS'] = 'Настройки комментариев';

$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['OFFER_IBLOCK_TYPE'] = 'Тип инфоблока предложений';
$MESS['OFFER_IBLOCK'] = 'Инфоблок предложений';
$MESS['SEF_MODE_LIST'] = 'Список заданий';
$MESS['SEF_MODE_DETAIL'] = 'Детально';
$MESS['SEF_MODE_COMMENT'] = 'Комментарий';
$MESS['LIST_FIELD_CODE'] = 'Поля';
$MESS['LIST_PROPERTY_CODE'] = 'Свойства';
$MESS['DETAIL_FIELD_CODE'] = 'Поля';
$MESS['DETAIL_PROPERTY_CODE'] = 'Свойства';
$MESS['OFFER_FIELD_CODE'] = 'Поля предложения';
$MESS['OFFER_PROPERTY_CODE'] = 'Свойства предложения';
$MESS['OFFER_IBLOCK_TYPE'] = 'Тип инфоблока предложений';
$MESS['OFFER_IBLOCK'] = 'Инфоблок предложений';

$MESS['ELEMENT_SORT_FIELD'] = 'Первое поле для сортировки';
$MESS['ELEMENT_SORT_ORDER'] = 'Направление первой сортировки';
$MESS['ELEMENT_SORT_FIELD2'] = 'Второе поле для сортировки';
$MESS['ELEMENT_SORT_ORDER2'] = 'Направление второй сортировки';
$MESS['SORT_ASC'] = 'по возрастанию';
$MESS['SORT_DESC'] = 'по убыванию';

$MESS['DETAIL_MESS_CUSTOMER'] = 'Текст "Заказчик этого задания"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'Заказчик этого задания';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Идентификатор раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Символьный код раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Путь из символьных кодов раздела';

$MESS['COMMENT_PROPERTY_CODE'] = 'Свойства';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'Свойства обязательные для заполнения';
$MESS['COMMENT_MESS_BTN_ADD'] = 'Текст кнопки "Сохранить"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Сохранить';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Надпись незаполненного свойства';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(не установлено)';

$MESS['USE_SWITCH'] = 'Использовать переключатель';
?>