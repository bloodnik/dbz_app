<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$comment_url = CComponentEngine::MakePathFromTemplate($arResult['FOLDER'] . $arResult['URL_TEMPLATES']['comment'], $arResult['VARIABLES']);
?>
<div>

	<?
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.executor.detail',
		'',
		array(
			'IBLOCK_TYPE'   => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID'     => $arParams['IBLOCK_ID'],
			'ELEMENT_ID'    => $arResult['VARIABLES']['ELEMENT_ID'],
			'ELEMENT_CODE'  => $arResult['VARIABLES']['ELEMENT_CODE'],
			'SECTION_ID'    => $arResult['VARIABLES']['SECTION_ID'],
			'SECTION_CODE'  => $arResult['VARIABLES']['SECTION_CODE'],
			'FIELD_CODE'    => $arParams['DETAIL_FIELD_CODE'],
			'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
			'CACHE_TYPE'    => $arParams['CACHE_TYPE'],
			'CACHE_TIME'    => $arParams['CACHE_TIME']
		),
		$component
	);
	?>
	<div class="col-md-12">

		<div class="clearfix"></div>
		<?

		if ( ! is_array($arParams['OFFER_FIELD_CODE'])) {
			$arParams['OFFER_FIELD_CODE'] = array();
		}
		foreach ($arParams['OFFER_FIELD_CODE'] as $key => $val) {
			if ( ! $val) {
				unset($arParams['OFFER_FIELD_CODE'][ $key ]);
			}
		}

		if ( ! is_array($arParams['OFFER_PROPERTY_CODE'])) {
			$arParams['OFFER_PROPERTY_CODE'] = array();
		}
		foreach ($arParams['OFFER_PROPERTY_CODE'] as $key => $val) {
			if ($val === '') {
				unset($arParams['OFFER_PROPERTY_CODE'][ $key ]);
			}
		}


		$text = CIBlockProperty::GetPropertyArray('TEXT', $arParams['OFFER_IBLOCK_ID']);
		?>
		<div class="afs-fixed-bottom">
			<div class="afs-buttons-group">
				<?
				$APPLICATION->IncludeComponent(
					'mlab:appforsale.offer',
					'dbz2',
					array(
						'IBLOCK_TYPE' => $arParams['OFFER_IBLOCK_TYPE'],
						'IBLOCK_ID'   => $arParams['OFFER_IBLOCK_ID'],
						'TASK_ID'     => $arResult['VARIABLES']['ELEMENT_ID'],
						'CACHE_TYPE'  => "N",
						'CACHE_TIME'  => $arParams['CACHE_TIME'],
					),
					$component
				);
				?>
			</div>
		</div>

	</div>
</div>