<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if ($arParams['USE_SWITCH'] != 'N'):?>
<div style="text-align: center; padding: 16px;">
	<span class="btn-group">
	  <span class="btn btn-primary btn-sm"><?=GetMessage('IS_EXECUTOR')?></span>
	  <a class="btn btn-default btn-sm" href="/youdo/tasks-customer/" onclick="return true;"><?=GetMessage('IS_CUSTOMER')?></a>
	</span>
</div>
<?endif;?>
<div style="padding: 16px;">
	<? 
	if ($_GET['archive'] == 'Y')
	{
		echo '<a href="/youdo/tasks-executor/" onclick="return true;">'.GetMessage('IS_OPEN').'</a> | <b>'.GetMessage('IS_ARCHIVE').'</b>';
	}
	else
	{
		echo '<b>'.GetMessage('IS_OPEN').'</b> | <a href="/youdo/tasks-executor/?archive=Y" onclick="return true;">'.GetMessage('IS_ARCHIVE').'</a>';
	}
	?>
</div>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.task.executor.list',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
		'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
		'FIELD_CODE' => $arParams['LIST_FIELD_CODE'],
		'PROPERTY_CODE' => $arParams['LIST_PROPERTY_CODE'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail'],
		'ARCHIVE' => ($_REQUEST['archive'] == 'Y') ?: 'N'
	),
	$component
);
?>