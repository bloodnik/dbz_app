<?
$MESS["DOWNLOAD_NAME"] = "Title";
$MESS["DOWNLOAD_NAME_DEFAULT"] = "Personal assistant in your pocket";
$MESS["DOWNLOAD_DESC"] = "Description";
$MESS["DOWNLOAD_DESC_DEFAULT"] = "Download our app and use it wherever you are.";
$MESS["LINK_APPLE"] = "Link to app in AppStore";
$MESS["LINK_APPLE_DEFAULT"] = "https://itunes.apple.com/ru/app/id1009394447";
$MESS["LINK_GOOGLE"] = "Link to app in Google Play";
$MESS["LINK_GOOGLE_DEFAULT"] = "https://play.google.com/store/apps/details?id=net.malahovsky.appforsale";
?>