<?
$MESS['DOWNLOAD_NAME'] = 'Заголовок';
$MESS['DOWNLOAD_NAME_DEFAULT'] = 'Персональный помощник в вашем кармане';
$MESS['DOWNLOAD_DESC'] = 'Описание';
$MESS['DOWNLOAD_DESC_DEFAULT'] = 'Скачайте наше приложение и пользуйтесь, где бы вы ни находились.';
$MESS['LINK_APPLE'] = 'Ссылка на приложение в AppStore';
$MESS['LINK_APPLE_DEFAULT'] = 'https://itunes.apple.com/ru/app/id1009394447';
$MESS['LINK_GOOGLE'] = 'Ссылка на приложение в Google Play';
$MESS['LINK_GOOGLE_DEFAULT'] = 'https://play.google.com/store/apps/details?id=net.malahovsky.appforsale';
?>