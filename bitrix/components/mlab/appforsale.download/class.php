<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class Download extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['MESS_DOWNLOAD_NAME'] = $arParams['MESS_DOWNLOAD_NAME'] ?: Loc::getMessage('DOWNLOAD_NAME');
		$arParams['MESS_DOWNLOAD_DESC'] = $arParams['MESS_DOWNLOAD_DESC'] ?: Loc::getMessage('DOWNLOAD_DESC');
		
		$arParams['LINK_APPLE'] = $arParams['LINK_APPLE'] ?: 'https://itunes.apple.com/ru/app/id1009394447';
		$arParams['LINK_GOOGLE'] = $arParams['LINK_GOOGLE'] ?: 'https://play.google.com/store/apps/details?id=net.malahovsky.appforsale';
		
		return $arParams;
	}
	
	public function executeComponent()
	{
		$this->includeComponentTemplate();
	}
}
?>