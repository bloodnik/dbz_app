/* mlab:appforsale.task.detail */
(function(window) {
	
	BX.addCustomEvent('OnIblock', function() {
		app.reload()
	});
	
	BX.ready(function() {
		
		var claim = BX('claim');
		BX.bind(claim, 'click', function() {
			
			if (claim.hasAttribute('disabled'))
				return;
			
			claim.setAttribute('disabled', 'disabled');
			
			BX.ajax({
				url: '/bitrix/components/mlab/appforsale.task.detail/claim.php',
				method: 'POST',
				data: {
					iblock_id: claim.getAttribute('data-iblock_id'),
					id: claim.getAttribute('data-id')
				},
				onsuccess: function() {
					alert(BX.message('CLAIM_SUCCESS'));
				},
				onfailure: BX.delegate(function() {
					claim.removeAttribute('disabled');
				}, this)
			});
		});
	
	});
	
})(window);