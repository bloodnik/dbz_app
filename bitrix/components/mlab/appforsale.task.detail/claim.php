<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$IBLOCK_ID = $request->get('iblock_id');
$ID = $request->get('id');
if (empty($IBLOCK_ID) || empty($ID))
	return;

if(!is_array($_SESSION['CLAIM']))
	$_SESSION['CLAIM'] = array();
if(in_array($ID, $_SESSION['CLAIM']))
	return;

$_SESSION['CLAIM'][] = $ID;

CEvent::Send('CLAIM', SITE_ID, array(
		'IBLOCK_ID' => $IBLOCK_ID,
		'ID' => $ID
), 'Y');
?>