<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<div class="executor-list-wrap">
<?foreach ($arResult['ITEMS'] as $arItem):?>
		<a class="executor-list-item" href="/youdo/executors/u<?=$arItem['USER']['ID']?>/" title="<?=$arItem['USER']['FORMAT_NAME']?>">
			<img src="<?=($arItem['USER']['PERSONAL_PHOTO'] ? $arItem['USER']['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png')?>" class="executor-list-item__ptoho" />
			<div class="executor-list-item__name"><?=$arItem['USER']['FORMAT_NAME']?></div>	
		</a>
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
</div>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>