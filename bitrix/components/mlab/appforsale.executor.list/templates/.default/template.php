<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<div class="afs-executor-items">
<?foreach ($arResult['ITEMS'] as $arItem):?>
		<a class="afs-executor-item" href="/youdo/executors/u<?=$arItem['USER']['ID']?>/" title="<?=$arItem['USER']['FORMAT_NAME']?>">
			<?
			$APPLICATION->IncludeComponent(
				'mlab:appforsale.user.item',
				'',
				array(
					'RESULT' => array(
						'ITEM' => $arItem['USER'],
						'AREA_ID' => $this->GetEditAreaId($arItem['CREATED_BY']),
						'TYPE' => 'LINE'
					)
				),
				$component,
				array('HIDE_ICONS' => 'Y')
			);
			?>
		</a>
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
</div>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>