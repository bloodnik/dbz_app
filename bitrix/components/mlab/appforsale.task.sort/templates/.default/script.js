/* mlab:appforsale.task.sort */
(function(window) {

	if (!!window.JCTaskSort)
	{
		return;
	}
	
	window.JCTaskSort = function(arParams)
	{
		this.pagePath = '';
		this.visual = {
			SELECT: ''
		};

		this.obSelect = null;
		
		if ('object' === typeof arParams)
		{
			this.pagePath = arParams.pagePath;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCTaskSort.prototype.Init = function()
	{
		this.obSelect = BX(this.visual.SELECT);
		
		if (!!this.obSelect)
		{
			BX.bind(this.obSelect, 'change', BX.delegate(this.OnChange, this));
		}
	}
		
	window.JCTaskSort.prototype.OnChange = function(e)
	{
		var field = '';
		if (!!this.obSelect)
		{
			for (var i=0; i < this.obSelect.options.length; i++)
			{
				if (this.obSelect.options[i].selected)
					field = this.obSelect.options[i].value;
			}
		}

		
	//	alert(field);
//		
////		BX.ajax.Setup(
////			{
////				denyShowWait: true
////			},
////			true
////		);  
//		
		path = this.pagePath + "?sort=" + field;
//		if (city_id > 0)
//			path = path + "&city_id=" + city_id;
//		
		BX.ajax.insertToNode(
			path,
			BX('list_for_filter')
		);
	}
	
})(window);