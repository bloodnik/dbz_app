(function (window) {
	
	if (!!window.JCPersonalProfilePay)
	{
		return;
	}
	
	window.JCPersonalProfilePay = function (arParams)
	{
		this.recur_id = 0;
		this.visual = {
			ID: '',
			BUTTON: ''
		};
		
		this.obButton = null;
		
		if ('object' === typeof arParams)
		{
			this.recur_id = arParams.recur_id;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCPersonalProfilePay.prototype.Init = function()
	{
		this.obButton = BX(this.visual.BUTTON);
		
		if (!!this.obButton)
		{
			BX.bind(this.obButton, 'click', BX.delegate(this.OnCLick, this));
		}
	}
	
	window.JCPersonalProfilePay.prototype.OnCLick = function(e)
	{	
		this.obButton.setAttribute('disabled', 'disabled');
		BX.ajax({
			url: '/bitrix/components/mlab/appforsale.personal.profile.pay/pay.php',
			method: 'POST',
			data: {
				id: this.recur_id
			},
			dataType: 'json',
			onsuccess: BX.delegate(function(data) {
				if (data.response)
				{
					location.reload();
				}
				else if (data.error)
				{
					this.obButton.removeAttribute('disabled');
					alert(data.error.error_msg);
				}
			}, this)
		});
	}
	
})(window);