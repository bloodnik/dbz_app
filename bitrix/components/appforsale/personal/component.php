<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized() && $_GET['ITEM'] != 'agreement')
{
	CAppforsale::AuthForm();
}

$arVariables = array(
	"ITEM" => $_GET['ITEM']
);

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate(
	(isset($arVariables["ITEM"])) ? $arVariables["ITEM"] : 'main'
);
?>