<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
if ($USER->IsAuthorized())
{
	$data = explode(',', $_POST['file']);
	$data = str_replace(' ', '+', $data[1]);
	$img = @imagecreatefromstring(base64_decode($data));
	$tmp_name = $_SERVER['DOCUMENT_ROOT']."/upload/tmp/".md5(date("Y-m-d H:i:s")).".jpg";
	ImageJPEG($img, $tmp_name);
	ImageDestroy($img);

	$USER->Update(
		$USER->GetID(),
		array(
			"PERSONAL_PHOTO" => array(
				"name" => GetFileName($tmp_name),
				"tmp_name" => $tmp_name,
				"type" => "image/jpeg",
				"MODULE_ID" => "mlab.appforsale",
				"del" => "Y"
			)
		)
	);
	
	$arResult = array("response" => 1);
}
else
{
	$arResult = array("error" => array("error_msg" => "Error"));
}

echo \Bitrix\Main\Web\Json::encode($arResult);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");
?>