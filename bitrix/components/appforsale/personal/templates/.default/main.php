<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:setting.main",
			"",
			array(
				'USE_SWITCH' => $arParams['USE_SWITCH'],
				'USE_SUBSCRIPTION' => $arParams['USE_SUBSCRIPTION'],
				'SWITCH_GROUP' => $arParams['SWITCH_GROUP']
			),
			$component
	);
?>