<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arItemIDs = array(
		"HEADER" => "header"
);

echo '<div class="header" id="'.$arItemIDs['HEADER'].'">';

echo '<div class="personal_photo">';
echo '<div class="progress" id="progress"></div>';
echo '<input class="personal_photo_file" type="file" onchange="uploadfile(this)" />';
echo '<canvas class="canvas" id="canvas"></canvas>';
echo '<img src="'.($arResult["arUser"]['PERSONAL_PHOTO'] ? $arResult["arUser"]['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/user_no_photo.png').'" width="96" height="96" />';

echo '</div>
		<div class="name">'.$arResult['arUser']['FORMAT_NAME'].'</div>
		<div class="profile" onclick="app.loadPageBlank({url: \'/youdo/personal/profile/edit.php\', title: \''.GetMessage('PROFILE').'\'})">'.GetMessage('EDIT').'</div>
	</div>';

echo '<ul>
		<li class="exit" onclick="return Logout(this)">'.GetMessage('EXIT').'</li>
	</ul>';

$arJSParams = array(
	"VISUAL" => array(
		"HEADER" => $arItemIDs['HEADER']
	)
);

echo '<script type="text/javascript">new JCPersonal('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>