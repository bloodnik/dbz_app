(function (window) {
	
	if (!!window.JCEditMap)
	{
		return;
	}

	window.JCEditMap = function (arParams)
	{		
		this.value = null;
		this.map = null;
		this.mapDiv = "map";
		this.marker = null;
		this.infowindow = null;
		this.code = arParams.code;
	}
			
	window.JCEditMap.prototype.InitGoogle = function()
	{
		this.map = new google.maps.Map(BX(this.mapDiv), {
			 center: {lat: 55.75399399999374, lng: 37.62209300000001},
			 zoom: 14,
			 disableDefaultUI: true
		});
		
		app.getCurrentPosition(BX.delegate(function(position) {
			this.map.setCenter({
				lat: position.coords.latitude,
				lng: position.coords.longitude
			});
		}, this));
		
		this.infowindow = new google.maps.InfoWindow();
		
		this.map.addListener('click', BX.delegate(function(e) {
			var geocoder = new google.maps.Geocoder;
			geocoder.geocode({'location': e.latLng}, BX.delegate(function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if (results[0])
					{
						if (this.marker == null)
						{
							this.marker = new google.maps.Marker({
								position: e.latLng,
								map: this.map,
								//draggable:true,
							});	
											
//							this.marker.addListener("dragend", BX.delegate(function(e) {
//								//this.setLatLng(e.latLng);
//							}, this));
						}
						else
						{
							this.marker.setPosition(e.latLng);
						}
			    	  
						
						
						this.infowindow.setContent(results[0].formatted_address + "<br /><a onclick=\"app.onCustomEvent('appforsale_map', {code: '" + this.code + "', value: '" + results[0].formatted_address + "'}); app.closeController()\" class=\"apply\">Установить</a>");
						this.infowindow.open(this.map, this.marker);
			      } else {
			        window.alert('No results found');
			      }
			    } else {
			      window.alert('Geocoder failed due to: ' + status);
			    }
			  }, this));
			
	
			
//			if (this.marker == null)
//			{
//				this.marker = new google.maps.Marker({
//					position: e.latLng,
//					map: this.map,
//					draggable:true,
//				});	
//								
//				this.marker.addListener("dragend", BX.delegate(function(e) {
//					this.setLatLng(e.latLng);
//				}, this));
//			}
//			else
//			{
//				this.marker.setPosition(e.latLng);
//			}
//			
		}, this));

		
		

//		var marker = new google.maps.Marker({
//      		position: {lat: 55.75399399999374, lng: 37.62209300000001},
//      		map: this.map,
//      		draggable:true,
//      	});
		

		
//		for (var key in this.profiles)
//		{
//			var arProfile = this.profiles[key];
//			var arUser = this.users[arProfile.CREATED_BY];
//			this.AddGoogleMarker(arProfile, arUser);
//        }

		app.hideProgress();
	}
	
//	window.JCMap.prototype.AddGoogleMarker = function(arProfile, arUser)
//	{
//		var infowindow = new google.maps.InfoWindow({
//      		content: '<div onclick="app.loadPageBlank({url: \'/youdo/profile/?USER_ID=' + arUser.ID + '\', title: \'' +  arUser.FORMAT_NAME + '\'})"><img class="bx_img" src="' + (arUser.PERSONAL_PHOTO ? arUser.PERSONAL_PHOTO.src : "/bitrix/components/appforsale/profile.map/templates/.default/images/no_photo.png") + '" /><div>' + arUser.FORMAT_NAME + '</div></div>'
//      	});
//
//		var marker = new google.maps.Marker({
//      		position: {lat: Number(arProfile.lat), lng: Number(arProfile.lng)},
//      		map: this.map,
//      		icon: this.icon
//      	});
//      	
//      	marker.addListener('click', function() {
//      		infowindow.open(this.map, marker);
//      	});
//	}

})(window);