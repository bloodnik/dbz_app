<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arVariables = array(
	"map" => $_GET['map']
);

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate(
	$arVariables['map'] == 'Y' ? 'map' : 'main'
);
?>