(function (window) {

	if (!!window.JCOffer)
	{
		return;
	}

	window.JCOffer = function (arParams)
	{
		this.id = 0;
		this.user_id = 0;
		this.visual = {
				ID: '',
				CANCEL: '',
				CONFIRM: '',
		};
		
//		
//		this.backUrl = false;
//		
		this.ob = null;
		
		this.obCancel = null;
		this.obConfirm = null;
//		this.obAuth = null;
		
		this.enabled = { props: { disabled: false }};
		this.disabled = { props: { disabled: true }};
		
		if ('object' === typeof arParams)
		{
			this.visual = arParams.VISUAL;
			this.id = arParams.ID;
			this.user_id = arParams.USER_ID;
//			if (arParams.backUrl)
//				this.backUrl = arParams.backUrl;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}

	window.JCOffer.prototype.Init = function()
	{
		this.ob = BX(this.visual.ID);
		this.obCancel = BX(this.visual.CANCEL);
		this.obConfirm = BX(this.visual.CONFIRM);
		
		if (!!this.obCancel)
		{
			BX.bind(this.obCancel, 'click', BX.delegate(this.Cancel, this));
		}
		
		if (!!this.obConfirm)
		{
			BX.bind(this.obConfirm, 'click', BX.delegate(this.Confirm, this));
		}
	}

	window.JCOffer.prototype.ShowProgress = function()
	{
		BX.adjust(this.obCancel, this.disabled);
		BX.adjust(this.obConfirm, this.disabled);
	}

	window.JCOffer.prototype.HideProgress = function()
	{
		BX.adjust(this.obCancel, this.enabled);
		BX.adjust(this.obConfirm, this.enabled);
	}

	window.JCOffer.prototype.Cancel = function()
	{
		this.ShowProgress();
		BX.ajax({
        url: window.location.href,
        data: {
			ajax: "Y",
			ID: this.id
		},
        method: 'POST',
        dataType: 'json',
        timeout: 15,
        async: true,
        processData: true,
        scriptsRunFirst: true,
        emulateOnload: false,
        start: true,
        cache: false,
        onsuccess: BX.delegate(function(data){
    		this.HideProgress();
    		if (data.response)
    			this.Fade(this.ob);

        }, this),
        onfailure: BX.delegate(function() {
    		this.HideProgress();
        	alert("����������� ��������");
        }, this)
    });
		
		
	
	}
	
	window.JCOffer.prototype.Confirm = function()
	{
		this.ShowProgress();
		BX.ajax({
	    url: window.location.href,
	    data: {
			ajax: "Y",
			contractor: this.user_id
		},
	    method: 'POST',
	    dataType: 'json',
	    timeout: 15,
	    async: true,
	    processData: true,
	    scriptsRunFirst: true,
	    emulateOnload: false,
	    start: true,
	    cache: false,
	    onsuccess: BX.delegate(function(data){
			this.HideProgress();
			if (data.response)
				this.Fade(BX('bx_offers'));
	
	    }, this),
	    onfailure: BX.delegate(function() {
			this.HideProgress();
	    	alert("����������� ��������");
	    }, this)
});
	
	
	}
	
	window.JCOffer.prototype.Fade = function(elem, t, f){
		  // ������ � ������� (�� ��������� 50)
		  var fps = f || 50; 
		  // ����� ������ �������� (�� ��������� 500��)
		  var time = t || 500; 
		  // ������� ����� ������� ������
		  var steps = time / fps;   

		  // ������� �������� opacity - ���������� 0
		  var op = 1;
		  var height = elem.offsetHeight;
		//  alert(height);
		  // ��������� ������������ �� 1 ����
		  var d0 = op / steps;
		  
		  var d1 = height / steps;
		  
			 // alert("height: " + height + ", steps: " + steps + ", d1: " + d1);
		  // ������������� �������� (1000 / fps) 
		  // ��������, 50fps -> 1000 / 50 = 20��  
		  var timer = setInterval(function(){
		    // ��������� ������� �������� opacity
		    op -= d0;
			  height -= d1;
		    // ������������� opacity �������� DOM
		    elem.style.opacity = op;
		    elem.style.height = height + 'px';
		    // ��������� ���������� ���������� ����� ��������
		    steps--;
		    
		    // ���� �������� ��������
		    if(steps == 0){
		      // ������� �������� ����������
		      clearInterval(timer);
		      // � ������� ������� �� ������ ���������
		      elem.style.display = 'none';
		      elem.remove();
		    }
		  }, (1000 / fps));
		}
	
	
//		this.ShowProgress();	
//		BX.ajax({
//	        url: "/bitrix/components/appforsale/system.auth.auth/templates/.default/login.php",
//	        data: {
//				LOGIN: this.obLogin.value,
//				PASSWORD: this.obPassword.value
//			},
//	        method: 'POST',
//	        dataType: 'json',
//	        timeout: 15,
//	        async: true,
//	        processData: true,
//	        scriptsRunFirst: true,
//	        emulateOnload: false,
//	        start: true,
//	        cache: false,
//	        onsuccess: BX.delegate(function(data){
//	        	if (data.response)
//				{
//	        		app.onCustomEvent("OnAfterUserLogin");
//	        		
//	        		if (this.backUrl)
//	        			app.loadPageStart({url: this.backUrl, title: ""});
//	        		else
//	        			app.reload();
//				}
//				else if (data.error)
//				{
//		        	this.HideProgress();
//					alert(data.error.message);
//				}
//	        }, this),
//	        onfailure: BX.delegate(function() {
//	        	this.HideProgress();
//	        	alert("����������� ��������");
//	        }, this)
//	    });
//		
//		
//		
//	}

	BX.ready(function() {
		app.hideProgress();
	});	
	
	BX.addCustomEvent("OnIblock", BX.delegate(reload, this));
	
	function reload() {
		app.reload()
	}
	
})(window);