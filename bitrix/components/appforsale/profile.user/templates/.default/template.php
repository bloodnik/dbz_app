<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

echo '<div class="bx_user">
		<img src="'.($arResult['PERSONAL_PHOTO'] ? $arResult['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_user_personal_photo" />
				<div class="bx_user_info">
					<div class="bx_user_name">'.$arResult['FORMAT_NAME'].'</div>';

if (!empty($arResult['PERSONAL_MOBILE']))
	echo '<a class="btn btn-primary call" href="tel:'.$arResult['PERSONAL_MOBILE'].'">'.GetMessage('CALL').'</a>'.(is_dir($_SERVER['DOCUMENT_ROOT'].'/youdo/im/') ? ' <a class="btn btn-primary call" href="/youdo/im/?sel='.$arResult['ID'].'">'.GetMessage('SEND').'</a>' : '');
else
	echo '<span class="btn btn-primary call disabled" onclick="alert(\''.GetMessage('CALL_DISABLED').'\')">'.GetMessage('CALL').'</span>'.(is_dir($_SERVER['DOCUMENT_ROOT'].'/youdo/im/') ? ' <span class="btn btn-primary call disabled" onclick="alert(\''.GetMessage('SEND_DISABLED').'\')">'.GetMessage('SEND').'</span>' : '');	

echo '</div>
	</div>';

if (!empty($arResult['PROFILES']))
{
	echo '<ul>';
	$iblock_section_id = 0;
	foreach ($arResult['PROFILES'] as $key => $arProfile)
	{
		if ($iblock_section_id != $arProfile['IBLOCK_SECTION_ID'])
		{
			echo '<div class="bx_comment_header">'.$arResult['SECTIONS'][$arProfile['IBLOCK_SECTION_ID']]['NAME'].'</div>';
		}

		echo '<li class="bx_profile" onclick="app.loadPageBlank({url: \'/youdo/profile/?SECTION_ID='.$arProfile['IBLOCK_SECTION_ID'].'&ELEMENT_ID='.$arProfile['ID'].'\', title: \''.$arProfile['NAME'].'\'})">
				<div class="bx_profile_name">'.$arProfile['NAME'].'</div>		</li>';
		
		$iblock_section_id = $arProfile['IBLOCK_SECTION_ID'];
	}
	unset($arUser);
	echo '</ul>';
}
?>