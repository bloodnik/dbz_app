<?
use Bitrix\Main\UserTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["USER_ID"] = intval($arParams["USER_ID"]);

$nameFormat = CSite::GetNameFormat(true);

$userIterator = UserTable::getList(array(
	'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'PERSONAL_MOBILE'),
	'filter' => array('ID' => $arParams["USER_ID"]),
));
if ($arUser = $userIterator->fetch())
{
	$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
	$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

	$arResult = $arUser;
}

$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		),
		true,
		array(
				"ID",
				"NAME",
				"UF_FORMAT_NAME"
		)
);

$arFilter = array();
while ($arSection = $rsSection->GetNext())
{
	$arResult['SECTIONS'][$arSection['ID']] = $arSection;

	preg_match_all("/#([a-z0-9_]+)#/i", $arSection['UF_FORMAT_NAME'], $matches);
	foreach ($matches[1] as $value)
	{
		$arFilter[$value] = true;
	}
}

$arSelect = array(
		"ID",
		"IBLOCK_SECTION_ID",
		"CREATED_BY",
		"NAME"
);

$rsProfile = CIBlockElement::GetList(
		array(
				"SECTION_ID" => "ASC",
				"DATE_CREATE" => "ASC"
		),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ACTIVE" => "Y",
				"CREATED_BY" => $arParams["USER_ID"]
		),
		false,
		false,
		array_merge($arSelect, array_keys($arFilter))
);

$input = array();
foreach (array_keys($arFilter) as $field)
{
	$input[] = '#'.$field.'#';
}


while($arProfile = $rsProfile->GetNext())
{
	$output = array();
	foreach (array_keys($arFilter) as $field)
	{
		if (substr($field, 0, 9) == 'PROPERTY_')
			$output[] = $arProfile[$field.'_VALUE'];
		else
			$output[] = $arProfile[$field];
	}
	
	
	$arProfile['FORMAT_NAME'] = str_replace($input, $output, $arResult['SECTIONS'][$arProfile['IBLOCK_SECTION_ID']]['UF_FORMAT_NAME']);
	
	
	$arResult['PROFILES'][] = $arProfile;
}

$user_id = $USER->GetID();
if ($USER->GetID() > 0)
{
	$dbElement = CIBlockElement::GetList(
			array(),
			array(
					'IBLOCK_CODE' => 'task',
					'ACTIVE' => 'Y',
					array(
						'LOGIC' => 'OR',
						array(
							'CREATED_BY' => $arParams["USER_ID"],
							'PROPERTY_PROFILE_ID' => $user_id,
						),
						array(
							'CREATED_BY' => $user_id,
							'PROPERTY_PROFILE_ID' => $arParams["USER_ID"],
						)
					),
					
					
					'!PROPERTY_STATUS_ID' => 'F',
			),
			false,
			false,
			array(
					'ID'
			)
	);

	if ($arElement = $dbElement->GetNext())
	{

	}
	else
	{
		$arResult['PERSONAL_MOBILE'] = '';
	}
}
else
{
	$arResult['PERSONAL_MOBILE'] = '';
}

$this->IncludeComponentTemplate();
?>