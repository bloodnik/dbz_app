<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	echo '<div class="filter_wrap" style="overflow-y: auto" id="filter"><div class="filter">';
	$APPLICATION->IncludeComponent(
			"bitrix:catalog.smart.filter",
			"",
			array(
					"COMPONENT_TEMPLATE" => ".default",
					"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
					"TEMPLATE_THEME" => "red"
			),
			false
	);
	echo '</div></div>';

	$APPLICATION->IncludeComponent(
			"appforsale:profile.section",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID']
			),
			$component
	);
?>