<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized())
{
	CAppforsale::AuthForm();
}
else
{
	$rsUser = CUser::GetByID($USER->GetID());
	if($arUser = $rsUser->GetNext(false))
	{
		if (intval($arUser['UF_CITY']) == 0)
		{
			global $APPLICATION;
			$APPLICATION->IncludeComponent(
				"appforsale:main.profile", 
				"", 
				array(
					"NO_BACK" => "Y"
				),
				false
			);
			die("");
		}
	}
}

// TEMPLATE_THEME

$arVariables = array(
	"ACTION" => $_GET['ACTION'],
	"SECTION_ID" => $_GET['SECTION_ID']
);

if (isset($arVariables["ACTION"]) && $arVariables["ACTION"] == 'edit')
{
	$componentPage = 'edit';
}
else
{
	$componentPage = 'sections';	
}

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate($componentPage);
?>