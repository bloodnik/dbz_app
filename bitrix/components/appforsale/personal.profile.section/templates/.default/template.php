<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['PROFILES']))
{
	echo '<div class="popup_wrap" id="popup"><div class="popup_wrap2"><div class="popup">
			<div class="bx_popup_item" onclick="event.stopPropagation(); editProfile()">'.GetMessage('EDIT_PROFILE').'</div>
			<div class="bx_popup_item" onclick="event.stopPropagation(); cancelProfile()">'.GetMessage('CANCEL_PROFILE').'</div>
			<div class="bx_popup_item red" onclick="event.stopPropagation(); hidePopup()">'.GetMessage('CLOSE').'</div>
		</div></div></div>';
	
	echo '<ul>';
	foreach ($arResult['PROFILES'] as $key => $arProfile)
	{
		$arUser = $arResult['USERS'][$arProfile['CREATED_BY']];
		$stmp = MakeTimeStamp($arProfile['ACTIVE_TO'], "DD.MM.YYYY HH:MI:SS");
		
		echo '<li id="bx'.$arProfile['ID'].'" class="bx_profile">
				<img src="'.($arUser['PERSONAL_PHOTO'] ? $arUser['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/user_no_photo.png').'" class="bx_profile_personal_photo" />
				<div class="bx_profile_name">'.$arProfile['NAME'].'<div style="font-size: 12px">'.($arProfile['ACTIVE_TO'] ? (time() > $stmp ? '<span style="color: #f44336">'.GetMessage('NOT_ACTIVE').'</span>' : '<span style="color: #4caf50">'.GetMessage('ACTIVE_TO').' '.$arProfile['ACTIVE_TO'].'</span>') : '<span style="color: #9e9e9e">'.GetMessage('FREE').'</span>').'</div></div>';

		echo '<img class="bx_profile_more" src="'.$templateFolder.'/images/more.png" onclick="event.stopPropagation(); showPopup('.$arParams['SECTION_ID'].', '.$arProfile['ID'].', \''.GetMessage('EDIT').'\')" />		';
		echo '</li>';
	}
	unset($arUser);
	echo '</ul>';
}
else
{
	echo '<div class="bx_profile_empty">'.GetMessage('LIST_EMPTY').'</div>';
}

if (intval($arResult['SECTION']['UF_COUNT']) == 0 || (count($arResult['PROFILES']) < intval($arResult['SECTION']['UF_COUNT'])))
	echo '<div class="btn btn-primary bx_profile_add" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ACTION=edit\', title: \''.GetMessage('ADD_PROFILE').'\'})">'.GetMessage('ADD_PROFILE').(!empty($arResult['SECTION']['FORMAT_NAME']) ? $arResult['SECTION']['FORMAT_NAME'] : '').'</div>';
?>