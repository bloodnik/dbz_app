/* appforsale:profile.section */
(function (window) {
	
	BX.ready(function() {
		app.hideProgress();
	});	
	
	BX.addCustomEvent("OnIblock", BX.delegate(reload, this));
	
	function reload() {
		app.reload()
	}
	
	window.section_id = 0;
	window.element_id = 0;
	window.format_name = "";
	
	window.hidePopup = function ()
	{
		BX.adjust(BX('popup'), {style: {display: "none"}});
	}
	
	window.showPopup = function (a, b, c)
	{
		section_id = a;
		element_id = b;
		format_name = c;
		BX.adjust(BX('popup'), {style: {display: "block"}});
	}
	
	window.editProfile = function()
	{
		hidePopup();
		app.loadPageBlank({url: '?SECTION_ID=' + section_id + '&ELEMENT_ID=' + element_id + '&ACTION=edit', title: format_name});
	}
	
	window.cancelProfile = function()
	{
		hidePopup();
		fade(BX('bx' + element_id));
		BX.ajax({
	        url: window.location.href,
	        data: {
				ajax: "Y",
				ID: element_id
			},
	        method: 'POST',
	        dataType: 'json',
	        timeout: 15,
	        async: true,
	        processData: true,
	        scriptsRunFirst: true,
	        emulateOnload: false,
	        start: true,
	        cache: false
		});
	}
	
	window.fade = function(elem, t, f){
	
		  var fps = f || 50; 

		  var time = t || 500; 

		  var steps = time / fps;   

		  var op = 1;
		  var height = elem.offsetHeight;

		  var d0 = op / steps;
		  
		  var d1 = height / steps;

		  var timer = setInterval(function(){
		    op -= d0;
			  height -= d1;
		    elem.style.opacity = op;
		    elem.style.height = height + 'px';
		    steps--;
		    
		    if(steps == 0){
		      clearInterval(timer);
		      elem.style.display = 'none';
		      elem.remove();
		    }
		  }, (1000 / fps));
		}
	
})(window);