<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['ELEMENTS']))
{
	echo '<ul>';
	foreach ($arResult['ELEMENTS'] as $key => $arElement)
	{
		echo '<li class="bx_guide" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ELEMENT_ID='.$arElement['ID'].'\', title: \''.$arElement['NAME'].'\'})">
				<div class="bx_guide_name">'.$arElement['NAME'].'</div>
				<img class="bx_guide_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />
				</li>';
	}
	echo '</ul>';
}
else
{
	echo '<div class="bx_guide_empty">'.GetMessage('LIST_EMPTY').'</div>';
}
?>