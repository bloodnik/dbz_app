<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arParams['TYPE'] = trim($arParams['TYPE']);
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['ELEMENT_ID'] = intval($arParams['ELEMENT_ID']);
$arParams['MESS_BTN_SAVE'] = trim($arParams['MESS_BTN_SAVE']);
CModule::IncludeModule("iblock");

if ($_REQUEST['AJAX_CALL'] == 'Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();

	if (!empty($arParams['FIELD_CODE']))
	{
		$arFields = array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"NAME" => "unknown"
		);
		
		foreach ($arParams['DATA'] as $key => $value)
		{
			$arFields[$key] = $value;
		}

		foreach ($arParams['FIELD_CODE'] as $field)
		{
			if (substr($field, 0, 1) == '*')
			{
				$field = substr($field, 1);
			}
				
			if (substr($field, 0 ,9) == 'PROPERTY_')
			{
				$arFields["PROPERTY_VALUES"][substr($field, 9)] = $_POST[$field];
			}
			else
			{
				$arFields[$field] = $_POST[$field];
			}
		}

		$el = new CIBlockElement;
		if ($arParams['ELEMENT_ID'] > 0)
		{
			$el->Update($arParams['ELEMENT_ID'], $arFields);
			die(
					\Bitrix\Main\Web\Json::encode(
							array(
									"response" => $arParams['ELEMENT_ID']
							)
					)
						
			);
		}
		else
		{
			if ($product_id = $el->Add($arFields))
			{
				die(
						\Bitrix\Main\Web\Json::encode(
								array(
										"response" => (int) $product_id
								)
						)
			
				);
			}
			else
			{
				die(
						\Bitrix\Main\Web\Json::encode(
								array(
										"error" => array(
												"error_msg" => $el->LAST_ERROR
										)
								)
						)
				);
			}
		}
	}
	die("error");
}


$properties = CIBlockProperty::GetList(
		array("SORT"=>"ASC", "NAME"=>"ASC", "ID" => "ASC"),
		array("IBLOCK_ID"=> $arParams['IBLOCK_ID'], "ACTIVE"=>"Y", "CHECK_PERMISSIONS"=>"N")
);
while($prop_fields = $properties->Fetch())
{
	$arResult['properties'][$prop_fields['CODE']] = $prop_fields;
}


if ($arParams['ELEMENT_ID'] > 0)
{
	$arSelect = array(
			"ID",
			"NAME",
			"ACTIVE",
			"DATE_ACTIVE_FROM",
			"DATE_ACTIVE_TO",
			"SORT",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"DATE_CREATE",
			"CREATED_BY",
			"TIMESTAMP_X",
			"MODIFIED_BY",
			"TAGS",
			"IBLOCK_SECTION_ID",
			"DETAIL_PAGE_URL",
			"LIST_PAGE_URL",
			"DETAIL_PICTURE",
			"PREVIEW_PICTURE",
			"PROPERTY_*",
	);

	$rsElement = CIBlockElement::GetList(
			array(),
			array(
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ID" => $arParams["ELEMENT_ID"]
			),
			false,
			false,
			$arSelect
	);

	if($obElement = $rsElement->GetNextElement())
	{
		$arResult['FIELDS'] = $obElement->GetFields();
		$arResult["PROPERTIES"] = $obElement->GetProperties();
	}
}

if ($arParams['TYPE'] == 'offer')
{
	$rsSection = CIBlockSection::GetList(
			array(),
			array(
					"ID" => intval($_GET['SECTION_ID'])
			),
			false,
			array(
					"ID", "IBLOCK_ID"
			)
	);
	
	if($arSection = $rsSection->GetNext())
	{
		$rsSection = CIBlockSection::GetList(
				array(),
				array(
						"IBLOCK_ID" => $arSection['IBLOCK_ID'],
						"ID" => $arSection['ID']
				),
				false,
				array(
						"ID", "UF_TYPICAL_OFFER"
				)
		);
		
		if($arSection = $rsSection->GetNext())
		{
			$arResult['TYPICAL_OFFER'] = $arSection['UF_TYPICAL_OFFER'];
		}
	}
}


if (!empty($arParams['FIELD_CODE']))
{
	foreach ($arParams['FIELD_CODE'] as $field)
	{	
		$arField = array(
			"PROPERTY_TYPE" => "S"
		);
		if (substr($field, 0, 1) == '*')
		{
			$arField['REQUIRED'] = 'Y';
			$field = substr($field, 1);
		}
		else
		{
			$arField['REQUIRED'] = 'N';
		}
		
		if (substr($field, 0, 9) == 'PROPERTY_')
		{
			$code = substr($field, 9);
			if (array_key_exists($code, $arResult['properties']))
			{				
				$code = substr($field, 9);
				$arField['ID'] = $field;
				$arField['ROW_COUNT'] = $arResult['properties'][$code]['ROW_COUNT'];
				$arField['DEFAULT_VALUE'] = $arResult['properties'][$code]['DEFAULT_VALUE'];
				$arField['CODE'] = $code;
				$arField['MULTIPLE'] = $arResult['properties'][$code]['MULTIPLE'];
				$arField['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
				$arField['NAME'] = $arResult['properties'][$code]['NAME'];
				$arField['PROPERTY_TYPE'] = $arResult['properties'][$code]['PROPERTY_TYPE'];
				$arField['USER_TYPE'] = $arResult['properties'][$code]['USER_TYPE'];
				$arField['USER_TYPE_SETTINGS'] = $arResult['properties'][$code]['USER_TYPE_SETTINGS'];
				$arField['LINK_IBLOCK_ID'] = $arResult['properties'][$code]['LINK_IBLOCK_ID'];
				$arField['LIST_TYPE'] = $arResult['properties'][$code]['LIST_TYPE'];
				
				$arField['HINT'] = $arResult['properties'][$code]['HINT'];
				
				if ($arParams['ELEMENT_ID'] > 0 && !empty($arResult['PROPERTIES'][$code]['VALUE']))
				{
					$arField['VALUE'] = $arResult['PROPERTIES'][$code]['VALUE'];
				}
				$arResult['DISPLAY_FIELDS'][] = $arField;
			}
		}
		else
		{
			if ($field == 'ACTIVE_TO')
			{
				$arField['ID'] = $field;
				$arField['MULTIPLE'] = "N";
				$arField['NAME'] = GetMessage('ACTIVE_TO');
				$arField['USER_TYPE'] = 'DateTime';
				$arField['VALUE'] = $arResult['FIELDS']['DATE_ACTIVE_TO'];
			}
			else
			{
				$arField['ID'] = $field;
				$arField['MULTIPLE'] = "N";
				$arField['NAME'] = "noname";
			}
			$arResult['DISPLAY_FIELDS'][] = $arField;
		}
	}
	unset($arField);
}

$this->IncludeComponentTemplate();
?>