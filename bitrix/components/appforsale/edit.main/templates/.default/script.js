(function (window) {

	if (!!window.JCEdit)
	{
		return;
	}
	
	window.JCEdit = function (arParams)
	{
		this.fields = null;
		this.visual = {
			BUTTON: ''
		};
		
		this.obButton = null;
		
		this.enabled = { props: { disabled: false }};
		this.disabled = { props: { disabled: true }};
		
		if ('object' === typeof arParams)
		{
			this.fields = arParams.FIELDS;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCEdit.prototype.Init = function()
	{
		this.obButton = BX(this.visual.BUTTON);
		
		if (!!this.obButton)
		{
			BX.bind(this.obButton, 'click', BX.delegate(this.Save, this));
		}
		
		app.hideProgress();
	}
	
	window.JCEdit.prototype.ShowProgress = function()
	{
		for (var code in this.fields)
		{
			BX.adjust(BX(this.fields[code].ID), this.disabled);
		}
		BX.adjust(this.obButton, this.disabled);
	}
	
	window.JCEdit.prototype.HideProgress = function()
	{
		for (var code in this.fields)
		{
			BX.adjust(BX(this.fields[code].ID), this.enabled);
		}
		BX.adjust(this.obButton, this.enabled);
	}
	
	
	window.JCEdit.prototype.Save = function(e)
	{
		var data = {
			AJAX_CALL: 'Y'
		};
		
		var sError = "";
		for (var code in this.fields)
		{
			var id = this.fields[code].ID;
			var x = document.getElementsByName(id);			
			if (x.length == 0)
			{
				if (this.fields[code].REQUIRED == 'Y')
					sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';
			}
			else if (x.length == 1)
			{
				if (this.fields[code].LIST_TYPE == 'C')
				{
					if (x[0].checked)
					{
						data[id] = x[0].value;
					}
					else
					{
						if (this.fields[code].REQUIRED == 'Y')
							sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';	
					}
				}
				else if (this.fields[code].USER_TYPE == 'Date')
				{
					data[id] = x[0].value.substr(8,2) + "." + x[0].value.substr(5,2) + "." + x[0].value.substr(0,4);
				}
				else if (this.fields[code].USER_TYPE == 'HTML')
				{
					data[id] = "[TEXT]" + x[0].value;
				}
				else if (this.fields[code].USER_TYPE == 'DateTime')
				{
					data[id] = x[0].value.substr(8,2) + "." + x[0].value.substr(5,2) + "." + x[0].value.substr(0,4) + " " + x[0].value.substr(11,5);
				}
				else
				{
					if (x[0].value == "" && this.fields[code].REQUIRED == 'Y')
						sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';
					
					if (x[0].tagName == 'SELECT')
					{
						 var result = [];
						 var options = x[0] && x[0].options;
						 var opt;
						 for (var i=0, iLen=options.length; i<iLen; i++)
						 {
							 opt = options[i];
							 if (opt.selected)
							 {
								 result.push(opt.value);
							 }
						  }
						 
						if (result.length == 1)
						{
							data[id] = result[0];
						}
						else if (result.length > 1)
						{
							data[id] = result;
						}
					}
					else
					{
						data[id] = x[0].value;
					}
				}
			}
			else
			{
				if (this.fields[code].LIST_TYPE == 'C')
				{
					var checked = 0;
					data[id] = [];
					for (i = 0; i < x.length; i++)
					{
						if (x[i].checked && x[i].value != '')
						{
							checked++;
							data[id][checked] = x[i].value;
						}
					}
					
					if (checked == 0 && this.fields[code].REQUIRED == 'Y')
						sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';	
			
				}
				else
				{
					if (x[0].value == "" && this.fields[code].REQUIRED == 'Y')
						sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';
	
					data[id] = [];
					for (i = 0; i < x.length; i++)
						data[id][i] = x[i].value;
				}
			}
				
			
//			var value = BX(this.fields[code].ID).value;
//			if (value == "" && this.fields[code].REQUIRED == 'Y')
//				sError += 'Свойство "' + this.fields[code].NAME + '" обязательно для заполнения<br />';
//			
			//data[this.fields[code].ID] = value;
		}

		
		if (sError != "")
		{
			return alert(sError);
		}
		
		this.ShowProgress();
		
		BX.ajax({
	        url: location.href,
	        data: data,
	        method: 'POST',
	        dataType: 'json',
	        processData: true,
	        scriptsRunFirst: true,
	        emulateOnload: false,
	        start: true,
	        cache: false,
	        onsuccess: BX.delegate(function(data) {

	        	
	        	if (data.response)
	        	{
	        		app.onCustomEvent("OnIblock");
	        		app.closeController();
	        	}
	        	else if (data.error)
	        	{
	        		this.HideProgress();
	        		alert(data.error.error_msg);
	        	}
	        	
	        }, this)
	    });
		
		
	}
			
})(window);