<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$iblock_id = intval($arParams['IBLOCK_ID']);
$section_id = intval($arParams['SECTION_ID']);
$arParams['MESS_BTN_ADD'] = trim($arParams['MESS_BTN_ADD']);

if ($_REQUEST['ajax']=='Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	$bs = new CIBlockElement;
	$arFields = Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "N"
	);
	$res = $bs->Update($_POST['ID'], $arFields);
	$arResult = array("response" => 1);
	
	die(\Bitrix\Main\Web\Json::encode($arResult));
}


if ($iblock_id > 0)
	$arIBlock = CIBlock::GetArrayByID($iblock_id);
else
	$arIBlock = false;

if ($arIBlock)
{
	$dbSection = CIBlockSection::GetList(
			array(),
			array(
					"IBLOCK_ID" => $arIBlock['ID'],
					"ID" => $section_id
			),
			true,
			array(
					"ID",
					"NAME",
					"UF_FORMAT_NAME"
			)
	);
	
	$arFilter = array();
	if($arSection = $dbSection->GetNext())
	{
		$arResult['SECTION'] = $arSection;
		
		preg_match_all("/#([a-z0-9_]+)#/i", $arResult['SECTION']['UF_FORMAT_NAME'], $matches);
		$arFilter = $matches[1];
	}
	



	$nameFormat = CSite::GetNameFormat(true);
	$arUserID = array();

	$dbTask = CIBlockElement::GetList(
			array(
					"TIMESTAMP_X" => "DESC"
			),
			array(
					"IBLOCK_ID" => $arIBlock['ID'],
					"SECTION_ID" => $section_id,
					"ACTIVE" => "Y",
					"CREATED_BY" => $USER->GetID()
			),
			false,
			false,
			array(
				"ID",
				"IBLOCK_ID",
				"CREATED_BY",
				"DATE_CREATE",
				"PROPERTY_*"
			)
	);
	
	$input = array();
	foreach ($arFilter as $field)
	{
		$input[] = '#'.$field.'#';
	}
	


	while($obTask = $dbTask->GetNextElement())
	{
		$arTask = $obTask->GetFields();
		$arProps = $obTask->GetProperties();

		$arUserID[$arTask['CREATED_BY']] = true;
		$output = array();
		foreach ($arFilter as $field)
		{
			if (substr($field, 0, 9) == 'PROPERTY_')
			{
				$code = substr($field, 9);
				if ($arProps[$code]['PROPERTY_TYPE'] == 'E')
				{
					$value = array();
					$res = CIBlockElement::GetList(array(), array("ID"=> $arProps[$code]['VALUE']));
					while($ar_res = $res->GetNext())
					{	
						$value[] = $ar_res['NAME'];
					}	
					$output[] = implode(', ', $value);
				}
				else
				{
					$output[] = (is_array($arProps[$code]['VALUE']) ? implode(', ', $arProps[$code]['VALUE']) : $arProps[$code]['VALUE']);
				}
				unset($code);
			}
			else 
				$output[] = $arTask[$field];
		}

		
		$arTask['FORMAT_NAME'] = str_replace($input, $output, $arResult['SECTION']['UF_FORMAT_NAME']);


		$arResult['TASKS'][] = $arTask;
	}
	
	if (!empty($arUserID))
	{
		$dbUser = Bitrix\Main\UserTable::getList(array(
				'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
				'filter' => array('ID' => array_keys($arUserID)),
		));
		
		while ($arUser = $dbUser->fetch())
		{
			$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	
			$arResult['USERS'][$arUser['ID']] = $arUser;
		}
		unset($arUser, $dbUser);
	}
}

$this->IncludeComponentTemplate();
?>