<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['TASKS']))
{
	echo '<div class="popup_wrap" id="popup"><div class="popup_wrap2"><div class="popup">
			<div class="bx_popup_item" onclick="event.stopPropagation(); editTask()">'.GetMessage('EDIT_TASK').'</div>
			<div class="bx_popup_item" onclick="event.stopPropagation(); cancelTask()">'.GetMessage('CANCEL_TASK').'</div>
			<div class="bx_popup_item red" onclick="event.stopPropagation(); hidePopup()">'.GetMessage('CLOSE').'</div>
		</div></div></div>';
	
	
	echo '<ul style="padding-bottom: 52px">';
	foreach ($arResult['TASKS'] as $key => $arTask)
	{
		$arUser = $arResult['USERS'][$arTask['CREATED_BY']];
		echo '<li id="bx'.$arTask['ID'].'" class="bx_profile" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ELEMENT_ID='.$arTask['ID'].'\', title: \''.$arTask['FORMAT_NAME'].'\'})">
				<div class="bx_profile_name">'.$arTask['FORMAT_NAME'].'</div>
				<div class="bx_desc">#'.$arTask['ID'].'</div>
				<img class="bx_profile_more" src="'.$templateFolder.'/images/more.png" onclick="event.stopPropagation(); showPopup('.$arParams['SECTION_ID'].', '.$arTask['ID'].', \''.GetMessage('EDIT').'\')" />		
						
						
		</li>';
	}
	unset($arUser);
	echo '</ul>';
}
else
{
	echo '<div class="bx_task_empty">'.GetMessage('LIST_EMPTY').'</div>';
}

echo '<div class="bx_task_add" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ACTION=edit\', title: \''.(!empty($arParams['MESS_BTN_ADD']) ? $arParams['MESS_BTN_ADD'] : GetMessage('ADD_TASK')).'\'})">'.(!empty($arParams['MESS_BTN_ADD']) ? $arParams['MESS_BTN_ADD'] : GetMessage('ADD_TASK')).'</div>';
?>