<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$APPLICATION->AddHeadString('<script async defer src="https://maps.googleapis.com/maps/api/js?key='.$arParams['USER_TYPE_SETTINGS']['API_KEY'].'&callback=tt'.$arParams['ID'].'.InitGoogle"></script>');

echo '<div class="bx_field">'.$arParams['NAME'].($arParams['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '').'</div>
		<div id="map'.$arParams['ID'].'" class="afs-field-map"></div>
		<input type="hidden" value="" id="'.$arParams['ID'].'" name="'.$arParams['ID'].'">';

echo '<script type="text/javascript">var tt'.$arParams['ID'].' = new JCFieldMap('.CUtil::PhpToJSObject($arParams, false, true).')</script>';
?>