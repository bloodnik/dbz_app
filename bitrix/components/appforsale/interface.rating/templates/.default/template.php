<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

	$strMainID = $this->GetEditAreaId($arParams['ID']);
	$arItemIDs = array(
		"ID" => $strMainID,
		"RATING" => $strMainID."_rating",
	);

	echo '<div class="bx_field">'.$arParams['NAME'].($arParams['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '').'</div>
		<input id="'.$arParams['ID'].'" name="'.$arParams['ID'].'" type="hidden" value="1" />
		<div id="'.$arItemIDs['RATING'].'">';
	for ($i = 1; $i <= 5; $i++)
		echo '<img data-rating="'.$i.'" class="bx_rating" src="'.$templateFolder.'/images/ic_rating_'.($i == 1 ? 'on' : 'off').'.png" />';
	echo '</div>';
	
$arJSParams = array(
	"ID" => $arParams['ID'],
	"templateFolder" => $templateFolder,
	"VISUAL" => array(
		"ID" => $arItemIDs['ID'],
		"RATING" => $arItemIDs['RATING']
	)
);
	echo '<script type="text/javascript">new JCRating('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>