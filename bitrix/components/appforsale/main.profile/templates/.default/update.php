<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CUtil::JSPostUnescape();
header("Content-Type: application/json");

if ($USER->IsAuthorized())
{
	$arFields = $_POST;
		
	// if (isset($_FILES['PERSONAL_PHOTO']))
	//	$arFields['PERSONAL_PHOTO'] = $_FILES['PERSONAL_PHOTO'];
		
	if ($USER->Update($USER->GetID(), $arFields) === true)
	{
		$arResult = array("response" => 1);
	}
	else
	{
		$arResult = array("error" => array("message" => $USER->LAST_ERROR));
	}
}

echo \Bitrix\Main\Web\Json::encode($arResult);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");
?>