<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$uf_city = intval($arResult["arUser"]['UF_CITY']);

$arItemIDs = array(
		"NAME" => "name",
		"LAST_NAME" => "last_name",
		"SECOND_NAME" => "second_name",
		"EMAIL" => "email",
		"CITY" => "city",
		"AGREE" => "agree",
		"UPDATE" => "update"
);

echo '<div style="padding: 16px">
		<div class="label">'.GetMessage('NAME').' <span style="color: red">*</span></div>
		<div><input type="text" id="'.$arItemIDs['NAME'].'" maxlength="50" value="'.$arResult["arUser"]['NAME'].'" /></div>
		<div class="label">'.GetMessage('LAST_NAME').'</div>
		<div><input type="text" id="'.$arItemIDs['LAST_NAME'].'" maxlength="50" value="'.$arResult["arUser"]['LAST_NAME'].'" /></div>
		<div class="label">'.GetMessage('SECOND_NAME').'</div>
		<div><input type="text" id="'.$arItemIDs['SECOND_NAME'].'" maxlength="50" value="'.$arResult["arUser"]['SECOND_NAME'].'" /></div>
		<div class="label">'.GetMessage('EMAIL').'</div>
		<div><input type="text" id="'.$arItemIDs['EMAIL'].'" value="'.$arResult["arUser"]['EMAIL'].'" /></div>';

		
		if (array_key_exists("UF_CITY", $arResult["arUser"]))
		{
			$val = GetMessage('SELECT');
			if (intval($arResult["arUser"]['UF_CITY']) > 0)
			{
				CModule::IncludeModule('iblock');
				$dbElement = CIBlockElement::GetByID($arResult["arUser"]['UF_CITY']);
				if($arElement = $dbElement->GetNext())
				{
					$val = $arElement['NAME'];
					$value = $arElement['ID'];
				}
			}
		
			
// 			global $USER_FIELD_MANAGER;
// 			$arFields = $USER_FIELD_MANAGER->GetUserFields("USER");
// 			CModule::IncludeModule("iblock");
// 			$rsElement = CIBlockElement::GetList(
// 					array(
// 							"ID" => "ASC"
// 					),
// 					array(
// 							"IBLOCK_ID" => $arFields['UF_CITY']['SETTINGS']['IBLOCK_ID'],
// 							"ACTIVE" => "Y"
// 					),
// 					false,
// 					false,
// 					array(
// 							"ID", "NAME"
// 					)
// 			);
					
		//	echo '<div class="label">'.GetMessage('CITY').'</div>
		//			<select id="'.$arItemIDs['CITY'].'">';
			


		//	if (empty($arResult["arUser"]['UF_CITY']))
		//		$arResult["arUser"]['UF_CITY'] = $arFields['UF_CITY']['SETTINGS']['DEFAULT_VALUE'];
			
		//	while($arElement = $rsElement->GetNext())
		//	{
		//		echo '<option value="'.$arElement['ID'].'"'.($arElement['ID'] == $arResult["arUser"]['UF_CITY'] ? ' selected="selected"' : '').'>'.$arElement['NAME'].'</option>';
		//	}
		//	echo '</select>';
		
		echo '<div class="label">'.GetMessage('CITY').' <span style="color: red">*</span></div>';
		?>
			<input type="hidden" id="address_UF_CITY_value" value="<?=$value?>">
			<span class="btn btn-default btn-block" id="address_UF_CITY_name" onclick="showE('UF_CITY', 0)"><?=$val?></span>
		<?

		
		
		
		
		
		}
		
		if ($uf_city == 0)
			echo '<div style="padding: 16px 0px"><input id="'.$arItemIDs['AGREE'].'" type="checkbox" /> '.GetMessage('AGREE').'</div>';
		
				
		echo '<div class="control"><button class="btn btn-primary" id="'.$arItemIDs['UPDATE'].'">'.GetMessage('SAVE').'</button></div>
		</div>';

$arJSParams = array(
		"VISUAL" => array(
				"NAME" => $arItemIDs['NAME'],
				"LAST_NAME" => $arItemIDs['LAST_NAME'],
				"SECOND_NAME" => $arItemIDs['SECOND_NAME'],
				"EMAIL" => $arItemIDs['EMAIL'],
				"CITY" => 'address_UF_CITY_value',
				"AGREE" => $arItemIDs['AGREE'],
				"UPDATE" => $arItemIDs['UPDATE']
		),
		"NO_BACK" => $arParams['NO_BACK'] != 'Y' ? 'N' : 'Y'
);

echo '<script type="text/javascript">BX.message({"NO_NAME":"'.GetMessage('NO_NAME').'", "NO_CITY":"'.GetMessage('NO_CITY').'"}); new JCProfile('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>