/* appforsale:main.profile */
(function (window) {

if (!!window.JCProfile)
{
	return;
}

window.JCProfile = function (arParams)
{
	this.visual = {
			NAME: '',
			LAST_NAME: '',
			SECOND_NAME: '',
			EMAIL: '',
			CITY: '',
			AGREE: '',
			UPDATE: ''
	};
	
	this.no_back = arParams.NO_BACK;
	
	this.obName = null;
	this.obLastName = null;
	this.obSecodName = null;
	this.obEmail = null;
	this.obCity = null;
	this.obAgree = null;
	this.obUpdate = null;
	
	this.enabled = { props: { disabled: false }};
	this.disabled = { props: { disabled: true }};
	
	if ('object' === typeof arParams)
	{
		this.visual = arParams.VISUAL;
	}
	
	BX.ready(BX.delegate(this.Init, this));
}

window.JCProfile.prototype.Init = function()
{
	this.obName = BX(this.visual.NAME);
	this.obLastName = BX(this.visual.LAST_NAME);
	this.obSecodName = BX(this.visual.SECOND_NAME);
	this.obEmail = BX(this.visual.EMAIL);
	this.obCity = BX(this.visual.CITY);
	this.obAgree = BX(this.visual.AGREE);
	this.obUpdate = BX(this.visual.UPDATE);
	
	if (!!this.obUpdate)
	{
		BX.bind(this.obUpdate, 'click', BX.delegate(this.Update, this));
	}
	
	if (!!this.obAgree)
	{
		BX.adjust(this.obUpdate, this.disabled);
		BX.bind(this.obAgree, 'click', BX.delegate(this.Checked, this));
	}
}

window.JCProfile.prototype.Checked = function()
{
	if (this.obAgree.checked)
		BX.adjust(this.obUpdate, this.enabled);
	else
		BX.adjust(this.obUpdate, this.disabled);
}


window.JCProfile.prototype.ShowProgress = function()
{
	BX.adjust(this.obName, this.disabled);
	BX.adjust(this.obLastName, this.disabled);
	BX.adjust(this.obSecodName, this.disabled);
	BX.adjust(this.obEmail, this.disabled);
	if (this.obCity != null)
		BX.adjust(this.obCity, this.disabled);
	BX.adjust(this.obUpdate, this.disabled);
}

window.JCProfile.prototype.HideProgress = function()
{
	BX.adjust(this.obName, this.enabled);
	BX.adjust(this.obLastName, this.enabled);
	BX.adjust(this.obSecodName, this.enabled);
	BX.adjust(this.obEmail, this.enabled);
	if (this.obCity != null)
		BX.adjust(this.obCity, this.enabled);
	BX.adjust(this.obUpdate, this.enabled);
}

window.JCProfile.prototype.Update = function()
{
	if (!this.obName.value)
		return alert(BX.message('NO_NAME'));
	
	if (this.obCity != null && !this.obCity.value)
		return alert(BX.message('NO_CITY'));
	
	
	
	this.ShowProgress();	
	BX.ajax({
        url: "/bitrix/components/appforsale/main.profile/templates/.default/update.php",
        data: {
        	NAME: this.obName.value,
        	LAST_NAME: this.obLastName.value,
        	SECOND_NAME: this.obSecodName.value,
        	EMAIL: this.obEmail.value,
        	UF_CITY: (this.obCity != null ? this.obCity.value : ""),
		},
        method: 'POST',
        dataType: 'json',
        timeout: 15,
        async: true,
        processData: true,
        scriptsRunFirst: true,
        emulateOnload: false,
        start: true,
        cache: false,
        onsuccess: BX.delegate(function(data){
        	if (data.response)
			{
        		app.onCustomEvent("OnAfterUserUpdate");
        		if (this.no_back == 'N')
        			app.closeController();
        		else
        			app.reload();
			}
			else if (data.error)
			{
	        	this.HideProgress();
				alert(data.error.message);
			}
        }, this),
        onfailure: BX.delegate(function() {
        	this.HideProgress();
        }, this)
    });
}

BX.ready(function() {
	app.hideProgress();
});

})(window);