/* appforsale:colorpicker */
(function (window) {
	
	if (!!window.JSColorPicker)
	{
		return;
	}
	
	window.JSColorPicker = function(arParams)
	{		
		this.currentItem = null;
		
		this.visual = {
			VALUE: '',
			ITEMS: '',
			CUSTOM: '',
			CUSTOM_COLOR: ''
		};
		
		this.obValue = null;
		this.obItems = null;
		this.obCustom = null;
		this.obCustomColor = null;
		
		if ('object' === typeof arParams)
		{
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JSColorPicker.prototype.Init = function()
	{
		this.obValue = BX(this.visual.VALUE);
		this.obItems = BX(this.visual.ITEMS);
		this.obCustom = BX(this.visual.CUSTOM);
		this.obCustomColor = BX(this.visual.CUSTOM_COLOR);
		
		var cur = BX.findChild(this.obItems, {
			attribute: {"data-color": this.obValue.value}
        }, true);
		
		BX.delegate(this.selectItem(cur || this.obCustom), this);
		this.currentItem = cur || this.obCustom;
		
		BX.bind(this.obItems, 'click', BX.delegate(this.onItemClick, this));
	}
	
	window.JSColorPicker.prototype.onItemClick = function()
	{
		var target = event.target;
		while (target != this.obItems)
		{
			if (target && target.nodeType && target.nodeType == 1 && BX.hasClass(target, "afs-colorpicker-item"))
			{
				result = null;
				if (BX.hasClass(target, "custom"))
				{
					result = prompt('Hex', '#ff0000');
					if (!result)
						return;
					
					result = '#' + result.replace('#', '');
				}
				
				this.obValue.value = result || target.getAttribute('data-color');
				
				if (this.currentItem != null)
					this.unselectItem(this.currentItem);
	
				BX.delegate(this.selectItem(target), this);
				this.currentItem = target;
				
				return;
			}
			target = target.parentNode;
		}
	}
	
	window.JSColorPicker.prototype.selectItem = function(item)
	{
		if (!BX.hasClass(item, "afs-colorpicker-item-active"))
			BX.addClass(item, "afs-colorpicker-item-active");
				
		if (BX.hasClass(item, "custom"))
			BX.adjust(this.obCustomColor, {style: {backgroundColor: this.obValue.value}});	
	}
	
	window.JSColorPicker.prototype.unselectItem = function(item)
	{
		BX.removeClass(item, "afs-colorpicker-item-active");
	}
	
})(window);