<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId($arParams['ID']);
$arItemIDs = array(
	"ID" => $strMainID,
	"CONTAINER" => $strMainID."_container",
	"ADD" => $strMainID."_add",
	"INPUT" => $arParams['ID'],
	"PROGRESS" => $strMainID."_progress"
);


echo '<div class="bx_field">'.$arParams['NAME'].($arParams['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '').'</div>
		<div>
			<span id="'.$arItemIDs['CONTAINER'].'"></span>
			<div id="'.$arItemIDs['ADD'].'" class="bx_file_add">
				<input id="'.$arItemIDs['INPUT'].'" class="bx_file_add_input" type="file" />
				<img src="'.$templateFolder.'/images/add.png" class="bx_file_add_img" />
			</div>
		</div>';

if (!empty($arParams['VALUE']))
{
	if (!is_array($arParams['VALUE']))
		$arParams['VALUE'] = array($arParams['VALUE']);
	foreach ($arParams['VALUE'] as &$file_id)
	{
		$file = CFile::ResizeImageGet($file_id, array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true);
		$file_id = array(
			"ID" => $file_id,
			"SRC" => $file['src']
		);
	}
}

$arJSParams = array(
	"ID" => $arParams['ID'],
	"templateFolder" => $templateFolder,
	"MULTIPLE" => $arParams['MULTIPLE'],
	"VISUAL" => array(
		"ID" => $arItemIDs['ID'],
		"CONTAINER" => $arItemIDs['CONTAINER'],
		"ADD" => $arItemIDs['ADD'],
		"INPUT" => $arItemIDs['INPUT']
	)
);

echo '<script type="text/javascript">
		var jcfile = new JCFile('.CUtil::PhpToJSObject($arJSParams, false, true).');';
if (!empty($arParams['VALUE']))
	echo 'jcfile.createFile('.CUtil::PhpToJSObject($arParams['VALUE'], false, true).');';
echo '</script>';
?>