<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arTransactTypes = array(
	"ORDER_PAY" => GetMessage("STA_TPAY"),
	"ORDER_PAY_PART" => GetMessage("STA_TPAY_PART"),
	"CC_CHARGE_OFF" => GetMessage("STA_TFROM_CARD"),
	"OUT_CHARGE_OFF" => GetMessage("STA_TMONEY"),
	"ORDER_UNPAY" => GetMessage("STA_TCANCEL_ORDER"),
	"ORDER_CANCEL_PART" => GetMessage("STA_TCANCEL_SEMIORDER"),
	"MANUAL" => GetMessage("STA_THAND"),
	"DEL_ACCOUNT" => GetMessage("STA_TDEL"),
	"AFFILIATE" => GetMessage("STA_AF_VIP"),
	"EXCESS_SUM_PAID" => GetMessage("STA_TTRANSF_EXCESS_SUM_PAID"),
	"ORDER_PART_RETURN" => GetMessage("STA_TRETURN"),
	"TARIFF_PAY" => GetMessage("STA_TTARIFF")
);

$rsTransact = CAppforsaleUserTransact::GetList(
		array(
				"TRANSACT_DATE" => "DESC"
		),
		array(
				"USER_ID" => $USER->GetID()
		),
		false,
		false,
		array(
				"ID",
				"AMOUNT",
				"DEBIT",
				"NOTES",
				"DESCRIPTION",
				"ORDER_ID",
				"TRANSACT_DATE"
		)
);

while($arTransact = $rsTransact->GetNext())
{
	if(strpos($arTransact['DESCRIPTION'], "Оплата заказа") === 0) {
		$arTransact['NOTES'] = $arTransact['DESCRIPTION'];
		$arTransact['DESCRIPTION'] = "Оплата заказа с основного баланса";
	} else {
		$arTransact['DESCRIPTION'] = $arTransactTypes[$arTransact['DESCRIPTION']];
	}
	$arResult['TRANSACT'][] = $arTransact;
//	PR($arTransact);
}

$entity_data_class = getHighloadEntityByName("DBZBonusTransact");
$rsData = $entity_data_class::getList(array(
	"select" => array("*"),
	"order"=> ["UF_DATE" => "DESC"],
	"filter" => [
		"UF_USER" => $USER->GetID()
	],
));

$arResult['BONUS_TRANSACT'] = [];
while ($arData = $rsData->Fetch()) {
	$arTransact["AMOUNT"] = round($arData["UF_SUMM"], 2);
	$arTransact["DEBIT"] = $arData["UF_DEBIT"] > 0 ? "Y" : "N";
	$arTransact["NOTES"] = $arData["UF_NOTE"];
	$arTransact["NOTES"] = $arData["UF_NOTE"];
	$arTransact["TRANSACT_DATE"] = $arData["UF_DATE"]->toString();
	$arTransact["DESCRIPTION"] = $arData["UF_DEBIT"] > 0 ? "Начисление бонусов" : "Списание бонусов";
	$arResult['BONUS_TRANSACT'][] = $arTransact;
}

$arResult['TRANSACT'] =  array_merge($arResult['TRANSACT'], $arResult['BONUS_TRANSACT']);
function dateSort( $a, $b ) {
	return strtotime($b["TRANSACT_DATE"]) - strtotime($a["TRANSACT_DATE"]) ;
}
usort($arResult['TRANSACT'], "dateSort");


$bonusAcсount = new CAppforsaleTaskBonusAccount($USER->GetID());
$arResult["BONUS_ACCOUNT"] = $bonusAcсount->getAccount();

$returnRating = new CAppforsaleReturnsRating($USER->GetID());
$arResult["RETURNS_RATING"] = $returnRating->getReturnsRating();

$this->IncludeComponentTemplate();
?>