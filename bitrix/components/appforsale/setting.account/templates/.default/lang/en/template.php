<?
$MESS['TRANSACT'] = 'Детализация';

$MESS['STA_TO_ACCOUNT'] = "(на счет)";
$MESS['STA_FROM_ACCOUNT'] = "(со счета)";

$MESS['INFO'] = 'Голоса - это уникальная условная единица для приобретения подписок и услуг';
$MESS['DESC'] = 'Право использования голосов предоставляется на условиях <a onclick="app.loadPageBlank({url: \'/youdo/personal/?ITEM=agreement\', title: \'Лицензионное соглашение\'})">Лицензионного соглашения</a>. Возврат средств невозможен.';
$MESS['PAYMENTS'] = 'Платежи';

$MESS['REFILL_HEADER'] = 'Пополнить баланс';
$MESS['REFILL'] = 'Пополнить';

$MESS['REFILL_BALANCE'] = 'Пополнение баланса';
$MESS['BALANCE'] = 'Ваш баланс';
?>