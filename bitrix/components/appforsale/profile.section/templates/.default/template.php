<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['PROFILES']))
{
	echo '<div class="bx_toggle"><div class="btn btn-primary bx_left_button">'.GetMessage('LIST').'</div><div class="bx_right_button" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ACTION=map\', title: \''.GetMessage('PERFORMERS_ON_THE_MAP').'\'})">'.GetMessage('ON_THE_MAP').'</div></div>';
	
	echo '<ul style="padding-bottom: 52px">';
	foreach ($arResult['PROFILES'] as $key => $arProfile)
	{
		$arUser = $arResult['USERS'][$arProfile['CREATED_BY']];
		echo '<li class="bx_profile" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ELEMENT_ID='.$arProfile['ID'].'\', title: \''.$arProfile['FORMAT_NAME'].'\'})">
				<img src="'.($arUser['PERSONAL_PHOTO'] ? $arUser['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/user_no_photo.png').'" class="bx_profile_personal_photo" />
				<div class="bx_profile_name">'.$arProfile['FORMAT_NAME'].'</div>
				<img class="bx_profile_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />
		</li>';
	}
	unset($arUser);
	echo '</ul>';
	
	echo '<div class="btn btn-primary bx_filter_button" onclick="BX.adjust(BX(\'filter\'), {style: {display: \'block\'}})">'.GetMessage('FILTER').'</div>';

}
else
{
	echo '<div class="bx_profile_empty">'.GetMessage('LIST_EMPTY').'</div>';
}
?>