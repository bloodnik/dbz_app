<?
$MESS['TASK'] = 'Новое задание'; 
$MESS['OFFER'] = 'Новое предложение';
$MESS['EXECUTOR'] = 'Вы назначены исполнителем';
$MESS['REJECT'] = 'Предложение отклонено';
$MESS['DONE'] = 'Исполнитель завершил задание';
$MESS['ARCHIVE'] = 'Задание выполнено';
$MESS['PAYMENT'] = 'Пополнение счета';
$MESS['COMMENT'] = 'Комментарии';
?>