<?
use Mlab\Appforsale\MobileApp;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$device_id = MobileApp::getInstance()->getDeviceId();

$rsDevice = $DB->Query("SELECT ID, SETTINGS FROM mlab_appforsale_push_device WHERE DEVICE_ID = '".$device_id."' LIMIT 1");
if ($arDevice = $rsDevice->Fetch())
{
	$arResult['SETTINGS'] = unserialize($arDevice['SETTINGS']);
}

if ($_REQUEST['AJAX_CALL'] == 'Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	$ID = (int) $_POST['ID'];
	if ($_POST['ACTION'] == 'on')
	{
		$arResult['SETTINGS'][$ID] = 'on';
	}
	else
	{
		$arResult['SETTINGS'][$ID] = 'off';
	}
	
	$DB->Update(
		"mlab_appforsale_push_device",
		array(
			"SETTINGS" => "'".serialize($arResult['SETTINGS'])."'"
		),
		"WHERE ID='".$arDevice['ID']."'"
	);
	$arRes = array("response" => 1);
	die(\Bitrix\Main\Web\Json::encode($arRes));
}

$dbNotify = array(
	array(
		'ID' => 1,
		'NAME' => GetMessage('TASK')
	),
	array(
		'ID' => 2,
		'NAME' => GetMessage('OFFER')
	),
	array(
		'ID' => 3,
		'NAME' => GetMessage('EXECUTOR')
	),
	array(
		'ID' => 8,
		'NAME' => GetMessage('REJECT')
	),
	array(
		'ID' => 4,
		'NAME' => GetMessage('DONE')
	),
	array(
		'ID' => 5,
		'NAME' => GetMessage('ARCHIVE')
	),
	array(
		'ID' => 6,
		'NAME' => GetMessage('PAYMENT')
	),
	array(
		'ID' => 7,
		'NAME' => GetMessage('COMMENT')
	)
);

foreach ($dbNotify as $arNotify)
{
	if (!empty($arResult['SETTINGS']))
	{
		if ($arResult['SETTINGS'][$arNotify['ID']] == 'off')
			$arNotify['TOGLE'] = false;
		else
			$arNotify['TOGLE'] = true;
	}
	else
	{
		$arNotify['TOGLE'] = true;
	}
	$arResult['NOTIFY'][$arNotify['ID']] = $arNotify;
}

$this->IncludeComponentTemplate();
?>