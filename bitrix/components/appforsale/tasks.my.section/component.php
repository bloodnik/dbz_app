<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$iblock_id = intval($arParams['IBLOCK_ID']);
$section_id = intval($arParams['SECTION_ID']);
$arParams['MESS_BTN_ADD'] = trim($arParams['MESS_BTN_ADD']);

if ($_REQUEST['ajax']=='Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	$bs = new CIBlockElement;
	$arFields = Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "N"
	);
	$res = $bs->Update($_POST['ID'], $arFields);
	$arResult = array("response" => 1);
	
	die(\Bitrix\Main\Web\Json::encode($arResult));
}


if ($iblock_id > 0)
	$arIBlock = CIBlock::GetArrayByID($iblock_id);
else
	$arIBlock = false;

if ($arIBlock)
{
	$nameFormat = CSite::GetNameFormat(true);
	$arUserID = array();

	$dbTask = CIBlockElement::GetList(
			array(
					"TIMESTAMP_X" => "DESC"
			),
			array(
					"IBLOCK_ID" => $arIBlock['ID'],
					"ACTIVE" => "Y",
					"CREATED_BY" => $USER->GetID()
			),
			false,
			false,
			array(
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"CREATED_BY",
					"DATE_CREATE",
				"PROPERTY_*"
			)
	);
	

	
	$secID = array();
	while($obTask = $dbTask->GetNextElement())
	{
		$arTask = $obTask->GetFields();
		$secID[$arTask['IBLOCK_SECTION_ID']] = true;
		$arTask['PROPERTIES'] = $obTask->GetProperties();
		$arResult['TASKS'][] = $arTask;
	}
	
	
		$dbSection = CIBlockSection::GetList(
				array(),
				array(
						"IBLOCK_ID" => $arIBlock['ID'],
						"ID" => array_keys($secID)
				),
				true,
				array(
						"ID",
						"NAME",
						"UF_FORMAT_NAME"
				)
		);
	
	// 	$arFilter = array();
		while($arSection = $dbSection->GetNext())
		{
			preg_match_all("/#([a-z0-9_]+)#/i", $arSection['UF_FORMAT_NAME'], $matches);
		 	$arSection['FILTER'] = $matches[1];
		 	
		 	$arSection['INPUT'] = array();
		 		foreach ($arSection['FILTER'] as $field)
		 			{
		 				$arSection['INPUT'][] = '#'.$field.'#';
		 			}
			
			$arResult['SECTIONS'][$arSection['ID']] = $arSection;
		}
	


	foreach($arResult['TASKS'] as &$arTask)
	{
		$arProps = $arTask['PROPERTIES'];

		$arUserID[$arTask['CREATED_BY']] = true;
		$output = array();
		foreach ($arResult['SECTIONS'][$arTask['IBLOCK_SECTION_ID']]['FILTER'] as $field)
		{
			if (substr($field, 0, 9) == 'PROPERTY_')
			{
				$code = substr($field, 9);
				if ($arProps[$code]['PROPERTY_TYPE'] == 'E')
				{
					$value = array();
					$res = CIBlockElement::GetList(array(), array("ID"=> $arProps[$code]['VALUE']));
					while($ar_res = $res->GetNext())
					{	
						$value[] = $ar_res['NAME'];
					}	
					$output[] = implode(', ', $value);
				}
				else
				{
					$output[] = (is_array($arProps[$code]['VALUE']) ? implode(', ', $arProps[$code]['VALUE']) : $arProps[$code]['VALUE']);
				}
				unset($code);
			}
			else 
				$output[] = $arTask[$field];
		}

		
		$arTask['FORMAT_NAME'] = str_replace($arResult['SECTIONS'][$arTask['IBLOCK_SECTION_ID']]['INPUT'], $output, $arResult['SECTIONS'][$arTask['IBLOCK_SECTION_ID']]['UF_FORMAT_NAME']);
	}
	
	if (!empty($arUserID))
	{
		$dbUser = Bitrix\Main\UserTable::getList(array(
				'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
				'filter' => array('ID' => array_keys($arUserID)),
		));
		
		while ($arUser = $dbUser->fetch())
		{
			$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	
			$arResult['USERS'][$arUser['ID']] = $arUser;
		}
		unset($arUser, $dbUser);
	}
}

$this->IncludeComponentTemplate();
?>