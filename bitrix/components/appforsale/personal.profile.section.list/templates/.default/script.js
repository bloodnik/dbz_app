/* appforsale:personal.profile.section.list */
(function (window) {

	if (!!window.JCProfileSection)
	{
		return;
	}
	
	window.JCProfileSection = function (arParams)
	{
		this.id = 0;
		this.subscribe_id = 0;
		this.templateFolder = null;
		this.visual = {
				SWITCH: '',
				PROGRESS: ''
		};
		
		this.obSwitch = null;
		this.obProgress = null;
				
		if ('object' === typeof arParams)
		{
			this.subscribe_id = arParams.SUBSCRIBE_ID;
			this.id = arParams.ID;
			this.templateFolder = arParams.templateFolder;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCProfileSection.prototype.ShowProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "none"}});
		BX.adjust(this.obProgress, {style: {display: "block"}});
	}
	
	window.JCProfileSection.prototype.HideProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "block"}});
		BX.adjust(this.obProgress, {style: {display: "none"}});
	}
	
	window.JCProfileSection.prototype.Init = function()
	{
		this.obSwitch = BX(this.visual.SWITCH);
		this.obProgress = BX(this.visual.PROGRESS);
		
		if (!!this.obSwitch)
		{
			BX.bind(this.obSwitch, 'click', BX.delegate(this.Switch, this));
		}
		
		app.hideProgress();
	}

	window.JCProfileSection.prototype.Switch = function()
	{
		this.ShowProgress();
		BX.ajax({
	        url: location.href,
	        data: {
				AJAX_CALL: 'Y',
				ACTION: (this.subscribe_id > 0 ? "unsubscribe" : "subscribe"),
				SUBSCTIBE_ID: this.subscribe_id,
				ID: this.id
			},
	        method: 'POST',
	        dataType: 'json',
	        onsuccess: BX.delegate(this.onsuccess, this)
	    });
		event.stopPropagation();
	}
	
	window.JCProfileSection.prototype.onsuccess = function(data)
	{
		if (data.response)
		{
			if (this.subscribe_id > 0)
			{
				this.subscribe_id = 0;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_off.png"}});
			}
			else
			{
				this.subscribe_id = data.response;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_on.png"}});
			}
		}
		else if (data.error)
		{
			alert(data.error.error_msg);
		}
		this.HideProgress();
	}
	
})(window);