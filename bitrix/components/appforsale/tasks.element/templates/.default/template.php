<? 
use Bitrix\Main\UserTable;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$nameFormat = CSite::GetNameFormat(true);

$uid = intval($arResult['PROPERTIES']['PROFILE_ID']['VALUE']);

$bOffer = true;

if (!empty($arResult['DISPLAY_FIELDS']))
{
	echo '<ul class="bx_field_list">';
	foreach ($arResult['DISPLAY_FIELDS'] as $field)
	{
		echo '<li class="bx_field_list">';
		echo '<div class="bx_name">'.$field['NAME'].'</div>';
		echo '<div class="bx_value">';
		if ($field['PROPERTY_TYPE'] == 'F')
		{
			if (!is_array($field['VALUE']))
				$field['VALUE'] = array($field['VALUE']);
			foreach ($field['VALUE'] as $file_id)
			{
				$arFile = CFile::ResizeImageGet($file_id, array('width' => 128, 'height' => 128), BX_RESIZE_IMAGE_EXACT, true);
		
				echo '<img src="'.$arFile['src'].'" />';
			}
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'map_google')
		{
			echo '<img border="0" style="width: 100%;" src="https://maps.googleapis.com/maps/api/staticmap?key='.$field['USER_TYPE_SETTINGS']['API_KEY'].'&size=480x250&center='.$field['VALUE'].'&zoom=16&markers=color:red%7C%7C'.$field['VALUE'].'"  />';
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'HTML')
		{
			if ($field['VALUE']['TYPE'] == 'HTML')
				echo htmlspecialcharsBack(FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']));
			else 
				echo FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']);
		}
		else
			echo (is_array($field['VALUE']) ? implode(', ', $field['VALUE']) : $field['VALUE']);
		
		echo '</div>';
		echo '</li>';
	}
	
	if ($uid > 0)
	{
		$userIterator = UserTable::getList(array(
				'select' => array('ID', 'NAME', 'LAST_NAME', 'PERSONAL_PHOTO'),
				'filter' => array('ID' => $uid),
		));
		if ($arUser = $userIterator->fetch())
		{
			$bOffer = false;
			$name = CUser::FormatName($nameFormat, $arUser);
			echo '<li class="bx_field_list" onclick="app.loadPageBlank({url: \'/youdo/profile/?USER_ID='.$arUser['ID'].'\', title: \''.$name.'\'})">';
			echo '<div class="bx_name">'.GetMessage('CONTRACTOR').'</div>';
			echo '<div class="bx_value">'.$name.'</div>';
			
			
			if ($arResult["PROPERTIES"]['STATUS_ID']['VALUE'] != "F")
			{
			
				echo '<div style="text-align: right">';
				if ($arResult['CAN_COMMENT']) 
					echo '<button style="width: 100%" class="mini neutral" onclick="event.stopPropagation(); app.loadPageBlank({url: \'?ACTION=comment&ELEMENT_ID='.$uid.'&TASK_ID='.$arResult['ID'].'\', title: \''.GetMessage('COMMENT').'\'})">'.GetMessage('COMMENT').'</button> ';
				echo '</div>';
				
				echo '<div><button style="width: 48%; margin-right: 2%;" class="mini" onclick="complete('.$arResult['ID'].')">'.GetMessage('COMPLETE').'</button><button style="width: 48%; margin-left: 2%;" class="mini" onclick="unsubscribe('.$arUser['ID'].', \''.GetMessage('COMMENT').'\')">'.GetMessage('UNSUBSCRIBE').'</button></div>';
			}
			echo '</li>';
		}
	}
	
	echo '</ul>';
}

if (!empty($arResult['OFFERS']) && $bOffer && $arResult["PROPERTIES"]['STATUS_ID']['VALUE'] != "F")
{
	echo '<div class="bx_order_offers" id="bx_offers">';
	echo '<div class="bx_order_offer_header">'.GetMessage('OFFER').'</div>';
	echo '<ul>';
	foreach ($arResult['OFFERS'] as $key => $arOrder)
	{
		$strMainID = $this->GetEditAreaId($arOrder['ID']);
		$arItemIDs = array(
				'ID' => $strMainID,
				'CANCEL' => $strMainID.'_cancel',
				'CONFIRM' => $strMainID.'_confirm',
		);

		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
		$user = $arResult['USERS'][$arOrder['CREATED_BY']];

		echo '<li id="'.$arItemIDs['ID'].'" class="bx_order_offer2">
				<div class="bx_order_offer">
					<img src="'.($user['PERSONAL_PHOTO'] ? $user['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_order_offer_photo" />
					<div class="bx_order_offer_info">
						<div class="bx_order_offer_name">'.$user['FORMAT_NAME'].'</div>
						<div class="bx_order_offer_date">'.$arOrder['DATE_CREATE'].'</div>
					</div>
				<div class="bx_order_offer_detail_text">'.$arOrder['PROPERTY_TEXT_VALUE'].'</div>';
		echo '<div style="text-align: right;">
				<button id="'.$arItemIDs['CANCEL'].'" class="mini neutral">'.GetMessage('REJECT').'</button> <button id="'.$arItemIDs['CONFIRM'].'" class="mini">'.GetMessage('CONFIRM').'</button></div>';

		$arJSParams = array(
				'ID' => $arOrder['ID'],
				'USER_ID' => $arOrder['CREATED_BY'],
				'USER_NAME' => $user['FORMAT_NAME'],
				'VISUAL' => array(
						'ID' => $arItemIDs['ID'],
						'CANCEL' => $arItemIDs['CANCEL'],
						'CONFIRM' => $arItemIDs['CONFIRM']
				)
		);

		echo '<script type="text/javascript">'.$strObName.' = new JCOffer('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';


		echo '</div></li>';

	}
	echo '</ul>';
	echo '</div>';
}
?>