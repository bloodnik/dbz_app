<? 
$MESS['PAYED'] = 'Payed';
$MESS['UF_TEST_PERIOD_RU'] = 'Тестовый период';
$MESS['UF_TEST_PERIOD_EN'] = 'Test period';
$MESS['UF_PRICE_RU'] = 'Стоимость';
$MESS['UF_PRICE_EN'] = 'Cost';
$MESS['UF_PRICE_TYPE_RU'] = 'Тип монетизации';
$MESS['UF_PRICE_TYPE_EN'] = 'Price type';

$MESS['PRICE_TYPE_F'] = 'Фиксированная цена за отклик на заявку';
$MESS['PRICE_TYPE_S'] = 'Подписка на доступ к заявкам';
$MESS['PRICE_TYPE_P'] = 'Процент с завершенной сделки';
?>