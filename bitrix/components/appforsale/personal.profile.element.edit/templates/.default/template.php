<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:edit",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arParams['ELEMENT_ID'],
				"FIELD_CODE" => $arResult['FIELD_CODE'],
				"DATA" => array(
					"IBLOCK_SECTION_ID" => $arParams['SECTION_ID']
				)
			),
			false
	);
?>