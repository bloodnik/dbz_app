<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized())
{
	CAppforsale::AuthForm();
}

$arVariables = array(
	"SECTION_ID" => $_GET['SECTION_ID'],
	"ELEMENT_ID" => $_GET['ELEMENT_ID']
);

if (isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0)
{
	$componentPage = 'element';
}
else 
{
//elseif (isset($arVariables["SECTION_ID"]) && intval($arVariables["SECTION_ID"]) > 0)
//{
	$componentPage = 'section';
//}
//else
//{
//	$componentPage = 'sections';	
}

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate($componentPage);
?>