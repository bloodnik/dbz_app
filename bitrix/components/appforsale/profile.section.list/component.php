<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

CModule::IncludeModule("iblock");

$rsSection = CIBlockSection::GetList(
		array(
			"LEFT_MARGIN" => "ASC"
		),
		array(
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"ACTIVE" => "Y",
			"SECTION_ID" => (intval($_GET['IBLOCK_SECTION_ID']) > 0 ? $_GET['IBLOCK_SECTION_ID'] : false)
		),
		false,
		array(
			"ID", "NAME", "PICTURE", "LEFT_MARGIN", "RIGHT_MARGIN"
		)
);

while($arSection = $rsSection->GetNext())
{
	$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::ResizeImageGet($arSection['PICTURE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	$arResult['SECTIONS'][] = $arSection;
}

$this->IncludeComponentTemplate();
?>