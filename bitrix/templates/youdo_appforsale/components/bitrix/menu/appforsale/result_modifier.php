<?

use Bitrix\Main\Localization\Loc;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

Loc::loadMessages(__FILE__);

$arParams['ASIDE_BG']                 = $arParams['ASIDE_BG'] ?: '#FFFFFF';
$arParams['ASIDE_LOGIN_BG']           = $arParams['ASIDE_LOGIN_BG'] ?: 'default';
$arParams['ASIDE_SWITCH_SHOW']        = $arParams['ASIDE_SWITCH_SHOW'] ?: 'N';
$arParams['ASIDE_SWITCH_LEFT_NAME']   = $arParams['ASIDE_SWITCH_LEFT_NAME'] ?: Loc::getMessage('ASIDE_SWITCH_LEFT_NAME');
$arParams['ASIDE_SWITCH_LEFT_LINK']   = trim($arParams['ASIDE_SWITCH_LEFT_LINK']);
$arParams['ASIDE_SWITCH_LEFT_TITLE']  = $arParams['ASIDE_SWITCH_LEFT_TITLE'] ?: Loc::getMessage('ASIDE_SWITCH_LEFT_TITLE');
$arParams['ASIDE_SWITCH_RIGHT_NAME']  = $arParams['ASIDE_SWITCH_RIGHT_NAME'] ?: Loc::getMessage('ASIDE_SWITCH_RIGHT_NAME');
$arParams['ASIDE_SWITCH_RIGHT_LINK']  = trim($arParams['ASIDE_SWITCH_RIGHT_LINK']);
$arParams['ASIDE_SWITCH_RIGHT_TITLE'] = $arParams['ASIDE_SWITCH_RIGHT_TITLE'] ?: Loc::getMessage('ASIDE_SWITCH_RIGHT_TITLE');
$arParams['ASIDE_SWITCH_CLOSE_MENU']  = $arParams['ASIDE_SWITCH_CLOSE_MENU'] ?: 'Y';
$arParams['ASIDE_SWITCH_GROUPS']      = intval($arParams['ASIDE_SWITCH_GROUPS']);

$arResult['NOTIFICATION_COUNT'] = 0;


if ($USER->IsAuthorized()) {
	$entity_data_class = getHighloadEntity(1);
	$notification_count = $entity_data_class::getCount(array("UF_ACTIVE" => 1, "UF_CREATED_BY" => $USER->GetID()));

	$arResult['NOTIFICATION_COUNT'] = intval($notification_count);



//	CModule::IncludeModule('iblock');
//	$dbElement          = CIBlockElement::GetList(
//		array(),
//		array(
//			'IBLOCK_CODE' => 'notification',
//			'ACTIVE'      => 'Y',
//			'CREATED_BY'  => $USER->GetID()
//		)
//	);
//	$notification_count = 0;
//	while ($arElement = $dbElement->GetNext()) {
//		$notification_count++;
//	}
//
//	$arResult['NOTIFICATION_COUNT'] = $notification_count;
} else {
	$arParams['ASIDE_SWITCH_SHOW'] = 'N';
}
?>