<?

use Bitrix\Main\Localization\Loc;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

Loc::loadMessages(__FILE__);

if ($USER->IsAuthorized()) {
	$dbUser = CUser::GetByID($USER->GetID());
	if ($arUser = $dbUser->Fetch()) {
		$nameFormat               = CSite::GetNameFormat(true);
		$arUser['FORMAT_NAME']    = CUser::FormatName($nameFormat, $arUser);
		$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	}
}
?>

<div id="native" class="primary-bg hidden"></div>
<div class="afs-top-nav">

	<div class="afs-aside-header" id="afs-aside-header">
		<? if ($USER->IsAuthorized()): ?>
			<div class="afs-aside-user">
				<a href="/youdo/personal/">
					<img class="afs-aside-user-photo" src="<?=$arUser['PERSONAL_PHOTO']['src'] ?: $templateFolder . '/images/user_no_photo.png'?>"/>
					<div class="afs-aside-user-name"><?=$arUser['FORMAT_NAME']?></div>
					<div class="afs-aside-user-name"><?=$arUser['LOGIN']?></div>
				</a>
			</div>
		<? else: ?>
			<div class="afs-aside-login">
				<a href="/youdo/login/"><?=Loc::getMessage('REGISTRATION')?></a>
			</div>
		<? endif; ?>
	</div>

	<? if ( ! empty($arResult)): ?>
		<ul id="ul_1">
			<? foreach ($arResult as $key => $arItem): ?>
				<? if ($arItem['SELECTED']) {
					$arItem['PARAMS']['class'] .= ' afs-selected';
				} ?>


				<? if (empty($arItem["TEXT"])): ?>
					<? continue; ?>
				<? endif; ?>

				<li <?=($arItem['PARAMS']['class'] ? ' class="afs-menu-item ' . $arItem['PARAMS']['class'] . '"' : 'class="afs-menu-item"')?>>
					<a <?=($arItem['LINK'] ? ' href="' . $arItem['LINK'] . '"' : '')?>
							title="<?=$arItem['PARAMS']['title'] ?: $arItem['TEXT']?>"<?=($arItem['PARAMS']['target'] ? ' target="' . $arItem['PARAMS']['target'] . '"' : '')?>>
						<? if ($arItem['PARAMS']['img']): ?>
							<div class="afs-top-nav-image">
								<img class="afs-top-nav-img svg" src="<?=$arItem['PARAMS']['img']?>"/>
							</div>
						<? endif; ?>
						<span><?=$arItem['TEXT']?></span>
					</a>
					<? if ($arItem['LINK'] == '/youdo/notification/'): ?>
						<span id="notification_count" style="display: <?=$arResult['NOTIFICATION_COUNT'] > 0 ? 'block' : 'none'?>" class="badge badge-default afs-menu-badge hidden-md hidden-lg">
							<?=($arResult['NOTIFICATION_COUNT'] > 0 ? $arResult['NOTIFICATION_COUNT'] : '')?>
						</span>
					<? endif; ?>
				</li>
				<div class="afs-menu-divider"></div>

				<? if ($key == '5'): ?>
					<li class="hidden-md hidden-lg" wfd-id="104"><a href="javascript:void(0)" onclick="bottomSheet.$refs.bottomSheet.fabClickHandler()" title="Чат для мастеров">
							<div class="afs-top-nav-image" wfd-id="106">
								<img class="afs-top-nav-img svg" src="/youdo/images/icons/menu_message.svg">
							</div>
							<span wfd-id="105">Чат для мастеров</span></a>
					</li>
					<div class="afs-menu-divider"></div>
				<? endif; ?>




			<? endforeach; ?>
		</ul>
	<? endif; ?>
</div>
<?
$signer       = new Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'menu');
?>
<script>
    new JCLeftMenu(<?=CUtil::PhpToJSObject(array(
		'signedParamsString' => CUtil::JSEscape($signedParams),
		'isContractor'       => CSite::InGroup(array($arParams['ASIDE_SWITCH_GROUPS'])),
		'closeMenu'          => $arParams['ASIDE_SWITCH_CLOSE_MENU'],
		'switchShow'         => $arParams['ASIDE_SWITCH_SHOW'],
		'switchLeftName'     => $arParams['ASIDE_SWITCH_LEFT_NAME'],
		'switchLeftLink'     => $arParams['ASIDE_SWITCH_LEFT_LINK'],
		'switchLeftTitle'    => $arParams['ASIDE_SWITCH_LEFT_TITLE'],
		'switchRightName'    => $arParams['ASIDE_SWITCH_RIGHT_NAME'],
		'switchRightLink'    => $arParams['ASIDE_SWITCH_RIGHT_LINK'],
		'switchRightTitle'   => $arParams['ASIDE_SWITCH_RIGHT_TITLE'],
		'templateFolder'     => $templateFolder,
		'LEFT'               => ($APPLICATION->GetCurPage() == '/youdo/left.php' ? 'Y' : 'N'),
		'AJAX'               => Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest(),
		'ASIDE_BG'           => $arParams['ASIDE_BG'],
		'ASIDE_LOGIN_BG'     => $arParams['ASIDE_LOGIN_BG']
	), false, true)?>);
</script>