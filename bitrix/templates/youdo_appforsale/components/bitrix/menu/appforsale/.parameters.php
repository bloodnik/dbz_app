<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arTemplateParameters['ASIDE_BG'] = array(
	'NAME' => Loc::getMessage('ASIDE_BG'),
	'TYPE' => 'STRING',
	'DEFAULT' => '#FFFFFF'
);

$arTemplateParameters['ASIDE_LOGIN_SHOW'] = array(
	'NAME' => Loc::getMessage('ASIDE_LOGIN_SHOW'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'Y',
	'REFRESH' => 'Y'
);

if ($arCurrentValues['ASIDE_LOGIN_SHOW'] == 'Y')
{
	$arTemplateParameters['ASIDE_LOGIN_BG'] = array(
		'NAME' => Loc::getMessage('ASIDE_LOGIN_BG'),
		'TYPE' => 'LIST',
		'VALUES' => array(
			'default' => Loc::getMessage('FROM_SITE_SETTING')
		),
		'ADDITIONAL_VALUES' => 'Y',
		'DEFAULT' => 'default'
	);
}
	
// SWITCH	
$arTemplateParameters['ASIDE_SWITCH_SHOW'] = array(
		'NAME' => Loc::getMessage('ASIDE_SWITCH_SHOW'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
		'REFRESH' => 'Y'
);

if ($arCurrentValues['ASIDE_SWITCH_SHOW'] == 'Y')
{
	$arTemplateParameters['ASIDE_SWITCH_LEFT_NAME'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_LEFT_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_LEFT_NAME_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_LEFT_LINK'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_LEFT_LINK'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_LEFT_LINK_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_LEFT_TITLE'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_LEFT_TITLE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_LEFT_TITLE_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_RIGHT_NAME'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_RIGHT_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_RIGHT_NAME_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_RIGHT_LINK'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_RIGHT_LINK'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_RIGHT_LINK_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_RIGHT_TITLE'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_RIGHT_TITLE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('ASIDE_SWITCH_RIGHT_TITLE_DEFAULT')
	);
	
	$arTemplateParameters['ASIDE_SWITCH_GROUPS'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_GROUPS'),
			'TYPE' => 'LIST'
	);
	
	$dbGroup = CGroup::GetList($by = "c_sort", $order = "asc", array('ADMIN' => 'N'));
	while($arGroup = $dbGroup->Fetch())
	{
		$arTemplateParameters['ASIDE_SWITCH_GROUPS']['VALUES'][$arGroup['ID']] = $arGroup['NAME'];
	}
	
	$arTemplateParameters['ASIDE_SWITCH_CLOSE_MENU'] = array(
			'NAME' => Loc::getMessage('ASIDE_SWITCH_CLOSE_MENU'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
	);
}
		

		
		
		
		
		

// 	'THEME' => array(
// 		'PARENT' => 'BASE',
// 		'NAME' => Loc::getMessage('THEME'),
// 		'TYPE' => 'LIST',
// 		'VALUES' => array(
// 			'LIGHT' => Loc::getMessage('LIGHT'),
// 			'DARK' => Loc::getMessage('DARK'),
// 			'SPECIAL' => Loc::getMessage('SPECIAL')
// 		),
// 		'REFRESH' => 'Y'
// 	)
// );

// if ($arCurrentValues['THEME'] == 'SPECIAL')
// {
// 	$arTemplateParameters['COLOR'] = array(
// 		'PARENT' => 'BASE',
// 		'NAME' => Loc::getMessage('COLOR'),
// 		'TYPE' => 'COLOR'
// 	);
// }
?>