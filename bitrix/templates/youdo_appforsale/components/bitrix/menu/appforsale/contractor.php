<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
	$params = $signer->unsign($request->get('signedParamsString'), 'menu');
	$params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
	die();
}

if ($USER->IsAuthorized())
{
	if ($request->get('contractor') == 'Y')
	{
		CUser::SetUserGroup(
			$USER->GetID(),
			array_merge(
				CUser::GetUserGroup($USER->GetID()),
				array($params['ASIDE_SWITCH_GROUPS'])
			)
		);
	}
	else
	{
		$arGroups = CUser::GetUserGroup($USER->GetID());
		foreach ($arGroups as $key=>$gid)
		{
			if($gid == $params['ASIDE_SWITCH_GROUPS'])
			{
				unset($arGroups[$key]);
				break;
			}
		}
		CUser::SetUserGroup(
			$USER->GetID(),
			$arGroups
		);
	}
	$USER->Authorize($USER->GetID());
}
?>