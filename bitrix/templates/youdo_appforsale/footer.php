<?
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Page\Asset;

?>
<?
if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php' && substr($APPLICATION->GetCurPage(), -8) != 'left.php' && $_REQUEST['APPFORSALE'] != 'Y') {
	echo '</div></div>';
}
?>

<? $APPLICATION->IncludeComponent("siril:app.bottom.sheet", "") ?>

<?
// Подключение VUE JS и компонентов
global $USER;
if ($USER->IsAdmin()) {
	Asset::getInstance()->addJs('//unpkg.com/vue@latest/dist/vue.js');
} else {
	Asset::getInstance()->addJs('https://unpkg.com/vue@2.6.12/dist/vue.min.js');
}
Asset::getInstance()->addJs('https://unpkg.com/axios/dist/axios.min.js');
Asset::getInstance()->addJs('//polyfill.io/v3/polyfill.min.js?features=es2015%2CIntersectionObserver');
Asset::getInstance()->addJs('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js');
Asset::getInstance()->addJs('https://unpkg.com/vue-bootstrap-typeahead');
Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js');
//Vue-dialog
Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vuejs-dialog@1.4.2/dist/vuejs-dialog.min.js');
Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vuejs-dialog@1.4.2/dist/vuejs-dialog-mixin.min.js');
// Vue Scroll to
Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vue-scrollto');

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/script.js');

//$APPLICATION->AddHeadScript('//unpkg.com/vue@latest/dist/vue.min.js');
//$APPLICATION->AddHeadScript('https://unpkg.com/axios/dist/axios.min.js');
//$APPLICATION->AddHeadScript('//polyfill.io/v3/polyfill.min.js?features=es2015%2CIntersectionObserver');
//$APPLICATION->AddHeadScript('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js');
//$APPLICATION->AddHeadScript('https://unpkg.com/vue-bootstrap-typeahead');
//$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js');
?>

<script>
    let options = {
        okText: 'Ок',
        cancelText: 'Отмена',
    };
    window.Vue.use(VuejsDialog.main.default, options)

    window.Vue.use(VueScrollTo);

</script>


</body>
</html>