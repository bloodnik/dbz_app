<?

use Mlab\Appforsale\MobileApp;
use Bitrix\Main\Page\Asset;

if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if ( ! CModule::IncludeModule('mlab.appforsale')) {
	die();
}
global $USER;

MobileApp::getInstance()->initializeBasicKernel();

CAppforsale::Init();
CJSCore::Init(array('fx', 'ajax'));

$mobileApp = \Mlab\Appforsale\MobileApp::getInstance();
$platform  = $mobileApp->getPlatform();


//PR($APPLICATION->GetCurPage() );
if ($USER->IsAuthorized()) {
	if ($APPLICATION->GetCurPage() != '/youdo/blockApp.php' && $APPLICATION->GetCurPage() != '/youdo/app_intro/np.php') {
		LocalRedirect("/youdo/blockApp.php");
	}
}

?>

<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico"/>
	<? $APPLICATION->ShowHead(); ?>
	<?
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/colors.css', true);
	Asset::getInstance()->addCss('/bitrix/css/main/bootstrap.css');
	Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/vendors/airdatepicker/css/datepicker.min.css', true);
	Asset::getInstance()->addCss('/js_vendors/bootstrap_4.min.css');
	Asset::getInstance()->addCss('/js_vendors/bootstrap-vue.min.css');
	Asset::getInstance()->addCss('https://unpkg.com/vue-bootstrap-typeahead/dist/VueBootstrapTypeahead.css');
	Asset::getInstance()->addCss('https://cdn.jsdelivr.net/npm/vuejs-dialog@1.4.1/dist/vuejs-dialog.min.css');
	Asset::getInstance()->addCss('https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic');
	?>
	<title><? $APPLICATION->ShowTitle() ?></title>


	<meta charset="<? echo SITE_CHARSET ?>"/>
	<?
	Asset::getInstance()->addJs('https://code.jquery.com/jquery-latest.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/vendors/airdatepicker/js/datepicker.min.js');
	?>

	<script type="text/javascript">

		function updatePanel() {

			var obShefPanel = BX('menu2');
			var pageWrap = BX('afs-page-wrap');
			if (!!obShefPanel && !!pageWrap) {
				if (BX.admin.panel.isFixed() === true) {
					obShefPanel.style.top = BX.admin.panel.DIV.offsetHeight + 'px';
					obShefPanel.style.position = 'fixed';
					pageWrap.style.marginTop = '56px';
				} else {
					if (BX.scrollTop(window) > BX.admin.panel.DIV.offsetHeight) {
						obShefPanel.style.top = '0px';
						obShefPanel.style.position = 'fixed';
						pageWrap.style.marginTop = '1px';
					} else {
						obShefPanel.style.top = '0px';
						obShefPanel.style.position = 'relative';
						if (BX.width(window) > 991)
							pageWrap.style.marginTop = '0px';
						else
							pageWrap.style.marginTop = '1px';
					}
				}
			}
		}

		BX.ready(function () {
			app.hideProgress();

			if (BX.admin && BX.admin.panel) {
				updatePanel();
				BX.addCustomEvent('onTopPanelCollapse', BX.delegate(updatePanel, this));
				BX.addCustomEvent('onTopPanelFix', BX.delegate(updatePanel, this));
				BX.bind(window, "scroll", BX.proxy(updatePanel, this));
				BX.bind(window, "resize", BX.proxy(updatePanel, this));
			}
		});

		function checkEvent(e) {
			return ((e = (e || window.event)) && (e.type == 'click' || e.type == 'mousedown' || e.type == 'mouseup') && (e.which > 1 || e.button > 1 || e.ctrlKey || e.shiftKey || BX.browser.IsMac() && e.metaKey)) || false;
		}

		document.onclick = function (e) {
			if (checkEvent(e))
				return true;

			var i = 8,
				target = e.target || e.srcElement,
				href,
				w = window;
			while (target && target != document.body && target.tagName != 'A' && i--) {
				target = target.parentNode;
			}
			if (!target || target.tagName != 'A' || target.onclick || target.onmousedown)
				return true;

			href = target.href;

			if (href && target.getAttribute('target')) {
				try {
					window.opener.location = href;
					return cancelEvent(e);
				} catch (er) {
					return true;
				}
			}
			if (href) {

				if (/javascript/i.test(target.href))
					return true;
				else {
					var title = '';
					if (target.hasAttribute('title'))
						title = target.getAttribute('title');

					if (window.location.pathname == '/youdo/left.php') {
						app.loadPageStart({
							url: target.href.replace(window.location.protocol + "//" + window.location.hostname, ''),
							title: title
						});

					} else {
						app.loadPageBlank({
							url: target.href.replace(window.location.protocol + "//" + window.location.hostname, ''),
							title: title
						});

					}


					return false;
				}
			}
		}
	</script>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (m, e, t, r, i, k, a) {
			m[i] = m[i] || function () {
				(m[i].a = m[i].a || []).push(arguments)
			};
			m[i].l = 1 * new Date();
			k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
		})
		(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		ym(64769164, "init", {
			clickmap: true,
			trackLinks: true,
			accurateTrackBounce: true,
			webvisor: true
		});
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/64769164" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<? if (preg_match('/(ipad|iphone|android|mobile|touch)/i', $_SERVER['HTTP_USER_AGENT'])): ?>
		<style>
			body {
				-webkit-tap-highlight-color: transparent;
				-webkit-user-select: none;
				-webkit-touch-callout: none;
			}
		</style>
	<? endif; ?>
</head>
<body onresize="onBodyResize()"<?=($APPLICATION->GetCurPage() == '/youdo/left.php' ? ' class="left"' : '')?>>
<? if ($_REQUEST['APPFORSALE'] != 'Y'): ?>
<div id="box_layer_bg"></div>
<div id="box_layer_wrap">
	<div id="box_layout"></div>
</div>
<script type="text/javascript">domStarted();</script>
<div id="panel"><? $APPLICATION->ShowPanel() ?></div>
<div id="afs-page-wrap"
     style="margin-top: <?=($APPLICATION->GetCurPage() != '/youdo/left.php' && $_COOKIE['APPLICATION'] != 'Y' ? '56' : '0')?>px">

	<?
	// 	if ($APPLICATION->GetCurPage() != '/youdo/left.php' && $_REQUEST['APPFORSALE'] != 'Y')
	// {
	?>

	<div id="menu2" class="afs-color"
	     style="display: <?=($APPLICATION->GetCurPage() != '/youdo/left.php' && $_COOKIE['APPLICATION'] == 'Y' ? 'none' : 'block')?>">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-12">
					<div class="afs-logo">
						<a href="/">
							<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/appforsale_logo.php"), false); ?>
						</a>
					</div>
				</div>
				<div class="col-md-7  d-none d-sm-block d-sm-none d-md-block">
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"appforsale",
						array(
							"ALLOW_MULTI_SELECT"    => "N",
							"CHILD_MENU_TYPE"       => "top",
							"COMPONENT_TEMPLATE"    => "appforsale",
							"DELAY"                 => "Y",
							"MAX_LEVEL"             => "1",
							"MENU_CACHE_GET_VARS"   => array(),
							"MENU_CACHE_TIME"       => "3600",
							"MENU_CACHE_TYPE"       => "A",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME"            => "site",
							"ROOT_MENU_TYPE"        => "top",
							"USE_EXT"               => "N",
							"ASIDE_LOGIN_SHOW"      => "Y",
							"ASIDE_LOGIN_BG"        => "default",
							"ASIDE_BG"              => "#FFFFFF"
						),
						false
					);
					?>
				</div>
				<div class="col-md-3 d-none d-sm-block d-sm-none d-md-block">
					<?
					$APPLICATION->IncludeComponent(
						'mlab:appforsale.personal.line',
						'',
						array(),
						false
					);
					?>
				</div>
			</div>
		</div>
	</div>
	<? // }?>


	<? endif; ?>

	<?
	$containerClass = "container";
	$pos            = strpos($APPLICATION->GetCurPage(), '/youdo/reports');
	if ($pos !== false) {
		$containerClass = "container-fluid";
	}

	if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php' && $APPLICATION->GetCurPage() != '/youdo/left.php' && $_REQUEST['APPFORSALE'] != 'Y') {
		echo '<div class="' . $containerClass . '" style="margin-bottom: 80px; padding-bottom: 50px"><div class="">';
	}
	?>
