<?
use Bitrix\Main\UserTable; 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
if (isset($_POST['submit']))
{
	COption::SetOptionString($mid, "condition", $_POST['condition']);
}




$saleModulePermissions = $APPLICATION->GetGroupRight('appforsale');
$readOnly = ($saleModulePermissions < 'W');
if ($saleModulePermissions < 'R')
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


$adminListTableID = 'tbl_push_cond';

$adminSort = new CAdminSorting($adminListTableID, 'ID', 'ASC');
$adminList = new CAdminList($adminListTableID, $adminSort);

//$filter = array();
//$filterFields = array(
////	'filter_active'
//);

//$adminList->InitFilter($filterFields);

//if (!empty($_REQUEST['filter_active']))
//{
//	$_REQUEST['filter_active'] = (string)$_REQUEST['filter_active'];
//	if ($_REQUEST['filter_active'] == 'Y' || $_REQUEST['filter_active'] == 'N')
//		$filter['ACTIVE'] = $_REQUEST['filter_active'];
//}

if (!$readOnly && $adminList->EditAction())
{
	if (isset($FIELDS) && is_array($FIELDS))
	{
		$conn = Application::getConnection();
		foreach ($FIELDS as $ID => $fields)
		{
			$ID = (int)$ID;
			if ($ID <= 0 || !$adminList->IsUpdated($ID))
				continue;

			$conn->startTransaction();
			if ($DB->Update("appforsale_push", $fields, "WHERE ID = " . $ID))
			{
				$conn->commitTransaction();
			}
			else
			{
				$conn->rollbackTransaction();
				$adminList->AddUpdateError(implode('<br>', $result->getErrorMessages()), $ID);
			}
		}
		unset($fields, $ID);
	}
}

if (!$readOnly && ($listID = $adminList->GroupAction()))
{
	if ($_REQUEST['action_target'] == 'selected')
	{
		$listID = array();
		//		$discountIterator = Internals\DiscountTable::getList(array(
		//			'select' => array('ID'),
		//			'filter' => $filter
		//		));
		//		while ($discount = $discountIterator->fetch())
		//			$listID[] = $discount['ID'];
	}

	$listID = array_filter($listID);
	if (!empty($listID))
	{
		switch ($_REQUEST['action'])
		{
			case 'activate':
			case 'deactivate':
				$fields = array(
					'ACTIVE' => ($_REQUEST['action'] == 'activate' ? '"Y"' : '"N"')
				);
				foreach ($listID as &$condID)
				{


					if (!$DB->Update("appforsale_push", $fields, "WHERE ID = " . $condID))
					{
						$adminList->AddGroupError(implode('<br>', $result->getErrorMessages(), $condID));
					}
				}
				unset($condID, $fields);
				break;
			case 'delete':
				foreach ($listID as &$condID)
				{
					$result = $DB->Query("DELETE FROM appforsale_push WHERE ID = " . $condID);
					//$result = Internals\DiscountTable::delete($discountID);
					//if (!$result->isSuccess())
					//{
					//	$adminList->AddGroupError(implode('<br>', $result->getErrorMessages(), $condID));
					//}
				}
				unset($condID);
				break;
		}
	}
	unset($listID);
}

$headerList = array();
$headerList['ID'] = array(
	'id' => 'ID',
	'content' => 'ID',
	'sort' => 'ID',
	'default' => true
);
$headerList['ACTIVE'] = array(
	'id' => 'ACTIVE',
	'content' => GetMessage('FIELD_ACTIVE'),
	'sort' => 'ACTIVE',
	'default' => true
);
$headerList['NAME'] = array(
	'id' => 'NAME',
	'content' => GetMessage('FIELD_NAME'),
	'sort' => 'NAME',
	'default' => true
);

$headerList['SORT'] = array(
	'id' => 'SORT',
	'content' => GetMessage('FIELD_SORT'),
	'sort' => 'SORT',
	'default' => true
);
$headerList['TIMESTAMP_X'] = array(
	'id' => 'TIMESTAMP_X',
	'content' => GetMessage('FIELD_TIMESTAMP_X'),
	'sort' => 'TIMESTAMP_X',
	'default' => false
);

$headerList['MODIFIED_BY'] = array(
	'id' => 'MODIFIED_BY',
	'content' => GetMessage('FIELD_MODIFIED_BY'),
	'sort' => 'MODIFIED_BY',
	'default' => false
);
$headerList['DATE_CREATE'] = array(
	'id' => 'DATE_CREATE',
	'content' => GetMessage('FIELD_DATE_CREATE'),
	'sort' => 'DATE_CREATE',
	'default' => false
);

$headerList['CREATED_BY'] = array(
	'id' => 'CREATED_BY',
	'content' => GetMessage('FIELD_CREATED_BY'),
	'sort' => 'CREATED_BY',
	'default' => false
);
$headerList['PRIORITY'] = array(
	'id' => 'PRIORITY',
	'content' => GetMessage('FIELD_PRIORITY'),
	'sort' => 'PRIORITY',
	'default' => true
);
$headerList['LAST_PUSH'] = array(
	'id' => 'LAST_PUSH',
	'content' => GetMessage('FIELD_LAST_PUSH'),
	'sort' => 'LAST_PUSH',
	'default' => true
);


if (!isset($by))
	$by = 'ID';
if (!isset($order))
	$order = 'ASC';


$adminList->AddHeaders($headerList);

$rsData = $DB->Query("SELECT * FROM appforsale_push ORDER BY " . $by . " " . $order);
$rsData = new CAdminResult($rsData, $adminListTableID);
$rsData->NavStart();
$adminList->NavText($rsData->GetNavPrint(GetMessage('RULES')));

//$lAdmin->BeginPrologContent();
//$lAdmin->EndPrologContent();

$userList = array();
$arUserID = array();
$nameFormat = CSite::GetNameFormat(true);

$arRows = array();
while($cond = $rsData->GetNext())
{
	$cond['ID'] = (int) $cond['ID'];
	//if ($selectFieldsMap['CREATED_BY'])
	//{
		$cond['CREATED_BY'] = (int)$cond['CREATED_BY'];
		if ($cond['CREATED_BY'] > 0)
			$arUserID[$cond['CREATED_BY']] = true;
	//}
	//if ($selectFieldsMap['MODIFIED_BY'])
	//{
		$cond['MODIFIED_BY'] = (int)$cond['MODIFIED_BY'];
		if ($cond['MODIFIED_BY'] > 0)
			$arUserID[$cond['MODIFIED_BY']] = true;
	//}

	$urlEdit = 'appforsale_push_edit.php?ID='.$cond['ID'].'&lang='.LANGUAGE_ID.GetFilterParams('filter_');
	$arRows[$cond['ID']] = $row = &$adminList->AddRow(
		$cond['ID'],
		$cond,
		$urlEdit,
		"test"
	);
	$row->AddViewField('ID', '<a href="'.$urlEdit.'">'.$discount['ID'].'</a>');


	$row->AddViewField("ID", '<a href="'.$urlEdit.'">'.$cond["ID"].'</a>');
	$row->AddCheckField("ACTIVE", false);
	$row->AddViewField("NAME", $cond["NAME"]);
	$row->AddViewField("SORT", $cond["SORT"]);
	$row->AddViewField("TIMESTAMP_X", $cond["TIMESTAMP_X"]);
	$row->AddViewField("MODIFIED_BY", $cond["MODIFIED_BY"]);
	$row->AddViewField("DATE_CREATE", $cond["DATE_CREATE"]);
	$row->AddViewField("CREATED_BY", $cond["CREATED_BY"]);
	$row->AddViewField("PRIORITY", $cond["PRIORITY"]);
	$row->AddCheckField("LAST_PUSH", false);








	$arActions = array();
	$arActions[] = array(
		'ICON' => 'edit',
		'TEXT' => GetMessage('EDIT'),
		'ACTION' => $adminList->ActionRedirect($urlEdit),
		'DEFAULT' => true
	);
	$arActions[] = array(
		'ICON' => 'delete',
		'TEXT' => GetMessage('DELETE'),
		'ACTION' => "if(confirm('".GetMessage('BT_SALE_DISCOUNT_LIST_MESS_DELETE_DISCOUNT_CONFIRM')."')) ".$adminList->ActionDoGroup($cond['ID'], 'delete'),
		'DEFAULT' => false,
	);

	$row->AddActions($arActions);
}
if (isset($row))
	unset($row);

//if ($selectFieldsMap['CREATED_BY'] || $selectFieldsMap['MODIFIED_BY'])
//{
	if (!empty($arUserID))
	{
		$userIterator = UserTable::getList(array(
			'select' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL'),
			'filter' => array('ID' => array_keys($arUserID)),
		));
		while ($arOneUser = $userIterator->fetch())
		{
			$arOneUser['ID'] = (int)$arOneUser['ID'];
			$userList[$arOneUser['ID']] = '<a href="/bitrix/admin/user_edit.php?lang='.LANGUAGE_ID.'&ID='.$arOneUser['ID'].'">'.CUser::FormatName($nameFormat, $arOneUser).'</a>';
		}
		unset($arOneUser, $userIterator);
	}

	foreach ($arRows as &$row)
	{
		//if ($selectFieldsMap['CREATED_BY'])
		//{
			$strCreatedBy = '';
			if ($row->arRes['CREATED_BY'] > 0 && isset($userList[$row->arRes['CREATED_BY']]))
			{
				$strCreatedBy = $userList[$row->arRes['CREATED_BY']];
			}
			$row->AddViewField("CREATED_BY", $strCreatedBy);
			//}
			//if ($selectFieldsMap['MODIFIED_BY'])
			//{
			$strModifiedBy = '';
			if ($row->arRes['MODIFIED_BY'] > 0 && isset($userList[$row->arRes['MODIFIED_BY']]))
			{
				$strModifiedBy = $userList[$row->arRes['MODIFIED_BY']];
			}
			$row->AddViewField("MODIFIED_BY", $strModifiedBy);
			//}
	}
	if (isset($row))
		unset($row);
//}

// $discountIterator->SelectedRowsCount()
$adminList->AddFooter(
	array(
		array(
			'title' => "��������",
			'value' => 0
		),
		array(
			'counter' => true,
			'title' => "�������", 
			'value' => "0"
		),
	)
);

$adminList->AddGroupActionTable(
	array(
		"delete" => GetMessage('DELETE'),
		"activate" => GetMessage('ACTIVATE'),
		"deactivate" => GetMessage('DEACTIVATE'),
	)
);

if (!$readOnly)
{
	$siteLID = '';
	$arSiteMenu = array();

	if (count($arSitesShop) == 1)
	{
		$siteLID = "&LID=".$arSitesShop[0]['ID'];
	}
	else
	{
		foreach ($arSitesShop as $val)
		{
			$arSiteMenu[] = array(
				"TEXT" => $val["NAME"]." (".$val['ID'].")",
				"ACTION" => "window.location = 'appforsale_push_edit.php?lang=".LANGUAGE_ID."&LID=".$val['ID']."';"
			);
		}
	}
	$aContext = array(
		array(
			"TEXT" => GetMessage('BTN_NEW'),
			"ICON" => "btn_new",
			"LINK" => "appforsale_push_edit.php?lang=".LANGUAGE_ID.$siteLID,
			"MENU" => $arSiteMenu
		),
	);

	$adminList->AddAdminContextMenu($aContext);
}

$adminList->CheckListMode();

$APPLICATION->SetTitle(GetMessage('RULES'));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$adminList->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>


