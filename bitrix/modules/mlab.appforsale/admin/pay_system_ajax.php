<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$instance = Application::getInstance();
$context = $instance->getContext();
$request = $context->getRequest();

$lang = ($request->get('lang') !== null) ? trim($request->get('lang')) : "ru";
\Bitrix\Main\Context::getCurrent()->setLanguage($lang);

Loc::loadMessages(__FILE__);

$arResult = array("ERROR" => "");

if (!\Bitrix\Main\Loader::includeModule('mlab.appforsale'))
	$arResult["ERROR"] = "Error! Can't include module \"AppForSale\"";

$appforsaleModulePermissions = $APPLICATION->GetGroupRight("appforsale");

if(strlen($arResult["ERROR"]) <= 0 && $appforsaleModulePermissions >= "W" && check_bitrix_sessid())
{
	$params = array(
		'waitResponse' => 10,
		'disableSslVerification' => true
	);
	$http = new \Bitrix\Main\Web\HttpClient($params);
	$response = @$http->get('https://'.$_SERVER['SERVER_NAME'].'/bitrix/tools/appforsale_ps_result.php');
	if ($response === false || $http->getStatus() != 200)
	{
		$arResult['CHECK_STATUS'] =  'ERROR';
		$arResult['CHECK_MESSAGE'] =  join('\n', $http->getError());
	}
	else
	{
		$arResult['CHECK_STATUS'] =  'OK';
		$arResult['CHECK_MESSAGE'] =  Loc::getMessage('APPFORSALE_PS_SSL_CHECK_MESSAGE');
	}
}
else
{
	if(strlen($arResult["ERROR"]) <= 0)
		$arResult["ERROR"] = "Error! Access denied";
}

if(strlen($arResult["ERROR"]) > 0)
	$arResult["RESULT"] = "ERROR";
else
	$arResult["RESULT"] = "OK";

if(strtolower(SITE_CHARSET) != 'utf-8')
	$arResult = $APPLICATION->ConvertCharsetArray($arResult, SITE_CHARSET, 'utf-8');

header('Content-Type: application/json');
die(json_encode($arResult));
?>