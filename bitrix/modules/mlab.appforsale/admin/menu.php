<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array(
	array(
		"parent_menu" => "global_menu_appforsale",
		"text" => Loc::getMessage('USERS'),
		"sort" => 6,
		"icon" => "statistic_icon_online",
		"items_id" => "menu_appforsale_users",
		"items" => array(				
			array(
				"text" => Loc::getMessage('APPFORSALE_ACCOUNT_ADMIN'),
				"url" => "appforsale_account_admin.php?lang=" . LANGUAGE_ID,
				"more_url"	=> array(
					"appforsale_account_edit.php"
				)
			),
			array(
				"text" => Loc::getMessage('TRANSACTIONS'),
				"url" => "appforsale_transact_admin.php?lang=" . LANGUAGE_ID,
				"more_url"	=> array(
					"appforsale_transaction_edit.php"
				)
			),
			array(
				"text" => Loc::getMessage('RECCURRING'),
				"url" => "appforsale_recurring_admin.php?lang=" . LANGUAGE_ID,
				"more_url"	=> array(
					"appforsale_recurring_edit.php"
				)
			),
			[
				"text" => Loc::getMessage("DEVICES"),
				"url" => "mlab_appforsale_push_device.php"
			]
		)
	),	
	array(
		"parent_menu" => "global_menu_appforsale",
		"text" => Loc::getMessage('SETTING'),
		"sort" => 7,
		"icon" => "sys_menu_icon",
		"items_id" => "menu_appforsale_setting",
		"items" => array(
			array(
				"text" => Loc::getMessage('NOTICE'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=push",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('YANDEX_MONEY'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=yandex_money",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('YANDEX_KASSA'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=yandex_kassa",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('LOGIN'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=login",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('MONETIZATION'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=monetization",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('OTHER'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=other",
				"more_url"	=> array()
			),
			array(
				"text" => Loc::getMessage('PUSH_TEXTS'),
				"url" => "settings.php?lang=" . LANGUAGE_ID. "&mid=mlab.appforsale&tabControl_active_tab=push_text",
				"more_url"	=> array()
			)
		)
	)
); 

if (Loader::includeModule('iblock'))
{
	$dbIBlock = CIBlock::GetList(array(), array('CODE' => array('task', 'profile', 'offer', 'comment', 'location')));
	while ($arIBlock = $dbIBlock->Fetch())
	{
		switch ($arIBlock['CODE'])
		{
			case 'task':
				$arMenu = array(
					'parent_menu' => 'global_menu_appforsale',
					'text' => $arIBlock['NAME'],
					'sort' => 1,
					'icon' => 'iblock_menu_icon_iblocks',
					'items_id' => 'menu_appforsale_task',
					'items' => array(
						array(
							'text' => Loc::getMessage('LIST_TASKS'),
							'url' => '/bitrix/admin/afs_element_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_el_y=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('SECTION_TASKS'),
							'url' => '/bitrix/admin/afs_list_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_section_section=0&SECTION_ID=0&apply_filter=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('PROPERTY_TASKS'),
							'url' => '/bitrix/admin/iblock_property_admin.php?lang='.LANGUAGE_ID.'&IBLOCK_ID='.$arIBlock['ID'].'&admin=N',
							'more_url'	=> array()
						)
					)
				);
				
				$aMenu[] = $arMenu;
				break;
			case 'profile':
				$arMenu = array(
					'parent_menu' => 'global_menu_appforsale',
					'text' => $arIBlock['NAME'],
					'sort' => 2,
					'icon' => 'iblock_menu_icon_iblocks',
					'items_id' => 'menu_appforsale_profile',
					'items' => array(
						array(
							'text' => Loc::getMessage('LIST_PROFILES'),
							'url' => '/bitrix/admin/afs_element_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_el_y=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('SECTION_PROFILES'),
							'url' => '/bitrix/admin/afs_list_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_section_section=0&SECTION_ID=0&apply_filter=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('PROPERTY_PROFILES'),
							'url' => '/bitrix/admin/iblock_property_admin.php?lang='.LANGUAGE_ID.'&IBLOCK_ID='.$arIBlock['ID'].'&admin=N',
							'more_url'	=> array()
						)
					)
				);
				
				$aMenu[] = $arMenu;
				break;
			case 'offer':
				$arMenu = array(
					'parent_menu' => 'global_menu_appforsale',
					'text' => $arIBlock['NAME'],
					'sort' => 3,
					'icon' => 'iblock_menu_icon_iblocks',
					'items_id' => 'menu_appforsale_offer',
					'items' => array(
						array(
							'text' => Loc::getMessage('LIST_OFFER'),
							'url' => '/bitrix/admin/afs_element_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_el_y=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('PROPERTY_OFFER'),
							'url' => '/bitrix/admin/iblock_property_admin.php?lang='.LANGUAGE_ID.'&IBLOCK_ID='.$arIBlock['ID'].'&admin=N',
							'more_url'	=> array()
						)
					)
				);
				
				$aMenu[] = $arMenu;
				break;
			case 'comment':
				$arMenu = array(
					'parent_menu' => 'global_menu_appforsale',
					'text' => $arIBlock['NAME'],
					'sort' => 4,
					'icon' => 'iblock_menu_icon_iblocks',
					'items_id' => 'menu_appforsale_comment',
					'items' => array(
						array(
							'text' => Loc::getMessage('LIST_COMMENT'),
							'url' => '/bitrix/admin/afs_element_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_el_y=Y',
							'more_url'	=> array()
						),
						array(
							'text' => Loc::getMessage('PROPERTY_COMMENT'),
							'url' => '/bitrix/admin/iblock_property_admin.php?lang='.LANGUAGE_ID.'&IBLOCK_ID='.$arIBlock['ID'].'&admin=N',
							'more_url'	=> array()
						)
					)
				);
				
				$aMenu[] = $arMenu;
				break;
			case 'location':
				$arMenu = array(
					'parent_menu' => 'global_menu_appforsale',
					'text' => $arIBlock['NAME'],
					'sort' => 5,
					'icon' => 'iblock_menu_icon_iblocks',
					'items_id' => 'menu_appforsale_location',
					'items' => array(
						array(
							'text' => Loc::getMessage('LIST_LOCATION'),
							'url' => '/bitrix/admin/afs_element_admin.php?IBLOCK_ID='.$arIBlock['ID'].'&type='.$arIBlock['IBLOCK_TYPE_ID'].'&lang='.LANGUAGE_ID.'&find_el_y=Y',
							'more_url'	=> array()
						)
					)
				);
			
				$aMenu[] = $arMenu;
				break;
		}
	}
}


return (!empty($aMenu) ? $aMenu : false);
?>