<?
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Entity\Query;
use Mlab\Appforsale\Push\Entity\DeviceTable;
use Bitrix\Main\Localization\Loc;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadMessages(__FILE__);

$tableId = "mlab_appforsale_push_device";

$sort = new CAdminSorting($tableId, "TIMESTAMP_X", "desc");
$list = new CAdminUiList($tableId, $sort);

$arHeaders = [
    [
        "id" => "DATE_CREATE",
        "content" => Loc::getMessage("FIELD_DATE_CREATE"),
        "sort" => "DATE_CREATE",
        "default" => true
    ],
    [
        "id" => "TIMESTAMP_X",
        "content" => Loc::getMessage("FIELD_TIMESTAMP_X"),
        "sort" => "TIMESTAMP_X"
    ],
    [
        "id" => "DEVICE_ID",
        "content" => Loc::getMessage("FIELD_DEVICE_ID")
    ],
    [
        "id" => "DEVICE_MODEL",
        "content" => Loc::getMessage("FIELD_DEVICE_MODEL"),
        "default" => true
    ],
    [
        "id" => "SYSTEM_VERSION",
        "content" => Loc::getMessage("FIELD_SYSTEM_VERSION"),
        "default" => true
    ],
    [
        "id" => "SETTINGS",
        "content" => Loc::getMessage("FIELD_SETTINGS")
    ],
    [
        "id" => "TOKEN",
        "content" => Loc::getMessage("FIELD_TOKEN")
    ],
    [
        "id" => "USER_ID",
        "content" => Loc::getMessage("FIELD_USER_ID"),
        "default" => true
    ],
    [
        "id" => "DATE_AUTH",
        "content" => Loc::getMessage("FIELD_DATE_AUTH"),
        "sort" => "DATE_AUTH",
        "default" => true
    ]
];
$USER_FIELD_MANAGER->adminListAddHeaders("BM_PUSH_DEVICE", $arHeaders);
$list->addHeaders($arHeaders);

$nav = new PageNavigation("pages-device-admin");
$nav->setPageSize($list->getNavSize());
$nav->initFromUri();

$query = new Query(DeviceTable::getEntity());
$select = $list->getVisibleHeaderColumns();

if (in_array("USER_ID", $select))
{
    $select[] = "USER.SHORT_NAME";
}

// if (!in_array("ID", $select))
//     $select[] = "ID";

$query->setSelect($select);
$sortBy = strtoupper($by);
if(!DeviceTable::getEntity()->hasField($sortBy))
{
    $sortBy = "TIMESTAMP_X";
}
$sortOrder = strtoupper($order);
if($sortOrder <> "DESC" && $sortOrder <> "ASC")
{
    $sortOrder = "DESC";
}
$query->setOrder([$sortBy => $sortOrder]);
$query->countTotal(true);
$query->setOffset($nav->getOffset());
if ($_REQUEST["mode"] !== "excel")
    $query->setLimit($nav->getLimit());

    
$result = $query->exec();
$nav->setRecordCount($result->getCount());
 
$list->setNavigation($nav, "Устройств", false);

while ($data = $result->fetch())
{    
    $row = &$list->addRow($data["ID"], $data);
    $row->addField("DEVICE_MODEL", DeviceTable::formatName($data["DEVICE_MODEL"]));
    $row->addField("USER_ID", $data["USER_ID"] > 0 ? "<a href=\"/bitrix/admin/user_edit.php?lang=".LANGUAGE_ID."&ID=".$data["USER_ID"]."\">".$data["MLAB_APPFORSALE_PUSH_ENTITY_DEVICE_USER_SHORT_NAME"]."</a>" : "&nbsp;");
}
    
$list->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");

















// use Bitrix\Main\UserTable;
// require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
// IncludeModuleLangFile(__FILE__);
// if (isset($_POST['submit']))
// {
// 	COption::SetOptionString($mid, "condition", $_POST['condition']);
// }




// $saleModulePermissions = $APPLICATION->GetGroupRight('appforsale');
// $readOnly = ($saleModulePermissions < 'W');
// if ($saleModulePermissions < 'R')
// 	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


// $adminListTableID = 'tbl_push';

// $adminSort = new CAdminSorting($adminListTableID, 'ID', 'ASC');
// $adminList = new CAdminList($adminListTableID, $adminSort);

// //$filter = array();
// //$filterFields = array(
// //	'filter_active'
// //);

// // $adminList->InitFilter($filterFields);




// $headerList = array();
// $headerList['ID'] = array(
// 	'id' => 'ID',
// 	'content' => 'ID',
// 	'sort' => 'ID',
// 	'default' => true
// );
// $headerList['DATE_CREATE'] = array(
// 	'id' => 'DATE_CREATE',
// 	'content' => GetMessage('FIELD_DATE_CREATE'),
// 	'sort' => 'DATE_CREATE',
// 	'default' => true
// );
// $headerList['DEVICE_ID'] = array(
// 	'id' => 'DEVICE_ID',
// 	'content' => GetMessage('FIELD_DEVICE_ID'),
// 	'sort' => 'DEVICE_ID',
// 	'default' => false
// );
// // $headerList['DEVICE_TYPE'] = array(
// // 	'id' => 'DEVICE_TYPE',
// // 	'content' => GetMessage('FIELD_DEVICE_TYPE'),
// // 	'sort' => 'DEVICE_TYPE',
// // 	'default' => false
// // );
// // $headerList['DEVICE_NAME'] = array(
// // 	'id' => 'DEVICE_NAME',
// // 	'content' => GetMessage('FIELD_DEVICE_NAME'),
// // 	'sort' => 'DEVICE_NAME',
// // 	'default' => false
// // );
// $headerList['DEVICE_MODEL'] = array(
// 	'id' => 'DEVICE_MODEL',
// 	'content' => GetMessage('FIELD_DEVICE_MODEL'),
// 	'sort' => 'DEVICE_MODEL',
// 	'default' => true
// );
// $headerList['SYSTEM_VERSION'] = array(
// 	'id' => 'SYSTEM_VERSION',
// 	'content' => GetMessage('FIELD_SYSTEM_VERSION'),
// 	'sort' => 'SYSTEM_VERSION',
// 	'default' => true
// );
// $headerList['SETTINGS'] = array(
// 	'id' => 'SETTINGS',
// 	'content' => GetMessage('FIELD_SETTINGS'),
// 	'sort' => 'SETTINGS',
// 	'default' => false
// );
// $headerList['TOKEN'] = array(
// 	'id' => 'TOKEN',
// 	'content' => GetMessage('FIELD_TOKEN'),
// 	'sort' => 'TOKEN',
// 	'default' => false
// );
// $headerList['USER_ID'] = array(
// 	'id' => 'USER_ID',
// 	'content' => GetMessage('FIELD_USER_ID'),
// 	'sort' => 'USER_ID',
// 	'default' => true
// );
// $headerList['DATE_AUTH'] = array(
// 	'id' => 'DATE_AUTH',
// 	'content' => GetMessage('FIELD_DATE_AUTH'),
// 	'sort' => 'DATE_AUTH',
// 	'default' => true
// );



// if (!isset($by))
// 	$by = 'ID';
// if (!isset($order))
// 	$order = 'ASC';

// $adminList->AddHeaders($headerList);

// $userList = array();
// $arUserID = array();
// $nameFormat = CSite::GetNameFormat(true);

// $rsData = $DB->Query("SELECT ID, DATE_FORMAT(DATE_CREATE, '%d.%m.%Y %H:%i:%s') AS DATE_CREATE, DEVICE_ID, DEVICE_MODEL, SYSTEM_VERSION, TOKEN, USER_ID, DATE_FORMAT(DATE_AUTH, '%d.%m.%Y %H:%i:%s') AS DATE_AUTH, SETTINGS FROM mlab_appforsale_push_device ORDER BY " . $by . " " . $order);
// $rsData = new CAdminResult($rsData, $adminListTableID);
// $rsData->NavStart();
// $adminList->NavText($rsData->GetNavPrint(GetMessage('DEVICES')));

// //$lAdmin->BeginPrologContent();
// //$lAdmin->EndPrologContent();


// while($device = $rsData->GetNext())
// {
// 	$device['ID'] = (int) $device['ID'];

// 	//if ($selectFieldsMap['CREATED_BY'])
// 	//{
// 		$device['USER_ID'] = (int)$device['USER_ID'];
// 		if ($device['USER_ID'] > 0)
// 			$arUserID[$device['USER_ID']] = true;
// 	//}

// 	//	$urlEdit = 'appforsale_push_cond_edit.php?ID='.$cond['ID'].'&lang='.LANGUAGE_ID.GetFilterParams('filter_');

// 	$arRows[$device['ID']] = $row = &$adminList->AddRow(
// 		$device['ID'],
// 		$device);




// 	$row->AddViewField("ID", $device["ID"]);
// 	$row->AddViewField("DATE_CREATE", $device["DATE_CREATE"]);
// 	$row->AddViewField("DEVICE_ID", $device["DEVICE_ID"]);
// 	//$row->AddViewField("DEVICE_TYPE", $device["DEVICE_TYPE"]);
// 	//$row->AddViewField("DEVICE_NAME", $device["DEVICE_NAME"]);
// 	$row->AddViewField("DEVICE_MODEL", $device["DEVICE_MODEL"]);
// 	$row->AddViewField("SYSTEM_VERSION", $device["SYSTEM_VERSION"]);
// 	$row->AddViewField("SETTINGS", $device["SETTINGS"]);
// 	$row->AddViewField("TOKEN", $device["TOKEN"]);
// 	$row->AddViewField("USER_ID", $device["USER_ID"] > 0 ? $device["USER_ID"] : "");
// 	$row->AddViewField("DATE_AUTH", $device["DATE_AUTH"]);



// }
// if (isset($row))
// 	unset($row);

// //if ($selectFieldsMap['CREATED_BY'] || $selectFieldsMap['MODIFIED_BY'])
// //{
// 	if (!empty($arUserID))
// 	{
// 		$userIterator = UserTable::getList(array(
// 			'select' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL'),
// 			'filter' => array('ID' => array_keys($arUserID)),
// 		));
// 		while ($arOneUser = $userIterator->fetch())
// 		{
// 			$arOneUser['ID'] = (int)$arOneUser['ID'];
// 			$userList[$arOneUser['ID']] = '<a href="/bitrix/admin/user_edit.php?lang='.LANGUAGE_ID.'&ID='.$arOneUser['ID'].'">'.CUser::FormatName($nameFormat, $arOneUser).'</a>';
// 		}
// 		unset($arOneUser, $userIterator);
// 	}

// 	foreach ($arRows as &$row)
// 	{
// 		//if ($selectFieldsMap['CREATED_BY'])
// 		//{
// 			$strCreatedBy = '';
// 			if ($row->arRes['USER_ID'] > 0 && isset($userList[$row->arRes['USER_ID']]))
// 			{
// 				$strCreatedBy = $userList[$row->arRes['USER_ID']];
// 			}
// 			$row->AddViewField("USER_ID", $strCreatedBy);
// 			//}

// 	}
// 	if (isset($row))
// 		unset($row);
// //}

// // $discountIterator->SelectedRowsCount()
// //$adminList->AddFooter(
// //	array(
// //		array(
// //			'title' => "��������",
// //			'value' => 0
// //		),
// //		array(
// //			'counter' => true,
// //			'title' => "�������", 
// //			'value' => "0"
// //		),
// //	)
// //);


// if (!$readOnly)
// {
// 	$siteLID = '';
// 	$arSiteMenu = array();

// 	if (count($arSitesShop) == 1)
// 	{
// 		$siteLID = "&LID=".$arSitesShop[0]['ID'];
// 	}
// 	else
// 	{
// 		foreach ($arSitesShop as $val)
// 		{
// 			$arSiteMenu[] = array(
// 				"TEXT" => $val["NAME"]." (".$val['ID'].")",
// 				"ACTION" => "window.location = 'appforsale_push_cond_edit.php?lang=".LANGUAGE_ID."&LID=".$val['ID']."';"
// 			);
// 		}
// 	}


// 	$adminList->AddAdminContextMenu(array());
// }

// $adminList->CheckListMode();

// $APPLICATION->SetTitle(GetMessage('DEVICES'));
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// $adminList->DisplayList();

// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>