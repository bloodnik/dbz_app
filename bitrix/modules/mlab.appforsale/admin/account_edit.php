<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$appforsaleModulePermissions = $APPLICATION->GetGroupRight("mlab.appforsale");
if ($appforsaleModulePermissions == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlab.appforsale/include.php");

IncludeModuleLangFile(__FILE__);

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;

$ID = IntVal($ID);

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && $appforsaleModulePermissions>="U" && check_bitrix_sessid())
{
	if ($ID <= 0)
	{
		if ($appforsaleModulePermissions < "W")
			$errorMessage .= GetMessage("SAE_NO_PERMS2ADD").".<br>";

		$USER_ID = IntVal($USER_ID);
		if ($USER_ID <= 0)
			$errorMessage .= GetMessage("SAE_EMPTY_USER").".<br>";

// 		$CURRENCY = Trim($CURRENCY);
// 		if (strlen($CURRENCY) <= 0)
// 			$errorMessage .= GetMessage("SAE_EMPTY_CURRENCY").".<br>";

		if (strlen($errorMessage) <= 0)
		{
			$arFilter = array(
					"USER_ID" => $USER_ID
				);

			$num = CAppforsaleUserAccount::GetList(
					array(),
					$arFilter,
					array()
				);
			
			if (IntVal($num) > 0)
				$errorMessage .= str_replace("#USER#", $USER_ID, GetMessage("SAE_ALREADY_EXISTS")).".<br>";
		}

		if (strlen($errorMessage) <= 0)
		{
			$OLD_BUDGET = 0.0;
		}
	}
	else
	{
		//if($_POST["UNLOCK"] == "Y")
		//	CSaleUserAccount::UnLock($USER_ID, $CURRENCY);		
		
		if (!($arOldUserAccount = CAppforsaleUserAccount::GetByID($ID)))
			$errorMessage .= str_replace("#ID#", $ID, GetMessage("SAE_NO_ACCOUNT")).".<br>";

		if (strlen($errorMessage) <= 0)
		{
			$USER_ID = $arOldUserAccount["USER_ID"];
			$OLD_BUDGET = DoubleVal($arOldUserAccount["CURRENT_BUDGET"]);
		}
	}
	
	$currentLocked = "";
	if (strlen($errorMessage) <= 0)
	{
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array("USER_ID" => $USER_ID)
		);
		$arUserAccount = $dbUserAccount->Fetch();
		$currentLocked = $arUserAccount["LOCKED"];
		
		
		$CURRENT_BUDGET = str_replace(",", ".", $CURRENT_BUDGET);
		$CURRENT_BUDGET = DoubleVal($CURRENT_BUDGET);

		$updateSum = $CURRENT_BUDGET - $OLD_BUDGET;

		if ($updateSum > 0)
		{
			if (!CAppforsaleUserAccount::UpdateAccount($USER_ID, $updateSum, "MANUAL", 0, $CHANGE_REASON))
			{
				if ($ex = $APPLICATION->GetException())
					$errorMessage .= $ex->GetString().".<br>";
				else
					$errorMessage .= GetMessage("SAE_ERROR_SAVING").".<br>";
			}
		}
	}

	if (strlen($errorMessage) <= 0 AND $currentLocked != "")
	{
		if($_POST["UNLOCK"] == "Y")
			CAppforsaleUserAccount::UnLock($USER_ID);
		
		if($_POST["UNLOCK"] == "N" OR ($currentLocked == "Y" AND !isset($_POST["UNLOCK"]))) 
			CAppforsaleUserAccount::Lock($USER_ID); 
	}
	
	if (strlen($errorMessage) <= 0)
	{
		$arUserAccount = CAppforsaleUserAccount::GetByUserID($USER_ID);
		if (DoubleVal($arUserAccount["CURRENT_BUDGET"]) != $CURRENT_BUDGET)
			$errorMessage .= GetMessage("SAE_ERROR_SAVING_SUM").".<br>";
	}

	if (strlen($errorMessage) <= 0)
	{
		$ID = IntVal($arUserAccount["ID"]);

		$arFields = array(
				"NOTES" => ((strlen($NOTES) > 0) ? $NOTES : False)
			);
		if (!CAppforsaleUserAccount::Update($ID, $arFields))
		{
			if ($ex = $APPLICATION->GetException())
				$errorMessage .= $ex->GetString().".<br>";
			else
				$errorMessage .= GetMessage("SAE_ERROR_SAVING_COMMENT").".<br>";
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
			LocalRedirect("/bitrix/admin/appforsale_account_admin.php?lang=".LANG.GetFilterParams("filter_", false));
	}
	else
	{
		$bVarsFromForm = true;
	}
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlab.appforsale/prolog.php");

if ($ID > 0)
	$APPLICATION->SetTitle(GetMessage("SAE_UPDATING"));
else
	$APPLICATION->SetTitle(GetMessage("SAE_ADDING"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");


$dbAccount = CAppforsaleUserAccount::GetList(
		array(),
		array("ID" => $ID),
		false,
		false,
		array("ID", "USER_ID", "CURRENT_BUDGET", "LOCKED", "NOTES", "TIMESTAMP_X", "DATE_LOCKED", "USER_LOGIN", "USER_NAME", "USER_LAST_NAME")
	);
if (!$dbAccount->ExtractFields("str_"))
{
	if ($appforsaleModulePermissions < "W")
		$errorMessage .= GetMessage("SAE_NO_PERMS2ADD").".<br>";
	$ID = 0;
}

if ($bVarsFromForm)
	$DB->InitTableVarsForEdit("appforsale_user_account", "", "str_");

?>

<?
$aMenu = array(
		array(
				"TEXT" => GetMessage("SAEN_2FLIST"),
				"LINK" => "/bitrix/admin/appforsale_account_admin.php?lang=".LANG.GetFilterParams("filter_"),
				"ICON"	=> "btn_list",
				"TITLE" => GetMessage("SAEN_2FLIST_TITLE"),
			)
	);

if ($ID > 0 && $appforsaleModulePermissions >= "U")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("SAEN_NEW_ACCOUNT"),
			"LINK" => "/bitrix/admin/appforsale_account_edit.php?lang=".LANG.GetFilterParams("filter_"),
			"ICON"	=> "btn_new",
			"TITLE" => GetMessage("SAEN_NEW_ACCOUNT_TITLE"),
		);

	if ($appforsaleModulePermissions >= "W")
	{
		$aMenu[] = array(
				"TEXT" => GetMessage("SAEN_DELETE_ACCOUNT"), 
				"LINK" => "javascript:if(confirm('".GetMessage("SAEN_DELETE_ACCOUNT_CONFIRM")."')) window.location='/bitrix/admin/appforsale_account_admin.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."#tb';",
				"WARNING" => "Y",
				"ICON"	=> "btn_delete"
			);
	}
}
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<?if(strlen($errorMessage)>0)
	echo CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=>GetMessage("SAE_ERROR"), "HTML"=>true));?>


<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?" name="form1">
<?echo GetFilterHiddens("filter_");?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="lang" value="<?echo LANG ?>">
<input type="hidden" name="ID" value="<?echo $ID ?>">
<?=bitrix_sessid_post()?>

<?
$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("SAEN_TAB_ACCOUNT"), "ICON" => "sale", "TITLE" => GetMessage("SAEN_TAB_ACCOUNT_DESCR"))
	);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();
?>

	<?if ($ID > 0):?>
		<tr>
			<td width="40%">ID:</td>
			<td width="60%"><?=$ID?></td>
		</tr>
		<tr>
			<td><?echo GetMessage("SAE_TIMESTAMP")?></td>
			<td><?=$str_TIMESTAMP_X?></td>
		</tr>
	<?endif;?>
	<tr class="adm-detail-required-field">
		<td width="40%"><?echo GetMessage("SAE_USER1")?></td>
		<td width="60%">
			<?if ($ID > 0):?>
				<input type="hidden" name="USER_ID" value="<?=$str_USER_ID?>">
				[<a title="<?echo GetMessage("SAE_USER_PROFILE")?>" href="/bitrix/admin/user_edit.php?lang=<?=LANGUAGE_ID?>&ID=<?=$str_USER_ID?>"><?=$str_USER_ID?></a>] (<?=$str_USER_LOGIN?>) <?=$str_USER_NAME?> <?=$str_USER_LAST_NAME?>
			<?else:?>
			<?echo FindUserID("USER_ID", $str_USER_ID);?>
			<?endif;?>
			</td>
	</tr>
	<tr class="adm-detail-required-field">
		<td><?echo GetMessage("SAE_SUM")?></td>
		<td>
			<input type="text" name="CURRENT_BUDGET" size="10" maxlength="20" value="<?= roundEx($str_CURRENT_BUDGET, SALE_VALUE_PRECISION) ?>">

		</td>
	</tr>
	
		
	<?if ($ID > 0 && $str_LOCKED=="Y"):?>
		<tr>
			<td><?echo GetMessage("SAE_UNLOCK")?></td>
			<td>
				<input type="checkbox" name="UNLOCK" value="Y"<?if ($str_LOCKED != "Y") echo " disabled"?>>
				<?
				if ($str_LOCKED=="Y")
					echo GetMessage("SAE_LOCKED").$str_DATE_LOCKED.")";
				?>
			</td>
		</tr>
	<?endif;?>
		
	<?if ($ID > 0 && $str_LOCKED=="N"):?>
		<tr>
			<td><?echo GetMessage("SAE_LOCK")?></td>
			<td>
				<input type="checkbox" name="UNLOCK" value="N"<?if ($str_LOCKED != "N") echo " disabled"?>>
			</td>
		</tr>
	<?endif;?>	
		
	<tr>
		<td valign="top"><?echo GetMessage("SAE_NOTES")?></td>
		<td valign="top">
			<textarea name="NOTES" rows="3" cols="40"><?= $str_NOTES ?></textarea>
		</td>
	</tr>
	<tr>
		<td valign="top"><?echo GetMessage("SAE_OSN")?><br><small><?echo GetMessage("SAE_OSN_NOTE")?></small></td>
		<td valign="top">
			<textarea name="CHANGE_REASON" rows="3" cols="40"><?= htmlspecialcharsEx($CHANGE_REASON) ?></textarea>
		</td>
	</tr>

<?
$tabControl->EndTab();
?>

<?
$tabControl->Buttons(
		array(
				"disabled" => ($appforsaleModulePermissions < "U"),
				"back_url" => "/bitrix/admin/sale_account_admin.php?lang=".LANG.GetFilterParams("filter_")
			)
	);
?>

<?
$tabControl->End();
?>

</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>