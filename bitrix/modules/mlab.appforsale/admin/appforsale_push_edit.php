<? 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$saleModulePermissions = $APPLICATION->GetGroupRight('appforsale');
$readOnly = ($saleModulePermissions < 'W');
if ($readOnly)
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if ($ex = $APPLICATION->GetException())
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	ShowError($ex->GetString());
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	die();
}

IncludeModuleLangFile(__FILE__);




$tabList = array(
	array('DIV' => 'edit1', 'ICON' => 'sale', 'TAB' => GetMessage('TAB1'), 'TITLE' => GetMessage('TAB1_TITLE')),
	array('DIV' => 'edit2', 'ICON' => 'sale', 'TAB' => GetMessage('TAB2'), 'TITLE' => GetMessage('TAB2_TITLE'))
);


$control = new CAdminForm("appforsale_push", $tabList);

$condID = 0;
$copy = false;
if (isset($_REQUEST['ID']))
{
	$condID = (int)$_REQUEST['ID'];
	if ($condID < 0)
		$condID = 0;
}
if ($condID > 0)
{
	$copy = (isset($_REQUEST['action']) && (string)$_REQUEST['action'] == 'copy');
}


if (
	check_bitrix_sessid()
	&& !$readOnly
	&& $_SERVER['REQUEST_METHOD'] == 'POST'
	&& isset($_POST['Update']) && (string)$_POST['Update'] == 'Y'
)
{

$arFields = array(
	//	"LID" => (array_key_exists('LID', $_POST) ? $_POST['LID'] : '\'s1\''),
		"NAME" => (array_key_exists('NAME', $_POST) ? "'".$_POST['NAME']."'" : ''),
	//"ACTIVE_FROM" => (array_key_exists('ACTIVE_FROM', $_POST) ? $_POST['ACTIVE_FROM'] : ''),
	//	"ACTIVE_TO" => (array_key_exists('ACTIVE_TO', $_POST) ? $_POST['ACTIVE_TO'] : ''),
		"ACTIVE" => (array_key_exists('ACTIVE', $_POST) && 'Y' == $_POST['ACTIVE'] ? '\'Y\'' : '\'N\''),
		"SORT" => (array_key_exists('SORT', $_POST) ? '"'.$_POST['SORT'].'"' : 500),
		"PRIORITY" => (array_key_exists('PRIORITY', $_POST) ? '"'.$_POST['PRIORITY'].'"' : ''),
		"LAST_PUSH" => (array_key_exists('LAST_PUSH', $_POST) && 'N' == $_POST['LAST_PUSH'] ? '\'N\'' : '\'Y\''),
	//	"XML_ID" => (array_key_exists('XML_ID', $_POST) ? $_POST['XML_ID'] : ''),
	//	'CONDITIONS' => $CONDITIONS,
	//	'ACTIONS' => $ACTIONS,
	//	'USER_GROUPS' => $arGroupID,
		"UNPACK" => (array_key_exists('UNPACK', $_POST) ? '"'.CUtil::addslashes($_POST['UNPACK']).'"' : ''),
		"APPLICATION" => (array_key_exists('APPLICATION', $_POST) ? '"'.CUtil::addslashes($_POST['APPLICATION']).'"' : '')
	);

	if ($condID == 0 || $copy)
	{
	}

if (empty($errors))
	{
		if ($condID > 0 && !$copy)
		{
			if (!$DB->Update("appforsale_push", $arFields, "WHERE ID = " . $condID))
			{
				if ($ex = $APPLICATION->GetException())
					$errors[] = $ex->GetString();
				else
					$errors[] = str_replace('#ID#', $discountID, GetMessage('BT_SALE_DISCOUNT_EDIT_ERR_UPDATE'));
			}
		}
		else
		{
			$condID = (int) $DB->Insert("appforsale_push", $arFields);
			if ($condID <= 0)
			{
				if ($ex = $APPLICATION->GetException())
					$errors[] = $ex->GetString();
				else
					$errors[] = GetMessage('BT_SALE_DISCOUNT_EDIT_ERR_ADD');
			}
			else
			{
				//if ($couponsAdd)
				//{
					//	$couponsFields['DISCOUNT_ID'] = $discountID;
				//	$couponsResult = Internals\DiscountCouponTable::addPacket(
				//		$couponsFields,
				//		$additionalFields['COUPON_COUNT']
				//	);
				//	if (!$couponsResult->isSuccess())
				//	{
				//		$errors = $couponsResult->getErrorMessages();
				//	}
				//}
			}
		}
	}
	if (empty($errors))
	{
		if (empty($_POST['apply']))
			LocalRedirect("/bitrix/admin/appforsale_push.php?lang=".LANGUAGE_ID.GetFilterParams("filter_", false));
		else
			LocalRedirect("/bitrix/admin/appforsale_push_edit.php?lang=".LANGUAGE_ID."&ID=".$condID.'&'.$control->ActiveTabParam());
	}
}


$arFields = array();


if ($condID > 0 && !$copy)
	$APPLICATION->SetTitle(GetMessage('BT_SALE_DISCOUNT_EDIT_MESS_UPDATE_DISCOUNT', array('#ID#' => $condID)));
else
	$APPLICATION->SetTitle(GetMessage('BT_SALE_DISCOUNT_EDIT_MESS_ADD_DISCOUNT'));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$defaultValues = array(
	'LID' => '',
	'NAME' => '',
	'CURRENCY' => '',
	'DISCOUNT_VALUE' => '',
	'DISCOUNT_TYPE' => 'P',
	'ACTIVE' => 'Y',
	'SORT' => '100',
	'ACTIVE_FROM' => '',
	'ACTIVE_TO' => '',
	'PRIORITY' => 1,
	'LAST_DISCOUNT' => 'Y',
	'CONDITIONS' => '',
	'XML_ID' => '',
	'ACTIONS' => '',
);

if (isset($_REQUEST['LID']))
	$defaultValues['LID'] = trim($_REQUEST['LID']);
if ('' == $defaultValues['LID'])
	$defaultValues['LID'] = 's1';


$contextMenuItems = array(
	array(
		"TEXT" => GetMessage('LIST_RULES'),
		"LINK" => "/bitrix/admin/appforsale_push.php?lang=".LANGUAGE_ID.GetFilterParams("filter_"),
		"ICON" => "btn_list"
	)
);

$contextMenu = new CAdminContextMenu($contextMenuItems);
$contextMenu->Show();
unset($contextMenu, $contextMenuItems);

$rsCound = $DB->Query("SELECT * FROM appforsale_push WHERE ID = " . $condID);
if (!($arCound = $rsCound->Fetch()))
{
	$condID = 0;
	$arCound = $defaultValues;
}

$control->BeginPrologContent();
CJSCore::Init(array('date'));
$control->EndPrologContent();

$control->BeginEpilogContent();
echo GetFilterHiddens("filter_");?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>">
<input type="hidden" name="ID" value="<? echo $condID; ?>">
<?
if ($copy)
{
	?><input type="hidden" name="action" value="copy"><?
}
echo bitrix_sessid_post();
$control->EndEpilogContent();
$control->Begin(array(
	"FORM_ACTION" => '/bitrix/admin/appforsale_push_edit.php?lang='.LANGUAGE_ID,
));



$control->BeginNextFormTab();
	if ($condID > 0 && !$copy)
		$control->AddViewField('ID','ID:',$condID,false);

$control->AddCheckBoxField("ACTIVE", GetMessage('FIELD_ACTIVE').":", false, array("Y","N"), $arCound['ACTIVE']=="Y");
$control->AddEditField("NAME", GetMessage('FIELD_NAME').":", true, array("size" => 20, "maxlength" => 255, "id" => "NAME"), $arCound['NAME']);

$control->AddEditField("SORT", GetMessage('FIELD_SORT').":", false, array("size" => 20, "maxlength" => 255, "id" => "SORT"), $arCound['SORT']);
$control->AddEditField("PRIORITY", GetMessage('FIELD_PRIORITY').":", false, array("size" => 20, "maxlength" => 255, "id" => "PRIORITY"), $arCound['PRIORITY']);


$control->AddCheckBoxField("LAST_PUSH", GetMessage("FIELD_LAST_PUSH").":", false, array("Y","N"), $arCound['LAST_PUSH'] == "Y");


$control->BeginNextFormTab();


$control->AddSection("SECTION_ACTIONS", GetMessage("SECTION_ACTIONS"));
$control->BeginCustomField("APPLICATION");
echo '<tr><td colspan="2"><textarea id="APPLICATION" name="APPLICATION" style="width:98%; overflow:auto;" wrap="OFF">'.htmlspecialcharsbx($arCound['APPLICATION']).'</textarea></td></tr>';

CCodeEditor::Show(
array(
'textareaId' => 'APPLICATION',
'height' => 150,
'forceSyntax' => "php"
		));

$control->EndCustomField("APPLICATION");

$control->AddSection("SECTION_CONDITIONS", GetMessage("SECTION_CONDITIONS"));
$control->BeginCustomField("UNPACK");


echo '<tr><td colspan="2"><textarea id="UNPACK" name="UNPACK" style="width:98%; overflow:auto;" wrap="OFF">'.htmlspecialcharsbx($arCound['UNPACK']).'</textarea></td></tr>';

CCodeEditor::Show(
		array(
			'textareaId' => 'UNPACK',
			'height' => 150,
			'forceSyntax' => "php"
		));

$control->EndCustomField("UNPACK");


$control->Buttons(
	array(
		"disabled" => ($saleModulePermissions < "W"),
		"back_url" => "/bitrix/admin/appforsale_push.php?lang=".LANGUAGE_ID.GetFilterParams("filter_")
	)
);
$control->Show();

//$tabControl->Begin();
//$tabControl->BeginNextTab();

//echo '1';


//$tabControl->BeginNextTab();
//echo '2';
//$tabControl->Buttons(array());
//$tabControl->End();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>

