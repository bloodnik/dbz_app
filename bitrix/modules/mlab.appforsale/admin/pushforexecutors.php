<?
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

//include($_SERVER["DOCUMENT_ROOT"].'/bitrix/admin/iblock_section_admin.php?IBLOCK_ID=10&type=pushforexecutors&lang=ru&find_section_section=0&SECTION_ID=0');

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");


// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("mlab.appforsale");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D") {
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$sTableID = "tbl_rubric"; // ID таблицы
$oSort    = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin   = new CAdminList($sTableID, $oSort); // основной объект списка

// ******************************************************************** //
//                ВЫБОРКА ЭЛЕМЕНТОВ СПИСКА                              //
// ******************************************************************** //

// выберем список рассылок
CModule::IncludeModule("iblock");
$arSelect = Array("DATE_CREATE", "ID", "NAME");
$arFilter = Array("IBLOCK_ID" => 10, "ACTIVE" => "Y");
$rsData   = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);


// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
	/* array("id"=>"ID",
	   "content"  =>"ID",
	   "align"    =>"right",
	   "default"  =>true,
	 ),*/
	array(
		"id"      => "NAME",
		"content" => 'Текст',
		"default" => true,
	),
	array(
		"id"      => "DATE_CREATE",
		"content" => 'Дата создания',
		"default" => true,
	),
));

while ($arRes = $rsData->NavNext(true, "f_")):

	// создаем строку. результат - экземпляр класса CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

endwhile;

// ******************************************************************** //
//                АДМИНИСТРАТИВНОЕ МЕНЮ                                 //
// ******************************************************************** //

// сформируем меню из одного пункта - добавление рассылки
/*$aContext = array(
  array(
    "TEXT"=>'Создать пуш',
    "LINK"=>"iblock_element_edit.php?IBLOCK_ID=10&type=pushforexecutors",
    "TITLE"=>GetMessage("POST_ADD_TITLE"),
    "ICON"=>"btn_new",
  ),
);

// и прикрепим его к списку
$lAdmin->AddAdminContextMenu($aContext);
*/
// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle(GetMessage("rub_title"));

//Список городов
// Города
$arCities = [];
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ] = $arCity["NAME"];
}


// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<select name="city" class="form-control" style="width: 35%">
		<option value="">Все города</option>
		<? foreach ($arCities as $key => $arCity): ?>
			<option value="<?=$key?>"><?=$arCity?></option>
		<? endforeach; ?>
	</select>
	<input class="form-control" name="push" style="width:38%" type="text">
	<input id="sendpush" type="button" value="Отправить пуш" class="adm-btn adm-btn-save" style="width:20%">


	<script>
        $(function () {
            $('#sendpush').on('click', function () {
                $.ajax({
                    url: '/youdo/api/push.php',
                    data: {
                        text: $("input[name=push]").val(),
                        city: $("select[name=city]").val(),
                    },
                    success: function (responsedata) {
                        document.location.reload(true);
                    }
                });
            });
        });
	</script>

<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>

<?
// завершение страницы
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>