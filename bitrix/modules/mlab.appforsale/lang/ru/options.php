<? 
$MESS['NOTIFICATION'] = 'Уведомления';
$MESS['CONFIGURE_PUSH_NOTIFICATION'] = 'Push-уведомления';
$MESS['CONFIGURE_SMS_NOTIFICATION'] = 'Sms-уведомления';
$MESS['URL'] = 'Url';
$MESS['STATUS'] = 'Статус';
$MESS['ACTIVE'] = 'Активен';
$MESS['ERROR_CURL'] = 'Не установлена библиотека cURL';
$MESS['ERROR_KEY'] = 'Неверный ключ';
$MESS['CONFIGURE'] = 'Настройки';
$MESS['API_KEY'] = 'Ключ API';
$MESS['GOOGLE_INFO'] = 'Чтобы получить ключ API, посетите сайт <a href="https://developers.google.com/mobile/add" target="_blank">Google Developers</a>.';
$MESS['SMS_INFO'] = '#PHONE# - номер телефона, #CODE# - код подтверждения';
$MESS['MULTIPROFILES'] = 'Мультипрофиль';
$MESS['ANALYTICS'] = 'Аналитика';
$MESS['GOOGLE_ANALYTICS_KEY'] = 'Ключ Google Analytics';

$MESS['OTHER'] = 'Прочие';

$MESS['APPFORSALE_YANDEX_MONEY_TAB'] = 'Яндекс.Деньги';
$MESS['APPFORSALE_YANDEX_MONEY_TITLE'] = 'Параметры платежной системы';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER'] = 'Номер кошелька';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_ALT'] = 'Номер кошелька в Яндекс.Деньги';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_INFO'] = 'Чтобы зарегистрировать новый кошелек, посетите сайт <a href="https://money.yandex.ru/reg/" target="_blank">Яндекс.Денег</a>';

$MESS['APPFORSALE_YANDEX_MONEY_CLIENT_ID'] = 'Идентификатор приложения';
$MESS['APPFORSALE_YANDEX_MONEY_CLIENT_ID_ALT'] = 'полученный при регистрации';

$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET'] = 'Секретное слово';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_ALT'] = 'Секрет, который получен от Яндекс';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_INFO'] = 'Получите секретное слово в <a href="https://money.yandex.ru/myservices/online.xml" target="_blank">настройках Яндекс.Денег</a> и укажите http://'.$_SERVER['SERVER_NAME'].'/bitrix/tools/appforsale_ps_result.php в качестве адреса для уведомлений';

$MESS['APPFORSALE_YANDEX_KASSA_TAB'] = 'Яндекс.Касса';
$MESS['APPFORSALE_YANDEX_KASSA_TITLE'] = 'Параметры платежной системы';
$MESS['APPFORSALE_YANDEX_INFO'] = 'Используется протокол commonHTTP-3.0<br />Работа через Центр Приема Платежей <a href="https://kassa.yandex.ru" target="_blank">https://kassa.yandex.ru</a>';
$MESS['APPFORSALE_YANDEX_CHECK'] = 'Проверка HTTPS';
$MESS['APPFORSALE_YANDEX_CHECK_ALT'] = 'Проверка доступности сайта по протоколу HTTPS. Необходимо для корректной работы платежной системы';
$MESS['APPFORSALE_YANDEX_ERROR'] = 'Ошибка';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_ADDITIONAL'] = 'Дополнительные настройки платежной системы';
$MESS['APPFORSALE_YANDEX_KASSA_DEMO'] = 'Тестовый режим';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION'] = 'Настройки подключения Яндекса';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID'] = 'Идентификатор магазина в ЦПП (ShopID)';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID_ALT'] = 'Код магазина, который получен от Яндекс';
$MESS['APPFORSALE_YANDEX_KASSA_SCID'] = 'Номер витрины магазина в ЦПП (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SCID_ALT'] = 'Номер витрины магазина в ЦПП (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD'] = 'Пароль магазина';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD_ALT'] = 'Пароль магазина на Яндекс';



$MESS['APPFORSALE_RUB'] = 'руб.';
$MESS['APPFORSALE_OTHER_FORMAT_STRING'] = 'Строка формата для вывода валюты';

$MESS['APPFORSALE_OTHER_HEADING_USER'] = 'Настройки пользователя';
$MESS['APPFORSALE_OTHER_CREDIT'] = 'Стартовый баланс пользователя';
$MESS['APPFORSALE_OTHER_CREDIT_ALT'] = 'Данная сумма будет начисляться вновь зарегистрированным пользователям';
$MESS['APPFORSALE_BONUS'] = 'Бонус при зачислении средств на счет';
$MESS['APPFORSALE_BONUS_RUB'] = 'Руб.';
$MESS['APPFORSALE_BONUS_PERCENT'] = '%';

$MESS['APPFORSALE_OTHER_HEADING_INVITE'] = 'Пригласить друга';
$MESS['APPFORSALE_INVITE_COUNT'] = 'Ограничение по количеству приглашений';
$MESS['APPFORSALE_INVITE_SUMM'] = 'Сумма бонуса';

$MESS['APPFORSALE_LOGIN_TAB'] = 'Авторизация';
$MESS['APPFORSALE_LOGIN_TITLE'] = 'Авторизация';
$MESS['APPFORSALE_LOGIN_MASK'] = 'Маска';
$MESS['APPFORSALE_LOGIN_APPROVED'] = 'Номер для проверки APPLE';

$MESS['APPFORSALE_MONETIZATION_TAB'] = 'Монетизация';
$MESS['APPFORSALE_MONETIZATION_TITLE'] = 'Монетизация';
$MESS['APPFORSALE_MONETIZATION_USE'] = 'Включить монетизацию';
$MESS['APPFORSALE_MONETIZATION_ACCESS'] = 'Стоимость доступа к сервису';



$MESS['APPFORSALE_MONETIZATION_RECUR_TEST'] = 'Настройки тестовой подписки';
$MESS['APPFORSALE_MONETIZATION_RECUR'] = 'Настройки подписки';
$MESS['APPFORSALE_MONETIZATION_RECUR_TYPE'] = 'Тип периода';
$MESS['APPFORSALE_MONETIZATION_RECUR_LENGTH'] = 'Период';
$MESS['APPFORSALE_MONETIZATION_RECUR_PRICE'] = 'Стоимость';
$MESS['APPFORSALE_MONETIZATION_RECUR_H'] = 'час';
$MESS['APPFORSALE_MONETIZATION_RECUR_D'] = 'сутки';
$MESS['APPFORSALE_MONETIZATION_RECUR_W'] = 'неделя';
$MESS['APPFORSALE_MONETIZATION_RECUR_M'] = 'месяц';
$MESS['APPFORSALE_MONETIZATION_RECUR_Q'] = 'квартал';
$MESS['APPFORSALE_MONETIZATION_RECUR_S'] = 'полугодие';
$MESS['APPFORSALE_MONETIZATION_RECUR_Y'] = 'год';

$MESS['APPFORSALE_MODERATION'] = 'Модерация';
$MESS['APPFORSALE_MODERATION_TASK'] = 'Модерация заданий';

$MESS['APPFORSALE_PUSH_TEXT_TAB'] = 'Тексты уведомлений';
$MESS['APPFORSALE_PUSH_TEXT_TITLE'] = 'Тексты уведомлений';
$MESS['APPFORSALE_PUSH_TEXT_MODERATE_HEAD'] = 'Регистрация/модерация';
$MESS['APPFORSALE_PUSH_TEXT_REGISTER'] = 'Пуш после регистрации';
$MESS['APPFORSALE_PUSH_TEXT_MODERATE'] = 'Пуш после модерации';
?>