<? 
$MESS['LIST_TASKS'] = 'Список заданий';
$MESS['SECTION_TASKS'] = 'Типы заданий';
$MESS['PROPERTY_TASKS'] = 'Свойства заданий';
$MESS['LIST_PROFILES'] = 'Список профилей';
$MESS['SECTION_PROFILES'] = 'Типы профилей';
$MESS['PROPERTY_PROFILES'] = 'Свойства профилей';
$MESS['LIST_OFFER'] = 'Список предложений';
$MESS['PROPERTY_OFFER'] = 'Свойства предложений';
$MESS['LIST_COMMENT'] = 'Список комментариев';
$MESS['PROPERTY_COMMENT'] = 'Свойства комментариев';
$MESS['LIST_LOCATION'] = 'Список городов';
$MESS['USERS'] = 'Исполнители';
$MESS['APPFORSALE_ACCOUNT_ADMIN'] = 'Счета';
$MESS['TRANSACTIONS'] = 'Транзакции';
$MESS['RECCURRING'] = 'Продление подписки';
$MESS['SETTING'] = 'Настройки';
$MESS['NOTICE'] = 'Уведомления';
$MESS['YANDEX_MONEY'] = 'Яндекс.Деньги';
$MESS['YANDEX_KASSA'] = 'Яндекс.Касса';
$MESS['OTHER'] = 'Прочие';
$MESS['LOGIN'] = 'Авторизация';
$MESS['MONETIZATION'] = 'Монетизация';
$MESS['STRATEGY_FOR_PUSH_NOTIFICATION'] = 'Стратегия push-уведомлений';
$MESS['RULES'] = 'Правила';
$MESS['DEVICES'] = 'Устройства';
$MESS['PUSH_TEXTS'] = 'Тексты уведомлений';
?>