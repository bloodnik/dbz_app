<?
$MESS['FIELD_NAME'] = 'Название';
$MESS['FIELD_ACTIVE'] = 'Активность';
$MESS['FIELD_SORT'] = 'Сортировка';
$MESS['FIELD_TIMESTAMP_X'] = 'Дата изменения';
$MESS['FIELD_MODIFIED_BY'] = 'Кем изменено';
$MESS['FIELD_DATE_CREATE'] = 'Дата создания';
$MESS['FIELD_CREATED_BY'] = 'Кем создано';
$MESS['FIELD_PRIORITY'] = 'Приоритет применимости';
$MESS['FIELD_LAST_PUSH'] = 'Прекратить прим. правил';

$MESS['RULES'] = 'Правила';
$MESS['BTN_NEW'] = 'Добавить правило';
$MESS['DELETE'] = 'Удалить';
$MESS['EDIT'] = 'Изменить';
$MESS['ACTIVATE'] = 'Активировать';
$MESS['DEACTIVATE'] = 'Деактивировать';
$MESS["BT_SALE_DISCOUNT_LIST_MESS_DELETE_DISCOUNT_CONFIRM"] = "Вы уверены, что хотите удалить это правило?";
?>