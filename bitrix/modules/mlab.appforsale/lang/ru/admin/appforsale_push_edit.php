<?
$MESS['FIELD_NAME'] = 'Название';
$MESS['FIELD_ACTIVE'] = 'Активность';
$MESS['FIELD_SORT'] = 'Индекс сортировки';
$MESS['FIELD_TIMESTAMP_X'] = 'Дата изменения';
$MESS['FIELD_MODIFIED_BY'] = 'Кем изменено';
$MESS['FIELD_DATE_CREATE'] = 'Дата создания';
$MESS['FIELD_CREATED_BY'] = 'Кем создано';
$MESS['FIELD_PRIORITY'] = 'Приоритет применимости';
$MESS['FIELD_LAST_PUSH'] = 'Прекратить дальнейшее применение правил';

$MESS['RULES'] = 'Правила';

$MESS['TAB1'] = 'Общие параметры';
$MESS['TAB1_TITLE'] = 'Общие параметры правила';
$MESS['TAB2'] = 'Действия и условия';
$MESS['TAB2_TITLE'] = 'Выполняемые действия и условия применения';

$MESS['SECTION_ACTIONS'] = 'Действия';
$MESS['SECTION_CONDITIONS'] = 'Дополнительные условия';
$MESS['LIST_RULES'] = 'Список правил';

$MESS["BT_SALE_DISCOUNT_EDIT_MESS_ADD_DISCOUNT"] = "Создание нового правила";
$MESS["BT_SALE_DISCOUNT_EDIT_MESS_UPDATE_DISCOUNT"] = "Изменение правила с кодом #ID#";
?>