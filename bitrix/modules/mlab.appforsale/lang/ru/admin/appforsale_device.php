<?
$MESS['FIELD_DATE_CREATE'] = 'Дата регистрации';
$MESS['FIELD_DEVICE_ID'] = 'Идентификатор';
$MESS['FIELD_DEVICE_TYPE'] = 'Тип';
$MESS['FIELD_DEVICE_NAME'] = 'Имя устройства';
$MESS['FIELD_DEVICE_MODEL'] = 'Модель устройства';
$MESS['FIELD_SYSTEM_VERSION'] = 'Версия ОС';
$MESS['FIELD_SETTINGS'] = 'Настройки';
$MESS['FIELD_TOKEN'] = 'Токен';
$MESS['FIELD_USER_ID'] = 'Пользователь';
$MESS['FIELD_DATE_AUTH'] = 'Дата авторизации';
$MESS['DEVICES'] = 'Устройства';
?>