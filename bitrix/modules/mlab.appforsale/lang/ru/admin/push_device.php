<?
$MESS["TITLE"] = "Список устройств";
$MESS["FIELD_DATE_CREATE"] = "Дата регистрации";
$MESS["FIELD_TIMESTAMP_X"] = "Дата изменения";
$MESS["FIELD_DEVICE_ID"] = "Идентификатор";
$MESS["FIELD_DEVICE_MODEL"] = "Модель устройства";
$MESS["FIELD_SYSTEM_VERSION"] = "Версия ОС";
$MESS["FIELD_SETTINGS"] = "Настройки";
$MESS["FIELD_TOKEN"] = "Токен";
$MESS["FIELD_USER_ID"] = "Пользователь";
$MESS["FIELD_DATE_AUTH"] = "Последняя авторизация";
?>