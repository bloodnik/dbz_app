<?
$MESS["SKGR_EMPTY_USER_ID"]="Не указан код пользователя";
$MESS["SKGR_EMPTY_NEXT_DATE"]="Не указана дата следующего платежа";
$MESS["SKGR_EMPTY_ORDER_ID"]="Не указан код заказа";
$MESS["SKGR_NO_USER"]="Пользователь с кодом #ID# не найден";
$MESS["SKGR_NO_ORDER"]="Заказ с кодом #ID# не найден";
$MESS["SKGR_NO_RECID"]="Не указан код записи";
$MESS["SKGR_NO_RECID1"]="Запись с кодом ##ID# не найдена";
$MESS["SKGR_NO_ORDER1"]="Не найден заказ с кодом ##ID#";
$MESS["SKGR_NO_DELIVERY"]="Не найдена подходящая служба доставки";
$MESS["SKGR_EMPTY_SUBSCR"]="Не указан код подписки";
$MESS["SKGR_NO_SUBSCR"]="Подписка с кодом ##ID# не найдена";
$MESS["SKGR_DUB_CANCEL"]="Подписка с кодом ##ID# уже имеет требуемый флаг отмены";
$MESS["SKGR_VAT"]="НДС";



$MESS['FREE'] = 'бесплатно';
$MESS['NEXT'] = 'далее';

$MESS['TYPE_H1'] = 'час';
$MESS['TYPE_H2'] = 'часа';
$MESS['TYPE_H5'] = 'часов';

$MESS['TYPE_D1'] = 'день';
$MESS['TYPE_D2'] = 'дня';
$MESS['TYPE_D5'] = 'дней';

$MESS['TYPE_W1'] = 'неделя';
$MESS['TYPE_W2'] = 'недели';
$MESS['TYPE_W5'] = 'недель';

$MESS['TYPE_M1'] = 'месяц';
$MESS['TYPE_M2'] = 'месяца';
$MESS['TYPE_M5'] = 'месяцев';

$MESS['TYPE_Q1'] = 'квартал';
$MESS['TYPE_Q2'] = 'квартала';
$MESS['TYPE_Q5'] = 'кварталов';

$MESS['TYPE_S1'] = 'полугодие';
$MESS['TYPE_S2'] = 'полугодия';
$MESS['TYPE_S5'] = 'полугодий';

$MESS['TYPE_Y1'] = 'год';
$MESS['TYPE_Y2'] = 'года';
$MESS['TYPE_Y5'] = 'лет';


?>