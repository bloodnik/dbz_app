<? 
$MESS['STRATEGY_FOR_PUSH_NOTIFICATION'] = 'Стратегия push-уведомлений';
$MESS['RULES'] = 'Правила';
$MESS['DEVICES'] = 'Устройства';


$MESS['TASKS'] = 'Задания';
$MESS['USERS'] = 'Исполнители';
$MESS['USERS2'] = 'Список исполнителей';
$MESS['APPFORSALE_ACCOUNT_ADMIN'] = 'Счета';
$MESS['TRANSACTIONS'] = 'Транзакции';
$MESS['RECCURRING'] = 'Продление подписки';
$MESS['SETTING'] = 'Настройки';


$MESS['NOTICE'] = 'Уведомления';
$MESS['YANDEX_MONEY'] = 'Яндекс.Деньги';
$MESS['YANDEX_KASSA'] = 'Яндекс.Касса';
$MESS['OTHER'] = 'Прочие';

$MESS['CURRENCY_RATE'] = 'Курсы валют';
?>