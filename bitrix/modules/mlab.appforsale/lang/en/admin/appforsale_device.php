<?
$MESS['FIELD_DATE_CREATE'] = 'Date create';
$MESS['FIELD_DEVICE_ID'] = 'Device id';
$MESS['FIELD_DEVICE_TYPE'] = 'Type';
$MESS['FIELD_DEVICE_NAME'] = 'Device name';
$MESS['FIELD_DEVICE_MODEL'] = 'Device model';
$MESS['FIELD_SYSTEM_VERSION'] = 'System version';
$MESS['FIELD_SETTINGS'] = 'Settings';
$MESS['FIELD_TOKEN'] = 'Token';
$MESS['FIELD_USER_ID'] = 'User';
$MESS['FIELD_DATE_AUTH'] = 'Date auth';
$MESS['DEVICES'] = 'Devices';
?>