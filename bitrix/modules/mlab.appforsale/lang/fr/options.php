<? 
$MESS['NOTIFICATION'] = 'Avis';
$MESS['CONFIGURE_PUSH_NOTIFICATION'] = 'Les notifications Push';
$MESS['CONFIGURE_SMS_NOTIFICATION'] = 'Les alertes Sms';
$MESS['URL'] = 'Url';
$MESS['STATUS'] = 'Le statut de';
$MESS['ACTIVE'] = 'Actif';
$MESS['ERROR_CURL'] = "N'avez pas installГ© la bibliothГЁque cURL";
$MESS['ERROR_KEY'] = 'ClГ© incorrecte';
$MESS['CONFIGURE'] = 'Les paramГЁtres de';
$MESS['API_KEY'] = "La clГ© de l'API";
$MESS['GOOGLE_INFO'] = 'Pour obtenir une clГ© API, visitez le site <a href="https://developers.google.com/mobile/add" target="_blank">Google Developers</a>.';
$MESS['SMS_INFO'] = '#PHONE#: numГ©ro de tГ©lГ©phone #CODE# - le code de confirmation';
$MESS['MULTIPROFILES'] = 'Multi le profil';
$MESS['ANALYTICS'] = "L'analyste";
$MESS['GOOGLE_ANALYTICS_KEY'] = 'La clГ© Google Analytics';

$MESS['OTHER'] = 'Autres';

$MESS['APPFORSALE_YANDEX_MONEY_TAB'] = "Yandex.De l'argent";
$MESS['APPFORSALE_YANDEX_MONEY_TITLE'] = 'ParamГЁtres du systГЁme de paiement';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER'] = 'Nombre de porte-monnaie';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_ALT'] = "Nombre de porte-monnaie dans Yandex.De l'argent";
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_INFO'] = "Pour enregistrer un nouveau portefeuille, visitez le site <a href=\"https://money.yandex.ru/reg/\" target=\"_blank\">Yandex.D'argent</a>";
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET'] = 'Le mot secret';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_ALT'] = 'Le secret, qui est le produit de Yandex';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_INFO'] = 'Obtenez le mot secret dans le <a href="https://money.yandex.ru/myservices/online.xml" target="_blank">les paramГЁtres de Yandex.D\'argent</a> et spГ©cifiez http://'.$_SERVER['SERVER_NAME'].'/bitrix/tools/appforsale_ps_result.php en tant qu\'adresse pour les notifications';

$MESS['APPFORSALE_YANDEX_KASSA_TAB'] = 'Yandex.Caisse';
$MESS['APPFORSALE_YANDEX_KASSA_TITLE'] = 'ParamГЁtres du systГЁme de paiement';
$MESS['APPFORSALE_YANDEX_INFO'] = "Le protocole utilisГ© est commonHTTP-3.0<br />Travail Г  travers le Centre de RГ©ception des Paiements <a href=\"https://kassa.yandex.ru\" target=\"_blank\">https://kassa.yandex.ru</a>";
$MESS['APPFORSALE_YANDEX_CHECK'] = 'L\'inspection HTTPS';
$MESS['APPFORSALE_YANDEX_CHECK_ALT'] = "VГ©rification de la disponibilitГ© d'un site HTTPS. NГ©cessaire pour le bon fonctionnement du systГЁme de paiement";
$MESS['APPFORSALE_YANDEX_ERROR'] = 'Erreur';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_ADDITIONAL'] = "ParamГЁtres avancГ©s du systГЁme de paiement";
$MESS['APPFORSALE_YANDEX_KASSA_DEMO'] = 'Mode de test';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION'] = 'Les paramГЁtres de connexion Yandex';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID'] = "L'identificateur de magasin dans la ZPP (ShopID)";
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID_ALT'] = 'Le code de la boutique, qui est le produit de Yandex';
$MESS['APPFORSALE_YANDEX_KASSA_SCID'] = 'Le numГ©ro de vitrine de magasin dans la ZPP (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SCID_ALT'] = 'Le numГ©ro de vitrine de magasin dans la ZPP (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD'] = 'Magasin de mot de passe';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD_ALT'] = 'Magasin de mot de passe sur Yandex';



$MESS['APPFORSALE_RUB'] = 'rub.';
$MESS['APPFORSALE_OTHER_FORMAT_STRING'] = 'La chaГ®ne de format de sortie change';

$MESS['APPFORSALE_OTHER_HEADING_USER'] = "Les paramГЁtres de l'utilisateur";
$MESS['APPFORSALE_OTHER_CREDIT'] = "Solde initial de l'utilisateur";
$MESS['APPFORSALE_OTHER_CREDIT_ALT'] = 'Cette somme vous sera facturГ© aux utilisateurs nouvellement inscrits';
$MESS['APPFORSALE_BONUS'] = "Bonus lors de l'inscription des fonds sur le compte";
$MESS['APPFORSALE_BONUS_RUB'] = 'Rub.';
$MESS['APPFORSALE_BONUS_PERCENT'] = '%';

$MESS['APPFORSALE_OTHER_HEADING_INVITE'] = 'Inviter un ami';
$MESS['APPFORSALE_INVITE_COUNT'] = "La restriction sur le nombre d'invitations";
$MESS['APPFORSALE_INVITE_SUMM'] = 'Le montant du bonus';

$MESS['APPFORSALE_LOGIN_TAB'] = 'Autorisation';
$MESS['APPFORSALE_LOGIN_TITLE'] = 'Autorisation';
$MESS['APPFORSALE_LOGIN_MASK'] = 'Le masque';
$MESS['APPFORSALE_LOGIN_APPROVED'] = "Chambre pour la validation d'APPLE";

$MESS['APPFORSALE_MONETIZATION_TAB'] = 'MonГ©tisation';
$MESS['APPFORSALE_MONETIZATION_TITLE'] = 'MonГ©tisation';
$MESS['APPFORSALE_MONETIZATION_USE'] = 'Activer la monГ©tisation';
$MESS['APPFORSALE_MONETIZATION_ACCESS'] = "Le coГ»t de l'accГЁs au service";



$MESS['APPFORSALE_MONETIZATION_RECUR_TEST'] = "Test de l'abonnement";
$MESS['APPFORSALE_MONETIZATION_RECUR'] = "ParamГЁtres d'abonnement";
$MESS['APPFORSALE_MONETIZATION_RECUR_TYPE'] = 'Type de pГ©riode';
$MESS['APPFORSALE_MONETIZATION_RECUR_LENGTH'] = 'PГ©riode';
$MESS['APPFORSALE_MONETIZATION_RECUR_PRICE'] = 'Prix';
$MESS['APPFORSALE_MONETIZATION_RECUR_H'] = 'heure';
$MESS['APPFORSALE_MONETIZATION_RECUR_D'] = 'jour';
$MESS['APPFORSALE_MONETIZATION_RECUR_W'] = 'la semaine';
$MESS['APPFORSALE_MONETIZATION_RECUR_M'] = 'mois';
$MESS['APPFORSALE_MONETIZATION_RECUR_Q'] = 'trimestre';
$MESS['APPFORSALE_MONETIZATION_RECUR_S'] = 'semestre';
$MESS['APPFORSALE_MONETIZATION_RECUR_Y'] = 'annГ©e';

$MESS['APPFORSALE_MODERATION'] = 'ModГ©ration';
$MESS['APPFORSALE_MODERATION_TASK'] = 'La modГ©ration des travaux';
?>