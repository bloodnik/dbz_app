<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

//if ($GLOBALS['USER']->IsAuthorized() && (!isset($_SESSION['USER_LAST_ONLINE']) || intval($_SESSION['USER_LAST_ONLINE']) + 450 <= time()))
//{
//	$_SESSION['USER_LAST_ONLINE'] = time();
//	CUser::SetLastActivityDate($GLOBALS['USER']->GetID());
//}

if ($GLOBALS['USER']->IsAuthorized())
{
	$dbUser = CUser::GetByID($GLOBALS['USER']->GetID());
	if ($arUser = $dbUser->Fetch())
	{
		if ($arUser['ACTIVE'] == 'N')
		{
			$GLOBALS['USER']->Logout();
		}
	}
}
?>