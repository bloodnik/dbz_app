<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$result = [];

try
{
    $request = Bitrix\Main\Context::getCurrent()->getRequest();
    $device = Mlab\Appforsale\Push\Device::createFromRequest($request);
    Mlab\Appforsale\Push\Manager::getInstance()->registerDevice($device);

    $result = ["response" => 1];
}
catch (Exception $e)
{
    $result = ["error" => $e->getMessage()];
}

header("Content-Type: application/json");
echo \Bitrix\Main\Web\Json::encode($result);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>