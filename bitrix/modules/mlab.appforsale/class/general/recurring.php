<?
IncludeModuleLangFile(__FILE__);

$GLOBALS["APPFORSALE_RECURRING"] = array();

class CAllAppforsaleRecurring
{
	function CheckFields($ACTION, &$arFields, $ID = 0)
	{
		if ((is_set($arFields, "USER_ID") || $ACTION=="ADD") && IntVal($arFields["USER_ID"]) <= 0)
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SKGR_EMPTY_USER_ID"), "NO_USER_ID");
			return false;
		}
		if ((is_set($arFields, "NEXT_DATE") || $ACTION=="ADD") && strlen($arFields["NEXT_DATE"]) <= 0)
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SKGR_EMPTY_NEXT_DATE"), "NO_NEXT_DATE");
			return false;
		}

		if (is_set($arFields, "USER_ID"))
		{
			$dbUser = CUser::GetByID($arFields["USER_ID"]);
			if (!$dbUser->Fetch())
			{
				$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $arFields["USER_ID"], GetMessage("SKGR_NO_USER")), "ERROR_NO_USER_ID");
				return false;
			}
		}

		if (is_set($arFields, "PRICE") || $ACTION=="ADD")
		{
			$arFields["PRICE"] = str_replace(",", ".", $arFields["PRICE"]);
			$arFields["PRICE"] = DoubleVal($arFields["PRICE"]);
		}

		if ((is_set($arFields, "RECUR_SCHEME_TYPE") || $ACTION=="ADD") && !array_key_exists($arFields["RECUR_SCHEME_TYPE"], $GLOBALS["SALE_TIME_PERIOD_TYPES"]))
		{
			$arTypes = array_keys($GLOBALS["SALE_TIME_PERIOD_TYPES"]);
			$arFields["RECUR_SCHEME_TYPE"] = $arTypes[1];
		}

		if ((is_set($arFields, "WITHOUT_ORDER") || $ACTION=="ADD") && $arFields["WITHOUT_ORDER"] != "Y")
			$arFields["WITHOUT_ORDER"] = "N";
		if ((is_set($arFields, "CANCELED") || $ACTION=="ADD") && $arFields["CANCELED"] != "Y")
			$arFields["CANCELED"] = "N";

		return True;
	}

	function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return False;

		if (!CAppforsaleRecurring::CheckFields("UPDATE", $arFields, $ID))
			return false;

		$strUpdate = $DB->PrepareUpdate("appforsale_recurring", $arFields);
		$strSql = "UPDATE appforsale_recurring SET ".$strUpdate." WHERE ID = ".$ID." ";
		$DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);

		unset($GLOBALS["APPFORSALE_RECURRING"]["APPFORSALE_RECURRING_CACHE_".$ID]);

		return $ID;
	}

	function Delete($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			return False;

		unset($GLOBALS["APPFORSALE_RECURRING"]["APPFORSALE_RECURRING_CACHE_".$ID]);

		return $DB->Query("DELETE FROM appforsale_recurring WHERE ID = ".$ID." ", true);
	}
	
	function CheckRecurring()
	{		
		if (defined("APPFOSALE_PROC_REC_NUM"))
			$processRecords = APPFOSALE_PROC_REC_NUM;
		else
			$processRecords = 3;
		
		$dbRecurring = CAppforsaleRecurring::GetList(
			array(),
			array(
				"<=NEXT_DATE" => Date($GLOBALS["DB"]->DateFormatToPHP(CLang::GetDateFormat("FULL", SITE_ID))),
				"CANCELED" => "N",
				"!REMAINING_ATTEMPTS" => 0
			),
			false,
			array("nTopCount" => $processRecords),
			array("ID")
		);
		$cnt = 0;
		while ($arRecurring = $dbRecurring->Fetch())
		{
			CAppforsaleRecurring::NextPayment($arRecurring["ID"]);
			$cnt++;
			if ($cnt >= $processRecords)
 				break;
		}
	}

	function NextPayment($ID)
	{
		global $DB;
		global $USER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SKGR_NO_RECID"), "NO_RECORD_ID");
			return False;
		}

		$arRecur = CAppforsaleRecurring::GetByID($ID);
		if (!$arRecur)
 		{
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("SKGR_NO_RECID1")), "NO_RECORD");
			return False;
 		}

		CModule::IncludeModule("iblock");

 		$bSuccess = True;

 		if (intval($arRecur['PRODUCT_ID']) > 0)
 		{
			$rsElement = CIBlockElement::GetList(
				array(), 
				array(
					'ID' => $arRecur['PRODUCT_ID']
				),
				false, 
				false, 
				array(
					'ID',
					'IBLOCK_ID',
					'IBLOCK_SECTION_ID',
					'PREVIEW_TEXT'
				)
			);
			if ($arElement = $rsElement->GetNext())
			{			
				$rsSection = CIBlockSection::GetList(
					array(),
					array(
						"IBLOCK_ID" => $arElement['IBLOCK_ID'],
						"ID" => $arElement['IBLOCK_SECTION_ID']
					),
					false,
					array(
						"ID",
						"NAME",
						"UF_RECUR_TYPE",
						"UF_RECUR_LENGTH",
						"UF_RECUR_PRICE"
					)
				);
				if ($arSection = $rsSection->GetNext())
				{							
					$rsRecurType = CUserFieldEnum::GetList(array(), array("ID" => $arSection['UF_RECUR_TYPE']));
					if ($arRecurType = $rsRecurType->GetNext())
						$arSection['UF_RECUR_TYPE'] = $arRecurType['XML_ID'];
					
					$arProduct = array(
						"PRODUCT_ID" => $arElement['ID'],
						"PRODUCT_NAME" => $arSection['NAME'],
						"RECUR_SCHEME_TYPE" => $arSection['UF_RECUR_TYPE'],
						"RECUR_SCHEME_LENGTH" => $arSection['UF_RECUR_LENGTH'],
						"PRICE" => CAppforsaleRecurring::ratio($arElement['~PREVIEW_TEXT'], $arSection['UF_RECUR_PRICE']),
					);
	
					$arProduct['NEXT_DATE'] = CAppforsaleRecurring::GetNextDate($arProduct);
				}
			}
 		}
 		else
 		{
 			$arProduct = array(
 				"PRODUCT_ID" => '',
 				"RECUR_SCHEME_TYPE" => COption::GetOptionString('mlab.appforsale', "RECUR_TYPE", 'M'),
 				"RECUR_SCHEME_LENGTH" => COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH", 0),
 				"PRICE" => COption::GetOptionInt('mlab.appforsale', "RECUR_PRICE", 0),
 			);

 			$arProduct['NEXT_DATE'] = CAppforsaleRecurring::GetNextDate($arProduct);
 		}
	
		if (!$arProduct || !is_array($arProduct) || empty($arProduct))
		{
			CAppforsaleRecurring::CancelRecurring($arRecur["ID"], "Y", "Product is not found");
			return true;
		}
			
		$res = false;
		if ($bSuccess)
		{
			$res = CAppforsaleUserAccount::Pay($arRecur["USER_ID"], $arProduct['PRICE']);
			if ($res)
			{
				if (intval($arProduct['PRODUCT_ID']) > 0)
				{
					$el = new CIBlockElement;
					$el->Update(
						$arProduct['PRODUCT_ID'],
						array(
							"DATE_ACTIVE_TO" => $arProduct['NEXT_DATE']
						)
					);
				}
					
				$arFields = array(
					"PRODUCT_NAME" => $arProduct["PRODUCT_NAME"],
					"RECUR_SCHEME_TYPE" => $arProduct["RECUR_SCHEME_TYPE"],
					"RECUR_SCHEME_LENGTH" => $arProduct["RECUR_SCHEME_LENGTH"],
					"PRIOR_DATE" => Date($GLOBALS["DB"]->DateFormatToPHP(CLang::GetDateFormat("FULL", SITE_ID))),
					"NEXT_DATE" => $arProduct['NEXT_DATE'],
					"REMAINING_ATTEMPTS" => (Defined("APPFORSALE_PROC_REC_ATTEMPTS") ? APPFORSALE_PROC_REC_ATTEMPTS : 3),
					"SUCCESS_PAYMENT" => "Y",
					"CANCELED" => 'N'
				);
	
	 			CAppforsaleRecurring::Update($arRecur["ID"], $arFields);
			}
			else
			{
				$arFields = array(
					"PRODUCT_NAME" => $arProduct["PRODUCT_NAME"],
					"RECUR_SCHEME_TYPE" => $arProduct["RECUR_SCHEME_TYPE"],
					"RECUR_SCHEME_LENGTH" => $arProduct["RECUR_SCHEME_LENGTH"],
					"NEXT_DATE" => Date($GLOBALS["DB"]->DateFormatToPHP(CLang::GetDateFormat("FULL", SITE_ID)), time() + APPFORSALE_PROC_REC_TIME + CTimeZone::GetOffset()),
					"REMAINING_ATTEMPTS" => (IntVal($arRecur["REMAINING_ATTEMPTS"]) - 1),
					"SUCCESS_PAYMENT" => "N"
				);
				CAppforsaleRecurring::Update($arRecur["ID"], $arFields);
	
				if ((IntVal($arRecur["REMAINING_ATTEMPTS"]) - 1) <= 0)
				{
					CAppforsaleRecurring::CancelRecurring($arRecur["ID"], "Y", "Can't pay order");
				}
			}
		}

		return $res;
	}

	function CancelRecurring($ID, $val, $description = "")
	{
		global $DB, $USER;

		$ID = IntVal($ID);
		$val = (($val != "Y") ? "N" : "Y");
 		$description = Trim($description);

		if ($ID <= 0)
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SKGR_EMPTY_SUBSCR"), "NO_RECURRING_ID");
			return False;
		}

		$arRecurring = CAppforsaleRecurring::GetByID($ID);
		if (!$arRecurring)
		{
			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("SKGR_NO_SUBSCR")), "NO_RECURRING");
			return False;
		}

// 		if ($arRecurring["CANCELED"] == $val)
// 		{
// 			$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $ID, GetMessage("SKGR_DUB_CANCEL")), "ALREADY_FLAG");
// 			return False;
// 		}

		$arFields = array(
				"CANCELED" => $val,
				"DATE_CANCELED" => (($val == "Y") ? Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))) : False),
				"CANCELED_REASON" => ( strlen($description)>0 ? $description : false )
			);
		$res = CAppforsaleRecurring::Update($ID, $arFields);

		unset($GLOBALS["APPFORSALE_RECURRING"]["APPFORSALE_RECURRING_CACHE_".$ID]);

		return $res;
	}

	function AgentCheckRecurring()
	{
		$bTmpUser = false;
 		if (!isset($GLOBALS["USER"]) || !is_object($GLOBALS["USER"]))
		{
			$bTmpUser = true;
			$GLOBALS["USER"] = new CUser;
		}

 		CAppforsaleRecurring::CheckRecurring();

 		global $pPERIOD;
 		if (defined("APPFORSALE_PROC_REC_FREQUENCY") && IntVal(APPFORSALE_PROC_REC_FREQUENCY) > 0)
 			$pPERIOD = IntVal(APPFORSALE_PROC_REC_FREQUENCY);
 		else
 			$pPERIOD = 7200;

		if ($bTmpUser)
		{
			unset($GLOBALS["USER"]);
		}

		return "CAppforsaleRecurring::AgentCheckRecurring();";
	}

	
	function GetNextDate($arProduct)
	{		
		switch ($arProduct['RECUR_SCHEME_TYPE'])
		{
			case "H":
				$add = array("HH" => $arProduct['RECUR_SCHEME_LENGTH']);
				break;
			case "D":
				$add = array("DD" => $arProduct['RECUR_SCHEME_LENGTH']);
				break;
			case "W":
				$add = array("DD" => ($arProduct['RECUR_SCHEME_LENGTH'] * 7));
				break;
			case "M":
				$add = array("MM" => $arProduct['RECUR_SCHEME_LENGTH']);
				break;
			case "Q":
				$add = array("MM" => ($arProduct['RECUR_SCHEME_LENGTH'] * 3));
				break;
			case "S":
				$add = array("MM" => ($arProduct['RECUR_SCHEME_LENGTH'] * 6));
				break;
			case "Y":
				$add = array("YYYY" => $arProduct['RECUR_SCHEME_LENGTH']);
				break;
		}
						
		return date("d.m.Y H:i:s", AddToTimeStamp($add));
	}
	
	function ToString($arSection, $b = false)
	{		

		
			
		$str = "";
		if ((COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH_TEST", 0) == 0 && COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH", 0) == 0) || $b)
		{
			$rsRecurTypeTest = CUserFieldEnum::GetList(array(), array("ID" => $arSection['UF_RECUR_TYPE_TEST']));
			if ($arRecurTypeTest = $rsRecurTypeTest->GetNext())
				$arSection['UF_RECUR_TYPE_TEST'] = $arRecurTypeTest['XML_ID'];
			
			$rsRecurType = CUserFieldEnum::GetList(array(), array("ID" => $arSection['UF_RECUR_TYPE']));
			if ($arRecurType = $rsRecurType->GetNext())
				$arSection['UF_RECUR_TYPE'] = $arRecurType['XML_ID'];
			
			if (intval($arSection['UF_RECUR_LENGTH_TEST']) > 0)
			{
				$str .= $arSection['UF_RECUR_LENGTH_TEST'] . ' ';
				switch ($arSection['UF_RECUR_TYPE_TEST'])
				{
					case "H":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_H1'),
							GetMessage('TYPE_H2'),
							GetMessage('TYPE_H5')
						);
						break;
					case "D":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_D1'),
							GetMessage('TYPE_D2'),
							GetMessage('TYPE_D5')
						);
						break;
					case "W":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_W1'),
							GetMessage('TYPE_W2'),
							GetMessage('TYPE_W5')
						);
						break;
					case "M":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_M1'),
							GetMessage('TYPE_M2'),
							GetMessage('TYPE_M5')
						);
						break;
					case "Q":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_Q1'),
							GetMessage('TYPE_Q2'),
							GetMessage('TYPE_Q5')
						);
						break;
					case "S":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_S1'),
							GetMessage('TYPE_S2'),
							GetMessage('TYPE_S5')
						);
						break;
					case "Y":
						$str .= CAppforsaleRecurring::pluralForm(
							$arSection['UF_RECUR_LENGTH_TEST'],
							GetMessage('TYPE_Y1'),
							GetMessage('TYPE_Y2'),
							GetMessage('TYPE_Y5')
						);
						break;
				}
				$str .= ' / ';
				if (intval($arSection['UF_RECUR_PRICE_TEST']) > 0)
				{
					$str .= AppforsaleFormatCurrency($arSection['UF_RECUR_PRICE_TEST']);
				}
				else
				{
					$str .= GetMessage('FREE');
				}
				$str .= '<br /> '.GetMessage('NEXT').' ';
			}
			
			if (intval($arSection['UF_RECUR_LENGTH']) > 0 && intval($arSection['UF_RECUR_PRICE']) > 0)
			{
				$str .= $arSection['UF_RECUR_LENGTH'] . ' ';
				switch ($arSection['UF_RECUR_TYPE'])
				{
					case "H":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_H1'),
								GetMessage('TYPE_H2'),
								GetMessage('TYPE_H5')
						);
						break;
					case "D":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_D1'),
								GetMessage('TYPE_D2'),
								GetMessage('TYPE_D5')
						);
						break;
					case "W":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_W1'),
								GetMessage('TYPE_W2'),
								GetMessage('TYPE_W5')
						);
						break;
					case "M":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_M1'),
								GetMessage('TYPE_M2'),
								GetMessage('TYPE_M5')
						);
						break;
					case "Q":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_Q1'),
								GetMessage('TYPE_Q2'),
								GetMessage('TYPE_Q5')
						);
						break;
					case "S":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_S1'),
								GetMessage('TYPE_S2'),
								GetMessage('TYPE_S5')
						);
						break;
					case "Y":
						$str .= CAppforsaleRecurring::pluralForm(
								$arSection['UF_RECUR_LENGTH'],
								GetMessage('TYPE_Y1'),
								GetMessage('TYPE_Y2'),
								GetMessage('TYPE_Y5')
						);
						break;
				}
				$str .= ' / ' . AppforsaleFormatCurrency($arSection['UF_RECUR_PRICE']);
			}
			
			if (!empty($str))
				$str = '<br /><small>'.$str.'</small>';
			
		}
		return $str;
	}
	
	function OnBeforeIBlockElementAdd(&$arFields)
	{
		global $USER;
		$arIBlock = CIBlock::GetArrayByID($arFields['IBLOCK_ID']);
		if (ToLower($arIBlock['CODE']) == 'profile' && CModule::IncludeModule('mlab.appforsale'))
		{	
			if (COption::GetOptionString('mlab.appforsale', 'monetization_use', 'Y') == 'Y' &&
				intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_PRICE', 0)) == 0)
			{
				$rsSection = CIBlockSection::GetList(
					array(),
					array(
						"IBLOCK_ID" => $arFields['IBLOCK_ID'],
						"ID" => $arFields['IBLOCK_SECTION'][0]
					),
					false, 
					array(
						"ID",
						"NAME",
						"UF_RECUR_TYPE_TEST",
						"UF_RECUR_LENGTH_TEST",
						"UF_RECUR_PRICE_TEST",
						"UF_RECUR_TYPE",
						"UF_RECUR_LENGTH",
						"UF_RECUR_PRICE"
					)
				);
				if ($arSection = $rsSection->GetNext())
				{
					$rsRecurTypeTest = CUserFieldEnum::GetList(array(), array("ID" => $arSection['UF_RECUR_TYPE_TEST']));
					if ($arRecurTypeTest = $rsRecurTypeTest->GetNext())
						$arSection['UF_RECUR_TYPE_TEST'] = $arRecurTypeTest['XML_ID'];
					
					$rsRecurType = CUserFieldEnum::GetList(array(), array("ID" => $arSection['UF_RECUR_TYPE']));
					if ($arRecurType = $rsRecurType->GetNext())
						$arSection['UF_RECUR_TYPE'] = $arRecurType['XML_ID'];
		
					$arFields['PRODUCT_NAME'] = $arSection['NAME'];
					
					if (intval($arSection['UF_RECUR_LENGTH_TEST']) > 0)
					{
						$arFields['ACTIVE_TO'] = CAppforsaleRecurring::GetNextDate(array(
							"RECUR_SCHEME_TYPE" => $arSection['UF_RECUR_TYPE_TEST'],
							"RECUR_SCHEME_LENGTH" => $arSection['UF_RECUR_LENGTH_TEST']
						));
						
						if (intval($arSection['UF_RECUR_PRICE_TEST']) > 0)
						{
							return CAppforsaleUserAccount::Pay($USER->GetID(), CAppforsaleRecurring::ratio($arFields['PREVIEW_TEXT'], $arSection['UF_RECUR_PRICE_TEST']));
						}
						return true;
					}
					else if (intval($arSection['UF_RECUR_LENGTH']) > 0 && intval($arSection['UF_RECUR_PRICE']) > 0)
					{
						$arFields['ACTIVE_TO'] = CAppforsaleRecurring::GetNextDate(array(
							"RECUR_SCHEME_TYPE" => $arSection['UF_RECUR_TYPE'],
							"RECUR_SCHEME_LENGTH" => $arSection['UF_RECUR_LENGTH']
						));
		
						return CAppforsaleUserAccount::Pay($USER->GetID(), CAppforsaleRecurring::ratio($arFields['PREVIEW_TEXT'], $arSection['UF_RECUR_PRICE']));
					}
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	
	function OnAfterIBlockElementAdd($arFields)
	{
		global $USER;
		$arIBlock = CIBlock::GetArrayByID($arFields['IBLOCK_ID']);
		if (ToLower($arIBlock['CODE']) == 'profile' 
				&& intval($arFields["ID"]) > 0 && !empty($arFields['ACTIVE_TO']) 
				&& CModule::IncludeModule('mlab.appforsale'))
		{
			if (COption::GetOptionString('mlab.appforsale', 'monetization_use', 'Y') == 'Y' &&
				intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_PRICE', 0)) == 0)
			{
				CAppforsaleRecurring::Add(array(
					"USER_ID" => $USER->GetID(),
					"MODULE" => "mlab.appforsale",
					"PRODUCT_ID" => $arFields["ID"],
					"NEXT_DATE" => $arFields['ACTIVE_TO'],
					"PRODUCT_NAME" => $arFields['PRODUCT_NAME'],
					"REMAINING_ATTEMPTS" => (Defined("APPFORSALE_PROC_REC_ATTEMPTS") ? APPFORSALE_PROC_REC_ATTEMPTS : 3),
					"SUCCESS_PAYMENT" => "Y"
				));
			}
		}
	}
	
	function pluralForm($n, $form1, $form2, $form5) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
	
	function ratio($previewText, $price)
	{
		if (!empty($previewText))
		{
			$arCity = CIBlockProperty::GetPropertyArray('CITY_ID');
			if ($arCity)
			{
				$subscribe = unserialize($previewText);
				$value = $subscribe[$arCity['ORIG_ID']];
				if ($value)
				{
					$result = 0;
					$dbElement = CIBlockElement::GetList(array(), array('ID' => $value), false, false, array('ID', 'PROPERTY_RATIO'));
					while($arElement = $dbElement->GetNext())
					{
						if ($arElement['PROPERTY_RATIO_VALUE'] > 0)
						{
							$result += $price * $arElement['PROPERTY_RATIO_VALUE'];
						}
					}

					return ($result > 0 ? $result : $price);
				}
			}
		}

		return $price;
	}
}
?>