<?
$module_id = "mlab.appforsale";

IncludeModuleLangFile(__FILE__);

if(!$USER->IsAdmin())
	return;

if (isset($_POST['save']) || isset($_POST['apply']))
{
	COption::SetOptionString($module_id, "google_push_key", $_POST['google_push_key']);
	COption::SetOptionString($module_id, "url_sms_service", $_POST['url_sms_service']);
	COption::SetOptionString($module_id, "google_analytics_key", $_POST['google_analytics_key']);
	COption::SetOptionString($module_id, "multiprofiles", ($_POST['multiprofiles'] == 'Y' ? 'Y' : 'N'));
	COption::SetOptionString($module_id, "demo", ($_POST['demo'] == 'Y' ? 'Y' : 'N'));
	COption::SetOptionString($module_id, "shopId", $_POST['shopId']);
	COption::SetOptionString($module_id, "scid", $_POST['scid']);
	COption::SetOptionString($module_id, "client_id", $_POST['client_id']);
	COption::SetOptionString($module_id, "shopPassword", $_POST['shopPassword']);
	
	COption::SetOptionInt($module_id, "starting_balance", intval($_POST['starting_balance']));
	COption::SetOptionString($module_id, "format_string", $_POST['format_string']);
	
	COption::SetOptionString($module_id, "receiver", $_POST['receiver']);
	COption::SetOptionString($module_id, "notification_secret", $_POST['notification_secret']);
	
	COption::SetOptionString($module_id, "bonus_balance", $_POST['bonus_balance']);
	COption::SetOptionString($module_id, "bonus_type_balance", $_POST['bonus_type_balance']);
	
	COption::SetOptionString($module_id, "invite_count", $_POST['invite_count']);
	COption::SetOptionString($module_id, "invite_sum", $_POST['invite_sum']);
	
	COption::SetOptionString($module_id, "login_mask", preg_replace('/[^0-9]/', '', $_POST['login_mask']));
	COption::SetOptionString($module_id, "login_approved", preg_replace('/[^0-9]/', '', $_POST['login_approved']));

	COption::SetOptionString($module_id, "monetization_use", ($_POST['monetization_use'] == 'Y' ? 'Y' : 'N'));
	COption::SetOptionInt($module_id, "monetization_access", intval($_POST['monetization_access']));

	COption::SetOptionString($module_id, "RECUR_TYPE_TEST", $_POST['RECUR_TYPE_TEST']);
	COption::SetOptionInt($module_id, "RECUR_LENGTH_TEST", intval($_POST['RECUR_LENGTH_TEST']));
	COption::SetOptionInt($module_id, "RECUR_PRICE_TEST", intval($_POST['RECUR_PRICE_TEST']));
	
	COption::SetOptionString($module_id, "RECUR_TYPE", $_POST['RECUR_TYPE']);
	COption::SetOptionInt($module_id, "RECUR_LENGTH", intval($_POST['RECUR_LENGTH']));
	COption::SetOptionInt($module_id, "RECUR_PRICE", intval($_POST['RECUR_PRICE']));
	
	COption::SetOptionString($module_id, "moderation_task", $_POST['moderation_task']);


	COption::SetOptionString($module_id, "REGISTER_PUSH", $_POST['REGISTER_PUSH']);
	COption::SetOptionString($module_id, "MODERATE_PUSH", $_POST['MODERATE_PUSH']);
}

$aTabs = array(
	array(
		"DIV" => "push",
		"TAB" => GetMessage('NOTIFICATION'),
		"TITLE" => GetMessage("NOTIFICATION")
	),
	array(
		"DIV" => "yandex_money",
		"TAB" => GetMessage('APPFORSALE_YANDEX_MONEY_TAB'),
		"TITLE" => GetMessage("APPFORSALE_YANDEX_MONEY_TITLE")
	),
	array(
		"DIV" => "yandex_kassa",
		"TAB" => GetMessage('APPFORSALE_YANDEX_KASSA_TAB'),
		"TITLE" => GetMessage("APPFORSALE_YANDEX_KASSA_TITLE")
	),
	array(
		"DIV" => "other",
		"TAB" => GetMessage('OTHER'),
		"TITLE" => GetMessage("OTHER")
	),
	array(
		'DIV' => 'login',
		'TAB' => GetMessage('APPFORSALE_LOGIN_TAB'),
		'TITLE' => GetMessage('APPFORSALE_LOGIN_TITLE')
	),
	array(
		'DIV' => 'monetization',
		'TAB' => GetMessage('APPFORSALE_MONETIZATION_TAB'),
		'TITLE' => GetMessage('APPFORSALE_MONETIZATION_TITLE')
	),
	array(
		'DIV' => 'push_text',
		'TAB' => GetMessage('APPFORSALE_PUSH_TEXT_TAB'),
		'TITLE' => GetMessage('APPFORSALE_PUSH_TEXT_TITLE')
	)
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);
$tabControl->Begin();
echo '<form method="POST" action="'.$APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mid='.$module_id.'">';
$tabControl->BeginNextTab();
?>
<tr class="heading"><td colspan="2"><?=GetMessage('CONFIGURE_PUSH_NOTIFICATION')?></td></tr>
<tr><td width="40%"><?=GetMessage("STATUS")?>:</td><td width="60%">
<?
$google_push_key = COption::GetOptionString($module_id, "google_push_key", "");
if ($google_push_key)
{
	$request = new Bitrix\Main\Web\HttpClient();
	$request->setHeader("Authorization", "key=".COption::GetOptionString($module_id, "google_push_key", ""));
	$request->setHeader("Content-Type", "application/json");
	$request->post("https://fcm.googleapis.com/fcm/send", '{"registration_ids":["ABC"]}');
	if ($request->getStatus() == 401)
	{
		echo '<span style="color:red">' . GetMessage('ERROR_KEY') . '</span>';
	}
	else
	{
		echo '<span style="color:green; font-weight: bold">' . GetMessage('ACTIVE') . '</span>';
	}
}
else
{
	echo '<span style="color:gray">' . GetMessage('ERROR_KEY') . '</span>';
}
?>
</td></tr>

<tr>
	<td width="40%"><?=GetMessage('API_KEY')?>:</td>
	<td width="60%"><input name="google_push_key" type="text" size="40" value="<?=COption::GetOptionString($module_id, "google_push_key", "")?>" /></td>
</tr>

<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage("GOOGLE_INFO")?></td>
</tr>


<tr class="heading"><td colspan="2"><?=GetMessage('CONFIGURE_SMS_NOTIFICATION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('URL')?>:</td>
	<td width="60%"><input name="url_sms_service" type="text" size="40" value="<?=COption::GetOptionString($module_id, "url_sms_service", "")?>" /></td>
</tr>

<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage("SMS_INFO")?></td>
</tr>

<?$tabControl->BeginNextTab();?>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER_ALT')?></div></td>
	<td width="60%"><input name="receiver" type="text" value="<?=COption::GetOptionString($module_id, "receiver", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER_INFO')?></td>
</tr>

<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_CLIENT_ID')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_MONEY_CLIENT_ID_ALT')?></div></td>
	<td width="60%"><input name="client_id" type="text" value="<?=COption::GetOptionString($module_id, "client_id", "")?>" size="30" /></td>
</tr>


<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_ALT')?></div></td>
	<td width="60%"><input name="notification_secret" type="text" value="<?=COption::GetOptionString($module_id, "notification_secret", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_INFO')?></td>
</tr>
<?$tabControl->BeginNextTab();?>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_INFO')?><br /><br />
	<input id="https_check_button" type="button" value="<?=GetMessage('APPFORSALE_YANDEX_CHECK')?>" title="<?=GetMessage('APPFORSALE_YANDEX_CHECK_ALT')?>" onclick="
		var checkHTTPS = function(){
			BX.showWait()
			var postData = {
				action: 'checkHttps',
				https_check: 'Y',
				lang: BX.message('LANGUAGE_ID'),
				sessid: BX.bitrix_sessid()
			};

			BX.ajax({
				timeout: 30,
				method: 'POST',
				dataType: 'json',
				url: '/bitrix/admin/appforsale_pay_system_ajax.php',
				data: postData,

				onsuccess: function (result)
				{
					BX.closeWait();
					BX.removeClass(BX('https_check_result'), 'https_check_success');
					BX.removeClass(BX('https_check_result'), 'https_check_fail');

					BX('https_check_result').innerHTML = '&nbsp;' + result.CHECK_MESSAGE;
					if (result.CHECK_STATUS == 'OK')
						BX.addClass(BX('https_check_result'), 'https_check_success');
					else
						BX.addClass(BX('https_check_result'), 'https_check_fail');
				},
				onfailure : function()
				{
					BX.closeWait();
					BX.removeClass(BX('https_check_result'), 'https_check_success');

					BX('https_check_result').innerHTML = '<?=GetMessage('APPFORSALE_YANDEX_ERROR')?>';
					BX.addClass(BX('https_check_result'), 'https_check_fail');
				}
			});
		};
		checkHTTPS();">
		<span id="https_check_result"></span>
	
	</td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_ADDITIONAL')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_DEMO')?></td>
	<td width="60%"><input name="demo" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "demo", "Y") == 'Y' ? ' checked' : '')?> /></td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPID')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPID_ALT')?></div></td>
	<td width="60%"><input name="shopId" type="text" value="<?=COption::GetOptionString($module_id, "shopId", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SCID')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SCID_ALT')?></div></td>
	<td width="60%"><input name="scid" type="text" value="<?=COption::GetOptionString($module_id, "scid", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPPASSWORD')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPPASSWORD_ALT')?></div></td>
	<td width="60%"><input name="shopPassword" type="text" value="<?=COption::GetOptionString($module_id, "shopPassword", "")?>" size="30" /></td>
</tr>
<?$tabControl->BeginNextTab()?>
<tr>
	<td width="40%"><?=GetMessage('GOOGLE_ANALYTICS_KEY')?>:</td>
	<td width="60%"><input name="google_analytics_key" type="text" size="40" value="<?=COption::GetOptionString($module_id, "google_analytics_key", "")?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('MULTIPROFILES')?>:</td>
	<td width="60%"><input name="multiprofiles" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "multiprofiles", "N") == 'Y' ? ' checked' : '')?> /></td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_OTHER_HEADING_USER')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_OTHER_FORMAT_STRING')?>:</td>
	<td width="60%"><input name="format_string" type="text" size="40" value="<?=COption::GetOptionString($module_id, "format_string", "# ".GetMessage('APPFORSALE_RUB'))?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_OTHER_CREDIT')?>:</td>
	<td width="60%"><input name="starting_balance" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "starting_balance", 0)?>" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_OTHER_CREDIT_ALT')?></td>
</tr>

<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_BONUS')?>:</td>
	<td width="60%"><input name="bonus_balance" type="text" size="5" value="<?=COption::GetOptionInt($module_id, "bonus_balance", 0)?>" /> <select name="bonus_type_balance"><option value="0"<?=(COption::GetOptionInt($module_id, "bonus_type_balance", 0) == 0 ? ' selected="selected"' : '')?>><?=GetMessage('APPFORSALE_BONUS_RUB')?></option><option value="1"<?=(COption::GetOptionInt($module_id, "bonus_type_balance", 0) == 1 ? ' selected="selected"' : '')?>><?=GetMessage('APPFORSALE_BONUS_PERCENT')?></option></select></td>
</tr>


<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_OTHER_HEADING_INVITE')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_INVITE_COUNT')?>:</td>
	<td width="60%"><input name="invite_count" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "invite_count", 0)?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_INVITE_SUMM')?>:</td>
	<td width="60%"><input name="invite_sum" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "invite_sum", 150)?>" /></td>
</tr>


<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_MODERATION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MODERATION_TASK')?>:</td>
	<td width="60%"><input name="moderation_task" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "moderation_task", "N") == 'Y' ? ' checked' : '')?> /></td>
</tr>
<?$tabControl->BeginNextTab()?>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_LOGIN_MASK')?>:</td>
	<td width="60%"><input name="login_mask" type="text" size="40" value="<?=COption::GetOptionString($module_id, "login_mask", '79999999999')?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_LOGIN_APPROVED')?>:</td>
	<td width="60%"><input name="login_approved" type="text" size="40" value="<?=COption::GetOptionString($module_id, "login_approved", '79270270273')?>" /></td>
</tr>
<?$tabControl->BeginNextTab()?>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_USE')?></td>
	<td width="60%"><input name="monetization_use" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "monetization_use", "Y") == 'Y' ? ' checked' : '')?> /></td>
</tr>

<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_TEST')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_TYPE')?>:</td>
	<td width="60%">
		<select name="RECUR_TYPE_TEST">
			<option value="H"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'H' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_H')?></option>
			<option value="D"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'D' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_D')?></option>
			<option value="W"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'W' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_W')?></option>
			<option value="M"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'M' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_M')?></option>
			<option value="Q"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'Q' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_Q')?></option>
			<option value="S"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'S' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_S')?></option>
			<option value="Y"<?=COption::GetOptionString($module_id, "RECUR_TYPE_TEST", 'M') == 'Y' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_Y')?></option>
		</select>
	</td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_LENGTH')?>:</td>
	<td width="60%"><input name="RECUR_LENGTH_TEST" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "RECUR_LENGTH_TEST", 0)?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_PRICE')?>:</td>
	<td width="60%"><input name="RECUR_PRICE_TEST" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "RECUR_PRICE_TEST", 0)?>" /></td>
</tr>


<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_TYPE')?>:</td>
	<td width="60%">
		<select name="RECUR_TYPE">
			<option value="H"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'H' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_H')?></option>
			<option value="D"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'D' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_D')?></option>
			<option value="W"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'W' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_W')?></option>
			<option value="M"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'M' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_M')?></option>
			<option value="Q"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'Q' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_Q')?></option>
			<option value="S"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'S' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_S')?></option>
			<option value="Y"<?=COption::GetOptionString($module_id, "RECUR_TYPE", 'M') == 'Y' ? ' selected="selected"' : ''?>><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_Y')?></option>
		</select>
	</td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_LENGTH')?>:</td>
	<td width="60%"><input name="RECUR_LENGTH" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "RECUR_LENGTH", 0)?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_MONETIZATION_RECUR_PRICE')?>:</td>
	<td width="60%"><input name="RECUR_PRICE" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "RECUR_PRICE", 0)?>" /></td>
</tr>

<?$tabControl->BeginNextTab()?>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_PUSH_TEXT_MODERATE_HEAD')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_PUSH_TEXT_REGISTER')?>:</td>
	<td width="60%">
		<textarea name="REGISTER_PUSH" type="text" cols="50" rows="8"><?=COption::GetOptionString($module_id, "REGISTER_PUSH", "")?></textarea>
	</td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_PUSH_TEXT_MODERATE')?>:</td>
	<td width="60%">
		<textarea name="MODERATE_PUSH" type="text" cols="50" rows="8"><?=COption::GetOptionString($module_id, "MODERATE_PUSH", "")?></textarea>
	</td>
</tr>


<?
$tabControl->Buttons(array());
$tabControl->End();
?>
</form>