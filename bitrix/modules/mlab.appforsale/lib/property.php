<?
namespace Mlab\Appforsale;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Property
{
	// OnBeforeIBlockPropertyAdd
	// OnBeforeIBlockPropertyUpdate
	function OnBeforeIBlockProperty($arFields)
	{
		$code = ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE'));
		if (in_array($code, array('task', 'profile', 'offer', 'comment')))
		{
			if ($arFields['IS_REQUIRED'] == 'Y')
			{
				global $APPLICATION;
				$APPLICATION->throwException(Loc::getMessage('PROPERTY_IS_REQUIRED_EXCEPTION'));
				return false;
			}
			
		}
	}
}
?>