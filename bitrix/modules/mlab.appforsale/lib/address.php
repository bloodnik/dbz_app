<?
namespace Mlab\Appforsale;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Address
{	
	function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'address',
			'DESCRIPTION' => Loc::getMessage('ADDRESS_DESCRIPTION'),
			'GetPropertyFieldHtml' => array('Mlab\Appforsale\Address', 'GetPropertyFieldHtml'),
			'PrepareSettings' => array('Mlab\Appforsale\Address', 'PrepareSettings'),
			'GetSettingsHTML' => array('Mlab\Appforsale\Address', 'GetSettingsHTML'),
 			'ConvertToDB' => array('Mlab\Appforsale\Address', 'ConvertToDB'),
			'ConvertFromDB' => array('Mlab\Appforsale\Address', 'ConvertFromDB'),
			'GetPublicViewHTML' => array('Mlab\Appforsale\Address', 'GetPublicViewHTML'),	
			'GetPublicEditHTML' => array('Mlab\Appforsale\Address', 'GetPublicEditHTML')
		);
	}
	
	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{		
		$arValue = explode('|', $value['VALUE']);
		
		if ($arProperty['MULTIPLE'] == 'Y' && isset($GLOBALS['ADDRESS_PROPERTY'][$arProperty['ID']]))
		{
			if ($value['VALUE'] != '')
			{
			?><script>tt.add({value: '<?=$value['VALUE']?>', name: '<?=$arValue[0]?>', loc: '<?=$arValue[1]?>'});</script><?
			}
		}
		else
		{
			$GLOBALS['ADDRESS_PROPERTY'][$arProperty['ID']] = true;

			global $APPLICATION;
			ob_start();
			$APPLICATION->IncludeComponent(
				'mlab:appforsale.interface.address',
				'',
				array(
					'ID' => $arProperty['ID'],
					'MULTIPLE' => ($arProperty['MULTIPLE'] == 'Y' ?: 'N')
				),
				false
			);
if ($value['VALUE'] != '')
{
?><script>tt.add({value: '<?=$value['VALUE']?>', name: '<?=$arValue[0]?>', loc: '<?=$arValue[1]?>'});</script><?
}
$resultHtml = ob_get_contents();
			ob_end_clean();
			return $resultHtml;
		}
	}
	
// 	function GetPublicEditHTMLMulty($property, $value, $controlSettings)
// 	{
// 		return '1223';
// 	}

	
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
// 		global $APPLICATION;
		ob_start();
		?>
		
		<input type="text" size="20" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>" />
		
		<?
		$content = ob_get_contents();
		ob_end_clean();

// // 		if ($arProperty['MULTIPLE'] == 'Y')
// // 			$googleMapLastNumber++;
		
		return $content;
		//return '<input type="text" size="20" name="'.$strHTMLControlName['VALUE'].'" value="'.$value['VALUE'].'" />';
	}
// 		function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
// 		{
// 	// 		$arCollections = array(
// 	// 				array(
// 	// 						"ID" => "N",
// 	// 						"NAME" => Loc::getMessage('STATUS_N')
// 	// 				),
// 	// 				array(
// 	// 						"ID" => "F",
// 	// 						"NAME" => Loc::getMessage('STATUS_F')
// 	// 				)
// 	// 		);
	
// 	// 					$html = "<select name=\"".$strHTMLControlName["VALUE"]."\">";
	
// 	// 					foreach($arCollections as $code=>$vl) {
	
// 	// 						if ($vl["ID"]==$value["VALUE"])
// 		// 							$html .=  "<option selected value=\"".$vl["ID"]."\">".$vl["NAME"]."</option>";
// 		// 						else
// 			// 							$html .=  "<option  value=\"".$vl["ID"]."\">".$vl["NAME"]."</option>";
// 			// 					}
	
// 			// 					$html .=  "</select>";
// 			// 					return $html;
// 			return '123';
// 							}
	
// 							function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
// 							{
// 			// 					$arCollections = array(
// 					// 						"" => Loc::getMessage('STATUS_N'),
// 					// 						"N" => Loc::getMessage('STATUS_N'),
// 					// 						"F" => Loc::getMessage('STATUS_F')
// 			// 					);
	
// 	// 					return $arCollections[$value['VALUE']];
// 	return '123';
// 	}

	public static function ConvertToDB($arProperty, $value)
	{
		return $value;
	}

	public static function ConvertFromDB($arProperty, $value, $format = '')
	{		
		return $value;
	}
	
	function PrepareSettings($arProperty)
	{
		return array(
				'MULTIPLE_CNT' => '1',
// 				'USER_TYPE_SETTINGS' => array(
// 					'API_KEY' => isset($arProperty['USER_TYPE_SETTINGS']['API_KEY']) ? $arProperty['USER_TYPE_SETTINGS']['API_KEY'] : ''
// 				)
		);
	}
	
	function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
	{
		$arPropertyFields = array(
			'HIDE' => array('ROW_COUNT', 'COL_COUNT', 'MULTIPLE_CNT')
		);

		return '';
// 		$settings = $arProperty['USER_TYPE_SETTINGS'];
// 		//$apiKey = isset($settings['API_KEY']) ? htmlspecialcharsbx($settings['API_KEY']) : '';

// 		return '
//             <tr>
//                 <td>'.Loc::getMessage('IBLOCK_PROP_AFS_MAP_API_KEY').':</td>
//                 <td>
//                     <input type="text" size="40" name="'.$strHTMLControlName['NAME'].'[API_KEY]" value="'.$apiKey.'">
//                 </td>
//             </tr>';
	}
	



// 	function GetPublicViewHTMLMulty($arProperty, $value, $strHTMLControlName)
// 	{
		
// 		return '111111';
// 	}
	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		global $APPLICATION;
		
		static $bReturn = array();
		
		if (!isset($bReturn[$arProperty['ID']]))
		{
			if (!is_array($arProperty['VALUE']))
				$arProperty['VALUE'] = array($arProperty['VALUE']);

			$strResult = '';
			
			if ($arProperty['FULL'] == 'Y')
			{
				$objects = array();
				$count = count($arProperty['VALUE']);
				//$bText = $count > 1;
				$index = 0;

				$mess = array('A','B','C','D','E','F','G','H','I','J');
				
				foreach ($arProperty['VALUE'] as $value)
				{
					$arValue = explode('|', $value);
					
					$strResult.= '<div style="padding-top: 4px;">';
					
					//if ($bText)
					//{
						$strResult.= '<span style="display: inline-block; color: #fff; border-radius: 50%; width: 20px; height: 20px; line-height: 20px; font-size: 11px; text-align: center; ';
						if ($index == 0)
						{
							$strResult.= ' background: #F23D30';
						}
						else if ($index == $count - 1)
						{
							$strResult.= ' background: #4296EA';
						}
						else
						{
							$strResult.= ' background: #71B732';
						}
						$strResult.= '">'.($mess[$index] ?: '●').'</span> ';
					//}
					$strResult.= $arValue[0].'</div>';

					$objects[] = array(
						explode(',', $arValue[1]),
						array(
							'balloonContent' => $arValue[0]
						)
					);
					$index++;
				}
				
				ob_start();
				
				$APPLICATION->IncludeComponent(
					'mlab:appforsale.map.system',
					'',
					array(
						'OBJECTS' => $objects,
						'ROUTE' => 'Y'
					),
					false
				);
				
				$strResult = '<div style="padding-top: 4px; padding-bottom: 4px;">'.ob_get_contents().'</div>'.$strResult;
				ob_end_clean();	
			}
			else
			{
				foreach ($arProperty['VALUE'] as $value)
				{
					if (strlen($strResult) > 0)
						$strResult.= ' > ';
					$arValue = explode('|', $value);
					$strResult.= $arValue[0];
				}
			}
			
			$bReturn[$arProperty['ID']] = true;
			
			return $strResult;
		}
	}
				
				
	
}
?>