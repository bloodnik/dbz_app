<?

namespace Mlab\Appforsale;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Status {
	function GetUserTypeDescription() {
		return array(
			'PROPERTY_TYPE'        => 'S',
			"USER_TYPE"            => "status",
			"DESCRIPTION"          => Loc::getMessage('STATUS_DESCRIPTION'),
			"GetPropertyFieldHtml" => array("Mlab\Appforsale\Status", "GetPropertyFieldHtml"),
			"GetAdminListViewHTML" => array("Mlab\Appforsale\Status", "GetAdminListViewHTML"),
			"GetSettingsHTML"      => array("Mlab\Appforsale\Status", "GetSettingsHTML"),
			"GetPublicViewHTML"    => array("Mlab\Appforsale\Status", "GetPublicViewHTML")
		);
	}

	function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields) {
		$arPropertyFields = array(
			"HIDE" => array("MULTIPLE"),
			"SET"  => array("MULTIPLE" => "N"),
		);

		return '';
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
		$arCollections = array(
			array(
				"ID"   => "N",
				"NAME" => Loc::getMessage('STATUS_N')
			),
			array(
				"ID"   => "P",
				"NAME" => Loc::getMessage('STATUS_P')
			),
			array(
				"ID"   => "D",
				"NAME" => Loc::getMessage('STATUS_D')
			),
			array(
				"ID"   => "F",
				"NAME" => Loc::getMessage('STATUS_F')
			),
			array(
				"ID"   => "E",
				"NAME" => Loc::getMessage('STATUS_E')
			)
		);

		$html = "<select name=\"" . $strHTMLControlName["VALUE"] . "\">";

		foreach ($arCollections as $code => $vl) {

			if ($vl["ID"] == $value["VALUE"]) {
				$html .= "<option selected value=\"" . $vl["ID"] . "\">" . $vl["NAME"] . "</option>";
			} else {
				$html .= "<option  value=\"" . $vl["ID"] . "\">" . $vl["NAME"] . "</option>";
			}
		}

		$html .= "</select>";

		return $html;
	}

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
		$arCollections = array(
			""  => Loc::getMessage('STATUS_N'),
			"N" => Loc::getMessage('STATUS_N'),
			"P" => Loc::getMessage('STATUS_P'),
			"D" => Loc::getMessage('STATUS_D'),
			"F" => Loc::getMessage('STATUS_F'),
			"E" => Loc::getMessage('STATUS_E')
		);

		return $arCollections[ $value['VALUE'] ];
	}

	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName) {
		$arCollections = array(
			''  => Loc::getMessage('STATUS_N'),
			'N' => Loc::getMessage('STATUS_N'),
			'P' => Loc::getMessage('STATUS_P'),
			'D' => Loc::getMessage('STATUS_D'),
			'F' => Loc::getMessage('STATUS_F'),
			'E' => Loc::getMessage('STATUS_E')
		);

		return $arCollections[ $value['VALUE'] ];
	}


}

?>