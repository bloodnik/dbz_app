<?
namespace Mlab\Appforsale;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Iblock
{	
	function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'iblock',
			'DESCRIPTION' => Loc::getMessage('IBLOCK_DESCRIPTION'),
			'GetPublicEditHTML' => array('Mlab\Appforsale\Iblock', 'GetPublicEditHTML'),
			'GetPropertyFieldHtml' => array('Mlab\Appforsale\Iblock', 'GetPropertyFieldHtml'),
			'PrepareSettings' => array('Mlab\Appforsale\Iblock', 'PrepareSettings'),
			'GetSettingsHTML' => array('Mlab\Appforsale\Iblock', 'GetSettingsHTML'),
			'GetPublicViewHTML' => array('Mlab\Appforsale\Iblock', 'GetPublicViewHTML'),
			'GetAdminListViewHTML' => array('Mlab\Appforsale\Iblock', 'GetAdminListViewHTML')
		);
	}
	
	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		$val = Loc::getMessage('SELECT');
		if (intval($value['VALUE']) > 0)
		{
			if(Loader::includeModule('iblock'))
			{
				$dbElement = \CIBlockElement::GetByID($value['VALUE']);
				if($arElement = $dbElement->GetNext())
				{
					$val = $arElement['NAME'];
				}
			}
		}
		else
		{
			if ($arProperty['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID'] == 'locations')
			{
				global $USER;
				$dbUser = $USER->GetByID($USER->GetID());
				if ($arUser = $dbUser->Fetch())
				{
					$dbElement = \CIBlockElement::GetByID($arUser['UF_CITY']);
					if($arElement = $dbElement->GetNext())
					{
						$value['VALUE'] = $arElement['ID'];
						$val = $arElement['NAME'];
					}
				}
			}
		}
		
		ob_start();
		?>
			<input type="hidden" id="address_<?=$arProperty['ID']?>_value" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>">
			<span class="btn btn-default btn-block" id="address_<?=$arProperty['ID']?>_name" onclick="showE(<?=$arProperty['ID']?>, 0)"><?=$val?></span>
		<?
		$resultHtml = ob_get_contents();
		ob_end_clean();
		return $resultHtml;
	}
	
	function GetSettingsHTML($arUserField, $arHtmlControl, $bVarsFromForm)
	{	
		if($bVarsFromForm)
		{
            $iblock_id = $GLOBALS[$arHtmlControl["NAME"]]["IBLOCK_ID"];
            $created_by = $GLOBALS[$arHtmlControl["NAME"]]["CREATED_BY"];
		}
		elseif(is_array($arUserField))
		{
			$iblock_id = $arUserField["USER_TYPE_SETTINGS"]["IBLOCK_ID"];
			$created_by = $arUserField["USER_TYPE_SETTINGS"]["CREATED_BY"];
		}
        else
        {
            $iblock_id = '';
            $created_by = '';
        }
        
		return '<tr>
					<td>'.GetMessage('IBLOCK').':</td>
                	<td>
                    '.GetIBlockDropDownList($iblock_id, $arHtmlControl["NAME"].'[IBLOCK_TYPE_ID]', $arHtmlControl["NAME"].'[IBLOCK_ID]', false, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"').'
               		 </td>
            </tr>
			
			<tr>
					<td>'.GetMessage('CREATED_BY').':</td>
                	<td>
				<input type="checkbox" name="PROPERTY_USER_TYPE_SETTINGS[CREATED_BY]" id="PROPERTY_USER_TYPE_SETTINGS[CREATED_BY]" value="Y" class="adm-designed-checkbox" '.($created_by == 'Y' ? 'checked="checked"' :'').'>
			<label class="adm-designed-checkbox-label" for="PROPERTY_USER_TYPE_SETTINGS[CREATED_BY]" title=""></label>
				</td>
            </tr>
			
			
            ';
	}
		
	
	function PrepareSettings($arProperty)
	{
		return $arProperty;
	}
	
	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		if(Loader::includeModule('iblock'))
		{
			$dbElement = \CIBlockElement::GetByID($value['VALUE']);
			if($arElement = $dbElement->GetNext())
			{
				$strResult = '';
				$dbSection = \CIBlockSection::GetNavChain($arElement['IBLOCK_ID'], $arElement['IBLOCK_SECTION_ID'], array('NAME'));
				while ($arSection = $dbSection->GetNext())
				{
					if (strlen($strResult) > 0)
						$strResult .= ' ';
					$strResult .= $arSection['NAME'];
				}
				if (strlen($strResult) > 0)
					$strResult .= ' ';
				$strResult .= $arElement['NAME'];
				return $strResult;
			}
		}
		return $value['VALUE'];
	}
	
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		ob_start();
		if (intval($value['VALUE']) > 0)
		{
			if(Loader::includeModule('iblock'))
			{
				$dbElement = \CIBlockElement::GetByID($value['VALUE']);
				if($arElement = $dbElement->GetNext())
				{
					$val = $arElement['NAME'];
				}
			}
		}
		?>
		<input type="hidden" size="20" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>" />
		<span><?=$val?></span>
		<?
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
	
	public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName)
	{
		$strResult = '';
		if (intval($arValue['VALUE']) > 0)
		{
			if(Loader::includeModule('iblock'))
			{
				$dbElement = \CIBlockElement::GetByID($arValue['VALUE']);
				if($arElement = $dbElement->GetNext())
				{
					$strResult = $arElement['NAME'];
				}
			}
		}
		return $strResult;
	}
}
?>