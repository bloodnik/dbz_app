<? 
namespace Mlab\Appforsale;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Comment
{
	function OnAfterUserDelete($user_id)
	{
		if(Loader::includeModule('iblock'))
		{
			$dbElement = \CIBlockElement::GetList(
					array(),
					array(
							'IBLOCK_TYPE' => 'comments',
							'IBLOCK_CODE' => 'comment',
							'CREATED_BY' => $user_id
					),
					false,
					false,
					array(
							'ID'
					)
			);
			while($arElement = $dbElement->GetNext())
			{
				\CIBlockElement::Delete($arElement['ID']);
			}
		}
	}
	
	function OnBeforeIBlockElementAdd($arFields)
	{
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'comment')
		{
			global $APPLICATION, $USER;
			$dbElement = \CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $arFields['IBLOCK_ID'],
					'CREATED_BY' => $USER->GetID(),
					'PROPERTY_TASK_ID' => $arFields['PROPERTY_VALUES']['TASK_ID'],
					'PROPERTY_PROFILE_ID' => $arFields['PROPERTY_VALUES']['PROFILE_ID']
				)
			);
			if ($arElement = $dbElement->GetNext())
			{
				$APPLICATION->throwException(Loc::getMessage('ERROR'));
				return false;
			}
		}
	}
	
	function OnAfterIBlockElementAdd($arFields)
	{
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'comment')
		{
			global $USER_FIELD_MANAGER;
			$dbElement = \CIBlockElement::GetList(
					array(),
					array(
							'IBLOCK_CODE' => 'comment',
							'PROPERTY_PROFILE_ID' => $arFields['PROPERTY_VALUES']['PROFILE_ID']
					),
					false,
					false,
					array(
							'PROPERTY_RATING'
					)
			);
			
			$rating_count = 0;
			$rating_sum = 0;
			while ($arElement = $dbElement->GetNext())
			{
				$rating_sum += (int) $arElement['PROPERTY_RATING_VALUE'];
				$rating_count++;
			}
				
			if ($rating_count > 0 && $rating_sum > 0)
			{
				$USER_FIELD_MANAGER->Update('USER', $arFields['PROPERTY_VALUES']['PROFILE_ID'], array(
						'UF_RATING' => ceil($rating_sum / $rating_count)
				));
			}
		}
	}
}
?>