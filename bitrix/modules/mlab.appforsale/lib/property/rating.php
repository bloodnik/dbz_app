<?
namespace Mlab\Appforsale\Property;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Rating
{
	function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'rating',
			'DESCRIPTION' => Loc::getMessage('RATING_DESCRIPTION'),
			'GetPublicEditHTML' => array('Mlab\Appforsale\Property\Rating', 'GetPublicEditHTML'),
			'GetPropertyFieldHtml' => array('Mlab\Appforsale\Property\Rating', 'GetPropertyFieldHtml'),
		);
	}
	
	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		global $APPLICATION;
		
		ob_start();
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.interface.rating',
			'',
			array(
				'arProperty' => $arProperty,
				'value' => $value,
				'strHTMLControlName' => $strHTMLControlName,
			),
			false
		);
		$resultHtml = ob_get_contents();
		ob_end_clean();
		return $resultHtml;
	}
	
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		ob_start();
		?>
		<input type="text" size="20" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>" />
		<?
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}
}
?>