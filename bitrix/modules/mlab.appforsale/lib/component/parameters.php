<?
namespace Mlab\Appforsale\Component;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Parameters
{
	function getIBlock($iblock_type)
	{
		$arResult = array();
		$dbIBlock = \CIBlock::GetList(
			array('SORT' => 'ASC'),
			array('TYPE' => $iblock_type, 'ACTIVE' => 'Y')
		);
		while($arIblock = $dbIBlock->Fetch())
		{
			$arResult[$arIblock['ID']] = '['.$arIblock['ID'].'] '.$arIblock['NAME'];
		}
		return $arResult;
	}
	
	public static function getIblockElementProperties($iblock_id)
	{
		$arResult = array();
// 		if (!isset(self::$elementPropertyCache[$iblock_id]))
// 		{
// 			self::$elementPropertyCache[$iblock_id] = array();
			$dbProperty = \CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iblock_id));
			while ($arProperty = $dbProperty->fetch())
			{
				$arResult[$arProperty['ID']] = '['.$arProperty['ID'].'] '.$arProperty['NAME'];
// 				self::$elementPropertyCache[$iblock_id][] = $property;
			}
// 		}
		return $arResult;
// 		return self::$elementPropertyCache[$iblock_id];
	}
	
	public static function getIblockElementField()
	{
		$arResult = array();
		$arResult['ACTIVE'] = 'Активность';
		$arResult['DATE_CREATE'] = 'Дата создания';
		return $arResult;
	}
	
	public static function getFieldCode($name, $parent, $options = array())
	{
		$result = array(
			'PARENT' => $parent,
			'NAME' => $name,
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'ADDITIONAL_VALUES' => 'Y',
			'SIZE' => 8,
			'VALUES' => array(
				'ID' => Loc::getMessage('IBLOCK_FIELD_ID'),
				'CODE' => Loc::getMessage('IBLOCK_FIELD_CODE')	
			)
		);
		
		return $result;
	}
}
?>