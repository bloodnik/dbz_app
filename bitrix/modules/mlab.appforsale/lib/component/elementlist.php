<?
namespace Mlab\Appforsale\Component;

abstract class ElementList extends Base
{
	protected $navigation = false;
	
	protected function getAdditionalCacheId()
	{
		global $USER;

		\CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
		
		$this->navParams = array(
				'nPageSize' => 50,
				'bShowAll' => false,
				// nTopCount
		);
		$this->navigation = \CDBResult::GetNavParams($this->navParams);

		return array(
			$USER->GetID(),
			$this->navigation
		);
	}
		
	protected function makeOutputResult()
	{
		parent::makeOutputResult();
		$this->arResult['ITEMS'] = $this->elements;
	}
}
?>