<?
namespace Mlab\Appforsale\Component;

use Bitrix\Main\Loader;

abstract class Element extends Base
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams = parent::onPrepareComponentParams($arParams);
		
		$arParams['SECTION_ID'] = isset($arParams['SECTION_ID']) ? (int) $arParams['SECTION_ID'] : 0;
		$arParams['SECTION_CODE'] = isset($arParams['SECTION_CODE']) ? trim($arParams['SECTION_CODE']) : '';
		
		$arParams['ELEMENT_ID'] = isset($arParams['ELEMENT_ID']) ? (int) $arParams['ELEMENT_ID'] : 0;
		$arParams['ELEMENT_CODE'] = isset($arParams['ELEMENT_CODE']) ? trim($arParams['ELEMENT_CODE']) : '';
		
		return $arParams;
	}
	
	protected function getAdditionalCacheId()
	{
		return array();
	}
	
	protected function makeOutputResult()
	{
		parent::makeOutputResult();
		$this->arResult = array_merge($this->arResult, $this->elements[0]);
	}
	
	public function executeComponent()
	{
		Loader::includeModule('iblock');
	
		$this->sendCounters();
		
		$section_id = \CIBlockFindTools::GetSectionID(
			$this->arParams['SECTION_ID'],
			$this->arParams['SECTION_CODE'],
			array(
				'GLOBAL_ACTIVE' => 'Y',
				'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
			)
		);
		$dbSection = \CIBlockSection::GetNavChain($this->arParams['IBLOCK_ID'], $section_id, array('ID'));
		while ($arSection = $dbSection->Fetch())
		{
			$arUF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$this->arParams['IBLOCK_ID'].'_SECTION', $arSection['ID']);
			foreach ($arUF['UF_DETAIL_PROPERTY']['VALUE'] as $v)
			{
				$this->arParams['PROPERTY_CODE'][] = $v;
			}
		}		
				
		ob_start();
		parent::executeComponent();
		$content = ob_get_contents();
		ob_end_clean();
		echo str_replace(
			array(
				'#DATE_CREATE#',
				'#SHOW_COUNTER#'
			),
			array(
				\CIBlockFormatProperties::DateFormat('x', MakeTimeStamp($this->arResult['DATE_CREATE'], \CSite::GetDateFormat())),
				$this->arResult['SHOW_COUNTER']
			),
			$content
		);
	}
	
	protected function sendCounters()
	{
		\CIBlockElement::CounterInc($this->arParams['ELEMENT_ID']);
	}
	
	protected function getSelect()
	{
		$arSelect = parent::getSelect();
		$arSelect[] = 'SHOW_COUNTER';
		return $arSelect;
	}
	
	protected function getCacheKeys()
	{
		$resultCacheKeys = parent::getCacheKeys();
		
		$this->arResult['META_TAGS'] = array();
		$resultCacheKeys[] = 'META_TAGS';
		
		$this->arResult['META_TAGS']['TITLE'] = $this->arResult['NAME'];
		$this->arResult['META_TAGS']['KEYWORDS'] = str_replace(array('.', ',', ' '), array('', '', ', '), $this->arResult['NAME']);
		$this->arResult['META_TAGS']['DESCRIPTION'] = $this->arResult['NAME'];
		
		return $resultCacheKeys;
	}
	
	protected function initMetaData()
	{
		global $APPLICATION;
		$arResult =& $this->arResult;
	
		$APPLICATION->SetTitle($arResult['META_TAGS']['TITLE']);
		$APPLICATION->SetPageProperty('keywords', $arResult['META_TAGS']['KEYWORDS']);
		$APPLICATION->SetPageProperty('description', $arResult['META_TAGS']['DESCRIPTION']);
	}
}
?>