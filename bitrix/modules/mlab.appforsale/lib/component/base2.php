<?
namespace Mlab\Appforsale\Component;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UserTable,
	Bitrix\Main\Config\Option,
	Mlab\Appforsale\Design;

Loc::loadMessages(__FILE__);

abstract class Base2 extends \CBitrixComponent
{
	protected $elements = array();
	
	public function onPrepareComponentParams($arParams)
	{		
		$arParams['IBLOCK_TYPE'] = isset($arParams['IBLOCK_TYPE']) ? trim($arParams['IBLOCK_TYPE']) : '';
		$arParams['IBLOCK_ID'] = isset($arParams['IBLOCK_ID']) ? (int) $arParams['IBLOCK_ID'] : 0;		

		$arParams['PARENT_SECTION'] = isset($arParams['PARENT_SECTION']) ? (int) $arParams['PARENT_SECTION'] : 0;
		$arParams['PARENT_SECTION_CODE'] = isset($arParams['PARENT_SECTION_CODE']) ? trim($arParams['PARENT_SECTION_CODE']) : '';

		if (!is_array($arParams['FIELD_CODE']))
			$arParams['FIELD_CODE'] = array();
		foreach ($arParams['FIELD_CODE'] as $key => $val)
			if (!$val)
				unset($arParams['FIELD_CODE'][$key]);
			
		
			
			
		return $arParams;
	}
	
	public function executeComponent()
	{
		$AJAX_ID = $this->GetEditAreaId('ajax');
		global $APPLICATION, $USER;

		if ($this->request->isAjaxRequest() && $this->request->get('AJAX_ID') == $AJAX_ID)
			$APPLICATION->RestartBuffer();
		
		if ($this->startResultCache(false, array()))
		{
			if(!Loader::includeModule('iblock'))
			{
				$this->abortResultCache();
				return;
			}

			if ($this->arResult = \CIBlock::GetArrayByID($this->arParams['IBLOCK_ID']))
			{
				$arSelect = array_merge($this->arParams['FIELD_CODE'], $this->getSelect());
// 				$bGetProperty = count($this->arParams['PROPERTY_CODE']) > 0;
// 				if ($bGetProperty)
// 					$arSelect[] = 'PROPERTY_*';
				
// 				$this->arParams['PARENT_SECTION'] = \CIBlockFindTools::GetSectionID(
// 					$this->arParams['PARENT_SECTION'],
// 					$this->arParams['PARENT_SECTION_CODE'],
// 					array(
// 						'GLOBAL_ACTIVE' => 'Y',
// 						'IBLOCK_ID' => $this->arResult['ID'],
// 					)
// 				);
				
// 				$arFilter = $this->getFilter();
				
// 				if ($this->arParams['PARENT_SECTION'] > 0)
// 				{
// 					$arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION'];
// 					$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
// 				}
				
// 				if ($this->arParams['ELEMENT_ID'] > 0)
// 				{
// 					$arFilter['ID'] = $this->arParams['ELEMENT_ID'];
// 				}

// 				$arUserID = array();
	

				$this->arParams['PARENT_SECTION'] = \CIBlockFindTools::GetSectionID(
					$this->arParams['PARENT_SECTION'],
					$this->arParams['PARENT_SECTION_CODE'],
					array(
						'GLOBAL_ACTIVE' => 'Y',
						'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
					)
				);

				$arOrder = $this->getSort();
				$arFilter = $this->getFilter();
				
				$dbSection = \CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
				$dbSection->SetUrlTemplates($this->arParams['DETAIL_URL'], $this->arParams['SECTION_URL'], $this->arParams['LIST_URL']);
				
				$dark = Design::button(Option::get('mlab.appforsale', SITE_TEMPLATE_ID.'_base_color', '#3f51b5')) == '#fff' ? true : false;
				while ($arSection = $dbSection->GetNext())
				{
					if ($dark)
						$arSection['PICTURE'] = (0 < $arSection['UF_ICON_DARK'] ? \CFile::ResizeImageGet($arSection['UF_ICON_DARK'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
					else
						$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? \CFile::ResizeImageGet($arSection['PICTURE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
						
					
					$arSection['DETAIL_PICTURE'] = (0 < $arSection['DETAIL_PICTURE'] ? \CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width' => 1200, 'height' => 400), BX_RESIZE_IMAGE_EXACT, true) : false);
					
					
					$arSection['FIELDS'] = array();
					foreach ($this->arParams['FIELD_CODE'] as $code)
						if (array_key_exists($code, $arSection))
							$arSection['FIELDS'][$code] = $arSection[$code];
						
					$arButtons = \CIBlock::GetPanelButtons($arSection['IBLOCK_ID'], 0, $arSection['ID']);
					$this->AddEditAction($arSection['ID'], $arButtons['edit']['edit_section']['ACTION_URL'], $this->arResult['SECTION_EDIT']);
					$this->AddDeleteAction($arSection['ID'], $arButtons['edit']['delete_section']['ACTION_URL'], $this->arResult['SECTION_DELETE'], array('CONFIRM' => Loc::getMessage('SECTION_DELETE_CONFIRM')));
						
					$this->elements[] = $arSection;
				}




// 				$obParser = new \CTextParser;
// 				$this->arResult['ELEMENTS'] = array();
// 				$dbElement = \CIBlockElement::GetList($this->getSort(), $arFilter, false, $arNavParams, $arSelect);
// 				$dbElement->SetUrlTemplates($this->arParams['DETAIL_URL'], $this->arParams['SECTION_URL'], $this->arParams['LIST_URL']);
// 				while ($obElement = $dbElement->GetNextElement())
// 				{
// 					$arItem = $obElement->GetFields();

// 					$arUserID[$arItem['CREATED_BY']] = true;
					
// 					$dbSection = \CIBlockSection::GetNavChain($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
// 					while ($arSection = $dbSection->Fetch())
// 					{
// 						$arItem['IBLOCK_SECTION'] = $arSection;
// 					}
						
// 					$arButtons = \CIBlock::GetPanelButtons(
// 							$arItem['IBLOCK_ID'],
// 							$arItem['ID'],
// 							0,
// 							array('SECTION_BUTTONS' => false, 'SESSID' => false)
// 					);
// 					$this->AddEditAction($arItem['ID'], $arButtons['edit']['edit_element']['ACTION_URL'], $this->arResult['ELEMENT_EDIT']);
// 					$this->AddDeleteAction($arItem['ID'], $arButtons['edit']['delete_element']['ACTION_URL'], $this->arResult['ELEMENT_DELETE'], array('CONFIRM' => Loc::getMessage('ELEMENT_DELETE_CONFIRM')));
						
					

					
// 					if ($bGetProperty)
// 						$arItem['PROPERTIES'] = $obElement->GetProperties();
						
// 					$arItem['DISPLAY_PROPERTIES'] = array();					
// 					foreach ($this->arParams['PROPERTY_CODE'] as $pid)
// 					{
// 						$prop = &$arItem['PROPERTIES'][$pid];
// 						if (
// 							(is_array($prop['VALUE']) && count($prop['VALUE']) > 0)
// 							|| (!is_array($prop['VALUE']) && strlen($prop['VALUE']) > 0)
// 						)
// 						{
// 							$arItem['DISPLAY_PROPERTIES'][$pid] = \CIBlockFormatProperties::GetDisplayValue($arItem, $prop);
// 						}
// 					}
					
// 					$this->elements[] = $arItem;
// 					$this->arResult['ELEMENTS'][] = $arItem['ID'];
// 				}
				
 				$this->makeOutputResult();
				
// 				if (!empty($arUserID))
// 				{
// 					$dbUser = UserTable::getList(array(
// 						'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'UF_CITY'),
// 						'filter' => array('ID' => array_keys($arUserID)),
// 					));
// 					$nameFormat = \CSite::GetNameFormat(true);
// 					while ($arUser = $dbUser->fetch())
// 					{
// 						$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arOneUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
// 						$arUser['FORMAT_NAME'] = \CUser::FormatName($nameFormat, $arUser);
// 						$this->arResult['USERS'][$arUser['ID']] = $arUser;
// 					}
// 				}
							
// 				ob_start();
// 				$APPLICATION->IncludeComponent(
// 						'appforsale:pagenavigation',
// 						'',
// 						array(
// 							'NAV_RESULT' => $dbElement,
// 							'AJAX_ID' => $AJAX_ID
// 						),
// 						$this,
// 						array(
// 							'HIDE_ICONS' => 'Y'
// 						)
// 				);
// 				$this->arResult['NAV_STRING'] = ob_get_contents();
// 				ob_end_clean();

				$this->setResultCacheKeys($this->getCacheKeys()); 				
 				$this->includeComponentTemplate();
			}
			else
			{
 				$this->abortResultCache();
			}
		}
		
		if ($this->request->isAjaxRequest() && $this->request->get('AJAX_ID') == $AJAX_ID)
			die();
		
		if(isset($this->arResult['ID']))
		{
			if($USER->IsAuthorized())
			{
				if($APPLICATION->GetShowIncludeAreas())
				{
					if (Loader::includeModule('iblock'))
					{			
						$arButtons = \CIBlock::GetPanelButtons(
							$this->arResult['ID'],
							0,
							$this->arParams['PARENT_SECTION']
						);
									
						$this->addIncludeAreaIcons(\CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
					}
				}
			}
			
			$this->initMetaData();
					
			return $this->arResult['ELEMENTS'];
		}
	}
	
	protected function initMetaData()
	{
	}
	
	protected function getSelect()
	{
		return array(
			'ID',
			'NAME',
			'IBLOCK_ID', 
			'CODE',
			'SECTION_PAGE_URL',
			'DETAIL_PAGE_URL',
			'PICTURE',
			'UF_ICON_DARK',
			'LEFT_MARGIN',
			'RIGHT_MARGIN',
			'DEPTH_LEVEL',
			'DETAIL_PICTURE'
		);
	}
	
	protected function getFilter()
	{
		return array(
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'IBLOCK_LID' => $this->getSiteId(),
			'ACTIVE' => 'Y',
			'GLOBAL_ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'Y',
			'MIN_PERMISSION' => 'R',
			'SECTION_ID' => $this->arParams['PARENT_SECTION'] > 0 ? $this->arParams['PARENT_SECTION'] : false
		);
	}
	
	protected function getSort()
	{
		return array(
			'LEFT_MARGIN' => 'ASC'
		);
	}

	protected function getCacheKeys()
	{
		return array(		
			'ID'
		);
	}
	
	protected function makeOutputResult()
	{
		
	}
	
// 	protected function setElementPanelButtons(&$element)
// 	{
// 		$buttons = \CIBlock::GetPanelButtons(
// 			$element['IBLOCK_ID'],
// 			$element['ID'],
// 			$element['IBLOCK_SECTION_ID'],
// 			array('SECTION_BUTTONS' => false, 'SESSID' => false, 'CATALOG' => false)
// 		);
// 		$element['EDIT_LINK'] = $buttons['edit']['edit_element']['ACTION_URL'];
// 		$element['DELETE_LINK'] = $buttons['edit']['delete_element']['ACTION_URL'];
// 	}
}
?>