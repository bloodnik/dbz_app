<?
namespace Mlab\Appforsale;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Text\Encoding,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Application,
	Bitrix\Main\IO\File,
	Bitrix\Main\Context;

Loc::loadMessages(__FILE__);

class Design
{
	function compile($template_id)
	{
		if (empty($template_id))
			return false;
		
		$file = new File(Application::getDocumentRoot().'/bitrix/templates/'.$template_id.'/colors.less');
		if ($file->isExists())
		{
			$base_color = Option::get('mlab.appforsale', $template_id.'_base_color', '#e91e63');
			File::putFileContents(
				Application::getDocumentRoot().'/bitrix/templates/'.$template_id.'/colors.css',
				str_replace(
					array(
						'@btn_color',
						'@base_color_light',
						'@base_color_dark',
						'@base_color'
					),
					array(
						self::button($base_color),
						self::lighten($base_color, 20),
						self::darken($base_color, 20),
						$base_color
					),
					Encoding::convertEncodingToCurrent($file->getContents())
				),
				File::REWRITE
			);
		}
	}
	
	function button($color)
	{
		$rgb = self::hexToRgb($color);
		$scale = 0.299 * $rgb[0] + 0.587 * $rgb[1] + 0.114 * $rgb[2];
		if ($scale > 127)
			return '#000';
		else
			return '#fff';
	}
	
	function lighten($color, $amount)
	{
		$rgb = self::hexToRgb($color);
		return self::rgbToHex(array(
			self::clamp($rgb[0] + $amount),
			self::clamp($rgb[1] + $amount),
			self::clamp($rgb[2] + $amount)
		));
	}
		
	function darken($color, $amount)
	{
		$rgb = self::hexToRgb($color);
		return self::rgbToHex(array(
			self::clamp($rgb[0] - $amount),
			self::clamp($rgb[1] - $amount),
			self::clamp($rgb[2] - $amount)
		));
	}
	
	function hexToRgb($color)
	{
		$color = str_replace('#', '', $color);
		
		return array(
			hexdec($color[0] . $color[1]),
			hexdec($color[2] . $color[3]),
			hexdec($color[4] . $color[5])
		);
	}
	
	function rgbToHex($color)
	{
		$red = dechex($color[0]);
		$green = dechex($color[1]);
		$blue = dechex($color[2]);
		
		return "#" . (strlen($red) == 1 ? '0'.$red : $red) . (strlen($green) == 1 ? '0'.$green : $green) . (strlen($blue) == 1 ? '0'.$blue : $blue);
	}
	
	function clamp($val, $max = 255)
	{
		return min(max($val, 0), $max);
	}
		
	function OnPanelCreate()
	{
		$file = new File(Application::getDocumentRoot().'/bitrix/templates/'.SITE_TEMPLATE_ID.'/colors.less');
		if ($file->isExists())
		{
			$GLOBALS['APPLICATION']->AddPanelButtonMenu(2, array(
					'TEXT' => Loc::getMessage('TAB'),
					'TITLE' => Loc::getMessage('TAB_TITLE'),
					'ACTION' => 'jsUtils.Redirect([], \'/bitrix/admin/template_edit.php?lang='.Context::getCurrent()->getLanguage().'&ID='.SITE_TEMPLATE_ID.'&tabControl_active_tab=design\')'
			));
		}
	}
	
	function OnAdminTabControlBegin(&$form)
	{
		$request = Context::getCurrent()->getRequest();
		if ($request->getRequestedPage() == '/bitrix/admin/template_edit.php')
		{
			$template_id = $request->get('ID');
			$file = new File(Application::getDocumentRoot().'/bitrix/templates/'.$template_id.'/colors.less');
			if ($file->isExists())
			{
				ob_start();
				$GLOBALS['APPLICATION']->IncludeComponent(
						"appforsale:colorpicker",
						"",
						array(
							'NAME' => 'BASE_COLOR',
							'VALUE' => Option::get('mlab.appforsale', $template_id.'_base_color', '#e91e63')
						),
						false
				);
					
				$colorpicker = ob_get_contents();
				ob_end_clean();
				$form->tabs[] = array(
					'DIV' => 'design',
					'TAB' => Loc::getMessage('TAB'),
					'TITLE' => Loc::getMessage('TAB_TITLE'),
					'CONTENT' => '<tr>
                		<td width="40%" class="adm-detail-content-cell-l">'.Loc::getMessage('BASE_COLOR').':</td>
               			<td width="60%" class="adm-detail-content-cell-r">'.$colorpicker.'</td>
            		</tr>'
				);
			}
		}
	}
	
	function OnBeforeProlog()
	{
		$request = Context::getCurrent()->getRequest();
		if ($request->isPost() && $request->getRequestedPage() == '/bitrix/admin/template_edit.php')
		{
			$template_id = $request->get('ID');
			$file = new File(Application::getDocumentRoot().'/bitrix/templates/'.$template_id.'/colors.less');
			if ($file->isExists())
			{
				Option::set('mlab.appforsale', $template_id.'_base_color', '#'.str_replace('#', '', $request->get('BASE_COLOR')));
				self::compile($template_id);
			}
		}		
	}
}
?>