<?
namespace Mlab\Appforsale\UserType;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Property extends \CUserTypeEnum
{
	function GetUserTypeDescription()
	{
		return array(
			'USER_TYPE_ID' => 'iblock_property',
			'CLASS_NAME' => '\Mlab\Appforsale\UserType\Property',
			'DESCRIPTION' => Loc::getMessage('DESCRIPTION'),
			'BASE_TYPE' => 'string'
		);
	}
	
	function CheckFields($arUserField, $value)
	{
		return array();
	}
	
	function PrepareSettings($arUserField)
	{
		$res = parent::PrepareSettings($arUserField);
		$res['IBLOCK_ID'] = intval($arUserField['SETTINGS']['IBLOCK_ID']);
		return $res;
	}
	
	function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
	{
		$result = parent::GetSettingsHTML($arUserField, $arHtmlControl, $bVarsFromForm);
		
		if($bVarsFromForm)
			$iblock_id = $GLOBALS[$arHtmlControl['NAME']]['IBLOCK_ID'];
		elseif (is_array($arUserField))
			$iblock_id = $arUserField['SETTINGS']['IBLOCK_ID'];
		else
			$iblock_id = '';
		
		$result .= '
            <tr>
                <td>'.Loc::getMessage('USER_TYPE_IBEL_DISPLAY').':</td>
                <td>
                    '.GetIBlockDropDownList($iblock_id, $arHtmlControl['NAME'].'[IBLOCK_TYPE_ID]', $arHtmlControl['NAME'].'[IBLOCK_ID]', false, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"').'
                </td>
            </tr>
            ';
		return $result;
	}
	
	function GetList($arUserField)
	{		
		$dbProperty = false;
		if(Loader::includeModule('iblock'))
		{
			$obProperty = new PropertyEnum;
			$dbProperty = $obProperty->GetList($arUserField['SETTINGS']['IBLOCK_ID']);
		}
		return $dbProperty;
	}
	

}

class PropertyEnum extends \CDBResult
{
	function GetList($iblock_id)
	{
		$dbProperty = false;
		if(Loader::includeModule('iblock'))
		{
			$dbProperty = \CIBlockProperty::GetList(
				array('SORT' => 'ASC', 'ID' => 'ASC'), 
				array('IBLOCK_ID' => $iblock_id, 'ACTIVE' => 'Y')
			);
			if($dbProperty)
			{
				$dbProperty = new PropertyEnum($dbProperty);
			}
		}
		return $dbProperty;
	}
	
	function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$r = parent::GetNext($bTextHtmlAuto, $use_tilda);
		if($r)
			$r['VALUE'] = '['.$r['ID'].'] '.$r['NAME'];
		
		return $r;
	}
}
?>