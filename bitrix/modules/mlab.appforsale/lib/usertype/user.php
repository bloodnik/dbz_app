<?
namespace Mlab\Appforsale\UserType;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class User extends \CUserTypeInteger
{
	function GetUserTypeDescription()
	{
		return array(
			'USER_TYPE_ID' => 'user',
			'CLASS_NAME' => '\Mlab\Appforsale\UserType\User',
			'DESCRIPTION' => Loc::getMessage('USER_DESCRIPTION'),
			'BASE_TYPE' => 'int'
		);
	}
	
	function GetAdminListViewHTML($arUserField, $arHtmlControl)
	{
		static $cache = array();
		if(!array_key_exists($arHtmlControl['VALUE'], $cache))
		{
			$dbUser = \CUser::GetByID($arHtmlControl['VALUE']);
			if ($arUser = $dbUser->Fetch())
			{
				$cache[$arUser['ID']] = '[<a href="/bitrix/admin/user_edit.php?ID='.$arUser['ID'].'&lang='.LANGUAGE_ID.'">'.$arUser['ID'].'</a>] ' . \CUser::FormatName(\CSite::GetNameFormat(true), $arUser); 
			}
		}
		return $cache[$arHtmlControl['VALUE']];
	}
	
	function GetEditFormHTML($arUserField, $arHtmlControl)
	{
		$dbUser = \CUser::GetByID($arHtmlControl['VALUE']);
		if ($arUser = $dbUser->Fetch())
		{
			return '[<a href="/bitrix/admin/user_edit.php?ID='.$arUser['ID'].'&lang='.LANGUAGE_ID.'">'.$arUser['ID'].'</a>] ' . \CUser::FormatName(\CSite::GetNameFormat(true), $arUser);
		}
        return '';
    }
}
?>