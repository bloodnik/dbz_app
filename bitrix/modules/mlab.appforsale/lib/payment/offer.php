<?
namespace Mlab\Appforsale\Payment;

class Offer
{	
	function OnBeforeIBlockElementAdd(&$arFields)
	{	
		global $USER, $USER_FIELD_MANAGER, $APPLICATION;
		
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'offer')
		{
			if (\COption::GetOptionString('mlab.appforsale', 'monetization_use', 'Y') == 'Y')
			{
				$taskID = 0;
				if (array_key_exists('TASK_ID', $arFields['PROPERTY_VALUES']))
				{
					$taskID = intval($arFields['PROPERTY_VALUES']['TASK_ID']);
				}
				else
				{
					$dbProperty = \CIBlockProperty::GetByID('TASK_ID', false, 'offer');
					if($arProperty = $dbProperty->GetNext())
					{
						$arValue = $arFields['PROPERTY_VALUES'][$arProperty['ID']];
						$n0 = current($arValue);
						$taskID = intval($n0['VALUE']);
					}
				}
				
				if ($taskID > 0)
				{
					$dbTask = \CIBlockElement::GetByID($taskID);
					if($obTask = $dbTask->GetNextElement())
					{
						$arTask = $obTask->GetFields();		
						$paySum = 0.0;
						$arFields['PAY'] = $USER_FIELD_MANAGER->GetUserFieldValue(
								'IBLOCK_'.$arTask['IBLOCK_ID'].'_SECTION',
								'UF_OFFER_PRICE',
								$arTask['IBLOCK_SECTION_ID']
						);
						if ($arFields['PAY'] > 0)
						{
							$paySum = (float) $arFields['PAY'];
						}
						
						$comission = $USER_FIELD_MANAGER->GetUserFieldValue(
								'IBLOCK_'.$arTask['IBLOCK_ID'].'_SECTION',
								'UF_COMISSION',
								$arTask['IBLOCK_SECTION_ID']
						);
						
						if ($comission > 0)
						{
							$properties = $obTask->GetProperties();
							if ($properties['PRICE']['VALUE'] > 0)
							{
								$paySum =+ ((float) $properties['PRICE']['VALUE']/100*$comission);
							}
						}
						
						$currentBudget = 0.0;
						$arUserAccount = \CAppforsaleUserAccount::GetByUserID($USER->GetID());
						if ($arUserAccount)
						{
							$currentBudget = (float) $arUserAccount['CURRENT_BUDGET'];
						}
						
						if ($currentBudget < $paySum)
						{
							//$APPLICATION->ThrowException(GetMessage("SKGU_NO_ENOUGH"), "NO_ENOUGH");
							//return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	function OnAfterIBlockElementAdd($arFields)
	{
		global $USER;
		
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'offer')
		{
			if ($arFields['ID'] > 0 && $arFields['PAY'] > 0)
			{
				//\CAppforsaleUserAccount::Pay($USER->GetID(), $arFields['PAY'], $arFields['ID']);
			}
		}
	}
}
?>