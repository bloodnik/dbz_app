<?
namespace Mlab\Appforsale\Payment;

class Task
{	
	function OnBeforeIBlockElementAdd(&$arFields)
	{	
		global $USER, $APPLICATION, $USER_FIELD_MANAGER;
		
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task')
		{
			if (\COption::GetOptionString('mlab.appforsale', 'moderation_task', 'N') == 'Y')
			{
				$arFields['ACTIVE'] = 'N';
			}
			else
			{
				$arFields['ACTIVE'] = 'Y';
			}
			
			if (\COption::GetOptionString('mlab.appforsale', 'monetization_use', 'Y') == 'Y')
			{
				$price = $USER_FIELD_MANAGER->GetUserFieldValue(
						'IBLOCK_'.$arFields['IBLOCK_ID'].'_SECTION',
						'UF_PRICE',
						$arFields['IBLOCK_SECTION_ID']
				);
				
				if (intval($price) > 0)
				{
					$arCity = \CIBlockProperty::GetPropertyArray('CITY_ID');
					if ($arCity)
					{
						if (array_key_exists($arCity['ORIG_ID'], $arFields['PROPERTY_VALUES']))
						{
							$dbElement = \CIBlockElement::GetList(array(), array('ID' => $arFields['PROPERTY_VALUES'][$arCity['ORIG_ID']]), false, false, array('ID', 'PROPERTY_RATIO'));
							if ($arElement = $dbElement->GetNext())
							{
								if ($arElement['PROPERTY_RATIO_VALUE'] > 0)
								{
									$price = $price * $arElement['PROPERTY_RATIO_VALUE'];
								}
							}
						}
					}
					
					$currentBudget = 0.0;
					$arUserAccount = \CAppforsaleUserAccount::GetByUserID($USER->GetID());
					if ($arUserAccount)
					{
						$currentBudget = (float) $arUserAccount['CURRENT_BUDGET'];
					}
					
					if ($currentBudget < $price)
					{
						$APPLICATION->ThrowException(GetMessage("SKGU_NO_ENOUGH"), "NO_ENOUGH");
						return false;
					}
					
					$arFields['PAY'] = $price;
				}
			}
		}
		return true;
	}
	
	function OnAfterIBlockElementAdd($arFields)
	{
		global $USER;
		
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task')
		{
			if ($arFields['ID'] > 0 && $arFields['PAY'] > 0)
			{
				\CAppforsaleUserAccount::Pay($USER->GetID(), $arFields['PAY'], $arFields['ID']);
			}
		}
	}
}
?>