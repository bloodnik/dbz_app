<?
namespace Mlab\Appforsale\Geolocation;

use Bitrix\Main\HttpRequest;

class Position
{
    private $coords;
    private $deviceId;
    private $timestamp;
    
    public function __construct()
    {

    }
    
    public static function createFromRequest(HttpRequest $request)
    {
        $position = new static();
        
        $position
            ->setDeviceId($request->get("device_id"))
            ->setCoords(Coordinates::createFromRequest($request))
            ->setTimestamp($request->get("timestamp"));
        
        return $position;
    }
    
    public function getCoords(): Coordinates
    {
        return $this->coords;
    }
    
    public function getDeviceId(): String
    {
        return $this->deviceId;
    }
    
    public function getTimestamp(): String
    {
        return $this->timestamp;
    }
    
    public function setCoords(Coordinates $coords): Position
    {
        $this->coords = $coords;
        return $this;
    }
    
    public function setDeviceId(String $deviceId): Position
    {
        $this->deviceId = $deviceId;
        return $this;
    }
    
    public function setTimestamp(String $timestamp): Position
    {
        $this->timestamp = substr($timestamp, 0, 10);
        return $this;
    }
}
?>