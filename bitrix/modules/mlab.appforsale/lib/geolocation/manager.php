<?
namespace Mlab\Appforsale\Geolocation;

use Bitrix\Main\Event,
    Mlab\Appforsale\Geolocation\Entity\PositionTable,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\SystemException;
use Bitrix\Main\Entity\Query;
use Mlab\Appforsale\MobileApp;

class Manager
{
    private static $instance = null;
    
    private function __construct()
    {
        
    }
    
    public static function getInstance(): Manager
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    public static function onUserLogin($userId)
    {
        $mobileApp = MobileApp::getInstance();
        if ($mobileApp->isDevice())
        {
            $query = new Query(PositionTable::getEntity());
            $query
                ->addSelect("ID")
                ->addFilter("DEVICE_ID", $mobileApp->getDeviceId())
                ->setLimit(1);
            
            if ($row = $query->exec()->fetch())
            {
                PositionTable::update($row["ID"], [
                    "USER_ID" => $userId,
                    "DATE_AUTH" => new DateTime()
                ]);
            }
        }
    }
    
    public static function onUserLogout($userId)
    {
        $mobileApp = MobileApp::getInstance();
        if ($mobileApp->isDevice())
        {
            $query = new Query(PositionTable::getEntity());
            $query
                ->addSelect("ID")
                ->addFilter("DEVICE_ID", $mobileApp->getDeviceId())
                ->setLimit(1);
            
            if ($row = $query->exec()->fetch())
            {
                PositionTable::update($row["ID"], [
                    "USER_ID" => "",
                    "DATE_AUTH" => ""
                ]);
            }
        }
    }
    
    public function setCurrentPosition(Position $position)
    {        
        global $USER;

        $coords = $position->getCoords();

        $data = [
            "DEVICE_ID" => $position->getDeviceId(),
            "TIMESTAMP_X" => DateTime::createFromTimestamp($position->getTimestamp()),
            "LATITUDE" => $coords->getLatitude(),
            "LONGITUDE" => $coords->getLongitude(),
            "ACCURACY" => $coords->getAccuracy()
        ];
        
        if ($USER->IsAuthorized())
        {
            $data["USER_ID"] = $USER->GetID();
            $data["DATE_AUTH"] = new DateTime();
        }
        
        $query = new Query(PositionTable::getEntity());
        $query
            ->addSelect("ID")
            ->addFilter("DEVICE_ID", $position->getDeviceId())
            ->setLimit(1);
        
        if ($row = $query->exec()->fetch())
        {
            $result = PositionTable::update($row["ID"], $data);
        }
        else
        {
            $result = PositionTable::add($data);
        }

        if (!$result->isSuccess())
        {
            throw new SystemException(implode(", ", $result->getErrorMessages()));
        }
        
        $event = new Event("mlab.appforsale", "onCurrentPosition", ["ENTITY" => $position]);
        $event->send();
    }
}
?>