<?
namespace Mlab\Appforsale\Geolocation;

use Bitrix\Main\HttpRequest;

class Coordinates
{
    private $accuracy;
    private $latitude;
    private $longitude;
    
    public function __construct()
    {
        
    }
    
    public static function createFromRequest(HttpRequest $request)
    {
        $coords = new static();
        
        $coords
            ->setAccuracy($request->get("accuracy"))
            ->setLatitude($request->get("latitude"))
            ->setLongitude($request->get("longitude"));
        
        return $coords;
    }
    
    public function getAccuracy(): Float
    {
        return $this->accuracy;
    }
    
    public function getLatitude(): Float
    {
        return $this->latitude;
    }
    
    public function getLongitude(): Float
    {
        return $this->longitude;
    }
    
    public function setAccuracy(Float $accuracy): Coordinates
    {
        $this->accuracy = round($accuracy, 6);
        return $this;
    }
    
    public function setLatitude(Float $latitude): Coordinates
    {
        $this->latitude = round($latitude, 6);
        return $this;
    }
    
    public function setLongitude(Float $longitude): Coordinates
    {
        $this->longitude = round($longitude, 6);
        return $this;
    }
}
?>