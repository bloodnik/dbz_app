<?
namespace Mlab\Appforsale\Geolocation\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class PositionTable extends Entity\DataManager
{
    public static function getMap()
    {
        return [
            new Entity\IntegerField("ID", [
                    "primary" => true,
                    "autocomplete" => true
            ]),
            new Entity\DatetimeField("DATE_CREATE", [
                "default_value" => function() { return new DateTime(); }
            ]),
            new Entity\DatetimeField("TIMESTAMP_X", [
                    "required" => true
            ]),
            new Entity\StringField("DEVICE_ID", [
                "validation" => [__CLASS__, "validateDeviceId"]
            ]),
            new Entity\FloatField("LATITUDE", [
                    "validation" => [__CLASS__, "validateLatitude"]
            ]),
            new Entity\FloatField("LONGITUDE", [
                    "validation" => [__CLASS__, "validateLongitude"]
            ]),
            new Entity\FloatField("ACCURACY", [
                    "validation" => [__CLASS__, "validateAccuracy"]
            ]),
            new Entity\IntegerField("USER_ID"),
            new Entity\DatetimeField("DATE_AUTH"),
            new Entity\ReferenceField("USER", "Bitrix\Main\UserTable", [
                    "=this.USER_ID" => "ref.ID"
            ])  
        ];   
    }
    
    public static function getTableName()
    {
        return "mlab_appforsale_geolocation_position";
    }
    
    public static function getUfId()
    {
        return "BM_GEO_POSITION";
    }
    
    public static function validateDeviceId()
    {
        return [
            new Entity\Validator\Length(null, 255)
        ];
    }
    
    public static function validateLatitude()
    {
        return [
            new Entity\Validator\Range(-90, 90)
        ];
    }
    
    public static function validateLongitude()
    {
        return [
            new Entity\Validator\Range(-180, 180)
        ];
    }
    
    public static function validateAccuracy()
    {
        return [
            new Entity\Validator\Range(0)
        ];
    }
}
?>