<?
namespace Mlab\Appforsale;

use Bitrix\Main\Loader;

class Profile
{
	function OnAfterUserDelete($user_id)
	{
		if(Loader::includeModule('iblock'))
		{
			$dbElement = \CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_TYPE' => 'profiles',
					'IBLOCK_CODE' => 'profile',
					'CREATED_BY' => $user_id
				),
				false,
				false,
				array(
					'ID'
				)
			);
			while($arElement = $dbElement->GetNext())
			{
				\CIBlockElement::Delete($arElement['ID']);
			}
		}
	}	
}
?>