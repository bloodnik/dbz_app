<?
namespace Mlab\Appforsale;

use Bitrix\Main\Loader;

class Task
{
	function OnAfterUserDelete($user_id)
	{
		if(Loader::includeModule('iblock'))
		{
			$dbElement = \CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_TYPE' => 'tasks',
					'IBLOCK_CODE' => 'task',
					'CREATED_BY' => $user_id
				),
				false,
				false,
				array(
					'ID'
				)
			);
			while($arElement = $dbElement->GetNext())
			{
				\CIBlockElement::Delete($arElement['ID']);
			}
		}
	}	
}
?>