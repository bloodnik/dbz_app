<?
namespace Mlab\Appforsale\Im\Model;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Application;

class DialogTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'mlab_im_dialog';
	}
	
	public static function getMap()
	{
		return array(
			new Entity\IntegerField(
					'id',
					array(
						'primary' => true,
						'autocomplete' => true
					)
			),
			new Entity\IntegerField(
					'user_id'
			),
			new Entity\IntegerField(
					'from_id'
			),
			new Entity\IntegerField(
					'read_state'
			),
			new Entity\IntegerField(
					'out'
			),
			new Entity\TextField(
					'body'
			),
			new Entity\IntegerField(
					'unread',
					array(
						'default_value' => 0
					)
			)
		);
	}
}

// class_alias('Mlab\Appforsale\Im\Model\DialogTable', 'Mlab\Appforsale\Im\DialogTable', false);
?>