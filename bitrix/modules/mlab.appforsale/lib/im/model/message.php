<?
namespace Mlab\Appforsale\Im\Model;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Application;

class MessageTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'mlab_im_message';
	}
	
	public static function getMap()
	{
		return array(
			new Entity\IntegerField(
					'id',
					array(
						'primary' => true,
						'autocomplete' => true
					)
			),
			new Entity\IntegerField(
					'user_id'
			),
			new Entity\IntegerField(
					'from_id'
			),
			new Entity\DatetimeField(
					'date',
					array(
						'default_value' => new Main\Type\DateTime()
					)
			),
			new Entity\IntegerField(
					'read_state',
					array(
						'default_value' => 0
					)
			),
			new Entity\IntegerField(
					'out',
					array(
						'default_value' => 0
					)
			),
			new Entity\TextField(
					'title'
			),
			new Entity\TextField(
					'body',
					array(
						'required' => true
					)
			),
			new Entity\TextField(
					'attachments'
			),	
			new Entity\IntegerField(
					'important',
					array(
						'default_value' => 0
					)
			),
			new Entity\IntegerField(
					'deleted',
					array(
						'default_value' => 0
					)
			),
			new Entity\IntegerField(
					'random_id'
			)
		);
	}
	
	
	
// 	public static function onBeforeAdd(Main\Entity\Event $event)
// 	{
//  		$result = new Entity\EventResult;
// // 		$data = $event->getParameter('fields');
		
//  		$result->addError(new Entity\EntityError('Ошибка'));
 	
//  		return $result;
// 	}
	
	public static function onAfterAdd(Main\Entity\Event $event)
	{
		$data = $event->getParameter('fields');
		
		$arDialog = DialogTable::getRow(array(
				'filter' => array(
					'=user_id' => $data['user_id'],
					'=from_id' => $data['from_id']
				),
				'select' => array(
					'id',
					'unread'
				)
		));
		
		if ($arDialog)
		{
			 $arFields = array(
				'body' => $data['body'],
				'out' => $data['out'],
				'read_state' => $data['read_state']		
			);
			if ($data['out'] == 0)
				$arFields['unread'] = ($arDialog['unread'] + 1);
			
			DialogTable::update($arDialog['id'], $arFields);
		}
		else
		{
			$arFields = array(
					'user_id' => $data['user_id'],
					'from_id' => $data['from_id'],
					'body' => $data['body'],
					'out' => $data['out'],
					'read_state' => $data['read_state'],
					'unread' => 1
			);
			if ($data['out'] == 0)
				$arFields['unread'] = 1;

			DialogTable::add($arFields);
		}
	}
}

//class_alias('Mlab\Appforsale\Im\Model\MessageTable', 'Mlab\Appforsale\Im\MessageTable', false);
?>