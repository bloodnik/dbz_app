<?
namespace Mlab\Appforsale\Im;

use Mlab\Appforsale\Im\Model\MessageTable,
	Mlab\Appforsale\Im\Model\DialogTable,
	Bitrix\Main\Application;

class Messages
{
// 	public function delete()
// 	{
// 		// message_ids
// 		// spam
// 	}
	
// 	public function deleteDialog()
// 	{
// 		// user_id
// 		// peer_id
// 	}
	
// // 	public function getById()
// // 	{
// // 		// message_ids
// // 		// preview_length
// // 	}

	public function getDialogs()
	{
//  		global $USER;
// // 		// offset
// // 		// count
// // 		// start_message_id
// // 		// preview_length
// // 		// unread
// // 		// important
// // 		// unanswered

		
		
	

// 		$dbMessages = MessagesTable::getList(array(
// 				'order' => array(
// 					'id' => 'desc'
// 				),
// 				'group' => array(
// 					'user_id'
// 				),
// 				'filter' => array(
// 					'=from_id' => $USER->GetID()
// 				),
// 				'runtime' => array(
// 						new Entity\ExpressionField('CNT', 'COUNT(*)')
// 				),
// 				'select' => array(
// 					'id', 'user_id', 'body'
// 				)

// 		));
		
// 		return $dbMessages->fetchAll();
	}
	
	public function getHistory($offset = 0, $count = 0, $user_id, $peer_id, $start_message_id = 0, $rev = false)
	{
		global $USER;
		
		$filter = array(
				'=user_id' => $user_id,
				'=from_id' => $USER->GetID()
		);
		
		if ($start_message_id > 0)
			$filter['<id'] = $start_message_id;

		$dbMessages = MessageTable::getList(array(
				'offset' => $offset,
				'limit'   => $count,
				'filter' => $filter,
				'order' => array(
					'id' => $rev ? 'asc' : 'desc'
				),
				'select' => array(
					'id',
					'date',
					'out',
					'read_state',
					'body'
				)
		));
		
		$arResult = array();
		
		while ($arMessages = $dbMessages->fetch())
		{
			$arMessages['body'] = htmlspecialcharsEx($arMessages['body']);
			$arResult[] = $arMessages;
		}
		
		
		return $arResult;
		// htmlspecialcharsEx
		//return $dbMessages->fetchAll();
	}
	
// 	public function restore($message_id)
// 	{
		
// 		return true;
// 	}
	
	public function send($user_id, $message)
	{
		global $USER;
		
		$dbUser = \CUser::GetByID($USER->GetID());
		if ($arUser = $dbUser->Fetch())
		{
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
		}
		
		MessageTable::add(array(
				'user_id' => $USER->GetID(),
				'from_id' => $user_id,
				'out' => 0,
				'read_state' => 0, // 1
				'body' => $message
		));
		
		$addResult = MessageTable::add(array(
				'user_id' => $user_id,
				'from_id' => $USER->GetID(),
				'out' => 1,
				'read_state' => 0,
				'body' => $message
		));
		
		
		\Mlab\Appforsale\Pull\Stack::addByUser($USER->GetID(), array(
				'command' => 'message',
				'params' => array(
					'user_id' => $user_id,
					'out' => 1,
					'body' => htmlspecialcharsEx($message)
				)
		));
		
		\Mlab\Appforsale\Pull\Stack::addByUser($user_id, array(
			'command' => 'message',
			'params' => array(
				'user_id' => $USER->GetID(),
				'out' => 0,
				'format_name' => \CUser::GetFullName(),
				'icon' => 'http://'.$_SERVER['SERVER_NAME'].$arUser['PERSONAL_PHOTO']['src'],
				'body' => htmlspecialcharsEx($message)
			)
		));
		
		\CAppforsale::SendMessage(
			array($user_id),
			array(
				'body' => \CUser::GetFullName() .': '.$message,
				'icon' => 'ic_stat_name',
				'click_action' => 'appforsale.exec',
				'badge' => 1
			),
			array(
				'url' => 'http://'.$_SERVER['SERVER_NAME'].'/youdo/im/?sel='.$USER->GetID(),
				'archive' => 'N'
			)
		);
		
		
		
		
		
		
// 		if ($addResult->getId() <= 0)
// 		{
//  			return $addResult->getErrors()[0]->getMessage();	
// 		}
		
		return $addResult->getId();
	}
	
	public function setActivity($user_id)
	{
		global $USER;
		
		\Mlab\Appforsale\Pull\Stack::addByUser($user_id, array(
			'command' => 'activity',
			'params' => array(
				'user_id' => $user_id,
				'from_id' => $USER->GetID()
			)			
		));
	}
	
	public function markAsRead($peer_id)
	{
		global $USER;
		
		$dbMessages = MessageTable::getList(array(
				'filter' => array(
						'=user_id' => $USER->GetID(),
						'=from_id' => $peer_id,
						'=read_state' => 0,
						'=out' => 1
				),
				'select' => array(
						'id'
				)
		));
		
		while ($arMessages = $dbMessages->fetch())
		{
			MessageTable::update($arMessages['id'], array(
					'read_state' => 1
			));
		}
		
		$dbDialog = DialogTable::getList(array(
				'filter' => array(
						'=user_id' => $USER->GetID(),
						'=from_id' => $peer_id,
						'=read_state' => 0,
						'=out' => 1
				),
				'select' => array(
						'id'
				)
		));
		
		if ($arDialog = $dbDialog->fetch())
		{
			DialogTable::update($arDialog['id'], array(
				'read_state' => 1
			));
		}

		\Mlab\Appforsale\Pull\Stack::addByUser($peer_id, array(
				'command' => 'markAsRead',
				'params' => array(
					'user_id' => $USER->GetID(),
					'out' => 1
				)
		));
		
		
		
		
		// =======
		
		
		$dbMessages = MessageTable::getList(array(
				'filter' => array(
						'=user_id' => $peer_id,
						'=from_id' => $USER->GetID(),
						'=read_state' => 0,
						'=out' => 0
				),
				'select' => array(
						'id'
				)
		));
		
		while ($arMessages = $dbMessages->fetch())
		{
			MessageTable::update($arMessages['id'], array(
				'read_state' => 1
			));
		}
		
		$dbDialog = DialogTable::getList(array(
				'filter' => array(
						'=user_id' => $peer_id,
						'=from_id' => $USER->GetID(),
						'=read_state' => 0,
						'=out' => 0
				),
				'select' => array(
						'id'
				)
		));
		
		if ($arDialog = $dbDialog->fetch())
		{
			DialogTable::update($arDialog['id'], array(
				'read_state' => 1,
				'unread' => 0
			));
		}
		
	
		\Mlab\Appforsale\Pull\Stack::addByUser($USER->GetID(), array(
				'command' => 'markAsRead',
				'params' => array(
					'user_id' => $peer_id,
					'out' => 0
				)
		));
	}
	
}


// CUserCounter::Set(1, 'unread', 2, SITE_ID, '', false);
// print_r(CUserCounter::GetValue(1, 'unread'));

//class_alias('Mlab\Appforsale\Im\Messages', 'Mlab\Appforsale\Messages', false);
?>