<? 
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Archive
{
	function OnAfterIBlockElementSetPropertyValuesEx($element_id, $iblock_id, $property_values, $flags)
	{
		if (ToLower(\CIBlock::GetArrayByID($iblock_id, 'CODE')) == 'task')
		{
			if (array_key_exists('STATUS_ID', $property_values) && $property_values['STATUS_ID'] == 'F')
			{
				$dbElement = \CIBlockElement::GetByID($element_id);
				$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
				if ($obElement = $dbElement->GetNextElement())
				{
					$arElement = $obElement->GetFields();
					$arElement['PROPERTY_VALUES'] = $obElement->GetProperties();
					global $push_id;
					$push_id = 5;
					\CAppforsale::SendMessage(
							array((int) $arElement['PROPERTY_VALUES']['PROFILE_ID']['VALUE']),
							array(
									'body' => Loc::getMessage('NOTIFICATION_ARCHIVE_BODY', array('#NAME#' => $arElement['NAME'])),
									'icon' => 'ic_stat_name',
									'click_action' => 'appforsale.exec'
							),
							array(
									'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/'.$arElement['DETAIL_PAGE_URL']
							)
					);
				}
			}
		}
	}
}
?>