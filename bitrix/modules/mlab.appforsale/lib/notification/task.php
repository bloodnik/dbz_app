<?

namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Task {
	function OnBeforeIBlockElementUpdate(&$arFields) {
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task') {
			$dbElement = \CIBlockElement::GetByID($arFields['ID']);
			if ($arElement = $dbElement->GetNext()) {
				$arFields['_ACTIVE'] = $arElement['ACTIVE'];
			}
		}
	}

	function OnAfterIBlockElementAdd($arFields) {
		if ($arFields['ID'] > 0) {
			if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task') {
				if ($arFields['ACTIVE'] == 'Y' && $arFields['_ACTIVE'] != 'Y') {
					\CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], array('STATUS_ID' => 'N'));

					$dbElement = \CIBlockElement::GetByID($arFields['ID']);
					$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
					if ($arElement = $dbElement->GetNext()) {
						$dbProperty = \CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
						while ($arProperty = $dbProperty->GetNext()) {
							$arElement['PROPERTY_VALUES'][ $arProperty['ID'] ] = $arProperty['VALUE'];
						}

						$arSelectTask = Array("*", "PROPERTY_*");
						$arFilterTask = Array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "ID" => $arFields['ID']);
						$dbTask       = \CIBlockElement::GetList(Array(), $arFilterTask, false, Array(), $arSelectTask);
						while ($arTask = $dbTask->GetNextElement()) {
							$arFieldsTask = $arTask->GetFields();
							$arPropsTask  = $arTask->GetProperties();
							$partners     = $arPropsTask['PARTNERS']['VALUE'];
						}


						$arSelectProfile = Array("*");
						$arFilterProfile = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "ID" => $partners);
						$dbProfile       = \CIBlockElement::GetList(Array(), $arFilterProfile, false, Array(), $arSelectProfile);
						while ($arProfile = $dbProfile->GetNextElement()) {
							$arFieldsProfile = $arProfile->GetFields();
							$profiles[] = $arFieldsProfile['CREATED_BY'];
						}

						if ( ! empty($profiles)) {
							global $push_id;
							$push_id = 1;
							\CAppforsale::SendMessage(
								array_unique($profiles),
								array(
									'body'         => Loc::getMessage('NOTIFICATION_TASK_BODY', array('#NAME#' => $arElement['NAME'])),
									'icon'         => 'ic_stat_name',
									'click_action' => 'appforsale.exec'
								),
								array(
									'url' => (\CMain::IsHTTPS() ? 'http://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/tasks/' . $arElement['DETAIL_PAGE_URL'],
									'id'  => $arElement['ID']
								)
							);
						}

						/*$uf = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$arElement['IBLOCK_ID'].'_SECTION', $arElement['IBLOCK_SECTION_ID']);
						$uf_profile = $uf['UF_PROFILE']['VALUE'];
						if (!empty($uf_profile))
						{
							$sections = array();						
							foreach ($uf_profile as $pid)
							{
								$nav = \CIBlockSection::GetNavChain(false, $pid);
								while ($arNav = $nav->GetNext())
									$sections[] = $arNav['ID'];
							}
	
							$dbProfile = \Bitrix\Iblock\ElementTable::getList(array(
									'select' => array(
										'CREATED_BY',
										'DETAIL_TEXT'
									),
									'filter' => array(
		  								'=ACTIVE' => 'Y',
										'=IBLOCK_SECTION_ID' => $sections
									)
							));
							
							$arUser = array();
							while ($arProfile = $dbProfile->fetch())
							{
								if ($arElement['CREATED_BY'] != $arProfile['CREATED_BY'])
								{
									$boolRes = true;
									if (!empty($arProfile['DETAIL_TEXT']))
									{
										$check = null;
			 							eval('$check='.$arProfile['DETAIL_TEXT'].';');
										if (is_callable($check))
										{
											$boolRes = $check($arElement['PROPERTY_VALUES']);
										}
										unset($check);
									}
									if ($boolRes)
										$arUser[] = $arProfile['CREATED_BY'];
								}
							}
							
							if (!empty($arUser))
							{
								global $push_id;
								$push_id = 1;
								\CAppforsale::SendMessage(
										array_unique($arUser),
										array(
											'body' => Loc::getMessage('NOTIFICATION_TASK_BODY', array('#NAME#' => $arElement['NAME'])),
											'icon' => 'ic_stat_name',
											'click_action' => 'appforsale.exec'
										),
										array(
											'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/'.$arElement['DETAIL_PAGE_URL']
										)
								);
							}
						}*/
					}
				} else {
					if ($arFields['ACTIVE'] == 'N') {
//						\CEvent::Send('NEWTASK', \CSite::GetDefSite(), array(
//							'IBLOCK_ID' => $arFields['IBLOCK_ID'],
//							'ID' => $arFields['ID']
//						), 'Y');
					}
				}
			}
		}
	}
}

?>