<?
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Reject
{
	function OnAfterIBlockElementUpdate($arFields)
	{
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'offer')
		{
			if ($arFields['RESULT'] && $arFields['ACTIVE'] == 'N')
			{
				$dbOffer = \CIBlockElement::GetList(array(), array('ID' => $arFields['ID']), false, false, array('ID', 'IBLOCK_ID', 'CREATED_BY', 'PROPERTY_TASK_ID'));
				if($arOffer = $dbOffer->GetNext())
				{
					$dbTask = \CIBlockElement::GetByID($arOffer['PROPERTY_TASK_ID_VALUE']);
					$dbTask->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
					if($arTask = $dbTask->GetNext())
					{
						global $push_id;
						$push_id = 8;
							
						\CAppforsale::SendMessage(
							array((int) $arOffer['CREATED_BY']),
							array(
								'body' => Loc::getMessage('NOTIFICATION_REJECT_BODY', array('#NAME#' => $arTask['NAME'])),
								'click_action' => 'appforsale.exec'
							),
							array(
								'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/notification/'
								//'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/'.$arTask['DETAIL_PAGE_URL']
							)
						);
					}
				}
			}
		}
	}
}
?>