<?
namespace Mlab\Appforsale\Notification;

class Delete
{
	function OnIBlockElementDelete($ID, $arFields)
	{
		if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'task')
		{
			$dbElement = \CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_CODE' => 'offer',
					'PROPERTY_TASK_ID' => $ID
				)
			);
			while($arElement = $dbElement->GetNext())
			{
				$obElement = new \CIBlockElement;
				$obElement->Update($arElement['ID'], array('ACTIVE' => 'N'));
			}
		}
	}
}