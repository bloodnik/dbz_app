<? 
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Executor
{
	function OnAfterIBlockElementSetPropertyValuesEx($element_id, $iblock_id, $property_values, $flags)
	{
		if (ToLower(\CIBlock::GetArrayByID($iblock_id, 'CODE')) == 'task')
		{
			if (array_key_exists('PROFILE_ID', $property_values) && $property_values['PROFILE_ID'] > 0)
			{
				$dbElement = \CIBlockElement::GetByID($element_id);
				$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
				if ($arElement = $dbElement->GetNext())
				{
					global $push_id;
					$push_id = 3;
					\CAppforsale::SendMessage(
						array((int) $property_values['PROFILE_ID']),
						array(
							'body' => Loc::getMessage('NOTIFICATION_EXECUTOR_BODY', array('#NAME#' => $arElement['NAME'])),
							'icon' => 'ic_stat_name',
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/tasks-executor/'.$arElement['DETAIL_PAGE_URL']
						)
					);
				}
			}
		}
	}
}
?>