<? 
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Offer
{
	function OnAfterIBlockElementAdd($arFields)
	{
		if ($arFields['ID'] > 0)
		{
			if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'offer')
			{
				$dbElement = \CIBlockElement::GetByID($arFields['PROPERTY_VALUES']['TASK_ID']);
				$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
				if($arElement = $dbElement->GetNext())
				{
					$arSelectOffer = Array('*','PROPERTY_*');
					$arFilterOffer = Array("IBLOCK_ID" => 2, "CREATED_BY" => $arFields['CREATED_BY']);  
					$dbOffer = \CIBlockElement::GetList(Array(), $arFilterOffer, false, Array(), $arSelectOffer);
					if($arOffer = $dbOffer->GetNextElement())
					{
						$arFieldsOffer = $arOffer->GetFields();
						$arPropsOffer = $arOffer->GetProperties();
						$NAME = $arPropsOffer['NAME']['VALUE'];
						$SECOND_NAME = $arPropsOffer['SECOND_NAME']['VALUE'];
						$LAST_NAME = $arPropsOffer['LAST_NAME']['VALUE'];
					}	

					$FORMAT_NAME = $LAST_NAME.' '.$NAME.' '.$SECOND_NAME;
					
					global $push_id;
					$push_id = 2;
					\CAppforsale::SendMessage(
						array((int) $arElement['CREATED_BY']),
						array(
							'body' => Loc::getMessage('NOTIFICATION_OFFER_BODY', array('#NAME#' => $arElement['NAME'], '#USER#'=> $FORMAT_NAME)),
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/tasks-customer/'.$arElement['DETAIL_PAGE_URL']
						)
					);
				}
			}
		}
	}
}
?>