<? 
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Done
{
	function OnAfterIBlockElementSetPropertyValuesEx($element_id, $iblock_id, $property_values, $flags)
	{
		if (ToLower(\CIBlock::GetArrayByID($iblock_id, 'CODE')) == 'task')
		{
			if (array_key_exists('STATUS_ID', $property_values) && $property_values['STATUS_ID'] == 'D')
			{
				$dbElement = \CIBlockElement::GetByID($element_id);
				$dbElement->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');
				if ($arElement = $dbElement->GetNext())
				{

					$dbPropsProfile = \CIBlockElement::GetProperty(
						$iblock_id,
						$element_id,
						array(),
						array(
							'CODE' => 'PROFILE_ID'
						)
					);	
					if ($arPropsProfile = $dbPropsProfile->Fetch())
					{
						$property_values['PROFILE_ID'] = $arPropsProfile['VALUE'];
					}
			
					$arSelectOffer = Array('*','PROPERTY_*');
					$arFilterOffer = Array("IBLOCK_ID" => 2, "CREATED_BY" => $property_values['PROFILE_ID']);  
					$dbOffer = \CIBlockElement::GetList(Array(), $arFilterOffer, false, Array(), $arSelectOffer);
					if($arOffer = $dbOffer->GetNextElement())
					{
						$arFieldsOffer = $arOffer->GetFields();
						$arPropsOffer = $arOffer->GetProperties();
						$NAME = $arPropsOffer['NAME']['VALUE'];
						$SECOND_NAME = $arPropsOffer['SECOND_NAME']['VALUE'];
						$LAST_NAME = $arPropsOffer['LAST_NAME']['VALUE'];
					}	

					$FORMAT_NAME = $LAST_NAME.' '.$NAME.' '.$SECOND_NAME;

					global $push_id;
					$push_id = 4;
					\CAppforsale::SendMessage(
							array((int) $arElement['CREATED_BY']),
							array(
								'body' => Loc::getMessage('NOTIFICATION_DONE_BODY', array('#NAME#' => $arElement['NAME'], '#USER#'=> $FORMAT_NAME)),
								'icon' => 'ic_stat_name',
								'click_action' => 'appforsale.exec'
							),
							array(
								'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/youdo/tasks-customer/'.$arElement['DETAIL_PAGE_URL']
							)
					);
				}
			}
		}
	}
}
?>