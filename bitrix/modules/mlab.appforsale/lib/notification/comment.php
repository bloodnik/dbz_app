<? 
namespace Mlab\Appforsale\Notification;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Comment
{
	function OnAfterIBlockElementAdd($arFields)
	{
		if ($arFields['ID'] > 0)
		{
			if (ToLower(\CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'comment')
			{
				$dbElement = \CIBlockElement::GetByID($arFields['ID']);
				if ($obElement = $dbElement->GetNextElement())
				{
					$arElement = $obElement->GetFields();
					$arElement['PROPERTY_VALUES'] = $obElement->GetProperties();

	
					$dbElement2 = \CIBlockElement::GetByID($arElement['PROPERTY_VALUES']['TASK_ID']['VALUE']);
					if ($arElement2 = $dbElement2->GetNext())
					{
						global $push_id;
						$push_id = 7;
						\CAppforsale::SendMessage(
								array((int) $arElement['PROPERTY_VALUES']['PROFILE_ID']['VALUE']),
								array(
										'body' => Loc::getMessage('NOTIFICATION_COMMENT_BODY', array('#NAME#' => $arElement['PROPERTY_VALUES']['TEXT']['VALUE'])),
										'icon' => 'ic_stat_name',
									//	'click_action' => 'appforsale.exec'
								)
								//,
// 								array(
// 										'url' => 'http://'.$_SERVER['SERVER_NAME'].'/youdo/profile/?SECTION_ID='.$arElement2['IBLOCK_SECTION_ID'].'&ELEMENT_ID='.$arElement2['ID']
// 								)
						);
					}					
				}
			}
		}
	}
}
?>