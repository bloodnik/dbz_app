<?

namespace Mlab\Appforsale;

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Page\AssetLocation;
use Bitrix\Main\Web\Cookie;

class MobileApp {
	private $apiVersion;
	private $dev = false;
	private $device;
	private $deviceId;
	private $isBasicKernelInitialized = false;
	private $platform;

	private static $instance = null;

	private function __construct() {
		$context  = \Bitrix\Main\Context::getCurrent();
		$request  = $context->getRequest();
		$response = $context->getResponse();

		if ( ! empty($request->getHeader("bm-api-version"))) {
			$this->apiVersion = $request->getHeader("bm-api-version");

			$cookie = new Cookie("bm-api-version", $request->getHeader("bm-api-version"), time() + 60 * 60 * 24 * 60);
			$cookie->setDomain($context->getServer()->getServerName());
			$cookie->setHttpOnly(false);

			$response->addCookie($cookie);
		} else if ($request->getCookie("bm-api-version") != null) {
			$this->apiVersion = $request->getCookie("bm-api-version");
		} else {
			$this->apiVersion = 1;
		}


		if ( ! empty($request->getHeader("bm-device"))) {
			$this->device = $request->getHeader("bm-device");

			$cookie = new Cookie("bm-device", $request->getHeader("bm-device"), time() + 60 * 60 * 24 * 60);
			$cookie->setDomain($context->getServer()->getServerName());
			$cookie->setHttpOnly(false);

			$response->addCookie($cookie);
		} else if ($request->getCookie("bm-device") != null) {
			$this->device = $request->getCookie("bm-device");
		} else {
			$this->device = "";
		}


		if ( ! empty($request->getHeader("bm-device-id"))) {
			$this->deviceId = $request->getHeader("bm-device-id");

			$cookie = new Cookie("bm-device-id", $request->getHeader("bm-device-id"), time() + 60 * 60 * 24 * 60);
			$cookie->setDomain($context->getServer()->getServerName());
			$cookie->setHttpOnly(false);

			$response->addCookie($cookie);
		} else if ($request->getCookie("bm-device-id") != null) {
			$this->deviceId = $request->getCookie("bm-device-id");
		} else {
			$this->deviceId = "";
		}

		$this->dev = ! empty($this->deviceId);

		if ($this->dev) {
			$_COOKIE["APPLICATION"] = "Y";
			setcookie("APPLICATION", "Y", time() + 3600 * 24 * 30 * 12);
		}

		switch ($this->getDevice()) {
			case "iphone":
				$this->platform = "ios";
				break;
			case "ipad":
				$this->platform = "ios";
				break;
			case "android":
				$this->platform = "android";
				break;
		}
	}

	public function getApiVersion(): Int {
		return $this->apiVersion;
	}

	public function getDevice(): String {
		return $this->device;
	}

	public function getDeviceId(): String {
		return $this->deviceId;
	}

	public static function getInstance(): MobileApp {
		if ( ! isset(static::$instance)) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	public function getPlatform() {
		return $this->platform;
	}

	public function getViewPort() {
		return "<meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width\" />";
	}

	public function initializeBasicKernel() {
		if ($this->isBasicKernelInitialized) {
			return;
		}

		AddEventHandler("main", "OnBeforeEndBufferContent", [__CLASS__, "initScripts"]);

		$this->isBasicKernelInitialized = true;
	}

	public static function initScripts() {
		\CJSCore::Init();
		\CJSCore::Init(["mlab_appforsale_core"]);

		$jsVarsFormat = <<<JSCODE
        <script type="text/javascript">
            var mobileDev = "%s";
            var mobileDevice = "%s";
            var mobileApiVersion = "%s";
        </script>
JSCODE;

		Asset::getInstance()->addString(
			sprintf(
				$jsVarsFormat,
				MobileApp::getInstance()->isDevice() ? "Y" : "N",
				MobileApp::getInstance()->getDevice(),
				MobileApp::getInstance()->getApiVersion()
			),
			false,
			AssetLocation::BEFORE_CSS
		);

		Asset::getInstance()->addJs("/bitrix/js/mlab_appforsale/mobile.js");
		Asset::getInstance()->addString(MobileApp::getInstance()->getViewPort());
	}

	public function isDevice(): Bool {
		return $this->dev;
	}
}

?>