<?
namespace Mlab\Appforsale\Pull;

class Config
{
    public static function get(Int $userId)
    {
        if ($userId == 0)
        {
            return false;
        }
        
        $privateChannel = Channel::get($userId, Channel::TYPE_PRIVATE);
        $sharedChannel = Channel::get(0, Channel::TYPE_SHARED); 
       
        $channelId = [
            $privateChannel["CHANNEL_ID"],
            $sharedChannel["CHANNEL_ID"]
        ];
        
        return [
            "CHANNEL_ID" => implode("/", $channelId),
            "PATH" => Option::getWebSocketUrl($channelId),
            "ERROR" => ""
        ];
    }
}
?>