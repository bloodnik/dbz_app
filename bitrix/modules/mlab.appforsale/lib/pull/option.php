<?
namespace Mlab\Appforsale\Pull;

use Bitrix\Main\Context;

class Option
{
    public static function getWebSocketUrl(Array $channelId): String
    {        
        return (Context::getCurrent()->getRequest()->isHttps() ? "wss" : "ws") . "://#DOMAIN#/bitrix/subws/".(count($channelId) > 0 ? "?CHANNEL_ID=".implode("/", $channelId) : ""); 
    }
}
?>