<?
namespace Mlab\Appforsale\Pull;

class Event
{
    private $command;
    private $params = [];
    private $recipients = [];
    
    public function __construct()
    {
        
    }
        
    public function getCommand(): String
    {
        return $this->command;
    }
    
    public function getParams(): Array
    {
        return $this->params;
    }
    
    public function getRecipients(): Array
    {
        return $this->recipients;
    }
    
    public function setCommand(String $command): Event
    {
        $this->command = $command;
        return $this;
    }
    
    public function setParams(Array $params): Event
    {
        $this->params = $params;
        return $this;
    }
    
    public function addRecipient($recipient): Event
    {
        $this->recipients[] = $recipient;
        return $this;
    }
}
?>