<?
namespace Mlab\Appforsale\Pull;

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Page\AssetLocation;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\ORM\Query\Query;
use Mlab\Appforsale\Pull\Entity\ChannelTable;

class Manager
{
    private static $instance = null;
    
    private function __construct()
    {
        
    }

    public static function getInstance(): Manager
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    public function sendEvent(Event $event)
    {
        $text = \CUtil::PhpToJsObject([
                "command" => $event->getCommand(),
                "params" => $event->getParams()
        ]);
        
        if (!defined("BX_UTF") || !BX_UTF)
            $text = $GLOBALS["APPLICATION"]->ConvertCharset($text, SITE_CHARSET, "utf-8");
 
        $text = str_replace("\n", " ", $text);
            
        $channelId = [];
        $recipients = $event->getRecipients();
        if (empty($recipients))
        {
            $data = Channel::get(0, Channel::TYPE_SHARED);
            if ($data)
            {
                $channelId[] = $data["CHANNEL_ID"];
            }
        }
        else
        {
            foreach ($recipients as $recipient)
            {
                if (is_string($recipient) && strlen($recipient) == 32)
                {
                    $channelId[] = $recipient;
                }
                else
                {
                    $data = Channel::get($recipient, Channel::TYPE_PRIVATE);
                    if ($data)
                    {
                        $channelId[] = $data["CHANNEL_ID"];
                    }
                }
            }
        }

        foreach (array_chunk($channelId, 100) as $batch)
        {
            $this->sendCommand($batch, $text);
        }
    }
    
    private function sendCommand(Array $channelId, String $message)
    {
        $channelId = implode("/", array_unique($channelId));

        if (strlen($channelId) <= 0 || strlen($message) <= 0)
            return false;

        $httpClient = new HttpClient([
                "socketTimeout" => 5
        ]);
        
        $httpClient->post("http://127.0.0.1:8895/bitrix/pub/?CHANNEL_ID=" . $channelId, $message);
                
        return $httpClient->getResult();
    }
    
    public static function onEpilog()
    {
        $userId = 0;
        if (is_object($GLOBALS["USER"]) && intval($GLOBALS["USER"]->GetID()) > 0)
        {
            $userId = intval($GLOBALS["USER"]->GetID());
        }
        else if (intval($_SESSION["SESS_GUEST_ID"]) > 0)
        {
            $userId = intval($_SESSION["SESS_GUEST_ID"])*-1;
        }
        
        if (!defined("BM_PULL_SKIP_INIT") && !Context::getCurrent()->getRequest()->isAjaxRequest()
            && $userId != 0 && Loader::includeModule("mlab.appforsale"))
        {
            define("BM_PULL_SKIP_INIT", true);

            \CJSCore::Init(["mlab_appforsale_core", "mlab_appforsale_pull"]);

            $config = Config::get($userId);
            Asset::getInstance()->addString('<script type="text/javascript">BX.bind(window, "load", function() { BM.Pull.start('.(empty($config) ? "" : \CUtil::PhpToJSObject($config)).'); });</script>', false, AssetLocation::AFTER_JS);  
        }
    }
    
    public static function checkExpireAgent()
    {
        $dateTime = new DateTime();
        $dateTime->add("13 HOUR");

        $query = new Query(ChannelTable::getEntity());
        $query
            ->addSelect("ID")
            ->addFilter("<DATE_CREATE", $dateTime);
        
        $result = $query->exec();
        
        while ($data = $result->fetch())
        {
            ChannelTable::delete($data["ID"]);
        }
        
        return "Mlab\Appforsale\Pull\Manager::checkExpireAgent();";
    }
}
?>