<?
namespace Mlab\Appforsale\Pull;

use Bitrix\Main\Web\HttpClient;
use Mlab\Appforsale\Pull\Entity\ChannelTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Type\DateTime;

class Channel
{
    const TYPE_PRIVATE = "private";
    const TYPE_SHARED = "shared";
        
    public static function get(Int $userId, String $channelType)
    {
        $query = new Query(ChannelTable::getEntity());
        $query
            ->setSelect(["ID", "CHANNEL_ID", "CHANNEL_TYPE", "DATE_CREATE", "LAST_ID"])
            ->setFilter(["=USER_ID" => $userId, "=CHANNEL_TYPE" => $channelType])
            ->setLimit(1)
            ->setCacheTtl(43200);
       
        $result = $query->exec()->fetch();
        
        if (empty($result) || $result["DATE_CREATE"]->getTimestamp() + 43200 < time())
        {
            $data = [
                "USER_ID" => $userId,
                "CHANNEL_ID" => self::getNewChannelId(),
                "CHANNEL_TYPE" => $channelType,
                "DATE_CREATE" => new DateTime(),
                "LAST_ID" => 0
            ];
            
            if (isset($result["ID"]))
            {
                ChannelTable::delete($result["ID"]);
            }

            if (ChannelTable::add($data)->isSuccess())
            {
                $event = new Event();
                $event
                    ->addRecipient($data["CHANNEL_ID"])
                    ->setCommand("open");
                
                Manager::getInstance()->sendEvent($event);

                return [
                    "CHANNEL_ID" => $data["CHANNEL_ID"],
                    "CHANNEL_TYPE" => $data["CHANNEL_TYPE"],
                    "CHANNEL_DT" => time(),
                    "LAST_ID" => 0
                ];
            }
            else
            {
                return false;    
            }
        }
        else
        {
            return [
                "CHANNEL_ID" => $result["CHANNEL_ID"],
                "CHANNEL_TYPE" => $result["CHANNEL_TYPE"],
                "CHANNEL_DT" => $result["DATE_CREATE"]->getTimestamp(),
                "LAST_ID" => $result["LAST_ID"]
            ];
        }
    }
    
    public static function getNewChannelId()
    {
        global $APPLICATION;
        return md5(uniqid().$_SERVER["REMOTE_ADDR"].$_SERVER["SERVER_NAME"].(is_object($APPLICATION) ? $APPLICATION->GetServerUniqID() : ""));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	public static function send($channelId, $message)
	{
		if (is_array($channelId))
		{
			$arGroup = array_chunk($channelId, 100);
			$results = array();
			foreach($arGroup as $channels)
			{
				$result = self::sendCommand($channels, $message);
				$subresult = json_decode($result);
				if (is_array($subresult->infos))
				{
					$results = array_merge($results, $subresult->infos);
				}
			}
			$result = json_decode('{"infos":'.json_encode($results).'}');
		}
		else
		{
			$result = self::sendCommand($channelId, $message);
			$result = json_decode('{"infos": [' . $result . ']}');
		}
	
		return $result;
	}
	
	private static function sendCommand($channelId, $message)
	{
		if (!is_array($channelId))
			$channelId = array($channelId);
	
		$channelId = implode('/', array_unique($channelId));
	
		if (strlen($channelId) <= 0 || strlen($message) <= 0)
			return false;
	
		$httpClient = new HttpClient(array(
				'socketTimeout' => 5
		));
	
		$httpClient->post('http://127.0.0.1:8895/bitrix/pub/?CHANNEL_ID='.$channelId, $message);
	
		return $httpClient->getResult();
	}
	
	public static function getChannel($userId)
	{
		$userId = intval($userId);
		return array(
			'CHANNEL_ID' => md5($userId)
		);
	}
	
	public static function getConfig($userId)
	{
		$arChannel = self::getChannel($userId);
		
		if (!is_array($arChannel))
			return false;

		self::send($arChannel['CHANNEL_ID'], Common::jsonEncode(array(
		 				'command' => 'reopen',
						'params' => array()
		)));

		$arChannels = array($arChannel['CHANNEL_ID']);

		return array(
			'CHANNEL_ID' => implode('/', $arChannels),
			'PATH' => (\Bitrix\Main\Context::getCurrent()->getRequest()->isHttps() ? 'https://' : 'http://') . '#DOMAIN#/bitrix/sub/?CHANNEL_ID=' . implode('/', $arChannels)
		);	
	}
}
?>