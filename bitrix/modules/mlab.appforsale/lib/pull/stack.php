<?
namespace Mlab\Appforsale\Pull;

use Bitrix\Main\Config\Option;

class Stack
{
	public static function addByUser($userIds, $arMessage)
	{
        if (!is_array($userIds))
            $userIds = array($userIds);
		
		$channels = array();
		foreach ($userIds as $userId)
		{
			$userId = intval($userId);
			if ($userId != 0)
			{
				$arChannel = Channel::getChannel($userId);
				$channels[$userId] = $arChannel['CHANNEL_ID'];
			}
		}
		
		if (empty($channels))
			return false;
		
		return self::addByChannel($channels, $arMessage);
	}
	
	public static function addByChannel($channelId, $arParams = array())
	{
        if (!is_array($channelId))
            $channelId = array($channelId);

		if (strlen($arParams['command']) > 0)
		{
			$arData = Array(
				'command' => $arParams['command'],
				'params' => is_array($arParams['params']) ? $arParams['params'] : array(),
			);
			
			$command = array('SERVER_TIME_WEB' => time(), 'SERVER_NAME' => Option::get('main', 'server_name', $_SERVER['SERVER_NAME']), 'MESSAGE' => array($arData), 'ERROR' => '');
			$message = \CUtil::PhpToJsObject($command);
			if (!defined('BX_UTF') || !BX_UTF)
				$message = $GLOBALS['APPLICATION']->ConvertCharset($message, SITE_CHARSET, 'utf-8');
			
			return Channel::send($channelId, str_replace("\n", " ", $message)) ? true : false;
		}

        return false;
	}
}
?>