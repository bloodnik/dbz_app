<?
namespace Mlab\Appforsale\Pull\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class ChannelTable extends Entity\DataManager
{
    public static function getMap()
    {
        return [
            new Entity\IntegerField("ID", [
                    "primary" => true,
                    "autocomplete" => true
            ]),
            new Entity\IntegerField("USER_ID", [
                    "required" => true
            ]),
            new Entity\StringField("CHANNEL_TYPE"),
            new Entity\StringField("CHANNEL_ID"),
            new Entity\IntegerField("LAST_ID"),
            new Entity\DatetimeField("DATE_CREATE", [
                    "default_value" => function() { return new DateTime(); }
            ]),
            new Entity\ReferenceField("USER", "Bitrix\Main\UserTable", [
                    "=this.USER_ID" => "ref.ID"
            ])
        ];
    }
    
    public static function getTableName()
    {
        return "mlab_appforsale_pull_channel";
    }
    
    public static function getUfId()
    {
        return "BM_PULL_CHANNEL";
    }
}
?>