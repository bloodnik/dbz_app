<?
namespace Mlab\Appforsale;

use Bitrix\Main\Web\HttpClient,
	Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Web\Json;

Loc::loadMessages(__FILE__);

class YandexMoney
{
	const URL = "https://money.yandex.ru/api/";
	
	function request($method, $arParams)
	{
		$request = new HttpClient();
		return Json::decode($request->post(self::URL.$method, $arParams));
	}
	
	function getInstance()
	{
		global $USER_FIELD_MANAGER, $USER;
		
		$instanceId = $USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_INSTANCE_ID", $USER->GetID());
		
		if (empty($instanceId))
		{
			$arResult = self::request("instance-id", array(
					"client_id" => \COption::GetOptionString("mlab.appforsale", "client_id", "")
			));
	
			if ($arResult["status"] == "success")
			{
				$instanceId = $arResult["instance_id"];
				$USER_FIELD_MANAGER->Update("USER", $USER->GetID(), array("UF_INSTANCE_ID" => $instanceId));
			}
			else
			{
				$instanceId = false;
			}
		}
		
		return $instanceId;
	}
	
	function requestPayment($instanceId, $sum)
	{
		global $USER_FIELD_MANAGER, $USER;

		$arResult = self::request("request-external-payment", array(
				"pattern_id" => "p2p",
				"instance_id" => $instanceId,
				"to" => \COption::GetOptionString("mlab.appforsale", "receiver", ""),
				"amount" => $sum,
			//	"message" => Loc::getMessage("MESSAGE"),
				"label" => $USER->GetID()
		));
			
		if ($arResult["status"] == "success")
		{
			$USER_FIELD_MANAGER->Update("USER", $USER->GetID(), array(
					"UF_REQUEST_ID" => $arResult["request_id"],
					"UF_SUM" => $sum
			));

			return $arResult["request_id"];
		}
		
		return false;
	}
	
	function processPayment($requestId, $instanceId)
	{
		$arResult = self::request("process-external-payment", array(
				"request_id" => $requestId,
				"instance_id" => $instanceId,
				"ext_auth_success_uri" => "http://".SITE_SERVER_NAME."/youdo/personal/payment/result.php?requestId=".$requestId,
				"ext_auth_fail_uri" => "http://".SITE_SERVER_NAME."/youdo/personal/payment/result.php"
		));
		
		if ($arResult["status"] == "ext_auth_required")
			return $arResult["acs_uri"]."?cps_context_id=".$arResult["acs_params"]["cps_context_id"]."&paymentType=".$arResult["acs_params"]["paymentType"];
		else
			return false;
	}
	
	function pay($sum)
	{
		if ($instanceId = self::getInstance())
		{
			if ($requestId = self::requestPayment($instanceId, $sum))
			{
				if ($url = self::processPayment($requestId, $instanceId))
				{
					?>
					<script>
						BX.ready(function() {
							location.replace("<?=$url?>");
						});
					</script>
					<?
				}
			}
		}	
	}
}
?>