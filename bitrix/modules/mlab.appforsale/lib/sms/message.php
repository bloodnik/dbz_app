<?
namespace Mlab\Appforsale\Sms;

class Message
{
    private $body;
    private $recipients;
    
    public function __construct()
    {
        
    }
    
    public function addRecipient($recipient): Message
    {
        $this->recipients[] = $recipient;
        return $this;
    }   
    
    public function getBody(): String
    {
        return $this->body;
    }
    
    public function getRecipients(): Array
    {
        return $this->recipients;
    }
    
    public function setBody(String $body): Message
    {
        $this->body = str_replace("\n", " ", $body);
        return $this;
    }
}
?>