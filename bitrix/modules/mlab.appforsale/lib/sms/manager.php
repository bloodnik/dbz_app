<?
namespace Mlab\Appforsale\Sms;

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Config\Option;

class Manager
{
    private static $instance = null;
    
    private function __construct()
    {
        
    }
    
    public static function getInstance(): Manager
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    public function sendMessage(Message $message): Bool
    {
        $url = Option::get("mlab.appforsale", "url_sms_service", "");
        
        if ($url == "")
            return false;
        
        $url = str_replace(
            [
                "#PHONE#",
                "#CODE#",
                " "
            ],
            [
                implode(";", $message->getRecipients()),
                $message->getBody(),
                "%20"
            ],
            $url
        );

        $request = new HttpClient();
        $request->post($url);
        
        return true;
    }
}
?>