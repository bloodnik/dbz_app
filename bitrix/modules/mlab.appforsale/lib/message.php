<?
namespace Mlab\Appforsale;

use Bitrix\Main,
	Bitrix\Main\Application,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class MessageTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'appforsale_im_message';
	}
	
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('id', array(
					'primary' => true,
					'autocomplete' => true
			)),
			new Entity\IntegerField('user_id'),
			new Entity\IntegerField('from_id'),
			new Entity\DatetimeField('date', array(
					'default_value' => new Main\Type\DateTime()
			)),
			new Entity\IntegerField('read_state'),
			new Entity\IntegerField('out'),
			new Entity\TextField('body', array(
					'required' => true
			)),
			new Entity\IntegerField('important'),
			new Entity\IntegerField('deleted'),
			new Entity\IntegerField('random_id')
		);
	}
	
	public static function createTable()
	{	
	//	echo '=';
		//echo static::getTableName();
		
// 		if (!Application::getConnection()->isTableExists(static::getTableName()))
// 		{
 			static::getEntity()->createDbTable();
// 			return true;
// 		}
// 		else
// 			return false;
	
	
	}
	
 	public static function dropTable(){
 		$connection = Application::getInstance()->getConnection();
	
		$connection->dropTable(static::getTableName());
// 		return true;
 	}
}
?>