<?
namespace Mlab\Appforsale\Push\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class DeviceTable extends Entity\DataManager
{
    public static function formatName($data)
    {
        $mess = [
            // iPhone
            "iPhone11,8" => "iPhone XR",
            "iPhone11,2" => "iPhone XS",
            "iPhone11,4" => "iPhone XS Max",
            "iPhone11,6" => "iPhone XS Max",
            "iPhone10,6" => "iPhone X",
            "iPhone10,5" => "iPhone X",
            "iPhone10,1" => "iPhone 8",
            "iPhone10,4" => "iPhone 8",
            "iPhone10,2" => "iPhone 8 Plus",
            "iPhone10,5" => "iPhone 8 Plus",
            "iPhone9,1" => "iPhone 7",
            "iPhone9,3" => "iPhone 7",
            "iPhone9,2" => "iPhone 7 Plus",
            "iPhone9,4" => "iPhone 7 Plus",
            "iPhone8,4" => "iPhone SE",
            "iPhone8,1" => "iPhone 6s",
            "iPhone8,2" => "iPhone 6s Plus",
            "iPhone7,2" => "iPhone 6",
            "iPhone7,1" => "iPhone 6 Plus",
            "iPhone6,2" => "iPhone 5s",
            "iPhone6,1" => "iPhone 5S",
            "iPhone5,3" => "iPhone 5c",
            "iPhone5,4" => "iPhone 5c",
            "iPhone5,1" => "iPhone 5",
            "iPhone5,2" => "iPhone 5",
            "iPhone4,1" => "iPhone 4s",
            "iPhone3,1" => "iPhone 4",
            "iPhone3,3" => "iPhone 4",
            "iPhone3,2" => "iPhone 4",
            "iPhone2,1" => "iPhone 3GS",
            "iPhone1,2" => "iPhone 3G",
            "iPhone1,1" => "iPhone 2G",
            
            //iPod
            "iPod1,1" => "iPod touch 1G",
            "iPod2,1" => "iPod touch 2G",
            "iPod3,1" => "iPod touch 3G",
            "iPod4,1" => "iPod touch 4G",
            "iPod5,1" => "iPod touch 5G",
            "iPod7,1" => "iPod touch 6G",

            // iPad
            "iPad1,1" => " iPad (1G)",
            "iPad2,1" => "iPad 2 (Wi-Fi)",
            "iPad2,2" => "iPad 2 (GSM)",
            "iPad2,3" => "iPad 2 (CDMA)",
            "iPad2,4" => "iPad 2 (Rev A, 2012)",
            "iPad3,1" => "iPad 3 (Wi-Fi)",
            "iPad3,2" => "iPad 3 (CDMA)",
            "iPad3,3" => "iPad 3 (GSM",
            "iPad3,4" => "iPad 4 (Wi-Fi)",
            "iPad3,5" => "iPad 4 (GSM)",
            "iPad3,6" => "iPad 4 (GSM+CDMA)",
            "iPad4,1" => " iPad Air (Wi-Fi)",
            "iPad4,2" => "iPad Air (GSM)",
            "iPad4,3" => "iPad Air (TD-LTE)",
            "iPad5,3" => "iPad Air 2 (Wi-Fi)",
            "iPad5,4" => "iPad Air 2 (GSM)",
            "iPad6,7" => "iPad Pro (Wi-Fi)",
            "iPad6,8" => "iPad Pro (GSM)",
            "iPad6,3" => "iPad Pro (9.7) (Wi-Fi)",
            "iPad6,4" => "iPad Pro (9.7) (GSM)",
            "iPad6,11" => "iPad 5 (Wi-Fi)",
            "iPad6,12" => "iPad 5 (GSM)",
            "iPad7,1" => "iPad Pro 2 (Wi-Fi)",
            "iPad7,2" => "iPad Pro 2 (GSM)",
            "iPad7,3" => "iPad Pro (10.5) (Wi-Fi)",
            "iPad7,4" => "iPad Pro (10.5)",
            "iPad7,5" => "iPad 6 (Wi-Fi)",
            "iPad7,6" => "iPad 6 (GSM)",
            "iPad2,5" => "iPad mini (Wi-Fi)",
            "iPad2,6" => "iPad mini (GSM)",
            "iPad2,7" => "iPad mini (GSM+CDMA)",
            "iPad4,4" => "iPad mini 2 (Wi-Fi)",
            "iPad4,5" => "iPad mini 2 (GSM)",
            "iPad4,6" => "iPad mini 2 (TD-LTE)",
            "iPad4,7" => "iPad mini 3 (Wi-Fi)",
            "iPad4,8" => "iPad mini 3 (GSM)",
            "iPad4,9" => "iPad mini 3 (TD-LTE",
            "iPad5,1" => " iPad mini 4 (Wi-Fi)",
            "iPad5,2" => "iPad mini 4 (GSM)"
        ];
        
        if (array_key_exists($data, $mess))
        {
            return $mess[$data];
        }
        
        return $data;
    }
    
    public static function getMap()
    {
        return [
            new Entity\IntegerField("ID", [
                    "primary" => true,
                    "autocomplete" => true
            ]),
            new Entity\DatetimeField("DATE_CREATE", [
                    "default_value" => function() { return new DateTime(); }
            ]),
            new Entity\DatetimeField("TIMESTAMP_X", [
                "default_value" => function() { return new DateTime(); }
            ]),            
            new Entity\StringField("APP_ID", [
                    "validation" => [__CLASS__, "validateAppId"]
            ]),
            new Entity\StringField("DEVICE_ID", [
                    "validation" => [__CLASS__, "validateDeviceId"]
            ]),
            new Entity\StringField("DEVICE_MODEL", [
                    "validation" => [__CLASS__, "validateDeviceModel"]
            ]),
            new Entity\StringField("SYSTEM_VERSION", [
                    "validation" => [__CLASS__, "validateSystemVersion"]
            ]),
            new Entity\StringField("SETTINGS", [
                    "serialized" => true
            ]),
            new Entity\StringField("TOKEN", [
                    "validation" => [__CLASS__, "validateToken"]
            ]),
            new Entity\IntegerField("USER_ID"),
            new Entity\DatetimeField("DATE_AUTH"),
            new Entity\ReferenceField("USER", "Bitrix\Main\UserTable", [
                    "=this.USER_ID" => "ref.ID"
            ])  
        ];
    }
    
    public static function getTableName()
    {
        return "mlab_appforsale_push_device";
    }
    
    public static function getUfId()
    {
        return "BM_PUSH_DEVICE";
    }
    
    public static function validateAppId()
    {
        return [
            new Entity\Validator\Length(null, 50)
        ];
    }
    
    public static function validateDeviceId()
    {
        return [
            new Entity\Validator\Length(null, 255)
        ];
    }
    
    public static function validateDeviceModel()
    {
        return [
            new Entity\Validator\Length(null, 255)
        ];
    }
    
    public static function validateSystemVersion()
    {
        return [
            new Entity\Validator\Length(null, 50)
        ];
    }
    
    public static function validateToken()
    {
        return [
            new Entity\Validator\Length(null, 255)
        ];
    }
}
?>