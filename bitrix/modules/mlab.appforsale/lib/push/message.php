<?
namespace Mlab\Appforsale\Push;

class Message
{
    private $badge = -1;
    private $body = "";
    private $category = "";
    private $data = [];
    private $sound = "";
    private $title = "";
    private $userId;
    
    public function __construct()
    {
        
    }
    
    public function getBadge(): Int
    {
        return $this->badge;
    }
    
    public function getPayload(): Array
    {
        $payload = [];
        
        if ($this->getBadge() >= 0)
        {
            $payload["notification"]["badge"] = $this->getBadge();
        }
        else
        {
            $payload["notification"]["badge"] = Badge::get($this->getUser());
        }

        if (!empty($this->getBody()))
            $payload["notification"]["body"] = $this->getBody();
        
        if (!empty($this->getCategory()))
            $payload["notification"]["category"] = $this->getCategory();
        
        if (!empty($this->getData()))
            $payload["data"] = $this->getData();
  
        if (!empty($this->getSound()))
            $payload["notification"]["sound"] = $this->getSound();
            
        if (!empty($this->getTitle()))
            $payload["notification"]["title"] = $this->getTitle();
                        
        return $payload;
    }
    
    public function getBody(): String
    {
        return $this->body;
    }
    
    public function getCategory(): String
    {
        return $this->category;
    }
    
    public function getData(): Array
    {
        return $this->data;
    }
        
    public function getSound(): String
    {
        return $this->sound;
    }
    
    public function getTitle(): String
    {
        return $this->title;
    }
    
    public function getUser(): Int
    {
        return $this->userId;
    }
            
    public function setBadge(Int $badge): Message
    {
        $this->badge = $badge;
        return $this;
    }
    
    public function setCategory(String $category): Message
    {
        $this->category = $category;
        return $this;
    }
    
    public function setBody(String $body): Message
    {
        $this->body = str_replace("\n", " ", $body);        
        return $this;
    }
    
    public function setData(Array $data): Message
    {
        $this->data = $data;
        return $this;
    }
    
    public function setSound(String $sound = "default"): Message
    {
        $this->sound = $sound;
        return $this;
    }
    
    public function setTitle(String $title): Message
    {
        $this->title = $title;
        return $this;
    }
    
    public function setUser(Int $userId): Message
    {
        $this->userId = $userId;
        return $this;
    }  
}
?>