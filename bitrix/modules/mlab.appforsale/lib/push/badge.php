<?
namespace Mlab\Appforsale\Push;

use Bitrix\Main\Event;
use Bitrix\Main\Context;
use Bitrix\Main\EventResult;

class Badge
{
    public static function get(Int $userId)
    {
        $badge = 0;
        
        $siteId = Context::getCurrent()->getSite();
        
        $event = new Event("mlab.appforsale", "onGetBadge", [
                "USER_ID" => $userId,
                "SITE_ID" => $siteId
        ]);
        $event->send();
        
        foreach ($event->getResults() as $eventResult)
        {
            if ($eventResult->getType() != EventResult::SUCCESS)
                continue;

            $result = $eventResult->getParameters();
  
            if (intval($result["BADGE"]) > 0)
            {
                $badge += $result["BADGE"];
            }
        }
                
        return $badge;
    }
}
?>