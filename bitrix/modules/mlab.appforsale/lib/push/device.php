<?
namespace Mlab\Appforsale\Push;

use Bitrix\Main\HttpRequest;

class Device
{
    private $appId;
    private $deviceId;
    private $deviceModel;
    private $systemVersion;
    private $token;
    
    public function __construct()
    {
        
    }
    
    public static function createFromRequest(HttpRequest $request)
    {
        $device = new static();
        
        $device
            ->setAppId($request->get("app_id"))
            ->setDeviceId($request->get("device_id"))
            ->setDeviceModel($request->get("device_model"))
            ->setSystemVersiob($request->get("system_version"))
            ->setToken($request->get("token"));
        
        return $device;
    }
    
    public function getAppId(): String
    {
        return $this->appId;
    }
    
    public function getDeviceId(): String
    {
        return $this->deviceId;
    }
    
    public function getDeviceModel(): String
    {
        return $this->deviceModel;
    }
    
    public function getSystemVersion(): String
    {
        return $this->systemVersion;
    }
    
    public function getToken(): String
    {
        return $this->token;
    }
    
    public function setAppId(String $appId): Device
    {
        $this->appId = $appId;
        return $this;
    }
    
    public function setDeviceId(String $deviceId): Device
    {
        $this->deviceId = $deviceId;
        return $this;
    }
    
    public function setDeviceModel(String $deviceModel): Device
    {
        $this->deviceModel = $deviceModel;
        return $this;
    }
    
    public function setSystemVersiob(String $systemVersion): Device
    {
        $this->systemVersion = $systemVersion;
        return $this;
    }
    
    public function setToken(String $token): Device
    {
        $this->token = $token;
        return $this;
    }
}
?>