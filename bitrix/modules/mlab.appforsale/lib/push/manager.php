<?
namespace Mlab\Appforsale\Push;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Event;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Json;
use Mlab\Appforsale\MobileApp;
use Mlab\Appforsale\Push\Entity\DeviceTable;
use Bitrix\Main\ORM\Query\Query;

Loc::loadMessages(__FILE__);

class Manager
{
    private static $instance = null;
    
    private function __construct()
    {
        
    }
        
    public static function getInstance(): Manager
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }
        
        return static::$instance;
    }
    
    public static function onUserLogin($userId)
    {
        $mobileApp = MobileApp::getInstance();
        if ($mobileApp->isDevice())
        {
            $query = new Query(DeviceTable::getEntity());
            $query
                ->addSelect("ID")
                ->addFilter("DEVICE_ID", $mobileApp->getDeviceId())
                ->setLimit(1);
            
            if ($row = $query->exec()->fetch())
            {
                DeviceTable::update($row["ID"], [
                        "USER_ID" => $userId,
                        "DATE_AUTH" => new DateTime()
                ]);
            }
        }
    }
    
    public static function onUserLogout($userId)
    {
        $mobileApp = MobileApp::getInstance();
        if ($mobileApp->isDevice())
        {
            $query = new Query(DeviceTable::getEntity());
            $query
                ->addSelect("ID")
                ->addFilter("DEVICE_ID", $mobileApp->getDeviceId())
                ->setLimit(1);
            
            if ($row = $query->exec()->fetch())
            {
                DeviceTable::update($row["ID"], [
                        "USER_ID" => "",
                        "DATE_AUTH" => ""
                ]);
            }
        }
    }
    
    public function registerDevice(Device $device)
    {
        global $USER;
        
        $data = [
            "TIMESTAMP_X" => new DateTime(),
            "APP_ID" => $device->getAppId(),
            "DEVICE_ID" => $device->getDeviceId(),
            "DEVICE_MODEL" => $device->getDeviceModel(),
            "SYSTEM_VERSION" => $device->getSystemVersion(),
            "TOKEN" => $device->getToken()
        ];
        
        if ($USER->IsAuthorized())
        {
            $data["USER_ID"] = $USER->GetID();
            $data["DATE_AUTH"] = new DateTime();
        }
        
        $query = new Query(DeviceTable::getEntity());
        $query
            ->addSelect("ID")
            ->addFilter("DEVICE_ID", $device->getDeviceId())
            ->setLimit(1);
        
        if ($row = $query->exec()->fetch())
        {
            $result = DeviceTable::update($row["ID"], $data);
        }
        else
        {
            $result = DeviceTable::add($data);
        }
        
        if (!$result->isSuccess())
        {
            throw new SystemException(implode(", ", $result->getErrorMessages()));
        }
        
        $event = new Event("mlab.appforsale", "onRegisterDevice", ["ENTITY" => $device]);
        $event->send();
    }
    
    private function sendBatch(Array $batch): Array
    {
        $authKey = Option::get("mlab.appforsale", "google_push_key", "");
        
        if ($authKey == "")
            return [];
    
        $request = new HttpClient();
            
        $request->setHeader("Content-Type", "application/json");
        $request->setHeader("Authorization", "key=" . $authKey);

        $request->post("https://fcm.googleapis.com/fcm/send", Json::encode($batch));
            
        return Json::decode($request->getResult());
    }
    
    public function sendMessage(Message $message): Bool
    {        
        $query = new Query(DeviceTable::getEntity());
        $query
            ->setSelect(["ID", "TOKEN"])
            ->addFilter("=USER_ID", $message->getUser());

        $result = $query->exec();
        $tokens = [];
        while ($data = $result->fetch())
        {
            $tokens[$data["ID"]] = $data["TOKEN"];
        }
        
        if (empty($tokens))
        {
            return false;
        }

        $batches = array_chunk($tokens, 1000, true);

        $payload = $message->getPayload();
        foreach ($batches as $batch)
        {
            $response = $this->sendBatch(array_merge(
                ["registration_ids" => array_values($batch)],
                $payload
            ));
            
            if (!empty($response) && array_key_exists("results", $response))
            {
                $ids = array_keys($batch);
                
                foreach ($response["results"] as $i => $result)
                {
                    if (array_key_exists("error", $result))
                    {
                        switch ($result["error"])
                        {
                            case "NotRegistered":
                            case "InvalidParameters":
                                DeviceTable::delete($ids[$i]);
                                break;
                        }
                    }
                }
            }
        }
            
        return true;
    } 
}
?>