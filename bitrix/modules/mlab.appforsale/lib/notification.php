<?
namespace Mlab\Appforsale;

use Bitrix\Main\Loader;

class Notification
{
	function OnAfterUserDelete($user_id)
	{
		if(Loader::includeModule('iblock'))
		{
//			$dbElement = \CIBlockElement::GetList(
//				array(),
//				array(
//					'IBLOCK_TYPE' => 'notifications',
//					'IBLOCK_CODE' => 'notification',
//					'CREATED_BY' => $user_id
//				),
//				false,
//				false,
//				array(
//					'ID'
//				)
//			);
//			while($arElement = $dbElement->GetNext())
//			{
//				\CIBlockElement::Delete($arElement['ID']);
//			}

			// Получаем все уведолмения
			$entity_data_class = getHighloadEntity(1);
			$rsData = $entity_data_class::getList(array(
				"select"      => array("*"),
				"order"       => array("ID" => "DESC"),
				"filter"      => array("UF_CREATED_BY" => $user_id),  // Задаем параметры фильтра выборки
			));


			while ($arData = $rsData->Fetch()) {
				$entity_data_class::delete($arData['ID']);
			}
		}
	}	
}
?>