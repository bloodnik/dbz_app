<?
namespace Mlab\Appforsale\Component;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

abstract class Base extends \CBitrixComponent
{
	const SORT_ORDER_MASK = '/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i';
	protected $navParams = false;
	protected $elements = array();
	
	public function onPrepareComponentParams($arParams)
	{		
		$arParams['IBLOCK_TYPE'] = isset($arParams['IBLOCK_TYPE']) ? trim($arParams['IBLOCK_TYPE']) : '';
		$arParams['IBLOCK_ID'] = isset($arParams['IBLOCK_ID']) ? (int) $arParams['IBLOCK_ID'] : 0;		
		
		$arParams['PARENT_SECTION'] = isset($arParams['PARENT_SECTION']) ? (int) $arParams['PARENT_SECTION'] : 0;
		$arParams['PARENT_SECTION_CODE'] = isset($arParams['PARENT_SECTION_CODE']) ? trim($arParams['PARENT_SECTION_CODE']) : '';
		
		$arParams['CACHE_TIME'] = isset($arParams['CACHE_TIME']) ? (int) $arParams['CACHE_TIME'] : 36000000;
		
		$arParams['DETAIL_URL'] = isset($arParams['DETAIL_URL']) ? trim($arParams['DETAIL_URL']) : '';
		$arParams['SECTION_URL'] = isset($arParams['SECTION_URL']) ? trim($arParams['SECTION_URL']) : '';
		$arParams['LIST_URL'] = isset($arParams['LIST_URL']) ? trim($arParams['LIST_URL']) : '';
		
		if (empty($arParams['SORT_FIELD']))
		{
			$arParams['SORT_FIELD'] = 'sort';
		}
		
		if (!preg_match(self::SORT_ORDER_MASK, $arParams['SORT_ORDER']))
		{
			$arParams['SORT_ORDER'] = 'asc';
		}
		
		if (empty($arParams['SORT_FIELD2']))
		{
			$arParams['SORT_FIELD2'] = 'id';
		}
		
		if (!preg_match(self::SORT_ORDER_MASK, $arParams['SORT_ORDER2']))
		{
			$arParams['SORT_ORDER2'] = 'desc';
		}
		
		if (!is_array($arParams['FIELD_CODE']))
			$arParams['FIELD_CODE'] = array();
		foreach ($arParams['FIELD_CODE'] as $key => $val)
			if (!$val)
				unset($arParams['FIELD_CODE'][$key]);

		if (!is_array($arParams['PROPERTY_CODE']))
			$arParams['PROPERTY_CODE'] = array();
		foreach ($arParams['PROPERTY_CODE'] as $key => $val)
			if ($val === '')
				unset($arParams['PROPERTY_CODE'][$key]);
			
			
		$arParams['MESS_LIST_EMPTY'] = $arParams['MESS_LIST_EMPTY'] ?: Loc::getMessage('LIST_EMPTY');
			
		return $arParams;
	}
	
	abstract protected function getAdditionalCacheId();
	
	public function executeComponent()
	{
		$AJAX_ID = $this->GetEditAreaId('ajax');

		global $APPLICATION, $USER;
				
		if ($this->request->isAjaxRequest() && $this->request->get('AJAX_ID') == $AJAX_ID)
			$APPLICATION->RestartBuffer();
		
		if ($this->startResultCache(false, $this->getAdditionalCacheId()))
		{
			if(!Loader::includeModule('iblock'))
			{
 				$this->abortResultCache();
				return;
			}

			if ($this->arResult = \CIBlock::GetArrayByID($this->arParams['IBLOCK_ID']))
			{
				$arSelect = array_merge($this->arParams['FIELD_CODE'], $this->getSelect());
				$bGetProperty = count($this->arParams['PROPERTY_CODE']) > 0;
				if ($bGetProperty)
					$arSelect[] = 'PROPERTY_*';
				
				$this->arParams['PARENT_SECTION'] = \CIBlockFindTools::GetSectionID(
					$this->arParams['PARENT_SECTION'],
					$this->arParams['PARENT_SECTION_CODE'],
					array(
						'GLOBAL_ACTIVE' => 'Y',
						'IBLOCK_ID' => $this->arResult['ID'],
					)
				);
				
				$arFilter = $this->getFilter();
				
				if ($this->arParams['PARENT_SECTION'] > 0)
				{
					$arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION'];
					$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
				}
				
				if ($this->arParams['ELEMENT_ID'] > 0)
				{
					$arFilter['ID'] = $this->arParams['ELEMENT_ID'];
				}

				$arUserID = array();
	
				$obParser = new \CTextParser;
				$this->arResult['ELEMENTS'] = array();
				$dbElement = \CIBlockElement::GetList($this->getSort(), $arFilter, $this->getGroup(), $this->navParams, $arSelect);
				$dbElement->SetUrlTemplates($this->arParams['DETAIL_URL'], $this->arParams['SECTION_URL'], $this->arParams['LIST_URL']);
				while ($obElement = $dbElement->GetNextElement())
				{
					$arItem = $obElement->GetFields();

					$arUserID[$arItem['CREATED_BY']] = true;
					
					$dbSection = \CIBlockSection::GetNavChain($arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID']);
					while ($arSection = $dbSection->Fetch())
					{
						$arItem['IBLOCK_SECTION'] = $arSection;
					}
						
					$arButtons = \CIBlock::GetPanelButtons(
							$arItem['IBLOCK_ID'],
							$arItem['ID'],
							intval($arItem['IBLOCK_SECTION_ID']),
							array('SECTION_BUTTONS' => true, 'SESSID' => false)
					);
					$this->AddEditAction($arItem['ID'], $arButtons['edit']['edit_element']['ACTION_URL'], $this->arResult['ELEMENT_EDIT']);
					$this->AddDeleteAction($arItem['ID'], $arButtons['edit']['delete_element']['ACTION_URL'], $this->arResult['ELEMENT_DELETE'], array('CONFIRM' => Loc::getMessage('ELEMENT_DELETE_CONFIRM')));
					
					$arItem['FIELDS'] = array();
					foreach ($this->arParams['FIELD_CODE'] as $code)
						if (array_key_exists($code, $arItem))
							$arItem['FIELDS'][$code] = $arItem[$code];
					
					if ($bGetProperty)
						$arItem['PROPERTIES'] = $obElement->GetProperties();
						
					$arLink = array();
					foreach ($arItem['PROPERTIES'] as $pid => $value)
					{
						$arLink[$value['ID']] = $pid;
					}
					
					$arItem['DISPLAY_PROPERTIES'] = array();					
					foreach ($this->arParams['PROPERTY_CODE'] as $pid)
					{
						$prop = &$arItem['PROPERTIES'][$arLink[$pid]];
						if (
							(is_array($prop['VALUE']) && count($prop['VALUE']) > 0)
							|| (!is_array($prop['VALUE']) && strlen($prop['VALUE']) > 0)
						)
						{
							$arItem['DISPLAY_PROPERTIES'][$pid] = \CIBlockFormatProperties::GetDisplayValue($arItem, $prop);
							switch ($prop['PROPERTY_TYPE'])
							{
								case 'E':
								case 'G':
									if (is_array($arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE']))
									{
										foreach ($arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] as $i=>$value)
										{
											$arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'][$i] = strip_tags($value);
										}
									}
									else
									{
										$arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] = strip_tags($arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE']);
									}
										
// 									$arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE']
// 									foreach ($arItem['DISPLAY_PROPERTIES'][$pid]['VALUE'] as $v)
// 									{
										
// 									}
// 									echo '<pre>';
// 									print_r($arItem['DISPLAY_PROPERTIES'][$pid]);
// 									echo '</pre>';
									
// 									$arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] = $arItem['DISPLAY_PROPERTIES'][$pid]['LINK_ELEMENT_VALUE'][$arItem['DISPLAY_PROPERTIES'][$pid]['VALUE']]['NAME'];
									break;
								case 'F':
									$str = '';
									foreach ($prop['VALUE'] as $file_id)
									{
										$arFile = \CFile::ResizeImageGet($file_id, array('width' => 128, 'height' => 128), BX_RESIZE_IMAGE_EXACT, true);
										$str .= '<img src="'.$arFile['src'].'" /> ';
									}
									$arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] = $str;
									break;
							}
							
							foreach ($arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'] as $key=>$value)
							{
								if (empty($value))
									unset($arItem['DISPLAY_PROPERTIES'][$pid]['DISPLAY_VALUE'][$key]);
							}
						}
					}
					
	
					$this->elements[] = $arItem;
					$this->arResult['ELEMENTS'][] = $arItem['ID'];
				}
								
				if (!empty($arUserID))
				{
// 					$dbUser = UserTable::getList(array(
// 						'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'IS_ONLINE', 'LAST_ACTIVITY_DATE', 'UF_CITY', 'UF_RATING'),
// 						'filter' => array('ID' => array_keys($arUserID)),
// 					));

					$dbUser = \CUser::GetList($b, $o, array('ID' => array_keys($arUserID)), array('SELECT' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'IS_ONLINE', 'LAST_ACTIVITY_DATE', 'UF_CITY', 'UF_RATING'), 'ONLINE_INTERVAL' => 900));
					$nameFormat = \CSite::GetNameFormat(true);
					while ($arUser = $dbUser->fetch())
					{
						$this->arResult['ONLINE'][$arUser['ID']] = ($arUser['IS_ONLINE'] == 'Y');
						$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
						$arUser['FORMAT_NAME'] = \CUser::FormatName($nameFormat, $arUser);
						$this->arResult['USERS'][$arUser['ID']] = $arUser;
					}
				}
				
				foreach ($this->elements as &$arItem)
				{
					$arItem['USER'] = $this->arResult['USERS'][$arItem['CREATED_BY']];
				}
				
				$this->makeOutputResult();
							
				ob_start();
				$APPLICATION->IncludeComponent(
						'mlab:appforsale.pagenavigation',
						'',
						array(
							'NAV_RESULT' => $dbElement,
							'AJAX_ID' => $AJAX_ID
						),
						$this,
						array(
							'HIDE_ICONS' => 'Y'
						)
				);
				$this->arResult['NAV_STRING'] = ob_get_contents();
				ob_end_clean();
		
				
 				$this->setResultCacheKeys($this->getCacheKeys()); 				
 				$this->includeComponentTemplate();
			}
			else
			{
 				$this->abortResultCache();
			}
		}
		else
		{
// 			$dbUser = UserTable::getList(array(
// 					'select' => array('ID', 'IS_ONLINE'),
// 					'filter' => array('ID' => array_keys($this->arResult['ONLINE'])),
// 			));
			$dbUser = \CUser::GetList($b, $o, array('ID' => array_keys($arUserID)), array('SELECT' => array('IS_ONLINE'), 'ONLINE_INTERVAL' => 900));
				
			while ($arUser = $dbUser->fetch())
			{
				$this->arResult['ONLINE'][$arUser['ID']] = ($arUser['IS_ONLINE'] == 'Y');
			}
		}

		if (!empty($this->arResult['ONLINE']))
		{
			echo '<script>';
			foreach ($this->arResult['ONLINE'] as $uid=>$isOnline)
			{
				if ($isOnline)
				{
					?>
					var ob = document.getElementsByClassName('user-<?=$uid?>');
					for (var i = 0; i < ob.length; i++)
					{
	   					BX.addClass(ob[i], 'online');
					}
					<?
				}
			}
			echo '</script>';
		}
		
		if ($this->request->isAjaxRequest() && $this->request->get('AJAX_ID') == $AJAX_ID)
			die();
		
		if(isset($this->arResult['ID']))
		{
			if($USER->IsAuthorized())
			{
				if($APPLICATION->GetShowIncludeAreas())
				{
					if (Loader::includeModule('iblock'))
					{					
						$arButtons = \CIBlock::GetPanelButtons(
							$this->arResult['ID'],
							0,
							$this->arParams['PARENT_SECTION'],
							array('SECTION_BUTTONS' => true)
						);
									
						$this->addIncludeAreaIcons(\CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
					}
				}
			}
			
			if ($this->arParams['SET_TITLE'])
			{
				$APPLICATION->SetTitle($this->arResult['META_TAGS']['TITLE']);
			}
					
			return $this->arResult['ELEMENTS'];
		}
	}
	
	protected function getSelect()
	{
		return array(
			'ID',
			'IBLOCK_ID',
			'NAME',
			'CREATED_BY',
			'DATE_CREATE',
			'IBLOCK_SECTION_ID',
			'PREVIEW_TEXT'
		);
	}
	
	protected function getFilter()
	{
		return array(
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'IBLOCK_LID' => $this->getSiteId(),
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'CHECK_PERMISSIONS' => 'Y',
			'MIN_PERMISSION' => 'R'
		);
	}
	
	protected function getSort()
	{
		return array(
			$this->arParams['SORT_FIELD'] => $this->arParams['SORT_ORDER'],
			$this->arParams['SORT_FIELD2'] => $this->arParams['SORT_ORDER2']
		);
	}
	
	protected function getGroup()
	{
		return false;
	}

	protected function getCacheKeys()
	{
		$resultCacheKeys = array(
				'ID',
				'ELEMENTS',
				'ONLINE',
				'DATE_CREATE',
				'SHOW_COUNTER'
		);
		
		$this->arResult['META_TAGS'] = array();
		$resultCacheKeys[] = 'META_TAGS';
		
		$this->arResult['META_TAGS']['TITLE'] = $this->arResult['NAME'];
		
		return $resultCacheKeys;
	}
	
	protected function makeOutputResult()
	{
		
	}
	
// 	protected function setElementPanelButtons(&$element)
// 	{
// 		$buttons = \CIBlock::GetPanelButtons(
// 			$element['IBLOCK_ID'],
// 			$element['ID'],
// 			$element['IBLOCK_SECTION_ID'],
// 			array('SECTION_BUTTONS' => false, 'SESSID' => false, 'CATALOG' => false)
// 		);
// 		$element['EDIT_LINK'] = $buttons['edit']['edit_element']['ACTION_URL'];
// 		$element['DELETE_LINK'] = $buttons['edit']['delete_element']['ACTION_URL'];
// 	}
}
?>