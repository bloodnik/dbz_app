<?
namespace Mlab\Appforsale;

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Iblock
{	
	function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'iblock',
			'DESCRIPTION' => Loc::getMessage('IBLOCK_DESCRIPTION'),
			'GetPublicEditHTML' => array('Mlab\Appforsale\Iblock', 'GetPublicEditHTML'),
				
				
			'GetPropertyFieldHtml' => array('Mlab\Appforsale\Iblock', 'GetPropertyFieldHtml'),
		
// 				//'GetPropertyFieldHtmlMulty' => array('Mlab\Appforsale\Address', 'GetPropertyFieldHtmlMulty'),
// // 			'GetAdminListViewHTML' => array('Mlab\Appforsale\Address', 'GetAdminListViewHTML'),
 			'PrepareSettings' => array('Mlab\Appforsale\Iblock', 'PrepareSettings'),
			'GetSettingsHTML' => array('Mlab\Appforsale\Iblock', 'GetSettingsHTML'),
				

//  			'ConvertToDB' => array('Mlab\Appforsale\Address', 'ConvertToDB'),
// 			'ConvertFromDB' => array('Mlab\Appforsale\Address', 'ConvertFromDB'),
			'GetPublicViewHTML' => array('Mlab\Appforsale\Iblock', 'GetPublicViewHTML'),
// 			//'GetPublicViewHTMLMulty' => array('Mlab\Appforsale\Address', 'GetPublicViewHTMLMulty'),
				
				
				
// 			'GetPublicEditHTML' => array('Mlab\Appforsale\Address', 'GetPublicEditHTML'),
// 			//'GetPublicEditHTMLMulty' => array('Mlab\Appforsale\Address', 'GetPublicEditHTMLMulty'),
		);
	}
	
	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		$val = Loc::getMessage('SELECT');
		if (intval($value['VALUE']) > 0)
		{
			if(Loader::includeModule('iblock'))
			{
				$dbElement = \CIBlockElement::GetByID($value['VALUE']);
				if($arElement = $dbElement->GetNext())
				{
					$val = $arElement['NAME'];
				}
			}
		}
		else
		{
			if ($arProperty['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID'] == 'locations')
			{
				global $USER;
				$dbUser = $USER->GetByID($USER->GetID());
				if ($arUser = $dbUser->Fetch())
				{
					$dbElement = \CIBlockElement::GetByID($arUser['UF_CITY']);
					if($arElement = $dbElement->GetNext())
					{
						$value['VALUE'] = $arElement['ID'];
						$val = $arElement['NAME'];
					}
				}
			}
		}
		
		ob_start();
		?>
			<input type="hidden" id="address_<?=$arProperty['ID']?>_value" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>">
			<span class="btn btn-default btn-block" id="address_<?=$arProperty['ID']?>_name" onclick="showE(<?=$arProperty['ID']?>, 0)"><?=$val?></span>
		<?
		$resultHtml = ob_get_contents();
		ob_end_clean();
		return $resultHtml;
	}
	
	function GetSettingsHTML($arUserField, $arHtmlControl, $bVarsFromForm)
	{	

		
		
		if($bVarsFromForm)
            $iblock_id = $GLOBALS[$arHtmlControl["NAME"]]["IBLOCK_ID"];
        elseif(is_array($arUserField))
            $iblock_id = $arUserField["USER_TYPE_SETTINGS"]["IBLOCK_ID"];
        else
            $iblock_id = "";
        
		return '<tr>
					<td>IBLOCK:</td>
                	<td>
                    '.GetIBlockDropDownList($iblock_id, $arHtmlControl["NAME"].'[IBLOCK_TYPE_ID]', $arHtmlControl["NAME"].'[IBLOCK_ID]', false, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"').'
               		 </td>
            </tr>
            ';
	}
		
	
	function PrepareSettings($arProperty)
	{
		return $arProperty;
	}
	
	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		if(Loader::includeModule('iblock'))
		{
			$dbElement = \CIBlockElement::GetByID($value['VALUE']);
			if($arElement = $dbElement->GetNext())
			{
				$strResult = '';
				$dbSection = \CIBlockSection::GetNavChain($arElement['IBLOCK_ID'], $arElement['IBLOCK_SECTION_ID'], array('NAME'));
				while ($arSection = $dbSection->GetNext())
				{
					if (strlen($strResult) > 0)
						$strResult .= ' ';
					$strResult .= $arSection['NAME'];
				}
				if (strlen($strResult) > 0)
					$strResult .= ' ';
				$strResult .= $arElement['NAME'];
				return $strResult;
			}
		}
		return $value['VALUE'];
	}
	
	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		// 		global $APPLICATION;
		ob_start();
		?>
			
			<input type="text" size="20" name="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>" />
			
			<?
			$content = ob_get_contents();
			ob_end_clean();
	
	// // 		if ($arProperty['MULTIPLE'] == 'Y')
	// // 			$googleMapLastNumber++;
			
			return $content;
			//return '<input type="text" size="20" name="'.$strHTMLControlName['VALUE'].'" value="'.$value['VALUE'].'" />';
		}
	
}
?>