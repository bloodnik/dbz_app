<?
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

class PayvideocallsPaymentReceive extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{		
		global $USER, $USER_FIELD_MANAGER;
		
		$requestId = $_GET["requestId"];
		
		if (!empty($requestId))
		{
			
			$request_id = $USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_REQUEST_ID", $USER->GetID());
			$sum = $USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_SUM", $USER->GetID());
			
			if ($request_id == $requestId)
			{
				CAppforsaleUserAccount::UpdateAccount(
					$USER->GetID(),
					$sum,
					'OUT_CHARGE_OFF',
					0,
					'request_id: ' .$request_id
				);
					
				$USER_FIELD_MANAGER->Update("USER", $USER->GetID(), array(
						"UF_REQUEST_ID" => "",
						"UF_SUM" => ""
				));
			}
		}		
		
		LocalRedirect("/youdo/personal/?ITEM=account");
	}
}
?>