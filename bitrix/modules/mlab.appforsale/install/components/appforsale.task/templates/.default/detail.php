<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<div>
	<div class="col-md-8">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.task.detail',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'FIELD_CODE' => $arParams['DETAIL_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
				'MESS_BTN_ADD' => $arParams['OFFER_MESS_BTN_ADD']
					
			),
			$component
		);
		?>
		<div class="row">
		<?
		$bAccess = false;
		if ($USER->IsAuthorized())
		{
			CModule::IncludeModule('iblock');
			$dbElement = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $arParams['OFFER_IBLOCK_ID'],
					'PROPERTY_TASK_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
					'CREATED_BY' => $USER->GetID()
				)
			);
			if ($arElement = $dbElement->GetNext())
			{
			?>
				<div class="col-md-12">
					<? 
					if (!is_array($arParams['OFFER_FORM_FIELD_CODE']))
						$arParams['OFFER_FORM_FIELD_CODE'] = array();
					foreach ($arParams['OFFER_FORM_FIELD_CODE'] as $key => $val)
						if (!$val)
							unset($arParams['OFFER_FORM_FIELD_CODE'][$key]);
					
						if (!is_array($arParams['OFFER_FORM_PROPERTY_CODE']))
							$arParams['OFFER_FORM_PROPERTY_CODE'] = array();
						foreach ($arParams['OFFER_FORM_PROPERTY_CODE'] as $key => $val)
							if ($val === '')
								unset($arParams['OFFER_FORM_PROPERTY_CODE'][$key]);
					
					
					$text = CIBlockProperty::GetPropertyArray('TEXT', $arParams['OFFER_IBLOCK_ID']);
					$APPLICATION->IncludeComponent(
						'mlab:appforsale.offer',
						'',
						array(
							'IBLOCK_TYPE' => $arParams['OFFER_IBLOCK_TYPE'],
							'IBLOCK_ID' => $arParams['OFFER_IBLOCK_ID'],
							'TASK_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
							'FIELD_CODE' => $arParams['OFFER_FORM_FIELD_CODE'],
							'PROPERTY_CODE' => $arParams['OFFER_FORM_PROPERTY_CODE'] ?: array($text['ORIG_ID']),
							'CACHE_TYPE' => $arParams['CACHE_TYPE'],
							'CACHE_TIME' => $arParams['CACHE_TIME'],
							'PROFILE_ID' => $USER->GetID(),
							'MESS_OFFER' => $arParams['DETAIL_MESS_YOUR_OFFER']
						),
						$component
					);
					?>
				</div>
				<?
			}
			else
			{
				$dbProps = CIBlockElement::GetProperty(
					$arParams['IBLOCK_ID'],
					$arResult['VARIABLES']['ELEMENT_ID'],
					array(),
					array(
						'CODE' => 'STATUS_ID'
					)
				);
				if ($arProps = $dbProps->Fetch())
				{
					if ($arProps['VALUE'] == 'N' || $arProps['VALUE'] == '')
					{
						$section_id = CIBlockFindTools::GetSectionID(
							$arResult['VARIABLES']['SECTION_ID'],
							$arResult['VARIABLES']['SECTION_CODE'],
							array(
								'GLOBAL_ACTIVE' => 'Y',
								'IBLOCK_ID' => $arParams['IBLOCK_ID']
							)
						);
						
						$dbNav = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $section_id, array('ID', 'CODE'));
						$path = '';
						$arNavChain = array();
						while($arNav = $dbNav->GetNext())
						{
							$arNavChain[] = $arNav;
							$path .= $arNav['CODE'].'/';
						}

						$arNavChain = array_reverse($arNavChain);
						
						$comission = 0;
						foreach ($arNavChain as $arNav)
						{
							$euf = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION', $arNav['ID']);
							if ($comission == 0 && $euf['UF_COMISSION']['VALUE'] > 0)
								$comission = $euf['UF_COMISSION']['VALUE'];
						}
						
						if ($comission > 0)
						{
							$dbProps = CIBlockElement::GetProperty(
									$arParams['IBLOCK_ID'],
									$arResult['VARIABLES']['ELEMENT_ID'],
									array(),
									array(
										'CODE' => 'PRICE'
									)
							);
							if ($arProps = $dbProps->Fetch())
							{
								if ($arProps['VALUE'] > 0)
								{
									$comissionText = '<br />'.AppforsaleFormatCurrency($arProps['VALUE'] / 100 * $comission).' ('.$comission.'% '.GetMessage('END').')';
								}
							}
						}
						
						$uf = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION', $section_id);
						$uf_profile = $uf['UF_PROFILE'];
						$bAccess = true;
						$profiles = array();
						foreach ($uf_profile['VALUE'] as $sid)
						{
							$dbNav = CIBlockSection::GetNavChain($uf_profile['SETTINGS']['IBLOCK_ID'], $sid, array('ID'));
							while($arNav = $dbNav->GetNext())
							{
								$profiles[] = $arNav['ID'];
								$price = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$uf_profile['SETTINGS']['IBLOCK_ID'].'_SECTION', $arNav['ID']);
								if (intval($price['UF_RECUR_PRICE']['VALUE']) > 0 || intval($price['UF_RECUR_PRICE_TEST']['VALUE']) > 0)
									$bAccess = false;
							}
						}

						if (!$bAccess)
						{
							$arProfile = CIBlockElement::GetList(
								array(),
								array(
									'IBLOCK_ID' => $uf_profile['SETTINGS']['IBLOCK_ID'],
									'ACTIVE' => 'Y',
									'IBLOCK_SECTION_ID' => $profiles,
									'CREATED_BY' => $USER->GetID()
								),
								array()
							);
							if ($arProfile['CNT'] > 0)
							{
								$arProfile = CIBlockElement::GetList(
										array(),
										array(
												'IBLOCK_ID' => $uf_profile['SETTINGS']['IBLOCK_ID'],
												'ACTIVE' => 'Y',
												'ACTIVE_DATE' => 'Y',
												'IBLOCK_SECTION_ID' => $profiles,
												'CREATED_BY' => $USER->GetID()
										),
										array()
								);
								if ($arProfile['CNT'] > 0)
								{
									$bAccess = true;
								}	
								else
								{
									$eMessage = GetMessage('WARNING_SUBSCRIPTION');
								}							
							}
							else
							{
								$eMessage = GetMessage('WARNING_PROFILE', array('#PATH#' => $path));
							}
						}
						
						$uf = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION', $section_id);
						$uf_offer_price = $uf['UF_OFFER_PRICE']['VALUE'];
					}
				}
			}
		}
		else
		{
			$eMessage = GetMessage('WARNING_LOGIN');
		}
		?>
		<div class="col-md-<?=$bAccess ? '6' : '12'?>" style="padding-top: 4px; padding-bottom: 4px">
		
		<?if ($bAccess):?>
			<a class="btn btn-primary btn-block" href="offer/" title="<?=$arParams['OFFER_MESS_BTN_ADD']?>"><?=$arParams['OFFER_MESS_BTN_ADD']?><?=(intval($uf_offer_price) > 0 ? ' ('.AppforsaleFormatCurrency($uf_offer_price).')' : '').$comissionText?></a>
		<?endif;?>
	
		<?if (!empty($eMessage)):?>
			<div class="bg-warning" style="padding: 15px;"><?=$eMessage?></div>
		<?endif;?>
		
		</div>

	<!-- 	<div class="col-md-6" style="padding-top: 13px; padding-bottom: 13px"> -->
<!-- 			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script> -->
<!-- 			<script src="//yastatic.net/share2/share.js"></script> -->
<!-- 			<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div> -->
<!-- 		</div> -->
		
		
		<div class="col-md-<?=$bAccess ? '6' : '12'?>" style="padding-top: 4px; padding-bottom: 4px"><span class="btn btn-link btn-block" id="claim" data-iblock_id="<?=$arParams['IBLOCK_ID']?>" data-id="<?=$arResult['VARIABLES']['ELEMENT_ID']?>"><?=$arParams['DETAIL_MESS_CLAIM']?></span></div>
		</div>
		<script type="text/javascript">BX.message({'CLAIM_SUCCESS': '<?=GetMessage('CLAIM_SUCCESS')?>'});</script>
	</div>
	<div class="col-md-4">
		<h5><?=$arParams['DETAIL_MESS_CUSTOMER']?></h5>
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.user.card',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME']
			),
			$component
		);
		?>
	</div>
</div>