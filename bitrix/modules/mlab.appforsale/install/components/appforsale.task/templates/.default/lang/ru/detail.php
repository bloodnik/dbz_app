<?
$MESS['CLAIM_SUCCESS'] = 'Жалоба отправлена!';
$MESS['WARNING_LOGIN'] = 'Для отправки предложения необходимо авторизоваться<br /><br /><a class="btn btn-warning" href="/youdo/login/">Войти</a>';
$MESS['WARNING_PROFILE'] = 'Для отправки предложения необходимо создать профиль исполнителя<br /><br /><a class="btn btn-warning" href="/youdo/personal/profile/#PATH#form/">Создать</a>';
$MESS['WARNING_SUBSCRIPTION'] = 'Для отправки предложения необходимо продлить профиль исполнителя<br /><br /><a class="btn btn-warning" href="/youdo/subscription/">Перейти в раздел подписок</a>';
$MESS['END'] = 'от цены задания';
?>