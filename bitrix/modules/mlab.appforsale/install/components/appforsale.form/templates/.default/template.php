<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId('');
$arItemIDs = array(
	'ID' => $strMainID,
	'DELETE' => $strMainID.'_delete'
);
?>
<form class="afs-form form-horizontal" id="form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" onsubmit="
	this.submit.setAttribute('disabled', 'disabled');
	BX.ajax({
		url: form.action,
		data: BX.ajax.prepareForm(this).data,
		method: 'POST',
		dataType: 'json',
		onsuccess: BX.delegate(function(data) {
    	if (data.response)
    	{
        	<?if (empty($arParams['REDIRECT_URL'])):?>
				app.onCustomEvent('OnIblock');
				app.closeController();
			<?else:?>
				app.loadPageBlank({url: '<?=$arParams['REDIRECT_URL']?>', title: ''});
			<?endif;?>
    	}
    	else if (data.error)
    	{
			this.submit.removeAttribute('disabled');
    		alert(data.error.error_msg);
    	}
    }, this)
});
return false;
">
<?=bitrix_sessid_post()?>
	
<?if (is_array($arResult['PROPERTY_LIST']) && !empty($arResult['PROPERTY_LIST'])):?>
		<?foreach ($arResult['PROPERTY_LIST'] as $propertyID):?>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['NAME']?><?if(in_array($propertyID, $arResult['PROPERTY_REQUIRED'])):?> <span class="afs-required">*</span><?endif?></label>
				<div class="col-sm-8">
					<? 
					if (intval($propertyID) > 0)
					{
						if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
						)
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
						elseif (
								(
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
										||
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
						)
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
					}
					
					if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
					{
						$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
						$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"] > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"] : 1;
					}
					else
					{
						$inputNum = 1;
					}
					
					
					if ($arResult['PROPERTY_LIST_FULL'][$propertyID]['GetPublicEditHTML'])
						$INPUT_TYPE = 'USER_TYPE';
					else
						$INPUT_TYPE = $arResult['PROPERTY_LIST_FULL'][$propertyID]['PROPERTY_TYPE'];
					
		
					
					
					switch ($INPUT_TYPE):
						case 'USER_TYPE':
						for ($i = 0; $i<$inputNum; $i++)
						{
						if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
						{
								$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
								$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
						}
						elseif ($i == 0)
						{
						$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
						$description = "";
						}
						else
							{
												$value = "";
																$description = "";
						}
						echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
						array(
						$arResult["PROPERTY_LIST_FULL"][$propertyID],
						array(
								"VALUE" => $value,
								"DESCRIPTION" => $description,
						),
								array(
										"VALUE" => $arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']."[".$propertyID."][".$i."][VALUE]",
										"DESCRIPTION" => $arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']."[".$propertyID."][".$i."][DESCRIPTION]",
										"FORM_NAME"=>"iblock_add",
								),
						));
						?><?
														}
							break;
							case "TAGS":
								break;
								case "HTML":
									break;
						case 'E':
						case 'G':
							$bDisable = false;
							if ($arParams["ID"] > 0 && $arResult['PROPERTY_LIST_FULL'][$propertyID]['CODE'] == 'CITY_ID')
							{
								foreach ($arResult['PROPERTY_LIST_FULL'][$propertyID]['LIST'] as $key => $arEnum)
								{
									if (intval($arEnum['PROPERTY_RATIO_VALUE']) > 0)
									{
										$bDisable = true;
										break;
									}
								}
							}
							?>

					<select class="form-control" name="<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']?>[<?=$propertyID?>]<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['MULTIPLE'] == 'Y' ? '[]" size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"].'" multiple="multiple' : '' ?>"<?=($bDisable ? ' disabled="disabled"' : '')?>>
								<?if($arResult['PROPERTY_LIST_FULL'][$propertyID]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
									<optgroup disabled hidden></optgroup>';
								<?endif;?>	
								<option value=""><?=$arParams['MESS_PROPERTY_VALUE_NA']?></option>
								<? 
								foreach ($arResult['PROPERTY_LIST_FULL'][$propertyID]['LIST'] as $key => $arEnum)
								{
									$checked = false;
									if ($arParams['ID'] > 0)
									{
										if (is_array($arResult['ELEMENT_PROPERTIES'][$propertyID]))
										{
											foreach ($arResult['ELEMENT_PROPERTIES'][$propertyID] as $arElEnum)
											{
												if ($arElEnum['VALUE'] == $key)
												{
													$checked = true;
													break;
												}
											}
										}
									}
									?>
									<option value="<?=$key?>" <?=$checked ? ' selected="selected"' : ''?>><?=$arEnum['NAME']?></option>
									<?
								}
								?>		
							</select>	
							<?
							break;
							case "T":
								for ($i = 0; $i<$inputNum; $i++)
								{
							
								if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
								{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
								}
								elseif ($i == 0)
								{
									$value = intval($propertyID) > 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"] : "";
								}
								else
									{
										$value = "";
									}
									?>
													<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
															<?
															}
														break;
														
						case 'S':
						case 'N':
							for ($i = 0; $i<$inputNum; $i++)
							{
								if ($arParams['ID'] > 0)
								{
									$value = intval($propertyID) > 0 ? $arResult['ELEMENT_PROPERTIES'][$propertyID][$i]['VALUE'] : $arResult['ELEMENT'][$propertyID];
								}
								elseif ($i == 0)
								{
									$value = intval($propertyID) <= 0 ? '' : $arResult['PROPERTY_LIST_FULL'][$propertyID]['DEFAULT_VALUE'];
								}
								else
								{
									$value = "";
								}
								?>
								<input<?=($arParams['ID'] > 0 && $arResult['PROPERTY_LIST_FULL'][$propertyID]['CODE'] == 'PRICE' ? ' readonly="readonly"' : '')?> class="form-control" <?=($INPUT_TYPE == 'N' ? 'type="number" pattern="[0-9]*"' : 'type="text"') ?> name="<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']?>[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" /><?
							}
							break;
						case 'L':
							if ($arResult['PROPERTY_LIST_FULL'][$propertyID]['LIST_TYPE'] == 'C')
								$type = $arResult['PROPERTY_LIST_FULL'][$propertyID]['MULTIPLE'] == 'Y' ? 'checkbox' : 'radio';
							else
								$type = $arResult['PROPERTY_LIST_FULL'][$propertyID]['MULTIPLE'] == 'Y' ? 'multiselect' : 'dropdown';
								
							switch ($type):
								case 'checkbox':
								case 'radio':
									if (count($arResult['PROPERTY_LIST_FULL'][$propertyID]['ENUM']) <= 1)
										$type = 'checkbox';
									
									foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
									{
										$checked = false;
										if ($arParams['ID'] > 0)
										{
											if (is_array($arResult['ELEMENT_PROPERTIES'][$propertyID]))
											{
												foreach ($arResult['ELEMENT_PROPERTIES'][$propertyID] as $arElEnum)
												{
													if ($arElEnum['VALUE'] == $key)
													{
														$checked = true;
														break;
													}
												}
											}
										}
										else
										{
											if ($arEnum['DEF'] == 'Y') $checked = true;
										}
										?>
										<div class="<?=$type?>"><label for="property_<?=$key?>"><input type="<?=$type?>" name="<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><?=$arEnum["VALUE"]?></label></div>
										<?
									}
				
									break;
							
								case "dropdown":
								case "multiselect":
									?>
									<select class="form-control" name="<?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['strHTMLControlName']?>[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
										<?if($arResult['PROPERTY_LIST_FULL'][$propertyID]['MULTIPLE'] == 'Y' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
											<optgroup disabled hidden></optgroup>';
										<?endif;?>
										<option value=""><?=$arParams['MESS_PROPERTY_VALUE_NA']?></option>
										<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";
									
										foreach ($arResult['PROPERTY_LIST_FULL'][$propertyID]['ENUM'] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams['ID'] > 0)
											{
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"])
													{
														$checked = true;
														break;
													}
												}
											}
											else
											{
												if ($arEnum['DEF'] == 'Y')
												{
													$checked = true;
												}
											}
											?>
											<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
											<?
											}
											?>
										</select>
										<?
									break;
							endswitch;
							
							break;
					
							case 'F':
								
								$value = array();
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams['ID'] > 0)
									{
										if (!empty($arResult['ELEMENT_PROPERTIES'][$propertyID][$i]['VALUE']))
											$value[] = $arResult['ELEMENT_PROPERTIES'][$propertyID][$i]['VALUE'];
									}
									
									
									
								}
								$APPLICATION->IncludeComponent(
									'mlab:appforsale.interface.file',
									'',
									array_merge($arResult['PROPERTY_LIST_FULL'][$propertyID], array('VALUE' => $value)),
									false
								);
								break;
					endswitch;
					
					if (!empty($arResult['PROPERTY_LIST_FULL'][$propertyID]['HINT'])):
					?>
						<p class="help-block"><?=$arResult['PROPERTY_LIST_FULL'][$propertyID]['HINT']?></p>
					<?endif;?>
			</div>		</div>		
		<?endforeach;?>		
<?endif;?>
	<input class="btn btn-primary" type="submit" name="submit" value="<?=$arParams['MESS_BTN_ADD']?>" /> <?if ($arParams['ID'] > 0):?><span class="btn btn-danger" id="<?=$arItemIDs['DELETE']?>"><?=$arParams['MESS_BTN_DELETE']?></span>
<?
$arJSParams = array(
	'ID' => $arParams['ID'],
	'ajaxUrl' => CUtil::JSEscape($component->getPath().'/delete.php'),
	'VISUAL' => array(
		'ID' => $arItemIDs['ID'],
		'DELETE' => $arItemIDs['DELETE']
	)
);
echo '<script type="text/javascript">new JCForm('.CUtil::PhpToJSObject($arJSParams, false, true).');</script>';
?>
<?endif;?>
	</form>