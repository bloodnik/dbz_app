<?
use Bitrix\Main;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
	<h5><?=$arParams['MESS_OFFER']?></h5>
	<div class="afs-offer-rows">
		<?foreach ($arResult['ITEMS'] as $arItem):?>
			<?
			$areaId = $this->GetEditAreaId($arItem['ID']);
			$itemIds = array(
				'ID' => $areaId,
				'WRAP' => $areaId.'_wrap',
				'CANCEL' => $areaId.'_cancel',
				'DONE' => $areaId.'_done',
				'FINISH' => $areaId.'_finish',
				'REJECT' => $areaId.'_reject',
				'CONFIRM' => $areaId.'_confirm',
				'COMMENT' => $areaId.'_comment'
			);
			$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
			?>
			<div class="row afs-offer-wrap" id="<?=$itemIds['WRAP']?>">
				<div class="afs-offer-row col-xs-12" id="<?=$areaId?>">
					<?if ($arItem['CREATED_BY'] != $USER->GetID()):?>
						<div>
							<?
							$APPLICATION->IncludeComponent(
								'mlab:appforsale.user.item',
								'',
								array(
									'RESULT' => array(
										'ITEM' => $arItem['USER'],
										'AREA_ID' => $this->GetEditAreaId($arItem['ID']),
									)
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);
							?>
						</div>
					<?endif;?>
										<div class="afs-offer-name">
					<?foreach ($arItem['DISPLAY_PROPERTIES'] as $property):?>
						<?=(
							is_array($property['DISPLAY_VALUE'])
							? implode(' / ', $property['DISPLAY_VALUE'])
							: $property['DISPLAY_VALUE']
						)?>
					<br />
					<?endforeach;?>
					
					</div>	
	
						
					<?if ($arItem['CREATED_BY'] == $USER->GetID()):?>
						<?if ($arParams['STATUS_ID'] == 'P'):?>
							<div class="text-right"><button type="button" class="btn btn-success btn-sm" id="<?=$itemIds['DONE']?>"><?=$arParams['MESS_BTN_DONE']?></button></div>
						<?elseif ($arParams['STATUS_ID'] == 'F'):?>
							<?if ($arParams['COMMENT_LEFT'] == 'N'):?>
								<div class="text-right"><button type="button" class="btn btn-default btn-sm" id="<?=$itemIds['COMMENT']?>"><?=$arParams['MESS_BTN_COMMENT']?></button></div>
							<?endif;?>
						<?endif;?>
					<?else:?>		
						<?if ($arParams['PROFILE_ID'] == 0):?>
							<div class="text-right"><button type="button" class="btn btn-danger btn-sm" id="<?=$itemIds['REJECT']?>"><?=$arParams['MESS_BTN_REJECT']?></button> <button type="button" class="btn btn-success btn-sm" id="<?=$itemIds['CONFIRM']?>"><?=$arParams['MESS_BTN_CONFIRM']?></button></div>
						<?else:?>
							<?if ($arParams['STATUS_ID'] == 'F'):?>
								<?if ($arParams['COMMENT_LEFT'] == 'N'):?>
									<div class="text-right"><button type="button" class="btn btn-default btn-sm" id="<?=$itemIds['COMMENT']?>"><?=$arParams['MESS_BTN_COMMENT']?></button></div>
								<?endif;?>
							<?else:?>
								<div class="text-right"><button type="button" class="btn btn-danger btn-sm" id="<?=$itemIds['CANCEL']?>"><?=$arParams['MESS_BTN_CANCEL']?></button> <button type="button" class="btn btn-success btn-sm" id="<?=$itemIds['FINISH']?>"><?=$arParams['MESS_BTN_DONE']?></button></div>
							<?endif;?>
						<?endif;?>
					<?endif;?>
					
					
					<?
					$signer = new Main\Security\Sign\Signer;
					$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'appforsale.offer');
					
					$arJSParams = array(
						'ID' => $arItem['ID'],
						'signedParamsString' => CUtil::JSEscape($signedParams),
						'ajaxUrl' => CUtil::JSEscape($component->getPath().'/ajax.php'),
						'commentUrl' => CUtil::JSEscape($arParams['COMMENT_URL']),
						'VISUAL' => $itemIds
					);
					echo '<script type="text/javascript">'.$obName.' = new JCOffer('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
					?>
				</div>
			</div>
		<?endforeach;?>
		<?=$arResult['NAV_STRING']?>
	</div>
<?endif;?>