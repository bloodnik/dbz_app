<?
use Bitrix\Main\Loader;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
	$params = $signer->unsign($request->get('signedParamsString'), 'appforsale.offer');
	$params = unserialize(base64_decode($params));
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
	die();
}

$action = $request->get('action');
if (empty($action))
	return;

if (!Loader::includeModule('iblock'))
	return;

$dbElement = CIBlockElement::GetList(
	array(),
	array(
		'ID' => $request->get('id'),
		'ACTIVE' => 'Y'
	),
	false,
	false,
	array(
		'ID',
		'CREATED_BY',
		'PROPERTY_TASK_ID.ID',
		'PROPERTY_TASK_ID.IBLOCK_ID',
		'PROPERTY_TASK_ID.CREATED_BY',
		'PROPERTY_PRICE'
	)
);
if($arElement = $dbElement->GetNext())
{
	if ($arElement['PROPERTY_TASK_ID_CREATED_BY'] == $USER->GetID())
	{
		if ($action == 'reject')
		{
			$obElement = new CIBlockElement;
			$obElement->Update(
				$arElement['ID'],
				array(
					'ACTIVE' => 'N'
				)
			);
		}
		
		if ($action == 'confirm')
		{
			$arFields = array(
					'PROFILE_ID' => $arElement['CREATED_BY'],
					'STATUS_ID' => 'P'
			);
			
			if (intval($arElement['PROPERTY_PRICE_VALUE']) > 0)
			$arFields['PRICE'] = $arElement['PROPERTY_PRICE_VALUE'];
			
			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				$arFields
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
		}
		
		if ($action == 'cancel')
		{
			$obElement = new CIBlockElement;
			$obElement->Update(
				$arElement['ID'],
				array(
					'ACTIVE' => 'N'
				)
			);
			
			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				array(
					'PROFILE_ID' => false,
					'STATUS_ID' => 'N'
				)
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
		}
		
		if ($action == 'finish')
		{	
// 			$obElement = new CIBlockElement;
// 			$obElement->Update(
// 				$arElement['ID'],
// 				array(
// 					'ACTIVE' => 'N'
// 				)
// 			);

			CIBlockElement::SetPropertyValuesEx(
				$arElement['PROPERTY_TASK_ID_ID'],
				$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
				array(
					'STATUS_ID' => 'F'
				)
			);
			CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
		}
		
		// Отправить push-уведомление $arElement['CREATED_BY']
	}
	

	if ($action == 'done')
	{
		CIBlockElement::SetPropertyValuesEx(
		$arElement['PROPERTY_TASK_ID_ID'],
		$arElement['PROPERTY_TASK_ID_IBLOCK_ID'],
			array(
				'STATUS_ID' => 'D'
			)
		);
		CIBlock::clearIblockTagCache($arElement['PROPERTY_TASK_ID_IBLOCK_ID']);
	}
}
?>