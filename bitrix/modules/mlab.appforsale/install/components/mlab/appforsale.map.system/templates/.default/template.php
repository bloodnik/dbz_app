<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<div id="map" style="height: 200px; width: 100%;"></div>
<script>
(function (window) {
	if (!!window.JCMap)
	{
		return;
	}

	window.JCMap = function (arParams)
	{
		this.map = null;
		this.objects = null;
		this.route = false;
		this.bounds = [];

		this.minLat = 0;
		this.maxLat = 0;
		this.minLng = 0;
		this.maxLng = 0;

		if ('object' === typeof arParams)
		{
			this.objects = arParams.OBJECTS;
			this.route = arParams.ROUTE;
		}

		if (this.objects.length > 0)
			BX.ready(BX.delegate(this.Check, this));
		else
			BX.hide(BX('map'));
	}

	window.JCMap.prototype.Check = function()
	{
		if (window.ymaps && window.ymaps.Map)
		{
			ymaps.ready(BX.delegate(this.Init, this));

		}
		else
		{
			setTimeout(BX.delegate(this.Check, this), 100);
		}
	}
	
	window.JCMap.prototype.Init = function()
	{
		this.map = new ymaps.Map('map', {
			center: [55.76, 37.64],
			zoom: 7,
			margin: 20,
			controls: ['zoomControl', 'fullscreenControl', 'geolocationControl']
		}, {suppressMapOpenBlock: true});


		ymaps.geolocation.get().then(BX.delegate(function (res) {
			if (res.geoObjects)
			{
				this.map.geoObjects.add(res.geoObjects);
			}	
		}, this), function (e) {
	
	});
		

		for (var i = 0; i < this.objects.length; i++)
		{
			var object = this.objects[i];
			this.bounds.push(object[0]);

			if (this.minLat == 0 || this.minLat > object[0][0])
				this.minLat = object[0][0];

			if (this.maxLat == 0 || this.maxLat < object[0][0])
				this.maxLat = object[0][0];

			if (this.minLng == 0 || this.minLng > object[0][1])
				this.minLng = object[0][1];

			if (this.maxLng == 0 || this.maxLng < object[0][1])
				this.maxLng = object[0][1];
			
			if (!this.route)
			{
				this.map.geoObjects.add(new ymaps.Placemark(object[0], object[1], {
					balloonPanelMaxMapArea: 0
				}));	
			}
		}

		if (this.route)
		{
			this.map.geoObjects.add(new ymaps.multiRouter.MultiRoute({
				referencePoints: this.bounds,
				params: { results: 2 }
			}, {
				boundsAutoApply: true
			}));
		}
		else
		{
			this.map.setBounds([[this.minLat, this.minLng], [this.maxLat, this.maxLng]], {
				checkZoomRange: true
			});
		}
	}
	
})(window);
</script>
<script type="text/javascript">new JCMap(<?=CUtil::PhpToJSObject(array(
	'OBJECTS' => $arParams['OBJECTS'],
	'ROUTE' => $arParams['ROUTE']
), false, true)?>)</script>