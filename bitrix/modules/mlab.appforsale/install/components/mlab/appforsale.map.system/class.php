<?
use \Bitrix\Main\Context,
	\Bitrix\Main\Page\Asset;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class MapSystem extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['ROUTE'] = $arParams['ROUTE'] == 'Y';
		
		return $arParams;
	}
	
	public function executeComponent()
	{
		if (!defined('AFS_MAP_SCRIPT_LOADED'))
		{
		$request = Context::getCurrent()->getRequest();
		$scheme = ($request->isHttps() ? 'https' : 'http');
        ?>
		<script>
			BX.ready(function() {
				setTimeout(function() {
					BX.loadScript("<?=$scheme?>://api-maps.yandex.ru/2.1/?lang=<?=LANGUAGE_ID?>");
				}, 50);
			});
		</script>
		<? 
	   define('AFS_MAP_SCRIPT_LOADED', 1);
	}
		$this->IncludeComponentTemplate();
	}
}
?>