<? 
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class ExecutorProfileList extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['ID'] = isset($arParams['ID']) ? (int) $arParams['ID'] : 0;
	
		return $arParams;
	}
	
	public function executeComponent()
	{
		Loader::includeModule('iblock');
		
		$dbSection = CIBlockSection::GetList(
				array(
						'left_margin' => 'asc'
				),
				array(
						'IBLOCK_CODE' => 'profile',
						'ACTIVE' => 'Y'
				),
				false,
				array(
						'ID',
						'NAME',
						'DEPTH_LEVEL',
						'IBLOCK_SECTION_ID'
				)
		);
		while ($arSection = $dbSection->GetNext())
		{
			$this->arResult['SECTIONS'][$arSection['ID']] = $arSection;
		}
			
		$dbElement = CIBlockElement::GetList(
				array(),
				array(
						'IBLOCK_CODE' => 'profile',
						'ACTIVE' => 'Y',
						'CREATED_BY' => $this->arParams['ID']
				),
				false,
				false,
				array(
						'ID',
						'IBLOCK_ID',
						'NAME',
						'IBLOCK_SECTION_ID',
						'DETAIL_PAGE_URL'
				)
		);
		$dbElement->SetUrlTemplates($this->arParams['DETAIL_URL']);		
		while ($arElement = $dbElement->GetNext())
		{
		    if ($arElement['NAME'] == "unknown")
		        $arElement['NAME'] = Loc::getMessage("UNKNOWN");
		    
			$this->arResult['ELEMENTS'][$arElement['IBLOCK_SECTION_ID']][] = $arElement;
			$this->active($arElement['IBLOCK_SECTION_ID']);
		}
		
		$this->IncludeComponentTemplate();
	}
	
	private function active($section_id)
	{
		$arSection = &$this->arResult['SECTIONS'][$section_id];
		$arSection['ACTIVE'] = 'Y';
		if (intval($arSection['IBLOCK_SECTION_ID']) > 0)
		{
			$this->active($arSection['IBLOCK_SECTION_ID']);
		}
	}
	
}
?>