<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class PersonalLine extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		if ($_GET['exit'] == 'Y')
			$USER->Logout();
		
		if ($USER->IsAuthorized())
		{
			$dbUser = CUser::GetByID($USER->GetID());
			if ($arUser = $dbUser->Fetch())
			{
				$arUser['FORMAT_NAME'] = CUser::FormatName('#NAME_SHORT# #LAST_NAME#', $arUser);
				$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
				$this->arResult['USER'] = $arUser;
			}
		}
		
		
		
		
		$this->includeComponentTemplate();
	}
}
?>