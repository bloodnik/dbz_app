/* mlab:appforsale.interface.file */
(function (window) {
	
	if (!!window.JCFile)
	{
		return;
	}

	window.JCFile = function (arParams)
	{
		this.i = 0;
		this.id = null;
		this.multiple = false;
		this.visual = {
			ID: '',
			BLOCK: '',
			CONTAINER: '',
			ADD: '',
			INPUT: ''
		};
		
		this.obBlock = null;
		this.obContainer = null;
		this.obAdd = null;
		this.obInput = null;
		
		this.templateFolder = null;

		if ('object' === typeof arParams)
		{
			this.id = arParams.ID;
			this.templateFolder = arParams.templateFolder;
			
			if (arParams.MULTIPLE == 'Y')
				this.multiple = true;
			
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCFile.prototype.Init = function()
	{
		this.obBlock = BX(this.visual.BLOCK);
		this.obContainer = BX(this.visual.CONTAINER);
		this.obAdd = BX(this.visual.ADD);
		this.obInput = BX(this.visual.INPUT);
		if (!!this.obInput)
		{
			BX.bind(this.obInput, 'change', BX.delegate(this.uploadfile, this));
		}
		
		if (!!this.obAdd)
		{
			BX.bind(this.obAdd, 'click', BX.delegate(this.showFileChooser, this));
		}
	}
	
	window.JCFile.prototype.showFileChooser = function()
	{
		app.showFileChooser(BX.proxy_context, BX.delegate(this.uploadfile, this));
	}

	window.JCFile.prototype.uploadfile = function(base64)
	{
		//this.obBlock.innerHTML = '<div id="' + this.visual.ADD + '" class="bx_file_add"><img src="' + this.templateFolder + '/images/add.png" class="bx_file_add_img" /></div><input id="'  + this.visual.INPUT +  '" class="bx_file_add_input" type="file" />';

		var target = BX.proxy_context;
	
		if (!this.multiple)
		{
			BX.adjust(this.obAdd, {style: {display: "none"}});
		}
			
		var canvas = BX.create(
			'canvas'
		);
		
	
		
		var progress = BX.create(
			'img',
			{
				props: {
					className: "bx_file_progress",
					src: this.templateFolder + "/images/progress.gif"
				}
			}
		);
			
		var file = BX.create(
			'div',
			{
				props: {
					className: "bx_file"
				},
				children: [
					canvas,
					progress
				]
			}
		);
		BX.append(file, this.obContainer);

		
		if (base64 instanceof Event)
		{
			
				
			
			var files = target.files;
			var reader = new FileReader();		
			reader.onload = BX.delegate(function (oFREvent) {
				var ctx = canvas.getContext('2d'); 
				var orientation = parse_exif(oFREvent.target.result, true);
				var pic = new Image();
				pic.onload = BX.delegate(function() { 
					
					if (pic.width > pic.height)
					{						
						w = Math.min(1024, pic.width);
						k = pic.width / w;
						h = pic.height / k;
						
						
						if (k == 1)
							k = pic.width / pic.height;

						canvas.style.width = 60 + "px";
						canvas.style.height = (60 / k) + "px";
						canvas.style.marginTop = (60 - (60 / k)) / 2 + "px";
					}
					else
					{
						h = Math.min(1024, pic.height);
						k = pic.height / h;
						w = pic.width / k;
						
						if (k == 1)
							k = pic.height / pic.width;
						
						canvas.style.height = 60 + "px";
						canvas.style.width = (60 / k) + "px";
						canvas.style.marginLeft = (60 - (60 / k)) / 2 + "px";
					}
					
					canvas.width = w;
					canvas.height = h;

					switch(orientation)	{
						case 8:
							ctx.translate(0,canvas.height);
							ctx.rotate(-90*Math.PI/180);
							break;
						case 3:
							ctx.translate(canvas.width,canvas.height);
							ctx.rotate(180*Math.PI/180);
							break;
						case 6:
							ctx.translate(canvas.width,0);
							ctx.rotate(90*Math.PI/180);
							break;
					} 
					if ((pic.width/(pic.height/canvas.height))>=canvas.width) ctx.drawImage(pic, (pic.width/(pic.height/canvas.height)-canvas.width)/2, 0, canvas.width*(pic.height/canvas.height), pic.height, 0, 0, canvas.width, canvas.height);
					else ctx.drawImage(pic, 0, (pic.height/(pic.width/canvas.width)-canvas.height)/2, pic.width, canvas.height*(pic.width/canvas.width), 0, 0, canvas.width, canvas.height);
				
					BX.ajax({
						url: "/bitrix/components/mlab/appforsale.interface.file/templates/.default/upload.php",
						data: {
							file: canvas.toDataURL('image/jpeg')
						},
				        method: 'POST',
				        dataType: 'json',
						onsuccess: BX.delegate(function(data) {
							BX.adjust(progress, {style: {display: "none"}});
							
							BX.append(
								BX.create(
									'input',
									{
										props: {
											type: "hidden",
											name: 'PROPERTY['+this.id+']['+this.i+']',
											value: data.response
										}
									}
								),
								file
							);
							this.i++;
							
							var del = BX.create(
									'img',
									{
										props: {
											className: "bx_file_delete_img",
											src: this.templateFolder + "/images/delete.png"
										}
									}
								);
							BX.append(del, file);
							
						
							
							BX.bind(del, 'click', BX.delegate(function() {
								this.remove(file);
							}, this));
							
						}, this),
						onfailure: function() {
							alert("Error");
						}
					});
					
					
					
					
					
					
					
		
				}, this);
				pic.src = oFREvent.target.result;
			}, this);
			reader.readAsDataURL(files[0]);	
		
		}
		else
		{
				var ctx = canvas.getContext('2d'); 
				var orientation = parse_exif(base64, true);
				var pic = new Image();
				pic.onload = BX.delegate(function() { 
					w = h = Math.min(1024, pic.height, pic.width)
					canvas.width = w;
					canvas.height = h;
					switch(orientation)	{
						case 8:
							ctx.translate(0,canvas.height);
							ctx.rotate(-90*Math.PI/180);
							break;
						case 3:
							ctx.translate(canvas.width,canvas.height);
							ctx.rotate(180*Math.PI/180);
							break;
						case 6:
							ctx.translate(canvas.width,0);
							ctx.rotate(90*Math.PI/180);
							break;
					} 
					if ((pic.width/(pic.height/canvas.height))>=canvas.width) ctx.drawImage(pic, (pic.width/(pic.height/canvas.height)-canvas.width)/2, 0, canvas.width*(pic.height/canvas.height), pic.height, 0, 0, canvas.width, canvas.height);
					else ctx.drawImage(pic, 0, (pic.height/(pic.width/canvas.width)-canvas.height)/2, pic.width, canvas.height*(pic.width/canvas.width), 0, 0, canvas.width, canvas.height);
				
					BX.ajax({
						url: "/bitrix/components/mlab/appforsale.interface.file/templates/.default/upload.php",
						data: {
							file: canvas.toDataURL('image/jpeg')
						},
				        method: 'POST',
				        dataType: 'json',
						onsuccess: BX.delegate(function(data) {
							BX.adjust(progress, {style: {display: "none"}});
							
							BX.append(
								BX.create(
									'input',
									{
										props: {
											type: "hidden",
											name: 'PROPERTY['+this.id+']['+this.i+']',
											value: data.response
										}
									}
								),
								file
							);
							this.i++;
							
							var del = BX.create(
									'img',
									{
										props: {
											className: "bx_file_delete_img",
											src: this.templateFolder + "/images/delete.png"
										}
									}
								);
							BX.append(del, file);
							
						
							
							BX.bind(del, 'click', BX.delegate(function() {
								this.remove(file);
							}, this));
							
						}, this),
						onfailure: function() {
							alert("Error");
						}
					});
					
					
					
					
					
					
					
		
				}, this);
				pic.src = base64;

		}
		
		
		
	//	this.obInput.value = null;
	}
	
	window.JCFile.prototype.createFile = function(rsFile)
	{
		BX.ready(BX.delegate(function() {
			
			if (!this.multiple)
			{
				BX.adjust(this.obAdd, {style: {display: "none"}});
			}
			
			
			
			for (var index in rsFile)
			{
				var arfile = rsFile[index];
				if (arfile.ID)
				{
					this.i++;
					var canvas = BX.create(
						'img',
						{
							props: {
								className: "canvas",
								src: arfile.SRC
							}
						}
					);
					var file = BX.create(
						'div',
						{
							props: {
								className: "bx_file"
							},
							children: [
								canvas
							]
						}
					);
					BX.append(file, this.obContainer);
					
					BX.append(
						BX.create(
							'input',
							{
								props: {
									type: "hidden",
									name: 'PROPERTY['+this.id+']['+index+']',
									value: arfile.ID
								}
							}
						),
						file
					);
						
					var del = BX.create(
						'img',
						{
							props: {
								className: "bx_file_delete_img",
								src: this.templateFolder + "/images/delete.png"
							}
						}
					);
					BX.append(del, file);
						
					BX.bind(del, 'click', BX.delegate(function() {
						var target = BX.proxy_context;
						this.remove(target.parentNode);
					}, this));
				}
			}
		}, this));
	}
	
	window.JCFile.prototype.remove = function(file)
	{
		BX.remove(file);
		if (!this.multiple)
		{
			BX.adjust(this.obAdd, {style: {display: "block"}});
		}
	}

})(window);

function parse_exif(data,decode) {
 var image_data=decode?base64decode(data.substr(data.indexOf(',')+1)):data;
 var section_id, section_length, exif;
 var offset_tags, num_of_tags, tag_id, tag_len, tag_value;

 if (image_data.substr(0,2)=='\xFF\xD8') {
     var idx=2;
     while (true) {
         if (idx>=image_data.length) { break; }

         chr1=image_data.charCodeAt(idx++);
         chr2=image_data.charCodeAt(idx++);
         section_id=(chr1<<8)+chr2;

         chr1=image_data.charCodeAt(idx++);
         chr2=image_data.charCodeAt(idx++);
         section_length=(chr1<<8)+chr2;

         if (section_id==0xFFD8 || section_id==0xFFDB ||
             section_id==0xFFC4 || section_id==0xFFDD ||
             section_id==0xFFC0 || section_id==0xFFDA ||
             section_id==0xFFD9) {
             break;
         }

         if (section_id==0xFFE1) {
             exif=image_data.substr(idx,(section_length-2));
             if (exif.substr(0,4)=='Exif') {
                  if (exif.substr(6,2)=='II') {
                     this.mask4=function(str) {
                         var chr1, chr2, chr3, chr4;
                         chr1=str.charCodeAt(0);
                         chr2=str.charCodeAt(1);
                         chr3=str.charCodeAt(2);
                         chr4=str.charCodeAt(3);
                         return (chr4<<24)+(chr3<<16)+(chr2<<8)+chr1;
                     }
                     this.mask2=function(str) {
                         var chr1, chr2;
                         chr1=str.charCodeAt(0);
                         chr2=str.charCodeAt(1);
                         return (chr2<<8)+chr1;
                     }
                 }
                 else if (exif.substr(6,2)=='MM') {
                     this.mask4=function(str) {
                         var chr1, chr2, chr3, chr4;
                         chr1=str.charCodeAt(3);
                         chr2=str.charCodeAt(2);
                         chr3=str.charCodeAt(1);
                         chr4=str.charCodeAt(0);
                         return (chr4<<24)+(chr3<<16)+(chr2<<8)+chr1;
                     }
                     this.mask2=function(str) {
                         var chr1, chr2;
                         chr1=str.charCodeAt(1);
                         chr2=str.charCodeAt(0);
                         return (chr2<<8)+chr1;
                     }
                 }

                 else {
                     return false;
                 }

                 offset_tags=this.mask4(exif.substr(10,4));
                 num_of_tags=this.mask2(exif.substr(14,2));

                 if (num_of_tags>0) {
                     offset=offset_tags+8;

                     for(var i=0; i<num_of_tags; i++) {

                         tag_id=this.mask2(exif.substr(offset,2));
                         tag_len=this.mask4(exif.substr(offset+4,4));
                         tag_value=this.mask4(exif.substr(offset+8,4));

                         // Make
                         if (tag_id==0x010f && tag_len>0 && tag_value>0) {
                            // write_log('Make',exif.substr(tag_value+6,tag_len));
                         }
                         // Model
                         else if (tag_id==0x0110 && tag_len>0 && tag_value>0) {
                            // write_log('Model',exif.substr(tag_value+6,tag_len));
                         }
                         // ModifyDate
                         else if (tag_id==0x0132 && tag_len>0 && tag_value>0) {
                            // write_log('ModifyDate',exif.substr(tag_value+6,tag_len));
                         }
                         // Software
                         else if (tag_id==0x0131 && tag_len>0 && tag_value>0) {
                            // write_log('Software',exif.substr(tag_value+6,tag_len));
                         }
                         // ImageDescription
                         else if (tag_id==0x010e && tag_len>0 && tag_value>0) {
                            // write_log('ImageDescription',exif.substr(tag_value+6,tag_len));
                         }
                         // Artist
                         else if (tag_id==0x013b && tag_len>0 && tag_value>0) {
                            // write_log('Artist',exif.substr(tag_value+6,tag_len));
                         }
                         // Orientation
                         else if (tag_id==0x0112 && tag_len>0) {
                        	 return this.mask2(exif.substr(offset+8,2));
                            // write_log('Orientation',this.mask2(exif.substr(offset+8,2)));
                         }
                         // Copyright
                         else if (tag_id==0x8298 && tag_len>0 && tag_value>0) {
                            // write_log('Copyright',exif.substr(tag_value+6,tag_len));
                         }
                         // GPSInfo
                         else if (tag_id==0x8825 && tag_len>0) {
                             // РђРґСЂРµСЃ GPSInfo РІ СЃРµРєС†РёРё
                             var gps_offset=tag_value+6;
                             // РљРѕР»РёС‡РµСЃС‚РІРѕ GPS-С‚РµРіРѕРІ
                             var num_of_gps_tags=tag_id=this.mask2(exif.substr(gps_offset,2));

                             if (num_of_gps_tags>0) {
                                 gps_offset+=2;

                                 // РћР±СЂР°Р±РѕС‚РєР° GPS-С‚РµРіРѕРІ
                                 for (j=0; j<num_of_gps_tags; j++) {
                                     tag_id=this.mask2(exif.substr(gps_offset,2));
                                     tag_value=this.mask4(exif.substr(gps_offset+8,4));

                                     // GPSLatitudeRef РёР»Рё GPSLongitudeRef
                                     if (tag_id==0x0001 || tag_id==0x0003) {
                                         if (tag_value!=0) {
                                             if (tag_id==0x0001) {
                                              //   write_log('GPSLatitudeRef',exif.substr(gps_offset+8,1));
                                             }
                                             else {
                                               //  write_log('GPSLongitudeRef',exif.substr(gps_offset+8,1));
                                             }
                                         }
                                     }
                                     // GPSLatitude РёР»Рё GPSLongitude
                                     else if (tag_id==0x0002 || tag_id==0x0004) {
                                         var rational_offset=tag_value+6;
                                         var val1=this.mask4(exif.substr(rational_offset+4*0,4));
                                         var div1=this.mask4(exif.substr(rational_offset+4*1,4));
                                         var val2=this.mask4(exif.substr(rational_offset+4*2,4));
                                         var div2=this.mask4(exif.substr(rational_offset+4*3,4));
                                         var val3=this.mask4(exif.substr(rational_offset+4*4,4));
                                         var div3=this.mask4(exif.substr(rational_offset+4*5,4));
                                         if (div1!=0 && div2!=0 && div3!=0) {
                                             var tmp=Math.ceil(val1/div1)+'.';
                                             tmp+=Math.floor((val2/div2/60+val3/div3/3600)*1000000).toString();
                                             if (tag_id==0x0002) {
                                               //  write_log('GPSLatitude',tmp);
                                             }
                                             else {
                                               //  write_log('GPSLongitude',tmp);
                                             }
                                         }
                                     }
                                     gps_offset+=12;
                                 }
                             }
                         }
                         // ExifOffset
                         else if (tag_id==0x8769 && tag_len>0) {
                             // РђРґСЂРµСЃ СЂР°СЃС€РёСЂРµРЅРЅРѕР№ EXIF-СЃРµРєС†РёРё
                             var exif_offset=tag_value+6;
                             // РљРѕР»РёС‡РµСЃС‚РІРѕ EXIF-С‚РµРіРѕРІ
                             var num_of_exif_tags=tag_id=this.mask2(exif.substr(exif_offset,2));

                             if (num_of_exif_tags>0) {
                                 exif_offset+=2;

                                 // РћР±СЂР°Р±РѕС‚РєР° EXIF-С‚РµРіРѕРІ
                                 for (j=0; j<num_of_exif_tags; j++) {
                                     // ID С‚РµРіР°, СЂР°Р·РјРµСЂ РґР°РЅРЅС‹С… Рё Р·РЅР°С‡РµРЅРёРµ С‚РµРіР°
                                     tag_id=this.mask2(exif.substr(exif_offset,2));
                                     tag_len=this.mask4(exif.substr(exif_offset+4,4));
                                     tag_value=this.mask4(exif.substr(exif_offset+8,4));

                                     // DateTimeOriginal
                                     if (tag_id==0x9003 && tag_len>0 && tag_value>0) {
                                        // write_log('DateTimeOriginal',exif.substr(tag_value+6,tag_len));
                                     }
                                     // ExifVersion
                                     else if (tag_id==0x9000) {
                                         var tmp=exif.substr(exif_offset+9,1)+'.';
                                         if (exif.substr(exif_offset+11,1)!='0') {
                                             tmp+=exif.substr(exif_offset+10,2);
                                         }
                                         else {
                                             tmp+=exif.substr(exif_offset+10,1);
                                         }
                                        // write_log('ExifVersion',tmp);
                                     }
                                     // ExifImageWidth
                                     else if (tag_id==0xa002 && tag_len>0 && tag_value!=0) {
                                         var tmp=this.mask2(exif.substr(exif_offset+8,2));
                                         if (tmp==0) {
                                             tmp=this.mask2(exif.substr(exif_offset+10,2));
                                         }
                                        // write_log('ExifImageWidth',tmp);
                                     }
                                     // ExifImageHeight
                                     else if (tag_id==0xa003 && tag_len>0 && tag_value!=0) {
                                         var tmp=this.mask2(exif.substr(exif_offset+8,2));
                                         if (tmp==0) {
                                             tmp=this.mask2(exif.substr(exif_offset+10,2));
                                         }
                                        // write_log('ExifImageHeight',tmp);
                                     }
                                     // CreateDate
                                     else if (tag_id==0x9004 && tag_len>0 && tag_value>0) {
                                       //  write_log('CreateDate',exif.substr(tag_value+6,tag_len));
                                     }
                                     // UserComment
                                     else if (tag_id==0x9286 && tag_len>0 && tag_value>0) {
                                         var tmp=exif.substr(tag_value+6,tag_len);
                                         if (tmp.substr(0,5)=='ASCII') {
                                             tmp=tmp.substr(6);
                                         }
                                         else if (tmp.substr(0,7)=='UNICODE') {
                                             tmp=tmp.substr(8);
                                         }
                                       //  write_log('UserComment',tmp);
                                     }
                                     // OwnerName
                                     else if (tag_id==0xa430 && tag_len>0 && tag_value>0) {
                                        // write_log('OwnerName',exif.substr(tag_value+6,tag_len));
                                     }
                                     // SerialNumber
                                     else if (tag_id==0xa431 && tag_len>0 && tag_value>0) {
                                        // write_log('SerialNumber',exif.substr(tag_value+6,tag_len));
                                     }
                                     // LensMake
                                     else if (tag_id==0xa433 && tag_len>0 && tag_value>0) {
                                        // write_log('LensMake',exif.substr(tag_value+6,tag_len));
                                     }
                                     // LensModel
                                     else if (tag_id==0xa434 && tag_len>0 && tag_value>0) {
                                       //  write_log('LensModel',exif.substr(tag_value+6,tag_len));
                                     }
                                     // LensSerialNumber
                                     else if (tag_id==0xa435 && tag_len>0 && tag_value>0) {
                                       //  write_log('LensSerialNumber',exif.substr(tag_value+6,tag_len));
                                     }
                                     // OwnerName
                                     else if (tag_id==0xfde8 && tag_len>0 && tag_value>0) {
                                       //  write_log('OwnerName',exif.substr(tag_value+6,tag_len));
                                     }
                                     // SerialNumber
                                     else if (tag_id==0xfde9 && tag_len>0 && tag_value>0) {
                                      //   write_log('SerialNumber',exif.substr(tag_value+6,tag_len));
                                     }
                                     // Lens
                                     else if (tag_id==0xfdea && tag_len>0 && tag_value>0) {
                                       //  write_log('Lens',exif.substr(tag_value+6,tag_len));
                                     }

                                     exif_offset+=12;
                                 }
                             }


                         }
                         offset+=12;
                     }
                 }
             }
         }
         idx+=(section_length-2);
     }
 }
}

//Р¤СѓРЅРєС†РёСЏ РґРµРєРѕРґРёСЂРѕРІР°РЅРёСЏ СЃС‚СЂРѕРєРё РёР· base64
function base64decode(str) {
 // РЎРёРјРІРѕР»С‹ РґР»СЏ base64-РїСЂРµРѕР±СЂР°Р·РѕРІР°РЅРёСЏ
 var b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg'+
                'hijklmnopqrstuvwxyz0123456789+/=';
 var b64decoded = '';
 var chr1, chr2, chr3;
 var enc1, enc2, enc3, enc4;

 str = str.replace(/[^a-z0-9\+\/\=]/gi, '');

 for (var i=0; i<str.length;) {
     enc1 = b64chars.indexOf(str.charAt(i++));
     enc2 = b64chars.indexOf(str.charAt(i++));
     enc3 = b64chars.indexOf(str.charAt(i++));
     enc4 = b64chars.indexOf(str.charAt(i++));

     chr1 = (enc1 << 2) | (enc2 >> 4);
     chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
     chr3 = ((enc3 & 3) << 6) | enc4;

     b64decoded = b64decoded + String.fromCharCode(chr1);

     if (enc3 < 64) {
         b64decoded += String.fromCharCode(chr2);
     }
     if (enc4 < 64) {
         b64decoded += String.fromCharCode(chr3);
     }
 }
 return b64decoded;
}