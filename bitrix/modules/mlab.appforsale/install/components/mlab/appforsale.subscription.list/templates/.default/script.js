(function(window) {

	if (!!window.JCSubscription)
	{
		return;
	}
	
	window.JCSubscription = function(arParams)
	{
		this.id = 0;
		
		this.activeTo = false;
		this.time = false;
		
		this.mess = arParams.mess;

		this.obRestore = null;
		this.obCancel = null;
		
		this.obInfo = null;
		
		this.ajaxUrl = '';
		this.visual = {
			ID: '',
			INFO: '',
			RESTORE: '',
			CANCEL: ''
		};
		
		this.obId = null;

		
		if ('object' === typeof arParams)
		{
			this.id = arParams.id;
			
			this.activeTo = arParams.activeTo;
			this.time = arParams.time;
			
			this.ajaxUrl = arParams.ajaxUrl;
			this.visual = arParams.visual;
		}

		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCSubscription.prototype.Init = function()
	{
		this.obId = BX(this.visual.ID);
		this.obInfo = BX(this.visual.INFO);
	
		
		if (this.activeTo)
		{
			if (this.activeTo > this.time)
			{
				this.showCancel(this.activeTo);	
			}
			else
			{
				this.showRestore(this.activeTo);
			}
		}
		else
		{
			this.showFree();
		}
	}

	window.JCSubscription.prototype.showRestore = function(date)
	{
		BX.html(this.obId, '')
		BX.html(this.obInfo, this.mess.expired + '<br /><small>' + BX.date.format('d F Y H:i', parseInt(date), parseInt(this.time)) + '</small>');
		
		this.obRestore = BX.create('button', {
			props: {
				className: 'btn btn-warning btn-sm btn-block'
			},
			html: this.mess.restore,
			events: {
				click: BX.delegate(this.requestRestore, this)
			}
		});
		
		BX.append(this.obRestore, this.obId);
	}
		
	window.JCSubscription.prototype.requestRestore = function(e)
	{
		BX.PreventDefault(e);
		
		this.obRestore.setAttribute('disabled', 'disabled');
		BX.ajax({
			url: this.ajaxUrl,
			method: 'POST',
			data: {
				id: this.id,
				action: 'restore'
			},
			dataType: 'json',
			onsuccess: BX.delegate(this.successRestore, this),
			onfailure: BX.delegate(this.failureRestore, this)
		});
	}
	
	window.JCSubscription.prototype.successRestore = function(data)
	{
		if (data.response)
		{
			this.showCancel(data.response.activeTo);
		}
		else if (data.error)
    	{
			this.failureRestore();
			alert(data.error.error_msg);
    	}
	}
	
	window.JCSubscription.prototype.failureRestore = function()
	{
		this.obRestore.removeAttribute('disabled');
	}
	
	window.JCSubscription.prototype.showCancel = function(date)
	{
		BX.html(this.obId, '')
		BX.html(this.obInfo, this.mess.active_to + '<br /><small>' + BX.date.format('d F Y H:i', parseInt(date), parseInt(this.time)) + '</small>');
		
		this.obCancel = BX.create('button', {
			props: {
				className: 'btn btn-danger btn-sm btn-block'
			},
			html: this.mess.cancel,
			events: {
				click: BX.delegate(this.requestCancel, this)
			}
		});
		
		BX.append(this.obCancel, this.obId);
	}
	
	window.JCSubscription.prototype.requestCancel = function(e)
	{
		BX.PreventDefault(e)
		
		this.obCancel.setAttribute('disabled', 'disabled');
		BX.ajax({
			url: this.ajaxUrl,
			method: 'POST',
			data: {
				id: this.id,
				action: 'cancel'
			},
			dataType: 'json',
			onsuccess: BX.delegate(this.successCancel, this),
			onfailure: BX.delegate(this.failureCancel, this)
		});
	}
	
	window.JCSubscription.prototype.successCancel = function(data)
	{
		if (data.response)
		{
			this.showRestore(data.response.activeTo);
		}
		else if (data.error)
    	{
			this.failureCancel();
			alert(data.error.error_msg);
    	}
	}
	
	window.JCSubscription.prototype.failureCancel = function()
	{
		this.obCancel.removeAttribute('disabled');
	}
	
	window.JCSubscription.prototype.showFree = function()
	{
		BX.html(this.obId, '')
		BX.html(this.obInfo, this.mess.info)
	}

})(window);