<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$action = $request->get('action');
$id = intval($request->get('id'));
if (empty($action))
	return;

if ($id == 0)
	return;

if (!CModule::IncludeModule('iblock'))
	return;

if (!CModule::IncludeModule('mlab.appforsale'))
	return;

$arRecurring = CAppforsaleRecurring::GetByID($id);
if ($arRecurring && $arRecurring['USER_ID'] == $USER->GetID())
{
	if ($action == 'restore')
	{
// 		if ($arRecurring['CANCELED'] == 'N')
// 		{
		iF (CAppforsaleRecurring::NextPayment($id))
		{
			$dbProfile = CIBlockElement::GetByID($arRecurring['PRODUCT_ID']);
			if($arProfile = $dbProfile->GetNext())
			{
				$arRes = array("response" => array('activeTo' => MakeTimeStamp($arProfile['ACTIVE_TO'], "DD.MM.YYYY HH:MI:SS")));
			}
		}
		else
		{
			if($ex = $APPLICATION->GetException())
				$arRes = array("error" => array('error_msg' => $ex->GetString() ));
		}
		
// 		}
	}
	elseif ($action == 'cancel')
	{
// 		if ($arRecurring['CANCELED'] == 'N')
// 		{
			if (CAppforsaleRecurring::CancelRecurring($arRecurring['ID'], 'Y', 'Canceled by the user'))
			{
				$obProfile = new CIBlockElement;
				$obProfile->Update($arRecurring['PRODUCT_ID'], array('ACTIVE_TO' => date('d.m.Y H:i:s')));
				$arRes = array("response" => array('activeTo' => time()));
			}
// 		}
	}
}

die(\Bitrix\Main\Web\Json::encode($arRes));
?>