<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskCustomerDetail extends \Mlab\Appforsale\Component\Element
{
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		unset($arFilter['ACTIVE']);
		$arFilter['CREATED_BY'] = $USER->GetID();
		return $arFilter;
	}
}
?>