<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<?foreach ($arResult['ITEMS'] as $arItem):?>
	<?
	$APPLICATION->IncludeComponent(
		'mlab:appforsale.task.item',
		'',
		array(
			'RESULT' => array(
				'ITEM' => $arItem,
				'AREA_ID' => $this->GetEditAreaId($arItem['ID']),
			)
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
	?>
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>