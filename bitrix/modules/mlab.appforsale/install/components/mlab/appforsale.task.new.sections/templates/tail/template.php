<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
	<div>
		<?foreach ($arResult['ITEMS'] as $key=>$arItem):?>		
			<div class="col-xs-6" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
				<?$fields = is_array($arItem['FIELDS']) ? implode(' / ', $arItem['FIELDS']) : $arItem['FIELDS'];?>
				<a class="afs-section-wrap" title="<?=$arItem['NAME']?>" href="<?=(($arItem['RIGHT_MARGIN'] - $arItem['LEFT_MARGIN']) == 1) ? $arItem['DETAIL_PAGE_URL'] : $arItem['SECTION_PAGE_URL']?>">
						<div class="text-center">
							<img class="afs-section-img afs-color" src="<?=$arItem['PICTURE'] ? $arItem['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png'?>" />
						</div>
						<div class="text-center afs-section-info ">
							<div class="afs-section-name"><?=$arItem['NAME']?></div>
							<div class="afs-section-field"><?=(is_array($arItem['FIELDS'])? implode(' / ', $arItem['FIELDS']) : $arItem['FIELDS'])?></div>
							<?
							$price = $arItem['UF_PRICE'];
							if ($price > 0)
							{
								?>
							<div class="afs-section-field"><?=AppforsaleFormatCurrency($price)?></div>
								<?
							}
							?>

						</div>
				</a>
			</div>
			<?if ($key > 0 && ($key + 1) % 2 == 0):?>
				<div class="clearfix"></div>
			<?endif;?>
		<?endforeach;?>
	</div>
<?endif;?>