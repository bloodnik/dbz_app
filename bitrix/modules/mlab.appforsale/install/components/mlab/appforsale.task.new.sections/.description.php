<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('TASK_NEW_SECTIONS_NAME'),
	'SORT' => 10,
	'DESCRIPTION' => Loc::getMessage('TASK_NEW_SECTIONS_DESCRIPTION'),
	'COMPLEX' => 'N',
	'PATH' => array(
		'ID' => 'appforsale',
		'NAME' => Loc::getMessage('TASK_NEW_SECTIONS_PATH_NAME'),
		'SORT' => 1,
		'CHILD' => array(
			'ID' => 'task',
			'NAME' => Loc::getMessage('TASK_NEW_SECTIONS_LIST_PATH_CHILD_NAME'),
			'SORT' => 10
		)
	)
);
?>