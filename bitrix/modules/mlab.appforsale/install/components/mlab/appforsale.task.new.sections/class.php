<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskNewSectionList extends \Mlab\Appforsale\Component\SectionList
{
	protected function getSelect()
	{
		$arSelect = parent::getSelect();
		$arSelect[] = 'UF_PRICE';
		return $arSelect;
	}
}
?>