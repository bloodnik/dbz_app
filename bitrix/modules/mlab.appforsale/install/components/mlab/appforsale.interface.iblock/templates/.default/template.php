<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<div class="afs-task-customer-rows">
<?
foreach ($arResult['ITEMS'] as $arItem)
{
	switch ($arItem['TYPE'])
	{
		case 'S':
			?>
			<div style="cursor: pointer" class="afs-iblock-item" onclick="showE('<?=$arParams['PID']?>', <?=$arItem['ID']?>);" style="position: relative"><?=$arItem['NAME']?></div>
			<?
			break;	
		case 'E':
			?>
			<div style="cursor: pointer" class="afs-iblock-item" onclick="BX.html(BX('address_<?=$arParams['PID']?>_name'), '<?=$arItem['NAME']?>'); BX('address_<?=$arParams['PID']?>_value').value='<?=$arItem['ID']?>'; __bq.hideAll(true)" style="position: relative"><?=$arItem['NAME']?></div>
			<?
			break;
	}		
}
?>
</div>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>