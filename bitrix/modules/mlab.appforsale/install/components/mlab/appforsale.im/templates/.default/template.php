<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId();
$arItemIDs = array(
	'ID' => $strMainID,
	'LIST' => $strMainID.'_list',
	'INPUT' => $strMainID.'_input',
	'BUTTON' => $strMainID.'_button'
);
?>

<div class="im-chat">
	<div class="im-body">
		<div class="im-history" id="<?=$arItemIDs['LIST']?>"></div>
	</div>
</div>
<div class="im-input">
	<form class="input-group" id="form">
		<input placeholder="<?=GetMessage('PLACEHOLDER')?>" type=text tabindex="0" class="form-control" id="<?=$arItemIDs['INPUT']?>" />
		<span class="input-group-addon" id="<?=$arItemIDs['BUTTON']?>"><?=GetMessage('SEND')?></span>
	</form>
</div>
<?
$arJSParams = array(
	'from' => array(
		'id' => $arResult['FROM']['ID'],
		'personal_photo' => $arResult['FROM']['PERSONAL_PHOTO'] ?: array('src' => $templateFolder.'/images/no_photo.png'),
		'format_name' => $arResult['FROM']['FORMAT_NAME']
	),
	'user' => array(
		'id' => $arResult['USER']['ID'],
		'personal_photo' => $arResult['USER']['PERSONAL_PHOTO'] ?: array('src' => $templateFolder.'/images/no_photo.png'),
		'format_name' => $arResult['USER']['FORMAT_NAME']
	),
	'VISUAL' => array(
		'ID' => $arItemIDs['ID'],
		'LIST' => $arItemIDs['LIST'],
		'INPUT' => $arItemIDs['INPUT'],
		'BUTTON' => $arItemIDs['BUTTON']
	)
);
echo '<script type="text/javascript">var im = new JCIm('.CUtil::PhpToJSObject($arJSParams, false, true).');</script>';
?>