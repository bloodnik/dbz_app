/* mlab:appforsale.im */
(function (window) {
	
	if (!!window.JCIm)
	{
		return;
	}
	

	
	window.JCIm = function (arParams)
	{
		this.ts = 0;
		this.audio = new Audio();
		this.audio.src = '/bitrix/components/mlab/appforsale.im/templates/.default/sounds/message.mp3';
		
		this.from = {'id': '', 'personal_photo': {'src': '', 'width': '', 'height': '', 'size': ''}, 'format_name': ''};
		this.user = {'id': '', 'personal_photo': {'src': '', 'width': '', 'height': '', 'size': ''}, 'format_name': ''};
		
	
		this.start_message_id = 0;
		this.progress = true;
		
		this.visual = {
			ID: '',
			LIST: '',
			INPUT: '',
			BUTTON: ''
		};
		
		this.obList = null;
		this.obInput = null;
		this.obButton = null;
		
		if ('object' === typeof arParams)
		{
			this.from = arParams.from;
			this.user = arParams.user;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
//	window.JCIm.prototype.getByID = function(id)
//	{
//	
//		BX.ajax({
//			url: '/bitrix/components/mlab/appforsale.im/get.php',
//			method: 'POST',
//			data: {
//				user_id: this.user.id,
//				id: id
//			},
//			dataType: 'json',
//			onsuccess: BX.delegate(function(data) {
//				
//				
//				this.AddMessage2(data.response.items[0].body);
//				BX.scrollTop(document.body, 999999);
//			}, this),
//		});
//	}
	
	window.JCIm.prototype.get = function()
	{
		this.progress = true;
		BX.ajax({
			url: '/bitrix/components/mlab/appforsale.im/get.php',
			method: 'POST',
			data: {
				user_id: this.user.id,
				start_message_id: this.start_message_id
			},
			dataType: 'json',
			onsuccess: BX.delegate(this.json, this),
			onfailure: BX.delegate(function() {
				this.progress = false;
			}, this)
		});
	}
	
	window.JCIm.prototype.json = function(data)
	{
		if (data.response.items.length == 0)
			return false;
		
		//alert(data.response.count);
		var previous_height = window.scrollY;

		data.response.items.forEach(BX.delegate(function(message, index) {
			this.start_message_id = message.id;
			previous_height = previous_height + this.AddMessage(message);
			
		}, this));
//		
		
		//alert(this.start_message_id);
//		//
//		var previous_height = 0;
//		current_top_element.prevAll().each(function() {
//		  previous_height += this.outerHeight();
//		});
	
	//	alert(previous_height);
//		$('body').scrollTop(previous_height);
	
	//	window.scrollY = previous_height;
	//	document.body.scrollTop(previous_height);
		BX.scrollTop(document.body, previous_height);
		setTimeout(BX.delegate(function() {
			
			this.progress = false;
		}, this), 5);
		
	}

	window.JCIm.prototype.Pull = function()
	{
		var _ajax = BX.ajax({
			url: 'http://' + window.location.hostname + '/bitrix/sub/?CHANNEL_ID='+this.from.id,
			method: 'GET',
			onsuccess: BX.delegate(function(data) {
				
				if (data.length > 0)
				{
					var dataArray = data.match(/#!NGINXNMS!#(.*?)#!NGINXNME!#/gm);
					if (dataArray != null)
					{
						for (var i = 0; i < dataArray.length; i++)
						{
							dataArray[i] = dataArray[i].substring(12, dataArray[i].length-12);
							if (dataArray[i].length <= 0)
								continue;

							var message = BX.parseJSON(dataArray[i]);
							var data = null;
						
							if (message && message.text)
								data = message.text;
							
							if (data !== null)
							{
								this.AddMessage2(data, 0);	
								this.audio.play();
								BX.scrollTop(document.body, 999999);
							}
						}
					}
				}
				
				this.Pull();
				
//				
////				if (data.ts && data.ts > 0)
////				{
////					this.ts = data.ts;
////				}
//				
//				if (data.updates)
//				{
//					if (data.updates.length > 0)
//					{
//				
//						for (var i = 0; i < data.updates.length; i++)
//						{
//							this.AddMessage2(data.updates[i], 0);	
//						}
//						this.audio.play();
//						BX.scrollTop(document.body, 999999);
//					}
//					this.Pull();
//				}
//				else
//				{
//					setTimeout(this.Pull(), 25000);
//				}	
			}, this),
			onfailure: BX.delegate(function(data) {
				
//				if (_ajax && (_ajax.status == 403 || _ajax.status == 404 || _ajax.status == 400))
//				{
//					setTimeout(this.Pull(), 25000);
//				}
//				
				
			}, this)
		});
	}
	
	window.JCIm.prototype.Init = function()
	{
		this.get();
		
		
		//var supportsTouch = ('ontouchstart' in document.documentElement);
//if (supportsTouch)
	//alert('phone');
		
		this.obList = BX(this.visual.LIST);
		this.obInput = BX(this.visual.INPUT);

		
		BX('form').onsubmit = BX.delegate(function(e) {
			this.Send();
			e.preventDefault();
		}, this);
		
		setTimeout(BX.delegate(function() {
			
			document.addEventListener('scroll', BX.delegate(this.Scroll, this));
		}, this), 300);
		
	//	document.addEventListener("touchmove", function(e) { e.preventDefault() });

		
		
		this.obButton = BX(this.visual.BUTTON);
		if (!!this.obButton)
		{
			BX.bind(this.obButton, 'click', BX.delegate(function(e) {
				//BX.focus(this.obInput);
				this.Send();
				e.preventDefault();
				
			}, this));
			BX.bind(this.obButton, 'touchstart', BX.delegate(function(e) {
				e.preventDefault();
				this.Send();
		
			}, this));
		}

		
		//var supportsTouch = ('ontouchstart' in document.documentElement);
	//	if (!supportsTouch)
			this.Pull();
	}
	
	window.JCIm.prototype.Scroll = function(e)
	{	
		if (window.scrollY < 500)
		{
			if (!this.progress)
			{
				console.log('Scroll', window.scrollY);
				BX.delegate(this.get(), this);
			}
		}
		
//		if (!this.progress)
//		{
//			var scrollTop = BX.scrollTop(document.body);
//			console.log('scrollTop', scrollTop);
//			if (scrollTop < 500)
//			{
//				this.progress = true;
//				this.get();
//			}
//		}
		//var scrollTop = BX.scrollTop(window);
		//console.log('scrollTop', scrollTop);
	}
		
	window.JCIm.prototype.Send = function()
	{	
		if (this.obInput.value.length <= 0)
			return;
		var status = this.AddMessage2(this.obInput.value, 1);
		BX.scrollTop(document.body, 999999);
		

		
		BX.ajax({
			url: '/bitrix/components/mlab/appforsale.im/send.php',
			method: 'POST',
			data: {
				'user_id': this.user.id,
				'message': this.obInput.value
			},
			onsuccess: BX.delegate(function() {
				//BX.html(status, 'v');
			}, this),
			onfailure: BX.delegate(function() {
				
			}, this)
		});
		BX('form').reset();
	}
	
	window.JCIm.prototype.AddMessage2 = function(body, out)
	{
		var src = '/bitrix/components/mlab/appforsale.im/templates/.default/images/no_photo.png';
		if (out == 1)
			src = this.from.personal_photo.src;
		else
			src = this.user.personal_photo.src;
		//var oldScroll = BX.scrollTop(document.body);
		
		var date = new Date();
		
		var message = BX.create('div', {props: {className: 'afs-im-message'}, html: '<div class="afs-im-message-image"><img class="afs-im-message-img" src="' + src + '" /></div><div class="afs-im-message-body"><div style="color: #ccc; font-size: 12px;">' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + '</div><div>' + body + '</div></div><span></span>'});
		var image = BX.firstChild(message);
		var body = BX.nextSibling(image);
		var status = BX.nextSibling(body);
		BX.append(message, this.obList);
		return status;
		//BX.scrollTop(document.body, oldScroll+message.offsetHeight);
	}
	
	window.JCIm.prototype.AddMessage = function(message)
	{
		var personal_photo = '';
		if (message.out == 1)
		{
			personal_photo = this.from.personal_photo.src			
		}
		else
		{
			personal_photo = this.user.personal_photo.src
		}
		
		
		var date = new Date(message.date * 1000);
		
		var message = BX.create('div', {
				props: {
					className: 'afs-im-message'
				},
				html: '<div class="afs-im-message-image"><img class="afs-im-message-img" src="' + personal_photo + '" /></div><div class="afs-im-message-body"><div style="color: #ccc; font-size: 12px;">' + date.toLocaleDateString() + ' ' + date.toLocaleTimeString() + '</div><div>' + message.body + '</div></div>'
			}
		);
		
		
		
		
		
		
		
		BX.prepend(message, this.obList);
	//	BX.scrollTop(document.body, oldScroll+message.offsetHeight);
		return message.offsetHeight;
	}
	
	
})(window);