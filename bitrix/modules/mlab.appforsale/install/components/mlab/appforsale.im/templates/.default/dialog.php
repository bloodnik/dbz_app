<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$APPLICATION->IncludeComponent(
	'mlab:appforsale.im.dialog',
	'',
	array(
		'MESSAGE_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['message']
	),
	$component
);
?>