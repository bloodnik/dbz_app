<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();


class Im extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $APPLICATION;
		
		$arComponentVariables = array(
			'sel'
		);
		
		$arDefaultVariableAliases = array('sel' => 'sel');
		
		$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $this->arParams['VARIABLE_ALIASES']);
		CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
		
		$componentPage = '';
		
		if(isset($arVariables['sel']) && intval($arVariables['sel']) > 0)
			$componentPage = 'message';
		else
			$componentPage = 'dialog';

		$this->arResult = array(
			'FOLDER' => '',
			'URL_TEMPLATES' => array(
				'dialog' => htmlspecialcharsbx($APPLICATION->GetCurPage()),
				'message' => htmlspecialcharsbx($APPLICATION->GetCurPage().'?'.$arVariableAliases['sel'].'=#sel#'),
			),
			'VARIABLES' => $arVariables,
			'ALIASES' => $arVariableAliases
		);
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>