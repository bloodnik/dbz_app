<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId('');
$arItemIDs = array(
	'ID' => $strMainID,
	'BUTTON' => $strMainID.'_button'
);

$strPrice = CAppforsaleRecurring::ToString(array(
		'UF_RECUR_TYPE_TEST' => COption::GetOptionString('mlab.appforsale', "RECUR_TYPE_TEST", 'M'),
		'UF_RECUR_LENGTH_TEST' => COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH_TEST", 0),
		'UF_RECUR_PRICE_TEST' => COption::GetOptionInt('mlab.appforsale', "RECUR_PRICE_TEST", 0),
		'UF_RECUR_TYPE' => COption::GetOptionString('mlab.appforsale', "RECUR_TYPE", 'M'),
		'UF_RECUR_LENGTH' => COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH", 0),
		'UF_RECUR_PRICE' => COption::GetOptionInt('mlab.appforsale', "RECUR_PRICE", 0)
), true);
?>

<?if ($arParams['RECUR']):?>
	<p class="bg-warning" style="padding: 15px; margin-top: 15px;"><?=GetMessage('INFO')?><br /><?=$strPrice?></p>
	<div><button id="<?=$arItemIDs['BUTTON']?>" class="btn btn-primary"><?=GetMessage('RECUR')?></button></div>
<?else:?>
	<p class="bg-warning" style="padding: 15px; margin-top: 15px;"><?=GetMessage('INFO')?><br /><?=$strPrice?></p>
	<div><button id="<?=$arItemIDs['BUTTON']?>" class="btn btn-primary"><?=GetMessage('PAY')?></button></div>
<?endif;?>

<?
$arJSParams = array(
	'recur_id' => intval($arParams['RECUR']['ID']),
	'VISUAL' => $arItemIDs
);
echo '<script type="text/javascript">new JCPersonalProfilePay('.CUtil::PhpToJSObject($arJSParams, false, true).');</script>';
?>