<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);


if (!CModule::IncludeModule('mlab.appforsale'))
	return;

if (!$USER->IsAuthorized())
	return;

$id = intval($request->get('id'));

if ($id > 0)
{
	$arRecurring = CAppforsaleRecurring::GetByID($id);
	if ($arRecurring && $arRecurring['USER_ID'] == $USER->GetID())
	{
		if (CAppforsaleRecurring::NextPayment($id))
		{
			$arRes = array("response" => 1);
		}
		else
		{
			if($ex = $APPLICATION->GetException())
				$arRes = array("error" => array('error_msg' => $ex->GetString() ));
		}
	}
}
else
{
	$bAccess = true;
	$arParams = array(
		'RECUR_TYPE_TEST' => COption::GetOptionString('mlab.appforsale', "RECUR_TYPE_TEST", 'M'),
		'RECUR_LENGTH_TEST' => COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH_TEST", 0),
		'RECUR_PRICE_TEST' => COption::GetOptionInt('mlab.appforsale', "RECUR_PRICE_TEST", 0),
		'RECUR_TYPE' => COption::GetOptionString('mlab.appforsale', "RECUR_TYPE", 'M'),
		'RECUR_LENGTH' => COption::GetOptionInt('mlab.appforsale', "RECUR_LENGTH", 0),
		'RECUR_PRICE' => COption::GetOptionInt('mlab.appforsale', "RECUR_PRICE", 0)
	);
	
	if (intval($arParams['RECUR_LENGTH_TEST']) > 0)
	{
		$arParams['NEXT_DATE'] = CAppforsaleRecurring::GetNextDate(array(
				"RECUR_SCHEME_TYPE" => $arParams['RECUR_TYPE_TEST'],
				"RECUR_SCHEME_LENGTH" => $arParams['RECUR_LENGTH_TEST']
		));
	
		if (intval($arParams['RECUR_PRICE_TEST']) > 0)
		{
			$bAccess = CAppforsaleUserAccount::Pay($USER->GetID(), $arParams['RECUR_PRICE_TEST']);
		}
	}
	else if (intval($arParams['RECUR_LENGTH']) > 0 && intval($arParams['RECUR_PRICE']) > 0)
	{
		$arParams['NEXT_DATE'] = CAppforsaleRecurring::GetNextDate(array(
				"RECUR_SCHEME_TYPE" => $arParams['RECUR_TYPE'],
				"RECUR_SCHEME_LENGTH" => $arParams['RECUR_LENGTH']
		));
		
		$bAccess = CAppforsaleUserAccount::Pay($USER->GetID(), $arParams['RECUR_PRICE']);
	}
		
	if ($bAccess)
	{
		if (CAppforsaleRecurring::Add(array(
				'USER_ID' => $USER->GetID(),
				'MODULE' => 'mlab.appforsale',
				'NEXT_DATE' => $arParams['NEXT_DATE']
		)))
		{
			$arRes = array("response" => 1);
		}
		else
		{
			if($ex = $APPLICATION->GetException())
				$arRes = array("error" => array('error_msg' => $ex->GetString() ));
		}
	}
	else
	{
		if($ex = $APPLICATION->GetException())
			$arRes = array("error" => array('error_msg' => $ex->GetString() ));
	}
}

die(\Bitrix\Main\Web\Json::encode($arRes));
?>