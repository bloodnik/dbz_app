<?
$MESS['SECTIONS_SETTINGS'] = 'Settings list of partitions';
$MESS['FORM_SETTINGS'] = 'Settings edit form';
$MESS['IBLOCK_TYPE'] = 'The type of InfoBlock';
$MESS['IBLOCK'] = 'Infoblok';
$MESS['SEF_MODE_SECTIONS'] = 'A list of topics';
$MESS['SEF_MODE_FORM'] = 'Form';
$MESS['SECTIONS_FIELD_CODE'] = 'Field';
$MESS['FORM_PROPERTY_CODE'] = 'Properties';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['FORM_MESS_BTN_ADD'] = 'The text of the button "Save"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Save';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'The inscription empty properties';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not installed)';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'The partition ID';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Character code section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'The path of the symbolic codes section';
?>