<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskNewForm extends CBitrixComponent
{
	public function executeComponent()
	{
		global $USER, $USER_FIELD_MANAGER;
		
		if (!$USER->IsAuthorized())
			CAppforsale::AuthForm();
		
		if (empty($USER->GetFirstName()) || empty($USER_FIELD_MANAGER->GetUserFieldValue("USER", "UF_CITY", $USER->GetID())))
			LocalRedirect("/youdo/personal/profile/edit.php");

		$this->IncludeComponentTemplate();
	}
}
?>