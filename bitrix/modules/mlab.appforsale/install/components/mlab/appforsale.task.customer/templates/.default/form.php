<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.form',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
		'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
		'ID' => $arResult['VARIABLES']['ELEMENT_ID'],
		'PROPERTY_CODE' => $arParams['FORM_PROPERTY_CODE'],
		'PROPERTY_CODE_REQUIRED' => $arParams['FORM_PROPERTY_CODE_REQUIRED'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'MESS_PROPERTY_VALUE_NA' => $arParams['FORM_MESS_PROPERTY_VALUE_NA'],
		'MESS_BTN_ADD' => $arParams['FORM_MESS_BTN_ADD']
	),
	$component
);
?>

