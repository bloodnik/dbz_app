<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$comment_url = CComponentEngine::MakePathFromTemplate($arResult['FOLDER'].$arResult['URL_TEMPLATES']['comment'], $arResult['VARIABLES']);
?>
<div>
	<div class="col-md-8">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.task.customer.detail',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'FIELD_CODE' => $arParams['DETAIL_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME']
			),
			$component
		);
		?>
		<div class="clearfix"></div>
		<? 
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.offer',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['OFFER_IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['OFFER_IBLOCK_ID'],
				'TASK_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'FIELD_CODE' => $arParams['OFFER_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['OFFER_PROPERTY_CODE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'MESS_OFFER' => $arParams['OFFER_MESS_OFFER'],
				'MESS_BTN_REJECT' => $arParams['OFFER_MESS_BTN_REJECT'],
				'MESS_BTN_CONFIRM' => $arParams['OFFER_MESS_BTN_CONFIRM'],
				'MESS_BTN_CANCEL' => $arParams['OFFER_MESS_BTN_CANCEL'],
				'MESS_BTN_DONE' => $arParams['OFFER_MESS_BTN_DONE'],
				'MESS_BTN_COMMENT' => $arParams['OFFER_MESS_BTN_COMMENT'],
				'COMMENT_URL' => $comment_url
			),
			$component
		);
		?>
		
	</div>
	<div class="col-md-4">
	</div>
</div>