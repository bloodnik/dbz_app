<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
CModule::IncludeModule('iblock');
$dbElement = CIBlockElement::GetList(array(), array('ID' => $arResult['VARIABLES']['ELEMENT_ID'], 'ACTIVE' => 'Y'), false, false, array('ID', 'PROPERTY_PROFILE_ID'));
$arElement = $dbElement->GetNext();

$APPLICATION->IncludeComponent(
	'mlab:appforsale.form',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['COMMENT_IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['COMMENT_IBLOCK_ID'],
		'PROPERTY_CODE' =>$arParams['COMMENT_PROPERTY_CODE'],
		'PROPERTY_CODE_REQUIRED' =>$arParams['COMMENT_PROPERTY_CODE_REQUIRED'],
		'MESS_PROPERTY_VALUE_NA' => $arParams['COMMENT_MESS_PROPERTY_VALUE_NA'],
		'MESS_BTN_ADD' => $arParams['COMMENT_MESS_BTN_ADD'],
		'UPDATE_PROPERTY_VALUES' => array(
			'PROFILE_ID' => $arElement['PROPERTY_PROFILE_ID_VALUE'],
			'TASK_ID' => $arElement['ID']
		)
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
?>