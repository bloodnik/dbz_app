<?
$MESS['LIST_SETTINGS'] = 'Настройки списка';
$MESS['DETAIL_SETTINGS'] = 'Настройки детального просмотра';
$MESS['OFFER_SETTINGS'] = 'Настройки предложений';
$MESS['COMMENT_SETTINGS'] = 'Настройки комментариев';
$MESS['FORM_SETTINGS'] = 'Настройки формы редактирования';

$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['SEF_MODE_LIST'] = 'Список заданий';
$MESS['SEF_MODE_DETAIL'] = 'Детально';
$MESS['SEF_MODE_COMMENT'] = 'Комментарий';
$MESS['ELEMENT_SORT_FIELD'] = 'Первое поле для сортировки';
$MESS['ELEMENT_SORT_ORDER'] = 'Направление первой сортировки';
$MESS['ELEMENT_SORT_FIELD2'] = 'Второе поле для сортировки';
$MESS['ELEMENT_SORT_ORDER2'] = 'Направление второй сортировки';
$MESS['SORT_ASC'] = 'по возрастанию';
$MESS['SORT_DESC'] = 'по убыванию';
$MESS['LIST_FIELD_CODE'] = 'Поля';
$MESS['LIST_PROPERTY_CODE'] = 'Свойства';
$MESS['DETAIL_FIELD_CODE'] = 'Поля';
$MESS['DETAIL_PROPERTY_CODE'] = 'Свойства';
$MESS['OFFER_FIELD_CODE'] = 'Поля';
$MESS['OFFER_PROPERTY_CODE'] = 'Свойства';
$MESS['OFFER_MESS_OFFER'] = 'Надпись "Предложения"';
$MESS['OFFER_MESS_OFFER_DEFAULT'] = 'Предложения';
$MESS['OFFER_MESS_BTN_REJECT'] = 'Текст кнопки "Отклонить"';
$MESS['OFFER_MESS_BTN_REJECT_DEFAULT'] = 'Отклонить';
$MESS['OFFER_MESS_BTN_CONFIRM'] = 'Текст кнопки "Подтвердить"';
$MESS['OFFER_MESS_BTN_CONFIRM_DEFAULT'] = 'Подтвердить';
$MESS['OFFER_MESS_BTN_CANCEL'] = 'Текст кнопки "Отменить"';
$MESS['OFFER_MESS_BTN_CANCEL_DEFAULT'] = 'Отменить';
$MESS['OFFER_MESS_BTN_DONE'] = 'Текст кнопки "Выполнено"';
$MESS['OFFER_MESS_BTN_DONE_DEFAULT'] = 'Выполнено';
$MESS['OFFER_MESS_BTN_COMMENT'] = 'Текст кнопки "Оставить отзыв"';
$MESS['OFFER_MESS_BTN_COMMENT_DEFAULT'] = 'Оставить отзыв';

$MESS['COMMENT_PROPERTY_CODE'] = 'Свойства';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'Свойства обязательные для заполнения';
$MESS['COMMENT_MESS_BTN_ADD'] = 'Текст кнопки "Сохранить"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Сохранить';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Надпись незаполненного свойства';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(не установлено)';

$MESS['FORM_PROPERTY_CODE'] = 'Свойства';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'Свойства обязательные для заполнения';
$MESS['FORM_MESS_BTN_ADD'] = 'Текст кнопки "Сохранить"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Сохранить';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'Надпись незаполненного свойства';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(не установлено)';

$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Идентификатор раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Символьный код раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Путь из символьных кодов раздела';

$MESS['USE_SWITCH'] = 'Использовать переключатель';
?>