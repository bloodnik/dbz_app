<?
$MESS['LIST_SETTINGS'] = 'ParamГЁtres de la liste';
$MESS['DETAIL_SETTINGS'] = 'Configuration dГ©taillГ©e de la navigation';
$MESS['OFFER_SETTINGS'] = 'ParamГЁtres d'offres';
$MESS['COMMENT_SETTINGS'] = 'ParamГЁtres des commentaires';
$MESS['FORM_SETTINGS'] = 'ParamГЁtres du formulaire d'Г©dition';

$MESS['IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР°';
$MESS['IBLOCK'] = 'un jeu de donnГ©es';
$MESS['SEF_MODE_LIST'] = 'Liste des tГўches';
$MESS['SEF_MODE_DETAIL'] = 'DГ©tail';
$MESS['SEF_MODE_COMMENT'] = 'Commentaires';
$MESS['ELEMENT_SORT_FIELD'] = 'le Premier champ de tri';
$MESS['ELEMENT_SORT_ORDER'] = 'la Direction du premier tri';
$MESS['ELEMENT_SORT_FIELD2'] = 'le DeuxiГЁme champ de tri';
$MESS['ELEMENT_SORT_ORDER2'] = 'la Direction de la deuxiГЁme tri';
$MESS['SORT_ASC'] = 'croissant';
$MESS['SORT_DESC'] = 'desc';
$MESS['LIST_FIELD_CODE'] = 'Champs';
$MESS['LIST_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['DETAIL_FIELD_CODE'] = 'Champs';
$MESS['DETAIL_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['OFFER_FIELD_CODE'] = 'Champs';
$MESS['OFFER_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['OFFER_MESS_OFFER'] = 'Inscription "Propositions"';
$MESS['OFFER_MESS_OFFER_DEFAULT'] = 'Offres';
$MESS['OFFER_MESS_BTN_REJECT'] = 'le Texte du bouton "Refuser"';
$MESS['OFFER_MESS_BTN_REJECT_DEFAULT'] = 'Rejeter';
$MESS['OFFER_MESS_BTN_CONFIRM'] = 'le Texte du bouton "Confirmer"';
$MESS['OFFER_MESS_BTN_CONFIRM_DEFAULT'] = 'Valider';
$MESS['OFFER_MESS_BTN_CANCEL'] = 'le Texte du bouton "Annuler"';
$MESS['OFFER_MESS_BTN_CANCEL_DEFAULT'] = 'Annuler';
$MESS['OFFER_MESS_BTN_DONE'] = 'le Texte du bouton "TerminГ©"';
$MESS['OFFER_MESS_BTN_DONE_DEFAULT'] = 'Done';
$MESS['OFFER_MESS_BTN_COMMENT'] = 'le Texte sur le bouton "Laisser un commentaire"';
$MESS['OFFER_MESS_BTN_COMMENT_DEFAULT'] = 'Laisser une rГ©ponse';

$MESS['COMMENT_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'PropriГ©tГ©s obligatoires';
$MESS['COMMENT_MESS_BTN_ADD'] = 'le Texte du bouton "Enregistrer"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Enregistrer';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Inscription vide propriГ©tГ©s';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(non dГ©fini)';

$MESS['FORM_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'PropriГ©tГ©s obligatoires';
$MESS['FORM_MESS_BTN_ADD'] = 'le Texte du bouton "Enregistrer"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Enregistrer';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'Inscription vide propriГ©tГ©s';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(non dГ©fini)';

$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Id de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'le code de CaractГЁre de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Chemin de caractГЁres des codes de la section';

$MESS['USE_SWITCH'] = 'Utiliser le commutateur';
?>