<?
$MESS['CLAIM_SUCCESS'] = 'the Complaint has been sent!';
$MESS['WARNING_LOGIN'] = 'To send an offer you need to log in<br /><br /><a class="btn btn-warning" href="/youdo/login/">Login</a>';
$MESS['WARNING_PROFILE'] = 'To send proposals you must create a profile of the artist<br /><br /><a class="btn btn-warning" href="/youdo/personal/profile/#PATH#form/">Create</a>';
$MESS['WARNING_SUBSCRIPTION'] = 'To send an offer you want to extend the profile of the artist<br /><br /><a class="btn btn-warning" href="/youdo/subscription/">go to subscriptions</a>';
$MESS['END'] = 'from the price of the task';

$MESS['WARNING_PROFILE_GLOBAL'] = 'To send an offer you want to subscribe to<br /><br /><a class="btn btn-warning" href="/youdo/personal/profile/">Subscribe</a>';
$MESS['WARNING_SUBSCRIPTION_GLOBAL'] = 'To send an offer you want to extend the profile of the artist<br /><br /><a class="btn btn-warning" href="/youdo/personal/profile/">go to subscriptions</a>';
?>