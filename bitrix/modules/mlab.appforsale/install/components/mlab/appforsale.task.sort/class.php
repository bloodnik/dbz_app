<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskSort extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		if (!is_array($arParams['FIELD_CODE']))
			$arParams['FIELD_CODE'] = array();
		foreach ($arParams['FIELD_CODE'] as $key => $val)
			if (!$val)
				unset($arParams['FIELD_CODE'][$key]);
			
		return $arParams;
	}

	public function executeComponent()
	{
		$arProps = array();
		foreach ($this->arParams['FIELD_CODE'] as $field)
		{
			if(substr($field, 0, 9) == "PROPERTY_")
			{
				$arProps[] = strtoupper(substr($field, 9));
			}
		}
		
		$dbProperty = CIBlockProperty::GetList(
			array(),
			array(
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
			)
		);
		while ($arProperty = $dbProperty->GetNext())
		{
			$this->arResult['PROPERTIES']['PROPERTY_'.$arProperty['CODE']] = $arProperty['NAME'];
		}
		
		$this->includeComponentTemplate();
	}
}
?>