<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$areaId = $this->GetEditAreaId('');
$itemIds = array(
		'ID' => $areaId,
		'SELECT' => $areaId.'_select'
);
?>
<div class="form-horizontal" style="padding: 15px; background: #f3f3f3">
	<select class="form-control" id="<?=$itemIds['SELECT']?>">
		<?if(strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
			<optgroup disabled hidden></optgroup>';
		<?endif;?>
		<option value="timestamp_xHigh">↑ <?=Loc::getMessage('TIMESTAMP_X')?></option>
		<option value="timestamp_xLow">↓ <?=Loc::getMessage('TIMESTAMP_X')?></option>
		<?foreach ($arParams['FIELD_CODE'] as $field):?>
		<option value="<?=$field?>High"<?=($field.'High' == $_SESSION['sort'] ? ' selected="selected"' : '')?>>↑ <?=$arResult['PROPERTIES'][$field] ?: $field?></option>
		<option value="<?=$field?>Low"<?=($field.'Low' == $_SESSION['sort'] ? ' selected="selected"' : '')?>>↓ <?=$arResult['PROPERTIES'][$field] ?: $field?></option>
		<?endforeach;?>
	</select>
</div>

<? 
$arJSParams = array(
	'pagePath' => GetPagePath(false, false),
	'VISUAL' => $itemIds
);
echo '<script type="text/javascript">new JCTaskSort('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>