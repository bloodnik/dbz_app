<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class PersonalProfile extends CBitrixComponent
{
	public function executeComponent()
	{
		global $USER;
		if (!$USER->IsAuthorized())
			CAppforsale::AuthForm();
		
		$arDefaultUrlTemplates404 = array(
			'form' => '#SECTION_CODE_PATH#/form/',
			'sections' => '#SECTION_CODE_PATH#/'
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'ID',
			'SECTION_ID',
			'SECTION_CODE'
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
			$arVariables = array();
			$engine = new CComponentEngine($this);
			if (\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$engine->addGreedyPart("#SECTION_CODE_PATH#");
				$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
			}
			
			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'sections';

		
			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);		

		}
		
		if (intval(COption::GetOptionInt('mlab.appforsale', 'RECUR_PRICE', 0) != 0))
		{
			$dbRecurring = CAppforsaleRecurring::GetList(
				array(), 
				array(
					'PRODUCT_ID' => '',
					'USER_ID' => $USER->GetID()
				)
			);
			if ($arRecurring = $dbRecurring->Fetch())
			{
				if (time() > MakeTimeStamp($arRecurring['NEXT_DATE'], "DD.MM.YYYY HH:MI:SS"))
					$componentPage = 'pay';
				
				if ($arRecurring['SUCCESS_PAYMENT'] != 'Y')
					$componentPage = 'pay';
				
				$this->arResult['RECUR'] = $arRecurring;
			}
			else
			{
				$componentPage = 'pay';
			}
		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>