<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.personal.profile.sections',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['form'],
		'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['sections'],
		'PARENT_SECTION' => $arResult['VARIABLES']['SECTION_ID'],
		'PARENT_SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE']
	),
	$component
);
?>