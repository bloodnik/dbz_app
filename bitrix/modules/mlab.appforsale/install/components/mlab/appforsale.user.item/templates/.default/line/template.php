<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="afs-user-row">
	<div class="afs-user-image user-<?=$item['ID']?>">
		<img class="afs-user-img" src="<?=($item['PERSONAL_PHOTO'] ? $item['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png')?>" />
	</div>
	<div class="afs-user-info">
		<div class="afs-user-name"><?=$item['FORMAT_NAME']?></div>
		<div style="font-size: 12px; line-height: 12px;">
		<? 
		$APPLICATION->IncludeComponent(
			'bitrix:system.field.edit',
			'rating',
			array(
				'arUserField' => $item['UF_RATING']
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);
		?>
		</div>
	</div>
</div>