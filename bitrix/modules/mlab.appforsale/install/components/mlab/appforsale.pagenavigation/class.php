<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class Pagenavigation extends \CBitrixComponent
{
	public function executeComponent()
	{
		if (is_object($this->arParams['NAV_RESULT']) && is_subclass_of($this->arParams['NAV_RESULT'], 'CAllDBResult'))
		{
			$dbresult =& $this->arParams['NAV_RESULT'];
			
			$this->arResult = array();
			
			$this->arResult['NavRecordCount'] = $dbresult->NavRecordCount;
			$this->arResult['NavPageCount'] = $dbresult->NavPageCount;
			$this->arResult['NavPageNomer'] = $dbresult->NavPageNomer;
			$this->arResult['NavNum'] = $dbresult->NavNum;
			
			$this->arResult['sUrlPath'] = GetPagePath(false, false);
			$this->arResult['NavQueryString'] = htmlspecialcharsbx(DeleteParam(array(
				'PAGEN_'.$dbresult->NavNum,
				'PHPSESSID',
				'bitrix_include_areas',
				'AJAX_ID'
			)));
			
			$this->includeComponentTemplate();
			return $this;
		}
	}
}
?>