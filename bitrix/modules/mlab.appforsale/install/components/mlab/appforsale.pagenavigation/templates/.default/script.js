/* mlab:appforsale.pagenavigation */
(function (window) {
	
	if (!!window.LazyLoad)
		return;
	
	window.LazyLoad = {
		blocks: [],
		blockStatus: {
		    error: -1,
		    undefined: 0,
		    inited: 1,
		    loaded: 2
		},
		register: function(e, t) {
			if (BX.type.isNotEmptyString(e))
			{
				this.blocks.push({
					id: e,
					node: null,
		            func: BX.type.isFunction(t) ? t : null,
		            status: this.blockStatus.undefined
				})
			}
		},
		show: function(e) {
			var t = null;
			var n = false;
			e = e !== false;
			for (var i = 0, r = this.blocks.length; i < r; i++)
			{
				t = this.blocks[i];
				if (t.status == this.blockStatus.undefined)
				{
					this.init(t)
				}
				if (t.status !== this.blockStatus.inited) 
				{
					continue
				}
				if (!t.node || !t.node.parentNode)
				{
					t.node = null;
					t.status = this.blockStatus.error;
					continue
				}
				if (this.isElementVisibleOnScreen(t.node))
				{
					t.func(t);
		            t.status = this.blockStatus.loaded
				}
			}
		},
		init: function(e) {
			e.status = this.blockStatus.error;
			var t = BX(e.id);
			if (t)
			{
				e.node = t;
				e.status = this.blockStatus.inited;
			}
		},
		isElementVisibleOnScreen: function(e) {
			var t = this.getElementCoords(e);
			var n = window.pageYOffset || document.documentElement.scrollTop;
			var i = n + document.documentElement.clientHeight + 1500;
			t.bottom = t.top + e.offsetHeight;
			var r = t.top > n && t.top < i;
			var o = t.bottom < i && t.bottom > n;
			return r || o
		},
		getElementCoords: function(e) {
			var t = e.getBoundingClientRect();
			return {
				originTop: t.top,
				originLeft: t.left,
				top: t.top + window.pageYOffset,
				left: t.left + window.pageXOffset
			}
		},
		onScroll: function() {
			LazyLoad.show()
		}
	};

	BX.ready(function() {
		document.addEventListener('scroll', function() {
			LazyLoad.onScroll();
		});
		LazyLoad.onScroll();
	});
	
})(window);