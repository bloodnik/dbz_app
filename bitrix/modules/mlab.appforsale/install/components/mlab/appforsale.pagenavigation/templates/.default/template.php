<? 
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if ($arResult['NavPageNomer'] < $arResult['NavPageCount']):
$temp = \Bitrix\Main\Security\Random::getString(32);
?>		
<div id="<?=$temp?>">
		<div class="col-md-12 afs-pagenavigation">
			Loading...
		</div>
	<script type="text/javascript">
		LazyLoad.register('<?=$temp?>', function() {
			BX.ajax.Setup({denyShowWait: true}, true);  
			BX.ajax.insertToNode(
				'<?=$arResult['sUrlPath']?>?<?=(!empty($arResult['NavQueryString']) ? $arResult['NavQueryString'].'&' : '')?>AJAX_ID=<?=$arParams['AJAX_ID']?>&PAGEN_<?=$arResult['NavNum']?>=<?=($arResult['NavPageNomer'] + 1)?>',
				'<?=$temp?>'
			);
		});
	</script>
</div>													
<?else:?>										
	<div class="col-md-12 afs-pagenavigation">
		<?=Loc::getMessage('RECORDS')?> - <?=$arResult['NavRecordCount']?>
	</div>
<?endif;?>