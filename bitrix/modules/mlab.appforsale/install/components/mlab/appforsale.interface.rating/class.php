<?
use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class InterfaceRating extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{

// 		$arParams['PID'] = isset($arParams['PID']) ? (int) $arParams['PID'] : 0;
		
// 		if ($arParams['PID'] > 0)
// 		{
// 			Loader::includeModule('iblock');
// 			$dbProperty = CIBlockProperty::GetByID($arParams['PID']);
// 			if($arProperty = $dbProperty->GetNext())
// 			{
// 				if ($arProperty['USER_TYPE'] == 'iblock')
// 				{
// 					$arParams['IBLOCK_ID'] = $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'];
// 				}
// 			}
// 		}

// 		$arParams['PARENT_SECTION'] = isset($arParams['PARENT_SECTION']) ? (int) $arParams['PARENT_SECTION'] : 0;
		
		return $arParams;
	}
	
	public function executeComponent()
	{	
// 		$dbElement = CIBlockSection::GetMixedList(
// 			array(
// 				'sort' => 'asc'
// 			),
// 			array(
// 				'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
// 				'ACTIVE' => 'Y',
// 				'SECTION_ID' => $this->arParams['PARENT_SECTION']
// 			),
// 			true,
// 			array(
// 				'ID',
// 				'NAME'
// 			)
// 		);
		
// 		while($arElement = $dbElement->GetNext())
// 		{
// 			$this->arResult['ITEMS'][] = $arElement;
// 		}
		
		$this->IncludeComponentTemplate();
	}
}