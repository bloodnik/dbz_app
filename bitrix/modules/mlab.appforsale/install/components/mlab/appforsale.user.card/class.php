<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\UserTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class UserCard extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['IBLOCK_TYPE'] = isset($arParams['IBLOCK_TYPE']) ? trim($arParams['IBLOCK_TYPE']) : '';
		$arParams['IBLOCK_ID'] = isset($arParams['IBLOCK_ID']) ? (int) $arParams['IBLOCK_ID'] : 0;
		
		return $arParams;
	}

	public function executeComponent()
	{
		if(!Loader::includeModule('iblock'))
		{
			return;
		}
		
		$dbElement = CIBlockElement::GetByID($this->arParams['ELEMENT_ID']);
		if ($arElement = $dbElement->GetNext())
		{
// 			$dbUser = UserTable::getList(array(
// 					'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'IS_ONLINE', 'LAST_ACTIVITY_DATE', 'UF_CITY', 'UF_RATING'),
// 					'filter' => array('ID' => $arElement['CREATED_BY']),
// 			));
			$dbUser = CUser::GetList($b, $o, array('ID' => $arElement['CREATED_BY']), array('SELECT' => array('IS_ONLINE', 'UF_RATING'), 'ONLINE_INTERVAL' => 900));
				
			$nameFormat = CSite::GetNameFormat(true);
			if ($arUser = $dbUser->fetch())
			{
				$this->arResult['ONLINE'][$arUser['ID']] = ($arUser['IS_ONLINE'] == 'Y');
				$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
				$arUser['FORMAT_NAME'] = \CUser::FormatName($nameFormat, $arUser);
				$this->arResult['USER'] = $arUser;
			}
		}
		$this->includeComponentTemplate();
		
		if (!empty($this->arResult['ONLINE']))
		{
			echo '<script>';
			foreach ($this->arResult['ONLINE'] as $uid=>$isOnline)
			{
				if ($isOnline)
				{
					?>
							var ob = document.getElementsByClassName('user-<?=$uid?>');
							for (var i = 0; i < ob.length; i++)
							{
			   					BX.addClass(ob[i], 'online');
							}
							<?
						}
					}
					echo '</script>';
				}
	}
}