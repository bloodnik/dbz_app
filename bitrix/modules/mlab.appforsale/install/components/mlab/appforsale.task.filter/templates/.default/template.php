<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$areaId = $this->GetEditAreaId('');
$itemIds = array(
	'ID' => $areaId,
	'CITY' => $areaId.'_city',
	'SELECT' => $areaId.'_select'
);
$sections = explode(',', $_GET['sections']);
?>
<div class="afs-task-filter form-horizontal">
	<?
	$arFilter = array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'ACTIVE' => 'Y',
		'PROPERTY_STATUS_ID' => array(false, 'N')
	);
	if ($USER->IsAuthorized())
	{
		$arProperty = CIBlockProperty::GetPropertyArray('CITY_ID', $arParams['IBLOCK_ID']);
		if ($arProperty)
		{
			$UF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', $USER->GetID());
			$city_id = intval($UF['UF_CITY']['VALUE']);
			if ($city_id > 0)
			{
				if (!empty($_SESSION['city_id']))
					$city_id = $_SESSION['city_id'];
				
				if (!empty($_GET['city_id']))
					$city_id = $_GET['city_id'];
				
				$settings = unserialize($arProperty['USER_TYPE_SETTINGS']);
				
				$SORT_ORDER_MASK = '/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i';
				
				if (empty($arParams['SORT_FIELD']))
				{
					$arParams['SORT_FIELD'] = 'sort';
				}
					
				if (!preg_match($SORT_ORDER_MASK, $arParams['SORT_ORDER']))
				{
					$arParams['SORT_ORDER'] = 'asc';
				}
					
				if (empty($arParams['SORT_FIELD2']))
				{
					$arParams['SORT_FIELD2'] = 'id';
				}
					
				if (!preg_match($SORT_ORDER_MASK, $arParams['SORT_ORDER2']))
				{
					$arParams['SORT_ORDER2'] = 'desc';
				}
				
				
				$dbLocation = CIBlockElement::GetList(array($arParams['SORT_FIELD'] => $arParams['SORT_ORDER'], $arParams['SORT_FIELD2'] => $arParams['SORT_ORDER2']), array('IBLOCK_ID' => $settings['IBLOCK_ID'], 'ACTIVE' => 'Y', 'SECTION_ACTIVE' => 'Y', 'SECTION_GLOBAL_ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
				?>
				<select class="form-control" id="<?=$itemIds['CITY']?>">
					<?if(strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
						<optgroup disabled hidden></optgroup>';
					<?endif;?>
					<?while($arLocation = $dbLocation->GetNext()):?>
						<option value="<?=$arLocation['ID']?>"<?=($city_id == $arLocation['ID'] ? ' selected="selected"' : '')?>><?=$arLocation['NAME']?></option>
					<?endwhile;?>
				</select>
				<br />
				<? 
				$arFilter['PROPERTY_CITY_ID'] = $city_id;
			}
		}
	}

	$arSection = Mlab\Appforsale\Cnt::get($arFilter);
	?>
	<select class="form-control" id="<?=$itemIds['SELECT']?>" multiple="multiple">
		<?if(strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')):?>
			<optgroup disabled hidden></optgroup>';
		<?endif;?>
		<?foreach ($arResult['ITEMS'] as $arItem):?>
			<option value="<?=$arItem['ID']?>"<?=(in_array($arItem['ID'], $sections) ? ' selected="selected"' : '')?>><?for($i = 1; $i < $arItem['DEPTH_LEVEL']; $i++) { echo '-'; }?><?=$arItem['DEPTH_LEVEL'] > 1 ? ' '.$arItem['NAME'] : $arItem['NAME']?><?=$arParams['CNT'] == 'Y' ? ' ('.intval($arSection[$arItem['ID']]). ')' : ''?></option>
		<?endforeach;?>
	</select>
</div>
<? 
$arJSParams = array(
	'pagePath' => GetPagePath(false, false),
	'VISUAL' => $itemIds
);
echo '<script type="text/javascript">new JCTaskFilter('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>