<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('INVITE_NAME'),
	'SORT' => 20,
	'DESCRIPTION' => Loc::getMessage('INVITE_DESCRIPTION'),
	'COMPLEX' => 'Y',
	'PATH' => array(
		'ID' => 'appforsale',
		'NAME' => Loc::getMessage('INVITE_PATH_NAME'),
		'SORT' => 1,
		'CHILD' => array(
			'ID' => 'invite',
			'NAME' => Loc::getMessage('INVITE_PATH_CHILD_NAME'),
			'SORT' => 10
		)
	)
);
?>