<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'INVITE_LEN' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('INVITE_LEN'),
			'TYPE' => 'NUMBER',
			'DEFAULT' => 8
		),
		'MESS_LEAD' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MESS_LEAD'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('MESS_LEAD_DEFAULT')
		),
		'MESS_DESCRIPTION' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MESS_DESCRIPTION'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('MESS_DESCRIPTION_DEFAULT')
		),
		'MESS_INVALID_PROMOCODE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MESS_INVALID_PROMOCODE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('MESS_INVALID_PROMOCODE_DEFAULT')
		),
		'MESS_PROMODOCE_ACTIVATE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MESS_PROMODOCE_ACTIVATE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('MESS_PROMODOCE_ACTIVATE_DEFAULT')
		),
		'MESS_ONLY_ONCE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MESS_ONLY_ONCE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('MESS_ONLY_ONCE_DEFAULT')
		),
		'SEF_MODE' => array(
			'out' => array(
				'NAME' => Loc::getMessage('SEF_MODE_OUT'),
				'DEFAULT' => '',
				'VARIABLES' => array()
			),
			'in' => array(
				'NAME' => Loc::getMessage('SEF_MODE_IN'),
				'DEFAULT' => '#CODE#',
				'VARIABLES' => array(
					'CODE'
				)
			)
		)
	)
);
?>