<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class Invite extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		if (!$USER->IsAuthorized())
			CAppforsale::AuthForm();
		
		$arDefaultUrlTemplates404 = array(
			'in' => '#CODE#/',
			'out' => ''
		);
		$arDefaultVariableAliases404 = array();
		
		$arComponentVariables = array(
			'CODE'
		);
		
		if($this->arParams['SEF_MODE'] == 'Y')
		{
			$arVariables = array();
			$engine = new CComponentEngine($this);
			$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams['SEF_URL_TEMPLATES']);
			$componentPage = $engine->guessComponentPath(
				$this->arParams['SEF_FOLDER'],
				$arUrlTemplates,
				$arVariables
			);
							
			if(!$componentPage)
				$componentPage = 'out';

			CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
			$this->arResult = array(
				'FOLDER' => $this->arParams['SEF_FOLDER'],
				'URL_TEMPLATES' => $arUrlTemplates,
				'VARIABLES' => $arVariables,
				'ALIASES' => $arVariableAliases
			);	
		}
		
		$this->IncludeComponentTemplate($componentPage);
	}
}
?>