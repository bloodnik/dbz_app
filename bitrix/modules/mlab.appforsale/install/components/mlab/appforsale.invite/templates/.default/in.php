<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.invite.in',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CODE' => $arResult['VARIABLES']['CODE'],		
		'MESS_INVALID_PROMOCODE' => $arParams['MESS_INVALID_PROMOCODE'],
		'MESS_PROMODOCE_ACTIVATE' => $arParams['MESS_PROMODOCE_ACTIVATE'],
		'MESS_ONLY_ONCE' => $arParams['MESS_ONLY_ONCE']
	),
	$component
);
?>		