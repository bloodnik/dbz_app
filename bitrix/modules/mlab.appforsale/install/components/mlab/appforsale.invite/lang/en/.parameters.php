<?
$MESS['IBLOCK_TYPE'] = 'The type of InfoBlock';
$MESS['IBLOCK'] = 'Infoblok';
$MESS['INVITE_LEN'] = 'The length of the code';
$MESS['MESS_LEAD'] = 'The text on the website';
$MESS['MESS_LEAD_DEFAULT'] = 'Give each 150 P to the account when you first sign up';
$MESS['MESS_DESCRIPTION'] = 'The text in the message';
$MESS['MESS_DESCRIPTION_DEFAULT'] = 'I use AppForSale. Click the link to receive a discount of 150 RUB for the first registration';

$MESS['MESS_INVALID_PROMOCODE'] = 'The text in the wrong code';
$MESS['MESS_INVALID_PROMOCODE_DEFAULT'] = 'Invalid promo code';
$MESS['MESS_PROMODOCE_ACTIVATE'] = 'The text with the right code';
$MESS['MESS_PROMODOCE_ACTIVATE_DEFAULT'] = 'The promotional code';
$MESS['MESS_ONLY_ONCE'] = 'Text when you re-enter the promotion code';
$MESS['MESS_ONLY_ONCE_DEFAULT'] = 'You are already registered';

$MESS['SEF_MODE_OUT'] = 'The results of the promotion code';
$MESS['SEF_MODE_IN'] = 'Enter the promo code';
?>