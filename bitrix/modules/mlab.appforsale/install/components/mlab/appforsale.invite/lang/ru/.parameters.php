<?
$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['INVITE_LEN'] = 'Длина кода';
$MESS['MESS_LEAD'] = 'Текст на сайте';
$MESS['MESS_LEAD_DEFAULT'] = 'Подари другу 150 Р на счет при первой регистрации';
$MESS['MESS_DESCRIPTION'] = 'Текст в сообщении';
$MESS['MESS_DESCRIPTION_DEFAULT'] = 'Я пользуюсь AppForSale. Кликни по ссылке, чтобы получить скидку 150 руб. при первой регистрации';

$MESS['MESS_INVALID_PROMOCODE'] = 'Текст при неверном промокоде';
$MESS['MESS_INVALID_PROMOCODE_DEFAULT'] = 'Неверный промокод';
$MESS['MESS_PROMODOCE_ACTIVATE'] = 'Текст при верном промокоде';
$MESS['MESS_PROMODOCE_ACTIVATE_DEFAULT'] = 'Промокод активирован';
$MESS['MESS_ONLY_ONCE'] = 'Текст при повторном вводе промокода';
$MESS['MESS_ONLY_ONCE_DEFAULT'] = 'Вы уже зарегистрированы';

$MESS['SEF_MODE_OUT'] = 'Выдача промокода';
$MESS['SEF_MODE_IN'] = 'Ввод промокода';
?>