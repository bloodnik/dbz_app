<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Web\PostDecodeFilter,
	Bitrix\Main\Web\Json;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class Form extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['IBLOCK_ID'] = isset($arParams['IBLOCK_ID']) ? (int) $arParams['IBLOCK_ID'] : 0;	
		$arParams['ID'] = isset($arParams['ID']) ? (int) $arParams['ID'] : 0;	
		
		$arParams['MESS_PROPERTY_VALUE_NA'] = $arParams['MESS_PROPERTY_VALUE_NA'] ?: Loc::getMessage('PROPERTY_VALUE_NA');
 		$arParams['MESS_BTN_ADD'] = $arParams['MESS_BTN_ADD'] ?: Loc::getMessage('BTN_ADD');
 		$arParams['MESS_BTN_UPDATE'] = $arParams['MESS_BTN_UPDATE'] ?: Loc::getMessage('BTN_UPDATE');
 		$arParams['MESS_BTN_DELETE'] = $arParams['MESS_BTN_DELETE'] ?: Loc::getMessage('BTN_DELETE');
		
 		if (!is_array($arParams['PROPERTY_CODE']))
 			$arParams['PROPERTY_CODE'] = array();
 		foreach ($arParams['PROPERTY_CODE'] as $key => $val)
 			if (!$val)
 				unset($arParams['PROPERTY_CODE'][$key]);

 		$arParams['PROPERTY_CODE'] = array_unique($arParams['PROPERTY_CODE']);
 			
 			
 		if (!is_array($arParams['PROPERTY_CODE_REQUIRED']))
 			$arParams['PROPERTY_CODE_REQUIRED'] = array();
 		foreach ($arParams['PROPERTY_CODE_REQUIRED'] as $key => $val)
 			if (!$val)
 				unset($arParams['PROPERTY_CODE_REQUIRED'][$key]);
 			
 		$arParams['PROPERTY_CODE_REQUIRED'] = array_unique($arParams['PROPERTY_CODE_REQUIRED']);

 		if (!is_array($arParams['UPDATE_VALUES']))
 			$arParams['UPDATE_VALUES'] = array();
 			
 		if (!is_array($arParams['UPDATE_PROPERTY_VALUES']))
 			$arParams['UPDATE_PROPERTY_VALUES'] = array();

 		
 		$arParams['REDIRECT_URL'] = isset($arParams['REDIRECT_URL']) ? $arParams['REDIRECT_URL'] : '';
 		
		return $arParams;
	}
	
	public function executeComponent()
	{		
		global $APPLICATION, $USER;
		
		Loader::includeModule('iblock');
		
		$this->arParams['SECTION_ID'] = CIBlockFindTools::GetSectionID(
			$this->arParams['SECTION_ID'],
			$this->arParams['SECTION_CODE'],
			array(
				'GLOBAL_ACTIVE' => 'Y',
				'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
			)
		);
		
		if (ToLower(CIBlock::GetArrayByID($this->arParams['IBLOCK_ID'], 'CODE')) == 'profile')
		{
			$this->arResult['bProfile'] = true;
			$dbIBlock = CIBlock::GetList(array(), array('CODE' => 'task'));
			if ($arIBlock = $dbIBlock->Fetch())
			{
				$this->arParams['TASK_IBLOCK_ID'] = intval($arIBlock['ID']);
				
				$dbSection = CIBlockSection::GetList(
					array(),
					array(
						'IBLOCK_ID' => $arIBlock['ID'],
						'ACTIVE' => 'Y',
						'GLOBAL_ACTIVE' => 'Y',
						'UF_PROFILE' => $this->arParams['SECTION_ID']
					),
					false,
					array(
						'ID'
					)
				);
				$propLink = array();
				while ($arSection = $dbSection->Fetch())
				{
					$dbNavChain = CIBlockSection::GetNavChain($arIBlock['ID'], $arSection['ID']);
					while ($arNavChain = $dbNavChain->Fetch())
					{
						if ($arNavChain['ACTIVE'] == 'Y' && $arNavChain['GLOBAL_ACTIVE'] == 'Y')
						{
							$arUF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$arIBlock['ID'].'_SECTION', $arNavChain['ID']);
							// UF_FORM_PROPERTY
							foreach ($arUF['UF_SUBSCRIBE']['VALUE'] as $v)
							{
								if (!in_array($v, $this->arParams['PROPERTY_CODE']))
									$this->arParams['PROPERTY_CODE'][] = $v;
							}
						}
					}
				}
				
				// =====
			}
		}
		
		$dbSection = CIBlockSection::GetNavChain($this->arParams['IBLOCK_ID'], $this->arParams['SECTION_ID']);
		while ($arSection = $dbSection->Fetch())
		{
			if ($arSection['ACTIVE'] == 'Y' && $arSection['GLOBAL_ACTIVE'] == 'Y')
			{
				$arUF = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('IBLOCK_'.$this->arParams['IBLOCK_ID'].'_SECTION', $arSection['ID']);
				foreach ($arUF['UF_FORM_PROPERTY']['VALUE'] as $v)
				{
					if (!in_array($v, $this->arParams['PROPERTY_CODE']))
						$this->arParams['PROPERTY_CODE'][] = $v;
				}
				foreach ($arUF['UF_FORM_PROPERTY_REQ']['VALUE'] as $v)
				{
					if (!in_array($v, $this->arParams['PROPERTY_CODE_REQUIRED']))
						$this->arParams['PROPERTY_CODE_REQUIRED'][] = $v;
				}
			}
		}
				
		$this->arResult['PROPERTY_REQUIRED'] = is_array($this->arParams['PROPERTY_CODE_REQUIRED']) ? $this->arParams['PROPERTY_CODE_REQUIRED'] : array();
		
		if ($this->arParams['ID'] > 0)
		{	
			$this->arResult['ELEMENT'] = array();
			$rsIBlockElements = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ID' => $this->arParams['ID']));
			if ($arElement = $rsIBlockElements->Fetch())
			{
				$this->arResult['ELEMENT'] = $arElement;
			}

			// load element properties
			$rsElementProperties = CIBlockElement::GetProperty($this->arParams['IBLOCK_ID'], $arElement['ID'], $by='sort', $order='asc');
			$this->arResult['ELEMENT_PROPERTIES'] = array();
			while ($arElementProperty = $rsElementProperties->Fetch())
			{
				if(!array_key_exists($arElementProperty['ID'], $this->arResult['ELEMENT_PROPERTIES']))
					$this->arResult['ELEMENT_PROPERTIES'][$arElementProperty['ID']] = array();
		
				if(is_array($arElementProperty['VALUE']))
				{
					$htmlvalue = array();
					foreach($arElementProperty['VALUE'] as $k => $v)
					{
						if(is_array($v))
						{
							$htmlvalue[$k] = array();
							foreach($v as $k1 => $v1)
								$htmlvalue[$k][$k1] = htmlspecialcharsbx($v1);
						}
						else
						{
							$htmlvalue[$k] = htmlspecialcharsbx($v);
						}
					}
				}
				else
				{
					$htmlvalue = htmlspecialcharsbx($arElementProperty['VALUE']);
				}
		
				$this->arResult['ELEMENT_PROPERTIES'][$arElementProperty['ID']][] = array(
						'ID' => htmlspecialcharsbx($arElementProperty['ID']),
						'VALUE' => $htmlvalue,
						'~VALUE' => $arElementProperty['VALUE'],
						'VALUE_ID' => htmlspecialcharsbx($arElementProperty['PROPERTY_VALUE_ID']),
						'VALUE_ENUM' => htmlspecialcharsbx($arElementProperty['VALUE_ENUM'])
				);
			}
			
			if ($this->arResult['bProfile'])
			{
				$dbTaskProperties = unserialize($this->arResult['ELEMENT']['PREVIEW_TEXT']);
				foreach ($dbTaskProperties as $ID=>$VALUE)
				{
					if(!array_key_exists($ID, $this->arResult['ELEMENT_PROPERTIES']))
						$this->arResult['ELEMENT_PROPERTIES'][$ID] = array();
					
					if(is_array($VALUE))
					{
						$htmlvalue = array();
						foreach($VALUE as $k => $v)
						{
							$this->arResult['ELEMENT_PROPERTIES'][$ID][] = array(
								'ID' => htmlspecialcharsbx($ID),
								'VALUE' => $v,
								'~VALUE' => $value
							);
						}
					}
					else
					{
						$this->arResult['ELEMENT_PROPERTIES'][$ID][] = array(
							'ID' => htmlspecialcharsbx($ID),
							'VALUE' => $VALUE,
							'~VALUE' => $value
						);
					}
				}
 			}
 			
//  			echo '<pre>';
//  			print_r($this->arResult['ELEMENT_PROPERTIES']);
//  			echo '</pre>';
		
// 			// process element property files
// 			$arResult["ELEMENT_FILES"] = array();
// 			foreach ($arResult["PROPERTY_LIST"] as $propertyID)
// 			{
// 				$arProperty = $arResult["PROPERTY_LIST_FULL"][$propertyID];
// 				if ($arProperty["PROPERTY_TYPE"] == "F")
// 				{
// 					$arValues = array();
// 					if (intval($propertyID) > 0)
// 					{
// 						foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arProperty)
// 						{
// 							$arValues[] = $arProperty["VALUE"];
// 						}
// 					}
// 					else
// 					{
// 						$arValues[] = $arResult["ELEMENT"][$propertyID];
// 					}
		
// 					foreach ($arValues as $value)
// 					{
// 						if ($arFile = CFile::GetFileArray($value))
// 						{
// 							$arFile["IS_IMAGE"] = CFile::IsImage($arFile["FILE_NAME"], $arFile["CONTENT_TYPE"]);
// 							$arResult["ELEMENT_FILES"][$value] = $arFile;
// 						}
// 					}
// 				}
// 			}
		
			//$bShowForm = true;
		}
// 		else
// 		{
// 			$bShowForm = true;
// 		}
		
		
	
// 		echo '<pre>';
// 		print_r($this->arResult['ELEMENT_PROPERTIES']);
// 		echo '</pre>';
		
		
		
		
		
	
		
		
		
		$bAllowAccess = $USER->GetID() > 0;
		$this->arResult['ERRORS'] = array();
		
		$this->arResult['PROPERTY_LIST'] = array();
		$this->arResult['PROPERTY_LIST_FULL'] = array();
		
		$arFilter = array(
			'ACTIVE' => 'Y',
			'@IBLOCK_ID' => array(
				$this->arParams['IBLOCK_ID']
			)
		);
		
		if ($this->arParams['TASK_IBLOCK_ID'] > 0)
			$arFilter['@IBLOCK_ID'][] = $this->arParams['TASK_IBLOCK_ID'];

		$dbProperty = CIBlockProperty::GetList(
			array(
				'sort' => 'asc',
				'name' => 'asc'
			),
			$arFilter
		);
		while ($arProperty = $dbProperty->GetNext())
		{
			if ($this->arResult['bProfile'] && $arProperty['IBLOCK_ID'] == $this->arParams['TASK_IBLOCK_ID'])
			{
				if ($arProperty['PROPERTY_TYPE'] != 'E' && $arProperty['PROPERTY_TYPE'] != 'G' && $arProperty['PROPERTY_TYPE'] != 'L' && $arProperty['USER_TYPE'] != 'iblock')
					continue;
			} 
			
			if ($arProperty['PROPERTY_TYPE'] == 'L')
			{
				$dbPropertyEnum = CIBlockProperty::GetPropertyEnum($arProperty['ID']);
				$arProperty['ENUM'] = array();
				while ($arPropertyEnum = $dbPropertyEnum->GetNext())
				{
					$arProperty['ENUM'][$arPropertyEnum['ID']] = $arPropertyEnum;
				}
			}
				
			if ($arProperty['PROPERTY_TYPE'] == 'E')
			{
				$dbElement = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), array('IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
				$arProperty['LIST'] = array();
				while($arElement = $dbElement->GetNext())
				{
					$arProperty['LIST'][$arElement['ID']] = $arElement;
				}
			}
				
			if ($arProperty['PROPERTY_TYPE'] == 'G')
			{
				$dbElement = CIBlockSection::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), array('IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'), false, array('ID', 'NAME'));
				$arProperty['LIST'] = array();
				while($arElement = $dbElement->GetNext())
				{
					$arProperty['LIST'][$arElement['ID']] = $arElement;
				}
			}
			
			if ($arProperty['USER_TYPE'] == 'iblock' && $this->arParams['IBLOCK_ID'] != $arProperty['IBLOCK_ID'])
			{
				$arProperty['PROPERTY_TYPE'] = 'E';


				$dbElement = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), array('IBLOCK_ID' => $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'], 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'PROPERTY_RATIO'));
				$arProperty['LIST'] = array();
				while($arElement = $dbElement->GetNext())
				{
					$arProperty['LIST'][$arElement['ID']] = $arElement;
				}
			}
			elseif(strlen($arProperty['USER_TYPE']) > 0)
			{
				$arUserType = CIBlockProperty::GetUserType($arProperty['USER_TYPE']);
				if(array_key_exists('GetPublicEditHTML', $arUserType))
					$arProperty['GetPublicEditHTML'] = $arUserType['GetPublicEditHTML'];
				else
					$arProperty['GetPublicEditHTML'] = false;
			}
			else
			{
				$arProperty['GetPublicEditHTML'] = false;
			}
					
			if (in_array($arProperty['ID'], $this->arParams['PROPERTY_CODE']))
				$this->arResult['PROPERTY_LIST'][] = $arProperty['ID'];
					
			if ($this->arResult['bProfile'] && $arProperty['IBLOCK_ID'] == $this->arParams['TASK_IBLOCK_ID'])
			{
				$arProperty['MULTIPLE'] = 'Y';
				$arProperty['strHTMLControlName'] = 'arrFilter';
			}
			else
			{
				$arProperty['strHTMLControlName'] = 'PROPERTY';
			}
				
			$this->arResult['PROPERTY_LIST_FULL'][$arProperty['ID']] = $arProperty;
		}
			
		if ($bAllowAccess)
		{
			if (check_bitrix_sessid() && $this->request->isAjaxRequest())
			{
				$APPLICATION->RestartBuffer();
				$this->request->addFilter(new PostDecodeFilter);
		
				$arrFilter = $this->request->getPost('arrFilter');
				
				$arProperties = $this->request->getPost('PROPERTY');
				$arUpdateValues = $this->arParams['UPDATE_VALUES'];
				$arUpdatePropertyValues = $this->arParams['UPDATE_PROPERTY_VALUES'];
				
				foreach ($this->arParams['PROPERTY_CODE'] as $propertyID)
				{					
					$arPropertyValue = $arProperties[$propertyID];
					// check if property is a real property, or element field
					if (intval($propertyID) > 0)
					{
						// for non-file properties
						if ($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] != "F")
						{
							// for multiple properties
							if ($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
							{
								$arUpdatePropertyValues[$propertyID] = array();
					
								if (!is_array($arPropertyValue))
								{
									$arUpdatePropertyValues[$propertyID][] = $arPropertyValue;
								}
								else
								{
									foreach ($arPropertyValue as $key => $value)
									{
										if (
												$this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "L" && intval($value) > 0
												||
												$this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] != "L" && !empty($value)
										)
										{
											$arUpdatePropertyValues[$propertyID][] = $value;
										}
									}
								}
							}
							// for single properties
							else
							{								
								if ($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] != 'L' && 
										$this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] != 'E')
									$arUpdatePropertyValues[$propertyID] = $arPropertyValue[0];
								else
									$arUpdatePropertyValues[$propertyID] = $arPropertyValue;
							}
						}
						// for file properties
						else
						{
							$arUpdatePropertyValues[$propertyID] = array();
							foreach ($arPropertyValue as $key => $value)
							{
// 								$arFile = $_FILES["PROPERTY_FILE_".$propertyID."_".$key];
// 								$arFile["del"] = $_REQUEST["DELETE_FILE"][$propertyID][$key] == "Y" ? "Y" : "";
								$arUpdatePropertyValues[$propertyID][$key] = $value;
					
// 								if(($arParams["MAX_FILE_SIZE"] > 0) && ($arFile["size"] > $arParams["MAX_FILE_SIZE"]))
// 									$arResult["ERRORS"][] = GetMessage("IBLOCK_ERROR_FILE_TOO_LARGE");
							}
							
							foreach ($this->arResult['ELEMENT_PROPERTIES'][$propertyID] as $key => $value)
							{
								$arUpdatePropertyValues[$propertyID][$value['VALUE_ID']] = array('del' => 'Y');
							}
					
							if (empty($arUpdatePropertyValues[$propertyID]))
								unset($arUpdatePropertyValues[$propertyID]);
							
				
						}
					}
// 					else
// 					{
// 						// for "virtual" properties
// 						if ($propertyID == "IBLOCK_SECTION")
// 						{
// 							if (!is_array($arProperties[$propertyID]))
// 								$arProperties[$propertyID] = array($arProperties[$propertyID]);
// 							$arUpdateValues[$propertyID] = $arProperties[$propertyID];
					
// 							if ($arParams["LEVEL_LAST"] == "Y" && is_array($arUpdateValues[$propertyID]))
// 							{
// 								foreach ($arUpdateValues[$propertyID] as $section_id)
// 								{
// 									$rsChildren = CIBlockSection::GetList(
// 											array("SORT" => "ASC"),
// 											array(
// 													"IBLOCK_ID" => $arParams["IBLOCK_ID"],
// 													"SECTION_ID" => $section_id,
// 											),
// 											false,
// 											array("ID")
// 									);
// 									if ($rsChildren->SelectedRowsCount() > 0)
// 									{
// 										$arResult["ERRORS"][] = GetMessage("IBLOCK_ADD_LEVEL_LAST_ERROR");
// 										break;
// 									}
// 								}
// 							}
					
// 							if ($arParams["MAX_LEVELS"] > 0 && count($arUpdateValues[$propertyID]) > $arParams["MAX_LEVELS"])
// 							{
// 								$arResult["ERRORS"][] = str_replace("#MAX_LEVELS#", $arParams["MAX_LEVELS"], GetMessage("IBLOCK_ADD_MAX_LEVELS_EXCEEDED"));
// 							}
// 						}
// 						else
// 						{
// 							if($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "F")
// 							{
// 								$arFile = $_FILES["PROPERTY_FILE_".$propertyID."_0"];
// 								$arFile["del"] = $_REQUEST["DELETE_FILE"][$propertyID][0] == "Y" ? "Y" : "";
// 								$arUpdateValues[$propertyID] = $arFile;
// 								if ($arParams["MAX_FILE_SIZE"] > 0 && $arFile["size"] > $arParams["MAX_FILE_SIZE"])
// 									$arResult["ERRORS"][] = GetMessage("IBLOCK_ERROR_FILE_TOO_LARGE");
// 							}
// 							elseif($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "HTML")
// 							{
// 								if($propertyID == "DETAIL_TEXT")
// 									$arUpdateValues["DETAIL_TEXT_TYPE"] = "html";
// 								if($propertyID == "PREVIEW_TEXT")
// 									$arUpdateValues["PREVIEW_TEXT_TYPE"] = "html";
// 								$arUpdateValues[$propertyID] = $arProperties[$propertyID][0];
// 							}
// 							else
// 							{
// 								if($propertyID == "DETAIL_TEXT")
// 									$arUpdateValues["DETAIL_TEXT_TYPE"] = "text";
// 								if($propertyID == "PREVIEW_TEXT")
// 									$arUpdateValues["PREVIEW_TEXT_TYPE"] = "text";
// 								$arUpdateValues[$propertyID] = $arProperties[$propertyID][0];
// 							}
// 						}
// 					}
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}
				
				

				
				foreach (array_intersect($this->arParams['PROPERTY_CODE'], $this->arParams['PROPERTY_CODE_REQUIRED']) as $propertyID)
				{
					$bError = false;
					
					if (intval($propertyID) > 0 && !array_key_exists($propertyID, $this->arResult["PROPERTY_LIST_FULL"]))
							continue;
					
					$propertyValue = intval($propertyID) > 0 ? $arUpdatePropertyValues[$propertyID] : $arUpdateValues[$propertyID];

					if($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] != "")
						$arUserType = CIBlockProperty::GetUserType($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"]);
					else
						$arUserType = array();
			
			


			// ===============
			//Files check
			if ($this->arResult['PROPERTY_LIST_FULL'][$propertyID]['PROPERTY_TYPE'] == 'F')
			{
 				//New element
				if ($arParams['ID'] <= 0)
				{
 					$bError = true;
					if(is_array($propertyValue))
					{
// 						if(array_key_exists("tmp_name", $propertyValue) && array_key_exists("size", $propertyValue))
// 						{
// 							if($propertyValue['size'] > 0)
// 							{
// 								$bError = false;
// 							}
// 						}
// 						else
// 						{
							foreach ($propertyValue as $arFile)
							{
								if (intval($arFile) > 0)
								{
									$bError = false;
									break;
								}
							}
// 						}
					}
				}
 				//Element field
				//elseif (intval($propertyID) <= 0)
// 				{
// 					if ($propertyValue['size'] <= 0)
// 					{
// 						if (intval($arElement[$propertyID]) <= 0 || $propertyValue['del'] == 'Y')
// 							$bError = true;
// 					}
// 				}
 				//Element property
				else
 				{
//  					CIBlockElement::SetPropertyValuesEx(
//  						$arParams['ID'],
//  						$arElement['IBLOCK_ID'],
//  						array($propertyID => false)
//  					);
 					
// 					$dbProperty = CIBlockElement::GetProperty(
// 						$arElement['IBLOCK_ID'],
// 						$arParams['ID'],
// 						'sort', 'asc',
// 						array('ID' => $propertyID)
// 					);

				
// // // 					$bCount = 0;
// 					$value = array();
// 			while ($arProperty = $dbProperty->Fetch())
// 						$value[$arProperty['PROPERTY_VALUE_ID']] = array('del' => 'Y');

// 			if (!empty($value))
//  				CIBlockElement::SetPropertyValuesEx($arParams['ID'], $arElement['IBLOCK_ID'], array('"'.$propertyID.'"' => $value));
// // 					foreach ($propertyValue as $arFile)
// 					{
// 						if ($arFile['size'] > 0)
// 						{
// 							$bCount++;
// 							break;
// 						}
// 						elseif ($arFile['del'] == 'Y')
// 						{
// 							$bCount--;
// 						}
// 					}

// 					$bError = $bCount <= 0;
 				}
			}
			elseif(array_key_exists("GetLength", $arUserType))
			{
				$len = 0;
				if(is_array($propertyValue) && !array_key_exists("VALUE", $propertyValue))
				{
					foreach($propertyValue as $value)
					{
						if(is_array($value) && !array_key_exists("VALUE", $value))
							foreach($value as $val)
								$len += call_user_func_array($arUserType["GetLength"], array($this->arResult["PROPERTY_LIST_FULL"][$propertyID], array("VALUE" => $val)));
						elseif(is_array($value) && array_key_exists("VALUE", $value))
							$len += call_user_func_array($arUserType["GetLength"], array($this->arResult["PROPERTY_LIST_FULL"][$propertyID], $value));
						else
							$len += call_user_func_array($arUserType["GetLength"], array($this->arResult["PROPERTY_LIST_FULL"][$propertyID], array("VALUE" => $value)));
					}
				}
				elseif(is_array($propertyValue) && array_key_exists("VALUE", $propertyValue))
				{
					$len += call_user_func_array($arUserType["GetLength"], array($this->arResult["PROPERTY_LIST_FULL"][$propertyID], $propertyValue));
				}
				else
				{
					$len += call_user_func_array($arUserType["GetLength"], array($this->arResult["PROPERTY_LIST_FULL"][$propertyID], array("VALUE" => $propertyValue)));
				}

				if($len <= 0)
					$bError = true;

			}
			//multiple property
			elseif ($this->arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" || $this->arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "L")
			{
				if(is_array($propertyValue))
				{
					$bError = true;
					foreach($propertyValue as $value)
					{
						if(strlen($value) > 0)
						{
							$bError = false;
							break;
						}
					}
				}
				elseif(strlen($propertyValue) <= 0)
				{
					$bError = true;
				}
			}
			//single
			elseif (is_array($propertyValue) && array_key_exists("VALUE", $propertyValue))
			{
		
				if(strlen($propertyValue["VALUE"]) <= 0)
					$bError = true;
			}
			elseif (!is_array($propertyValue))
			{	

				if(strlen($propertyValue) <= 0)
					$bError = true;
			}
		
			
			

			// ===============
					
					if ($bError)
					{
						$this->arResult['ERRORS'][] = str_replace('#PROPERTY_NAME#', intval($propertyID) > 0 ? $this->arResult['PROPERTY_LIST_FULL'][$propertyID]['NAME'] : (!empty($this->arParams["CUSTOM_TITLE_".$propertyID]) ? $this->arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)), Loc::getMessage("IBLOCK_ADD_ERROR_REQUIRED"));
					}
				}

	
	
				if (empty($this->arResult['ERRORS']))
				{
					$arUpdateValues['NAME'] = 'unknown';

					$arField = \CIBlock::GetFields($this->arParams['IBLOCK_ID']);
					if (!empty($arField['NAME']['DEFAULT_VALUE']))
							$arUpdateValues['NAME'] = $arField['NAME']['DEFAULT_VALUE'];

					if (!empty($arrFilter))
					{
						$arUpdateValues['PREVIEW_TEXT'] = serialize($arrFilter);
 						$strFunction = 'function($arProp){return((1==1)';
						foreach ($arrFilter as $pid=>$val)
						{
 							$strFunction .= '&&(in_array($arProp[\''.$pid.'\'],array(\''.implode('\',\'', $val).'\')))';
						}
 						$strFunction .= ');}';
 						$arUpdateValues['DETAIL_TEXT'] = $strFunction;
					}
					
					$arUpdateValues['MODIFIED_BY'] = $USER->GetID();
					$arUpdateValues['PROPERTY_VALUES'] = $arUpdatePropertyValues;
					
					$obElement = new CIBlockElement();
					if ($this->arParams['ID'] > 0)
					{
						$dbProp = CIBlockElement::GetProperty($this->arParams['IBLOCK_ID'], $this->arParams['ID'], 'sort', 'asc', array('ACTIVE' => 'Y'));
						while ($arProp = $dbProp->Fetch())
						{
							if (!array_key_exists($arProp['ID'], $arUpdateValues['PROPERTY_VALUES']) && $arProp['PROPERTY_TYPE'] != 'F')
							{
								if ($arProp['MULTIPLE'] == 'Y')
								{
									if(!array_key_exists($arProp['ID'], $arUpdateValues['PROPERTY_VALUES']))
										$arUpdateValues['PROPERTY_VALUES'][$arProp['ID']] = array();
									$arUpdateValues['PROPERTY_VALUES'][$arProp['ID']][$arProp['PROPERTY_VALUE_ID']] = array(
										'VALUE' => $arProp['VALUE'],
										'DESCRIPTION' => $arProp['DESCRIPTION']
									);
								}
								else
								{
									$arUpdateValues['PROPERTY_VALUES'][$arProp['ID']] = array(
										'VALUE' => $arProp['VALUE'],
										'DESCRIPTION' => $arProp['DESCRIPTION']
									);
								}
							}
						}

						if ($this->arResult['ELEMENT']['CREATED_BY'] != $USER->GetID())
						{
							$this->arResult['ERRORS'][] = 'Access denied';
						}
						else
						{
							if (!$obElement->Update($this->arParams['ID'], $arUpdateValues))
							{
								$this->arResult['ERRORS'][] = $obElement->LAST_ERROR;
							}
						}
					}
					else
					{
						$arUpdateValues['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
						if (!$this->arParams['ID'] = $obElement->Add($arUpdateValues))
						{
							$this->arResult['ERRORS'][] = $obElement->LAST_ERROR;
						}
					}
				}
				
				if (empty($this->arResult['ERRORS']))
				{
					echo Json::encode(array(
							'response' => $this->arParams['ID']
					));
				}
				else
				{
					echo Json::encode(array(
							'error' => array(
								'error_msg' => implode("\n", $this->arResult['ERRORS'])
							)
					));
				}
				exit();
			}
		}

		$this->IncludeComponentTemplate();
	}
}
?>