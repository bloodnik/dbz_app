<?
use Bitrix\Main\Loader;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$ID = $request->get('id');

if (!Loader::includeModule('iblock'))
	return;

$dbElement = CIBlockElement::GetList(
	array(),
	array(
		'ID' => $ID,
		'ACTIVE' => 'Y',
		'CREATED_BY' => $USER->GetID()
	)
);
if($arElement = $dbElement->GetNext())
{
	if (ToLower(CIBlock::GetArrayByID($arElement['IBLOCK_ID'], 'CODE')) == 'task')
	{
		CIBlockElement::Delete($ID);
	}
	else
	{
		$obElement = new CIBlockElement;
		$obElement->Update(
				$ID,
				array(
						'ACTIVE' => 'N'
				)
		);
	}
}
?>