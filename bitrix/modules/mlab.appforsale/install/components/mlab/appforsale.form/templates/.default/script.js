/* mlab:appforsale.form */
(function (window) {
	
	if (!!window.JCForm)
	{
		return;
	}
	
	window.JCForm = function (arParams)
	{
		this.id = 0;
		this.ajaxUrl = null;
		this.visual = {
			ID: '',
			DELETE: ''
		};
		
		this.obDelete = null;
		
		if ('object' === typeof arParams)
		{
			this.id = arParams.ID;
			this.ajaxUrl = arParams.ajaxUrl;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCForm.prototype.Init = function()
	{
		this.obDelete = BX(this.visual.DELETE);
		
		if (!!this.obDelete)
		{
			BX.bind(this.obDelete, 'click', BX.delegate(this.Delete, this));
		}
	}
	
	window.JCForm.prototype.Delete = function()
	{
		BX.ajax({
			url: this.ajaxUrl,
			method: 'POST',
			data: {
				id: this.id
			},
			onsuccess: BX.delegate(function(data) {
				app.onCustomEvent('OnIblock');
				app.closeController();
			}, this),
			onfailure: BX.delegate(function() {
				
			}, this)
		});
	}
	
	
})(window);