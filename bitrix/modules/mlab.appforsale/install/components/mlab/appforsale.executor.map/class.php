<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
	
class ExecutorMap extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['HIDE_NONAME'] = $arParams['HIDE_NONAME'] == 'Y' ? 'Y' : 'N';
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		
		CModule::IncludeModule('iblock');
		
		$arFilter = array(
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			'!PROPERTY_GEO' => false
		);
		
		if (!empty($_GET['sections']))
		{
			$arFilter['SECTION_ID'] = explode(',', $_GET['sections']);
			$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		}
		
		$dbElement = CIBlockElement::GetList(
			array(),
			$arFilter,
			false,
			false,
			array(
				'ID',
				'IBLOCK_ID',
				'CREATED_BY',
				'PROPERTY_*'
			)
		);
		//$dbElement->SetUrlTemplates($this->arParams['DETAIL_URL']);
		$this->arResult['ITEMS'] = array();
		$arUserID = array();
		while($obElement = $dbElement->GetNextElement())
		{
			$arElement = $obElement->GetFields();
			if (!array_key_exists($arElement['CREATED_BY'], $arUserID))
			{
				$arUserID[$arElement['CREATED_BY']] = true;
				$arElement['PROPERTIES'] = $obElement->GetProperties();
	
				$this->arResult['ITEMS'][] = $arElement;
			}
		}
		
		if (!empty($arUserID))
		{
			$dbUser = CUser::GetList($b, $o, array('ID' => array_keys($arUserID)), array('SELECT' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME')));
			$nameFormat = CSite::GetNameFormat(true);
			while ($arUser = $dbUser->fetch())
			{
				$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
				$this->arResult['USERS'][$arUser['ID']] = $arUser;
			}
		}
		
		$this->arResult['OBJECTS'] = array();
		foreach ($this->arResult['ITEMS'] as $arElement)
		{
			$arAddress = explode(',', $arElement['PROPERTIES']['GEO']['VALUE']);
			$this->arResult['OBJECTS'][] = array(
					$arAddress,
					array(
							//		'clusterCaption' => $arElement['NAME'],
							'balloonContent' => '<p>'.$this->arResult['USERS'][$arElement['CREATED_BY']]['FORMAT_NAME'] .'</p><p><a class="btn btn-info btn-xs" title="'.$this->arResult['USERS'][$arElement['CREATED_BY']]['FORMAT_NAME'] .'" href="u'.$arElement['CREATED_BY'].'/">'.Loc::getMessage("DETAIL_PAGE_URL").'</a></p>'
					)
			);
		}
		
		
		$this->IncludeComponentTemplate();
	}
}
?>