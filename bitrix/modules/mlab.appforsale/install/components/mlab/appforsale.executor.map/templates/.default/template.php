<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.map.system', 
	'', 
	array(
		'OBJECTS' => $arResult['OBJECTS'],
		'ROUTE' => 'N'
	),
	$component
);
?>