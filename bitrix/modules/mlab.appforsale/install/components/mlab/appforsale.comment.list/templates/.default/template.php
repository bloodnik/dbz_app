<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<?foreach ($arResult['ITEMS'] as $arItem):?>
		<div class="afs-comment">
				<?
				$APPLICATION->IncludeComponent(
					'mlab:appforsale.user.item',
					'',
					array(
						'RESULT' => array(
							'ITEM' => $arItem['USER'],
							'AREA_ID' => $this->GetEditAreaId($arItem['CREATED_BY']),
							'TYPE' => 'LINE'
						)
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
				?>
			<?
			
			$rating = 0;
			foreach ($arItem['DISPLAY_PROPERTIES'] as $property):
				if ($property['CODE'] == 'RATING')
				{
					$rating = intval($property['DISPLAY_VALUE']);
					continue;
				}
			?>
				<div><?=(
					is_array($property['DISPLAY_VALUE'])
						? implode(' / ', $property['DISPLAY_VALUE'])
						: $property['DISPLAY_VALUE']
					)?>
				</div>
			<?endforeach;?>
			
			<?if ($rating > 0):?>
			<div class="afs-rating">
				<?for ($i = 0; $i < 5; $i++):?>
					<?
					if ($i >= $rating)
						echo '<img src="'.$templateFolder.'/images/ic_rating_off.png" />';
					else
						echo '<img src="'.$templateFolder.'/images/ic_rating_on.png" />';
					?>
				<?endfor;?>
			</div>
			<?endif;?>
		</div>
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>