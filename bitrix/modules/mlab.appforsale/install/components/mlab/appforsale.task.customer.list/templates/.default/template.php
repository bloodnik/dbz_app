<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['ITEMS'])):?>
<div class="afs-task-customer-rows">
<?foreach ($arResult['ITEMS'] as $arItem):?>
		<div style="position: relative">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.task.item',
			'',
			array(
				'RESULT' => array(
					'ITEM' => $arItem,
					'AREA_ID' => $this->GetEditAreaId($arItem['ID']),
					'MORE' => 'Y'
				)
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
		?>
		<?if($_GET['archive'] != 'Y'):?>
			<a title="<?=$arItem['NAME']?>" href="<?=$arItem['DETAIL_PAGE_URL']?>edit/"><img class="afs-item-more" src="<?=$templateFolder?>/images/more.png"></a>
		<?endif;?>
		</div>
<?endforeach;?>
<?=$arResult['NAV_STRING']?>
</div>
<?else:?>
	<div class="afs-empty"><?=$arParams['MESS_LIST_EMPTY']?></div>	
<?endif;?>