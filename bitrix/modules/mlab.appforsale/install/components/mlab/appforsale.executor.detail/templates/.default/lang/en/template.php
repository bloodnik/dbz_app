<?
$MESS['EXECUTOR_DONE'] = 'Performed';
$MESS['EXECUTOR_CREATE'] = 'created';
$MESS['EXECUTOR_TASK1'] = 'job';
$MESS['EXECUTOR_TASK2'] = 'job';
$MESS['EXECUTOR_TASK5'] = 'jobs';
$MESS['EXECUTOR_SHOW_COUNTER1'] = 'view profile';
$MESS['EXECUTOR_SHOW_COUNTER2'] = 'view profile';
$MESS['EXECUTOR_SHOW_COUNTER5'] = 'profile views';
$MESS['CALL'] = 'Call';
$MESS['SEND'] = 'Write';
$MESS['CALL_DISABLED'] = 'The call will be available if the confirm offer';
$MESS['SEND_DISABLED'] = 'Sending messages will be available if the confirm offer';

$MESS['OFFER_BUTTON'] = 'To offer the job';
$MESS['OFFER_INFO'] = 'The contractor will be notified and will be able to provide you their services';
?>