<?
$MESS['EXECUTOR_DONE'] = 'Выполнил';
$MESS['EXECUTOR_CREATE'] = 'создал';
$MESS['EXECUTOR_TASK1'] = 'задание';
$MESS['EXECUTOR_TASK2'] = 'задания';
$MESS['EXECUTOR_TASK5'] = 'заданий';
$MESS['EXECUTOR_SHOW_COUNTER1'] = 'просмотр профиля';
$MESS['EXECUTOR_SHOW_COUNTER2'] = 'просмотра профиля';
$MESS['EXECUTOR_SHOW_COUNTER5'] = 'просмотров профиля';
$MESS['CALL'] = 'Позвонить';
$MESS['SEND'] = 'Написать';
$MESS['CALL_DISABLED'] = 'Звонок станет доступным если подтвердить предложение';
$MESS['SEND_DISABLED'] = 'Отправка сообщений станет доступной если подтвердить предложение';

$MESS['OFFER_BUTTON'] = 'Предложить задание';
$MESS['OFFER_INFO'] = 'Исполнитель получит уведомление и сможет оказать вам свои услуги';
?>