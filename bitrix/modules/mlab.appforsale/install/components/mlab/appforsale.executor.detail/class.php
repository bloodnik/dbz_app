<? 
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class ExecutorDetail extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['ID'] = isset($arParams['ID']) ? (int) $arParams['ID'] : 0;	
		
		return $arParams;
	}
	
	public function executeComponent()
	{		
		global $USER;
		Loader::includeModule('iblock');
		
		if ($this->arParams['ID'] > 0)
		{
			$this->counterInc($this->arParams['ID']);
			
			
			//$dbUser = \CUser::GetList($b, $o, array('ID' => array_keys($arUserID)), array('SELECT' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'IS_ONLINE', 'LAST_ACTIVITY_DATE', 'UF_CITY', 'UF_RATING'), 'ONLINE_INTERVAL' => 900));
			
			
			$dbUser = CUser::GetList($b, $o, array('ID' => $this->arParams['ID']), array('SELECT' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'IS_ONLINE', 'LAST_ACTIVITY_DATE', 'PERSONAL_MOBILE', 'UF_RATING'), 'ONLINE_INTERVAL' => 900));
			//$dbUser = CUser::GetByID($this->arParams['ID']);
			if ($this->arResult['arUser'] = $dbUser->Fetch())
			{
				if ($this->arResult['arUser']['IS_ONLINE'] == 'Y')
				{
					$this->arResult['LAST_ACTIVITY_DATE'] = '<span style="color: #4caf50">'.Loc::getMessage('IS_ONLINE').'</span>';
				}
				else
				{
					$this->arResult['LAST_ACTIVITY_DATE'] = Loc::getMessage('LAST_ACTIVITY_DATE') . ' ' .CIBlockFormatProperties::DateFormat('x', MakeTimeStamp($this->arResult['arUser']['LAST_ACTIVITY_DATE'], 'YYYY-MM-DD HH:MI:SS'));
				}
				
				$this->arResult['arUser']['PERSONAL_PHOTO'] = (0 < $this->arResult['arUser']['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($this->arResult['arUser']['PERSONAL_PHOTO'], array('width' => 160, 'height' => 160), BX_RESIZE_IMAGE_EXACT, true) : false);
				$nameFormat = CSite::GetNameFormat(true);
				$this->arResult['arUser']['FORMAT_NAME'] = CUser::FormatName($nameFormat, $this->arResult['arUser']);				
				
				$user_id = $USER->GetID();
				if ($user_id > 0)
				{
					$dbElement = CIBlockElement::GetList(
						array(),
						array(
							'IBLOCK_CODE' => 'task',
							'ACTIVE' => 'Y',
							array(
								'LOGIC' => 'OR',
								array(
									'CREATED_BY' => $this->arResult['arUser']['ID'],
									'PROPERTY_PROFILE_ID' => $user_id
								),
								array(
									'CREATED_BY' => $user_id,
									'PROPERTY_PROFILE_ID' => $this->arResult['arUser']['ID']
								)
							),
							'!PROPERTY_STATUS_ID' => 'F',
						),
						false,
						false,
						array(
							'ID'
						)
					);
					
					if ($arElement = $dbElement->GetNext())
					{

					}
					else
					{
						$this->arResult['arUser']['PERSONAL_MOBILE'] = '+7 (XXX) XXX-XX-XX';
					}
				}
				else
				{
					$this->arResult['arUser']['PERSONAL_MOBILE'] = '+7 (XXX) XXX-XX-XX';
				}
				
				// CREATE
				$this->arResult['CREATE'] = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_CODE' => 'task',
						'ACTIVE' => 'Y',
						'CREATED_BY' => $this->arResult['arUser']['ID']
					),
					array()
				);
				
				// DONE
				$this->arResult['DONE'] = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_CODE' => 'task',
						'ACTIVE' => 'Y',
						'PROPERTY_PROFILE_ID' => $this->arResult['arUser']['ID'],
						'PROPERTY_STATUS_ID' => 'F'
					),
					array()
				);
				
			

				$this->IncludeComponentTemplate();
			}
		}
	}
	
	private function counterInc($user_id)
	{
		global $USER_FIELD_MANAGER;
		
		$user_id = (int) $user_id;
		if ($user_id <= 0)
			return;
		
		$this->arResult['SHOW_COUNTER'] = intval($USER_FIELD_MANAGER->GetUserFieldValue('USER', 'UF_SHOW_COUNTER', $user_id));
		
		if(!is_array($_SESSION['USER_COUNTER']))
			$_SESSION['USER_COUNTER'] = array();
		if(in_array($user_id, $_SESSION['USER_COUNTER']))
			return;
		$_SESSION['USER_COUNTER'][] = $user_id;
		
		$this->arResult['SHOW_COUNTER']++;
		$USER_FIELD_MANAGER->Update('USER', $user_id, array('UF_SHOW_COUNTER'  => $this->arResult['SHOW_COUNTER']));
	}
}
?>