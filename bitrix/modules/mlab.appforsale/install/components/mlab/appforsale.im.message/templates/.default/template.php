<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId('');
$arItemIDs = array(
	'HISTORY' => $strMainID.'_history',
	'TYPER' => $strMainID.'_typer',
	'INPUT' => $strMainID.'_input',
	'CONTROL' => $strMainID.'_control',
	'BTN' => $strMainID.'_btn'
);
?>
<div style="position: relative">
<div class="im-chat-wrap">
	<div class="im-chat">
		<div class="im-history" id="<?=$arItemIDs['HISTORY']?>"></div>
		<div class="im-typer" id="<?=$arItemIDs['TYPER']?>"><?=GetMessage('TYPER')?></div>
	</div>
</div>
<div class="im-input">
	<form class="input-group" id="<?=$arItemIDs['INPUT']?>">
		<input class="form-control" style="font-size: 14px" type="text" tabindex="0" id="<?=$arItemIDs['CONTROL']?>" placeholder="<?=GetMessage('PLACEHOLDER')?>" autocomplete="off" />
		<span class="input-group-btn">
        	<button class="btn btn-primary" id="<?=$arItemIDs['BTN']?>" disabled="disabled"><?=GetMessage('SEND')?></button>
        </span>
	</form>
</div>
</div>
<script type="text/javascript">
	new Im(<?=CUtil::PhpToJSObject(array('VISUAL' => $arItemIDs, 'user_id' => $arParams['USER_ID'], 'from_id' => $USER->GetID(), 'USERS' => $arResult['USERS']), false, true)?>);
</script>