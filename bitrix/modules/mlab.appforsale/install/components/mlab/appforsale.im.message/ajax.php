<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

$request = Bitrix\Main\Context::getCurrent()->getRequest();
$request->addFilter(new Bitrix\Main\Web\PostDecodeFilter);

CModule::IncludeModule('mlab.appforsale');

if ($request->getPost('GET_HISTORY') == 'Y')
{
	$arResult = Mlab\Appforsale\Im\Messages::getHistory($request->getPost('offset'), $request->getPost('count'), $request->getPost('user_id'), 0, $request->getPost('start_message_id'));
	echo CUtil::PhpToJsObject($arResult);
}
else if ($request->getPost('SEND_MESSAGE') == 'Y')
{
	$arResult = Mlab\Appforsale\Im\Messages::send($request->getPost('user_id'), $request->getPost('message'));
	echo CUtil::PhpToJsObject($arResult);
}
else if ($request->getPost('SET_ACTIVITY') == 'Y')
{	
	Mlab\Appforsale\Im\Messages::setActivity($request->getPost('user_id'));
}
else if ($request->getPost('MARK_AS_READ') == 'Y')
{
	\Mlab\Appforsale\Im\Messages::markAsRead($request->getPost('user_id'));
}

CMain::FinalActions();
die();
?>