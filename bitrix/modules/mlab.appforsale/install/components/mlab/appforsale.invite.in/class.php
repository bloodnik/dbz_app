<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

class InviteIn extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$arParams['MESS_INVALID_PROMOCODE'] = $arParams['MESS_INVALID_PROMOCODE'] ?: Loc::getMessage('INVALID_PROMOCODE');
		$arParams['MESS_PROMODOCE_ACTIVATE'] = $arParams['MESS_PROMODOCE_ACTIVATE'] ?: Loc::getMessage('PROMODOCE_ACTIVATE');
		$arParams['MESS_ONLY_ONCE'] = $arParams['MESS_ONLY_ONCE'] ?: Loc::getMessage('ONLY_ONCE');
		
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER, $USER_FIELD_MANAGER;
		
		if (Loader::includeModule('iblock'))
		{
			$parent_id = $USER_FIELD_MANAGER->GetUserFieldValue('USER', 'UF_PARENT_ID', $USER->GetID());
			if (intval($parent_id) == 0)
			{
				$dbElement = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
						'ACTIVE' => 'Y',
						'NAME' => $this->arParams['CODE']
					),
					false,
					false,
					array(
						'ID',
						'CREATED_BY'
					)
				);
				if($arElement = $dbElement->GetNext())
				{
					$USER_FIELD_MANAGER->Update('USER', $USER->GetID(), array(
							'UF_PARENT_ID' => $arElement['CREATED_BY']
					));
					
					CIBlockElement::CounterInc($arElement['ID']);
					
					$this->arResult['MESSAGE'] = Loc::getMessage('PROMODOCE_ACTIVATE');
				}
				else
				{
					$this->arResult['MESSAGE'] = Loc::getMessage('INVALID_PROMOCODE');
				}
			}
			else
			{
				$this->arResult['MESSAGE'] = Loc::getMessage('ONLY_ONCE');
			}	
		}
		
		$this->includeComponentTemplate();
	}
}
?>