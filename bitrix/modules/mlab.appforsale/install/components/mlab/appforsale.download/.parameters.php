<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'MESS_DOWNLOAD_NAME' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('DOWNLOAD_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DOWNLOAD_NAME_DEFAULT')
		),
		'MESS_DOWNLOAD_DESC' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('DOWNLOAD_DESC'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('DOWNLOAD_DESC_DEFAULT')
		),
		'LINK_APPLE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('LINK_APPLE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('LINK_APPLE_DEFAULT')
		),
		'LINK_GOOGLE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('LINK_GOOGLE'),
			'TYPE' => 'STRING',
			'DEFAULT' => Loc::getMessage('LINK_GOOGLE_DEFAULT')
		)
	)
);
?>