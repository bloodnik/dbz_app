<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('DOWNLOAD_NAME'),
	'SORT' => 20,
	'DESCRIPTION' => Loc::getMessage('DOWNLOAD_DESCRIPTION'),
	'COMPLEX' => 'N',
	'PATH' => array(
		'ID' => 'appforsale',
		'NAME' => Loc::getMessage('DOWNLOAD_PATH_NAME'),
		'SORT' => 1,
		'CHILD' => array(
			'ID' => 'landing',
			'NAME' => Loc::getMessage('DOWNLOAD_PATH_CHILD_NAME'),
			'SORT' => 10
		)
	)
);
?>