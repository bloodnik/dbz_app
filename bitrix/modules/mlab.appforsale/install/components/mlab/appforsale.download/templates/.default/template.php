<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="row afs-download">
	<div class="col-md-6">
		<div class="afs-download-title"><?=$arParams['MESS_DOWNLOAD_NAME']?></div>
		<div class="afs-download-text"><?=$arParams['MESS_DOWNLOAD_DESC']?></div>
		<div class="afs-download-badge">	
			<a href="<?=$arParams['LINK_APPLE']?>" target="_blank"><img src="<?=$templateFolder?>/images/badge-download-on-the-app-store-ru.png" /></a> <a href="<?=$arParams['LINK_GOOGLE']?>" target="_blank"><img src="<?=$templateFolder?>/images/google-play-badge.png" /></a>
		</div>
	</div>
	<div class="col-md-6">
	</div>	
</div>