<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.form',
	'',
	array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'PROPERTY_CODE' =>$arParams['PROPERTY_CODE'],
		'PROPERTY_CODE_REQUIRED' =>$arParams['PROPERTY_CODE_REQUIRED'],
		'MESS_PROPERTY_VALUE_NA' => $arParams['MESS_PROPERTY_VALUE_NA'],
		'MESS_BTN_ADD' => $arParams['MESS_BTN_ADD'],
		'UPDATE_PROPERTY_VALUES' => array(
			'TASK_ID' => $arParams['ELEMENT_ID']
		)
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
?>