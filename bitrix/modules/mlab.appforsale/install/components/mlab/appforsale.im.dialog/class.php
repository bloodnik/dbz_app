<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use Mlab\Appforsale\Im\Model\DialogTable;

class ImDialog extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
	
	public function executeComponent()
	{
		global $USER;
		
		CModule::IncludeModule('mlab.appforsale');
		
		$userIDs = array();
		$dbDialog = DialogTable::getList(array(
				'filter' => array(
					'=from_id' => $USER->GetID()
				),
				'select' => array(
					'user_id',
					'body',
					'out',
					'read_state',
					'unread'
				)
		));
		while ($arDialog = $dbDialog->fetch())
		{
			$userIDs[] = $arDialog['user_id'];

			
			
			
			$arDialog['message_page_url'] = str_replace(array('#sel#', 'index.php'), array($arDialog['user_id'], ''), $this->arParams['MESSAGE_URL']);
			
			$arDialog['body'] = ($arDialog['out'] == 1 ? GetMessage('YOU').': ' : '') . htmlspecialcharsEx($arDialog['body']);
			$this->arResult['DIALOG'][] = $arDialog;
		}
		
		$dbUser = \CUser::GetList(
			$b,
			$o,
			array(
				'ID' => implode('|', $userIDs)
			),
			array(
				'FIELDS' => array(
					'ID',
					'NAME',
					'LAST_NAME',
					'PERSONAL_PHOTO'
				)
			)
		);
		$nameFormat =\CSite::GetNameFormat(true);
		while ($arUser = $dbUser->fetch())
		{
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? \CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : array('src' => '/bitrix/components/mlab/appforsale.im.message/templates/.default/images/no_photo.png'));
			$arUser['FORMAT_NAME'] = \CUser::FormatName($nameFormat, $arUser);
			$this->arResult['USERS'][$arUser['ID']] = $arUser;
		}
		
		$this->IncludeComponentTemplate();
	}
}
?>