(function (window) {
	
	window.ImDialog = function (arParams)
	{
		this.user_id = 0;
		this.out = 0;
		this.unread = 0;
		
		this.obBody = null;
		this.obUnread = null;
		
		this.visual = {
			BODY: '',
			UNREAD: ''
		};
		
		if ('object' === typeof arParams)
		{
			this.user_id = arParams.user_id;
			this.out = arParams.out;
			this.unread = arParams.unread;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.ImDialog.prototype.showUnread = function()
	{
		if (this.unread > 0)
		{
			if (!BX.hasClass(this.obUnread, 'im-dialog-unread-big'))
				BX.addClass(this.obUnread, 'im-dialog-unread-big');
			BX.html(this.obUnread, this.unread);
		}
		else
		{
			if (BX.hasClass(this.obUnread, 'im-dialog-unread-big'))
				BX.removeClass(this.obUnread, 'im-dialog-unread-big')
			BX.html(this.obUnread, '');
		}
	}
	
	window.ImDialog.prototype.Init = function()
	{
		this.obBody = BX(this.visual.BODY);
		this.obUnread = BX(this.visual.UNREAD);
		if (!!this.obBody)
		{
			BX.addCustomEvent('message', BX.delegate(function(data) {
				
				if (data.user_id == this.user_id)
				{
					if (data.out == 1)
					{
						data.body = BX.message('YOU') + ': ' + data.body;
						this.unread = 0;
						this.showUnread();
					}
					else
					{
						this.unread++;
						this.showUnread();
					}
					BX.html(this.obBody, data.body)
					BX.show(this.obUnread);
					this.out = data.out;
				}

			}, this));	
			
			BX.addCustomEvent('markAsRead', BX.delegate(function(data) {
				
				if (data.user_id == this.user_id && data.out == this.out)
				{
					this.unread = 0;
					this.showUnread();
					BX.hide(this.obUnread);
				}
				
			}, this));
		}
	}

})(window);
