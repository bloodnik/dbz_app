<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['DIALOG'])):?>
	<div>
	<?foreach ($arResult['DIALOG'] as $dialog):?>
		<?
		$strMainID = $this->GetEditAreaId($dialog['user_id']);
		$arItemIDs = array(
				'BODY' => $strMainID.'_body',
				'UNREAD' => $strMainID.'_unread',
		);
		?>
		<a class="im-dialog-wrap" href="<?=$dialog['message_page_url']?>" title="<?=$arResult['USERS'][$dialog['user_id']]['FORMAT_NAME']?>">
			<div class="im-dialog">
				<div class="im-dialog-image">
					<img class="im-dialog-img" src="<?=$arResult['USERS'][$dialog['user_id']]['PERSONAL_PHOTO']['src']?>" />
				</div>
				<div class="im-dialog-info">
					<div class="im-dialog-title"><?=$arResult['USERS'][$dialog['user_id']]['FORMAT_NAME']?></div>
					<div class="im-dialog-body" id="<?=$arItemIDs['BODY']?>"><?=$dialog['body']?></div>
					<div class="im-dialog-unread<?=($dialog['unread'] > 0 ? ' im-dialog-unread-big' : '')?>"<?=($dialog['read_state'] == 1 ? ' style="display: none;"' : '')?>id="<?=$arItemIDs['UNREAD']?>"><?=($dialog['unread'] > 0 ? $dialog['unread'] : '')?></div>
				</div>
			</div>
		</a>
		<script type="text/javascript">
			new ImDialog(<?=CUtil::PhpToJSObject(array('VISUAL' => $arItemIDs, 'user_id' => $dialog['user_id'], 'out' => $dialog['out'], 'unread' => $dialog['unread']), false, true)?>);
		</script>
		
		

		
		
		
		
		
	<?endforeach;?>
	</div>
	<script type="text/javascript">
			BX.message({YOU: '<?=GetMessage('YOU')?>'});
	</script>
<?endif;?>