<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class NotificationList extends \Mlab\Appforsale\Component\ElementList
{
	public function executeComponent()
	{
		global $USER;
		if (!$USER->IsAuthorized())
			CAppforsale::AuthForm();
		
		CModule::IncludeModule('iblock');
		$dbElement = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
				'ACTIVE' => 'Y',
				'CREATED_BY' => $USER->GetID()
			)
		);
		while($arElement = $dbElement->GetNext())
		{
			$obElement = new CIBlockElement;
			$obElement->Update($arElement['ID'], array('ACTIVE' => 'N'));
		}
		
		parent::executeComponent();
	}
	
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		$arFilter['CREATED_BY'] = $USER->GetID();
		unset($arFilter['ACTIVE']);
		return $arFilter;
	}
	

	protected function getSelect()
	{
		$arSelect = parent::getSelect();
		$arSelect[] = 'DATE_CREATE';
		return $arSelect;
	}
}
?>