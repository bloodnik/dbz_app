<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['DISPLAY_PROPERTIES'])):?>
<div class="afs-task-customer-detail-rows">
	<div class="row hidden-sm hidden-xs">
		<div class="col-md-12">
			<h1 style="font-size: 26px"><?=$arResult['NAME']?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 afs-task-header">
		<? $status_id = CIBlockFormatProperties::GetDisplayValue(null, $arResult['PROPERTIES']['STATUS_ID'], ""); echo $status_id['DISPLAY_VALUE']?> | #SHOW_COUNTER# <?=GetMessage('SHOW_COUNTER')?> | <?=GetMessage('CREATE')?> #DATE_CREATE# | <?=$arResult['IBLOCK_SECTION']['NAME']?> | <?=GetMessage('TASK')?><?=$arResult['ID']?>
		</div>
	</div>
	<?foreach ($arResult['DISPLAY_PROPERTIES'] as $property):?>
		<div class="afs-task-customer-detail-row row">
			<div class="col-md-3"><b><?=$property['NAME']?></b></div>
			<div class="col-md-9"><?=(
				is_array($property['DISPLAY_VALUE'])
					? implode(' / ', $property['DISPLAY_VALUE'])
					: $property['DISPLAY_VALUE']
				)?>
			</div>
		</div>
	<?endforeach;?>
</div>
<?endif;?>