<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

class TaskExecutorDetail extends \Mlab\Appforsale\Component\Element
{
	protected function getFilter()
	{
		global $USER;
		$arFilter = parent::getFilter();
		$arFilter['!CREATED_BY'] = $USER->GetID();
		$arFilter['PROPERTY_PROFILE_ID'] = $USER->GetID();
		return $arFilter;
	}
}
?>