<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if (!empty($arResult['DISPLAY_PROPERTIES'])):?>
<div class="afs-task-customer-detail-rows">
	<?foreach ($arResult['DISPLAY_PROPERTIES'] as $property):?>
		<div class="afs-task-customer-detail-row row">
			<div class="col-md-3"><b><?=$property['NAME']?></b></div>
			<div class="col-md-9"><?=(
				is_array($property['DISPLAY_VALUE'])
					? implode(' / ', $property['DISPLAY_VALUE'])
					: $property['DISPLAY_VALUE']
				)?>
			</div>
		</div>
	<?endforeach;?>
</div>
<?endif;?>