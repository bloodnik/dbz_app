<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

foreach ($arResult['ITEMS'] as &$arItem)
{
	$arItem['PREVIEW_PICTURE'] = (0 < $arItem['PREVIEW_PICTURE'] ? CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 200, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true) : false);
}
?>