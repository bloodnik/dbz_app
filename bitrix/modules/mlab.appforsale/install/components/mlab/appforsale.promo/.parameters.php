<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Mlab\Appforsale\Component\Parameters;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(!Loader::includeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arSort = CIBlockParameters::GetElementSortFields(
		array('TIMESTAMP_X', 'SORT', 'ID'),
		array('KEY_LOWERCASE' => 'Y')
);

$arAscDesc = array(
		'asc' => Loc::getMessage('SORT_ASC'),
		'desc' => Loc::getMessage('SORT_DESC')
);

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arIBlockType,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('IBLOCK'),
			'TYPE' => 'LIST',
			'VALUES' => Parameters::getIBlock($arCurrentValues['IBLOCK_TYPE']),
			'REFRESH' => 'Y',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'CACHE_TIME' => array('DEFAULT' => 36000000),
		'SORT_FIELD' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'timestamp_x',
		),
		'SORT_ORDER' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
		'SORT_FIELD2' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arSort,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'timestamp_x',
		),
		'SORT_ORDER2' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'desc',
			'ADDITIONAL_VALUES' => 'Y'
		),
			
		'FIELD_CODE' => Parameters::getFieldCode(Loc::getMessage('FIELD_CODE'), 'BASE'),
	)
);
?>