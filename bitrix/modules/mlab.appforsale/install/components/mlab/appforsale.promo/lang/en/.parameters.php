<?
$MESS['IBLOCK_TYPE'] = 'The type of InfoBlock';
$MESS['IBLOCK'] = 'Infoblok';
$MESS['SORT_FIELD'] = 'The first field for sorting';
$MESS['SORT_ORDER'] = 'The direction of the first sort';
$MESS['SORT_FIELD2'] = 'A second field for sorting';
$MESS['SORT_ORDER2'] = 'The direction of the secondary sort';
$MESS['SORT_ASC'] = 'ascending';
$MESS['SORT_DESC'] = 'descending';
$MESS['FIELD_CODE'] = 'Field';
?>