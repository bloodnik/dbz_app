<?
$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['SORT_FIELD'] = 'Первое поле для сортировки';
$MESS['SORT_ORDER'] = 'Направление первой сортировки';
$MESS['SORT_FIELD2'] = 'Второе поле для сортировки';
$MESS['SORT_ORDER2'] = 'Направление второй сортировки';
$MESS['SORT_ASC'] = 'по возрастанию';
$MESS['SORT_DESC'] = 'по убыванию';
$MESS['FIELD_CODE'] = 'Поля';
?>