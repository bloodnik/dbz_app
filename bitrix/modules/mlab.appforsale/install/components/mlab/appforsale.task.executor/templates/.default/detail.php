<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$comment_url = CComponentEngine::MakePathFromTemplate($arResult['FOLDER'].$arResult['URL_TEMPLATES']['comment'], $arResult['VARIABLES']);
?>
<div>
	<div class="col-md-8">
		<?		
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.task.executor.detail',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'FIELD_CODE' => $arParams['DETAIL_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME']
			),
			$component
		);
		?>
		
		
		<div class="clearfix"></div>
		<? 
		
		if (!is_array($arParams['OFFER_FIELD_CODE']))
			$arParams['OFFER_FIELD_CODE'] = array();
		foreach ($arParams['OFFER_FIELD_CODE'] as $key => $val)
			if (!$val)
				unset($arParams['OFFER_FIELD_CODE'][$key]);
		
			if (!is_array($arParams['OFFER_PROPERTY_CODE']))
				$arParams['OFFER_PROPERTY_CODE'] = array();
			foreach ($arParams['OFFER_PROPERTY_CODE'] as $key => $val)
				if ($val === '')
					unset($arParams['OFFER_PROPERTY_CODE'][$key]);
	
				
		$text = CIBlockProperty::GetPropertyArray('TEXT', $arParams['OFFER_IBLOCK_ID']);
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.offer',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['OFFER_IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['OFFER_IBLOCK_ID'],
				'TASK_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'FIELD_CODE' => $arParams['OFFER_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['OFFER_PROPERTY_CODE'] ?: array($text['ORIG_ID']),
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'COMMENT_URL' => $comment_url
			),
			$component
		);
		?>
		

		
	</div>
	<div class="col-md-4">
		<div class="row afs-h5"><div class="col-xs-12"><h5><?=GetMessage('CUSTOMER_TASK')?></h5></div></div>

		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.user.card',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME']
			),
			$component
		);
		?>
	</div>
</div>