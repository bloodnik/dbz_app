<?
$MESS['LIST_SETTINGS'] = 'list Settings';
$MESS['DETAIL_SETTINGS'] = 'Settings detailed view';
$MESS['OFFER_SETTINGS'] = 'set suggestions';
$MESS['COMMENT_SETTINGS'] = 'Settings for comments';

$MESS['IBLOCK_TYPE'] = 'Type of the information block';
$MESS['IBLOCK'] = 'information block';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type of the information block proposals';
$MESS['OFFER_IBLOCK'] = 'information block proposals';
$MESS['SEF_MODE_LIST'] = 'jobs List';
$MESS['SEF_MODE_DETAIL'] = 'Details';
$MESS['SEF_MODE_COMMENT'] = 'Comment';
$MESS['LIST_FIELD_CODE'] = 'Fields';
$MESS['LIST_PROPERTY_CODE'] = 'Properties';
$MESS['DETAIL_FIELD_CODE'] = 'Fields';
$MESS['DETAIL_PROPERTY_CODE'] = 'Properties';
$MESS['OFFER_FIELD_CODE'] = 'Fields';
$MESS['OFFER_PROPERTY_CODE'] = 'Properties of sentences';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type of the information block proposals';
$MESS['OFFER_IBLOCK'] = 'information block proposals';

$MESS['ELEMENT_SORT_FIELD'] = 'First field to sort';
$MESS['ELEMENT_SORT_ORDER'] = 'the first sort';
$MESS['ELEMENT_SORT_FIELD2'] = 'the Second field to sort';
$MESS['ELEMENT_SORT_ORDER2'] = 'the Direction of the second sort';
$MESS['SORT_ASC'] = 'ascending';
$MESS['SORT_DESC'] = 'descending';

$MESS['DETAIL_MESS_CUSTOMER'] = 'the Text "Customer job"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'the Client of this job';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'topic ID';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Character code of the section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'the Path of the symbolic codes section';

$MESS['COMMENT_PROPERTY_CODE'] = 'Properties';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'Properties are mandatory';
$MESS['COMMENT_MESS_BTN_ADD'] = 'the text of the button "Save"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Save';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Inscription blank properties';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(not set)';

$MESS['USE_SWITCH'] = 'Use switch';
?>