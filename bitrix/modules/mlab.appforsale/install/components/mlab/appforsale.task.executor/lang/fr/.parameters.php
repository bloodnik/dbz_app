<?
$MESS['LIST_SETTINGS'] = 'ParamГЁtres de la liste';
$MESS['DETAIL_SETTINGS'] = 'Configuration dГ©taillГ©e de la navigation';
$MESS['OFFER_SETTINGS'] = 'ParamГЁtres d\'offres';
$MESS['COMMENT_SETTINGS'] = 'ParamГЁtres des commentaires';

$MESS['IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР°';
$MESS['IBLOCK'] = 'un jeu de donnГ©es';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР° offres';
$MESS['OFFER_IBLOCK'] = 'un jeu de donnГ©es d\'offres';
$MESS['SEF_MODE_LIST'] = 'Liste des tГўches';
$MESS['SEF_MODE_DETAIL'] = 'DГ©tail';
$MESS['SEF_MODE_COMMENT'] = 'Commentaires';
$MESS['LIST_FIELD_CODE'] = 'Champs';
$MESS['LIST_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['DETAIL_FIELD_CODE'] = 'Champs';
$MESS['DETAIL_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['OFFER_FIELD_CODE'] = 'le Champ de la proposition';
$MESS['OFFER_PROPERTY_CODE'] = 'PropriГ©tГ©s de la proposition';
$MESS['OFFER_IBLOCK_TYPE'] = 'Type de РёРЅС„РѕР±Р»РѕРєР° offres';
$MESS['OFFER_IBLOCK'] = 'un jeu de donnГ©es d\'offres';

$MESS['ELEMENT_SORT_FIELD'] = 'le Premier champ de tri';
$MESS['ELEMENT_SORT_ORDER'] = 'la Direction du premier tri';
$MESS['ELEMENT_SORT_FIELD2'] = 'le DeuxiГЁme champ de tri';
$MESS['ELEMENT_SORT_ORDER2'] = 'la Direction de la deuxiГЁme tri';
$MESS['SORT_ASC'] = 'croissant';
$MESS['SORT_DESC'] = 'desc';

$MESS['DETAIL_MESS_CUSTOMER'] = 'le Texte "le Client de cette tГўche"';
$MESS['DETAIL_MESS_CUSTOMER_DEFAULT'] = 'le Client de cette tГўche';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Id de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'le code de CaractГЁre de la section';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Chemin de caractГЁres des codes de la section';

$MESS['COMMENT_PROPERTY_CODE'] = 'PropriГ©tГ©s';
$MESS['COMMENT_PROPERTY_CODE_REQUIRED'] = 'PropriГ©tГ©s obligatoires';
$MESS['COMMENT_MESS_BTN_ADD'] = 'le Texte du bouton "Enregistrer"';
$MESS['COMMENT_MESS_BTN_ADD_DEFAULT'] = 'Enregistrer';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA'] = 'Inscription vide propriГ©tГ©s';
$MESS['COMMENT_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(non dГ©fini)';

$MESS['USE_SWITCH'] = 'Utiliser le commutateur';
?>