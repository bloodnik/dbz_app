<?
$MESS['SECTIONS_SETTINGS'] = 'Настройки списка разделов';
$MESS['FORM_SETTINGS'] = 'Настройки формы редактирования';
$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['SEF_MODE_SECTIONS'] = 'Список разделов';
$MESS['SEF_MODE_FORM'] = 'Форма';
$MESS['SECTIONS_FIELD_CODE'] = 'Поля';
$MESS['FORM_PROPERTY_CODE'] = 'Свойства';
$MESS['FORM_PROPERTY_CODE_REQUIRED'] = 'Свойства обязательные для заполнения';
$MESS['FORM_MESS_BTN_ADD'] = 'Текст кнопки "Сохранить"';
$MESS['FORM_MESS_BTN_ADD_DEFAULT'] = 'Сохранить';
$MESS['FORM_MESS_PROPERTY_VALUE_NA'] = 'Надпись незаполненного свойства';
$MESS['FORM_MESS_PROPERTY_VALUE_NA_DEFAULT'] = '(не установлено)';
$MESS['VARIABLE_ALIASES_SECTION_ID'] = 'Идентификатор раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE'] = 'Символьный код раздела';
$MESS['VARIABLE_ALIASES_SECTION_CODE_PATH'] = 'Путь из символьных кодов раздела';
?>