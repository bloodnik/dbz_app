<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('EXECUTOR_NAME'),
	'SORT' => 100,
	'DESCRIPTION' => Loc::getMessage('EXECUTOR_DESCRIPTION'),
	'COMPLEX' => 'Y',
	'PATH' => array(
		'ID' => 'appforsale',
		'NAME' => Loc::getMessage('EXECUTOR_PATH_NAME'),
		'SORT' => 1,
		'CHILD' => array(
			'ID' => 'executor',
			'NAME' => Loc::getMessage('EXECUTOR_PATH_CHILD_NAME'),
			'SORT' => 10
		)
	)
);
?>