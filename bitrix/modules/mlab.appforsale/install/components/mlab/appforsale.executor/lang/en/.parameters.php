<?
$MESS["LIST_SETTINGS"] = "Settings list";
$MESS["COMMENT_SETTINGS"] = "Settings reviews";
$MESS["FILTER_SETTINGS"] = "Filter settings";
$MESS["FILTER_SETTINGS_CNT"] = "To display the number in skobkah";

$MESS["IBLOCK_TYPE"] = "The type of InfoBlock";
$MESS["IBLOCK"] = "Infoblok";
$MESS["COMMENT_IBLOCK_TYPE"] = "The type of InfoBlock reviews";
$MESS["COMMENT_IBLOCK"] = "Iblock reviews";
$MESS["SEF_MODE_LIST"] = "The list of performers";
$MESS["SEF_MODE_DETAIL"] = "Details";

$MESS["HIDE_NONAME"] = "Hide users without a name";
$MESS["COMMENT_PROPERTY_CODE"] = "Properties";
?>