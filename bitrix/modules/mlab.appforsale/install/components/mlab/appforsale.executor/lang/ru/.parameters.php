<?
$MESS['LIST_SETTINGS'] = 'Настройки списка';
$MESS['COMMENT_SETTINGS'] = 'Настройки комментариев';
$MESS['FILTER_SETTINGS'] = 'Настройки фильтра';
$MESS['FILTER_SETTINGS_CNT'] = 'Отображать количество в скобках';

$MESS['IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['IBLOCK'] = 'Инфоблок';
$MESS['COMMENT_IBLOCK_TYPE'] = 'Тип инфоблока комментариев';
$MESS['COMMENT_IBLOCK'] = 'Инфоблок комментариев';
$MESS['SEF_MODE_LIST'] = 'Список исполнителей';
$MESS['SEF_MODE_DETAIL'] = 'Детально';

$MESS['HIDE_NONAME'] = 'Скрывать пользователей без имени';
$MESS['COMMENT_PROPERTY_CODE'] = 'Свойства';
?>