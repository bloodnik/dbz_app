<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.executor.filter',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
// 		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
// 		'CACHE_TIME' => $arParams['CACHE_TIME']
		'CNT' => $arParams['CNT']
	),
	$component
);
?>
<div id="list_for_filter">
<?
if (array_key_exists('sections', $_GET))
	$APPLICATION->RestartBuffer();

$APPLICATION->IncludeComponent(
	'mlab:appforsale.executor.map',
	'',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail']
	),
	$component
);
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.executor.list',
	'newview',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['detail']
	),
	$component
);
?>
</div>