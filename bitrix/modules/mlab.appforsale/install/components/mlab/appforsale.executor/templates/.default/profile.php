<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="col-md-8">
	<div class="row">
		<div class="col-md-12">
		<?
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.executor.detail',
			'',
			array(
				'ID' => $arResult['VARIABLES']['USER_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
			),
			$component
		);
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<?
		$dbElement = CIBlockElement::GetByID($arResult['VARIABLES']['ELEMENT_ID']);
		if($arElement = $dbElement->GetNext())
		{
			$arResult['VARIABLES']['SECTION_ID'] = $arElement['IBLOCK_SECTION_ID'];
		}
		
		$APPLICATION->IncludeComponent(
			'mlab:appforsale.executor.profile',
			'',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
				'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'FIELD_CODE' => $arParams['DETAIL_FIELD_CODE'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE']
			),
			$component
		);
		?>
		</div>
	</div>
</div>
<div class="col-md-4">
</div>