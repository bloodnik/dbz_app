<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);

$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ID" => $arParams['SECTION_ID']
		),
		true,
		array(
				"ID",
				"NAME",
				"UF_FORMAT_NAME"
		)
);

$arFilter = array();
if($arSection = $rsSection->GetNext())
{
	$arResult['SECTION'] = $arSection;

	preg_match_all("/#([a-z0-9_]+)#/i", $arResult['SECTION']['UF_FORMAT_NAME'], $matches);
	$arFilter = $matches[1];
}

$nameFormat = CSite::GetNameFormat(true);
$arUserID = array();

$arFilter2 = array(
	"IBLOCK_ID" => $arParams['IBLOCK_ID'],
	"SECTION_ID" => $arParams['SECTION_ID'],
	"ACTIVE" => "Y"
);

global $USER;
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->GetNext(false);
if (!empty($arUser['UF_CITY']))
{
	$arFilter2[] = array(
			"LOGIC" => "OR",
			array("PROPERTY_CITY" => $arUser['UF_CITY']),
			array("PROPERTY_CITY" => false),
	);	
}

global $arrFilter;
$rsProfile = CIBlockElement::GetList(
		array(
			"DATE_CREATE" => "ASC"
		),
		array_merge(
			$arFilter2,
			$arrFilter
		),
		false,
		false,
		array(
			"ID",
			"IBLOCK_ID",
			"CREATED_BY",
			"PROPERTY_*"
		)
);

$input = array();
foreach ($arFilter as $field)
{
	$input[] = '#'.$field.'#';
}

while($obProfile = $rsProfile->GetNextElement())
{
	$arProfile = $obProfile->GetFields();
	$arProfile['PROPERTIES'] = $obProfile->GetProperties();
	
	$arUserID[$arProfile['CREATED_BY']] = true;	
	$arResult['PROFILES'][] = $arProfile;
}

if (!empty($arUserID))
{
	$rsUser = Bitrix\Main\UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
			'filter' => array('ID' => array_keys($arUserID)),
	));

	while ($arUser = $rsUser->fetch())
	{
		$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
		$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

		$arResult['USERS'][$arUser['ID']] = $arUser;
	}
	unset($arUser, $rsUser);
}


foreach ($arResult['PROFILES'] as &$arProfile)
{
	$arProfile['FORMAT_NAME'] = $arResult['USERS'][$arProfile['CREATED_BY']]['FORMAT_NAME'];

	$output = array();
	foreach ($arFilter as $field)
	{
		if (substr($field, 0, 9) == 'PROPERTY_')
		{
			$code = substr($field, 9);
			if ($arProfile['PROPERTIES'][$code]['PROPERTY_TYPE'] == 'E')
			{
				$value = array();
				$res = CIBlockElement::GetList(array(), array("ID"=> $arProfile['PROPERTIES'][$code]['VALUE']));
				while($ar_res = $res->GetNext())
				{
					$value[] = $ar_res['NAME'];
				}
				$output[] = implode(', ', $value);
			}
			else
			{
				$output[] = (is_array($arProfile['PROPERTIES'][$code]['VALUE']) ? implode(', ', $arProfile['PROPERTIES'][$code]['VALUE']) : $arProfile['PROPERTIES'][$code]['VALUE']);
			}
			unset($code);
		}
		else
			$output[] = $arProfile[$field];
	}
	

	if (!empty($arResult['SECTION']['UF_FORMAT_NAME']))
		$arProfile['FORMAT_NAME'] = str_replace($input, $output, $arResult['SECTION']['UF_FORMAT_NAME']);
}


$this->IncludeComponentTemplate();
?>