<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['SECTIONS']))
{
	echo '<ul>';
	foreach ($arResult['SECTIONS'] as $arSection)
	{
		$strMainID = $this->GetEditAreaId($arSection['ID']);
		$arItemIDs = array(
				"SWITCH" => $strMainID."_switch",
				"PROGRESS" => $strMainID."_progress",
		);
		
		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
		
		$subscribe_id = (int) $arSection['SUBSCRIBE'];
		
		echo '<li class="bx_profile_section"';
		if (($arSection['RIGHT_MARGIN'] - $arSection['LEFT_MARGIN']) == 1)
		{
			if ($arResult['MULTIPROFILES'])
				echo ' onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arSection['ID'].'\', title: \''.$arSection['NAME'].'\'})"';
		}
		else
			echo ' onclick="app.loadPageBlank({url: \'?IBLOCK_SECTION_ID='.$arSection['ID'].'\', title: \''.$arSection['NAME'].'\'})"';
		
		echo '>
				<img src="'.($arSection['PICTURE'] ? $arSection['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png').'" class="bx_profile_section_picture" />
				<div class="bx_profile_section_name">'.$arSection['NAME'].((!$arResult['MULTIPROFILES']) ? $price = CAppforsaleRecurring::ToString($arSection) : '').'</div>';
		if (!$arResult['MULTIPROFILES'] && ((($arSection['RIGHT_MARGIN'] - $arSection['LEFT_MARGIN']) == 1) || !empty($price)))
		{
			if ($arResult['isBlock'])
			{
				echo '<img src="'.$templateFolder.'/images/switch_block.png" class="bx_profile_section_switch" />';
			}
			else
			{
				echo '<img id="'.$arItemIDs['SWITCH'].'" src="'.$templateFolder.'/images/switch_'.($subscribe_id > 0 ? "on" : "off").'.png" class="bx_profile_section_switch" />
 				<img id="'.$arItemIDs['PROGRESS'].'" src="'.$templateFolder.'/images/progress.gif" class="bx_profile_section_progress" />';
			}
		}
			echo '</li>';
		
		$arJSParams = array(
				"ID" => $arSection['ID'],
				"SUBSCRIBE_ID" => $subscribe_id,
				"templateFolder" => $templateFolder,
				"VISUAL" => array(
						"SWITCH" => $arItemIDs['SWITCH'],
						"PROGRESS" => $arItemIDs['PROGRESS']
				)
		);
		echo '<script type="text/javascript">var '.$strObName.' = new JCProfileSection('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
	}
	echo '</ul>';
}
?>