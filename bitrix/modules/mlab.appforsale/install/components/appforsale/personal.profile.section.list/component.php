<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized())
{
	CAppforsale::AuthForm();
}

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

CModule::IncludeModule("iblock");

if ($_REQUEST['AJAX_CALL'] == 'Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	$id = intval($_POST['ID']);
	if ($id > 0)
	{
		switch ($_POST['ACTION'])
		{
			case "subscribe":
				$arFields = array(
					"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					"NAME" => "Profile",
					"IBLOCK_SECTION_ID" => $id,
					"CREATED_BY" => $USER->GetID()
				);
				$el = new CIBlockElement;
				if ($product_id = $el->Add($arFields))
				{
					$arRes = array("response" => (int) $product_id);
				}
				else
				{
					$arRes = array("error" => array("error_msg" => $el->LAST_ERROR));
				}
				break;
			case "unsubscribe":
				$subscribe_id = intval($_POST['SUBSCTIBE_ID']);
				if (CIBlockElement::Delete($subscribe_id))
				{
					$arRes = array("response" => 1);
				}
				else
				{
					$arRes = array("error" => array("error_msg" => "error"));
				}
				break;
		}
	}
	die(\Bitrix\Main\Web\Json::encode($arRes));
}


$arResult['isBlock'] = false;
$sections = array();
$IBLOCK_SECTION_ID = intval($_GET['IBLOCK_SECTION_ID']);
if ($IBLOCK_SECTION_ID > 0)
{
	$nav = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $IBLOCK_SECTION_ID);
	while ($arNav = $nav->GetNext())
		$sections[] = $arNav['ID'];

	$rsElement = CIBlockElement::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"SECTION_ID" => $sections,
			"CREATED_BY" => $USER->GetID()
		),
		false,
		false,
		array(
				"ID", "IBLOCK_SECTION_ID"
		)
	);

	if ($arElement = $rsElement->GetNext())
		$arResult['isBlock'] = true;
}



$rsSection = CIBlockSection::GetList(
		array(
			"LEFT_MARGIN"=>"ASC"
		),
		array(
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"SECTION_ID" => (intval($_GET['IBLOCK_SECTION_ID']) > 0 ? $_GET['IBLOCK_SECTION_ID'] : false),
			"ACTIVE" => "Y"
		),
		false,
		array(
			"ID", "NAME", "PICTURE", "DESCRIPTION", "LEFT_MARGIN", "RIGHT_MARGIN", "UF_RECUR_TYPE_TEST", "UF_RECUR_LENGTH_TEST", "UF_RECUR_PRICE_TEST", "UF_RECUR_TYPE", "UF_RECUR_LENGTH", "UF_RECUR_PRICE", "UF_COUNT"
		)	
);

while($arSection = $rsSection->GetNext())
{
	$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::ResizeImageGet($arSection['PICTURE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	$arResult['SECTIONS'][$arSection['ID']] = $arSection;
}

$rsElement = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => array_keys($arResult['SECTIONS']),
				"CREATED_BY" => $USER->GetID()
		),
		false,
		false,
		array(
				"ID", "IBLOCK_SECTION_ID"
		)
);

while ($arElement = $rsElement->GetNext())
{
	$arResult['SECTIONS'][$arElement['IBLOCK_SECTION_ID']]['SUBSCRIBE'] = $arElement['ID'];
}

$arResult['MULTIPROFILES'] = COption::GetOptionString("mlab.appforsale", "multiprofiles", "N") == 'Y';

$this->IncludeComponentTemplate();
?>