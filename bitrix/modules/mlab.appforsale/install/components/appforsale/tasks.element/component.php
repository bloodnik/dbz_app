<?
use Bitrix\Main\UserTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
$nameFormat = CSite::GetNameFormat(true);
CModule::IncludeModule("iblock");

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["OFFER_IBLOCK_ID"] = intval($arParams["OFFER_IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["ELEMENT_ID"] = intval($arParams["ELEMENT_ID"]);



if($_REQUEST['ajax']=='Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();

	if ($_POST['contractor'] > 0)
	{
		$dbOffer = CIBlockElement::GetList(
				array(),
				array(
						"IBLOCK_ID" => $arParams["OFFER_IBLOCK_ID"],
						"ACTIVE" => "Y",
						"PROPERTY_ORDER_ID" => $arParams["ELEMENT_ID"]
				),
				false,
				false,
				array(
						"ID"
				)
		);
		while ($arOffer = $dbOffer->GetNext())
		{
			$bs = new CIBlockElement;
			$arFields = Array(
					"IBLOCK_ID" => $arParams["OFFER_IBLOCK_ID"],
					"ACTIVE" => "N"
			);

			$res = $bs->Update($arOffer['ID'], $arFields);
		}
		
		
		$res = CIBlockElement::SetPropertyValuesEx(
			$arParams["ELEMENT_ID"],
			$arParams["IBLOCK_ID"],
			array(
				"PROFILE_ID" => $_POST['contractor']
			)
		);
	}
	else if ($_POST['USER_ID'] > 0)
	{
		$res = CIBlockElement::SetPropertyValuesEx(
				$arParams["ELEMENT_ID"],
				$arParams["IBLOCK_ID"],
				array(
					"PROFILE_ID" => false
				)
		);
	}
	else if ($_POST['COMPLETE'] == "Y")
	{
		$res = CIBlockElement::SetPropertyValues(
				$arParams["ELEMENT_ID"],
				$arParams["IBLOCK_ID"],
				array(
						"STATUS_ID" => array("VALUE" => "F")
				),
				"STATUS_ID"
		);
	}
	else
	{
		$bs = new CIBlockElement;
		$arFields = Array(
				"IBLOCK_ID" => $arParams["OFFER_IBLOCK_ID"],
				"ACTIVE" => "N"
		);

		$res = $bs->Update($_POST['ID'], $arFields);
	}

	$arResult = array("response" => 1);

	die(\Bitrix\Main\Web\Json::encode($arResult));
}







$arSelect = array(
		"ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"LIST_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE",
		"PROPERTY_*",
);

$rsElement = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => $arParams["ELEMENT_ID"]
		),
		false,
		false,
		$arSelect
);

if($obElement = $rsElement->GetNextElement())
{
	$arResult = $obElement->GetFields();
	$arResult["PROPERTIES"] = $obElement->GetProperties();
}

$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => $arParams["SECTION_ID"],
				"ACTIVE" => "Y"
		),
		true,
		array(
				"ID",
				"NAME",
				"UF_DETAIL_FIELD_CODE"
		)
);

if($arSection = $rsSection->GetNext())
{
	$arResult['SECTION'] = $arSection;
	$arResult['FIELD_CODE'] = $arSection['UF_DETAIL_FIELD_CODE'];
}

if (!empty($arResult['FIELD_CODE']))
{
	foreach ($arResult['FIELD_CODE'] as $field)
	{
		if(substr($field, 0, 9) == 'PROPERTY_')
		{
			$pid = substr($field, 9);
			if (!$arResult['PROPERTIES'][$pid] || empty($arResult['PROPERTIES'][$pid]['VALUE']))
				continue;
			
			if ($arResult['PROPERTIES'][$pid]['PROPERTY_TYPE'] == 'E')
			{
				$value = array();
				$res = CIBlockElement::GetList(array(), array("ID"=> $arResult['PROPERTIES'][$pid]['VALUE']));
				while($ar_res = $res->GetNext())
				{
					$value[] = $ar_res['NAME'];
				}
				$arResult['DISPLAY_FIELDS'][$field] = array(
						"NAME" => $arResult['PROPERTIES'][$pid]['NAME'],
						"VALUE" => $value,
						"PROPERTY_TYPE" => $arResult['PROPERTIES'][$pid]['PROPERTY_TYPE']
				);
			}
			else
			{
				$arResult['DISPLAY_FIELDS'][$field] = array(
						"NAME" => $arResult['PROPERTIES'][$pid]['NAME'],
						"VALUE" => $arResult['PROPERTIES'][$pid]['VALUE'],
						"PROPERTY_TYPE" => $arResult['PROPERTIES'][$pid]['PROPERTY_TYPE'],
						"USER_TYPE" => $arResult['PROPERTIES'][$pid]['USER_TYPE'],
						"USER_TYPE_SETTINGS" => $arResult['PROPERTIES'][$pid]['USER_TYPE_SETTINGS']
				);
			}
		}
		else
		{
			if (!$arResult[$field])
				continue;
				
			$arResult['DISPLAY_FIELDS'][$field] = array(
					"NAME" => "Name",
					"VALUE" => $arResult[$field],
					"PROPERTY_TYPE" => "S"
			);
		}
	}
}

$dbOffer = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["OFFER_IBLOCK_ID"],
				"ACTIVE" => "Y",
				"PROPERTY_TASK_ID" => $arParams["ELEMENT_ID"]
		),
		false,
		false,
		array(
				"ID",
				"PROPERTY_TEXT",
				"CREATED_BY",
				"DATE_CREATE"
		)
);
while ($arOffer = $dbOffer->GetNext())
{
	$arUserID[$arOffer['CREATED_BY']] = true;
	$arResult['OFFERS'][] = $arOffer;
}

if (!empty($arUserID))
{
	$userIterator = UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'PERSONAL_PHOTO'),
			'filter' => array('ID' => array_keys($arUserID)),
	));
	while ($arOneUser = $userIterator->fetch())
	{
		$arOneUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arOneUser);
		$arOneUser['PERSONAL_PHOTO'] = (0 < $arOneUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arOneUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

		$arResult['USERS'][$arOneUser['ID']] = $arOneUser;
	}
	unset($arOneUser, $userIterator);
}


$dbComment = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["COMMENT_IBLOCK_ID"],
				"ACTIVE" => "Y",
				"PROPERTY_TASK_ID" => $arResult['ID'],
				"CREATED_DY" => $USER->GetID()
		),
		false,
		false,
		array(
				"ID"
		)
);
if ($dbComment->GetNext())
{
	$arResult['CAN_COMMENT'] = false;	
}
else
{
	$arResult['CAN_COMMENT'] = true;
}

$this->IncludeComponentTemplate();
?>