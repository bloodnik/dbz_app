<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);
$arParams['ELEMENT_ID'] = intval($arParams['ELEMENT_ID']);

CModule::IncludeModule("iblock");

$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ACTIVE" => "Y",
				"ID" => $arParams['SECTION_ID']
		),
		false,
		array(
				"ID", "UF_EDIT_FIELD_CODE"
		)
);

if($arSection = $rsSection->GetNext())
{
	$arResult['SECTION'] = $arSection;
	$arResult['FIELD_CODE'] = $arSection['UF_EDIT_FIELD_CODE'];
}

$this->IncludeComponentTemplate();
?>