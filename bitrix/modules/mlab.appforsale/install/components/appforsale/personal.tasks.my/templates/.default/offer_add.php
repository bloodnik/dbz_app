<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:edit",
			"",
			array(
				"IBLOCK_ID" => $arParams['OFFER_IBLOCK_ID'],
				"FIELD_CODE" => array(
					"*PROPERTY_TEXT"
				),
				"DATA" => array(
					"PROPERTY_VALUES" => array(
						"TASK_ID" => $arResult['VARIABLES']['ELEMENT_ID']
					)
				)
			),
			false
	);
?>