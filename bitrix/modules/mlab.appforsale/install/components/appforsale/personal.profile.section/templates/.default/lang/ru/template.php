<?
$MESS['LIST_EMPTY'] = 'Список пуст';
$MESS['ADD_PROFILE'] = 'Добавить профиль';	
$MESS['EDIT'] = 'Редактировать';
$MESS['PAY'] = 'Оплатить';

$MESS['EDIT_PROFILE'] = 'Редактировать профиль';
$MESS['CANCEL_PROFILE'] = 'Удалить профиль';
$MESS['CLOSE'] = 'Закрыть';
$MESS['FREE'] = 'Бесплатный';
$MESS['ACTIVE_TO'] = 'Активен до';
$MESS['NOT_ACTIVE'] = 'Не активен';
?>