<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);


if ($_REQUEST['ajax']=='Y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();

	$bs = new CIBlockElement;
	$arFields = Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "N"
	);
	$res = $bs->Update($_POST['ID'], $arFields);
	$arResult = array("response" => 1);

	die(\Bitrix\Main\Web\Json::encode($arResult));
}


$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ID" => $arParams['SECTION_ID']
		),
		true,
		array(
				"ID",
				"NAME",
				"UF_FORMAT_NAME",
				"UF_RECUR_TYPE_TEST",
				"UF_RECUR_LENGTH_TEST",
				"UF_RECUR_PRICE_TEST",
				"UF_RECUR_TYPE",
				"UF_RECUR_LENGTH",
				"UF_RECUR_PRICE",
				"UF_COUNT"
		)
);

$arFilter = array();
if($arSection = $rsSection->GetNext())
{
	$arSection['FORMAT_NAME'] = CAppforsaleRecurring::ToString($arSection);
	$arResult['SECTION'] = $arSection;

	preg_match_all("/#([a-z0-9_]+)#/i", $arResult['SECTION']['UF_FORMAT_NAME'], $matches);
	$arFilter = $matches[1];
}



$nameFormat = CSite::GetNameFormat(true);
$arUserID = array();



$rsProfile = CIBlockElement::GetList(
		array(
				"DATE_CREATE" => "ASC"
		),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arParams['SECTION_ID'],
				"ACTIVE" => "Y",
				"CREATED_BY" => $USER->GetID()
		),
		false,
		false,
		array(
			"ID",
			"NAME",
			"IBLOCK_ID",
			"CREATED_BY",
			"ACTIVE_TO",
			"PROPERTY_*"
		)
);

$input = array();
foreach ($arFilter as $field)
{
	$input[] = '#'.$field.'#';
}

while($obProfile = $rsProfile->GetNextElement())
{
	$arProfile = $obProfile->GetFields();
	$arProfile['PROPERTIES'] = $obProfile->GetProperties();
	
	$arUserID[$arProfile['CREATED_BY']] = true;	
	$arResult['PROFILES'][] = $arProfile;
}

if (!empty($arUserID))
{
	$rsUser = Bitrix\Main\UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
			'filter' => array('ID' => array_keys($arUserID)),
	));

	while ($arUser = $rsUser->fetch())
	{
		$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
		$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

		$arResult['USERS'][$arUser['ID']] = $arUser;
	}
	unset($arUser, $rsUser);
}

$this->IncludeComponentTemplate();
?>