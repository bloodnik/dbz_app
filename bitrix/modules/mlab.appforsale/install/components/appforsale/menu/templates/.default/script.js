/* appforsale:menu */
(function (window) {
	
	if (!!window.JCMenu)
	{
		return;
	}
	
	window.JCMenu = function(arParams)
	{		
		this.ajax = false;
		this.desktop = false;
		this.currentItem = null;
		this.background_color_bar = null;
		this.title_color_bar = null;
		
		this.visual = {
				HEADER: '',
				ITEMS: ''
		};
		
		this.obHeader = null;
		this.obItems = null;
		
		if ('object' === typeof arParams)
		{
			this.ajax = arParams.AJAX;
			this.currentItem = arParams.currentItem;
			
			this.background_color_bar = arParams.BACKGROUND_COLOR_BAR;
			this.title_color_bar = arParams.TITLE_COLOR_BAR;
			
			this.visual = arParams.VISUAL;
			this.desktop = arParams.DESKTOP;
		}

		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCMenu.prototype.Init = function()
	{
		this.obHeader = BX(this.visual.HEADER);
		this.obItems = BX(this.visual.ITEMS);
			
		BX.bind(this.obItems, 'click', BX.delegate(this.onItemClick, this));

		
		if (this.desktop == false || this.ajax == false)
		{
			BX.addCustomEvent("OnAfterUserRegister", BX.delegate(updateUser, this));
			BX.addCustomEvent("OnAfterUserLogin", BX.delegate(updateUser, this));
			BX.addCustomEvent("OnAfterUserUpdate", BX.delegate(updateUser, this));
			BX.addCustomEvent("OnAfterUserLogout", BX.delegate(updateUser, this));
			
			app.enableMenu(true);
				
			app.setColors(
				{
					background: this.background_color_bar,
					titleText: this.title_color_bar
				}
			);	
				
			app.getToken(this.registerDevice);
		}
	}

	window.JCMenu.prototype.registerDevice = function(data)
	{
		BX.ajax({
	        url: location.href,
	        data: {
	        	ajax_call: 'y',
	        	DEVICE_ID: data.DEVICE_ID,
	        	DEVICE_TYPE: data.DEVICE_TYPE,
	        	DEVICE_NAME: data.DEVICE_NAME,
	        	DEVICE_MODEL: data.DEVICE_MODEL,
	        	SYSTEM_VERSION: data.SYSTEM_VERSION,
	        	TOKEN: data.TOKEN
			},
	        method: 'POST'
	    });	
	}
	
	window.JCMenu.prototype.onItemClick = function(event)
	{
		var target = event.target;
		while (target != this.obItems)
		{
			if (target && target.nodeType && target.nodeType == 1 && BX.hasClass(target, "bx_menu_item"))
			{
				if (this.currentItem != null)
					this.unselectItem(this.currentItem);

				this.selectItem(target);

				var url = target.getAttribute("data-url");
				var title = target.getAttribute("data-title");
				if(BX.type.isNotEmptyString(url))
				{
					app.closeMenu();
					app.loadPageStart({url: url, title: title});
				}
				this.currentItem = target;
				return;
			}
			target = target.parentNode;
		}
	}
	
	window.JCMenu.prototype.selectItem = function(item)
	{
		if (!BX.hasClass(item, "bx_menu_item_selected"))
			BX.addClass(item, "bx_menu_item_selected");
	}
	
	window.JCMenu.prototype.unselectItem = function(item)
	{
		BX.removeClass(item,"bx_menu_item_selected");
	}
		
	function updateUser(data)
	{
		BX.ajax.Setup(
			{
				denyShowWait: true
			},
			true
		);  
		BX.ajax.insertToNode(
			location.href,
			document.body
		);
	}
	
	window.setLocation = function(lat, lng)
	{
		BX.ajax({
	        url: "/youdo/api/setLocation.php",
	        data: {
	        	lat: lat,
	        	lng: lng
			},
	        method: 'POST'
	    });	
	}
	
})(window);