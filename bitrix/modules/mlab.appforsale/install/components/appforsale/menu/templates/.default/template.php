<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId("menu");
$arItemIDs = array(
		"HEADER" => $strMainID."_header",
		"ITEMS" => $strMainID."_items"
);

echo '<div class="bx_menu_header" id="'.$arItemIDs['HEADER'].'"'.(!empty($arParams['BACKGROUND_COLOR_MENU']) ? ' style="background: '.$arParams['BACKGROUND_COLOR_MENU'].'"' : '').'>';
if ($arResult['USER'])
{
	echo '<div class="bx_menu_user" onclick="app.closeMenu();  app.loadPageStart({url:\''.$arParams['PATH_TO_PROFILE'].'\', title: \''.GetMessage('SETTING').'\'})">
			<img class="bx_menu_user_personal_photo" src="'.($arResult['USER']['PERSONAL_PHOTO'] ? $arResult['USER']['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/user_no_photo.png').'" />
					<div class="bx_menu_user_name">'.$arResult['USER']['FORMAT_NAME'].'</div>
			</div>
			</a>';
}
else
{
	echo '<div class="bx_menu_login" onclick="app.closeMenu(); app.loadPageStart({url:\''.$arParams['PATH_TO_PROFILE'].'\', title: \''.GetMessage('SETTING').'\'})">'.GetMessage('AUTHORIZATION').'</div>';
}
echo '</div>';

echo '<div class="bx_menu_items" id="'.$arItemIDs['ITEMS'].'">';
$htmlMenu = "";
foreach ($arResult["MENU"] as $arMenuSection)
{
	if(!isset($arMenuSection['type']) && $arMenuSection['type'] != "section")
		continue;
	
	if(!isset($arMenuSection['items']) || !is_array($arMenuSection['items']))
		continue;

	$htmlMenu .= '<div class="bx_menu_section">';

	if (isset($arMenuSection['text']))
		$htmlMenu .= '<div class="bx_menu_separator">'.$arMenuSection['text'].'</div>';
	
	foreach ($arMenuSection['items'] as $arMenuItem)
	{
		$htmlMenu .= '<div class="bx_menu_item';

		if(isset($arMenuItem["class"]))
			$htmlMenu .= ' '.$arMenuItem["class"];

		$htmlMenu .= '"';

		if (!isset($arMenuItem['data-title']))
		{
			$htmlMenu .= ' data-title="'.$arMenuItem['text'].'"';
		}
		
		foreach ($arMenuItem as $attrName => $attrVal)
		{
			if($attrName == 'text' || $attrName == 'type' || $attrName == 'class' || $attrName == 'icon')
				continue;

			$htmlMenu .= ' '.$attrName.'="'.$attrVal.'"';
		}
		
		$htmlMenu .= '>';

		if(isset($arMenuItem['icon']))
			$htmlMenu .= '<div class="bx_menu_item_image"><img class="bx_menu_item_img" src="'.$arMenuItem['icon'].'" /></div>';
		
		if(isset($arMenuItem['text']))
			$htmlMenu .= '<div class="bx_menu_item_name">'.$arMenuItem['text'].'</div>';

		$htmlMenu .= '</div>';
	}
	$htmlMenu .= '</div>';
}
echo $htmlMenu;
echo '</div>';

$arJSParams = array(
		"currentItem" => null,
		"AJAX" => Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest(),
		"VISUAL" => array(
				"HEADER" => $arItemIDs['HEADER'],
				"ITEMS" => $arItemIDs['ITEMS']
		),
		"DESKTOP" => (empty($arParams['DESKTOP']) ? false : true),
		"BACKGROUND_COLOR_MENU" => $arParams['BACKGROUND_COLOR_MENU'],
		"BACKGROUND_COLOR_BAR" => $arParams['BACKGROUND_COLOR_BAR'],
		"TITLE_COLOR_BAR" => $arParams['TITLE_COLOR_BAR']	
);	
echo '<script type="text/javascript">var menu = new JCMenu('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>