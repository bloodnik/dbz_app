<?
use Bitrix\Main\UserTable,
	Mlab\Appforsale\Design,
	Bitrix\Main\Config\Option;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(true);

if ($_REQUEST['ajax_call'] == 'y')
{
	$APPLICATION->RestartBuffer();
	CUtil::JSPostUnescape();
	
	$user_id = 'NULL';
	$date_auth = 'NULL';
	if ($USER->IsAuthorized())
	{
		$user_id = $USER->GetID();
		$date_auth = 'NOW()';
	}
	
	$DB -> Query(
			'INSERT INTO appforsale_device
				(DATE_CREATE, TIMESTAMP_X, DEVICE_ID, DEVICE_TYPE, DEVICE_NAME, DEVICE_MODEL, SYSTEM_VERSION, SETTINGS, TOKEN, USER_ID, DATE_AUTH)
				VALUES
				(NOW(), NOW(), "' . $DB->ForSql($_POST['DEVICE_ID']) . '", "' . $DB->ForSql($_POST['DEVICE_TYPE']) . '", "' . $DB->ForSql($_POST['DEVICE_NAME']) . '", "' . $DB->ForSql($_POST['DEVICE_MODEL']) . '", "' . $DB->ForSql($_POST['SYSTEM_VERSION']) . '", "' . $DB->ForSql($_POST['SETTINGS']) . '", "' . $DB->ForSql($_POST['TOKEN']) . '", ' . $user_id . ', ' . $date_auth . ')
				on duplicate key update
				TIMESTAMP_X = NOW(),
				DEVICE_NAME = "' . $DB->ForSql($_POST['DEVICE_NAME']) . '",
				SYSTEM_VERSION = "' . $DB->ForSql($_POST['SYSTEM_VERSION']) . '",
				SETTINGS = "' . $DB->ForSql($_POST['SETTINGS']) . '",
				TOKEN = "' . $DB->ForSql($_POST['TOKEN']) . '",
				USER_ID = ' . $user_id . ',
				DATE_AUTH = ' . $date_auth . '
				');
	die();
}

$arParams['PATH_TO_LOGIN'] = trim($arParams['PATH_TO_LOGIN']);
$arParams['PATH_TO_PROFILE'] = trim($arParams['PATH_TO_PROFILE']);
$arParams['PATH_TO_FILE_MENU'] = trim($arParams['PATH_TO_FILE_MENU']);

$base_color = Option::get('mlab.appforsale', SITE_TEMPLATE_ID.'_base_color', '#3f51b5');

$arParams['BACKGROUND_COLOR_MENU'] = isset($arParams['BACKGROUND_COLOR_MENU']) ? trim($arParams['BACKGROUND_COLOR_MENU']) : $base_color;
$arParams['BACKGROUND_COLOR_BAR'] = isset($arParams['BACKGROUND_COLOR_BAR']) ? trim($arParams['BACKGROUND_COLOR_BAR']) : $base_color;
$arParams['TITLE_COLOR_BAR'] = isset($arParams['TITLE_COLOR_BAR']) ? trim($arParams['TITLE_COLOR_BAR']) : (Design::button($base_color) == '#fff' ? '#ffffff' : '#000000');

$arResult['USER'] = false;
if ($USER->IsAuthorized())
{
	$nameFormat = CSite::GetNameFormat(true);
	$rsUser = UserTable::getList(
			array(
				'select' => array(
						'ID',
						'NAME',
						'LAST_NAME',
						'SECOND_NAME',
						'PERSONAL_PHOTO'
				),
				'filter' => array(
						'ID' => $USER->GetID()
				)
			)
	);
	if ($arUser = $rsUser->fetch())
	{
		$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
		$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 128, 'height' => 128), BX_RESIZE_IMAGE_EXACT, true) : false);
		$arResult['USER'] = $arUser;
	}
	unset($arUser, $rsUser);
}

global $arMenu;
require($_SERVER["DOCUMENT_ROOT"] . $arParams['PATH_TO_FILE_MENU']);
$arResult['MENU'] = CAppforsale::buildMenu();

$this->IncludeComponentTemplate();
?>