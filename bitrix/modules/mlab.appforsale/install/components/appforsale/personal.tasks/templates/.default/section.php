<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:personal.tasks.section",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"PROFILE_IBLOCK_ID" => $arParams['PROFILE_IBLOCK_ID'],
				"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID']
			),
			$component
	);
?>