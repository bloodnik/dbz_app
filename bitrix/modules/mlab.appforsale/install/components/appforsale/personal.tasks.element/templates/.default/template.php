<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

echo '<div style="padding-bottom: 50px">';

if (!empty($arResult['DISPLAY_FIELDS']))
{
	echo '<ul class="bx_field_list">';
	foreach ($arResult['DISPLAY_FIELDS'] as $field)
	{
		echo '<li class="bx_field_list">';
		echo '<div class="bx_name">'.$field['NAME'].'</div>';
echo '<div class="bx_value">';
		if ($field['PROPERTY_TYPE'] == 'F')
		{
			if (!is_array($field['VALUE']))
				$field['VALUE'] = array($field['VALUE']);
			foreach ($field['VALUE'] as $file_id)
			{
				$arFile = CFile::ResizeImageGet($file_id, array('width' => 128, 'height' => 128), BX_RESIZE_IMAGE_EXACT, true);
		
				echo '<img src="'.$arFile['src'].'" />';
			}
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'map_google')
		{
			echo '<img border="0" style="width: 100%;" src="https://maps.googleapis.com/maps/api/staticmap?key='.$field['USER_TYPE_SETTINGS']['API_KEY'].'&size=480x250&center='.$field['VALUE'].'&zoom=16&markers=color:red%7C%7C'.$field['VALUE'].'"  />';
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'HTML')
		{
			if ($field['VALUE']['TYPE'] == 'HTML')
				echo htmlspecialcharsBack(FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']));
			else 
				echo FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']);
		}
		else
			echo (is_array($field['VALUE']) ? implode(', ', $field['VALUE']) : $field['VALUE']);
		
		echo '</div>';
		echo '</li>';
	}
	echo '</ul>';
}

$arUser = $arResult['USERS'][$arResult['CREATED_BY']];
echo '<div class="bx_order_offer_header">'.GetMessage('CLIENT').'</div>';
echo '
				<div class="bx_order_offer">
					<img src="'.($arUser['PERSONAL_PHOTO'] ? $arUser['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_order_offer_photo" />
					<div class="bx_order_offer_info">
						<div class="bx_order_offer_name">'.$arUser['FORMAT_NAME'].'</div>
						<div class="bx_order_offer_date">';
								
				if (!empty($arUser['PERSONAL_MOBILE']) && (($arResult['SECTION']['UF_OFFER_PRICE'] == 0) || ($arResult['PROPERTIES']['PROFILE_ID']['VALUE'] == $USER->GetID())))
					echo '<a class="btn btn-primary call" href="tel:'.$arUser['PERSONAL_MOBILE'].'">'.GetMessage('CALL').'</a>';
				else
					echo '<a class="btn btn-primary call disabled">'.GetMessage('CALL').'</a>';	
				echo '</div>';
								
								
								echo '</div>
					</div>';

echo '</div>';


echo '</div>';


if (!empty($arResult['OFFERS']))
{
	echo '<div class="bx_order_offers" id="bx_offers">';
	echo '<div class="bx_order_offer_header">'.GetMessage('OFFER').'</div>';
	echo '<ul>';
	foreach ($arResult['OFFERS'] as $key => $arOrder)
	{
		$user = $arResult['USERS'][$arOrder['CREATED_BY']];
	
		echo '<li class="bx_order_offer2">
				<div class="bx_order_offer">
					<img src="'.($user['PERSONAL_PHOTO'] ? $user['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_order_offer_photo" />
					<div class="bx_order_offer_info">
						<div class="bx_order_offer_name">'.$user['FORMAT_NAME'].'</div>
						<div class="bx_order_offer_date">'.$arOrder['DATE_CREATE'].'</div>
					</div>
				<div class="bx_order_offer_detail_text">'.$arOrder['PROPERTY_TEXT_VALUE'].'</div>';

		echo '</div></li>';
	
	}
	echo '</ul>';
	echo '</div>';
}
else
{
	if (intval($arResult["PROPERTIES"]['PROFILE_ID']['VALUE']) == 0)
		echo '<div class="btn btn-primary bx_offer_add" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arParams['SECTION_ID'].'&ELEMENT_ID='.$arParams["ELEMENT_ID"].'&ACTION=offer_add\', title: \''.GetMessage('OFFER_ADD').'\'})">'.GetMessage('OFFER_ADD').(intval($arResult['SECTION']['UF_OFFER_PRICE']) > 0 ? ' ('.AppforsaleFormatCurrency($arResult['SECTION']['UF_OFFER_PRICE']).')' : '').'</div>';
}
?>