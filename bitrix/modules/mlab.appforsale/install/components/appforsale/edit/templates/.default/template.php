<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['DISPLAY_FIELDS']))
{
	$strMainID = $this->GetEditAreaId("edit");
	$arItemIDs = array(
		"BUTTON" => $strMainID."_button"
	);
	
	$fields = array();
	echo '<div class="bx_form">';
	foreach ($arResult['DISPLAY_FIELDS'] as $arField)
	{
		$fields[] = $arField;
		echo CAppforsale::getFieldHtml($arField);
	}
	echo '<div class="bx_form_control"><button id="'.$arItemIDs['BUTTON'].'">'.(!empty($arParams['MESS_BTN_SAVE']) ? $arParams['MESS_BTN_SAVE'] : GetMessage('SAVE')).'</button>';
	echo '</div></div>';
	$arJSParams = array(
			"FIELDS" => $fields,
			"VISUAL" => array(
				"BUTTON" => $arItemIDs['BUTTON']
			)
	);
	echo '<script type="text/javascript">var edit = new JCEdit('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
	
}
?>