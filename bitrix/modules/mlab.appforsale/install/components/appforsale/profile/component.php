<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arVariables = array(
	"SECTION_ID" => $_GET['SECTION_ID'],
	"ELEMENT_ID" => $_GET['ELEMENT_ID'],
	"USER_ID" => $_GET['USER_ID'],
	"ACTION" => $_GET['ACTION']
);

if (isset($arVariables["ACTION"]) && $arVariables["ACTION"] == 'map')
{
	$componentPage = 'map';
}
elseif (isset($arVariables["USER_ID"]) && intval($arVariables["USER_ID"]) > 0)
{
	$componentPage = 'user';
}
elseif (isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0)
{
	$componentPage = 'element';
}
elseif (isset($arVariables["SECTION_ID"]) && intval($arVariables["SECTION_ID"]) > 0)
{
	$componentPage = 'section';
}
else
{
	$componentPage = 'sections';	
}

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate($componentPage);
?>