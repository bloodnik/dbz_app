<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:profile.map",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
				"MARKER_ICON" => $arParams['MAP_MARKER_ICON'],
			),
			$component
	);
?>