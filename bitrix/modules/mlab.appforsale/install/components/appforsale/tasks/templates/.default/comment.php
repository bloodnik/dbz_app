<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (COption::GetOptionString("mlab.appforsale", "comment_check_rating", "N") != "Y")
{
	COption::SetOptionString("mlab.appforsale", "comment_check_rating", "Y");
	CModule::IncludeModule("iblock");
	$ibp = new CIBlockProperty;
	$ibp->Add(
		array(
			"NAME" => GetMessage('RATING'),
			"CODE" => "RATING",
			"PROPERTY_TYPE" => "N",
			"IBLOCK_ID" => $arParams['COMMENT_IBLOCK_ID']
		)
	);
}
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:edit",
			"",
			array(
				"IBLOCK_ID" => $arParams['COMMENT_IBLOCK_ID'],
				"FIELD_CODE" => array(
					"*PROPERTY_TEXT",
					"PROPERTY_RATING"
				),
				"DATA" => array(
					"PROPERTY_VALUES" => array(
						"PROFILE_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
						"TASK_ID" => $arResult['VARIABLES']['TASK_ID']
					)
				)
			),
			false
	);
?>