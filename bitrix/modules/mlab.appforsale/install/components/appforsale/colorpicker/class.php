<?
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class ColorPicker extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}
		
	public function executeComponent()
	{		
		//if($this->startResultCache())
		//{
			//$this->arResult["ID"] = $this->var1();
			$this->includeComponentTemplate();
		//}
		//return $this->arResult["ID"];
	}
}