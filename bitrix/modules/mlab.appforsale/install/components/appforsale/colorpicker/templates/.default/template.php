<?
use Mlab\Appforsale\Design;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$strMainID = $this->GetEditAreaId("colorpicker");
$arItemIDs = array(
		"VALUE" => $strMainID."_value",
		"ITEMS" => $strMainID."_items",
		"CUSTOM" => $strMainID."_custom",
		"CUSTOM_COLOR" => $strMainID."_custom_color"
);

$palette = array(
	array('NAME' => 'Red', 'HEX' => '#f44336', 'COLOR' => '#ffffff'),
	array('NAME' => 'Pink', 'HEX' => '#e91e63', 'COLOR' => '#ffffff'),
	array('NAME' => 'Purple', 'HEX' => '#9c27b0', 'COLOR' => '#ffffff'),
	array('NAME' => 'Deep Purple', 'HEX' => '#673ab7', 'COLOR' => '#ffffff'),
	array('NAME' => 'Indigo', 'HEX' => '#3f51b5', 'COLOR' => '#ffffff'),
	array('NAME' => 'Blue', 'HEX' => '#2196f3', 'COLOR' => '#ffffff'),
	array('NAME' => 'Light Blue', 'HEX' => '#03a9f4', 'COLOR' => '#000000'),
	array('NAME' => 'Cyan', 'HEX' => '#00bcd4', 'COLOR' => '#000000'),
	array('NAME' => 'Teal', 'HEX' => '#009688', 'COLOR' => '#ffffff'),
	array('NAME' => 'Green', 'HEX' => '#4caf50', 'COLOR' => '#000000'),
	array('NAME' => 'Light Green', 'HEX' => '#8bc34a', 'COLOR' => '#000000'),
	array('NAME' => 'Lime', 'HEX' => '#cddc39', 'COLOR' => '#000000'),
	array('NAME' => 'Yellow', 'HEX' => '#ffeb3b', 'COLOR' => '#000000'),
	array('NAME' => 'Amber', 'HEX' => '#ffc107', 'COLOR' => '#000000'),
	array('NAME' => 'Orange', 'HEX' => '#ff9800', 'COLOR' => '#000000'),
	array('NAME' => 'Deep Orange', 'HEX' => '#ff5722', 'COLOR' => '#000000'),
	array('NAME' => 'Brown', 'HEX' => '#795548', 'COLOR' => '#ffffff'),
	array('NAME' => 'Grey', 'HEX' => '#9e9e9e', 'COLOR' => '#000000'),
	array('NAME' => 'Blue Grey', 'HEX' => '#607d8b', 'COLOR' => '#ffffff')
);
?>
<input type="hidden" id="<?=$arItemIDs['VALUE']?>" name="<?=$arParams['NAME']?>" value="<?=$arParams['VALUE']?>" />
<div id="<?=$arItemIDs['ITEMS']?>" class="afs-colorpicker-items">
	<?foreach ($palette as $arColor):?>
		<div class="afs-colorpicker-item" data-color="<?=$arColor['HEX']?>">
			<div class="afs-colorpicker-item-color" style="background-color: <?=$arColor['HEX']?>">
				<div class="afs-colorpicker-name" style="color: <?=$arColor['COLOR']?>"><?=$arColor['NAME']?></div>
			</div>
		</div>
	<?endforeach;?>
	<div id="<?=$arItemIDs['CUSTOM']?>" class="afs-colorpicker-item custom">
		<div id="<?=$arItemIDs['CUSTOM_COLOR']?>" class="afs-colorpicker-item-color" style="background-color: #dedede">
			<div class="afs-colorpicker-name">Custom</div>
		</div>
	</div>
</div>
<?
$arJSParams = array(
		'VISUAL' => array(
			'VALUE' => $arItemIDs['VALUE'],
			'ITEMS' => $arItemIDs['ITEMS'],
			'CUSTOM' => $arItemIDs['CUSTOM'],
			'CUSTOM_COLOR' => $arItemIDs['CUSTOM_COLOR']
		),
);

echo '<script type="text/javascript">new JSColorPicker('.CUtil::PhpToJSObject($arJSParams, false, true).');</script>';
?>