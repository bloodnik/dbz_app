<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);

$rsGuide = CIBlockElement::GetList(
		array(
				"ID" => "DESC"
		),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arParams['SECTION_ID']
		),
		false,
		false,
		array()
);
while($arGuide = $rsGuide->GetNext())
{
	$arResult['ELEMENTS'][] = $arGuide;
}

$this->IncludeComponentTemplate();
?>