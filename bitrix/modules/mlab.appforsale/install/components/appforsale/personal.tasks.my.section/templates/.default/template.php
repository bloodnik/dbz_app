<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['TASKS']))
{
	echo '<ul>';
	foreach ($arResult['TASKS'] as $key => $arTask)
	{
		$arUser = $arResult['USERS'][$arTask['CREATED_BY']];
		echo '<li class="bx_profile" onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arTask['IBLOCK_SECTION_ID'].'&ELEMENT_ID='.$arTask['ID'].'\', title: \''.$arTask['FORMAT_NAME'].'\'})">
				<div class="bx_profile_name">'.$arTask['FORMAT_NAME'].'</div>
				<div class="bx_desc">#'.$arTask['ID'].'</div>
				<img class="bx_profile_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />
		
						</li>';
	}
	unset($arUser);
	echo '</ul>';
}
else
{
	echo '<div class="bx_task_empty">'.GetMessage('LIST_EMPTY').'</div>';
}
?>