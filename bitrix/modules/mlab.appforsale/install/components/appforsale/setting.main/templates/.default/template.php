<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arItemIDs = array(
	"HEADER" => "header",
	"SWITCH" => "switch",
	"PROGRESS" => "progress2"
);

echo '<div class="header2" id="'.$arItemIDs['HEADER'].'">';
echo '<div style="cursor: pointer" class="personal_photo" onclick="app.showFileChooser(this, uploadfile2)">
		<div class="load" id="progress"></div>
		<canvas class="canvas" id="canvas"></canvas>
		<img src="'.($arResult["arUser"]['PERSONAL_PHOTO'] ? $arResult["arUser"]['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/user_no_photo.png').'" width="96" height="96" />
	</div>
	<input class="personal_photo_file" type="file" onchange="uploadfile(this)" />
	
		<div class="name">'.$arResult['arUser']['FORMAT_NAME'].'</div>
		<div style="cursor: pointer" class="profile" onclick="app.loadPageBlank({url: \'/youdo/personal/profile/edit.php\', title: \''.GetMessage('PROFILE').'\'})">'.GetMessage('EDIT').'</div>
	</div>
				<ul>';

if ($arParams['USE_SWITCH'] == 'Y'):
	$isContractor = CSite::InGroup(array($arParams['SWITCH_GROUP']));
?>
	<li style="cursor: pointer" class="bx_profile_section">
		<div class="bx_profile_section_name"><?=GetMessage('CONTRACTOR')?></div>
		<img id="<?=$arItemIDs['SWITCH']?>" src="<?=$templateFolder?>/images/switch_<?=($isContractor ? "on" : "off")?>.png" class="bx_profile_section_switch" />
 		<img id="<?=$arItemIDs['PROGRESS']?>" src="<?=$templateFolder?>/images/progress.gif" class="bx_profile_section_progress" />
	</li>
<?
endif;


if ($arParams['USE_SUBSCRIPTION'] == 'Y' && $USER->GetLogin() != COption::GetOptionString('mlab.appforsale', 'login_approved', '79270270273')):
?>
	<li style="cursor: pointer" class="bx_setting_item" onclick="app.loadPageBlank({url: '/youdo/subscription/', title: '<?=GetMessage('SUBSCRIPTION')?>'})">
		<img src="<?=$templateFolder?>/images/subscription.png" class="bx_setting_picture" />
		<div class="bx_setting_name"><?=GetMessage('SUBSCRIPTION')?></div>
		<img class="bx_setting_section_indicator" src="<?=$templateFolder?>/images/section_indicator.png" />					
	</li>
<?
endif;




echo '
		<li style="cursor: pointer" class="bx_setting_item" onclick="app.loadPageBlank({url: \'?ITEM=notify\', title: \''.GetMessage('NOTICE').'\'})">
			<img src="'.$templateFolder.'/images/notify.png" class="bx_setting_picture" />
			<div class="bx_setting_name">'.GetMessage('NOTICE').'</div>
			<img class="bx_setting_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />					
		</li>';

	if ($USER->GetLogin() != COption::GetOptionString('mlab.appforsale', 'login_approved', '79270270273'))
		echo '<li style="cursor: pointer" class="bx_setting_item" onclick="app.loadPageBlank({url: \'?ITEM=account\', title: \''.GetMessage('BALANCE').'\'})">
			<img src="'.$templateFolder.'/images/balance.png" class="bx_setting_picture" />
			<div class="bx_setting_name">'.GetMessage('BALANCE').'</div>
			<img class="bx_setting_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />					
		</li>';
					
		echo '<li style="cursor: pointer" class="bx_setting_item" onclick="app.loadPageBlank({url: \'?ITEM=agreement\', title: \''.GetMessage('AGREEMENT').'\'})">
			<img src="'.$templateFolder.'/images/agreement.png" class="bx_setting_picture" />
			<div class="bx_setting_name">'.GetMessage('AGREEMENT').'</div>
			<img class="bx_setting_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />					
		</li>

					
		<li style="cursor: pointer" class="bx_setting_item exit" onclick="return Logout(this)">
			'.GetMessage('EXIT').'
		</li>
	</ul>';

$arJSParams = array(
	"templateFolder" => $templateFolder,
	"isContractor" => $isContractor,
	"AJAX" => Bitrix\Main\Context::getCurrent()->getRequest()->isAjaxRequest(),
	"VISUAL" => $arItemIDs
);

echo '<script type="text/javascript">new JCPersonal('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>