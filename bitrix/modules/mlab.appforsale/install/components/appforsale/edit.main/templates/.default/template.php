<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['DISPLAY_FIELDS']))
{
	$strMainID = $this->GetEditAreaId("edit");
	$arItemIDs = array(
		"BUTTON" => $strMainID."_button"
	);
	
	$fields = array();
	echo '<div class="bx_form" '.(!empty($arResult['TYPICAL_OFFER']) ? 'style="display: none"' : '').'>';
	foreach ($arResult['DISPLAY_FIELDS'] as $arField)
	{
		global $_ID;
		$_ID = $arParams['ELEMENT_ID'];
		$fields[] = $arField;
		echo CAppforsale::getFieldHtml($arField);
	}
	
	echo '<div class="bx_form_control"><button id="'.$arItemIDs['BUTTON'].'" class="btn btn-primary">'.(!empty($arParams['MESS_BTN_SAVE']) ? $arParams['MESS_BTN_SAVE'] : GetMessage('SAVE')).'</button>';
	echo '</div></div>';
	$arJSParams = array(
			"FIELDS" => $fields,
			"VISUAL" => array(
				"BUTTON" => $arItemIDs['BUTTON']
			)
	);
	echo '<script type="text/javascript">var edit = new JCEdit('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
	
	
	if (!empty($arResult['TYPICAL_OFFER']))
	{
		foreach ($arResult['TYPICAL_OFFER'] as $arOffer)
		{
			echo '<button onclick="BX(\'PROPERTY_TEXT\').value = \''.$arOffer.'\' ; BX(\''.$arItemIDs['BUTTON'].'\').click()">'.$arOffer.'</button>';
		}
	}

	
	
	
}
?>