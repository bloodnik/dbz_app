<?
$MESS ['STA_TPAY'] = "Оплата заказа";
$MESS ['STA_TPAY_PART'] = "Частичная оплата заказа";
$MESS ['STA_TFROM_CARD'] = "Внесение денег с пластиковой карты";
$MESS ['STA_TMONEY'] = "Внесение денег";
$MESS ['STA_TCANCEL_ORDER'] = "Отмена оплаченности заказа";
$MESS ['STA_TCANCEL_SEMIORDER'] = "Отмена частично оплаченного заказа";
$MESS ['STA_THAND'] = "Ручное изменение счета";
$MESS ['STA_TDEL'] = "Удаление счета";
$MESS ['STA_TTRANSF_EXCESS_SUM_PAID'] = "Перевод излишка оплаты заказа";
$MESS ['STA_TRETURN'] = "Возврат средств оплаты заказа";
?>