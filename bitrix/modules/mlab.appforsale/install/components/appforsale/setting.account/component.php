<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arTransactTypes = array(
	"ORDER_PAY" => GetMessage("STA_TPAY"),
	"ORDER_PAY_PART" => GetMessage("STA_TPAY_PART"),
	"CC_CHARGE_OFF" => GetMessage("STA_TFROM_CARD"),
	"OUT_CHARGE_OFF" => GetMessage("STA_TMONEY"),
	"ORDER_UNPAY" => GetMessage("STA_TCANCEL_ORDER"),
	"ORDER_CANCEL_PART" => GetMessage("STA_TCANCEL_SEMIORDER"),
	"MANUAL" => GetMessage("STA_THAND"),
	"DEL_ACCOUNT" => GetMessage("STA_TDEL"),
	"AFFILIATE" => GetMessage("STA_AF_VIP"),
	"EXCESS_SUM_PAID" => GetMessage("STA_TTRANSF_EXCESS_SUM_PAID"),
	"ORDER_PART_RETURN" => GetMessage("STA_TRETURN")
);

$rsTransact = CAppforsaleUserTransact::GetList(
		array(
				"TRANSACT_DATE" => "DESC"
		),
		array(
				"USER_ID" => $USER->GetID()
		),
		false,
		false,
		array(
				"ID",
				"AMOUNT",
				"DEBIT",
				"DESCRIPTION",
				"TRANSACT_DATE"
		)
);

while($arTransact = $rsTransact->GetNext())
{
	$arTransact['DESCRIPTION'] = $arTransactTypes[$arTransact['DESCRIPTION']];
	$arResult['TRANSACT'][] = $arTransact;
}

$this->IncludeComponentTemplate();
?>