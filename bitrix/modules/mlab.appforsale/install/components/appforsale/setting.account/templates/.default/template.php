<? 
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);
	
$arAccount = CAppforsaleUserAccount::GetByUserID($USER->GetID());
echo '
		<div class="afs-setting-account-current-wrap"><div class="afs-setting-account-current-budget">'.AppforsaleFormatCurrency($arAccount['CURRENT_BUDGET']).'</div>'.GetMessage('BALANCE').'</div>';



$shopId = COption::GetOptionString('mlab.appforsale', "shopId", "");
$scid = COption::GetOptionString('mlab.appforsale', "scid", "");
$receiver = COption::GetOptionString('mlab.appforsale', 'receiver', '');
if (($shopId != '' && $scid != '') || ($receiver != ''))
{
	echo '<div class="afs-setting-account-list-header">'.GetMessage('REFILL_HEADER').'</div>';
	
	?><div class="col-md-12" style="padding-bottom: 32px;"><?

	if ($receiver == '')
	{
		?>
		<script>
			function ymMerchantReceipt()
			{
				BX("ym_merchant_receipt").value = '{"customercontact":"+<?=$USER->GetLogin()?>","items":[{"quantity":1,"price":{"amount":'+BX("sum").value+'},"tax":1,"text":"\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0435 \u0443\u0441\u043b\u0443\u0433\u0438"}]}';
			}
		</script>
		<?
		
		echo '
					<form id="pay" action="https://'.(COption::GetOptionString('mlab.appforsale', "demo", "Y") == 'Y' ? 'demomoney' : 'money').'.yandex.ru/eshop.xml" method="post" onsubmit="return ymMerchantReceipt()">
		    			<input name="shopId" value="'.$shopId.'" type="hidden" />
		    			<input name="scid" value="'.$scid.'" type="hidden" />
		    			<input name="customerNumber" value="'.$USER->GetID().'" type="hidden" />
		    			<input name="cps_phone" value="'.$USER->GetLogin().'" type="hidden" />
						<input id="ym_merchant_receipt" name="ym_merchant_receipt" type="hidden" />';
		
		?>
		<div class="form-group">
			<select name="paymentType" class="form-control"> 
				<option value="PC"><?=Loc::getMessage("PAYMENT_TYPE_PC")?></option>
				<option value="AC"><?=Loc::getMessage("PAYMENT_TYPE_AC")?></option>
			</select>
		</div>
		<? 
	}
	else
	{
		$client_id = COption::GetOptionString("mlab.appforsale", "client_id", "");
		if (!empty($client_id))
		{
			?>
				<script>
				function loadPageBlank()
				{
					app.loadPageBlank({url: "/youdo/personal/payment/?sum=" + BX("sum").value, title: ""});
					return false;
				}
				</script>
				<form id="pay" action="" onsubmit="return loadPageBlank()">
			<? 
		}
		else
		{
	?>
		<form id="pay" action="https://money.yandex.ru/quickpay/confirm.xml" method="post">
			<input name="receiver" value="<?=COption::GetOptionString('mlab.appforsale', 'receiver', '')?>" type="hidden" />
			<input name="label" value="<?=$USER->GetID()?>" type="hidden" />
			<input name="quickpay-form" value="shop" type="hidden" />
			<input name="targets" value="<?=GetMessage('REFILL_BALANCE')?>" type="hidden" />
			<input name="paymentType" value="AC" type="hidden" />
	
		<? 
		}
	}

			?>
			<div class="form-group">
				<input class="form-control" id="sum" name="sum" value="100" type="number" pattern="[0-9]*" />
			</div>
			
			<button type="submit" class="btn btn-default"><?=GetMessage('REFILL')?></button>
		</form>
	</div>
			<? 	
}

// echo '<div class="afs-setting-account-info">'.GetMessage('DESC').'</div>';

if (!empty($arResult['TRANSACT']))
{
	echo '<div class="afs-setting-account-list-header">'.GetMessage('PAYMENTS').'</div>';
	foreach ($arResult['TRANSACT'] as $arTransact)
	{
		echo '<div class="afs-setting-account-list-wrap">
		<div class="afs-setting-account-list-name">'.$arTransact['DESCRIPTION'].'</div>
		<div class="afs-setting-account-list-value">'.FormatDate("d M Y ".Loc::getMessage("V")." H:i", MakeTimeStamp($arTransact['TRANSACT_DATE'])).'</div>
		<div class="afs-setting-account-list-amount">'.(($arTransact["DEBIT"] == "Y") ? "+" : "-").AppforsaleFormatCurrency($arTransact['AMOUNT']).'<br><small>'.(($arTransact["DEBIT"] == "Y") ? GetMessage("STA_TO_ACCOUNT") : GetMessage("STA_FROM_ACCOUNT")).'</small></div>
		</div>';
	}
}
?>