/* appforsale:interface.file */
(function (window) {
	
	if (!!window.JCRating)
	{
		return;
	}

	window.JCRating = function (arParams)
	{
		this.id = null;
		this.visual = {
			ID: '',
			RATING: ''
		};
		
		this.obInput = null;
		this.obRating = null;
		
		this.templateFolder = null;

		if ('object' === typeof arParams)
		{
			this.id = arParams.ID;
			this.templateFolder = arParams.templateFolder;			
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCRating.prototype.Init = function()
	{
		this.obInput = BX(this.id);
		this.obRating = BX(this.visual.RATING);
		if (!!this.obRating)
		{
			BX.bind(this.obRating, 'click', BX.delegate(this.onItemClick, this));
		}
	}
	
	window.JCRating.prototype.onItemClick = function()
	{
		var target = event.target;
		if (target && target.nodeType && target.nodeType == 1 && BX.hasClass(target, "bx_rating"))
		{
			var rating = target.getAttribute("data-rating");
			this.obInput.value = rating;
			var items = BX.findChildren(this.obRating, {
		            tag: "img"
		        }, true);
			
			for (var i = 0; i < 5; i++)
			{
				if (rating <= i)
					items[i].src = this.templateFolder + '/images/ic_rating_off.png';
				else
					items[i].src = this.templateFolder + '/images/ic_rating_on.png';
			}
			
	
		}
		
	}

})(window);