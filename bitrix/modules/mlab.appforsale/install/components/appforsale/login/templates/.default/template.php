<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$APPLICATION->AddHeadScript("https://code.jquery.com/jquery-latest.min.js");
//$APPLICATION->AddHeadScript($templateFolder."/jquery.maskedinput.min.js");
$APPLICATION->AddHeadScript($templateFolder."/jquery.inputmask.bundle.min.js");

$strMainID = $this->GetEditAreaId("login");

$arItemIDs = array(
		"PHONE" => $strMainID."_phone",
		"CONFIRM" => $strMainID."_confirm",
		"CONFIRM_CODE_DIV" => $strMainID."_confirm_code_div",
		"CONFIRM_CODE" => $strMainID."_confirm_code",
		"CAME" => $strMainID."_came"
);

echo '<div class="bx_login_parent">
		<div class="bx_login_inner">
			<div class="bx_login_block">
				<div class="text-justify" style="font-size: 12px; padding-bottom: 16px;">'.GetMessage('INTRO').'</div>
				<div class="label">'.GetMessage('PHONE_NUMBER').'</div>
				<div><input style="width: 100%;" id="'.$arItemIDs['PHONE'].'" class="phone" type="tel" size="18" /></div>
				<div id="'.$arItemIDs['CONFIRM_CODE_DIV'].'" class="bx_login_confirm_div">	
					<div class="label">'.GetMessage('CONFIRM_CODE').'</div>
					<div><input id="'.$arItemIDs['CONFIRM_CODE'].'" class="bx_login_confirm" type="number" pattern="[0-9]*" /><span class="afs-came" id="'.$arItemIDs['CAME'].'">'.GetMessage('CAME').'</span></div>	
					<div class="bx_login_disc">'.GetMessage('DISC').'</div>
				</div>	
				<div><button id="'.$arItemIDs['CONFIRM'].'" class="btn btn-primary bx_login_button_confirm">'.GetMessage('GET_CODE').'</button></div>
				<div class="text-justify" style="font-size: 12px; padding-top: 16px;">
					'.GetMessage('AGREEMENT').'
				</div>
			</div>
		</div>
 	</div>';

$arJSParams = array(
		"mask" => str_replace('9', '\d{1}', COption::GetOptionString('mlab.appforsale', 'login_mask', '79999999999')),
		"VISUAL" => array(
				"PHONE" => $arItemIDs['PHONE'],
				"CONFIRM" => $arItemIDs['CONFIRM'],
				"CONFIRM_CODE_DIV" => $arItemIDs['CONFIRM_CODE_DIV'],
				"CONFIRM_CODE" => $arItemIDs['CONFIRM_CODE'],
				"CAME" => $arItemIDs['CAME']
		),
		"MESS" => array(
			"INVALID_PHONE" => GetMessage('INVALID_PHONE'),
			"ENTER" => GetMessage('ENTER'),
			"GET_CODE" => GetMessage('GET_CODE')
		)
);

echo '<script type="text/javascript">var login = new JCLogin('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
?>