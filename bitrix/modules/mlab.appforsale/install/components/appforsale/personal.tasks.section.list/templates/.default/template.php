<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['SECTIONS']))
{
	echo '<ul>';
	foreach ($arResult['SECTIONS'] as $arSection)
	{
		echo '<li class="bx_profile_section" ';
		if (($arSection['RIGHT_MARGIN'] - $arSection['LEFT_MARGIN']) == 1)
			echo 'onclick="app.loadPageBlank({url: \'?SECTION_ID='.$arSection['ID'].'\', title: \''.$arSection['NAME'].'\'})"';
		else
			echo 'onclick="app.loadPageBlank({url: \'?IBLOCK_SECTION_ID='.$arSection['ID'].'\', title: \''.$arSection['NAME'].'\'})"';
			
				echo '>
				<img src="'.($arSection['PICTURE'] ? $arSection['PICTURE']['src'] : $templateFolder.'/images/section_no_picture.png').'" class="bx_profile_section_picture" />
				<div class="bx_profile_section_name">'.$arSection['NAME'].'</div>
				<img class="bx_profile_section_indicator" src="'.$templateFolder.'/images/section_indicator.png" />
			</li>';
	}
	echo '</ul>';
}
else
{
	echo '<div class="bx_profile_section_empty">'.GetMessage('LIST_EMPTY').'</div>';	
}
?>