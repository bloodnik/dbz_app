<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['PROFILE_IBLOCK_ID'] = intval($arParams['PROFILE_IBLOCK_ID']);

CModule::IncludeModule("iblock");

$rsSub = CIBlockElement::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arParams['PROFILE_IBLOCK_ID'],
			"ACTIVE" => "Y",
			"CREATED_BY" => $USER->GetID()
		),
		false,
		false,
		array(
			"IBLOCK_SECTION_ID"
		)
);

$subIDs = array();
while($arSub = $rsSub->GetNext())
{
	$subIDs[$arSub['IBLOCK_SECTION_ID']] = true;
}

$rsSection = CIBlockSection::GetList(
	array(
		"LEFT_MARGIN" => "ASC"
	),
	array(
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"ACTIVE" => "Y",
		"UF_PROFILE" => array_keys($subIDs)
	),
	false,
	array(
		"ID"
	)
);

$IBLOCK_SECTION_ID = (intval($_GET['IBLOCK_SECTION_ID']) > 0 ? $_GET['IBLOCK_SECTION_ID'] : "");
$sectionIDs = array();
while($arSection = $rsSection->GetNext())
{
	$rsChain = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $arSection['ID'], array("ID", "NAME", "PICTURE", "LEFT_MARGIN", "RIGHT_MARGIN"));
	while($arChain = $rsChain->GetNext())
	{
		if ($arChain['IBLOCK_SECTION_ID'] == $IBLOCK_SECTION_ID && !array_key_exists($arChain['ID'], $sectionIDs))
		{
			$sectionIDs[$arChain['ID']] = true;
			$arChain['PICTURE'] = (0 < $arChain['PICTURE'] ? CFile::ResizeImageGet($arChain['PICTURE'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
			$arResult['SECTIONS'][] = $arChain;
		}
	}
}

$this->IncludeComponentTemplate();
?>