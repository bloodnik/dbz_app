<?
use Bitrix\Main\UserTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["COMMENT_IBLOCK_ID"] = intval($arParams["COMMENT_IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["ELEMENT_ID"] = intval($arParams["ELEMENT_ID"]);
$arParams["TASK_IBLOCK_ID"] = intval($arParams["TASK_IBLOCK_ID"]);


$arSelect = array(
		"ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"LIST_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE",
		"PROPERTY_*",
);

$rsElement = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => $arParams["ELEMENT_ID"]
		),
		false,
		false,
		$arSelect
);

$nameFormat = CSite::GetNameFormat(true);
$arUserID = array();

if($obElement = $rsElement->GetNextElement())
{
	$arResult = $obElement->GetFields();
	$arUserID[$arResult['CREATED_BY']] = true;
	$arResult["PROPERTIES"] = $obElement->GetProperties();
}

if (!empty($arUserID))
{
	$rsUser = Bitrix\Main\UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO', 'PERSONAL_MOBILE'),
			'filter' => array('ID' => array_keys($arUserID)),
	));

	while ($arUser = $rsUser->fetch())
	{
		$arResult['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
		$arResult['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
		$arResult['PERSONAL_MOBILE'] = $arUser['PERSONAL_MOBILE'];

	}
	unset($arUser, $rsUser);
}

$rsSection = CIBlockSection::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => $arParams["SECTION_ID"],
				"ACTIVE" => "Y"
		),
		true,
		array(
				"ID",
				"NAME",
				"UF_DETAIL_FIELD_CODE",
				"UF_DETAIL_PROPERTY"
		)
);

if($arSection = $rsSection->GetNext())
{
	$arResult['SECTION'] = $arSection;
	if (!empty($arSection['UF_DETAIL_FIELD_CODE']))
		$arResult['FIELD_CODE'] = $arSection['UF_DETAIL_FIELD_CODE'];
	else
		$arResult['FIELD_CODE'] = array();
	
	if (!empty($arSection['UF_DETAIL_PROPERTY']))
	{
		$arPropLink = array();
		$dbProp = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $arParams["IBLOCK_ID"], '!CODE' => false));
		while ($arProp = $dbProp->GetNext())
		{
			if (!in_array($arProp['ID'], $arSection['UF_DETAIL_PROPERTY']))
				continue;
			
			$arResult['FIELD_CODE'][] = 'PROPERTY_'.$arProp['CODE'];
		}
	}
}

if (!empty($arResult['FIELD_CODE']))
{
	foreach ($arResult['FIELD_CODE'] as $field)
	{
		if(substr($field, 0, 9) == 'PROPERTY_')
		{
			$pid = substr($field, 9);
			if (!$arResult['PROPERTIES'][$pid] || empty($arResult['PROPERTIES'][$pid]['VALUE']))
				continue;
				
			if ($arResult['PROPERTIES'][$pid]['PROPERTY_TYPE'] == 'E')
			{		
				$value = array();
				$res = CIBlockElement::GetList(array(), array("ID"=> $arResult['PROPERTIES'][$pid]['VALUE']));
				while($ar_res = $res->GetNext())				
				{
					$value[] = $ar_res['NAME'];
				}
				$arResult['DISPLAY_FIELDS'][$field] = array(
						"NAME" => $arResult['PROPERTIES'][$pid]['NAME'],
						"VALUE" => implode(', ', $value),
						"PROPERTY_TYPE" => $arResult['PROPERTIES'][$pid]['PROPERTY_TYPE']
				);
			}
			else
			{
				$arResult['DISPLAY_FIELDS'][$field] = array(
						"NAME" => $arResult['PROPERTIES'][$pid]['NAME'],
						"VALUE" => $arResult['PROPERTIES'][$pid]['VALUE'],
						"PROPERTY_TYPE" => $arResult['PROPERTIES'][$pid]['PROPERTY_TYPE'],
						"USER_TYPE" => $arResult['PROPERTIES'][$pid]['USER_TYPE'],
						"USER_TYPE_SETTINGS" => $arResult['PROPERTIES'][$pid]['USER_TYPE_SETTINGS']
				);
			}
		}
		else
		{
			if (!$arResult[$field])
				continue;

			$arResult['DISPLAY_FIELDS'][$field] = array(
					"NAME" => "Name",
					"VALUE" => $arResult[$field],
					"PROPERTY_TYPE" => "S"
			);
		}
	}
}

$arResult['RATING'] = 0;
$rating_count = 0;
$rating_sum = 0;

$dbOrder = CIBlockElement::GetList(
		array(
				"DATE_CREATE" => "ASC"
		),
		array(
				"IBLOCK_ID" => $arParams["COMMENT_IBLOCK_ID"],
				"PROPERTY_PROFILE_ID" => $arResult['CREATED_BY']
		),
		false,
		false,
		array(
				"ID",
				"CREATED_BY",
				"DATE_CREATE",
				"PROPERTY_TEXT",
				"PROPERTY_RATING"
		)
);

while($arOrder = $dbOrder->GetNext())
{
	$arUserID[$arOrder['CREATED_BY']] = true;
	$arResult['COMMENTS'][] = $arOrder;
	
	$rating_sum += (int) $arOrder['PROPERTY_RATING_VALUE'];
	$rating_count++;
}

if ($rating_count > 0 && $rating_sum > 0)
	$arResult['RATING'] = ceil($rating_sum / $rating_count);

if (!empty($arUserID))
{
	$userIterator = UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
			'filter' => array('ID' => array_keys($arUserID)),
	));
	while ($arOneUser = $userIterator->fetch())
	{
		$arOneUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arOneUser);
		$arOneUser['PERSONAL_PHOTO'] = (0 < $arOneUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arOneUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

		$arResult['USERS'][$arOneUser['ID']] = $arOneUser;
	}
	unset($arOneUser, $userIterator);
}

$user_id = $USER->GetID();
if ($user_id > 0)
{
	$dbElement = CIBlockElement::GetList(
			array(),
			array(
					'IBLOCK_CODE' => 'task',
					'ACTIVE' => 'Y',
					array(
						'LOGIC' => 'OR',
						array(
							'CREATED_BY' => $arResult['CREATED_BY'],
							'PROPERTY_PROFILE_ID' => $user_id,
						),
						array(
							'CREATED_BY' => $user_id,
							'PROPERTY_PROFILE_ID' => $arResult['CREATED_BY'],
						)
					),
					
					
					'!PROPERTY_STATUS_ID' => 'F',
			),
			false,
			false,
			array(
					'ID'
			)
	);

	if ($arElement = $dbElement->GetNext())
	{

	}
	else
	{
		$arResult['PERSONAL_MOBILE'] = '';
	}
}
else
{
	$arResult['PERSONAL_MOBILE'] = '';
}

$this->IncludeComponentTemplate();
?>