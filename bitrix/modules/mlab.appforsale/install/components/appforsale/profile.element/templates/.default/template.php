<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

echo '<div class="bx_user">
		<img src="'.($arResult['PERSONAL_PHOTO'] ? $arResult['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_user_personal_photo" />
				<div class="bx_user_info">';




echo '<div class="bx_user_name">'.$arResult['FORMAT_NAME'].'</div>';
echo '<div class="raiting">';
for ($i = 0; $i < 5; $i++)
{
	if ($i >= $arResult['RATING'])
		echo '<img src="'.$templateFolder.'/images/ic_rating_off.png" />';
	else
		echo '<img src="'.$templateFolder.'/images/ic_rating_on.png" />';
}
echo '</div>';
echo '<div>';
				if (!empty($arResult['PERSONAL_MOBILE']))
					echo '<a class="btn btn-primary call" href="tel:'.$arResult['PERSONAL_MOBILE'].'">'.GetMessage('CALL').'</a>'.(is_dir($_SERVER['DOCUMENT_ROOT'].'/youdo/im/') ? ' <a class="btn btn-primary call" href="/youdo/im/?sel='.$arResult['ID'].'">'.GetMessage('SEND').'</a>' : '');
				else
					echo '<span class="btn btn-primary call disabled" onclick="alert(\''.GetMessage('CALL_DISABLED').'\')">'.GetMessage('CALL').'</span>'.(is_dir($_SERVER['DOCUMENT_ROOT'].'/youdo/im/') ? ' <span class="btn btn-primary call disabled" onclick="alert(\''.GetMessage('SEND_DISABLED').'\')">'.GetMessage('SEND').'</span>' : '');	
				echo '</div>';
			
				
			
				
				$rsSection = CIBlockSection::GetList(
						array(),
						array(
								"IBLOCK_ID" => $arParams['TASK_IBLOCK_ID'],
								"UF_PROFILE" => $arResult['IBLOCK_SECTION_ID']
						),
						false,
						array(
								"ID"
						)
				);
				
				if($arSection = $rsSection->GetNext())
				{
					//echo '/youdo/?SECTION_ID='.$arSection['ID'].'&ACTION=edit&PROFILE_ID='.$arResult['CREATED_BY'];
					echo '<div><a class="call" onclick="app.loadPageBlank({url: \'/youdo/?SECTION_ID='.$arSection['ID'].'&ACTION=edit&PROFILE_ID='.$arResult['CREATED_BY'].'\', title: \''.GetMessage('ADD_TASK').'\'})">'.GetMessage('TASK').'</a></div>';
				}
				echo '</div>';

				
				
						echo '</div>';

if (!empty($arResult['DISPLAY_FIELDS']))
{
	echo '<ul class="bx_fieds">';
	foreach ($arResult['DISPLAY_FIELDS'] as $field)
	{
		echo '<li class="bx_field_list">';
		echo '<div class="bx_name">'.$field['NAME'].'</div>';
		echo '<div class="bx_value">';
		
		
		if ($field['USER_TYPE'] == 'address')
		{
			if (!is_array($field['VALUE']))
				$field['VALUE'] = array($field['VALUE']);
		
			foreach ($field['VALUE'] as &$val)
			{
				$ex = explode('|', $val);
				$val = $ex[0];
			}
		
			echo implode(', ', $field['VALUE']);
		}
		else if ($field['USER_TYPE'] == 'iblock')
		{
			$res = CIBlockElement::GetByID($field['VALUE']);
			if($ar_res = $res->GetNext())
				echo $ar_res['NAME'];
		}
		else if ($field['PROPERTY_TYPE'] == 'F')
		{
			if (!is_array($field['VALUE']))
				$field['VALUE'] = array($field['VALUE']);
			
			foreach ($field['VALUE'] as $file_id)
			{
			
				$arFile = CFile::ResizeImageGet($file_id, array('width' => 128, 'height' => 128), BX_RESIZE_IMAGE_EXACT, true);
				
				echo '<img src="'.$arFile['src'].'" />';
			}
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'map_google')
		{
			echo '<img border="0" style="width: 100%;" src="https://maps.googleapis.com/maps/api/staticmap?key='.$field['USER_TYPE_SETTINGS']['API_KEY'].'&size=480x250&center='.$field['VALUE'].'&zoom=16&markers=color:red%7C%7C'.$field['VALUE'].'"  />';
		}
		else if ($field['PROPERTY_TYPE'] == 'S' && $field['USER_TYPE'] == 'HTML')
		{
			if ($field['VALUE']['TYPE'] == 'HTML')
				echo htmlspecialcharsBack(FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']));
			else 
				echo FormatText($field['VALUE']['TEXT'], $field['VALUE']['TYPE']);
		}
		else
			echo (is_array($field['VALUE']) ? implode(', ', $field['VALUE']) : $field['VALUE']);
		
		
		echo '</div>';
		echo '</li>';
	}
	echo '</ul>';
}


if (!empty($arResult['COMMENTS']))
{
	echo '<div class="bx_comments">';
	echo '<div class="bx_comment_header">'.GetMessage('COMMENTS').'</div>';
	echo '<ul>';
	foreach ($arResult['COMMENTS'] as $key => $arOrder)
	{
		$user = $arResult['USERS'][$arOrder['CREATED_BY']];
		echo '<li class="bx_comment">
				<img src="'.($user['PERSONAL_PHOTO'] ? $user['PERSONAL_PHOTO']['src'] : $templateFolder.'/images/no_photo.png').'" class="bx_comment_photo" />
						<div class="bx_comment_info"><div class="bx_comment_name">'.$user['FORMAT_NAME'].'</div>
						';


		echo '<div class="bx_comment_date">'.$arOrder['DATE_CREATE'].'</div>
				</div>
								<div class="bx_comment_detail_text">'.$arOrder['PROPERTY_TEXT_VALUE'].'</div>
		';

		$rating = intval($arOrder['PROPERTY_RATING_VALUE']);	
		echo '<div class="raiting2">';
		for ($i = 0; $i < 5; $i++)
		{
			if ($i >= $rating)
				echo '<img src="'.$templateFolder.'/images/ic_rating_off.png" />';
			else
				echo '<img src="'.$templateFolder.'/images/ic_rating_on.png" />';
		}
		echo '</div>';
 		echo '</li>';

		unset($user);
	}
	echo '</ul>';
	echo '</div>';
	}
?>