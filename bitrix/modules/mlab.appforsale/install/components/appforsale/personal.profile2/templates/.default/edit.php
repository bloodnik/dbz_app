<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.form',
	'',
	array(
		'IBLOCK_TYPE' => 'profile',
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
 		'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
		'ID' => $arResult['VARIABLES']['ELEMENT_ID'],
		'UPDATE_VALUES' => array(
			'IBLOCK_SECTION_ID' => $arResult['VARIABLES']['SECTION_ID']
		)
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);
?>