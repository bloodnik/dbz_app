<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arComponentParameters['PARAMETERS']['USE_SWITCH'] = array(
	'NAME' => Loc::getMessage('USE_SWITCH'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'N',
	'REFRESH' => 'Y'
);

$arComponentParameters['PARAMETERS']['USE_SUBSCRIPTION'] = array(
	'NAME' => Loc::getMessage('USE_SUBSCRIPTION'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'N',
	'REFRESH' => 'Y'
);

if ($arCurrentValues['USE_SWITCH'] == 'Y')
{
	$arComponentParameters['PARAMETERS']['SWITCH_GROUP'] = array(
		'NAME' => Loc::getMessage('SWITCH_GROUP'),
		'TYPE' => 'LIST',
		'VALUES' => $arSort
	);

	$dbGroup = CGroup::GetList($by = "c_sort", $order = "asc", array('ADMIN' => 'N'));
	while($arGroup = $dbGroup->Fetch())
	{
		$arComponentParameters['PARAMETERS']['SWITCH_GROUP']['VALUES'][$arGroup['ID']] = $arGroup['NAME'];
	}
}
?>