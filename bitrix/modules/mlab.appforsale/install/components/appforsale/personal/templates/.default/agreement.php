<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<script>
BX.ready(function() {
	app.hideProgress();
});	
</script>
<div style="padding: 16px">
<?
	$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/agreement.php"
		),
		false
	);
?>
</div>