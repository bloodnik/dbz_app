/* appforsale:interface.map */
(function (window) {
	
	if (!!window.JCFieldMap)
	{
		return;
	}

	window.JCFieldMap = function (arParams)
	{		
		this.map = null;
		this.marker = null;
		this.id = BX(arParams.ID);
		this.mapDiv = "map" + arParams.ID;
		//this.icon = arParams.MARKER_ICON
		//this.profiles = arParams.PROFILES;
		//this.users = arParams.USERS;
	}

//	window.JCFieldMap.prototype.InitYandex = function()
//	{
//		ymaps.ready(function () {
//			this.map = new ymaps.Map('map', {
//                center: [55.75399399999374, 37.62209300000001],
//                zoom: 14,
//                controls: []
//			});
//			
//			app.getCurrentPosition(BX.delegate(function(position) {
//				this.map.setCenter([position.coords.latitude, position.coords.longitude], 14);
//			}, this));
// 
//            for (var key in this.profiles)
//            {
//            	var arProfile = this.profiles[key];
//            	var arUser = this.users[arProfile.CREATED_BY];
//	        	var placemark = new ymaps.Placemark(
//	        		arProfile.PROPERTY_GEO_VALUE.split(","),
//	        		{
//	        			balloonContent: '<div onclick="app.loadPageBlank({url: \'/youdo/profile/?USER_ID=' + arUser.ID + '\', title: \'' +  arUser.FORMAT_NAME + '\'})"><img class="bx_img" src="' + (arUser.PERSONAL_PHOTO ? arUser.PERSONAL_PHOTO.src : "/bitrix/components/appforsale/profile.map/templates/.default/images/no_photo.png") + '" /><div>' + arUser.FORMAT_NAME + '</div></div>'
//	        		},
//	        		{
//	        			balloonPanelMaxMapArea: 0,
//	        			iconColor: 'red'
//	        		}
//	        	);
//	        	this.map.geoObjects.add(placemark);
//			}
//            
//		    app.hideProgress();
//     	
//        }.bind(this));
//	}
	
	window.JCFieldMap.prototype.setLatLng = function(latLng) {
		this.id.value = latLng.lat() + "," + latLng.lng();
	}
		
	window.JCFieldMap.prototype.InitGoogle = function()
	{
		this.map = new google.maps.Map(BX(this.mapDiv), {
			 center: {lat: 55.75399399999374, lng: 37.62209300000001},
			 zoom: 14,
			 disableDefaultUI: true
		});
	
		this.map.addListener('click', BX.delegate(function(e) {
			
			this.setLatLng(e.latLng);
			
			if (this.marker == null)
			{
				this.marker = new google.maps.Marker({
					position: e.latLng,
					map: this.map,
					draggable:true,
				});	
								
				this.marker.addListener("dragend", BX.delegate(function(e) {
					this.setLatLng(e.latLng);
				}, this));
			}
			else
			{
				this.marker.setPosition(e.latLng);
			}
			
		}, this));

		
		

//		var marker = new google.maps.Marker({
//      		position: {lat: 55.75399399999374, lng: 37.62209300000001},
//      		map: this.map,
//      		draggable:true,
//      	});
		

		
//		for (var key in this.profiles)
//		{
//			var arProfile = this.profiles[key];
//			var arUser = this.users[arProfile.CREATED_BY];
//			this.AddGoogleMarker(arProfile, arUser);
//        }
//
//		app.hideProgress();
	}
	
//	window.JCMap.prototype.AddGoogleMarker = function(arProfile, arUser)
//	{
//		var infowindow = new google.maps.InfoWindow({
//      		content: '<div onclick="app.loadPageBlank({url: \'/youdo/profile/?USER_ID=' + arUser.ID + '\', title: \'' +  arUser.FORMAT_NAME + '\'})"><img class="bx_img" src="' + (arUser.PERSONAL_PHOTO ? arUser.PERSONAL_PHOTO.src : "/bitrix/components/appforsale/profile.map/templates/.default/images/no_photo.png") + '" /><div>' + arUser.FORMAT_NAME + '</div></div>'
//      	});
//
//		var marker = new google.maps.Marker({
//      		position: {lat: Number(arProfile.lat), lng: Number(arProfile.lng)},
//      		map: this.map,
//      		icon: this.icon
//      	});
//      	
//      	marker.addListener('click', function() {
//      		infowindow.open(this.map, marker);
//      	});
//	}

})(window);