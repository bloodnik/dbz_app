<?
$MESS['NAME'] = 'Имя';
$MESS['NO_NAME'] = 'Имя обязательно для заполнения';
$MESS['LAST_NAME'] = 'Фамилия';
$MESS['SECOND_NAME'] = 'Отчество';
$MESS['EMAIL'] = 'Email';
$MESS['CITY'] = 'Город';
$MESS['NO_CITY'] = 'Город обязательно для заполнения';
$MESS['SAVE'] = 'Сохранить';
$MESS['NOT_INSTALLED'] = '(не установлено)';
$MESS['AGREE'] = 'С <a onclick="app.loadPageBlank({url: \'/youdo/personal/?ITEM=agreement\', title: \'Оферта\'})">офертой</a> согласен';
$MESS['SELECT'] = 'Выбрать';
?>