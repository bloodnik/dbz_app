<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!empty($arResult['NOTIFY']))
{
	echo '<ul>';
	foreach ($arResult['NOTIFY'] as $key => $arNotify)
	{
		$strMainID = $this->GetEditAreaId($arNotify['ID']);
		$arItemIDs = array(
				"SWITCH" => $strMainID."_switch",
				"PROGRESS" => $strMainID."_progress",
		);
		
		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
				
		echo '<li class="bx_setting_notify">
			<div class="bx_setting_notify_name">'.$arNotify['NAME'].'</div>
			<img id="'.$arItemIDs['SWITCH'].'" src="'.$templateFolder.'/images/switch_'.($arNotify['TOGLE'] ? "on" : "off").'.png" class="bx_setting_notify_switch" />
 			<img id="'.$arItemIDs['PROGRESS'].'" src="'.$templateFolder.'/images/progress.gif" class="bx_setting_notify_progress" />
		</li>';
		
		$arJSParams = array(
				"ID" => $arNotify['ID'],
				"TOGLE" => $arNotify['TOGLE'],
				"templateFolder" => $templateFolder,
				"VISUAL" => array(
						"SWITCH" => $arItemIDs['SWITCH'],
						"PROGRESS" => $arItemIDs['PROGRESS']
				)
		);
		echo '<script type="text/javascript">var '.$strObName.' = new JCSettingNotify('.CUtil::PhpToJSObject($arJSParams, false, true).')</script>';
		
	}
	echo '</ul>';
}
?>