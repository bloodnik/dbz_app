/* appforsale:setting.notify */
(function (window) {

	if (!!window.JCSettingNotify)
	{
		return;
	}
	
	window.JCSettingNotify = function (arParams)
	{
		this.id = 0;
		this.togle = true;
		this.templateFolder = null;
		this.visual = {
				SWITCH: '',
				PROGRESS: ''
		};
		
		this.obSwitch = null;
		this.obProgress = null;
				
		if ('object' === typeof arParams)
		{
			this.togle = arParams.TOGLE;
			this.id = arParams.ID;
			this.templateFolder = arParams.templateFolder;
			this.visual = arParams.VISUAL;
		}
		
		BX.ready(BX.delegate(this.Init, this));
	}
	
	window.JCSettingNotify.prototype.ShowProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "none"}});
		BX.adjust(this.obProgress, {style: {display: "block"}});
	}
	
	window.JCSettingNotify.prototype.HideProgress = function()
	{
		BX.adjust(this.obSwitch, {style: {display: "block"}});
		BX.adjust(this.obProgress, {style: {display: "none"}});
	}
	
	window.JCSettingNotify.prototype.Init = function()
	{
		this.obSwitch = BX(this.visual.SWITCH);
		this.obProgress = BX(this.visual.PROGRESS);
		
		if (!!this.obSwitch)
		{
			BX.bind(this.obSwitch, 'click', BX.delegate(this.Switch, this));
		}
	}

	window.JCSettingNotify.prototype.Switch = function()
	{
		this.ShowProgress();
		BX.ajax({
	        url: location.href,
	        data: {
				AJAX_CALL: 'Y',
				ACTION: (this.togle ? "off" : "on"),
				ID: this.id
			},
	        method: 'POST',
	        dataType: 'json',
	        onsuccess: BX.delegate(this.onsuccess, this)
	    });
		event.stopPropagation();
	}
	
	window.JCSettingNotify.prototype.onsuccess = function(data)
	{
		if (data.response)
		{
			if (this.togle)
			{
				this.togle = false;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_off.png"}});
			}
			else
			{
				this.togle = true;
				BX.adjust(this.obSwitch, {props: {src: this.templateFolder + "/images/switch_on.png"}});
			}
		}
		else if (data.error)
		{
			alert(data.error.error_msg);
		}
		this.HideProgress();
	}
	
	BX.ready(function() {
		app.hideProgress();
	});	
	
})(window);