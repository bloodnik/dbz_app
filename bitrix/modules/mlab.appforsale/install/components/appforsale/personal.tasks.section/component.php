<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$iblock_id = intval($arParams['IBLOCK_ID']);
$arParams['PROFILE_IBLOCK_ID'] = intval($arParams['PROFILE_IBLOCK_ID']);
$section_id = intval($arParams['SECTION_ID']);

if ($iblock_id > 0)
	$arIBlock = CIBlock::GetArrayByID($iblock_id);
else
	$arIBlock = false;

if ($arIBlock)
{
	$dbSection = CIBlockSection::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arIBlock['ID'],
			"ID" => $section_id
		),
		true,
		array(
			"ID",
			"NAME",
			"UF_FORMAT_NAME",
			"UF_PROFILE",
			"UF_DETAIL_FIELD_CODE"
		)
	);
	
	$arFilter = array();
	$arPropertyCode = array();
	if($arSection = $dbSection->GetNext())
	{
		$arResult['SECTION'] = $arSection;

		foreach ($arSection['UF_DETAIL_FIELD_CODE'] as $value)
		{
			$arPropertyCode[$value] = true;
		}

		preg_match_all("/#([a-z0-9_]+)#/i", $arResult['SECTION']['UF_FORMAT_NAME'], $matches);
		$arFilter = $matches[1];
	}

	$arrFilter = array("LOGIC" => "OR");
	$rsProfile = CIBlockElement::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arParams['PROFILE_IBLOCK_ID'],
			"ACTIVE" => "Y",
			"SECTION_ID" => $arResult['SECTION']['UF_PROFILE'],
			"CREATED_BY" => $USER->GetID()
		),
		false,
		false,
		array(
			"ID",
			"IBLOCK_ID",
			"PROPERTY_*"
		)
	);
	while($obElement = $rsProfile->GetNextElement())
	{
		$arProp = $obElement->GetProperties();
		foreach ($arProp as $key=>$value)
		{
			if (array_key_exists('PROPERTY_'.$key, $arPropertyCode) && !empty($value['VALUE']))
			{
				if ($value['PROPERTY_TYPE'] == 'E')
					$pr['PROPERTY_'.$key] = $value['VALUE'];
				else
					$pr['PROPERTY_'.$key.'_VALUE'] = $value['VALUE'];
			}
		}
		if (!empty($pr))
			$arrFilter[] = $pr;
		
		unset($pr);
	}
	
	$nameFormat = CSite::GetNameFormat(true);
	$arUserID = array();
		
	$dbTask = CIBlockElement::GetList(
			array(
					"TIMESTAMP_X" => "DESC"
			),
			array_merge(
				array(
					"IBLOCK_ID" => $arIBlock['ID'],
					"ACTIVE" => "Y",
					"ACTIVE_DATE" => "Y",
					"SECTION_ID" => $section_id,
					"!CREATED_BY" => $USER->GetID()
				),
 				array($arrFilter)
			),
			false,
			false,
			array(
				"ID",
				"IBLOCK_ID",
				"CREATED_BY",
					"DATE_CREATE",
				"PROPERTY_*"
			)
	);
	
	$input = array();
	foreach ($arFilter as $field)
	{
		$input[] = '#'.$field.'#';
	}
	
	
	while($obTask = $dbTask->GetNextElement())
	{
		$arTask = $obTask->GetFields();
		$arProps = $obTask->GetProperties();
		
		$arUserID[$arTask['CREATED_BY']] = true;
		$output = array();
		foreach ($arFilter as $field)
		{
			if (substr($field, 0, 9) == 'PROPERTY_')
			{				
				$code = substr($field, 9);
				if ($arProps[$code]['PROPERTY_TYPE'] == 'E')
				{
					$value = array();
					$res = CIBlockElement::GetList(array(), array("ID"=> $arProps[$code]['VALUE']));
					while($ar_res = $res->GetNext())
					{	
						$value[] = $ar_res['NAME'];
					}	
					$output[] = implode(', ', $value);
				}
				else
				{
					$output[] = (is_array($arProps[$code]['VALUE']) ? implode(', ', $arProps[$code]['VALUE']) : $arProps[$code]['VALUE']);
				}
				unset($code);
			}
			else 
				$output[] = $arTask[$field];
		}
		
		
		$arTask['FORMAT_NAME'] = str_replace($input, $output, $arResult['SECTION']['UF_FORMAT_NAME']);
		
		
		$arResult['TASKS'][] = $arTask;
	}
	
	if (!empty($arUserID))
	{
		$dbUser = Bitrix\Main\UserTable::getList(array(
				'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
				'filter' => array('ID' => array_keys($arUserID)),
		));
		
		while ($arUser = $dbUser->fetch())
		{
			$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
			$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);
	
			$arResult['USERS'][$arUser['ID']] = $arUser;
		}
		unset($arUser, $dbUser);
	}
}

$this->IncludeComponentTemplate();
?>