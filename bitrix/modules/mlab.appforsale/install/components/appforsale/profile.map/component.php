<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);
$arResult['MARKER_ICON'] = trim($arParams['MARKER_ICON']);

$rsProperty = CIBlockProperty::GetByID(
	"GEO",
	$arParams['IBLOCK_ID']
);

if($arProperty = $rsProperty->GetNext())
{	
	$arResult['TYPE'] = $arProperty['USER_TYPE'];
	$arResult['TYPE_SETTINGS'] = $arProperty['USER_TYPE_SETTINGS'];
}

$arrFilter = array();
$rsProfile = CIBlockElement::GetList(
	array(
		"DATE_CREATE" => "ASC"
	),
	array_merge(
		array(
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"SECTION_ID" => $arParams['SECTION_ID'],
			"ACTIVE" => "Y",
			"!PROPERTY_GEO" => false
		),
		$arrFilter
	),
	false,
	false,
	array(
		"ID", "PROPERTY_GEO", "CREATED_BY"
	)
);

$arUserID = array();
while($arProfile = $rsProfile->GetNext())
{	
	list($arProfile['lat'], $arProfile['lng']) = explode(",", $arProfile['PROPERTY_GEO_VALUE'], 2);

	$arUserID[$arProfile['CREATED_BY']] = true;
	$arResult['PROFILES'][] = $arProfile;
}

$nameFormat = CSite::GetNameFormat(true);
if (!empty($arUserID))
{
	$rsUser = Bitrix\Main\UserTable::getList(array(
			'select' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'),
			'filter' => array('ID' => array_keys($arUserID)),
	));

	while ($arUser = $rsUser->fetch())
	{
		$arUser['FORMAT_NAME'] = CUser::FormatName($nameFormat, $arUser);
		$arUser['PERSONAL_PHOTO'] = (0 < $arUser['PERSONAL_PHOTO'] ? CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_EXACT, true) : false);

		$arResult['USERS'][$arUser['ID']] = $arUser;
	}
	unset($arUser, $rsUser);
}

$this->IncludeComponentTemplate();
?>