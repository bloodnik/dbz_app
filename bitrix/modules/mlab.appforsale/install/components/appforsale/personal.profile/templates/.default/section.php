<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (COption::GetOptionString("mlab.appforsale", "profile_check_pay", "N") != "Y")
{
	COption::SetOptionString("mlab.appforsale", "profile_check_pay", "Y");
	CModule::IncludeModule("iblock");
	$ibp = new CIBlockProperty;
	$ibp->Add(
		array(
			"NAME" => GetMessage('PAYED'),
			"CODE" => "PAYED",
			"PROPERTY_TYPE" => "S",
			"IBLOCK_ID" => $arParams['IBLOCK_ID']
		)
	);
			
	$rsUserField  = new CUserTypeEntity;
	$rsUserField->Add(
		array(
			"FIELD_NAME" => "UF_TEST_PERIOD",
			"USER_TYPE_ID" => "integer",
			"MULTIPLE" => "N",
			"MANDATORY" => "N",
			"EDIT_FORM_LABEL" => array(
				"ru"=> GetMessage('UF_TEST_PERIOD_RU'),
				"en"=> GetMessage('UF_TEST_PERIOD_EN')
			),
			"ENTITY_ID" => "IBLOCK_".$arParams['IBLOCK_ID']."_SECTION"
		)
	);
	
	$rsUserField  = new CUserTypeEntity;
	$rsUserField->Add(
		array(
			"FIELD_NAME" => "UF_PRICE",
			"USER_TYPE_ID" => "double",
			"MULTIPLE" => "N",
			"MANDATORY" => "N",
			"EDIT_FORM_LABEL" => array(
				"ru"=> GetMessage('UF_PRICE_RU'),
				"en"=> GetMessage('UF_PRICE_EN')
			),
			"ENTITY_ID" => "IBLOCK_".$arParams['IBLOCK_ID']."_SECTION"
		)
	);
	
	$rsUserField  = new CUserTypeEntity;
	$newID = $rsUserField->Add(
		array(
			"FIELD_NAME" => "UF_PRICE_TYPE",
			"USER_TYPE_ID" => "enumeration",
			"MULTIPLE" => "N",
			"MANDATORY" => "N",
			"EDIT_FORM_LABEL" => array(
				"ru"=> GetMessage('UF_PRICE_TYPE_RU'),
				"en"=> GetMessage('UF_PRICE_TYPE_EN')
			),
			"ENTITY_ID" => "IBLOCK_".$arParams['IBLOCK_ID']."_SECTION"
		)
	);
	
	$obEnum = new CUserFieldEnum();
	$obEnum->SetEnumValues(
		$newID, 
		array(
			"n0" => array(
				'XML_ID' => "F",
				'VALUE' => GetMessage('PRICE_TYPE_F'),
				'SORT' => 1
			),
			"n1" => array(
				'XML_ID' => "S",
				'VALUE' => GetMessage('PRICE_TYPE_S'),
				'SORT' => 2
			),
			"n2" => array(
				'XML_ID' => "P",
				'VALUE' => GetMessage('PRICE_TYPE_P'),
				'SORT' => 3
			)
		)
	);
}
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:personal.profile.section",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID']
			),
			$component
	);
?>