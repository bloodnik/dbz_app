<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

CModule::IncludeModule("iblock");

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['SECTION_ID'] = intval($arParams['SECTION_ID']);
$arParams['ELEMENT_ID'] = intval($arParams['ELEMENT_ID']);

$rsGuide = CIBlockElement::GetList(
		array(),
		array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arParams['SECTION_ID'],
				"ID" => $arParams['ELEMENT_ID']
		),
		false,
		false,
		array()
);
if ($arGuide = $rsGuide->GetNext())
{
	$arResult = $arGuide;
}

$this->IncludeComponentTemplate();
?>