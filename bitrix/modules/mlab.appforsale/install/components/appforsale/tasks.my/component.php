<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!$USER->IsAuthorized())
{
	CAppforsale::AuthForm();
}
else
{
	$rsUser = CUser::GetByID($USER->GetID());
	if($arUser = $rsUser->GetNext(false))
	{
		if (intval($arUser['UF_CITY']) == 0)
		{
			global $APPLICATION;
			$APPLICATION->IncludeComponent(
				"appforsale:main.profile", 
				"", 
				array(
					"NO_BACK" => "Y"
				),
				false
			);
			die("");
		}
	}
}

// TEMPLATE_THEME

$arVariables = array(
	"ELEMENT_ID" => $_GET['ELEMENT_ID'],
		"SECTION_ID" => $_GET['SECTION_ID'],
		"TASK_ID" => $_GET['TASK_ID'],
	"ACTION" => $_GET['ACTION']
);

if (isset($arVariables["ACTION"]) && $arVariables["ACTION"] == 'edit')
{
	$componentPage = 'edit';
}
elseif (isset($arVariables["ACTION"]) && $arVariables["ACTION"] == 'comment')
{
	$componentPage = 'comment';
}
elseif (isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0)
{
	$componentPage = 'element';
}
else
{
	$componentPage = 'section';	
}

$arResult = array(
		"VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate($componentPage);
?>