<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	$APPLICATION->IncludeComponent(
			"appforsale:tasks.element.edit",
			"",
			array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
				"ELEMENT_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
				"MESS_BTN_SAVE" => $arParams['MESS_BTN_SAVE']
			),
			$component
	);
?>