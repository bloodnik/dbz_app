<?
$module_id = "mlab.appforsale";

IncludeModuleLangFile(__FILE__);

if(!$USER->IsAdmin())
	return;

if (isset($_POST['save']) || isset($_POST['apply']))
{
	COption::SetOptionString($module_id, "google_push_key", $_POST['google_push_key']);
	COption::SetOptionString($module_id, "url_sms_service", $_POST['url_sms_service']);
	COption::SetOptionString($module_id, "google_analytics_key", $_POST['google_analytics_key']);
	COption::SetOptionString($module_id, "multiprofiles", ($_POST['multiprofiles'] == 'Y' ? 'Y' : 'N'));
	COption::SetOptionString($module_id, "demo", ($_POST['demo'] == 'Y' ? 'Y' : 'N'));
	COption::SetOptionString($module_id, "shopId", $_POST['shopId']);
	COption::SetOptionString($module_id, "scid", $_POST['scid']);
	COption::SetOptionString($module_id, "shopPassword", $_POST['shopPassword']);
	
	COption::SetOptionInt($module_id, "starting_balance", intval($_POST['starting_balance']));
	COption::SetOptionString($module_id, "format_string", $_POST['format_string']);
	
	COption::SetOptionString($module_id, "receiver", $_POST['receiver']);
	COption::SetOptionString($module_id, "notification_secret", $_POST['notification_secret']);
	
	COption::SetOptionString($module_id, "bonus_balance", $_POST['bonus_balance']);
	COption::SetOptionString($module_id, "bonus_type_balance", $_POST['bonus_type_balance']);
}

$aTabs = array(
	array("DIV" => "push", "TAB" => GetMessage('NOTIFICATION'), "TITLE" => GetMessage("NOTIFICATION")),
	array(
		"DIV" => "yandex_money",
		"TAB" => GetMessage('APPFORSALE_YANDEX_MONEY_TAB'),
		"TITLE" => GetMessage("APPFORSALE_YANDEX_MONEY_TITLE")
	),
	array(
		"DIV" => "yandex_kassa",
		"TAB" => GetMessage('APPFORSALE_YANDEX_KASSA_TAB'),
		"TITLE" => GetMessage("APPFORSALE_YANDEX_KASSA_TITLE")
	),
	array("DIV" => "other", "TAB" => GetMessage('OTHER'), "TITLE" => GetMessage("OTHER"))
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);
$tabControl->Begin();
echo '<form method="POST" action="'.$APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mid='.$module_id.'">';
$tabControl->BeginNextTab();
?>
<tr class="heading"><td colspan="2"><?=GetMessage('CONFIGURE_PUSH_NOTIFICATION')?></td></tr>
<tr><td width="40%"><?=GetMessage("STATUS")?>:</td><td width="60%">
<?
$google_push_key = COption::GetOptionString($module_id, "google_push_key", "");
if ($google_push_key)
{
	$request = new Bitrix\Main\Web\HttpClient();
	$request->setHeader("Authorization", "key=".COption::GetOptionString($module_id, "google_push_key", ""));
	$request->setHeader("Content-Type", "application/json");
	$request->post("https://gcm-http.googleapis.com/gcm/send", '{"registration_ids":["ABC"]}');
	if ($request->getStatus() == 401)
	{
		echo '<span style="color:red">' . GetMessage('ERROR_KEY') . '</span>';
	}
	else
	{
		echo '<span style="color:green; font-weight: bold">' . GetMessage('ACTIVE') . '</span>';
	}
}
else
{
	echo '<span style="color:gray">' . GetMessage('ERROR_KEY') . '</span>';
}
?>
</td></tr>

<tr>
	<td width="40%"><?=GetMessage('API_KEY')?>:</td>
	<td width="60%"><input name="google_push_key" type="text" size="40" value="<?=COption::GetOptionString($module_id, "google_push_key", "")?>" /></td>
</tr>

<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage("GOOGLE_INFO")?></td>
</tr>


<tr class="heading"><td colspan="2"><?=GetMessage('CONFIGURE_SMS_NOTIFICATION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('URL')?>:</td>
	<td width="60%"><input name="url_sms_service" type="text" size="40" value="<?=COption::GetOptionString($module_id, "url_sms_service", "")?>" /></td>
</tr>

<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage("SMS_INFO")?></td>
</tr>

<?$tabControl->BeginNextTab();?>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER_ALT')?></div></td>
	<td width="60%"><input name="receiver" type="text" value="<?=COption::GetOptionString($module_id, "receiver", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_RECEIVER_INFO')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_ALT')?></div></td>
	<td width="60%"><input name="notification_secret" type="text" value="<?=COption::GetOptionString($module_id, "notification_secret", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_INFO')?></td>
</tr>
<?$tabControl->BeginNextTab();?>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_YANDEX_INFO')?><br /><br />
	<input id="https_check_button" type="button" value="<?=GetMessage('APPFORSALE_YANDEX_CHECK')?>" title="<?=GetMessage('APPFORSALE_YANDEX_CHECK_ALT')?>" onclick="
		var checkHTTPS = function(){
			BX.showWait()
			var postData = {
				action: 'checkHttps',
				https_check: 'Y',
				lang: BX.message('LANGUAGE_ID'),
				sessid: BX.bitrix_sessid()
			};

			BX.ajax({
				timeout: 30,
				method: 'POST',
				dataType: 'json',
				url: '/bitrix/admin/appforsale_pay_system_ajax.php',
				data: postData,

				onsuccess: function (result)
				{
					BX.closeWait();
					BX.removeClass(BX('https_check_result'), 'https_check_success');
					BX.removeClass(BX('https_check_result'), 'https_check_fail');

					BX('https_check_result').innerHTML = '&nbsp;' + result.CHECK_MESSAGE;
					if (result.CHECK_STATUS == 'OK')
						BX.addClass(BX('https_check_result'), 'https_check_success');
					else
						BX.addClass(BX('https_check_result'), 'https_check_fail');
				},
				onfailure : function()
				{
					BX.closeWait();
					BX.removeClass(BX('https_check_result'), 'https_check_success');

					BX('https_check_result').innerHTML = '<?=GetMessage('APPFORSALE_YANDEX_ERROR')?>';
					BX.addClass(BX('https_check_result'), 'https_check_fail');
				}
			});
		};
		checkHTTPS();">
		<span id="https_check_result"></span>
	
	</td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_ADDITIONAL')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_DEMO')?></td>
	<td width="60%"><input name="demo" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "demo", "Y") == 'Y' ? ' checked' : '')?> /></td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPID')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPID_ALT')?></div></td>
	<td width="60%"><input name="shopId" type="text" value="<?=COption::GetOptionString($module_id, "shopId", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SCID')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SCID_ALT')?></div></td>
	<td width="60%"><input name="scid" type="text" value="<?=COption::GetOptionString($module_id, "scid", "")?>" size="30" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPPASSWORD')?><div style="font-size:10px;"><?=GetMessage('APPFORSALE_YANDEX_KASSA_SHOPPASSWORD_ALT')?></div></td>
	<td width="60%"><input name="shopPassword" type="text" value="<?=COption::GetOptionString($module_id, "shopPassword", "")?>" size="30" /></td>
</tr>
<?$tabControl->BeginNextTab()?>
<tr>
	<td width="40%"><?=GetMessage('GOOGLE_ANALYTICS_KEY')?>:</td>
	<td width="60%"><input name="google_analytics_key" type="text" size="40" value="<?=COption::GetOptionString($module_id, "google_analytics_key", "")?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('MULTIPROFILES')?>:</td>
	<td width="60%"><input name="multiprofiles" value="Y" type="checkbox"<?=(COption::GetOptionString($module_id, "multiprofiles", "N") == 'Y' ? ' checked' : '')?> /></td>
</tr>
<tr class="heading">
	<td colspan="2"><?=GetMessage('APPFORSALE_OTHER_HEADING_USER')?></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_OTHER_FORMAT_STRING')?>:</td>
	<td width="60%"><input name="format_string" type="text" size="40" value="<?=COption::GetOptionString($module_id, "format_string", "# ".GetMessage('APPFORSALE_RUB'))?>" /></td>
</tr>
<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_OTHER_CREDIT')?>:</td>
	<td width="60%"><input name="starting_balance" type="text" size="40" value="<?=COption::GetOptionInt($module_id, "starting_balance", 0)?>" /></td>
</tr>
<tr>
	<td width="40%"></td>
	<td width="60%"><?=GetMessage('APPFORSALE_OTHER_CREDIT_ALT')?></td>
</tr>

<tr>
	<td width="40%"><?=GetMessage('APPFORSALE_BONUS')?>:</td>
	<td width="60%"><input name="bonus_balance" type="text" size="5" value="<?=COption::GetOptionInt($module_id, "bonus_balance", 0)?>" /> <select name="bonus_type_balance"><option value="0"<?=(COption::GetOptionInt($module_id, "bonus_type_balance", 0) == 0 ? ' selected="selected"' : '')?>><?=GetMessage('APPFORSALE_BONUS_RUB')?></option><option value="1"<?=(COption::GetOptionInt($module_id, "bonus_type_balance", 0) == 1 ? ' selected="selected"' : '')?>><?=GetMessage('APPFORSALE_BONUS_PERCENT')?></option></select></td>
</tr>
<?
$tabControl->Buttons(array());
$tabControl->End();
?>
</form>