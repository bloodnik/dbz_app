<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if ($APPLICATION->GetCurPage() != '/youdo/left.php' && $APPLICATION->GetCurPage() != '/index.php' && $APPLICATION->GetCurPage() != '/' && $_REQUEST['APPFORSALE'] != 'Y')
{
?>

<div id="menu" style="display: none; position: fixed; top: 0; left:0; width: 300px;">
	<?
	if ($USER->IsAuthorized() || !define('NEED_AUTH'))
	{
		$APPLICATION->IncludeComponent(
				"appforsale:menu",
				"",
				array(
						"PATH_TO_LOGIN" => SITE_DIR."youdo/login/",
						"PATH_TO_PROFILE" => SITE_DIR."youdo/personal/",
						"PATH_TO_FILE_MENU" => SITE_DIR."youdo/.menu.php",
						"DESKTOP" => "Y"
				),
				false
		);
	}
	?>
	</div>
<?}?>
	</body>
</html>