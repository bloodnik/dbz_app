<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!CModule::IncludeModule("mlab.appforsale"))
	die();

CAppforsale::Init();
CJSCore::Init(array("fx", 'ajax'));
?>
<!doctype html>
<html>
	<head>
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    	<meta charset="<? echo SITE_CHARSET?>" />
    	<? 
//     	$APPLICATION->ShowCSS(true);
//         $APPLICATION->ShowHeadStrings();
//         $APPLICATION->ShowHeadScripts();
        $APPLICATION->ShowHead();
        
   
        $APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
        $APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
        CJSCore::Init('ajax');
        
		$APPLICATION->AddHeadScript("https://code.jquery.com/jquery-latest.min.js");
    	?>
    	<title><?$APPLICATION->ShowTitle()?></title>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
    	<script type="text/javascript">
   
    	BX.ready(function() {
    		app.hideProgress();
    	});	
    	
    	
    	function checkEvent(e)
    	{
    	    return ((e = (e || window.event)) && (e.type == 'click' || e.type == 'mousedown' || e.type == 'mouseup') && (e.which > 1 || e.button > 1 || e.ctrlKey || e.shiftKey || BX.browser.IsMac() && e.metaKey)) || false;
    	}
    	
    	document.onclick = function(e)
    	{
    		if (checkEvent(e))
    		        return true;

    	    var i = 8,
            target = e.target || e.srcElement,
            href,
            w = window;
        	while (target && target != document.body && target.tagName != 'A' && i--)
            {
            	target = target.parentNode;
        	}
        	if (!target || target.tagName != 'A' || target.onclick || target.onmousedown)
            	return true;

        	href = target.href;

        	if (href && target.getAttribute('target'))
        	{
                try {
                	window.opener.location = href;
                    return cancelEvent(e);
                } catch (er) {
                    return true;
                }
            }
            
        	if (/javascript/i.test(target.href))
				return true;
        	else
        	{
            	var title = '';
            	if (target.hasAttribute('title'))
            		title = target.getAttribute('title')
     
        		app.loadPageBlank({url: target.href.replace(window.location.protocol + "//" + window.location.hostname, ''), title: title});
        		return false;
           	}
    	}
    	</script>
    	<?if (preg_match('/(ipad|iphone|android|mobile|touch)/i', $_SERVER['HTTP_USER_AGENT'])):?>
		<style>
			body {
				-webkit-tap-highlight-color: transparent;
				-webkit-user-select: none;
				-webkit-touch-callout: none; 
			}
		</style>
    	<?endif;?>
	</head>
	<body onresize="onBodyResize()">
	<?if ($_REQUEST['APPFORSALE'] != 'Y'):?>
		<div id="box_layer_bg"></div><div id="box_layer_wrap"><div id="box_layout"></div></div>
		<script type="text/javascript">domStarted();</script>
	
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<div id="afs-page-wrap">
	<?endif;?>