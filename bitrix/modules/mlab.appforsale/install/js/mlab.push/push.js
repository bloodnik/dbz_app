(function(window){
	
	var _updateStateTimeout = null,
		_pullPath = null,
		_channelID = null,
		_pullTimeConst = (new Date(2022, 2, 19)).toUTCString(),
		_pullTime = _pullTimeConst,
		_pullTag = 1,
		_pullTimeout = 60,
		_pullMid = null,
		_channelLastID = 0,
		_channelStack = {},
		_escStatus = false,
		_sendAjaxTry = 0,
		_pathToAjax = '/bitrix/components/mlab/pull.request/ajax.php?';
	
	MLab = function() {};
	
	MLab.pull = function() {};
	MLab.pull.start = function(params)
	{
		if (typeof(params) != 'object')
		{
			params = {};
		}
		
		if (BX.browser.IsFirefox())
		{
			BX.bind(window, 'keypress', function(event) {
				if (event.keyCode == 27)
					_escStatus = true;
			});
		}
		
		if (params.CHANNEL_ID)
		{
			_channelID = params.CHANNEL_ID;
			_pullPath = params.PATH
		}
		
		MLab.pull.init();
	}
	
	MLab.pull.init = function()
	{
		MLab.pull.updateState('init');
	}
	
	MLab.pull.getChannelID = function(code, withoutCache, send)
	{
//		if (!_pullTryConnect)
//			return false;
//
//		send = send != false;
//		withoutCache = withoutCache == true;
		code = typeof(code) == 'undefined' ? '0' : code;
		BX.ajax({
			url: _pathToAjax + 'GET_CHANNEL',
			method: 'POST',
			skipAuthCheck: true,
			dataType: 'json',
			lsId: 'PULL_GET_CHANNEL',
			lsTimeout: 1,
			timeout: 30,
			data: {
				'PULL_GET_CHANNEL' : 'Y',
				'SITE_ID': (BX.message.SITE_ID ? BX.message('SITE_ID'): ''),
				'PULL_AJAX_CALL' : 'Y',
				'sessid': BX.bitrix_sessid()
			},
			onsuccess: BX.delegate(function(data)
			{
//				_channelClearReason = 0;
//				if (send && BX.localStorage.get('pgc') === null)
//					BX.localStorage.set('pgc', withoutCache, 1);
//
				if (typeof(data) == 'object' && data.ERROR == '')
				{
//					if (data.REVISION && !BX.PULL.checkRevision(data.REVISION))
//						return false;
//
					_channelID = data.CHANNEL_ID;
				//	_pullPath = data.PATH;
//					_wsPath = data.PATH_WS;
//					_pullMethod = data.METHOD;
//
//					var CHANNEL_DT = data.CHANNEL_DT.toString().split('/');
//					_pullTimeConfig = CHANNEL_DT[0];
//					_pullTimeConfigShared = CHANNEL_DT[1]? CHANNEL_DT[1]: CHANNEL_DT[0];
//
//					_pullTimeConfig = parseInt(_pullTimeConfig)+parseInt(BX.message('SERVER_TZ_OFFSET'))+parseInt(BX.message('USER_TZ_OFFSET'));
//					_pullTimeConfigShared = parseInt(_pullTimeConfigShared)+parseInt(BX.message('SERVER_TZ_OFFSET'))+parseInt(BX.message('USER_TZ_OFFSET'));
//					_channelLastID = _pullMethod=='PULL'? data.LAST_ID: _channelLastID;
//					data.TIME_LAST_GET = _pullTimeConfig;
//					data.TIME_LAST_GET_SHARED = _pullTimeConfigShared;
					MLab.pull.updateState('11');
//					BX.PULL.expireConfig();
//					if (_lsSupport)
//						BX.localStorage.set('pset', data, 600);
				}
				else
				{
					_sendAjaxTry++;
//					_channelClearReason = 2;
					_channelID = null;
//					clearTimeout(_updateStateStatusTimeout);
//					BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//					if (typeof(data) == 'object' && data.BITRIX_SESSID)
//					{
//						BX.message({'bitrix_sessid': data.BITRIX_SESSID});
//					}
//
//					if (typeof(data) == 'object' && data.ERROR == 'SESSION_ERROR')
//					{
//						clearTimeout(_updateStateTimeout);
//						_updateStateTimeout = setTimeout(function(){BX.PULL.updateState('12', true)}, (_sendAjaxTry < 2? 2000: BX.PULL.tryConnectTimeout()));
//						BX.onCustomEvent(window, 'onPullError', [data.ERROR, data.BITRIX_SESSID]);
//					}
//					else if (typeof(data) == 'object' && data.ERROR == 'AUTHORIZE_ERROR')
//					{
//						BX.onCustomEvent(window, 'onPullError', [data.ERROR]);
//					}
//					else
//					{
//						clearTimeout(_updateStateTimeout);
//						_updateStateTimeout = setTimeout(function(){BX.PULL.updateState('31', true)}, BX.PULL.tryConnectTimeout());
//						BX.onCustomEvent(window, 'onPullError', ['NO_DATA']);
//					}
//					if (send && typeof(console) == 'object')
//					{
//						var text = "\n========= PULL ERROR ===========\n"+
//									"Error type: getChannel error\n"+
//									"Error: "+data.ERROR+"\n"+
//									"\n"+
//									"Data array: "+JSON.stringify(data)+"\n"+
//									"================================\n\n";
//						console.log(text);
					}
//				}
			}, this),
			onfailure: BX.delegate(function(data)
			{
//				_sendAjaxTry++;
//				_channelClearReason = 3;
//				_channelID = null;
//				clearTimeout(_updateStateStatusTimeout);
//				BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//				if (data == "timeout")
//				{
//					clearTimeout(_updateStateTimeout);
//					_updateStateTimeout = setTimeout(function(){
//						BX.PULL.updateState('1')
//					}, 10000);
//				}
//				else
//				{
//					if (typeof(data) == 'object' && data.ERROR == 'auth')
//					{
//						BX.onCustomEvent(window, 'onPullError', ['AUTHORIZE_ERROR']);
//					}
//					if (typeof(console) == 'object')
//					{
//						var text = "\n========= PULL ERROR ===========\n"+
//									"Error type: getChannel onfailure\n"+
//									"Error: "+data.ERROR+"\n"+
//									"\n"+
//									"Data array: "+JSON.stringify(data)+"\n"+
//									"================================\n\n";
//						console.log(text);
//					}
//					clearTimeout(_updateStateTimeout);
//					_updateStateTimeout = setTimeout(function(){BX.PULL.updateState('14', true)}, BX.PULL.tryConnectTimeout());
//				}
			}, this)
		});
	};
	
	MLab.pull.updateState = function(code, force)
	{
		code = typeof(code) == 'undefined' ? '' : code;
		
		if (_channelID == null || _pullPath == null)
		{
			
		}
		else
		{
			MLab.pull.connectPull(force);
		}
	}
	
	MLab.pull.connectPull = function(force)
	{
		force = force == true;
		clearTimeout(_updateStateTimeout);
		_updateStateTimeout = setTimeout(function(){
//			if (!_pullPath || typeof(_pullPath) != "string" || _pullPath.length <= 32)
//			{
//				_pullPath = null;
//
//				clearTimeout(_updateStateTimeout);
//				_updateStateTimeout = setTimeout(function(){
//					BX.PULL.updateState('17');
//				}, 10000);
//
//				return false;
//			}
//
//			_updateStateStatusTimeout = setTimeout(function(){
//				BX.onCustomEvent(window, 'onPullStatus', ['online']);
//			}, 5000);
//
//			_updateStateSend = true;
//
			var headersForRequest = [];
//			if (_pullWithHeaders)
//			{
				headersForRequest = [
					{'name':'If-Modified-Since', 'value': _pullTime},
					{'name':'If-None-Match', 'value': _pullTag}
				];
//			}
//
//			BX.onCustomEvent(window, 'onPullStatus', ['connect']);
//
				
			var pullPath = _pullPath.replace('#DOMAIN#', location.hostname);
			var _ajax = BX.ajax({
				url: (pullPath + (_pullTag != null ? '&tag=' + _pullTag : '') + (_pullTime != null ? '&time=' + _pullTime : '') + (_pullMid !== null ? '&mid=' + _pullMid : '')) + '&rnd=' + (+new Date),
				skipAuthCheck: true,
				skipBxHeader: true,
				method: 'GET',
				dataType: 'html',
				headers: headersForRequest,
				data: {},
				timeout: _pullTimeout,
				onsuccess: function(data)
				{
//					clearTimeout(_updateStateStatusTimeout);
//					_updateStateSend = false;
//					if (_WS) _WS.close(1000, "ajax_onsuccess");
//
				//	if (typeof(data) == 'object')
			//	{
					//	alert('object');
//						if (data.ERROR == "")
//						{
//							BX.onCustomEvent(window, 'onPullStatus', ['online']);
//
//							_sendAjaxTry = 0;
//							BX.PULL.executeMessages(data.MESSAGE, {'SERVER_TIME': (new Date()).toUTCString(), 'SERVER_TIME_WEB': Math.round((+new Date())/1000)});
//							if (_lsSupport)
//								BX.localStorage.set('pus', {'MESSAGE':data.MESSAGE}, 5);
//						}
//						else
//						{
//							clearTimeout(_updateStateStatusTimeout);
//							BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//
//							if (data && data.BITRIX_SESSID)
//							{
//								BX.message({'bitrix_sessid': data.BITRIX_SESSID});
//							}
//
//							if (data.ERROR == 'SESSION_ERROR')
//							{
//								BX.onCustomEvent(window, 'onPullError', [data.ERROR, data.BITRIX_SESSID]);
//							}
//							else
//							{
//								BX.onCustomEvent(window, 'onPullError', [data.ERROR]);
//							}
//							if (typeof(console) == 'object')
//							{
//								var text = "\n========= PULL ERROR ===========\n"+
//											"Error type: updateState error\n"+
//											"Error: "+(data && data.ERROR? data.ERROR: 'unknown')+"\n"+
//											"\n"+
//											"Connect CHANNEL_ID: "+_channelID+"\n"+
//											"Connect PULL_PATH: "+_pullPath+"\n"+
//											"\n"+
//											"Data array: "+JSON.stringify(data)+"\n"+
//											"================================\n\n";
//								console.log(text);
//							}
//							_channelClearReason = 5;
//							_channelID = null;
//						}
//						if (_channelID != null && _lsSupport)
//							BX.localStorage.set('pset', {'CHANNEL_ID': _channelID, 'LAST_ID': _channelLastID, 'PATH': _pullPath, 'PATH_WS': _wsPath, 'TAG': _pullTag, 'MID': _pullMid, 'TIME': _pullTime, 'TIME_LAST_GET': _pullTimeConfig, 'TIME_LAST_GET_SHARED': _pullTimeConfigShared, 'METHOD': _pullMethod}, 600);
//
//						BX.PULL.setUpdateStateStep();
					//}
				//	else
				//	{
						if (data.length > 0)
						{
							var messageCount = 0;
//							_sendAjaxTry = 0;
//
							var dataArray = data.match(/#!NGINXNMS!#(.*?)#!NGINXNME!#/gm);
							if (dataArray != null)
							{
								for (var i = 0; i < dataArray.length; i++)
								{
									dataArray[i] = dataArray[i].substring(12, dataArray[i].length-12);
									if (dataArray[i].length <= 0)
										continue;
									
									var message = BX.parseJSON(dataArray[i]);
									var data = null;
									if (message && message.text)
										data = message.text;
									if (data !== null && typeof (data) == 'object')
									{
										if (data && data.ERROR == '')
										{
											if (message.id)
											{
												message.id = parseInt(message.id);
												message.channel = message.channel ? message.channel : (data.CHANNEL_ID ? data.CHANNEL_ID : message.time);
												if (!_channelStack['' + message.channel + message.id])
												{
													_channelStack['' + message.channel + message.id] = message.id;

													if (_channelLastID < message.id)
														_channelLastID = message.id;
	
													MLab.pull.executeMessages(data.MESSAGE, {'SERVER_TIME': message.time, 'SERVER_TIME_WEB': data.SERVER_TIME_WEB});
												}
											}
										}
//										else
//										{
//											if (typeof(console) == 'object')
//											{
//												var text = "\n========= PULL ERROR ===========\n"+
//															"Error type: updateState fetch\n"+
//															"Error: "+(data && data.ERROR? data.ERROR: 'unknown')+"\n"+
//															"\n"+
//															"Connect CHANNEL_ID: "+_channelID+"\n"+
//															"Connect PULL_PATH: "+_pullPath+"\n"+
//															"\n"+
//															"Data array: "+JSON.stringify(data)+"\n"+
//															"================================\n\n";
//												console.log(text);
//											}
//											_channelClearReason = 6;
//											_channelID = null;
//											clearTimeout(_updateStateStatusTimeout);
//											BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//										}
									}
//									else
//									{
//										if (typeof(console) == 'object')
//										{
//											var text = "\n========= PULL ERROR ===========\n"+
//														"Error type: updateState parse\n"+
//														"\n"+
//														"Connect CHANNEL_ID: "+_channelID+"\n"+
//														"Connect PULL_PATH: "+_pullPath+"\n"+
//														"\n"+
//														"Data string: "+dataArray[i]+"\n"+
//														"================================\n\n";
//											console.log(text);
//										}
//										_channelClearReason = 7;
//										_channelID = null;
//										clearTimeout(_updateStateStatusTimeout);
//										BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//									}
																		
									if (message.tag)
										_pullTag = message.tag;
									if (message.time)
										_pullTime = message.time;
									if (message.mid)
										_pullMid = message.mid;
									messageCount++;
									
						
										
								
								}
							}
//							else
//							{
//								if (typeof(console) == 'object')
//								{
//									var text = "\n========= PULL ERROR ===========\n"+
//												"Error type: updateState error getting message\n"+
//												"\n"+
//												"Connect CHANNEL_ID: "+_channelID+"\n"+
//												"Connect PULL_PATH: "+_pullPath+"\n"+
//												"\n"+
//												"Data string: "+data+"\n"+
//												"================================\n\n";
//									console.log(text);
//								}
//								_channelClearReason = 8;
//								_channelID = null;
//								clearTimeout(_updateStateStatusTimeout);
//								BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//							}
							if (messageCount > 0 || _ajax && _ajax.status == 0)
							{
								MLab.pull.updateState(messageCount > 0 ? '19' : '20');
							}
							else
							{
//								_channelClearReason = 9;
								_channelID = null;
								clearTimeout(_updateStateTimeout);
								_updateStateTimeout = setTimeout(function() { MLab.pull.updateState('21') }, 10000);
							}
						}
						else
						{
							if (_ajax && (_ajax.status == 304 || _ajax.status == 0))
							{
								if (_ajax.status == 0)
								{
									if (_escStatus)
									{
										_escStatus = false;
										MLab.pull.updateState('22-3');
									}
									else
									{
										_updateStateTimeout = setTimeout(function() {
											MLab.pull.updateState('22-2');
										}, 30000);
									}
								}
								else
								{
									try {
										var expires = _ajax.getResponseHeader("Expires");
										if (expires === "Thu, 01 Jan 1973 11:11:01 GMT")
										{
											var lastMessageId = _ajax.getResponseHeader("Last-Message-Id");
											if (_pullMid === null && lastMessageId && lastMessageId.length > 0)
											{
												_pullMid = lastMessageId;
											}
										}
									}
									catch(event) {

									}
									MLab.pull.updateState('22-1');
								}
							}
							else if (_ajax && (_ajax.status == 502 || _ajax.status == 500))
							{
//								clearTimeout(_updateStateStatusTimeout);
//								BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//								_sendAjaxTry++;
//								_channelClearReason = 10;
								_channelID = null;
								clearTimeout(_updateStateTimeout);
								_updateStateTimeout = setTimeout(function(){
									MLab.pull.updateState('23');
								}, MLab.pull.tryConnectTimeout());
							}
							else
							{
//								clearTimeout(_updateStateStatusTimeout);
//								BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//
								_sendAjaxTry++;
//								_channelClearReason = 11;
								_channelID = null;
								var timeout = MLab.pull.tryConnectTimeout();
								var code = (_ajax && typeof(_ajax.status) != 'undefined'? _ajax.status: 'NaN');
								clearTimeout(_updateStateTimeout);
								_updateStateTimeout = setTimeout(function(){
									MLab.pull.updateState('24-'+code+'-'+(timeout/1000));
								}, timeout);
							}
					//	}
					}
				},
				onfailure: function(data)
				{

//					clearTimeout(_updateStateStatusTimeout);
//					BX.onCustomEvent(window, 'onPullStatus', ['offline']);
//					_updateStateSend = false;
					_sendAjaxTry++;
//					if (_WS) _WS.close(1000, "ajax_onfailure");
					if (data == "timeout")
					{
//						if (_pullMethod=='PULL')
//							BX.PULL.setUpdateStateStep();
//						else
							MLab.pull.updateState('25');
					}
					else if (_ajax && (_ajax.status == 403 || _ajax.status == 404 || _ajax.status == 400))
					{
						if (_ajax.status == 403)
						{
							_channelLastID = 0;
							_channelStack = {};
						}
//
//						_channelClearReason = 12;
						_channelID = null;
						clearTimeout(_updateStateTimeout);
						_updateStateTimeout = setTimeout(function(){
							MLab.pull.getChannelID('7-'+_ajax.status, _ajax.status == 403)
						}, (_sendAjaxTry < 2 ? 50 : MLab.pull.tryConnectTimeout()));
					}
					else if (_ajax && (_ajax.status == 500 || _ajax.status == 502))
					{
//						_channelClearReason = 13;
						_channelID = null;
						clearTimeout(_updateStateTimeout);
//						_updateStateTimeout = setTimeout(function(){
//							BX.PULL.getChannelID('8-'+_ajax.status)
//						}, (_sendAjaxTry < 2? 50: BX.PULL.tryConnectTimeout()));
					}
//					else
//					{
//						if (typeof(console) == 'object')
//						{
//							var text = "\n========= PULL ERROR ===========\n"+
//										"Error type: updateState onfailure\n"+
//										"\n"+
//										"Connect CHANNEL_ID: "+_channelID+"\n"+
//										"Connect PULL_PATH: "+_pullPath+"\n"+
//										"\n"+
//										"Data array: "+JSON.stringify(data)+"\n"+
//										"================================\n\n";
//							console.log(text);
//						}
//						clearTimeout(_updateStateTimeout);
//						if (_pullMethod=='PULL')
//							_updateStateTimeout = setTimeout(BX.PULL.setUpdateStateStep, 10000);
//						else
//							_updateStateTimeout = setTimeout(function(){BX.PULL.updateState('26');}, 10000);
//					}
				}
			});
		}, force ? 150 : 300);
	}
	
	MLab.pull.executeMessages = function(message, time)
	{
		time = time === null ? {'SERVER_TIME': (new Date()).toUTCString(), 'SERVER_TIME_WEB': Math.round((+new Date())/1000)} : time;
//		pull = pull !== false;
		for (var i = 0; i < message.length; i++)
		{
//			message[i].module_id = message[i].module_id.toLowerCase();
//
			if (message[i].id)
			{
				message[i].id = parseInt(message[i].id);
//				if (_channelStack[''+_channelID+message[i].id])
//					continue;
//				else
//					_channelStack[''+_channelID+message[i].id] = message[i].id;
//
//				if (_channelLastID < message[i].id)
//					_channelLastID = message[i].id;
			}
			message[i].params['SERVER_TIME_WEB'] = parseInt(time.SERVER_TIME_WEB);
			message[i].params['SERVER_TIME'] = time.SERVER_TIME;
//
//			if (message[i].module_id == 'pull')
//			{
//				if (pull)
//				{
//					if (message[i].command == 'channel_die' && typeof(message[i].params.replace) == 'object')
//					{
//						BX.PULL.updateChannelID({
//							'METHOD': _pullMethod,
//							'LAST_ID': _channelLastID,
//							'CHANNEL_ID': _channelID,
//							'CHANNEL_DT': _pullTimeConfig+'/'+message[i].params.replace.CHANNEL_DIE,
//							'PATH': _pullPath.replace(message[i].params.replace.PREV_CHANNEL_ID, message[i].params.replace.CHANNEL_ID),
//							'PATH_WS': _wsPath? _wsPath.replace(message[i].params.replace.PREV_CHANNEL_ID, message[i].params.replace.CHANNEL_ID): _wsPath
//						});
//					}
//					else if (message[i].command == 'channel_die' || message[i].command == 'config_die')
//					{
//						_channelClearReason = 14;
//						_channelID = null;
//						_pullPath = null;
//						if (_wsPath) _wsPath = null;
//						if (_WS) _WS.close(1000, "config_die");
//					}
//					else if (message[i].command == 'server_restart')
//					{
//						BX.PULL.tryConnectSet(0, false);
//						BX.localStorage.set('prs', true, 600);
//						if (_WS) _WS.close(1000, "server_restart");
//						setTimeout(function(){
//							BX.PULL.tryConnect();
//						}, ((Math.floor(Math.random() * (61)) + 60)*1000)+600000)
//					}
//				}
//			}
//			else
//			{
//				if (!(message[i].module_id == 'main' && message[i].command == 'user_counter'))
//					BX.PULL.setUpdateStateStepCount(1,4);
//
				try
				{
//					message[i].params['PULL_TIME_AGO'] = BX.PULL.getDateDiff(message[i].params['SERVER_TIME_WEB']+parseInt(BX.message('USER_TZ_OFFSET')));
//					if (message[i].module_id == 'online')
//					{
//						if (message[i].params['PULL_TIME_AGO'] < 120)
//							BX.onCustomEvent(window, 'onPullOnlineEvent', [message[i].command, message[i].params], true);
//					}
//					else
//					{
			
					
					//console.log(message[i])
					BX.onCustomEvent(message[i].command, [message[i].params]);
					
					//BX.onCustomEvent(window, 'onPullEvent-'+message[i].module_id, [message[i].command, message[i].params], true);
//						BX.onCustomEvent(window, 'onPullEvent', [message[i].module_id, message[i].command, message[i].params], true);
//					}
				}
				catch(e)
				{
//					if (typeof(console) == 'object')
//					{
//						console.log(
//							"\n========= PULL ERROR ===========\n"+
//							"Error type: onPullEvent onfailure\n"+
//							"Error event: ", e, "\n"+
//							"Message: ", message[i], "\n"+
//							"================================\n"
//						);
//						BX.debug(e);
//					}
				}
			//}
		}
	}
	
	MLab.pull.tryConnectTimeout = function()
	{
		var timeout = 0;
		if (_sendAjaxTry <= 2)
			timeout = 15000;
		else if (_sendAjaxTry > 2 && _sendAjaxTry <= 5)
			timeout = 45000;
		else if (_sendAjaxTry > 5 && _sendAjaxTry <= 10)
			timeout = 600000;
		else if (_sendAjaxTry > 10)
		{
			_pullTryConnect = false;
			timeout = 3600000;
		}

		return timeout;
	}
	
})(window);