;(function(window){

	if (!window.BM)
	{
		if (typeof(console) == "object") console.error("Mobile notice: mlab.appforsale core not loaded");
		return;
	}
	
	if (window.BM.MobileApp)
	{
		if (typeof(console) == "object") console.error("Mobile notice: script is already loaded");
		return;
	}
			
	BM.MobileApp = {
			
//			callbacks: {},
//			callbackIndex: 0,
			
			isDevice: function()
			{
				return (typeof mobileDev != "undefined" ? mobileDev == "Y" : false)
			},
		
			getDevice: function()
			{
				return (typeof mobileDevice != "undefined" ? mobileDevice : "");
			},
			
			getApiVersion: function()
			{
				return (typeof mobileApiVersion != "undefined" ? mobileApiVersion : 1);
			},
			
			getPlatform: function()
			{
				if (this.getDevice() == "android")
					return "android";

				return "ios";	
			},
			
//			registerCallBack: function(callback)
//			{
//				if (typeof(callback) == "function")
//				{
//					this.callbackIndex++;
//					this.callbacks["callback" + this.callbackIndex] = callback;
//					return this.callbackIndex;
//				}
//
//				return 0;
//			},
//
//			callBackExecute: function(index, result)
//			{
//				if (this.callbacks["callback" + index] && (typeof this.callbacks["callback" + index]) === "function")
//				{
//					this.callbacks["callback" + index](result);
//				}
//			},
//			
//			exec: function(command, params)
//			{
//				var jsonParams = {};
//
//				if (typeof(params) != "undefined")
//				{
//					jsonParams = params;
//					
//					if (typeof(jsonParams) == "object")
//						jsonParams = JSON.stringify(jsonParams);
//				}
//				else
//				{
//					jsonParams = "{}";
//				}
//
//				if (this.getPlatform() == "android")
//				{
//					return exec.postMessage(command, jsonParams);	
//				}
//				else
//				{
//					return window.webkit.messageHandlers.exec.postMessage('{"command":"' + command + '", "params":' + jsonParams + '}');
//				}
//				
//			},
			
//			loadPage: function(url, flags)
//			{
//				if (this.isDevice())
//				{
//					url = BX.util.add_url_param(url, flags);
//				}
//			
//				location.href = url;
//			}
			
		};


//	if (BM.MobileApp.isDevice())
//	{
//		navigator.geolocation.getCurrentPosition = function(success, error, options)
//		{
//			var params = {};
//				
//			if (typeof(success) == "function")
//			{
//				params.success = BM.MobileApp.registerCallBack(success);
//			}
//				
//			if (typeof(error) == "function")
//			{
//				params.error = BM.MobileApp.registerCallBack(error);
//			}
//				
//			if (typeof(options) != "undefined")
//			{
//				params.options = options;
//			}
//					
//			BM.MobileApp.exec("getCurrentPosition", params);
//		};
//				
////		history.back = function()
////		{
////			BM.MobileApp.exec("historyBack");
////		};
//	}
	
})(window);