;(function(window){
	
	if (!window.BM)
	{
		if (typeof(console) == "object") console.error("Pull notice: mlab.appforsale core not loaded");
		return;
	}
	
	if (window.BM.Pull)
	{
		if (typeof(console) == "object") console.error("Pull notice: script is already loaded");
		return;
	}
	
	BM.Pull = {
		
		updateStateTimeout: null,
		pullTryConnect: false,
		time: (new Date(2022, 2, 19)).toUTCString(),
		tag: 1,
		mid: null,
		channelId: null,
		channelLastId: 0,
		channelStack: {},
		path: "",
		connected: false,
		sendAjaxTry: 0,
		pathToAjax: "/bitrix/tools/mlab_appforsale/pull_request.php?",
	
		start: function(params)
		{
			if (typeof(params) != "object")
			{
				params = {};
			}
			
			this.pullTryConnect = true;
			
			if (params.PATH)
			{
				this.path = params.PATH;
			}
			
			this.connectWebSocket();
		},
		
		connectWebSocket: function()
		{			
			var wsPath = this.path.replace("#DOMAIN#", location.hostname);
			var wsServer = wsPath + (this.tag != null ? "&tag=" + this.tag : "") + (this.time != null ? "&time=" + this.time :"") + (this.mid !== null ? "&mid=" + this.mid : "");
			
			try
			{
				ws = new WebSocket(wsServer);	
			}
			catch(e)
			{
				if (typeof(console) == "object")
				{
					console.error(e);
				}
				
				this.path = null;
				return false;
			}
			
			ws.onopen = function()
			{
				this.connected = true;
			};
			
			ws.onclose = function(data)
			{
				var code = typeof(data.code) != "undefined" ? data.code : "NA";
				var reason = "";
				if (data.reason)
				{
					try
					{
						reason = JSON.parse(data.reason);
					}
					catch(e)
					{
						reason = {"reason": data.reason};
					}
				}
				
				if (!this.connected)
				{
					
				}
				else
				{				
					this.connected = false;
					clearTimeout(this.updateStateTimeout);
					if (data.wasClean && reason && reason.http_status == 403)
					{
						this.channelId = null;
						this.channelLastId = 0;
						this.channelStack = {};
						
						this.updateStateTimeout = setTimeout(function() {
							BM.Pull.getChannelId();
						}, BM.Pull.sendAjaxTry < 2 ? 1000 : BM.Pull.tryConnectTimeout());
					}
					else
					{
						this.updateStateTimeout = setTimeout(function() {
							BM.Pull.connectWebSocket();
						}, BM.Pull.sendAjaxTry < 2 && data.wasClean === true ? 1000 : BM.Pull.tryConnectTimeout());
					}
				}
				
				if (typeof(console) == "object")
				{
					console.warn(code + " " + JSON.stringify(reason));
				}

			};
			
			ws.onmessage = function(evente)
			{
				var dataArray = event.data.match(/#!NGINXNMS!#(.*?)#!NGINXNME!#/gm);
				if (dataArray != null)
				{
					for (var i = 0; i < dataArray.length; i++)
					{
						dataArray[i] = dataArray[i].substring(12, dataArray[i].length - 12);
						if (dataArray[i].length <= 0)
							continue;
						
						var message = BX.parseJSON(dataArray[i]);
						
						if (message.id)
						{
							message.id = parseInt(message.id);
							message.mid = message.mid ? message.mid : (message.text.channel ? message.text.channel : message.time) + message.id;
														
							if (!BM.Pull.channelStack[message.mid])
							{
								BM.Pull.channelStack[message.mid] = true;

								if (BM.Pull.channelLastId < message.id)
								{
									BM.Pull.channelLastId = message.id;
								}

								BX.onCustomEvent(window, "onPullEvent-mlab.appforsale", [message.text.command, message.text.params], true);
							}							
						}
						
						if (message.tag)
						{
							BM.Pull.tag = message.tag;
						}
						
						if (message.time)
						{
							BM.Pull.time = message.time;
						}
						
						if (message.mid)
						{
							BM.Pull.mid = message.mid;
						}
						
					}
				}
			}
		},
		
		tryConnectTimeout: function()
		{
			var timeout = 0;
			
			if (this.sendAjaxTry <= 2)
			{
				timeout = 15000;
			}
			else if (this.sendAjaxTry > 2 && this.sendAjaxTry <= 5)
			{
				timeout = 45000;
			}
			else if (this.sendAjaxTry > 5 && this.sendAjaxTry <= 10)
			{
				timeout = 600000;
			}	
			else if (this.sendAjaxTry > 10)
			{
				this.pullTryConnect = false;
				timeout = 3600000;
			}

			return timeout;
		},
		
		getChannelId: function()
		{
			if (!this.pullTryConnect)
			{
				return false;
			}
						
			BX.ajax({
					url: this.pathToAjax + "GET_CHANNEL",
					method: "POST",
					dataType: "json",
					timeout: 30,
					data: { "PULL_GET_CHANNEL": "Y", "sessid": BX.bitrix_sessid()},
					onsuccess: BX.delegate(function(data) {
						
						if (typeof(data) == "object" && data.ERROR == "")
						{
							this.sendAjaxTry++;
							BM.Pull.connectWebSocket();
						}
						else
						{
							this.sendAjaxTry++;
						}
						
					}, this),
					onfailure: BX.delegate(function(data) {
									
					}, this),
			});
		}
		
	};
	
		
})(window);