<?
if (!check_bitrix_sessid())
	return;

IncludeModuleLangFile(__FILE__);

if ($ex = $APPLICATION->GetException())
{
	echo CAdminMessage::ShowMessage(
			array(
					"TYPE" => "ERROR",
					"MESSAGE" => GetMessage("MOD_INST_ERR"),
					"DETAILS" => $ex->GetString(),
					"HTML" => true
			)
	);
}
else
{
	echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));
}
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="submit" name="" value="<?echo GetMessage("MOD_BACK")?>"> <a class="adm-btn adm-btn-green" href="/bitrix/admin/wizard_install.php?wizardName=mlab:appforsale&lang=<?=LANG?>&sessid=<?=bitrix_sessid()?>"><?=GetMessage('WIZARD')?></a>
</form>