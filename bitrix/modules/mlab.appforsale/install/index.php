<?
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

class mlab_appforsale extends CModule
{
    var $MODULE_ID = "mlab.appforsale";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    
    var $PARTNER_NAME;
    var $PARTNER_URI;
    
    var $FOLDER = "youdo";
    var $TASK_IBLOCK_ID;
    var $PROFILE_IBLOCK_ID;
    var $OFFER_IBLOCK_ID;
    var $COMMENT_IBLOCK_ID;
   
	function mlab_appforsale()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		} 
		
		$this->MODULE_NAME = GetMessage("APPFORSALE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("APPFORSALE_MODULE_DESCRIPTION");
		
		$this->PARTNER_NAME = "Malahovsky labs";
		$this->PARTNER_URI = "http://promo.malahovsky.net/mobilesearchartist";
	}

	function DoInstall()
	{
		global $APPLICATION, $step;
		$step = intval($step);
		
		$this->InstallDB();
		$this->InstallFiles();
		
		$APPLICATION->IncludeAdminFile(
				GetMessage("APPFORSALE_INSTALL_TITLE"),
				$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/step.php"
		);
	}

	function DoUninstall()
	{
		global $APPLICATION, $step;
		$step = intval($step);
		
		$this->UnInstallDB();
		$this->UnInstallFiles();
				
		$APPLICATION->IncludeAdminFile(
			GetMessage("APPFORSALE_UNINSTALL_TITLE"),
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/unstep.php"
		);
	}
	
	function InstallDB()
	{
		global $DB, $APPLICATION;
		
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "CAppforsale", "OnAfterIBlockElementAdd");
		RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "CAppforsale", "OnAfterIBlockElementUpdate");
		
		RegisterModuleDependences("main", "OnBuildGlobalMenu", "mlab.appforsale", "CAppforsale", "OnBuildGlobalMenu");
		
			RegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", "mlab.appforsale", "CAppforsaleRecurring", "OnBeforeIBlockElementAdd");
		RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "mlab.appforsale", "CAppforsaleRecurring", "OnAfterIBlockElementAdd");
		
		CAgent::AddAgent("CAppforsaleRecurring::AgentCheckRecurring();", "mlab.appforsale", "N", 7200, "", "Y");
		
		
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		
		$eventManager->registerEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Task', 'OnAfterUserDelete');
		$eventManager->registerEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Profile', 'OnAfterUserDelete');
		$eventManager->registerEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Offer', 'OnAfterUserDelete');
		$eventManager->registerEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnAfterUserDelete');
		$eventManager->registerEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Notification', 'OnAfterUserDelete');
		
		
		
		
		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockPropertyAdd', 'mlab.appforsale', '\Mlab\Appforsale\Property', 'OnBeforeIBlockProperty');
		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockPropertyUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Property', 'OnBeforeIBlockProperty');
		
		
		$eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Status', 'GetUserTypeDescription');
		$eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Iblock', 'GetUserTypeDescription');
		$eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Property\Rating', 'GetUserTypeDescription');
		
		
		
		$eventManager->registerEventHandler('iblock', 'OnIBlockElementSetPropertyValues', 'mlab.appforsale', 'CAppforsaleStatus', 'OnIBlockElementSetPropertyValues');
		$eventManager->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Address', 'GetUserTypeDescription');
		
		$eventManager->registerEventHandler('main', 'OnPanelCreate', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnPanelCreate');
		$eventManager->registerEventHandler('main', 'OnAdminTabControlBegin', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnAdminTabControlBegin');
		$eventManager->registerEventHandler('main', 'OnBeforeProlog', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnBeforeProlog');
		
		RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "mlab.appforsale", "CAppforsaleTask", "OnAfterIBlockElementAdd");
		RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "mlab.appforsale", "CAppforsaleTask", "OnAfterIBlockElementUpdate");

		
		RegisterModuleDependences("main", "OnAfterUserAdd", "mlab.appforsale", "CAppforsale", "OnAfterUserAdd");
		
		$eventManager->registerEventHandler('main', 'OnUserTypeBuildList', 'mlab.appforsale', '\Mlab\Appforsale\UserType\Property', 'GetUserTypeDescription');

		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Offer', 'OnBeforeIBlockElementAdd');
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Offer', 'OnAfterIBlockElementAdd');
		
		
		
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', 'CAppforsaleStatus', 'OnIBlockElementSetPropertyValues');
		$eventManager->registerEventHandler('iblock', 'OnIBlockElementDelete', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Delete', 'OnIBlockElementDelete', 1);
 		
		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnBeforeIBlockElementUpdate', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnAfterIBlockElementAdd', 200);
		
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnAfterIBlockElementAdd', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Offer', 'OnAfterIBlockElementAdd', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Executor', 'OnAfterIBlockElementSetPropertyValuesEx', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Done', 'OnAfterIBlockElementSetPropertyValuesEx', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Archive', 'OnAfterIBlockElementSetPropertyValuesEx', 200);
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Comment', 'OnAfterIBlockElementAdd', 200);
		
		
		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Task', 'OnBeforeIBlockElementAdd');
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Task', 'OnAfterIBlockElementAdd');
		
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Reject', 'OnAfterIBlockElementUpdate');
		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnBeforeIBlockElementAdd');
		$eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnAfterIBlockElementAdd');
		
		RegisterModuleDependences("main", "OnBeforeProlog", "main", "", "", 100, "/modules/mlab.appforsale/prolog_before.php");
		
		$eventManager->registerEventHandler('main', 'OnUserTypeBuildList', 'mlab.appforsale', '\Mlab\Appforsale\UserType\User', 'GetUserTypeDescription');
		
		
		$eventManager->registerEventHandler('main', 'OnUserLogin', 'mlab.appforsale', '\Mlab\Appforsale\Push\Manager', 'onUserLogin');
		$eventManager->registerEventHandler('main', 'OnUserLogout', 'mlab.appforsale', '\Mlab\Appforsale\Push\Manager', 'onUserLogout');
		$eventManager->registerEventHandler('main', 'OnUserLogin', 'mlab.appforsale', '\Mlab\Appforsale\Geolocation\Manager', 'onUserLogin');
		$eventManager->registerEventHandler('main', 'OnUserLogout', 'mlab.appforsale', '\Mlab\Appforsale\Geolocation\Manager', 'onUserLogout');
		
		$eventManager->registerEventHandler('main', 'OnEpilog', 'mlab.appforsale', '\Mlab\Appforsale\Pull\Manager', 'onEpilog');
		
		$eventManager->registerEventHandler("mlab.appforsale", "onCurrentPosition", "mlab.appforsale", "CAppforsalePosition", "onCurrentPosition");
		
		
		CAgent::AddAgent("Mlab\Appforsale\Pull\Manager::checkExpireAgent();", "mlab.appforsale", "N", 43200, "", "Y", ConvertTimeStamp(time()+CTimeZone::GetOffset() + 43200, "FULL"));
		
		
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/db/" . strtolower($DB->type) . "/install.sql");

		return true;
	}
	
	
	function UnInstallDB()
	{
		global $DB, $APPLICATION;

		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/db/" . strtolower($DB->type) . "/uninstall.sql");
		
		CAgent::RemoveModuleAgents($this->MODULE_ID);
		
		UnRegisterModule($this->MODULE_ID);
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", $this->MODULE_ID, "CAppforsale", "OnAfterIBlockElementAdd");
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", $this->MODULE_ID, "CAppforsale", "OnAfterIBlockElementUpdate");
		UnRegisterModuleDependences("main", "OnBeforeProlog", "main", "", "", "/modules/mlab.appforsale/prolog_before.php");
		
		UnRegisterModuleDependences("main", "OnBuildGlobalMenu", "mlab.appforsale", "CAppforsale", "OnBuildGlobalMenu");
		
			UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementAdd", "mlab.appforsale", "CAppforsaleRecurring", "OnBeforeIBlockElementAdd");
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "mlab.appforsale", "CAppforsaleRecurring", "OnAfterIBlockElementAdd");

		UnRegisterModuleDependences("main", "OnAfterUserAdd", "mlab.appforsale", "CAppforsale", "OnAfterUserAdd");
		
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		
		$eventManager->unRegisterEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Task', 'OnAfterUserDelete');
		$eventManager->unRegisterEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Profile', 'OnAfterUserDelete');
		$eventManager->unRegisterEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Offer', 'OnAfterUserDelete');
		$eventManager->unRegisterEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnAfterUserDelete');
		$eventManager->unRegisterEventHandler('main', 'OnAfterUserDelete', 'mlab.appforsale', '\Mlab\Appforsale\Notification', 'OnAfterUserDelete');
		
		
		
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockPropertyAdd', 'mlab.appforsale', '\Mlab\Appforsale\Property', 'OnBeforeIBlockProperty');
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockPropertyUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Property', 'OnBeforeIBlockProperty');
	$eventManager->unRegisterEventHandler('iblock', 'OnIBlockElementDelete', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Delete', 'OnIBlockElementDelete', 1);
 		
		$eventManager->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Property\Rating', 'GetUserTypeDescription');
		
		$eventManager->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Status', 'GetUserTypeDescription');
		$eventManager->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Iblock', 'GetUserTypeDescription');
		$eventManager->unRegisterEventHandler('iblock', 'OnIBlockElementSetPropertyValues', 'mlab.appforsale', 'CAppforsaleStatus', 'OnIBlockElementSetPropertyValues');
		$eventManager->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', 'mlab.appforsale', '\Mlab\Appforsale\Address', 'GetUserTypeDescription');
		
		$eventManager->unRegisterEventHandler('main', 'OnPanelCreate', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnPanelCreate');
		$eventManager->unRegisterEventHandler('main', 'OnAdminTabControlBegin', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnAdminTabControlBegin');
		$eventManager->unRegisterEventHandler('main', 'OnBeforeProlog', 'mlab.appforsale', '\Mlab\Appforsale\Design', 'OnBeforeProlog');
		
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "mlab.appforsale", "CAppforsaleTask", "OnAfterIBlockElementAdd");
		UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "mlab.appforsale", "CAppforsaleTask", "OnAfterIBlockElementUpdate");
		
		$eventManager->unRegisterEventHandler('main', 'OnUserTypeBuildList', 'mlab.appforsale', '\Mlab\Appforsale\UserType\Property', 'GetUserTypeDescription');
		$eventManager->unRegisterEventHandler('main', 'OnUserTypeBuildList', 'mlab.appforsale', '\Mlab\Appforsale\UserType\User', 'GetUserTypeDescription');
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Offer', 'OnBeforeIBlockElementAdd');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Offer', 'OnAfterIBlockElementAdd');
		
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', 'CAppforsaleStatus', 'OnIBlockElementSetPropertyValues');
		
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnBeforeIBlockElementUpdate');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnAfterIBlockElementAdd');
		
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Task', 'OnAfterIBlockElementAdd');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Offer', 'OnAfterIBlockElementAdd');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Executor', 'OnAfterIBlockElementSetPropertyValuesEx');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Done', 'OnAfterIBlockElementSetPropertyValuesEx');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementSetPropertyValuesEx', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Archive', 'OnAfterIBlockElementSetPropertyValuesEx');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Comment', 'OnAfterIBlockElementAdd');
		
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Task', 'OnBeforeIBlockElementAdd');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Payment\Task', 'OnAfterIBlockElementAdd');
				
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'mlab.appforsale', '\Mlab\Appforsale\Notification\Reject', 'OnAfterIBlockElementUpdate');
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnBeforeIBlockElementAdd');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', 'mlab.appforsale', '\Mlab\Appforsale\Comment', 'OnAfterIBlockElementAdd');
		
		
		$eventManager->unRegisterEventHandler('main', 'OnUserLogin', 'mlab.appforsale', '\Mlab\Appforsale\Push\Manager', 'onUserLogin');
		$eventManager->unRegisterEventHandler('main', 'OnUserLogout', 'mlab.appforsale', '\Mlab\Appforsale\Push\Manager', 'onUserLogout');
		$eventManager->unRegisterEventHandler('main', 'OnUserLogin', 'mlab.appforsale', '\Mlab\Appforsale\Geolocation\Manager', 'onUserLogin');
		$eventManager->unRegisterEventHandler('main', 'OnUserLogout', 'mlab.appforsale', '\Mlab\Appforsale\Geolocation\Manager', 'onUserLogout');
		
		$eventManager->unRegisterEventHandler('main', 'OnEpilog', 'mlab.appforsale', '\Mlab\Appforsale\Pull\Manager', 'onEpilog');
		
		$eventManager->unRegisterEventHandler("mlab.appforsale", "onCurrentPosition", "mlab.appforsale", "CAppforsalePosition", "onCurrentPosition");
		
		return true;
	}
	
	function InstallFiles()
	{
		CopyDirFiles(
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/images/',
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/images/',
			true, true
		);
		
		CopyDirFiles(
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/tools/',
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/tools/',
			true, true
		);
		
		CopyDirFiles(
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/admin/',
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/',
			true, true
		);
		
		CopyDirFiles(
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/themes/',
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/',
			true, true
		);
		

		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/js/', $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.$this->MODULE_ID.'/install/components/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/', true, true);
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/wizards/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/', true, true);

		return true;
	}
	
	function UnInstallFiles()
	{		
		DeleteDirFilesEx("/bitrix/js/appforsale");		
		DeleteDirFilesEx("/bitrix/components/appforsale");
		DeleteDirFilesEx("/bitrix/components/mlab");
		DeleteDirFilesEx('/bitrix/wizards/mlab/');
		
		return true;
	}
}
?>	