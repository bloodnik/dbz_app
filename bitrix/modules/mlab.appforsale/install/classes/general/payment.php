<? 
class CAppforsalePayment
{
	function bonus($amount)
	{
		$bonus_balance = intval(COption::GetOptionInt('mlab.appforsale', 'bonus_balance', 0));
		$bonus_type_balance = COption::GetOptionInt('mlab.appforsale', 'bonus_type_balance', 0);
		if ($bonus_balance > 0)
		{
			if ($bonus_type_balance == 0)
			{
				$amount += $bonus_balance;
			}
			elseif ($bonus_type_balance == 1)
			{
				$amount += $amount/100*$bonus_balance;
			}
		}
		
		return $amount;
	}
	
	function processRequest(Bitrix\Main\Request $request)
	{
		if ($request->get('notification_type'))
		{
			if ($request->get('label') && $request->get('sha1_hash') == sha1($request->get('notification_type').'&'.$request->get('operation_id').'&'.$request->get('amount').'&'.$request->get('currency').'&'.$request->get('datetime').'&'.$request->get('sender').'&'.$request->get('codepro').'&'.COption::GetOptionString('mlab.appforsale', 'notification_secret', '').'&'.$request->get('label')))
			{
				if (!CAppforsaleUserAccount::UpdateAccount($request->get('label'), CAppforsalePayment::bonus($request->get('withdraw_amount')), 'OUT_CHARGE_OFF', 0, 'operation_id: ' .$request->get('operation_id')))
					CHTTP::SetStatus('400 Bad Request');
			}
			else
			{
				CHTTP::SetStatus('400 Bad Request');
			}
		}
		else
		{
			$performedDatetime = date("Y-m-d") . "T" . date("H:i:s") . ".000" . date("P");
			
			echo '<?xml version="1.0" encoding="UTF-8" ?>';
			if ($request->get("md5") == strtoupper(md5($request->get("action").';'.$request->get("orderSumAmount").';'.$request->get("orderSumCurrencyPaycash").';'.$request->get("orderSumBankPaycash").';'.$request->get("shopId").';'.$request->get("invoiceId").';'.$request->get("customerNumber").';'.COption::GetOptionString('mlab.appforsale', "shopPassword", ""))))
			{
				switch ($request->get("action"))
				{
					case "checkOrder":
						echo '<checkOrderResponse performedDatetime="'.$performedDatetime.'" code="0" invoiceId="'.$request->get("invoiceId").'" shopId="'.$request->get("shopId").'" />';
						break;
					case "paymentAviso":
						if (CAppforsaleUserAccount::UpdateAccount($request->get("customerNumber"), CAppforsalePayment::bonus($request->get("orderSumAmount")), "OUT_CHARGE_OFF", 0, "invoiceId: " .$request->get("invoiceId")))
							echo '<paymentAvisoResponse performedDatetime="'.$performedDatetime.'" code="0" invoiceId="'.$request->get("invoiceId").'" shopId="'.$request->get("shopId").'" />';
						else 
						{
							global $APPLICATION;
							$ex = $APPLICATION->GetException();
							echo '<paymentAvisoResponse performedDatetime="'.$performedDatetime.'" code="1" message="'.$ex->GetString().'" />';
						}
						break;
				}
			}
			else
			{
				echo '<'.$request->get("action").'Response performedDatetime="'.$performedDatetime.'" code="1" message="Значение параметра md5 не совпадает с результатом расчета хэш-функции" />';
			}
		}
	}
}
?>