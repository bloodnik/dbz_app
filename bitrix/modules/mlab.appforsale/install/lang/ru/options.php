<? 
$MESS['NOTIFICATION'] = 'Уведомления';
$MESS['CONFIGURE_PUSH_NOTIFICATION'] = 'Push-уведомления';
$MESS['CONFIGURE_SMS_NOTIFICATION'] = 'Sms-уведомления';
$MESS['URL'] = 'Url';
$MESS['STATUS'] = 'Статус';
$MESS['ACTIVE'] = 'Активен';
$MESS['ERROR_CURL'] = 'Не установлена библиотека cURL';
$MESS['ERROR_KEY'] = 'Неверный ключ';
$MESS['CONFIGURE'] = 'Настройки';
$MESS['API_KEY'] = 'Ключ API';
$MESS['GOOGLE_INFO'] = 'Чтобы получить ключ API, посетите сайт <a href="https://developers.google.com/mobile/add" target="_blank">Google Developers</a>.';
$MESS['SMS_INFO'] = '#PHONE# - номер телефона, #CODE# - код подтверждения';
$MESS['MULTIPROFILES'] = 'Мультипрофиль';
$MESS['ANALYTICS'] = 'Аналитика';
$MESS['GOOGLE_ANALYTICS_KEY'] = 'Ключ Google Analytics';

$MESS['OTHER'] = 'Прочие';

$MESS['APPFORSALE_YANDEX_MONEY_TAB'] = 'Яндекс.Деньги';
$MESS['APPFORSALE_YANDEX_MONEY_TITLE'] = 'Параметры платежной системы';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER'] = 'Номер кошелька';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_ALT'] = 'Номер кошелька в Яндекс.Деньги';
$MESS['APPFORSALE_YANDEX_MONEY_RECEIVER_INFO'] = 'Чтобы зарегистрировать новый кошелек, посетите сайт <a href="https://money.yandex.ru/reg/" target="_blank">Яндекс.Денег</a>';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET'] = 'Секретное слово';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_ALT'] = 'Секрет, который получен от Яндекс';
$MESS['APPFORSALE_YANDEX_MONEY_NOTIFICATION_SECRET_INFO'] = 'Получите секретное слово в <a href="https://money.yandex.ru/myservices/online.xml" target="_blank">настройках Яндекс.Денег</a> и укажите http://'.$_SERVER['SERVER_NAME'].'/bitrix/tools/appforsale_ps_result.php в качестве адреса для уведомлений';

$MESS['APPFORSALE_YANDEX_KASSA_TAB'] = 'Яндекс.Касса';
$MESS['APPFORSALE_YANDEX_KASSA_TITLE'] = 'Параметры платежной системы';
$MESS['APPFORSALE_YANDEX_INFO'] = 'Используется протокол commonHTTP-3.0<br />Работа через Центр Приема Платежей <a href="https://kassa.yandex.ru" target="_blank">https://kassa.yandex.ru</a>';
$MESS['APPFORSALE_YANDEX_CHECK'] = 'Проверка HTTPS';
$MESS['APPFORSALE_YANDEX_CHECK_ALT'] = 'Проверка доступности сайта по протоколу HTTPS. Необходимо для корректной работы платежной системы';
$MESS['APPFORSALE_YANDEX_ERROR'] = 'Ошибка';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_ADDITIONAL'] = 'Дополнительные настройки платежной системы';
$MESS['APPFORSALE_YANDEX_KASSA_DEMO'] = 'Тестовый режим';
$MESS['APPFORSALE_YANDEX_KASSA_HEADING_CONNECTION'] = 'Настройки подключения Яндекса';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID'] = 'Идентификатор магазина в ЦПП (ShopID)';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPID_ALT'] = 'Код магазина, который получен от Яндекс';
$MESS['APPFORSALE_YANDEX_KASSA_SCID'] = 'Номер витрины магазина в ЦПП (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SCID_ALT'] = 'Номер витрины магазина в ЦПП (scid)';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD'] = 'Пароль магазина';
$MESS['APPFORSALE_YANDEX_KASSA_SHOPPASSWORD_ALT'] = 'Пароль магазина на Яндекс';



$MESS['APPFORSALE_RUB'] = 'руб.';
$MESS['APPFORSALE_OTHER_FORMAT_STRING'] = 'Строка формата для вывода валюты';

$MESS['APPFORSALE_OTHER_HEADING_USER'] = 'Настройки пользователя';
$MESS['APPFORSALE_OTHER_CREDIT'] = 'Стартовый баланс пользователя';
$MESS['APPFORSALE_OTHER_CREDIT_ALT'] = 'Данная сумма будет начисляться вновь зарегистрированным пользователям';
$MESS['APPFORSALE_BONUS'] = 'Бонус при зачислении средств на счет';
$MESS['APPFORSALE_BONUS_RUB'] = 'Руб.';
$MESS['APPFORSALE_BONUS_PERCENT'] = '%';
?>