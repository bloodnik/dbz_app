<?
use Bitrix\Main\Localization\Loc;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arWizardDescription = array(
	'NAME' => Loc::getMessage('WIZARD_NAME'),
	'DESCRIPTION' => Loc::getMessage('WIZARD_DESC'),
	'VERSION' => '1.0.0',
	'START_TYPE' => 'WINDOW',
	'WIZARD_TYPE' => 'INSTALL',
	'PARENT' => 'wizard_sol',
	'TEMPLATES' => array(
		array(
			'SCRIPT' => 'wizard_sol'
		)
	),
	'STEPS' => array(
		'SiteSettingsStep',
		'DataInstallStep',
		'FinishStep'
	)
);
?>