<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/install/wizard_sol/wizard.php');

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		parent::InitStep();
	
 		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
	
 		$siteID = $wizard->GetVar('siteID');
 		$isWizardInstalled = Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', $siteID) == 'Y';
	
 		$this->SetNextStep("data_install");

		$wizard->SetDefaultVars(Array(
		 	'siteLogo' => file_exists(WIZARD_SITE_PATH.'include/appforsale_logo.png') ? WIZARD_SITE_DIR.'include/appforsale_logo.png' : ($isWizardInstalled ? '' : '/bitrix/wizards/mlab/appforsale/site/templates/youdo_appforsale/images/logo.png')
		));
	}
	
	function ShowStep()
	{
		$wizard =& $this->GetWizard();
	
		$this->content .= '<div class="wizard-input-form">';
	
		$siteLogo = $wizard->GetVar("siteLogo", true);

		$this->content .= '
		<div class="wizard-input-form-block" style="background-color: #3f51b5; color: #fff; width: 571px; padding: 10px">
			<label for="siteLogo">'.GetMessage("WIZ_COMPANY_LOGO").'</label><br/>';
		$this->content .= CFile::ShowImage($siteLogo, 215, 50, "border=0 vspace=15");
		$this->content .= "<br/>".$this->ShowFileField("siteLogo", Array("show_file_info" => "N", "id" => "siteLogo")).
		'</div>';

		$this->content .= '</div>';
	}
	
	function OnPostForm()
	{
		$wizard =& $this->GetWizard();
		$res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 150, "max_width" => 500, "make_preview" => "Y"));
	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function CorrectServices(&$arServices)
	{

	}
}

class FinishStep extends CFinishWizardStep
{
	function InitStep()
	{
		$this->SetStepID('finish');
		$this->SetNextStep('finish');
		$this->SetTitle(GetMessage('FINISH_STEP_TITLE'));
		$this->SetNextCaption(GetMessage('wiz_go'));  
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		$siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));
		$rsSites = CSite::GetByID($siteID);
		$siteDir = "/";
		if ($arSite = $rsSites->Fetch())
			$siteDir = $arSite["DIR"];
		
		$wizard->SetFormActionScript(str_replace('//', '/', $siteDir.'/?finish'));
		
		$this->CreateNewIndex();
		
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'wizard_installed', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installNotification', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installLanding', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installSite', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installSubscription', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installInvite', 'Y', WIZARD_SITE_ID);
		Bitrix\Main\Config\Option::set('mlab.appforsale', 'installExecutor', 'Y', WIZARD_SITE_ID);
		
		$this->content .=
		'<table class="wizard-completion-table">
				<tr>
					<td class="wizard-completion-cell">'
				.GetMessage('FINISH_STEP_CONTENT').
				'</td>
				</tr>
			</table>';
	}
}
?>