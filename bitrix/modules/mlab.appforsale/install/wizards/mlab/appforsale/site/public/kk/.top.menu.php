<?
$aMenuLinks = Array(
	Array(
		"Құруға тапсырма", 
		"youdo/tasks-new/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task_add.png"), 
		"" 
	),
	Array(
		"Барлық тапсырмалар", 
		"youdo/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task.png"), 
		"" 
	),
	Array(
		"Орындаушылар", 
		"youdo/executors/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_users.png"), 
		"" 
	),
	Array(
		"Менің тапсырыстарым", 
		"youdo/tasks-customer/", 
		Array("/youdo/tasks-executor/"), 
		Array("img"=>"/youdo/images/menu_task_customer.png"), 
		"" 
	),
	Array(
		"Хабарлама", 
		"youdo/notification/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_notification.png"), 
		"" 
	),
	Array(
		"Көрсетілетін қызмет түрлері", 
		"youdo/personal/profile/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_subscribe.png"), 
		"" 
	),
	Array(
		"Бірін шақыра",
		"youdo/invite/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_invite.png"),
		""
	),
	Array(
		"Параметрлер",
		"youdo/personal/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_setting.png", "title"=>"Профиль"),
		""
	)
);
?>