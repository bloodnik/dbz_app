<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('To create a job');
?><?$APPLICATION->IncludeComponent(
	"mlab:appforsale.task.new",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "#TASK_IBLOCK_ID#",
		"IBLOCK_TYPE" => "tasks",
		"SECTIONS_FIELD_CODE" => array("",""),
		"SEF_FOLDER" => "/youdo/tasks-new/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("form"=>"#SECTION_CODE_PATH#/form/","sections"=>"#SECTION_CODE_PATH#/")
	)
);?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>