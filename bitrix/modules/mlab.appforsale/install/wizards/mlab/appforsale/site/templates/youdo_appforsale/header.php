<?
use Mlab\Appforsale\MobileApp;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!CModule::IncludeModule('mlab.appforsale'))
	die();

MobileApp::getInstance()->initializeBasicKernel();

CAppforsale::Init();
CJSCore::Init(array('fx', 'ajax'));
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
    	<?$APPLICATION->ShowHead();?>
    	<?
    	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/colors.css', true);
    	$APPLICATION->SetAdditionalCSS('/bitrix/css/main/bootstrap.css');
    	$APPLICATION->SetAdditionalCSS('/bitrix/css/main/font-awesome.css');
    	?>
    	<title><?$APPLICATION->ShowTitle()?></title>
    	
    	
    	
    	<meta charset="<? echo SITE_CHARSET?>" />
    	<? 
		$APPLICATION->AddHeadScript("https://code.jquery.com/jquery-latest.min.js");
    	?>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
    	<script type="text/javascript">

    	function updatePanel() {

			var obShefPanel = BX('menu2');
			var pageWrap = BX('afs-page-wrap');
    		if (!!obShefPanel && !!pageWrap)
    		{
	    		if (BX.admin.panel.isFixed() === true)
	    		{
	    			obShefPanel.style.top = BX.admin.panel.DIV.offsetHeight + 'px';
	    			obShefPanel.style.position = 'fixed';
	    			pageWrap.style.marginTop = '56px';
	            }
	    		else
	    		{
					if (BX.scrollTop(window) > BX.admin.panel.DIV.offsetHeight)
					{
						obShefPanel.style.top  = '0px';
		    			obShefPanel.style.position = 'fixed';
		    			pageWrap.style.marginTop = '56px';
					}
					else
					{
		    			obShefPanel.style.top  = '0px';
		    			obShefPanel.style.position = 'relative';
		    			if (BX.width(window) > 991)
		    				pageWrap.style.marginTop = '0px';
		    			else
		    				pageWrap.style.marginTop = '56px';
					}
	        	}
    		}	
        }

    	BX.ready(function() {
    		app.hideProgress();

    		if (BX.admin && BX.admin.panel)
    		{
	    		updatePanel();
	    		BX.addCustomEvent('onTopPanelCollapse', BX.delegate(updatePanel, this));
	    		BX.addCustomEvent('onTopPanelFix', BX.delegate(updatePanel, this));
	    		BX.bind(window, "scroll", BX.proxy(updatePanel, this));
	    		BX.bind(window, "resize", BX.proxy(updatePanel, this));
    		}
    	});	
    	
    	function checkEvent(e)
    	{
    	    return ((e = (e || window.event)) && (e.type == 'click' || e.type == 'mousedown' || e.type == 'mouseup') && (e.which > 1 || e.button > 1 || e.ctrlKey || e.shiftKey || BX.browser.IsMac() && e.metaKey)) || false;
    	}
    	
    	document.onclick = function(e)
    	{
    		if (checkEvent(e))
    		        return true;

    	    var i = 8,
            target = e.target || e.srcElement,
            href,
            w = window;
        	while (target && target != document.body && target.tagName != 'A' && i--)
            {
            	target = target.parentNode;
        	}
        	if (!target || target.tagName != 'A' || target.onclick || target.onmousedown)
            	return true;

        	href = target.href;

        	if (href && target.getAttribute('target'))
        	{
                try {
                	window.opener.location = href;
                    return cancelEvent(e);
                } catch (er) {
                    return true;
                }
            }
            if (href) {
            
        	if (/javascript/i.test(target.href))
				return true;
        	else
        	{
            	var title = '';
            	if (target.hasAttribute('title'))
            		title = target.getAttribute('title');
     
            	if (window.location.pathname == '/youdo/left.php')
            	{
            		app.loadPageStart({url: target.href.replace(window.location.protocol + "//" + window.location.hostname, ''), title: title});
                    
            	}
            	else
            	{
            		app.loadPageBlank({url: target.href.replace(window.location.protocol + "//" + window.location.hostname, ''), title: title});
                    
            	}
				
        	
                		return false;
           	}
            }
    	}
    	</script>

    	<?if (preg_match('/(ipad|iphone|android|mobile|touch)/i', $_SERVER['HTTP_USER_AGENT'])):?>
		<style>
			body {
				-webkit-tap-highlight-color: transparent;
				-webkit-user-select: none;
				-webkit-touch-callout: none; 
			}
		</style>
    	<?endif;?>
	</head>
	<body onresize="onBodyResize()"<?=($APPLICATION->GetCurPage() == '/youdo/left.php' ? ' class="left"' : '')?>>
	<?if ($_REQUEST['APPFORSALE'] != 'Y'):?>
		<div id="box_layer_bg"></div><div id="box_layer_wrap"><div id="box_layout"></div></div>
		<script type="text/javascript">domStarted();</script><div id="panel"><?$APPLICATION->ShowPanel()?></div><div id="afs-page-wrap" style="margin-top: <?=($APPLICATION->GetCurPage() != '/youdo/left.php' && $_COOKIE['APPLICATION'] != 'Y' ? '56' : '0')?>px">
	
<? 	
// 	if ($APPLICATION->GetCurPage() != '/youdo/left.php' && $_REQUEST['APPFORSALE'] != 'Y')
// {
?>

	<div id="menu2" class="afs-color" style="display: <?=($APPLICATION->GetCurPage() != '/youdo/left.php' && ($_COOKIE['APPLICATION'] == 'Y' || Mlab\Appforsale\MobileApp::getInstance()->isDevice()) ? 'none' : 'block')?>">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-12">
					<div class="afs-logo">
				 		<a href="/">
	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/appforsale_logo.php"), false);?>
						</a>
					</div>
				</div>
				<div class="col-md-7 hidden-xs hidden-sm">
					 <?
					 $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"appforsale", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "top",
		"COMPONENT_TEMPLATE" => "appforsale",
		"DELAY" => "Y",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"ASIDE_LOGIN_SHOW" => "Y",
		"ASIDE_LOGIN_BG" => "default",
		"ASIDE_BG" => "#FFFFFF"
	),
	false
);
					?>
				</div>
				<div class="col-md-3 hidden-xs hidden-sm">
					<?
					$APPLICATION->IncludeComponent(
						'mlab:appforsale.personal.line',
						'', 
						array(),
						false
					);
					?>
				</div>
			</div>
		</div>
	</div>
	<?// }?>
	
	
	<?endif;?>
	
<?
if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php' && $APPLICATION->GetCurPage() != '/youdo/left.php' && $_REQUEST['APPFORSALE'] != 'Y')
	echo '<div class="container"><div class="row">';
?>
	