<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_TASK_ID = WizardServices::ImportIBlockFromXML(
	WIZARD_SERVICE_RELATIVE_PATH.'/xml/'.LANGUAGE_ID.'/task.xml',
	'task',
	'tasks',
	WIZARD_SITE_ID,
	array(
		'1' => 'X',
		'2' => 'R'
	)
);

if ($IBLOCK_TASK_ID < 1)
	return;

$_SESSION['WIZARD_TASK_IBLOCK_ID'] = $IBLOCK_TASK_ID;
?>