<?
use Bitrix\Main\Loader;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'N')
{
	$IBLOCK_PROFILE_ID = (isset($_SESSION['WIZARD_PROFILE_IBLOCK_ID']) ? intval($_SESSION['WIZARD_PROFILE_IBLOCK_ID']) : 0);
	
	if ($IBLOCK_PROFILE_ID)
	{
		$iblockCode = 'profile_'.WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$arFields = array(
			'ACTIVE' => 'Y',
			'FIELDS' => array(
				'IBLOCK_SECTION' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => ''),
				'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown'),
				'SECTION_CODE' => array(
					'IS_REQUIRED' => 'Y',
					'DEFAULT_VALUE' => array(
						'UNIQUE' => 'Y',
						'TRANSLITERATION' => 'Y',
						'TRANS_LEN' => 100,
						'TRANS_CASE' => 'L',
						'TRANS_SPACE' => '-',
						'TRANS_OTHER' => '-',
						'TRANS_EAT' => 'Y',
						'USE_GOOGLE' => 'Y'
					)
				)
			),
			'LIST_MODE' => 'C',
			'CODE' => 'profile',
			'XML_ID' => $iblockCode
		);
		$iblock->Update($IBLOCK_PROFILE_ID, $arFields);
		
		
		
		
		
		$arLanguages = array();
		$dbLanguage = CLanguage::GetList($by, $order, array());
		while($arLanguage = $dbLanguage->Fetch())
			$arLanguages[] = $arLanguage['LID'];
		
		$arUserFields = array('UF_ICON_DARK', 'UF_FORMAT_NAME', 'UF_DETAIL_PROPERTY', 'UF_FORM_PROPERTY', 'UF_FORM_PROPERTY_REQ', 'UF_RECUR_TYPE_TEST', 'UF_RECUR_LENGTH_TEST', 'UF_RECUR_PRICE_TEST', 'UF_RECUR_TYPE', 'UF_RECUR_LENGTH', 'UF_RECUR_PRICE', 'UF_COUNT');
		foreach ($arUserFields as $userField)
		{
			$arLabelNames = Array();
			foreach($arLanguages as $languageID)
			{
				WizardServices::IncludeServiceLang('property_names.php', $languageID);
				$arLabelNames[$languageID] = GetMessage($userField);
			}
		
			$arProperty['EDIT_FORM_LABEL'] = $arLabelNames;
			$arProperty['LIST_COLUMN_LABEL'] = $arLabelNames;
			$arProperty['LIST_FILTER_LABEL'] = $arLabelNames;
		
			$dbRes = CUserTypeEntity::GetList(Array(), Array('ENTITY_ID' => 'IBLOCK_'.$IBLOCK_PROFILE_ID.'_SECTION', 'FIELD_NAME' => $userField));
			if ($arRes = $dbRes->Fetch())
			{
				if (in_array($userField, array('UF_DETAIL_PROPERTY', 'UF_FORM_PROPERTY', 'UF_FORM_PROPERTY_REQ')))
				{
					$arProperty['SETTINGS'] = $arRes['SETTINGS'];
					$arProperty['SETTINGS']['IBLOCK_ID'] = $IBLOCK_PROFILE_ID;
				}
				
				$userType = new CUserTypeEntity();
				$userType->Update($arRes['ID'], $arProperty);
			}
		}
		
		
		
		
		
		WizardServices::IncludeServiceLang('profile2.php');
		
		
		
		
		
		
		CUserOptions::SetOption(
			'form',
			'form_section_'.$IBLOCK_PROFILE_ID,
			array(
				'tabs' => GetMessage('FORM_SECTION_PROFILE_TABS')
			),
			true
		);
		
	// 	CUserOptions::SetOption(
	// 		'form',
	// 		'form_element_'.$IBLOCK_TASK_ID,
	// 		array(
	// 		'tabs' => 'edit1--#--Задание--,--ACTIVE--#--Активность--,--NAME--#--*Название--;--edit2--#--Типы заданий--,--SECTIONS--#--Разделы--;--'
	// 				),
	// 				true
	// 	);
		
	
		CUserOptions::SetOption(
		'list',
		'tbl_iblock_list_'.md5('tasks.'.$IBLOCK_PROFILE_ID),
		array(
			'columns' => 'NAME,ACTIVE,SECTION_CNT,ELEMENT_CNT,ID',
			'by' => 'id',
			'order' => 'asc',
			'page_size' => 100
		),
				true
		);
	
		$countUsers = count($_SESSION['WIZARD_USERS']);
		if ($countUsers > 0)
		{
			$dbSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), array('IBLOCK_ID' => $IBLOCK_PROFILE_ID), false, array('ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'));
			while ($arSection = $dbSections->Fetch())
			{
				if (($arSection['RIGHT_MARGIN'] - $arSection['LEFT_MARGIN']) == 1)
				{
					$obElement = new CIBlockElement;
					$obElement->Add(array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, 'NAME' => 'unknown', 'IBLOCK_SECTION_ID' => $arSection['ID'], 'CREATED_BY' => $_SESSION['WIZARD_USERS'][0]['ID']));
					
					$obElement = new CIBlockElement;
					$obElement->Add(array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, 'NAME' => 'unknown', 'IBLOCK_SECTION_ID' => $arSection['ID'], 'CREATED_BY' => $_SESSION['WIZARD_USERS'][1]['ID']));
					
					$obElement = new CIBlockElement;
					$obElement->Add(array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, 'NAME' => 'unknown', 'IBLOCK_SECTION_ID' => $arSection['ID'], 'CREATED_BY' => $_SESSION['WIZARD_USERS'][2]['ID']));
					
					$obElement = new CIBlockElement;
					$obElement->Add(array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, 'NAME' => 'unknown', 'IBLOCK_SECTION_ID' => $arSection['ID'], 'CREATED_BY' => $_SESSION['WIZARD_USERS'][3]['ID']));
					
					$obElement = new CIBlockElement;
					$obElement->Add(array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, 'NAME' => 'unknown', 'IBLOCK_SECTION_ID' => $arSection['ID'], 'CREATED_BY' => $_SESSION['WIZARD_USERS'][4]['ID']));
				}
			}
		}
	
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/api/setLocation.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/profile/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/profile/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/subscription/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/services/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('PROFILE_IBLOCK_ID' => $IBLOCK_PROFILE_ID));
	}
}
else if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installSubscription', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
			array(),
			array(
					'TYPE' => 'profiles',
					'SITE_ID' => WIZARD_SITE_ID,
					'ACTIVE'=>'Y',
					"CODE"=>'profile'
			),
			false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/subscription/index.php', array('PROFILE_IBLOCK_ID' => $arIBlock['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/services/index.php', array('PROFILE_IBLOCK_ID' => $arIBlock['ID']));
	}
}
else if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installExecutor', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
			array(),
			array(
					'TYPE' => 'profiles',
					'SITE_ID' => WIZARD_SITE_ID,
					'ACTIVE'=>'Y',
					"CODE"=>'profile'
			),
			false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('PROFILE_IBLOCK_ID' => $arIBlock['ID']));
	}
}
?>