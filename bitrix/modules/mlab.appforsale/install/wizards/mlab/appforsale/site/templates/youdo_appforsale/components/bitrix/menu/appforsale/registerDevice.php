<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

$user_id = 'NULL';
$date_auth = 'NULL';
if ($USER->IsAuthorized())
{
	$user_id = $USER->GetID();
	$date_auth = 'NOW()';
}

$DB -> Query(
			'INSERT INTO appforsale_device
				(DATE_CREATE, TIMESTAMP_X, DEVICE_ID, DEVICE_TYPE, DEVICE_NAME, DEVICE_MODEL, SYSTEM_VERSION, SETTINGS, TOKEN, USER_ID, DATE_AUTH)
				VALUES
				(NOW(), NOW(), "' . $DB->ForSql($request->get('DEVICE_ID')) . '", "' . $DB->ForSql($request->get('DEVICE_TYPE')) . '", "' . $DB->ForSql($request->get('DEVICE_NAME')) . '", "' . $DB->ForSql($request->get('DEVICE_MODEL')) . '", "' . $DB->ForSql($request->get('SYSTEM_VERSION')) . '", "' . $DB->ForSql($request->get('SETTINGS')) . '", "' . $DB->ForSql($request->get('TOKEN')) . '", ' . $user_id . ', ' . $date_auth . ')
				on duplicate key update
				TIMESTAMP_X = NOW(),
				DEVICE_NAME = "' . $DB->ForSql($request->get('DEVICE_NAME')) . '",
				SYSTEM_VERSION = "' . $DB->ForSql($request->get('SYSTEM_VERSION')) . '",
				TOKEN = "' . $DB->ForSql($request->get('TOKEN')) . '",
				USER_ID = ' . $user_id . ',
				DATE_AUTH = ' . $date_auth . '
');
?>