<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Исполнители');
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.executor', 
	'', 
	array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/youdo/executors/",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"IBLOCK_TYPE" => "profiles",
		"IBLOCK_ID" => "#PROFILE_IBLOCK_ID#",
		"COMMENT_IBLOCK_TYPE" => "comments",
		"COMMENT_IBLOCK_ID" => "#COMMENT_IBLOCK_ID#",
		"COMMENT_PROPERTY_CODE" => array('#COMMENT_PROPERTY_ID#', '#RATING_PROPERTY_ID#'),
		"SEF_URL_TEMPLATES" => array(
			"list" => "#SECTION_CODE_PATH#/",
			"detail" => "u#USER_ID#/",
		)
	),
	false
);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>