<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Мои заказы');
?><?$APPLICATION->IncludeComponent(
	"mlab:appforsale.task.customer", 
	".default", 
	array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"ELEMENT_SORT_FIELD" => "timestamp_x",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"IBLOCK_ID" => "#TASK_IBLOCK_ID#",
		"IBLOCK_TYPE" => "tasks",
		"OFFER_IBLOCK_ID" => "#OFFER_IBLOCK_ID#",
		"OFFER_IBLOCK_TYPE" => "offers",
		'OFFER_PROPERTY_CODE' => array('#OFFER_PROPERTY_ID#'),
		"SEF_FOLDER" => "/youdo/tasks-customer/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
			"comment" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/comment/"
		),
		"COMMENT_IBLOCK_TYPE" => "comments",
		"COMMENT_IBLOCK_ID" => "#COMMENT_IBLOCK_ID#",
		"COMMENT_PROPERTY_CODE" => array('#COMMENT_PROPERTY_ID#', '#RATING_PROPERTY_ID#'),
		"COMMENT_PROPERTY_CODE_REQUIRED" => array('#COMMENT_PROPERTY_ID#'),
	),
	false
);?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>