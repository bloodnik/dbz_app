<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_LOCATION_ID = WizardServices::ImportIBlockFromXML(
	WIZARD_SERVICE_RELATIVE_PATH.'/xml/'.LANGUAGE_ID.'/location.xml',
	'location',
	'locations',
	WIZARD_SITE_ID,
	array(
		'1' => 'X',
		'2' => 'R'
	)
);

if ($IBLOCK_LOCATION_ID < 1)
	return;

$_SESSION['WIZARD_LOCATION_IBLOCK_ID'] = $IBLOCK_LOCATION_ID;
?>