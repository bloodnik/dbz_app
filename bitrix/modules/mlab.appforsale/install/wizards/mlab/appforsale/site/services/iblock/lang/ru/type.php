<?
$MESS['LOCATIONS_TYPE_NAME'] = 'Города';
$MESS['LOCATIONS_ELEMENT_NAME'] = 'Города';
$MESS['LOCATIONS_SECTION_NAME'] = 'Области';
$MESS['TASKS_TYPE_NAME'] = 'Задания';
$MESS['TASKS_ELEMENT_NAME'] = 'Задания';
$MESS['TASKS_SECTION_NAME'] = 'Типы заданий';
$MESS['PROFILES_TYPE_NAME'] = 'Профили';
$MESS['PROFILES_ELEMENT_NAME'] = 'Профили';
$MESS['PROFILES_SECTION_NAME'] = 'Типы профилей';
$MESS['OFFERS_TYPE_NAME'] = 'Предложения';
$MESS['OFFERS_ELEMENT_NAME'] = 'Предложения';
$MESS['COMMENTS_TYPE_NAME'] = 'Комментарии';
$MESS['COMMENTS_ELEMENT_NAME'] = 'Комментарии';
$MESS['NOTIFICATIONS_TYPE_NAME'] = 'Уведомления';
$MESS['NOTIFICATIONS_ELEMENT_NAME'] = 'Уведомления';
$MESS['ADVANTAGE_TYPE_NAME'] = 'Преимущества';
$MESS['ADVANTAGE_ELEMENT_NAME'] = 'Преимущества';
$MESS['RULES_TYPE_NAME'] = 'Как это работает';
$MESS['RULES_ELEMENT_NAME'] = 'Как это работает';
$MESS['INVITE_TYPE_NAME'] = 'Приглашение';
$MESS['INVITE_ELEMENT_NAME'] = 'Приглашения';
?>