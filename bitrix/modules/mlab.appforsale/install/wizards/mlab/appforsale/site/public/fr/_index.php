<? 
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Un service de recherche d\'interprètes');
?>
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-4 hidden-sm hidden-xs">
			</div>
			<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
				<div class="card">
					<div class="afs-video-title">
						 Un moyen pratique de commander n'importe quel service
					</div>
					<div class="afs-video-text">
						 Vous aider à trouver un fiable de l'artiste pour résoudre les tâches ménagères
					</div>
					<div class="text-center">
 <a class="btn btn-primary btn-lg" href="/youdo/tasks-new/">Commander le service de</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wrap">
	<div class="container">
		 <?$APPLICATION->IncludeComponent(
	"mlab:appforsale.task.new.sections",
	"top",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"FORM_MESS_BTN_ADD" => "Enregistrer",
		"FORM_MESS_PROPERTY_VALUE_NA" => "(non défini)",
		"FORM_PROPERTY_CODE" => array(0=>"",1=>"",),
		"FORM_PROPERTY_CODE_REQUIRED" => array(0=>"",1=>"",),
		"IBLOCK_ID" => "#TASK_IBLOCK_ID#",
		"IBLOCK_TYPE" => "tasks",
		"SECTIONS_FIELD_CODE" => array(0=>"",1=>"",),
		'DETAIL_URL' => '/youdo/tasks-new/#SECTION_CODE_PATH#/form/',
		'SECTION_URL' => '/youdo/tasks-new/#SECTION_CODE_PATH#/',
		"SEF_URL_TEMPLATES" => array("sections"=>"#SECTION_CODE_PATH#/","form"=>"#SECTION_CODE_PATH#/form/",)
	)
);?>
	</div>
</div>
<div class="wrap" style="background: #f3f3f3">
	<div class="container">
		<h2 class="text-center">Comment cela fonctionne</h2>
		 <?$APPLICATION->IncludeComponent(
	"mlab:appforsale.promo", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"FIELD_CODE" => array(
			0 => "",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"IBLOCK_ID" => "#RULES_IBLOCK_ID#",
		"IBLOCK_TYPE" => "rules",
		"SORT_FIELD" => "sort",
		"SORT_ORDER" => "asc",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"SORT_FIELD2" => "id",
		"SORT_ORDER2" => "asc"
	),
	false
);?>
		<div class="text-center">
 <a class="btn btn-primary btn-lg" href="/youdo/tasks-new/">Placer la tâche</a>
		</div>
	</div>
</div>
<div class="wrap">
	<div class="container">
		<h2 class="text-center">Les principaux avantages</h2>
		 <?$APPLICATION->IncludeComponent(
	"mlab:appforsale.promo", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"FIELD_CODE" => array(
			0 => "",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"IBLOCK_ID" => "#ADVANTAGE_IBLOCK_ID#",
		"IBLOCK_TYPE" => "advantage",
		"SORT_FIELD" => "sort",
		"SORT_ORDER" => "asc",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"SORT_FIELD2" => "id",
		"SORT_ORDER2" => "asc"
	),
	false
);?>
	</div>
</div>
<div class="wrap" style="background: #f3f3f3">
	<div class="container">
		 <?$APPLICATION->IncludeComponent(
	"mlab:appforsale.download",
	".default",
Array()
);?>
	</div>
</div>
<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				© <?=date('Y').' Un service de recherche d\'interprètes'?>
			</div>
			<div class="col-md-6 text-right">
				<a href="/youdo/personal/?ITEM=agreement" style="color: #fff">
					L'accord
				</a>
			</div>
		</div>
	</div>
</div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>