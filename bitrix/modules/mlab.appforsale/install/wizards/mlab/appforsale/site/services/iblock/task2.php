<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'N')
{

	$IBLOCK_TASK_ID = (isset($_SESSION['WIZARD_TASK_IBLOCK_ID']) ? intval($_SESSION['WIZARD_TASK_IBLOCK_ID']) : 0);
	$IBLOCK_PROFILE_ID = (isset($_SESSION['WIZARD_PROFILE_IBLOCK_ID']) ? intval($_SESSION['WIZARD_PROFILE_IBLOCK_ID']) : 0);
	$IBLOCK_OFFER_ID = (isset($_SESSION['WIZARD_OFFER_IBLOCK_ID']) ? intval($_SESSION['WIZARD_OFFER_IBLOCK_ID']) : 0);
	
	
	if ($IBLOCK_TASK_ID)
	{
		$iblockCode = 'task_'.WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$arFields = array(
			'ACTIVE' => 'Y',
			'FIELDS' => array(
				'IBLOCK_SECTION' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => ''),
				'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown'),
				'SECTION_CODE' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => array('UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '-', 'TRANS_OTHER' => '-', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'Y'))
			),
			'LIST_MODE' => 'C',
			'CODE' => 'task',
			'XML_ID' => $iblockCode
		);
		$iblock->Update($IBLOCK_TASK_ID, $arFields);
	
		$arLanguages = array();
		$dbLanguage = CLanguage::GetList($by, $order, array());
		while($arLanguage = $dbLanguage->Fetch())
			$arLanguages[] = $arLanguage['LID'];
		
		
		$userType = new CUserTypeEntity();
		$userType->Add(array(
				'ENTITY_ID' => 'IBLOCK_'.$IBLOCK_TASK_ID.'_SECTION',
				'FIELD_NAME' => 'UF_SUBSCRIBE',
				'USER_TYPE_ID' => 'iblock_property',
				'XML_ID' => 'SUBSCRIBE',
				'MULTIPLE' => 'Y',
				'SETTINGS' => array(
					'LIST_HEIGHT' => '8'
				),
				'EDIT_FORM_LABEL' => array(
					'ru'    => 'Properties participating in the subscription',
					'en'    => 'Properties participating in the subscription',
				),
				'LIST_COLUMN_LABEL' => array(
					'ru'    => 'Properties participating in the subscription',
					'en'    => 'Properties participating in the subscription',
				),
				'LIST_FILTER_LABEL' => array(
					'ru'    => 'Properties participating in the subscription',
					'en'    => 'Properties participating in the subscription',
				)
		));
		
		$userType = new CUserTypeEntity();
		$userType->Add(array(
				'ENTITY_ID' => 'IBLOCK_'.$IBLOCK_TASK_ID.'_SECTION',
				'FIELD_NAME' => 'UF_PRICE',
				'USER_TYPE_ID' => 'integer',
				'XML_ID' => 'PRICE',
				'EDIT_FORM_LABEL' => array(
						'ru'    => 'The cost of placing job',
						'en'    => 'The cost of placing job'
				),
				'LIST_COLUMN_LABEL' => array(
						'ru'    => 'The cost of placing job',
						'en'    => 'The cost of placing job'
				),
				'LIST_FILTER_LABEL' => array(
						'ru'    => 'The cost of placing job',
						'en'    => 'The cost of placing job'
				)
		));
	
		$arUserFields = array('UF_ICON_DARK', 'UF_FORMAT_NAME', 'UF_PROFILE', 'UF_DETAIL_PROPERTY', 'UF_FORM_PROPERTY', 'UF_FORM_PROPERTY_REQ', 'UF_SUBSCRIBE', 'UF_OFFER_PRICE', 'UF_COMISSION', 'UF_PRICE');
		foreach ($arUserFields as $userField)
		{
			$arLabelNames = Array();
			foreach($arLanguages as $languageID)
			{
				WizardServices::IncludeServiceLang('property_names.php', $languageID);
				$arLabelNames[$languageID] = GetMessage($userField);
			}
		
			$arProperty['EDIT_FORM_LABEL'] = $arLabelNames;
			$arProperty['LIST_COLUMN_LABEL'] = $arLabelNames;
			$arProperty['LIST_FILTER_LABEL'] = $arLabelNames;
		
			$dbRes = CUserTypeEntity::GetList(Array(), Array('ENTITY_ID' => 'IBLOCK_'.$IBLOCK_TASK_ID.'_SECTION', 'FIELD_NAME' => $userField));
			if ($arRes = $dbRes->Fetch())
			{
				if ($userField == 'UF_PROFILE')
				{
					$arProperty['SETTINGS'] = $arRes['SETTINGS'];
					$arProperty['SETTINGS']['IBLOCK_ID'] = $IBLOCK_PROFILE_ID;
				}
				else if (in_array($userField, array('UF_DETAIL_PROPERTY', 'UF_FORM_PROPERTY', 'UF_FORM_PROPERTY_REQ', 'UF_SUBSCRIBE')))
				{
					$arProperty['SETTINGS'] = $arRes['SETTINGS'];
					$arProperty['SETTINGS']['IBLOCK_ID'] = $IBLOCK_TASK_ID;
				}
				
				$userType = new CUserTypeEntity();
				$userType->Update($arRes['ID'], $arProperty);
			}
		}
		
		
		
		
		// Properties
		$arPropLink = array();
		$dbProp = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $IBLOCK_TASK_ID, '!CODE' => false));
		while ($arProp = $dbProp->GetNext())
		{
			$arPropLink[$arProp['CODE']] = $arProp['ID'];
		}
		
		$arSectionLink = array(
			'kurerskie-uslugi' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['ADDRESS'], $arPropLink['NEED'], $arPropLink['PACKAGE_SIZE'], $arPropLink['PACKAGE_WEIGHT']),
				'UF_FORM_PROPERTY' => array($arPropLink['ADDRESS'], $arPropLink['NEED'], $arPropLink['PACKAGE_SIZE'], $arPropLink['PACKAGE_WEIGHT']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['ADDRESS'], $arPropLink['NEED']),
				'UF_SUBSCRIBE' => array($arPropLink['PACKAGE_WEIGHT'])
			),
			'bytovoy-remont' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED'], $arPropLink['REPAIR_SUPPLIES'], $arPropLink['REPAIR_DISMANTLING']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED'], $arPropLink['REPAIR_SUPPLIES'], $arPropLink['REPAIR_DISMANTLING']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED']),
				'UF_SUBSCRIBE' => array($arPropLink['REPAIR_SUPPLIES'], $arPropLink['REPAIR_DISMANTLING'])
			),	
			'gruzoperevozki' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['ADDRESS'], $arPropLink['NEED'], $arPropLink['TRANSPORTATION_SIZE'], $arPropLink['TRANSPORTATION_WEIGHT'], $arPropLink['TRANSPORTATION_LOADING']), 
				'UF_FORM_PROPERTY' => array($arPropLink['ADDRESS'], $arPropLink['NEED'], $arPropLink['TRANSPORTATION_SIZE'], $arPropLink['TRANSPORTATION_WEIGHT'], $arPropLink['TRANSPORTATION_LOADING']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['ADDRESS'], $arPropLink['NEED']),
				'UF_SUBSCRIBE' => array($arPropLink['TRANSPORTATION_LOADING'])
			),
			'uborka-i-pomoshch-po-khozyaystvu' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'virtualnyy-pomoshnik' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'kompyuternaya-pomoshch' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'meropriyatiya-i-promo-aktsii' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'dizayn' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED'], $arPropLink['PHOTO']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED'], $arPropLink['PHOTO']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'web-razrabotka' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED'], $arPropLink['PHOTO']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED'], $arPropLink['PHOTO']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'foto-i-video-uslugi' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'ustanovka-i-remont-tekhniki' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'krasota-i-zdorove' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'remont-tsifrovoy-tekhniki' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'yuridicheskaya-pomoshch' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'repetitory-i-obrazovanie' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED'], $arPropLink['EDUCATION_AGE'], $arPropLink['EDUCATION_PLACE'], $arPropLink['EDUCATION_DURATION']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED'], $arPropLink['EDUCATION_AGE'], $arPropLink['EDUCATION_PLACE'], $arPropLink['EDUCATION_DURATION']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			),
			'remont-transporta' => array(
				'UF_FORMAT_NAME' => false,
				'UF_PROFILE' => false,
				'UF_DETAIL_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY' => array($arPropLink['NEED']),
				'UF_FORM_PROPERTY_REQ' => array($arPropLink['NEED'])
			)		
		);
		$dbSection = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $IBLOCK_PROFILE_ID, '!SECTION_ID' => false), false, array('ID', 'CODE'));
		while ($arSection = $dbSection->GetNext())
		{
			$arSectionLink[$arSection['CODE']]['UF_PROFILE']['VALUE'] = $arSection['ID'];
			$arSectionLink[$arSection['CODE']]['UF_FORMAT_NAME'] = '#PROPERTY_NEED#';
			$arSectionLink[$arSection['CODE']]['UF_DETAIL_PROPERTY'] = false;
			$arSectionLink[$arSection['CODE']]['UF_FORM_PROPERTY'] = false;
			$arSectionLink[$arSection['CODE']]['UF_FORM_PROPERTY_REQ'] = false;
		}
		
		$obSection = new CIBlockSection;
		$dbSection = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $IBLOCK_TASK_ID), false, array('ID', 'CODE'));
		while ($arSection = $dbSection->GetNext())
		{
			$obSection->Update($arSection['ID'], $arSectionLink[$arSection['CODE']]);
		}
		
	
		$countUsers = count($_SESSION['WIZARD_USERS']);
		if ($countUsers > 0)
		{
			$dbTask = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $IBLOCK_TASK_ID
				),
				false,
				false,
				array(
					'ID'
				)
			);
			while ($arTask = $dbTask->GetNext())
			{
				$obTask = new CIBlockElement;
				$obTask->Update($arTask['ID'], array(
						'CREATED_BY' => $_SESSION['WIZARD_USERS'][rand(0, ($countUsers-1))]['ID']
				));
			}
		}
		
		
		
		
		
		
		WizardServices::IncludeServiceLang('task2.php');
	
		CUserOptions::SetOption(
			'form',
			'form_section_'.$IBLOCK_TASK_ID,
			array(
				'tabs' => GetMessage('FORM_SECTION_TASK_TABS')
			),
			true
		);
		
		CUserOptions::SetOption(
			'list',
			'tbl_iblock_list_'.md5('tasks.'.$IBLOCK_TASK_ID),
			array(
				'columns' => 'NAME,ACTIVE,SECTION_CNT,ELEMENT_CNT,ID',
				'by' => 'id',
				'order' => 'asc',
				'page_size' => 100
			),
			true
		);
	
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/_index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/my/index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-new/index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('TASK_IBLOCK_ID' => $IBLOCK_TASK_ID));
	}
}
else if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installLanding', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
		array(),
		array(
			'TYPE' => 'tasks',
			'SITE_ID' => WIZARD_SITE_ID,
			'ACTIVE'=>'Y',
			"CODE"=>'task'
		),
		false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/_index.php', array('TASK_IBLOCK_ID' => $arIBlock['ID']));
		$ib = new CIBlock;
		$ib->Update($arIBlock['ID'], array(
				'SECTION_PAGE_URL' => 'youdo/tasks-new/#SECTION_CODE_PATH#/',
				'DETAIL_PAGE_URL' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/'
		));
	}
}
else if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installSite', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
			array(),
			array(
					'TYPE' => 'tasks',
					'SITE_ID' => WIZARD_SITE_ID,
					'ACTIVE'=>'Y',
					"CODE"=>'task'
			),
			false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/_index.php', array('TASK_IBLOCK_ID' => $arIBlock['ID']));
	}
}
?>