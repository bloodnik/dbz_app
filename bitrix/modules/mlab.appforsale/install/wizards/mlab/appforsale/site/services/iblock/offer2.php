<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_OFFER_ID = (isset($_SESSION['WIZARD_OFFER_IBLOCK_ID']) ? intval($_SESSION['WIZARD_OFFER_IBLOCK_ID']) : 0);

if ($IBLOCK_OFFER_ID)
{
	$iblockCode = 'offer_'.WIZARD_SITE_ID;
	$iblock = new CIBlock;
	$arFields = array(
		'ACTIVE' => 'Y',
		'FIELDS' => array(
			'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown')
		),
		'LIST_MODE' => 'C',
		'CODE' => 'offer',
		'XML_ID' => $iblockCode
	);
	$iblock->Update($IBLOCK_OFFER_ID, $arFields);	
	
	//TEXT
	$dbProperty = CIBlockProperty::GetList(array(), array('CODE' => 'TEXT', 'IBLOCK_ID' => $IBLOCK_OFFER_ID));
	if ($arProperty = $dbProperty->GetNext())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/index.php', array('OFFER_PROPERTY_ID' => $arProperty['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('OFFER_PROPERTY_ID' => $arProperty['ID']));
	}
	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/index.php', array('OFFER_IBLOCK_ID' => $IBLOCK_OFFER_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/index.php', array('OFFER_IBLOCK_ID' => $IBLOCK_OFFER_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/my/index.php', array('OFFER_IBLOCK_ID' => $IBLOCK_OFFER_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('OFFER_IBLOCK_ID' => $IBLOCK_OFFER_ID));
}
?>