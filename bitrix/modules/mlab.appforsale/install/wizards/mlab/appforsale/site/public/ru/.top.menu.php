<?
$aMenuLinks = Array(
	Array(
		"Создать задание", 
		"youdo/tasks-new/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task_add.png"), 
		"" 
	),
	Array(
		"Все задания", 
		"youdo/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task.png"), 
		"" 
	),
	Array(
		"Исполнители", 
		"youdo/executors/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_users.png"), 
		"" 
	),
	Array(
		"Мои заказы", 
		"youdo/tasks-customer/", 
		Array("/youdo/tasks-executor/"), 
		Array("img"=>"/youdo/images/menu_task_customer.png"), 
		"" 
	),
	Array(
		"Уведомления", 
		"youdo/notification/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_notification.png"), 
		"" 
	),
	Array(
		"Оказываемые виды услуг", 
		"youdo/personal/profile/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_subscribe.png"), 
		"" 
	),
	Array(
		"Пригласить друга",
		"youdo/invite/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_invite.png"),
		""
	),
	Array(
		"Настройки",
		"youdo/personal/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_setting.png", "title"=>"Профиль"),
		""
	)
);
?>