<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

$arTypes = array(
	array(
		'ID' => 'locations',
		'SECTIONS' => 'Y',
		'IN_RSS' => 'N',
		'SORT' => 100,
		'LANG' => array()
	),
	array(
		'ID' => 'tasks',
		'SECTIONS' => 'Y',
		'IN_RSS' => 'N',
		'SORT' => 200,
		'LANG' => array()
	),
	array(
		'ID' => 'profiles',
		'SECTIONS' => 'Y',
		'IN_RSS' => 'N',
		'SORT' => 300,
		'LANG' => array()
	),
	array(
		'ID' => 'offers',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 400,
		'LANG' => array()
	),
	array(
		'ID' => 'comments',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 500,
		'LANG' => array()
	),
	array(
		'ID' => 'notifications',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 600,
		'LANG' => array()
	),
	array(
		'ID' => 'advantage',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 700,
		'LANG' => array()
	),
	array(
		'ID' => 'rules',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 800,
		'LANG' => array()
	),
	array(
		'ID' => 'invite',
		'SECTIONS' => 'N',
		'IN_RSS' => 'N',
		'SORT' => 900,
		'LANG' => array()
	)
);

$arLanguages = array();
$dbLanguage = CLanguage::GetList($by, $order, array());
while($arLanguage = $dbLanguage->Fetch())
	$arLanguages[] = $arLanguage['LID'];

$obIBlockType = new CIBlockType;
foreach ($arTypes as $arType)
{
	$dbType = CIBlockType::GetList(array(), array('=ID' => $arType['ID']));
	if($ar = $dbType->Fetch())
		continue;

	foreach($arLanguages as $languageID)
	{
		WizardServices::IncludeServiceLang('type.php', $languageID);

		$code = strtoupper($arType['ID']);
		$arType['LANG'][$languageID]['NAME'] = GetMessage($code.'_TYPE_NAME');
		$arType['LANG'][$languageID]['ELEMENT_NAME'] = GetMessage($code.'_ELEMENT_NAME');

		if ($arType['SECTIONS'] == 'Y')
			$arType['LANG'][$languageID]['SECTION_NAME'] = GetMessage($code.'_SECTION_NAME');
	}

	$obIBlockType->Add($arType);
}
?>