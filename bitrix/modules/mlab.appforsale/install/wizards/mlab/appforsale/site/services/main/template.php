<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

WizardServices::IncludeServiceLang('template.php');

CopyDirFiles(
	$_SERVER['DOCUMENT_ROOT'].WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH.'/site').'/youdo_appforsale',
	$_SERVER['DOCUMENT_ROOT'].BX_PERSONAL_ROOT.'/templates/youdo_appforsale',
	$rewrite = true,
	$recursive = true,
	$delete_after_copy = false
);

$rsSite = CSite::GetList($by = 'def', $order = 'desc', array('LID' => WIZARD_SITE_ID));
if ($arSite = $rsSite->Fetch())
{
	$arTemplates = array();
	$found = false;
	$dbTemplate = CSite::GetTemplateList($arSite['LID']);
	while($arTemplate = $dbTemplate->Fetch())
	{
		if(!$found && strlen(trim($arTemplate['CONDITION'])) <= 0)
		{
			$arTemplate['TEMPLATE'] = 'youdo_appforsale';
			$found = true;
		}
		if($arTemplate['TEMPLATE'] == 'empty')
		{
			continue;
		}
		$arTemplates[] = $arTemplate;
	}

	if (!$found)
		$arTemplates[] = array(
			'CONDITION' => '',
			'SORT' => 150,
			'TEMPLATE' => 'youdo_appforsale'
		);

	$arFields = array(
		'TEMPLATE' => $arTemplates,
		'NAME' => GetMessage('APPFORSALE_SITE_NAME')
	);

	$obSite = new CSite();
	$obSite->Update(
		$arSite['LID'],
		$arFields
	);
}

function ___writeToAreasFile($fn, $text)
{
	$fd = @fopen($fn, "wb");
	if(!$fd)
		return false;

	if(false === fwrite($fd, $text))
	{
		fclose($fd);
		return false;
	}

	fclose($fd);

	if(defined("BX_FILE_PERMISSIONS"))
		@chmod($fn, BX_FILE_PERMISSIONS);
}

$fLogo = CFile::GetByID($wizard->GetVar('siteLogo'));
$logo = $fLogo->Fetch();

if($logo)
{
	$strOldFile = str_replace('//', '/', WIZARD_SITE_ROOT_PATH.'/'.(COption::GetOptionString('main', 'upload_dir', 'upload')).'/'.$logo['SUBDIR'].'/'.$logo['FILE_NAME']);
	@copy($strOldFile, WIZARD_SITE_PATH.'include/appforsale_logo.png');
	CFile::Delete($fLogo);
	
	$content = '<img src="'.WIZARD_SITE_DIR.'include/appforsale_logo.png" />';
	___writeToAreasFile(WIZARD_SITE_PATH."include/appforsale_logo.php", $content);
}
elseif(WIZARD_INSTALL_DEMO_DATA || !file_exists(WIZARD_SITE_PATH."/include/appforsale_logo.php") && !file_exists(WIZARD_SITE_PATH."/include/appforsale_logo.png"))
{
	copy(WIZARD_ABSOLUTE_PATH."/site/templates/youdo_appforsale/images/logo.png", WIZARD_SITE_PATH."include/appforsale_logo.png");
	___writeToAreasFile(WIZARD_SITE_PATH."include/appforsale_logo.php", '<img src="'.WIZARD_SITE_DIR.'include/appforsale_logo.png" />');
}
?>