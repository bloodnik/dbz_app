<?
$aMenuLinks = Array(
	Array(
		"Créer une tâche", 
		"youdo/tasks-new/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task_add.png"), 
		"" 
	),
	Array(
		"Tous les travaux", 
		"youdo/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task.png"), 
		"" 
	),
	Array(
		"Les artistes", 
		"youdo/executors/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_users.png"), 
		"" 
	),
	Array(
		"Mes commandes", 
		"youdo/tasks-customer/", 
		Array("/youdo/tasks-executor/"), 
		Array("img"=>"/youdo/images/menu_task_customer.png"), 
		"" 
	),
	Array(
		"Avis", 
		"youdo/notification/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_notification.png"), 
		"" 
	),
	Array(
		"Offerts types de services", 
		"youdo/personal/profile/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_subscribe.png"), 
		"" 
	),
	Array(
		"Inviter un ami",
		"youdo/invite/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_invite.png"),
		""
	),
	Array(
		"Les paramètres de",
		"youdo/personal/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_setting.png", "title"=>"Profil"),
		""
	)
);
?>