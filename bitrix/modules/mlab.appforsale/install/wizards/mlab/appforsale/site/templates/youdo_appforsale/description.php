<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arTemplate = array(
	'NAME' => Loc::getMessage('TEMPLATE_NAME'),
	'DESCRIPTION' => Loc::getMessage('TEMPLATE_DESC'),
	'EDITOR_STYLES' => array(
		'/bitrix/templates/youdo_appforsale/colors.css'
	)
);
?>