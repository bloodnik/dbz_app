<? 
use Bitrix\Main\Config\Option;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

WizardServices::IncludeServiceLang('user.php');

if (!defined('WIZARD_SITE_ID') || !defined('WIZARD_SITE_DIR'))
	return;

if(Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'N')
{
	$arUsers = array(
		array(
			'NAME' => GetMessage('USER_1_NAME'),
			'LAST_NAME' => GetMessage('USER_1_LAST_NAME'),
			'LOGIN' => '79270000001',
			'PERSONAL_PHOTO' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/mlab/appforsale/site/services/main/images/1.jpg')
		),
		array(
			'NAME' => GetMessage('USER_2_NAME'),
			'LAST_NAME' => GetMessage('USER_2_LAST_NAME'),
			'LOGIN' => '79270000002',
			'PERSONAL_PHOTO' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/mlab/appforsale/site/services/main/images/2.jpg')
		),
		array(
			'NAME' => GetMessage('USER_3_NAME'),
			'LAST_NAME' => GetMessage('USER_3_LAST_NAME'),
			'LOGIN' => '79270000003',
			'PERSONAL_PHOTO' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/mlab/appforsale/site/services/main/images/3.jpg')
		),
		array(
			'NAME' => GetMessage('USER_4_NAME'),
			'LAST_NAME' => GetMessage('USER_4_LAST_NAME'),
			'LOGIN' => '79270000004',
			'PERSONAL_PHOTO' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/mlab/appforsale/site/services/main/images/4.jpg')
		),
		array(
			'NAME' => GetMessage('USER_5_NAME'),
			'LAST_NAME' => GetMessage('USER_5_LAST_NAME'),
			'LOGIN' => '79270000005',
			'PERSONAL_PHOTO' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/mlab/appforsale/site/services/main/images/5.jpg')
		)
	);

	
	$def_group = Option::get('main', 'new_user_registration_def_group', '');
	if ($def_group != '')
	{
		$group_id = explode(',', $def_group);
		$arPolicy = $USER->GetGroupPolicy($group_id);
	}
	else
	{
		$arPolicy = $USER->GetGroupPolicy(array());
	}
	
	$password_min_length = intval($arPolicy['PASSWORD_LENGTH']);
	if($password_min_length <= 0)
		$password_min_length = 6;
	$password_chars = array(
		'abcdefghijklnmopqrstuvwxyz',
		'ABCDEFGHIJKLNMOPQRSTUVWXYZ',
		'0123456789'
	);
	if($arPolicy['PASSWORD_PUNCTUATION'] === 'Y')
		$password_chars[] = ',.<>/?;:\'"[]{}\\|`~!@#\$%^&*()-_+=';
	
	$obUser = new CUser;
	foreach ($arUsers as &$arUser)
	{
		$arUser['EMAIL'] = $arUser['LOGIN'].'@'.$_SERVER['SERVER_NAME'];
		$arUser['GROUP_ID'] = $group_id;
		$arUser['PASSWORD'] = $arUser['CONFIRM_PASSWORD'] = randString($password_min_length, $password_chars);
		$arUser['ID'] = $obUser->Add($arUser);
	}
	
	$_SESSION['WIZARD_USERS'] = $arUsers;
	
	$group = new CGroup;
	$group->Add(array(
			'ACTIVE' => 'Y',
			'NAME' => GetMessage('GROUP_CONTRACTOR_NAME'),
			'STRING_ID' => 'CONTRACTOR'
	));
	
	$obUserField  = new CUserTypeEntity;
	$obUserField->Add(array(
			"ENTITY_ID" => "USER",
			"FIELD_NAME" => "UF_INSTANCE_ID",
			"USER_TYPE_ID" => "string",
			"EDIT_FORM_LABEL" => "INSTANCE_ID"
	));
	
	$obUserField  = new CUserTypeEntity;
	$obUserField->Add(array(
			"ENTITY_ID" => "USER",
			"FIELD_NAME" => "UF_REQUEST_ID",
			"USER_TYPE_ID" => "string",
			"EDIT_FORM_LABEL" => "REQUEST_ID"
	));
	
	$obUserField  = new CUserTypeEntity;
	$obUserField->Add(array(
			"ENTITY_ID" => "USER",
			"FIELD_NAME" => "UF_SUM",
			"USER_TYPE_ID" => "double",
			"EDIT_FORM_LABEL" => "SUM"
	));
}
?>