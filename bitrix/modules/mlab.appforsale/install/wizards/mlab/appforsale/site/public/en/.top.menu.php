<?
$aMenuLinks = Array(
	Array(
		"To create a job", 
		"youdo/tasks-new/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task_add.png"), 
		"" 
	),
	Array(
		"All jobs", 
		"youdo/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_task.png"), 
		"" 
	),
	Array(
		"Performers", 
		"youdo/executors/", 
		Array(), 
		Array("img"=>"/youdo/images/menu_users.png"), 
		"" 
	),
	Array(
		"My orders", 
		"youdo/tasks-customer/", 
		Array("/youdo/tasks-executor/"), 
		Array("img"=>"/youdo/images/menu_task_customer.png"), 
		"" 
	),
	Array(
		"Notice", 
		"youdo/notification/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_notification.png"), 
		"" 
	),
	Array(
		"Provide the types of services", 
		"youdo/personal/profile/", 
		Array(), 
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_subscribe.png"), 
		"" 
	),
	Array(
		"Invite a friend",
		"youdo/invite/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_invite.png"),
		""
	),
	Array(
		"Settings",
		"youdo/personal/",
		Array(),
		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_setting.png", "title"=>"Profile"),
		""
	)
);
?>