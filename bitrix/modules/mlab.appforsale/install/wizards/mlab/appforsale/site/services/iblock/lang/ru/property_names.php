<?
$MESS['UF_ICON_DARK'] = 'Иконка светлая';
$MESS['UF_FORMAT_NAME'] = 'Маска имени';
$MESS['UF_PROFILE'] = 'Профили выполняющие данный тип задания';
$MESS['UF_DETAIL_PROPERTY'] = 'Свойства';
$MESS['UF_FORM_PROPERTY'] = 'Свойства';
$MESS['UF_FORM_PROPERTY_REQ'] = 'Свойства обязательные к заполнению';
$MESS['UF_SUBSCRIBE'] = 'Свойства участвующие в подписке';
$MESS['UF_OFFER_PRICE'] = 'Стоимость предложения';
$MESS['UF_COMISSION'] = 'Комиссия с выполненного задания (%)';
$MESS['UF_RECUR_TYPE_TEST'] = 'Тип периода';
$MESS['UF_RECUR_LENGTH_TEST'] = 'Период';
$MESS['UF_RECUR_PRICE_TEST'] = 'Стоимость';
$MESS['UF_RECUR_TYPE'] = 'Тип периода';
$MESS['UF_RECUR_LENGTH'] = 'Период';
$MESS['UF_RECUR_PRICE'] = 'Стоимость';
$MESS['UF_COUNT'] = 'Лимит количества профилей';
$MESS['UF_CITY'] = 'Город';
$MESS['UF_PRICE'] = 'Стоимость размещения задания';
?>