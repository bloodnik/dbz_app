<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installNotification', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_NOTIFICATION_ID = (isset($_SESSION['WIZARD_NOTIFICATION_IBLOCK_ID']) ? intval($_SESSION['WIZARD_NOTIFICATION_IBLOCK_ID']) : 0);

if ($IBLOCK_NOTIFICATION_ID)
{
	$iblock = new CIBlock;
	$iblock->Update($IBLOCK_NOTIFICATION_ID, array(
			'ACTIVE' => 'Y',
			'FIELDS' => array(
				'NAME' => array(
					'IS_REQUIRED' => 'Y',
					'DEFAULT_VALUE' => 'unknown'
				)
			),
			'LIST_MODE' => 'C',
			'CODE' => 'notification',
			'XML_ID' => 'notification_'.WIZARD_SITE_ID
	));	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/notification/index.php', array('NOTIFICATION_IBLOCK_ID' => $IBLOCK_NOTIFICATION_ID));

	Bitrix\Main\Config\Option::set('mlab.appforsale', 'installNotification', 'Y', WIZARD_SITE_ID);
}
?>