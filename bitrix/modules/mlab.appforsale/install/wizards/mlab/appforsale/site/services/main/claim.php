<? 
use Bitrix\Main\Config\Option;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

WizardServices::IncludeServiceLang('claim.php');

CEventType::Add(array(
		'LID' => 'ru',
		'EVENT_NAME' => 'CLAIM',
		'NAME' => GetMessage('NAME')
));

$em = new CEventMessage;
$em->Add(array(
		'ACTIVE' => 'Y',
		'EVENT_NAME' => 'CLAIM',
		'LID' => WIZARD_SITE_ID,
		'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
		'EMAIL_TO' => '#DEFAULT_EMAIL_FROM#',
		'SUBJECT' => GetMessage('NAME'),
		'BODY_TYPE' => 'text',
		'MESSAGE' => GetMessage('MESSAGE').'#ID# #SERVER_NAME#/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=#IBLOCK_ID#&type=tasks&ID=#ID#&lang=ru&find_section_section=-1&WF=Y'
));

CEventType::Add(array(
'LID' => 'ru',
'EVENT_NAME' => 'PUSH',
'NAME' => 'Push'
		));

$em = new CEventMessage;
$em->Add(array(
		'ACTIVE' => 'Y',
		'EVENT_NAME' => 'PUSH',
		'LID' => CSite::GetDefSite(),
		'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
		'EMAIL_TO' => '#EMAIL#',
		'BODY_TYPE' => 'text',
		'MESSAGE' => '#TEXT#'
));
?>