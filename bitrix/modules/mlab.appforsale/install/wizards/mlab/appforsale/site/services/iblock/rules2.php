<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installLanding', 'N', WIZARD_SITE_ID) == 'N')
{
	$IBLOCK_RULES_ID = (isset($_SESSION['WIZARD_RULES_IBLOCK_ID']) ? intval($_SESSION['WIZARD_RULES_IBLOCK_ID']) : 0);
	
	if ($IBLOCK_RULES_ID)
	{
		$iblockCode = 'rules_'.WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$arFields = array(
			'ACTIVE' => 'Y',
			'FIELDS' => array(
				'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown')
			),
			'LIST_MODE' => 'C',
			'CODE' => 'rules',
			'XML_ID' => $iblockCode
		);
		$iblock->Update($IBLOCK_RULES_ID, $arFields);	
			
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/_index.php', array('RULES_IBLOCK_ID' => $IBLOCK_RULES_ID));
	}
}
if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installSite', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
			array(),
			array(
					'TYPE' => 'rules',
					'SITE_ID' => WIZARD_SITE_ID,
					'ACTIVE'=>'Y',
					"CODE"=>'rules'
			),
			false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/_index.php', array('RULES_IBLOCK_ID' => $arIBlock['ID']));
	}
}
?>