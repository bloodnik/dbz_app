<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

Loc::loadMessages(__FILE__);

$arServices = array(
	'main' => array(
		'NAME' => Loc::getMessage('SERVICE_MAIN'),
		'STAGES' => array(
			'files.php',
			'template.php',
			'user.php',
			'claim.php',
			'newtask.php'
		),
	),
	'iblock' => array(
		'NAME' => Loc::getMessage('SERVICE_IBLOCK'),
		'STAGES' => array(
			'types.php',
			'location.php',
			'location2.php',
			'profile.php',
			'profile2.php',
			'offer.php',
			'offer2.php',
			'task.php',
			'task2.php',
			'task3.php',
			'comment.php',	
			'comment2.php',
			'notification.php',
			'notification2.php',
			'advantage.php',
			'advantage2.php',
			'rules.php',
			'rules2.php',
			'invite.php',
			'invite2.php'
		)
	)
);
?>