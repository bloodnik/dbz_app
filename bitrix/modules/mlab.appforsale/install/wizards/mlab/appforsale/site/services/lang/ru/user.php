<?
$MESS['GROUP_CONTRACTOR_NAME'] = 'Исполнитель';

$MESS['USER_1_NAME'] = 'Антон';
$MESS['USER_1_LAST_NAME'] = 'Иванов';

$MESS['USER_2_NAME'] = 'Петр';
$MESS['USER_2_LAST_NAME'] = 'Петров';

$MESS['USER_3_NAME'] = 'Иван';
$MESS['USER_3_LAST_NAME'] = 'Курский';

$MESS['USER_4_NAME'] = 'Екатерина';
$MESS['USER_4_LAST_NAME'] = 'Иванова';

$MESS['USER_5_NAME'] = 'Анна';
$MESS['USER_5_LAST_NAME'] = 'Романова';

$MESS['UF_PARENT_ID'] = 'Родитель';
$MESS['UF_PARENT_PAY'] = 'Оплачен';

$MESS['UF_LATITUDE_RU'] = 'Широта';
$MESS['UF_LATITUDE_EN'] = 'Latitude';
$MESS['UF_LONGITUDE_RU'] = 'Долгота';
$MESS['UF_LONGITUDE_EN'] = 'Longitude';

$MESS['UF_SHOW_COUNTER_RU'] = 'Просмотров';
$MESS['UF_SHOW_COUNTER_EN'] = 'Views';
?>