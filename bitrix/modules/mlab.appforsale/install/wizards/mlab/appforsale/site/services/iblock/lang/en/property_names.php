<?
$MESS['UF_ICON_DARK'] = 'Icon light';
$MESS['UF_FORMAT_NAME'] = 'Name mask';
$MESS['UF_PROFILE'] = 'Profiles of performing this type of task';
$MESS['UF_DETAIL_PROPERTY'] = 'Properties';
$MESS['UF_FORM_PROPERTY'] = 'Properties';
$MESS['UF_FORM_PROPERTY_REQ'] = 'Properties mandatory to be filled';
$MESS['UF_SUBSCRIBE'] = 'Properties participating in the subscription';
$MESS['UF_OFFER_PRICE'] = 'The value proposition';
$MESS['UF_COMISSION'] = 'The Commission completed jobs (%)';
$MESS['UF_DETAIL_FIELD_CODE'] = 'Properties';
$MESS['UF_EDIT_FIELD_CODE'] = 'Properties';
$MESS['UF_RECUR_TYPE_TEST'] = 'The period type';
$MESS['UF_RECUR_LENGTH_TEST'] = 'Period';
$MESS['UF_RECUR_PRICE_TEST'] = 'The cost';
$MESS['UF_RECUR_TYPE'] = 'The period type';
$MESS['UF_RECUR_LENGTH'] = 'Period';
$MESS['UF_RECUR_PRICE'] = 'The cost';
$MESS['UF_COUNT'] = 'Limit number of profiles';
$MESS['UF_CITY'] = 'City';
$MESS['UF_PRICE'] = 'The cost of placing job';
?>