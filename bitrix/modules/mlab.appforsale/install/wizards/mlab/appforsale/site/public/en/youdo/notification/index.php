<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Notice');
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.notification.list', 
	'', 
	array(
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'ELEMENT_SORT_FIELD' => 'timestamp_x',
		'ELEMENT_SORT_FIELD2' => 'id',
		'ELEMENT_SORT_ORDER' => 'desc',
		'ELEMENT_SORT_ORDER2' => 'desc',
		'IBLOCK_ID' => '#NOTIFICATION_IBLOCK_ID#',
		'IBLOCK_TYPE' => 'notifications'
	),
	false
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>