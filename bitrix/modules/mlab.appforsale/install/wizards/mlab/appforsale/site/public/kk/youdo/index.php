<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Барлық тапсырмалар');
?>
<?
$APPLICATION->IncludeComponent(
	'mlab:appforsale.task', 
	'', 
	array(
		'CACHE_TIME' => '36000000',
		'CACHE_TYPE' => 'A',
		'ELEMENT_SORT_FIELD' => 'timestamp_x',
		'ELEMENT_SORT_FIELD2' => 'id',
		'ELEMENT_SORT_ORDER' => 'desc',
		'ELEMENT_SORT_ORDER2' => 'desc',
		'IBLOCK_ID' => '#TASK_IBLOCK_ID#',
		'IBLOCK_TYPE' => 'tasks',
		'OFFER_IBLOCK_ID' => '#OFFER_IBLOCK_ID#',
		'OFFER_IBLOCK_TYPE' => 'offers',
		'SEF_FOLDER' => '/youdo/',
		'SEF_MODE' => 'Y',
		'SEF_URL_TEMPLATES' => array(
			'list' => '',
			'detail' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/',
			'offer' => '#SECTION_CODE_PATH#/#ELEMENT_ID#/offer/'
		),
		'OFFER_FORM_PROPERTY_CODE' => array('#OFFER_PROPERTY_ID#'),
		'OFFER_FORM_PROPERTY_CODE_REQUIRED' => array('#OFFER_PROPERTY_ID#'),
		'OFFER_PROPERTY_CODE' => array('#OFFER_PROPERTY_ID#')
	),
	false
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>