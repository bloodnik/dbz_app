<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_LOCATION_ID = (isset($_SESSION['WIZARD_LOCATION_IBLOCK_ID']) ? intval($_SESSION['WIZARD_LOCATION_IBLOCK_ID']) : 0);

if ($IBLOCK_LOCATION_ID)
{
	$iblockCode = 'location_'.WIZARD_SITE_ID;
	$iblock = new CIBlock;
	$arFields = array(
		'ACTIVE' => 'Y',
		'FIELDS' => array(
			'IBLOCK_SECTION' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => ''),
		),
		'LIST_MODE' => 'C',
		'CODE' => 'location',
		'XML_ID' => $iblockCode
	);
	$iblock->Update($IBLOCK_LOCATION_ID, $arFields);
	
	$arLanguages = array();
	$dbLanguage = CLanguage::GetList($by, $order, array());
	while($arLanguage = $dbLanguage->Fetch())
		$arLanguages[] = $arLanguage['LID'];
	
	$arLabelNames = Array();
	foreach($arLanguages as $languageID)
	{
		WizardServices::IncludeServiceLang('property_names.php', $languageID);
		$arLabelNames[$languageID] = GetMessage('UF_CITY');
	}
	
	$obUserField  = new CUserTypeEntity;
	$obUserField->Add(array(
			'ENTITY_ID' => 'USER',
			'FIELD_NAME' => 'UF_CITY',
			'USER_TYPE_ID' => 'iblock_element',
			'MULTIPLE' => 'N',
			'MANDATORY' => 'N',
			'SETTINGS' => array(
				'IBLOCK_TYPE_ID' => 'locations',
				'IBLOCK_ID' => $IBLOCK_LOCATION_ID,
				'ACTIVE_FILTER' => 'Y'
			),
			'EDIT_FORM_LABEL' => $arLabelNames
	));
}
?>