<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arMenu = array(
	array(
		'type' => 'section',
		'items' => array(
			array(
				'text' => 'Добавить задание',
				'icon' => SITE_DIR.'youdo/images/menu_task_add.png',
				'data-url' => SITE_DIR.'youdo/tasks-new/'
			),
			array(
				'text' => 'Все задания',
				'icon' => SITE_DIR.'youdo/images/menu_task.png',
				'data-url' => SITE_DIR.'youdo/'
			),
			array(
				'text' => 'Исполнители',
				'icon' => SITE_DIR.'youdo/images/menu_users.png',
				'data-url' => SITE_DIR.'youdo/profile/'
			),
			array(
				'text' => 'Мои задания',
				'icon' => SITE_DIR.'youdo/images/menu_task_customer.png',
				'data-url' => SITE_DIR.'youdo/tasks-customer/'
			),
			array(
				'text' => 'Уведомления',
				'icon' => SITE_DIR.'youdo/images/menu_notification.png',
				'data-url' => SITE_DIR.'youdo/notification/'
			),
			array(
				'text' => 'Оказываемые виды услуг',
				'icon' => SITE_DIR.'youdo/images/menu_subscribe.png',
				'data-url' => SITE_DIR.'youdo/personal/profile/'
			),
			array(
				'text' => 'Настройки',
				'icon' => SITE_DIR.'youdo/images/menu_setting.png',
				'data-url' => SITE_DIR.'youdo/personal/'
			),
			array(
				'text' => 'Соглашение',
				'icon' => SITE_DIR.'youdo/images/menu_agreement.png',
				'data-url' => SITE_DIR.'youdo/personal/?ITEM=agreement'
			)
				
				
		)
	)
);
?>