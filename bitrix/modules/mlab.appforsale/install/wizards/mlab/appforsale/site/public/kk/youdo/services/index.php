<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?$APPLICATION->IncludeComponent(
	"mlab:appforsale.personal.profile", 
	".default", 
	array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "#PROFILE_IBLOCK_ID#",
		"SEF_FOLDER" => "/youdo/personal/profile/",
		"SEF_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "profiles",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "#SECTION_CODE_PATH#/",
			"form" => "#SECTION_CODE_PATH#/form/",
		)
	),
	false
);?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>