<?
$MESS['ASIDE_BG'] = 'Фон левого меню';
$MESS['ASIDE_LOGIN_SHOW'] = 'Отображение блока авторизации левого меню';
$MESS['ASIDE_LOGIN_BG'] = 'Фон блока авторизации левого меню';
$MESS['FROM_SITE_SETTING'] = 'Из настроек сайта';
// SWITCH
$MESS['ASIDE_SWITCH_SHOW'] = 'Отображать блок переключения роли пользователя';
$MESS['ASIDE_SWITCH_LEFT_NAME'] = 'Текст кнопки "Клиент"';
$MESS['ASIDE_SWITCH_LEFT_NAME_DEFAULT'] = 'Клиент';
$MESS['ASIDE_SWITCH_LEFT_LINK'] = 'Ссылка на главную страницу клиента';
$MESS['ASIDE_SWITCH_LEFT_LINK_DEFAULT'] = '/youdo/';
$MESS['ASIDE_SWITCH_LEFT_TITLE'] = 'Заголовок страницы клиента';
$MESS['ASIDE_SWITCH_LEFT_TITLE_DEFAULT'] = 'Все заказы';
$MESS['ASIDE_SWITCH_RIGHT_NAME'] = 'Текст кнопки "Исполнитель"';
$MESS['ASIDE_SWITCH_RIGHT_NAME_DEFAULT'] = 'Исполнитель';
$MESS['ASIDE_SWITCH_RIGHT_LINK'] = 'Ссылка на главную страницу исполнителя';
$MESS['ASIDE_SWITCH_RIGHT_LINK_DEFAULT'] = '/youdo/tasks-executor/';
$MESS['ASIDE_SWITCH_RIGHT_TITLE'] = 'Заголовок страницы исполнителя';
$MESS['ASIDE_SWITCH_RIGHT_TITLE_DEFAULT'] = 'Мои заказы';
$MESS['ASIDE_SWITCH_CLOSE_MENU'] = 'Скрывать меню после выбора';
$MESS['ASIDE_SWITCH_GROUPS'] = 'Группа исполнителя';
?>