<?
use Bitrix\Main\Config\Option;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!defined('WIZARD_SITE_ID') || !defined('WIZARD_SITE_DIR'))
	return;

if(Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'N')
{
	if(file_exists(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/'))
	{
		CopyDirFiles(
			WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/',
			WIZARD_SITE_PATH,
			$rewrite = true,
			$recursive = true,
			$delete_after_copy = false
		);
	}
}
else
{
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installNotification', 'N', WIZARD_SITE_ID) == 'N')
	{
		if(file_exists(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/'))
		{
			unlink(WIZARD_SITE_ROOT_PATH.'/youdo/.menu.php');
			
			CopyDirFiles(
				WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/',
				WIZARD_SITE_PATH,
				$rewrite = false,
				$recursive = true,
				$delete_after_copy = false
			);
		}
		
		WizardServices::PatchHtaccess(WIZARD_SITE_PATH);
		
		$arUrlRewrite = array();
		if (file_exists(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php'))
		{
			include(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php');
		}
		
		$arNewUrlRewrite = array(
			array(
				'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/tasks-executor/#',
				'RULE' => '',
				'ID' => 'mlab:appforsale.tasks.executor',
				'PATH' => WIZARD_SITE_DIR.'youdo/personal/tasks/my/index.php',
			)
		);
		
		foreach ($arNewUrlRewrite as $arUrl)
		{
			if (!in_array($arUrl, $arUrlRewrite))
			{
				CUrlRewriter::Add($arUrl);
			}
		}
	}
	
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installLanding', 'N', WIZARD_SITE_ID) == 'N')
	{
		if(file_exists(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/'))
		{
			CopyDirFiles(
				WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/',
				WIZARD_SITE_PATH,
				$rewrite = true,
				$recursive = false,
				$delete_after_copy = false
			);
		}
	}
	
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installSite', 'N', WIZARD_SITE_ID) == 'N')
	{
		if(file_exists(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/'))
		{
			CopyDirFiles(
			WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/',
			WIZARD_SITE_PATH,
			$rewrite = true,
			$recursive = false,
			$delete_after_copy = false
			);
		}
	}
	
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installSubscription', 'N', WIZARD_SITE_ID) == 'N')
	{
		if(file_exists(WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/'))
		{
			CopyDirFiles(
				WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/youdo/subscription/',
				WIZARD_SITE_PATH.'youdo/subscription/',
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = false
			);
			
			CopyDirFiles(
			WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/youdo/services/',
			WIZARD_SITE_PATH.'youdo/services/',
			$rewrite = true,
			$recursive = true,
			$delete_after_copy = false
			);
			
			WizardServices::PatchHtaccess(WIZARD_SITE_PATH);
			
			$arUrlRewrite = array();
			if (file_exists(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php'))
			{
				include(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php');
			}
				
			$arNewUrlRewrite = array(
				array(
					'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/subscription/#',
					'RULE' => '',
					'ID' => 'mlab:appforsale.subscription',
					'PATH' => WIZARD_SITE_DIR.'youdo/subscription/index.php',
				),
				array(
					'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/personal/profile/#',
					'RULE' => '',
					'ID' => 'mlab:appforsale.personal.profile',
					'PATH' => WIZARD_SITE_DIR.'youdo/services/index.php',
				)
			);
			
			foreach ($arNewUrlRewrite as $arUrl)
			{
				if (!in_array($arUrl, $arUrlRewrite))
				{
					CUrlRewriter::Add($arUrl);
				}
			}
		}
	}
	
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installInvite', 'N', WIZARD_SITE_ID) == 'N')
	{
		
		CopyDirFiles(
			WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/youdo/invite/',
			WIZARD_SITE_PATH.'youdo/invite/',
			$rewrite = true,
			$recursive = true,
			$delete_after_copy = false
		);

		WizardServices::PatchHtaccess(WIZARD_SITE_PATH);
		
		$arUrlRewrite = array();
		if (file_exists(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php'))
		{
			include(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php');
		}
		
		$arNewUrlRewrite = array(
			array(
				'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/invite/#',
				'RULE' => '',
				'ID' => 'mlab:appforsale.invite',
				'PATH' => WIZARD_SITE_DIR.'youdo/invite/index.php',
			)
		);
		
		foreach ($arNewUrlRewrite as $arUrl)
		{
			if (!in_array($arUrl, $arUrlRewrite))
			{
				CUrlRewriter::Add($arUrl);
			}
		}
		
	}
	
	if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installExecutor', 'N', WIZARD_SITE_ID) == 'N')
	{
	
		CopyDirFiles(
			WIZARD_ABSOLUTE_PATH.'/site/public/'.LANGUAGE_ID.'/youdo/executors/',
			WIZARD_SITE_PATH.'youdo/executors/',
			$rewrite = true,
			$recursive = true,
			$delete_after_copy = false
		);
	
		WizardServices::PatchHtaccess(WIZARD_SITE_PATH);
	
		$arUrlRewrite = array();
		if (file_exists(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php'))
		{
			include(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php');
		}
	
		$arNewUrlRewrite = array(
				array(
						'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/executors/#',
						'RULE' => '',
						'ID' => 'mlab:appforsale.executor',
						'PATH' => WIZARD_SITE_DIR.'youdo/executors/index.php',
				)
		);
	
		foreach ($arNewUrlRewrite as $arUrl)
		{
			if (!in_array($arUrl, $arUrlRewrite))
			{
				CUrlRewriter::Add($arUrl);
			}
		}
	
	}
}

if(Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return

WizardServices::PatchHtaccess(WIZARD_SITE_PATH);

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php'))
{
	include(WIZARD_SITE_ROOT_PATH.'/urlrewrite.php');
}

$arNewUrlRewrite = array(
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/tasks-new/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.task.new',
		'PATH' => WIZARD_SITE_DIR.'youdo/tasks-new/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.task',
		'PATH' => WIZARD_SITE_DIR.'youdo/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/tasks-customer/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.tasks.customer',
		'PATH' => WIZARD_SITE_DIR.'youdo/tasks-customer/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/tasks-executor/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.tasks.executor',
		'PATH' => WIZARD_SITE_DIR.'youdo/personal/tasks/my/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/subscription/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.subscription',
		'PATH' => WIZARD_SITE_DIR.'youdo/subscription/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/personal/profile/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.personal.profile',
		'PATH' => WIZARD_SITE_DIR.'youdo/services/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/invite/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.invite',
		'PATH' => WIZARD_SITE_DIR.'youdo/invite/index.php',
	),
	array(
		'CONDITION' => '#^'.WIZARD_SITE_DIR.'youdo/executors/#',
		'RULE' => '',
		'ID' => 'mlab:appforsale.executor',
		'PATH' => WIZARD_SITE_DIR.'youdo/executors/index.php',
	)
);

foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}
?>