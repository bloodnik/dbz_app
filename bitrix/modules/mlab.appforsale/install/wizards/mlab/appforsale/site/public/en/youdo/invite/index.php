<? 
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
?>
<?
$APPLICATION->IncludeComponent(
	"mlab:appforsale.invite", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "invite",
		"IBLOCK_ID" => "#INVITE_IBLOCK_ID#",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/youdo/invite/",
		"SEF_URL_TEMPLATES" => array(
			"out" => "",
			"in" => "#CODE#/",
		)
	),
	false
);
?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>