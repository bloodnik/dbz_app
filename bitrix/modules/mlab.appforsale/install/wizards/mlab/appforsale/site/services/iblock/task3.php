<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_TASK_ID = (isset($_SESSION['WIZARD_TASK_IBLOCK_ID']) ? intval($_SESSION['WIZARD_TASK_IBLOCK_ID']) : 0);

if ($IBLOCK_TASK_ID)
{
	$arSectionLink = array(
			// kurerskie-uslugi
			'uslugi-peshego-kurera' => '40',
			'uslugi-kurera-na-legkovom-avto' => '46',
			'kupit-i-dostavit' => '30',
			'srochnaya-dostavka' => '46',
			'dostavka-produktov' => '30',
			'dostavka-edy-iz-restoranov' => '30',
			'kurer-na-den' => '46',
			// bytovoy-remont
			'master-na-chas' => '53',
			'remont-pod-klyuch' => '97',
			'santekhnicheskie-raboty' => '58',
			'elektromontazhnye-raboty' => '70',
			'otdelochnye-raboty' => '79',
			'potolki' => '71',
			'poly' => '59',
			'plitochnye-raboty' => '59',
			'sborka-i-remont-mebeli' => '58',
			'ustanovka-i-remont-dverey-zamkov' => '54',
			'osteklenie-otdelka-balkonov-i-lodzhiy' => '53',
			'krovelnye-i-fasadnye-raboty' => '65',
			'otoplenie-vodosnabzhenie-kanalizatsiya' => '79',
			'izolyatsionnye-raboty' => '42',
			'stroitelno-montazhnye-raboty' => '65',
			'krupnoy-stroitelstvo' => '97',
			'okhrannye-sistemy' => '79',
			// gruzoperevozki
			'perevozka-veshchey-pereezdy' => '52',
			'passazhirskie-perevozki' => '48',
			'stroitelnye-gruzy-i-oborudovanie' => '48',
			'vyvoz-musora' => '52',
			'evakuatory' => '38',
			'mezhdugorodnye-perevozki' => '38',
			'uslugi-gruzchikov' => '48',
			'perevozka-produktov' => '38',
			'uslugi-manipulyatora' => '43',
			// uborka-i-pomoshch-po-khozyaystvu
			'podderzhivayushchaya-ubor' => '63',
			'generalnaya-uborka' => '63',
			'myte-okon' => '58',
			'vynos-musora' => '56',
			'pomoshch-shvei' => '41',
			'prigotovlenie-edy' => '46',
			'glazhenie-belya' => '44',
			'khimchistka' => '48',
			'ukhod-za-zhivotnymi' => '52',
			'raboty-v-sadu-ogorode-na-uchastke' => '58',
			'sidelki' => '52',
			'nyani' => '58',
			// virtualnyy-pomoshnik
			'rabota-s-tekstom-kopirayting-perevody' => '62',
			'poisk-i-obrabotka-informatsii' => '44',
			'rabota-v-excel-power-point-i-t-d' => '62',
			'rasshifrovka-audio-i-videozapisey' => '61',
			'razmeshchenie-obyavleniy' => '45',
			'reklama-i-prodvizhenie-v-internete' => '39',
			'obzvon-po-baze' => '46',
			// kompyuternaya-pomoshch
			'remont-kompyuterov-i-noutbukov' => '62',
			'ustanovka-i-nastroyka-operats-sistem-programm' => '64',
			'udalenie-virusov' => '64',
			'nastroyka-interneta-i-wi-fi' => '62',
			'remont-i-zamena-komplektuyushchikh' => '53',
			'vosstanovlenie-dannykh' => '52',
			'nastroyka-i-remont-orgtekhniki' => '47',
			'konsultatsiya-i-obuchenie' => '39',
			// meropriyatiya-i-promo-aktsii
			'pomoshch-v-provedenii-meropriyatiy' => '53',
			'razdacha-promo-materialov' => '34',
			'taynyy-pokupatel' => '38',
			'raznorabochiy' => '47',
			'promouter' => '38',
			'tamada-vedushchiy-animator' => '56',
			'promo-model' => '39',
			// dizayn
			'logotipy-firmennyy-stil' => '68',
			'listovki-buklety-vizitki' => '62',
			'illyustratsii-ikonki-art' => '62',
			'dizayn-saytov' => '60',
			'bannery-oformlenie-sots-setey' => '62',
			'3d-grafika-animatsiya' => '53',
			'infografika-prezentatsii' => '62',
			'naruzhnaya-reklama-vyveski' => '65',
			'dizayn-intererov-i-arkhitektura' => '66',
			// web-razrabotka
			'sayt-pod-klyuch' => '55',
			'podderzhka-i-pomoshch-po-saytu' => '49',
			'programmirovanie' => '47',
			'verstka' => '49',
			'razrabotka-prilozheniy-i-programm' => '44',
			// foto-i-video-uslugi
			'fotosemka' => '70',
			'videosemka' => '62',
			'retush-fotografiy' => '62',
			'sozdanie-videorolikov-pod-klyuch' => '35',
			'montazh-i-tsvetokorrektsiya-video' => '70',
			'otsifrovka' => '53',
			// ustanovka-i-remont-tekhniki
			'kholodilniki-i-morozilnye-kamery' => '44',
			'stiralnye-i-sushilnye-mashiny' => '46',
			'posudomoechnye-mashiny' => '46',
			'elektricheskie-plity-i-paneli' => '47',
			'gazovye-plity' => '47',
			'dukhovye-shkafy' => '47',
			'vytyazhki' => '47',
			'klimaticheskaya-tekhnika' => '30',
			'vodonagrevateli-boylery-kotly-kolonki' => '47',
			'shveynye-mashiny' => '32',
			'pylesosy-i-ochistiteli' => '32',
			'utyugi-i-ukhod-za-odezhdoy' => '32',
			'kofemashiny' => '47',
			'svch-pechi' => '32',
			'melkaya-kukhonnaya-tekhnika' => '32',
			'ukhod-za-telom-i-zdorovem' => '32',
			'stroitelnaya-i-sadovaya-tekhnika' => '32',
			// krasota-i-zdorove
			'parikmakherskie-uslugi' => '39',
			'nogtevoy-servis' => '37',
			'massazh' => '54',
			'kosmetologiya-makiyazh' => '43',
			'stilisty-i-imedzhmeykery' => '20',
			'personalnyy-trener' => '43',
			// remont-tsifrovoy-tekhniki
			'planshety-i-telefony' => '38',
			'audiotekhnika-i-sistemy' => '28',
			'televizory-i-monitory' => '28',
			'avtomobilnaya-elektronika' => '28',
			'video-fototekhnika' => '28',
			'igrovye-pristavki' => '28',
			'sputnikovye-i-efirnye-anteny' => '28',
			'chasy-i-khronometry' => '15',
			// yuridicheskaya-pomoshch
			'yuridicheskaya-konsultatsiya' => '58',
			'sostavlenie-i-proverka-dogovorov' => '58',
			'sostavlenie-i-podacha-zhalob-iskov' => '63',
			'oformlenie-dokumentov' => '58',
			'registratsiya-likvidatsiya-kompaniy' => '59',
			// repetitory-i-obrazovanie
			'inostrannye-yazyki' => '53',
			'podgotovka-k-shkole' => '50',
			'nachalnaya-shkola' => '50',
			'srednyaya-shkola-vypusknye-klassy' => '53',
			'podgotovka-k-gia' => '25',
			'podgotovka-k-ege' => '53',
			'vuzovskaya-programma' => '53',
			'logopedy' => '15',
			'muzyka' => '53',
			'sport' => '33',
			'krasota-i-ukhod-za-soboy' => '33',
			'rukodelie-i-khobbi' => '33',
			// remont-transporta
			'tekhnicheskoe-obsluivanie-avtomobilya' => '48',
			'diagnostika-i-remont-dvigatelya-kpp-i-khodovoy-chasti' => '53',
			'obsluzhivanie-sistemy-konditsionirovaniya' => '43',
			'kuzovnoy-remont' => '53',
			'avtoelektrika' => '43',
			'avtostekla-i-tonirovka' => '33',
			'shinomontazh' => '43',
			'moyka-i-khimchistka' => '43',
			'tyuning-vneshniy-i-vnutrenniy' => '53',
			'pomoshch-na-doroge' => '38',
			'motoservis' => '43'
	);
	
	$dbSection = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $IBLOCK_TASK_ID));
	while ($arSection = $dbSection->GetNext())
	{
		$obSection = new CIBlockSection;
		$obSection->Update($arSection['ID'], array(
				'UF_OFFER_PRICE' => $arSectionLink[$arSection['CODE']]
		));
	}
}
?>