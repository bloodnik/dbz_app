<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
header('Content-Type: application/json');
if (CModule::IncludeModule('mlab.appforsale'))
{
	if ($USER->IsAuthorized())
	{
		$arFields = array(
				'UF_LATITUDE' => $_POST['lat'],
				'UF_LONGITUDE' => $_POST['lng']
		);
		if ($USER_FIELD_MANAGER->CheckFields('USER', $USER->GetID(), $arFields))
		{
			$USER_FIELD_MANAGER->Update('USER', $USER->GetID(), $arFields);
		}
		
		if (CModule::IncludeModule('iblock'))
		{
			$dbProfile = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_CODE' => 'profile',
					'ACTIVE' => 'Y',
					'CREATED_BY' => $USER->GetID()
				),
				false,
				false,
				array(	
					'ID',
					'IBLOCK_ID'
				)
			);
			while($arProfile = $dbProfile->GetNext())
			{
				CIBlockElement::SetPropertyValuesEx(
					(int) $arProfile['ID'],
					$arProfile['IBLOCK_ID'],
					array(
						'GEO' => $_POST['lat'].','.$_POST['lng']
					)
				);
			}
		}
	}
}
echo \Bitrix\Main\Web\Json::encode(array('response' => 1));
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');	
?>