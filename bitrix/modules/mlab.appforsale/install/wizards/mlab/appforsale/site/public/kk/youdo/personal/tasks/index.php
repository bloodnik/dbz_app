<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Тапсырмалар');
?>
<?
$APPLICATION->IncludeComponent(
	"appforsale:personal.tasks", 
	"", 
	array(
		"IBLOCK_ID" => "#TASK_IBLOCK_ID#",
		"PROFILE_IBLOCK_ID" => "#PROFILE_IBLOCK_ID#",
		"OFFER_IBLOCK_ID" => "#OFFER_IBLOCK_ID#"
	),
	false
);
?>
<? 
if ($_SESSION['mode'] != 1)
{
	$_SESSION['mode'] = 1;
	echo '<script>app.onCustomEvent("OnAfterUserUpdate")</script>';	
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>