<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'wizard_installed', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_COMMENT_ID = (isset($_SESSION['WIZARD_COMMENT_IBLOCK_ID']) ? intval($_SESSION['WIZARD_COMMENT_IBLOCK_ID']) : 0);

if ($IBLOCK_COMMENT_ID)
{
	$iblockCode = 'comment_'.WIZARD_SITE_ID;
	$iblock = new CIBlock;
	$arFields = array(
		'ACTIVE' => 'Y',
		'FIELDS' => array(
			'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown')
		),
		'LIST_MODE' => 'C',
		'CODE' => 'comment',
		'XML_ID' => $iblockCode
	);
	$iblock->Update($IBLOCK_COMMENT_ID, $arFields);	
	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/profile/index.php', array('COMMENT_IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('COMMENT_IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/my/index.php', array('COMMENT_IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('COMMENT_IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	
	//TEXT
	$dbProperty = CIBlockProperty::GetList(array(), array('CODE' => 'TEXT', 'IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	if ($arProperty = $dbProperty->GetNext())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('COMMENT_PROPERTY_ID' => $arProperty['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/my/index.php', array('COMMENT_PROPERTY_ID' => $arProperty['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('COMMENT_PROPERTY_ID' => $arProperty['ID']));
	}
	
	//RATING
	$dbProperty = CIBlockProperty::GetList(array(), array('CODE' => 'RATING', 'IBLOCK_ID' => $IBLOCK_COMMENT_ID));
	if ($arProperty = $dbProperty->GetNext())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/tasks-customer/index.php', array('RATING_PROPERTY_ID' => $arProperty['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/personal/tasks/my/index.php', array('RATING_PROPERTY_ID' => $arProperty['ID']));
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('RATING_PROPERTY_ID' => $arProperty['ID']));
	}

}
else if (Bitrix\Main\Config\Option::get('mlab.appforsale', 'installExecutor', 'N', WIZARD_SITE_ID) == 'N')
{
	$dbIBlock = CIBlock::GetList(
			array(),
			array(
					'TYPE' => 'comments',
					'SITE_ID' => WIZARD_SITE_ID,
					'ACTIVE'=>'Y',
					"CODE"=>'comment'
			),
			false
	);
	if ($arIBlock = $dbIBlock->Fetch())
	{
		CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/executors/index.php', array('COMMENT_IBLOCK_ID' => $arIBlock['ID']));
	}
}
?>