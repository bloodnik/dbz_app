<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

if (!Bitrix\Main\Loader::includeModule('iblock'))
	return;

if(Bitrix\Main\Config\Option::get('mlab.appforsale', 'installInvite', 'N', WIZARD_SITE_ID) == 'Y')
	return;

$IBLOCK_INVITE_ID = (isset($_SESSION['WIZARD_INVITE_IBLOCK_ID']) ? intval($_SESSION['WIZARD_INVITE_IBLOCK_ID']) : 0);

if ($IBLOCK_INVITE_ID)
{
	$iblockCode = 'invite_'.WIZARD_SITE_ID;
	$iblock = new CIBlock;
	$arFields = array(
		'ACTIVE' => 'Y',
		'FIELDS' => array(
			'NAME' => array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'unknown')
		),
		'LIST_MODE' => 'C',
		'CODE' => 'invite',
		'XML_ID' => $iblockCode
	);
	$iblock->Update($IBLOCK_INVITE_ID, $arFields);	
	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH.'/youdo/invite/index.php', array('INVITE_IBLOCK_ID' => $IBLOCK_INVITE_ID));
}
?>