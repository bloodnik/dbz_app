<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->ShowHeadStrings();
$APPLICATION->ShowCSS();

$APPLICATION->IncludeComponent(
	'mlab:appforsale.interface.iblock',
	'',
	array(
		'PID' => $_GET['PID'],
		'PARENT_SECTION' => $_GET['PARENT_SECTION']
	),
	false
);
?>