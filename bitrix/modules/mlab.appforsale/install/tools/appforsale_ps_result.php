<?
use \Bitrix\Main\Application;

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define('NOT_CHECK_PERMISSIONS', true);
define("DisableEventsCheck", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;

if (CModule::IncludeModule("mlab.appforsale"))
{
	$context = Application::getInstance()->getContext();
	$request = $context->getRequest();

	CAppforsalePayment::processRequest($request);
}

$APPLICATION->FinalActions();
die();
?>