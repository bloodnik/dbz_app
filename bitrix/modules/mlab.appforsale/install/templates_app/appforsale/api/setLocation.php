<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: application/json");
if (CModule::IncludeModule("mlab.appforsale"))
{
	if ($USER->IsAuthorized())
	{
		CModule::IncludeModule("iblock");
		$rsProfile = CIBlockElement::GetList(
				array(),
				array(
						"IBLOCK_ID" => "#profile_iblock_id#",
						"ACTIVE" => "Y",
						"CREATED_BY" => $USER->GetID()
				),
				false,
				false,
				array(	
					"ID"
				)
		);
		while($arProfile = $rsProfile->GetNext())
		{
			CIBlockElement::SetPropertyValuesEx(
				(int) $arProfile['ID'],
				"#profile_iblock_id#",
				array(
					"GEO" => $_POST['lat'].",".$_POST['lng']
				)
			);
		}
	}
	$arResult = array("response" => 1);
}
echo \Bitrix\Main\Web\Json::encode($arResult);
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/epilog_after.php");	
?>