<? 
require($_SERVER["DOCUMENT_ROOT"]."/#folder#/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?
$APPLICATION->IncludeComponent(
	"appforsale:personal.tasks", 
	"", 
	array(
		"IBLOCK_ID" => "#task_iblock_id#",
		"PROFILE_IBLOCK_ID" => "#profile_iblock_id#",
		"OFFER_IBLOCK_ID" => "#offer_iblock_id#"
	),
	false
);
?>
<? 
if ($_SESSION['mode'] != 1)
{
	$_SESSION['mode'] = 1;
	echo '<script>app.onCustomEvent("OnAfterUserUpdate")</script>';	
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>