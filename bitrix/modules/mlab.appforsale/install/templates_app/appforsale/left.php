<? 
require($_SERVER["DOCUMENT_ROOT"]."/#folder#/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?
$APPLICATION->IncludeComponent(
	"appforsale:menu", 
	"", 
	array(
		"PATH_TO_LOGIN" => SITE_DIR."#folder#/login/",
		"PATH_TO_PROFILE" => SITE_DIR."#folder#/personal/",
		"PATH_TO_FILE_MENU" => SITE_DIR."#folder#/.menu.php",
		"BACKGROUND_COLOR_MENU" => "#3F51B5",
		"BACKGROUND_COLOR_BAR" => "#3F51B5",
		"TITLE_COLOR_BAR" => "#ffffff"
	),
	false
);
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>