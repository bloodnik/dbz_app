<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (LANGUAGE_ID == 'en')
{
	$MESS = array(
		"MY_ORDERS" => "My orders",
		"NEW_ORDERS" => "New task",
		"CONTRACTORS" => "Contractors",
		"SWITCH_TO" => "Switch to",
		"ORDERS" => "Orders",
		"CONTRACTOR" => "Contractor",
		"CLIENT" => "Client",
		"SERVICE" => "Provide the types of services",
		"TYPES_OF_SERVICES" => "The types of services"
	);
}
else
{
	$MESS = array(
		"MY_ORDERS" => "Мои задания",
		"NEW_ORDERS" => "Добавить задание",
		"CONTRACTORS" => "Исполнители",
		"SWITCH_TO" => "Переключиться в режим",
		"ORDERS" => "Заказы",
		"CONTRACTOR" => "Исполнитель",
		"CLIENT" => "Заказчик",
		"SERVICE" => "Оказываемые виды услуг",
		"TYPES_OF_SERVICES" => "Виды услуг"
	);
}

if ($_SESSION['mode'] != 1)
{
	$arMenu = array(
		array(
			"type" => "section",
			"items" => array(
					array(
							"text" => $MESS['NEW_ORDERS'],
							"icon" => SITE_DIR."youdo/images/menu_tasks.png",
							"data-url" => SITE_DIR."youdo/tasks/new/"
					),
					array(
						"text" => $MESS['MY_ORDERS'],
						"icon" => SITE_DIR."youdo/images/menu_tasks.png",
						"data-url" => SITE_DIR."youdo/"
					),
					array(
						"text" => $MESS['CONTRACTORS'],
						"icon" => SITE_DIR."youdo/images/menu_users.png",
						"data-url" => SITE_DIR."youdo/profile/"
					)
				)
		),
		array(
			"type" => "section",
			"text" => $MESS['SWITCH_TO'],
			"items" => array (
					array(
						"text" => $MESS['CONTRACTOR'],
						"icon" => SITE_DIR."youdo/images/menu_mode.png",
						"data-url" => SITE_DIR."youdo/personal/tasks/",
						"data-title" => $MESS['ORDERS']
					)
			)
		)
	);
}
else
{
	$arMenu = array(
		array(
			"type" => "section",
			"items" => array(
					array(
						"text" => $MESS['ORDERS'],
						"icon" => SITE_DIR."youdo/images/menu_tasks.png",
						"data-url" => SITE_DIR."youdo/personal/tasks/"
					),
					array(
							"text" => $MESS['MY_ORDERS'],
							"icon" => SITE_DIR."youdo/images/menu_tasks.png",
							"data-url" => SITE_DIR."youdo/personal/tasks/my/"
					),
					array(
						"text" => $MESS['SERVICE'],
						"icon" => SITE_DIR."youdo/images/menu_profile.png",
						"data-url" => SITE_DIR."youdo/personal/profile/",
						"data-title" => $MESS['TYPES_OF_SERVICES']
					)
			)
		),
		array(
			"type" => "section",
			"text" => $MESS['SWITCH_TO'],
			"items" => array (
					array(
						"text" => $MESS['CLIENT'],
						"icon" => SITE_DIR."youdo/images/menu_mode.png",
						"data-url" => SITE_DIR."youdo/",
						"data-title" => $MESS['MY_ORDERS']
					)
			)
		)
	);
}
?>