<? 
require($_SERVER["DOCUMENT_ROOT"]."/#folder#/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?
$APPLICATION->IncludeComponent(
	"appforsale:tasks.my", 
	"", 
	array(
		"IBLOCK_ID" => "#task_iblock_id#",
		"OFFER_IBLOCK_ID" => "#offer_iblock_id#",
		"COMMENT_IBLOCK_ID" => "#comment_iblock_id#"
	),
	false
);
?>
<? 
if ($_SESSION['mode'] != 0)
{
	$_SESSION['mode'] = 0;
	echo '<script>app.onCustomEvent("OnAfterUserUpdate")</script>';	
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>