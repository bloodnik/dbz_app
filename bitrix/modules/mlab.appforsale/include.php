<?

use Bitrix\Main\Web\HttpClient;
use Mlab\Appforsale\MobileApp;

IncludeModuleLangFile(__FILE__);

$arClasses = array(
	"CAppforsaleUserAccount"       => "classes/mysql/user.php",
	"CAppforsaleUserTransact"      => "classes/mysql/user_transact.php",
	"CAppforsaleRecurring"         => "classes/mysql/recurring.php",
	"CAppforsalePayment"           => "classes/general/payment.php",
	"CAppforsalePush"              => "classes/general/push.php",
	"CAppforsaleStatus"            => "classes/general/status.php",
	"CAppforsaleTask"              => "classes/general/task.php",
	"CAppforsalePosition"          => "classes/general/position.php",
	"CAppforsaleSubscription"      => "classes/general/CAppforsaleSubscription.php",
	"CAppforsaleProfile"           => "classes/general/CAppforsaleProfile.php",
	"CAppforsaleComission"         => "classes/general/CAppforsaleComission.php",
	"CAppforsaleTariffOption"      => "classes/general/tariffs/CAppforsaleTariffOption.php",
	"AAppforsaleTariff"            => "classes/general/tariffs/AAppforsaleTariff.php",
	"CAppforsaleTariff"            => "classes/general/tariffs/CAppforsaleTariff.php",
	"CAppforsaleTaskBonus"         => "classes/general/CAppforsaleTaskBonus.php",
	"CAppforsaleTaskBonusAccount"  => "classes/general/CAppforsaleTaskBonusAccount.php",
	"CAppforsaleTaskBonusTransact" => "classes/general/CAppforsaleTaskBonusTransact.php",
	"CAppforsalePushSchedule"      => "classes/general/CAppforsalePushSchedule.php",
	"CAppforsaleComplexPush"       => "classes/general/CAppforsaleComplexPush.php",
	"CAppforsaleReturnRequests"    => "classes/general/CAppforsaleReturnRequests.php",
	"CAppforsaleBonusReturns"      => "classes/general/CAppforsaleBonusReturns.php",
	"CAppforsaleTaskAcceptLog"     => "classes/general/CAppforsaleTaskAcceptLog.php",
	"CAppforsaleCashback"          => "classes/general/CAppforsaleCashback.php",
	"CAppforsaleReturnsRating"     => "classes/general/CAppforsaleReturnsRating.php",
);
CModule::AddAutoloadClasses("mlab.appforsale", $arClasses);

define("APPFORSALE_PROC_REC_NUM", 3);
define("APPFORSALE_PROC_REC_ATTEMPTS", 3);
define("APPFORSALE_PROC_REC_TIME", 43200);
define("APPFORSALE_PROC_REC_FREQUENCY", 7200);
define("APPFORSALE_VALUE_PRECISION", 4);

CJSCore::RegisterExt("mlab_appforsale_core", [
	"js" => "/bitrix/js/mlab_appforsale/core.js"
]);

CJSCore::RegisterExt("mlab_appforsale_pull", [
	"js" => "/bitrix/js/mlab_appforsale/pull.js"
]);

class CAppforsale {
	public function Init() {
		global $APPLICATION, $USER;

		if ( ! MobileApp::getInstance()->isDevice()) {

			if (empty($_COOKIE['APPLICATION'])) {
				$APPLICATION->AddHeadString('<script type="text/javascript">
    				var bMobile = false;
    				try { if (typeof(exec.postMessage) === "function") bMobile = true; } catch (e) {}
    				try { if (typeof(window.webkit.messageHandlers.exec.postMessage) === "function") bMobile = true; } catch (e) {}
    				if (BX.setCookie("APPLICATION", bMobile ? "Y" : "N"))
    					location.reload();
    			</script>');
			} else if ($_COOKIE['APPLICATION'] == 'Y') {
				$APPLICATION->AddHeadString('<style>.afs-aside-nav-control { display: none !important }</style>');
			}
		} else {
			$APPLICATION->AddHeadString('<style>.afs-aside-nav-control { display: none !important }</style>');
		}

		$APPLICATION->AddHeadString('<script type="text/javascript" src="' . CUtil::GetAdditionalFileURL("/bitrix/js/appforsale/kernel.js") . '"></script>');

		$google_analytics_key = COption::GetOptionString("mlab.appforsale", "google_analytics_key", "");
		if ($google_analytics_key != "") {
			$APPLICATION->AddHeadString("<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '" . $google_analytics_key . "', 'auto');
  ga('send', 'pageview');

</script>", false, true);
		}
	}

	public function buildMenu() {
		global $arMenu;
		$arItems = array();

		if ( ! empty($arMenu)) {
			foreach ($arMenu as $arItem) {
				$arItems[] = $arItem;
			}
		}

		return $arItems;
	}

	public function AuthForm() {
		global $APPLICATION;
		$APPLICATION->IncludeComponent(
			"appforsale:login",
			"",
			array(),
			false
		);
		die("");
	}

	public static function getFieldHtml($arField) {
		global $APPLICATION;
		$resultHtml = '';

		switch ($arField["PROPERTY_TYPE"]) {
			case 'S':
				if ($arField['USER_TYPE'] == 'map_google') {
					ob_start();
					$APPLICATION->IncludeComponent(
						"appforsale:interface.map",
						"",
						$arField,
						false
					);

					$resultHtml .= ob_get_contents();
					ob_end_clean();
				} else {
					$bGeo = false;
					if ($arField['CODE'] == 'TAXI_TO' || $arField['CODE'] == 'TAXI_FROM' || (substr($arField['CODE'], strlen($arField['CODE']) - 4) == '_MAP')) {
						$bGeo = true;
					}

					$bTextarea = false;
					if (intval($arField['ROW_COUNT']) > 1 || $arField['USER_TYPE'] == 'HTML') {
						$bTextarea = true;
					}

					if ($arField['USER_TYPE'] == 'HTML') {
						$value = $arField['VALUE']['TEXT'];
					} else {
						$value = $arField['VALUE'];
					}

					if (empty($value)) {
						global $_ID;
						if ($_ID == 0) {
							if ($arField['USER_TYPE'] == 'HTML') {
								$value = $arField['DEFAULT_VALUE']['TEXT'];
							} else {
								$value = $arField['DEFAULT_VALUE'];
							}
						}
					}
					$resultHtml .= '<div class="bx_field">' . $arField['NAME'] . ($arField['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '') . '</div>';
					$resultHtml .= '<div style="position: relative"><' . ($bTextarea ? 'textarea' : 'input') . ' id="' . $arField['ID'] . '" name="' . $arField['ID'] . '" type="';

					switch ($arField['USER_TYPE']) {
						case "Date": // yyyy-mm-dd
							$value      = ConvertDateTime($value, "Y-m-d");
							$resultHtml .= 'date';
							break;
						case "DateTime": // yyyy-mm-dd HH:MM
							$value      = ConvertDateTime($value, "Y-m-d") . 'T' . ConvertDateTime($value, "H:i:s");
							$resultHtml .= 'datetime-local';
							break;
						default:
							$resultHtml .= 'text';
							break;
					}

					if ($bTextarea) {
						$resultHtml .= '" rows="' . $arField['ROW_COUNT'] . '" style="width: 100%; ' . (intval($arField['USER_TYPE_SETTINGS']['height']) > 0 ? 'height: ' . $arField['USER_TYPE_SETTINGS']['height'] . 'px;' : '') . '">' . $value . '</textarea>';
					} else {
						$resultHtml .= '" value="' . $value . '" />';
					}

					if ($bGeo) {
						$resultHtml .= '<img onclick="app.loadPageBlank({url: \'' . $_SERVER['REQUEST_URI'] . '&map=Y&code=' . $arField['ID'] . '\', title: \'\'})" style="box-sizing: content-box; position: absolute; right: 0px; top: 0px; width: 24px; height: 24px; padding: 9px" src="/bitrix/images/mlab.appforsale/geo.png" />';
						$resultHtml .= '<script>BX.addCustomEvent("appforsale_map", function(params) {  if (params.code == \'' . $arField['ID'] . '\') { BX(\'' . $arField['ID'] . '\').value = params.value; } });</script>';
					}


					$resultHtml .= '</div>';
				}
				break;
			case 'N':
				if ($arField['CODE'] == 'RATING') {
					ob_start();
					$APPLICATION->IncludeComponent(
						"appforsale:interface.rating",
						"",
						$arField,
						false
					);
					$resultHtml .= ob_get_contents();
					ob_end_clean();
				} else {
					$value      = $arField['VALUE'];
					$resultHtml .= '<div class="bx_field">' . $arField['NAME'] . ($arField['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '') . '</div><div><input id="' . $arField['ID'] . '" name="' . $arField['ID'] . '" type="number" pattern="[0-9]*" value="' . $value . '" /></div>';
				}
				break;
			case 'L':
				if ($arField['LIST_TYPE'] == 'C') {
					$resultHtml     .= '<div class="bx_field">' . $arField['NAME'] . ($arField['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '') . '</div><div>';
					$rsPropertyEnum = CIBlockPropertyEnum::GetList(
						array(
							"DEF"  => "DESC",
							"SORT" => "ASC"
						),
						array(
							"IBLOCK_ID" => $arField['IBLOCK_ID'],
							"CODE"      => $arField['CODE']
						)
					);
					$items          = array();
					$checked_i      = -1;

					if ( ! empty($arField['VALUE'])) {
						$checked_i = -2;
					}

					$i = 0;
					while ($arPropertyEnum = $rsPropertyEnum->GetNext()) {
						if (empty($arField['VALUE']) && $arPropertyEnum['DEF'] == 'Y') {
							$checked_i = $i;
						}
						$i++;
						$items[] = $arPropertyEnum;
					}

					if ($arField['MULTIPLE'] == 'Y' || count($items) == 1) {
						foreach ($items as $i => $arPropertyEnum) {
							$resultHtml .= '<label style="display: block; padding: 8px 0px 8px 0px; box-sizing: border-box; width: 100%"><input' . (($checked_i == $i || in_array($arPropertyEnum['VALUE'], $arField['VALUE'])) ? ' checked="checked"' : '') . ' type="checkbox" ' . ($i == 0 ? 'id="' . $arField['ID'] . '"' : '') . ' name="' . $arField['ID'] . '" value="' . $arPropertyEnum['ID'] . '"> ' . $arPropertyEnum['VALUE'] . '</label>';
						}
					} else {
						$resultHtml .= '<label style="display: block; padding: 8px 0px 8px 0px; box-sizing: border-box; width: 100%"><input' . ($checked_i == -1 ? ' checked="checked"' : '') . ' type="radio" id="' . $arField['ID'] . '" name="' . $arField['ID'] . '" value=""> ' . GetMessage('NOT_INSTALLED') . '</label>';
						foreach ($items as $i => $arPropertyEnum) {
							$resultHtml .= '<label style="display: block; padding: 8px 0px 8px 0px; box-sizing: border-box; width: 100%"><input' . (($checked_i == $i || $arField['VALUE'] == $arPropertyEnum['VALUE']) ? ' checked="checked"' : '') . ' type="radio" name="' . $arField['ID'] . '" value="' . $arPropertyEnum['ID'] . '"> ' . $arPropertyEnum['VALUE'] . '</label>';
						}
					}


					$resultHtml .= '</div>';
				} else {
					$resultHtml     .= '<div class="bx_field">' . $arField['NAME'] . ($arField['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '') . '</div><div><select' . ($arField['MULTIPLE'] == 'Y' ? ' multiple="multiple"' : '') . ' id="' . $arField['ID'] . '" name="' . $arField['ID'] . '">';
					$rsPropertyEnum = CIBlockPropertyEnum::GetList(
						array(
							"DEF"  => "DESC",
							"SORT" => "ASC",
							"ID"   => "ASC"
						),
						array(
							"IBLOCK_ID" => $arField['IBLOCK_ID'],
							"CODE"      => $arField['CODE']
						)
					);

					if ($arField['MULTIPLE'] != 'Y') {
						$resultHtml .= '<option value="">' . GetMessage('NOT_INSTALLED') . '</option>';
					} else {
						if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
							$resultHtml .= '<optgroup disabled hidden></optgroup>';
						}
					}

					while ($arPropertyEnum = $rsPropertyEnum->GetNext()) {
						$selected = false;
						if ( ! empty($arField['VALUE'])) {
							if (is_array($arField['VALUE'])) {
								if (in_array($arPropertyEnum['VALUE'], $arField['VALUE'])) {
									$selected = true;
								}
							} else {
								if ($arPropertyEnum['VALUE'] == $arField['VALUE']) {
									$selected = true;
								}
							}
						} else {
							if ($arPropertyEnum['DEF'] == 'Y') {
								$selected = true;
							}
						}

						$resultHtml .= '<option value="' . $arPropertyEnum['ID'] . '"' . ($selected ? ' selected="selected"' : '') . '>' . $arPropertyEnum['VALUE'] . '</option>';
					}
					$resultHtml .= '</select></div>';
				}
				break;

			case 'E':
				$resultHtml .= '<div class="bx_field">' . $arField['NAME'] . ($arField['REQUIRED'] == 'Y' ? ' <span class="required">*</span>' : '') . '</div>
						<div><select' . ($arField['MULTIPLE'] == 'Y' ? ' multiple="multiple"' : '') . ' id="' . $arField['ID'] . '" name="' . $arField['ID'] . '">';

				$items     = array();
				$rsElement = CIBlockElement::GetList(
					array(
						"ID" => "ASC"
					),
					array(
						"IBLOCK_ID" => $arField['LINK_IBLOCK_ID'],
						"ACTIVE"    => "Y"
					),
					false,
					false,
					array(
						"ID",
						"NAME"
					)
				);
				while ($arElement = $rsElement->GetNext()) {
					$items[] = array(
						"ID"    => $arElement['ID'],
						"VALUE" => $arElement['NAME']
					);
				}

				if ($arField['CODE'] == 'CITY' && $arField['VALUE'] == '') {
					global $USER;
					$rsUser            = CUser::GetByID($USER->GetID());
					$arUser            = $rsUser->GetNext(false);
					$arField2['VALUE'] = $arUser['UF_CITY'];

				}


				if ($arField['MULTIPLE'] != 'Y') {
					$resultHtml .= '<option value="">' . GetMessage('NOT_INSTALLED') . '</option>';
				} else {
					if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
						$resultHtml .= '<optgroup disabled hidden></optgroup>';
					}
				}

				foreach ($items as $arItem) {
					$resultHtml .= '<option value="' . $arItem['ID'] . '"' . (($arItem['ID'] == $arField['VALUE'] || $arItem['ID'] == $arField2['VALUE'] || in_array($arItem['ID'], $arField['VALUE'])) ? ' selected="selected"' : '') . '>' . $arItem['VALUE'] . '</option>';
				}
				$resultHtml .= '</select></div>';

				break;
			case 'F':
				ob_start();
				$APPLICATION->IncludeComponent(
					"appforsale:interface.file",
					"",
					$arField,
					false
				);

				$resultHtml .= ob_get_contents();
				ob_end_clean();
				break;
		}

		if ( ! empty($arField['HINT'])) {
			$resultHtml .= '<div><small style="color: #ccc">' . $arField['HINT'] . '</small></div>';
		}

		return $resultHtml;
	}

	public function SendMessage($to, $message, $data = array()) {
		global $push_id;

		if (empty($to)) {
			return false;
		}

		$google_push_key = COption::GetOptionString("mlab.appforsale", "google_push_key", "");
		if ($google_push_key == "") {
			return false;
		}

		if (is_array($to)) {
			$registration_ids = CAppforsale::getToken($to, $message, $data);
			if (empty($registration_ids)) {
				return false;
			}

			shuffle($registration_ids);

			$part = array();
			foreach (array_chunk($registration_ids, 1000) as $key => $ids) {
				$part[ $key ] = array(
					"registration_ids" => $ids
				);
			}

		} else {
			$part = array(
				0 => array(
					"to" => "/topics/" . $to
				)
			);
		}

		foreach ($part as $key => $payload) {
			if ( ! empty($message)) {
				if (is_array($message)) {
					$payload['notification'] = $message;
					if ( ! isset($payload['notification']['sound'])) {
						$payload['notification']['sound'] = 'default';
					}
				} else {
					$payload['notification'] = array(
						"body"  => $message,
						"sound" => "default"
					);
				}

			} else {
				$payload['content_available'] = true;
			}

			$payload['priority'] = "high";

			if ( ! empty($data) && is_array($data)) {
				$payload['data'] = $data;
			}

			$request = new HttpClient();
			$request->setHeader("Authorization", "key=" . $google_push_key);
			$request->setHeader("Content-Type", "application/json");
			$request->post("https://fcm.googleapis.com/fcm/send", \Bitrix\Main\Web\Json::encode($payload));
		}

		return true;
	}

	private function getToken($to, $message, $data) {
		if ($data['archive'] != 'N') {
			CModule::IncludeModule('iblock');
			$dbIBlock = CIBlock::GetList(array(), array('CODE' => 'notification'));
			if ($arIBlock = $dbIBlock->Fetch()) {
//				// EMAIL
//				$dbUser = \Bitrix\Main\UserTable::getList(array(
//						"filter" => array(
//								"ID" => $to
//						),
//						"select" => array(
//								"LOGIN",
//								"EMAIL"
//						)
//				));
//				while ($arUser = $dbUser->fetch())
//				{
//					if (!preg_match("/^".$arUser["LOGIN"]."/", $arUser["EMAIL"]))
//					{
//						$event = new CEvent;
//						$event->SendImmediate("PUSH", "s1", array(
//								"EMAIL" => $arUser["EMAIL"],
//								"TEXT" => $message['body'] . "\n".$data['url']
//						));
//					}
//				}

				$entity_data_class = getHighloadEntity(1);

				foreach ($to as $uid) {
//					$obElement = new CIBlockElement;
//					$obElement->Add(array(
//						'IBLOCK_ID'    => $arIBlock['ID'],
//						'CREATED_BY'   => $uid,
//						'NAME'         => $message['body'],
//						'PREVIEW_TEXT' => $data['url']
//					));

					// Массив полей для добавления
					$hlData = array(
						"UF_NAME"         => $message['body'],
						"UF_LINK"         => $data['url'],
						"UF_CREATED_BY"   => $uid,
						"UF_CREATED_DATE" => new \Bitrix\Main\Type\DateTime(),
						"UF_ACTIVE"       => "Y",
						"UF_TASK_ID"      => $data['id']
					);
					$entity_data_class::add($hlData);
				}
			}
		}

		global $push_id;
		global $DB;
		$arResult = array();
		$res      = $DB->Query("SELECT SETTINGS, TOKEN FROM mlab_appforsale_push_device WHERE USER_ID IN (" . implode(',', $to) . ")");
		while ($ar = $res->Fetch()) {
			$setting = unserialize($ar['SETTINGS']);
			if (intval($push_id) > 0) {
				if ($setting[ $push_id ] != 'off') {
					$arResult[] = $ar['TOKEN'];
				}
			} else {
				$arResult[] = $ar['TOKEN'];
			}
		}

		return $arResult;
	}

	public function CheckPush($arFields) {
		if (empty($arFields) || ! is_array($arFields)) {
			return false;
		}

		global $push_id;
		global $DB;
		$res = $DB->Query('SELECT ID, UNPACK, APPLICATION, LAST_PUSH FROM appforsale_push WHERE ACTIVE = "Y" ORDER BY PRIORITY DESC, SORT ASC');
		while ($ar = $res->Fetch()) {
			$conds[ $ar['ID'] ]['UNPACK']      = $ar['UNPACK'];
			$conds[ $ar['ID'] ]['APPLICATION'] = $ar['APPLICATION'];
			$conds[ $ar['ID'] ]['LAST_PUSH']   = $ar['LAST_PUSH'];
		}
		unset($res, $ar);

		foreach ($conds as $id => $cond) {
			$push_id = $id;
			if (CAppforsalePush::__Unpack($arFields, $cond['UNPACK'])) {
				CAppforsalePush::__ApplyActions($arFields, $cond['APPLICATION']);

				if ($cond['LAST_PUSH'] == 'Y') {
					break;
				}
			}
		}
	}

	function OnAfterIBlockElementAdd(&$arFields) {
// 		if($arFields["ID"]>0)
// 		{
// 			CAppforsale::CheckPush($arFields);
// 		}
	}

	function OnAfterIBlockElementUpdate(&$arFields) {
// 		if($arFields["RESULT"])
// 		{
// 			CAppforsale::CheckPush($arFields);
// 		}
	}

	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu) {
		$aGlobalMenu['global_menu_appforsale'] = array(
			"menu_id"    => "appforsale",
			"text"       => "AppForSale",
			"icon"       => "global_menu_appforsale",
			"sort"       => 150,
			"items_id"   => "global_menu_appforsale",
			'skip_chain' => false,
			"module_id"  => "mlab.appforsale",
			"items"      => array()
		);
	}

	function PrepareSql(&$arFields, $arOrder, &$arFilter, $arGroupBy, $arSelectFields, $obUserFieldsSql = false, $callback = false, $arOptions = array()) {
		global $DB;

		$arGroupByFunct = array("COUNT", "AVG", "MIN", "MAX", "SUM");

		$arAlreadyJoined = array();

		$strSqlGroupBy = '';
		$strSqlFrom    = '';
		$strSqlSelect  = '';
		$strSqlWhere   = '';

		// GROUP BY -->
		if (is_array($arGroupBy) && count($arGroupBy) > 0) {
			$arSelectFields = $arGroupBy;
			foreach ($arGroupBy as $key => $val) {
				$val = ToUpper($val);
				$key = ToUpper($key);
				if (array_key_exists($val, $arFields) && ! in_array($key, $arGroupByFunct)) {
					if ($strSqlGroupBy != '') {
						$strSqlGroupBy .= ", ";
					}
					$strSqlGroupBy .= $arFields[ $val ]["FIELD"];

					if (isset($arFields[ $val ]["FROM"])
					    && strlen($arFields[ $val ]["FROM"]) > 0
					    && ! in_array($arFields[ $val ]["FROM"], $arAlreadyJoined)) {
						if ($strSqlFrom != '') {
							$strSqlFrom .= " ";
						}
						$strSqlFrom        .= $arFields[ $val ]["FROM"];
						$arAlreadyJoined[] = $arFields[ $val ]["FROM"];
					}
				}
			}
		}
		// <-- GROUP BY

		// SELECT -->
		$arFieldsKeys = array_keys($arFields);

		if (is_array($arGroupBy) && count($arGroupBy) == 0) {
			$strSqlSelect = "COUNT(%%_DISTINCT_%% " . $arFields[ $arFieldsKeys[0] ]["FIELD"] . ") as CNT ";
		} else {
			if (isset($arSelectFields) && ! is_array($arSelectFields) && is_string($arSelectFields) && strlen($arSelectFields) > 0 && array_key_exists($arSelectFields, $arFields)) {
				$arSelectFields = array($arSelectFields);
			}

			if ( ! isset($arSelectFields)
			     || ! is_array($arSelectFields)
			     || count($arSelectFields) <= 0
			     || in_array("*", $arSelectFields)) {
				$countFieldKey = count($arFieldsKeys);
				for ($i = 0; $i < $countFieldKey; $i++) {
					if (isset($arFields[ $arFieldsKeys[ $i ] ]["WHERE_ONLY"])
					    && $arFields[ $arFieldsKeys[ $i ] ]["WHERE_ONLY"] == "Y") {
						continue;
					}

					if ($strSqlSelect != '') {
						$strSqlSelect .= ", ";
					}

					if ($arFields[ $arFieldsKeys[ $i ] ]["TYPE"] == "datetime") {
						if ((ToUpper($DB->type) == "ORACLE" || ToUpper($DB->type) == "MSSQL") && (array_key_exists($arFieldsKeys[ $i ], $arOrder))) {
							$strSqlSelect .= $arFields[ $arFieldsKeys[ $i ] ]["FIELD"] . " as " . $arFieldsKeys[ $i ] . "_X1, ";
						}

						$strSqlSelect .= $DB->DateToCharFunction($arFields[ $arFieldsKeys[ $i ] ]["FIELD"], "FULL") . " as " . $arFieldsKeys[ $i ];
					} elseif ($arFields[ $arFieldsKeys[ $i ] ]["TYPE"] == "date") {
						if ((ToUpper($DB->type) == "ORACLE" || ToUpper($DB->type) == "MSSQL") && (array_key_exists($arFieldsKeys[ $i ], $arOrder))) {
							$strSqlSelect .= $arFields[ $arFieldsKeys[ $i ] ]["FIELD"] . " as " . $arFieldsKeys[ $i ] . "_X1, ";
						}

						$strSqlSelect .= $DB->DateToCharFunction($arFields[ $arFieldsKeys[ $i ] ]["FIELD"], "SHORT") . " as " . $arFieldsKeys[ $i ];
					} else {
						$strSqlSelect .= $arFields[ $arFieldsKeys[ $i ] ]["FIELD"] . " as " . $arFieldsKeys[ $i ];
					}

					if (isset($arFields[ $arFieldsKeys[ $i ] ]["FROM"])
					    && strlen($arFields[ $arFieldsKeys[ $i ] ]["FROM"]) > 0
					    && ! in_array($arFields[ $arFieldsKeys[ $i ] ]["FROM"], $arAlreadyJoined)) {
						if (strlen($strSqlFrom) > 0) {
							$strSqlFrom .= " ";
						}
						$strSqlFrom        .= $arFields[ $arFieldsKeys[ $i ] ]["FROM"];
						$arAlreadyJoined[] = $arFields[ $arFieldsKeys[ $i ] ]["FROM"];
					}
				}
			} else {
				foreach ($arSelectFields as $key => $val) {
					$val = ToUpper($val);
					$key = ToUpper($key);
					if (array_key_exists($val, $arFields)) {
						if (strlen($strSqlSelect) > 0) {
							$strSqlSelect .= ", ";
						}

						if (in_array($key, $arGroupByFunct)) {
							$strSqlSelect .= $key . "(" . $arFields[ $val ]["FIELD"] . ") as " . $val;
						} else {
							if ($arFields[ $val ]["TYPE"] == "datetime") {
								if ((ToUpper($DB->type) == "ORACLE" || ToUpper($DB->type) == "MSSQL") && (array_key_exists($val, $arOrder))) {
									$strSqlSelect .= $arFields[ $val ]["FIELD"] . " as " . $val . "_X1, ";
								}

								$strSqlSelect .= $DB->DateToCharFunction($arFields[ $val ]["FIELD"], "FULL") . " as " . $val;
							} elseif ($arFields[ $val ]["TYPE"] == "date") {
								if ((ToUpper($DB->type) == "ORACLE" || ToUpper($DB->type) == "MSSQL") && (array_key_exists($val, $arOrder))) {
									$strSqlSelect .= $arFields[ $val ]["FIELD"] . " as " . $val . "_X1, ";
								}

								$strSqlSelect .= $DB->DateToCharFunction($arFields[ $val ]["FIELD"], "SHORT") . " as " . $val;
							} else {
								$strSqlSelect .= $arFields[ $val ]["FIELD"] . " as " . $val;
							}
						}

						if (isset($arFields[ $val ]["FROM"])
						    && strlen($arFields[ $val ]["FROM"]) > 0
						    && ! in_array($arFields[ $val ]["FROM"], $arAlreadyJoined)) {
							if (strlen($strSqlFrom) > 0) {
								$strSqlFrom .= " ";
							}
							$strSqlFrom        .= $arFields[ $val ]["FROM"];
							$arAlreadyJoined[] = $arFields[ $val ]["FROM"];
						}
					}
				}
			}

			if (strlen($strSqlGroupBy) > 0) {
				if (strlen($strSqlSelect) > 0) {
					$strSqlSelect .= ", ";
				}
				$strSqlSelect .= "COUNT(%%_DISTINCT_%% " . $arFields[ $arFieldsKeys[0] ]["FIELD"] . ") as CNT";
			} else {
				$strSqlSelect = "%%_DISTINCT_%% " . $strSqlSelect;
			}
		}
		// <-- SELECT

		// WHERE -->
		$arSqlSearch = array();

		if ( ! is_array($arFilter)) {
			$filter_keys = array();
		} else {
			$filter_keys = array_keys($arFilter);
		}

		$countFilterKey = count($filter_keys);
		for ($i = 0; $i < $countFilterKey; $i++) {
			$vals = $arFilter[ $filter_keys[ $i ] ];
			if ( ! is_array($vals)) {
				$vals = array($vals);
			} else {
				$vals = array_values($vals);
			}

			$key          = $filter_keys[ $i ];
			$key_res      = CAppforsale::GetFilterOperation($key);
			$key          = $key_res["FIELD"];
			$strNegative  = $key_res["NEGATIVE"];
			$strOperation = $key_res["OPERATION"];
			$strOrNull    = $key_res["OR_NULL"];

			if (array_key_exists($key, $arFields)) {
				$arSqlSearch_tmp = array();
				if (count($vals) > 0) {
					if ($strOperation == "IN") {
						if (isset($arFields[ $key ]["WHERE"])) {
							$arSqlSearch_tmp1 = call_user_func_array(
								$arFields[ $key ]["WHERE"],
								array($vals, $key, $strOperation, $strNegative, $arFields[ $key ]["FIELD"], $arFields, $arFilter)
							);
							if ($arSqlSearch_tmp1 !== false) {
								$arSqlSearch_tmp[] = $arSqlSearch_tmp1;
							}
						} else {
							if ($arFields[ $key ]["TYPE"] == "int") {
								array_walk($vals, create_function("&\$item", "\$item=IntVal(\$item);"));
								$vals = array_unique($vals);
								$val  = implode(",", $vals);

								if (count($vals) <= 0) {
									$arSqlSearch_tmp[] = "(1 = 2)";
								} else {
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " IN (" . $val . "))";
								}
							} elseif ($arFields[ $key ]["TYPE"] == "double") {
								array_walk($vals, create_function("&\$item", "\$item=DoubleVal(\$item);"));
								$vals = array_unique($vals);
								$val  = implode(",", $vals);

								if (count($vals) <= 0) {
									$arSqlSearch_tmp[] = "(1 = 2)";
								} else {
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							} elseif ($arFields[ $key ]["TYPE"] == "string" || $arFields[ $key ]["TYPE"] == "char") {
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->ForSql(\$item).\"'\";"));
								$vals = array_unique($vals);
								$val  = implode(",", $vals);

								if (count($vals) <= 0) {
									$arSqlSearch_tmp[] = "(1 = 2)";
								} else {
									$arSqlSearch_tmp[] = (($strNegative == "Y") ? " NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							} elseif ($arFields[ $key ]["TYPE"] == "datetime") {
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->CharToDateFunction(\$GLOBALS[\"DB\"]->ForSql(\$item), \"FULL\").\"'\";"));
								$vals = array_unique($vals);
								$val  = implode(",", $vals);

								if (count($vals) <= 0) {
									$arSqlSearch_tmp[] = "1 = 2";
								} else {
									$arSqlSearch_tmp[] = ($strNegative == "Y" ? " NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							} elseif ($arFields[ $key ]["TYPE"] == "date") {
								array_walk($vals, create_function("&\$item", "\$item=\"'\".\$GLOBALS[\"DB\"]->CharToDateFunction(\$GLOBALS[\"DB\"]->ForSql(\$item), \"SHORT\").\"'\";"));
								$vals = array_unique($vals);
								$val  = implode(",", $vals);

								if (count($vals) <= 0) {
									$arSqlSearch_tmp[] = "1 = 2";
								} else {
									$arSqlSearch_tmp[] = ($strNegative == "Y" ? " NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " (" . $val . "))";
								}
							}
						}
					} else {
						$countVals = count($vals);
						for ($j = 0; $j < $countVals; $j++) {
							$val = $vals[ $j ];

							if (isset($arFields[ $key ]["WHERE"])) {
								$arSqlSearch_tmp1 = call_user_func_array(
									$arFields[ $key ]["WHERE"],
									array($val, $key, $strOperation, $strNegative, $arFields[ $key ]["FIELD"], $arFields, $arFilter)
								);
								if ($arSqlSearch_tmp1 !== false) {
									$arSqlSearch_tmp[] = $arSqlSearch_tmp1;
								}
							} else {
								if ($arFields[ $key ]["TYPE"] == "int") {
									if ((IntVal($val) == 0) && (strpos($strOperation, "=") !== false)) {
										$arSqlSearch_tmp[] = "(" . $arFields[ $key ]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND" : "OR") . " " . (($strNegative == "Y") ? "NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " 0)";
									} else {
										$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[ $key ]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " " . IntVal($val) . " )";
									}
								} elseif ($arFields[ $key ]["TYPE"] == "double") {
									$val = str_replace(",", ".", $val);

									if ((DoubleVal($val) == 0) && (strpos($strOperation, "=") !== false)) {
										$arSqlSearch_tmp[] = "(" . $arFields[ $key ]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND" : "OR") . " " . (($strNegative == "Y") ? "NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " 0)";
									} else {
										$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[ $key ]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " " . DoubleVal($val) . " )";
									}
								} elseif ($arFields[ $key ]["TYPE"] == "string" || $arFields[ $key ]["TYPE"] == "char") {
									if ($strOperation == "QUERY") {
										$arSqlSearch_tmp[] = GetFilterQuery($arFields[ $key ]["FIELD"], $val, "Y");
									} else {
										if ((strlen($val) == 0) && (strpos($strOperation, "=") !== false)) {
											$arSqlSearch_tmp[] = "(" . $arFields[ $key ]["FIELD"] . " IS " . (($strNegative == "Y") ? "NOT " : "") . "NULL) " . (($strNegative == "Y") ? "AND NOT" : "OR") . " (" . $DB->Length($arFields[ $key ]["FIELD"]) . " <= 0) " . (($strNegative == "Y") ? "AND NOT" : "OR") . " (" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " '" . $DB->ForSql($val) . "' )";
										} else {
											$arSqlSearch_tmp[] = (($strNegative == "Y") ? " " . $arFields[ $key ]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " '" . $DB->ForSql($val) . "' )";
										}
									}
								} elseif ($arFields[ $key ]["TYPE"] == "datetime") {
									if (strlen($val) <= 0) {
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? "NOT" : "") . "(" . $arFields[ $key ]["FIELD"] . " IS NULL)";
									} else {
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? " " . $arFields[ $key ]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " " . $DB->CharToDateFunction($DB->ForSql($val), "FULL") . ")";
									}
								} elseif ($arFields[ $key ]["TYPE"] == "date") {
									if (strlen($val) <= 0) {
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? "NOT" : "") . "(" . $arFields[ $key ]["FIELD"] . " IS NULL)";
									} else {
										$arSqlSearch_tmp[] = ($strNegative == "Y" ? " " . $arFields[ $key ]["FIELD"] . " IS NULL OR NOT " : "") . "(" . $arFields[ $key ]["FIELD"] . " " . $strOperation . " " . $DB->CharToDateFunction($DB->ForSql($val), "SHORT") . ")";
									}
								}
							}
						}
					}
				}

				if (isset($arFields[ $key ]["FROM"])
				    && strlen($arFields[ $key ]["FROM"]) > 0
				    && ! in_array($arFields[ $key ]["FROM"], $arAlreadyJoined)) {
					if (strlen($strSqlFrom) > 0) {
						$strSqlFrom .= " ";
					}
					$strSqlFrom        .= $arFields[ $key ]["FROM"];
					$arAlreadyJoined[] = $arFields[ $key ]["FROM"];
				}

				$strSqlSearch_tmp = "";
				$countSqlSearch   = count($arSqlSearch_tmp);
				for ($j = 0; $j < $countSqlSearch; $j++) {
					if ($j > 0) {
						$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
					}
					$strSqlSearch_tmp .= "(" . $arSqlSearch_tmp[ $j ] . ")";
				}
				if ($strOrNull == "Y") {
					if (strlen($strSqlSearch_tmp) > 0) {
						$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
					}
					$strSqlSearch_tmp .= "(" . $arFields[ $key ]["FIELD"] . " IS " . ($strNegative == "Y" ? "NOT " : "") . "NULL)";

					if ($arFields[ $key ]["TYPE"] == "int" || $arFields[ $key ]["TYPE"] == "double") {
						if (strlen($strSqlSearch_tmp) > 0) {
							$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
						}
						$strSqlSearch_tmp .= "(" . $arFields[ $key ]["FIELD"] . " " . ($strNegative == "Y" ? "<>" : "=") . " 0)";
					} elseif ($arFields[ $key ]["TYPE"] == "string" || $arFields[ $key ]["TYPE"] == "char") {
						if (strlen($strSqlSearch_tmp) > 0) {
							$strSqlSearch_tmp .= ($strNegative == "Y" ? " AND " : " OR ");
						}
						$strSqlSearch_tmp .= "(" . $arFields[ $key ]["FIELD"] . " " . ($strNegative == "Y" ? "<>" : "=") . " '')";
					}
				}

				if ($strSqlSearch_tmp != "") {
					$arSqlSearch[] = "(" . $strSqlSearch_tmp . ")";
				}
			}
		}

		// custom subquery callback
		if (is_callable($callback)) {
			$arSqlSearch[] = call_user_func_array($callback, array($arFields));
		}

		$countSqlSearch = count($arSqlSearch);
		for ($i = 0; $i < $countSqlSearch; $i++) {
			if ($strSqlWhere != '') {
				$strSqlWhere .= " AND ";
			}
			$strSqlWhere .= "(" . $arSqlSearch[ $i ] . ")";
		}

		// <-- WHERE

		// ORDER BY -->
		$arSqlOrder = array();
		if ( ! is_array($arOrder)) {
			$arOrder = array();
		}
		foreach ($arOrder as $by => $order) {
			$by    = ToUpper($by);
			$order = ToUpper($order);

			if ($order != "ASC") {
				$order = "DESC";
			} else {
				$order = "ASC";
			}

			if (is_array($arGroupBy) && count($arGroupBy) > 0 && in_array($by, $arGroupBy)) {
				$arSqlOrder[] = " " . $by . " " . $order . " ";
			} elseif (array_key_exists($by, $arFields)) {
				$arSqlOrder[] = " " . $arFields[ $by ]["FIELD"] . " " . $order . " ";

				if (isset($arFields[ $by ]["FROM"])
				    && strlen($arFields[ $by ]["FROM"]) > 0
				    && ! in_array($arFields[ $by ]["FROM"], $arAlreadyJoined)) {
					if (strlen($strSqlFrom) > 0) {
						$strSqlFrom .= " ";
					}
					$strSqlFrom        .= $arFields[ $by ]["FROM"];
					$arAlreadyJoined[] = $arFields[ $by ]["FROM"];
				}
			} elseif ($obUserFieldsSql) {
				$arSqlOrder[] = " " . $obUserFieldsSql->GetOrder($by) . " " . $order . " ";
			}
		}

		$nullsLast     = isset($arOptions['NULLS_LAST']) ? (bool) $arOptions['NULLS_LAST'] : false;
		$strSqlOrderBy = "";
		DelDuplicateSort($arSqlOrder);
		$countSqlOrder = count($arSqlOrder);
		for ($i = 0; $i < $countSqlOrder; $i++) {
			if (strlen($strSqlOrderBy) > 0) {
				$strSqlOrderBy .= ", ";
			}

			$order = (substr($arSqlOrder[ $i ], -3) == "ASC") ? "ASC" : "DESC";
			if ( ! $nullsLast) {
				if (ToUpper($DB->type) == "ORACLE") {
					if ($order === "ASC") {
						$strSqlOrderBy .= $arSqlOrder[ $i ] . " NULLS FIRST";
					} else {
						$strSqlOrderBy .= $arSqlOrder[ $i ] . " NULLS LAST";
					}
				} else {
					$strSqlOrderBy .= $arSqlOrder[ $i ];
				}
			} else {
				$field = substr($arSqlOrder[ $i ], 0, -strlen($order) - 1);
				if (ToUpper($DB->type) === "MYSQL") {
					if ($order === 'ASC') {
						$strSqlOrderBy .= '(CASE WHEN ISNULL(' . $field . ') THEN 1 ELSE 0 END) ' . $order . ', ' . $field . " " . $order;
					} else {
						$strSqlOrderBy .= $field . " " . $order;
					}
				} elseif (ToUpper($DB->type) === "MSSQL") {
					if ($order === 'ASC') {
						$strSqlOrderBy .= '(CASE WHEN ' . $field . ' IS NULL THEN 1 ELSE 0 END) ' . $order . ', ' . $field . " " . $order;
					} else {
						$strSqlOrderBy .= $field . " " . $order;
					}

				} elseif (ToUpper($DB->type) === "ORACLE") {
					if ($order === 'DESC') {
						$strSqlOrderBy .= $field . " " . $order . " NULLS LAST";
					} else {
						$strSqlOrderBy .= $field . " " . $order;
					}
				}
			}
		}

		// <-- ORDER BY

		return array(
			"SELECT"  => $strSqlSelect,
			"FROM"    => $strSqlFrom,
			"WHERE"   => $strSqlWhere,
			"GROUPBY" => $strSqlGroupBy,
			"ORDERBY" => $strSqlOrderBy
		);
	}

	function GetFilterOperation($key) {
		$strNegative = "N";
		if (substr($key, 0, 1) == "!") {
			$key         = substr($key, 1);
			$strNegative = "Y";
		}

		$strOrNull = "N";
		if (substr($key, 0, 1) == "+") {
			$key       = substr($key, 1);
			$strOrNull = "Y";
		}

		if (substr($key, 0, 2) == ">=") {
			$key          = substr($key, 2);
			$strOperation = ">=";
		} elseif (substr($key, 0, 1) == ">") {
			$key          = substr($key, 1);
			$strOperation = ">";
		} elseif (substr($key, 0, 2) == "<=") {
			$key          = substr($key, 2);
			$strOperation = "<=";
		} elseif (substr($key, 0, 1) == "<") {
			$key          = substr($key, 1);
			$strOperation = "<";
		} elseif (substr($key, 0, 1) == "@") {
			$key          = substr($key, 1);
			$strOperation = "IN";
		} elseif (substr($key, 0, 1) == "~") {
			$key          = substr($key, 1);
			$strOperation = "LIKE";
		} elseif (substr($key, 0, 1) == "%") {
			$key          = substr($key, 1);
			$strOperation = "QUERY";
		} else {
			$strOperation = "=";
		}

		return array("FIELD" => $key, "NEGATIVE" => $strNegative, "OPERATION" => $strOperation, "OR_NULL" => $strOrNull);
	}


	function OnAfterUserAdd(&$arFields) {
		$credit = COption::GetOptionInt('mlab.appforsale', "starting_balance", 0);
		$credit = (int) $credit;
		if ($credit > 0 && intval($arFields['ID']) > 0) {
			CAppforsaleUserAccount::UpdateAccount(
				$arFields['ID'],
				$credit, "OUT_CHARGE_OFF",
				0,
				"Start balance"
			);
		}
	}
}

function AppforsaleFormatCurrency($fSum) {
	$intDecimals = 2;
	if (round($fSum, 2) == round($fSum, 0)) {
		$intDecimals = 0;
	}

	$price         = number_format((float) $fSum, $intDecimals, '.', ' ');
	$format_string = COption::GetOptionString("mlab.appforsale", "format_string", "# " . GetMessage('APPFORSALE_RUB'));

	return preg_replace('/(^|[^&])#/', '${1}' . $price, $format_string);
}

?>