<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectException;
use Bitrix\Main\Type\DateTime;

class CAppforsaleSubscription {

	// USER FIELDS
	const LAST_WRITEOF_DATE_CODE = 'UF_LAST_WRITEOF_DATE';
	const NEXT_WRITEOF_DATE_CODE = 'UF_NEXT_WRITEOF_DATE';
	const SUBSCRIPTION_DAYS_CODE = 'UF_SUBSCRIPTION_DAYS';
	const DEMO_PERIOD_CODE = 'UF_DEMO_PERIOD';

	// IBLOCKS
	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	/**
	 * Возвращает состояние флага "Демо период"
	 *
	 * @param int $userId
	 *
	 * @return bool Статус демо периода пользователя
	 */
	public static function isDemoPeriod($userId = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$demoPeriod = false;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$demoPeriod = $arUser[ self::DEMO_PERIOD_CODE ];
		}

		return ! ! $demoPeriod;
	}

	public static function setDemoPeriod($userId = 0, $value = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::DEMO_PERIOD_CODE => $value));

		return true;
	}

	/**
	 * Есть ли у мастера дни подписки на платные задачи
	 *
	 * @param int $userId
	 *
	 * @return bool
	 */
	public static function hasSubscriptionDays($userId = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		return self::getUserSubscriptionDays($userId) > 0 ? true : false;
	}

	/**
	 * Проверка что заявка бесплатная
	 *
	 * @param int $sectionId
	 *
	 * @return bool
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function isFreeTask($sectionId = 0) {
		try {
			Loader::includeModule('iblock');
		} catch (LoaderException $e) {
			throw new \Bitrix\Main\SystemException($e->getMessage());
		}
		$isFree = false;

		// Получаем ID групп к которым привязана заявка
		$dbTaskSectionsId = CIBlockElement::GetElementGroups($sectionId, true);
		while ($ar_group = $dbTaskSectionsId->Fetch()) {
			$arSectionsId[] = $ar_group["ID"];
		}

		$arFilter = array(
			"IBLOCK_ID" => self::TASKS_IBLOCK_ID,
			"ID"        => $arSectionsId,
		);

		// Проверяем, если есть хоть один бесплатный раздел, то заявка бесплатная
		$rsTaskSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "UF_FEE_PRICE"));
		while ($arTaskSections = $rsTaskSections->getNext()) {
			if ( ! $arTaskSections['UF_FEE_PRICE']) {
				$isFree = true;
			}
		}

		return $isFree;
	}


	/**
	 * Активна ли подписка
	 *
	 * @param int $userId
	 *
	 * @return bool
	 */
	public static function isSubscriptionActive($userId = 0) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		try {
			return self::hasSubscriptionDays($userId);
		} catch (ObjectException $e) {
			return false;
		}
	}

	/**
	 * Возвращает количество оставшихся дней подписки у мастера
	 *
	 * @param int $userId
	 *
	 * @return int - Количество дней подписок пользователя
	 */
	public static function getUserSubscriptionDays($userId = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$daysCount = 0;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$daysCount = $arUser[ self::SUBSCRIPTION_DAYS_CODE ];
		}

		return $daysCount;
	}

	/**
	 * Добавляет дни подписок для мастера
	 * И устанавливает дату следующего списания
	 *
	 * @param $userId - id пользователя
	 * @param int $daysCount - количество дней для записи
	 *
	 * @return boolean
	 */
	public static function addUserSubscriptionDays($userId, $daysCount = 0) {
		global $APPLICATION;
		$result = false;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");
		}

		try {
			$ob = new CUserTypeManager;
			$ob->Update("USER", $userId, array(self::SUBSCRIPTION_DAYS_CODE => $daysCount));

			self::setNextWriteofDate($userId);

			$result = true;
		} catch (\Exception $e) {
			$APPLICATION->ThrowException($e->getMessage());
		}

		return $result;
	}

	/**
	 * Уменьшает количество дней подписки мастера
	 *
	 * @param $userId - id пользователя
	 *
	 * @return int
	 */
	public static function decrementSubscriptionDays($userId) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$currentDaysCount = self::getUserSubscriptionDays($userId);

		if ($currentDaysCount > 0) {
			$ob = new CUserTypeManager;
			$ob->Update("USER", $userId, array(self::SUBSCRIPTION_DAYS_CODE => --$currentDaysCount));
		}

		return $currentDaysCount;
	}


	/**
	 * @param int $userId
	 *
	 * @return String  Дата последнего списания
	 */
	public static function getLastWriteofDate($userId = 0) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");
		}

		$date = null;
		$diff = 0;
		if (CTimeZone::Enabled()) {
			$diff = CTimeZone::GetOffset();
		}

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$ts = MakeTimeStamp($arUser[ self::LAST_WRITEOF_DATE_CODE ], "DD.MM.YYYY HH:MI");

			if ($ts) {
				$date = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $ts);
			}
		}

		return $date;
	}

	/**
	 * Устанавливает дату последнего списания абон платы
	 *
	 * @param $userId - id пользователя
	 *
	 * @return boolean
	 */
	public static function setLastWriteofDate($userId) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}
		\CTimeZone::Disable();

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::LAST_WRITEOF_DATE_CODE => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time())));

		\CTimeZone::Enable();

		return true;
	}

	/**
	 * @param int $userId
	 *
	 * @return String  Дата следующего списания
	 */
	public static function getNextWriteofDate($userId = 0) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");
		}

		$diff = 0;
		if (CTimeZone::Enabled()) {
			$diff = CTimeZone::GetOffset();
		}

		$date = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$ts = MakeTimeStamp($arUser[ self::NEXT_WRITEOF_DATE_CODE ], "DD.MM.YYYY HH:MI");
			if ($ts) {
				$date = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $ts);
			}
		}

		return $date;
	}

	/**
	 * Устанавливаем дату следующего списания абон платы
	 *
	 * @param $userId - id пользователя
	 *
	 * @return boolean
	 */
	public static function setNextWriteofDate($userId) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		\CTimeZone::Disable();

		$date   = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());
		$stmp   = MakeTimeStamp($date, "DD.MM.YYYY HH:MI:SS");
		$arrAdd = array(
			"DD"   => 1,
			"MM"   => 0,
			"YYYY" => 0,
			"HH"   => 0,
			"MI"   => 0,
			"SS"   => 0,
		);
		$stmp   = AddToTimeStamp($arrAdd, $stmp);

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::NEXT_WRITEOF_DATE_CODE => date("d.m.Y H:i:s", $stmp)));

		\CTimeZone::Enable();

		return true;
	}


	/**
	 * Подсчет суммы абонентской платы мастера.
	 * Сумма платежа выичислется как сумма стоимости каждой категории на которую подписан мастер
	 *
	 * @param $userId - id пользователя
	 *
	 * @return int
	 */
	public static function getUserFeeSumm($userId) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		try {
			Loader::includeModule('iblock');
		} catch (LoaderException $e) {
			$APPLICATION->ThrowException("Не подключен модуль Iblock");
		}

		$feeSumm = 0;

		// Получаем список видов деятельности на которые подписан профиль мастера
		$arProfileDirectionIds = [];
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, 'CREATED_BY' => $userId));
		if ($arProfile = $rsProfile->getNext()) {
			$dbPropsUserDirections = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'PROFILE_WORK')
			);
			while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
				$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
			}

			$arFilter       = array(
				"IBLOCK_ID"     => self::TASKS_IBLOCK_ID,
				"ID"            => $arProfileDirectionIds,
				"!UF_FEE_PRICE" => false // Платные категории,
			);
			$rsTaskSections = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "UF_FEE_PRICE"));

			while ($arTaskSections = $rsTaskSections->getNext()) {
				$feeSumm += $arTaskSections['UF_FEE_PRICE'];
			}
		}

		return $feeSumm;
	}

	/**
	 * Подсчет суммы абонентской платы мастера.
	 * Сумма платежа выичислется как сумма стоимости каждой категории на которую подписан мастер
	 *
	 * @param $userId - id пользователя
	 *
	 * @return int
	 */
	public static function getUserFeeSummNew($userId) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		try {
			Loader::includeModule('iblock');
		} catch (LoaderException $e) {
			$APPLICATION->ThrowException("Не подключен модуль Iblock");
		}

		$feeSumm = 0;

		// Получаем список видов деятельности на которые подписан профиль мастера
		$arProfileDirectionIds = [];
		$arProfileCity         = 0;
		$rsProfile             = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 2, 'CREATED_BY' => $userId));
		if ($arProfile = $rsProfile->getNext()) {

			//	Получаем направления деятельности профиля
			$dbPropsUserDirections = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'PROFILE_WORK')
			);
			while ($arPropsUserDirections = $dbPropsUserDirections->GetNext()) {
				$arProfileDirectionIds[] = $arPropsUserDirections['VALUE'];
			}

			// Получаем город профиля
			$dbPropsUserCity = CIBlockElement::GetProperty(
				$arProfile['IBLOCK_ID'],
				$arProfile['ID'],
				array(),
				array('CODE' => 'CITY')
			);
			while ($arPropsUserCity = $dbPropsUserCity->GetNext()) {
				$arProfileCity = $arPropsUserCity["VALUE"];
			}

			//	Стоимость по направлениям
//	$subQuery = \Bitrix\Sale\Internals\OrderPropsTable::query()->setSelect(['ID'])->whereIn('UF_CITIES', $arProfileCity);

			$entity_data_class = getHighloadEntity(3);
			$rsData            = $entity_data_class::getList(array(
				"select" => array("UF_DIRECTION", "UF_PRICE", "UF_CITIES"),
				"order"  => array("ID" => "ASC"),
				"filter" => array(
					"UF_DIRECTION" => $arProfileDirectionIds,
					"UF_CITIES"    => $arProfileCity,
				),
			));

			while ($arData = $rsData->Fetch()) {
				$feeSumm = $feeSumm + $arData["UF_PRICE"];
			}

		}

		return $feeSumm;
	}


	/**
	 * Списание дней подписок у мастеров
	 *
	 * @return bool
	 */
	public static function writeOffDay() {
		global $DB, $push_id;
		\CTimeZone::Disable();
		$filter = array(
			"<=" . self::NEXT_WRITEOF_DATE_CODE => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()),
			"!" . self::SUBSCRIPTION_DAYS_CODE  => false
		);

		$arUsersIds = [];
		$rsUsers    = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
		while ($arUser = $rsUsers->Fetch()) {

			// Уменьшаем количество дней
			self::decrementSubscriptionDays($arUser['ID']);

			// Устанавливаем даты списания
			self::setLastWriteofDate($arUser["ID"]);
			self::setNextWriteofDate($arUser["ID"]);

			$currentDaysCount = self::getUserSubscriptionDays($arUser['ID']);

			if ($currentDaysCount == 0) {
				// Отключаем демо период, если он включен и закончились дни
				if (self::isDemoPeriod($arUser['ID'])) {
					self::setDemoPeriod($arUser['ID'], 0);

					\CAppforsale::SendMessage(
						array($arUser['ID']),
						array(
							'body'         => 'Пробный период завершился. Пожалуйста выберите направления, которые Вы готовы выполнять',
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
						)
					);
				}

				\CAppforsale::SendMessage(
					array($arUser['ID']),
					array(
						'body'         => 'У Вас закончились дни подписки. Пожалуйста выберите необходимые направления и количество дней для продления',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
					)
				);
				continue;
			} elseif ($currentDaysCount == 1) {
				\CAppforsale::SendMessage(
					array($arUser['ID']),
					array(
						'body'         => 'У Вас остался последний день подписки. Не забудьте продлить, чтобы получать заявки по платным направлениям',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
					)
				);
			}

			$arUsersIds[] = $arUser["ID"];
		}

		//Уведомляем о списании дня
		if (count($arUsersIds) > 0) {
			foreach ($arUsersIds as $arUsersId) {
				$currentDaysCount = self::getUserSubscriptionDays($arUsersId);

				\CAppforsale::SendMessage(
					array($arUsersId),
					array(
						'body'         => 'У вас осталось ' . $currentDaysCount . ' дн. подписки',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
					)
				);
			}

		}
		\CTimeZone::Enable();

		return true;
	}

	// Списание дней для Агента
	function writeOffDayAgent() {
		self::writeOffDay();

		return 'writeOffDayAgent();';
	}

	/**
	 * Списание единовременной суммы за дни подписки
	 *
	 * @param int $userId
	 * @param int $days
	 *
	 * @return bool
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function pay($userId = 0, $days = 0) {
		global $USER;

		if ($userId <= 0) {
			throw new \Bitrix\Main\SystemException("Не заполнено поле \"Пользователь\"");
		}
		if ($days <= 0) {
			throw new \Bitrix\Main\SystemException("Не указано количество дней");
		}

		// Сумма абон платы
		$feeSumm = intval($days) * self::getUserFeeSumm($userId);

		//Скидки
		$arDiscounts = self::getSubscribeDicounts();
		if ( ! empty($arDiscounts)) {
			$percent = $arDiscounts[ $days ];
			$feeSumm -= round($feeSumm * ($percent / 100), 2);
		}

		try {
			\CAppforsaleUserAccount::SubscriptionPay($userId, $feeSumm);
		} catch (Exception $e) {
			throw new \Bitrix\Main\SystemException($e->getMessage());
		}

		return $feeSumm;
	}

	/**
	 * Метод возвращает массив процентов скидки за количество дней
	 */
	public static function getSubscribeDicounts() {
		$arDiscounts = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/include/subscribeDiscounts.json"), true);

		return $arDiscounts;
	}

	public static function addDemoPeriod($userId = 0, $days = 0) {
		global $APPLICATION;

		if ($userId <= 0) {
			throw new \Bitrix\Main\SystemException("Не заполнено поле \"Пользователь\"");
		}
		if ($days <= 0) {
			throw new \Bitrix\Main\SystemException("Не указано количество дней");
		}

		if ( ! self::hasSubscriptionDays($userId) && ! self::getLastWriteofDate($userId)) {
			if (self::addUserSubscriptionDays($userId, $days)) {

//				self::setDemoPeriod($userId, 1);
				CAppforsaleProfile::setFirstTask($userId);

				global $push_id;
				$push_id = 1;
				\CAppforsale::SendMessage(
					array((int) $userId),
					array(
						'body'         => 'Вам добавлено ' . $days . ' дн. пробной подписки',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/personal/subscription/'
					)
				);
				\CAppforsale::SendMessage(
					array((int) $userId),
					array(
						'body'         => 'Вам доступна одна бесплатная заявка',
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . 'dom-bez-zabot.online/youdo/'
					)
				);
			}


		}

		return true;
	}

	/**
	 * Добавляет запись лога подписки
	 *
	 * @param $userId
	 * @param $daysCount
	 * @param $feeSumm
	 * @param $directions
	 */
	public static function addLogRecord($userId, $daysCount, $feeSumm, $directions) {

		if ($userId <= 0) {
			throw new \Bitrix\Main\SystemException("Не заполнено поле \"Пользователь\"");
		}
		$entity_data_class = getHighloadEntity(2);

		// Массив полей для добавления
		$hlData = array(
			"UF_DATE"       => new \Bitrix\Main\Type\DateTime(),
			"UF_USER"       => $userId,
			"UF_DAYS"       => $daysCount,
			"UF_COST"       => $feeSumm,
			"UF_DIRECTIONS" => $directions,
		);
		$entity_data_class::add($hlData);
	}

}