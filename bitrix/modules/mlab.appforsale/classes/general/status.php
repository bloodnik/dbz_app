<?
IncludeModuleLangFile(__FILE__);

class CAppforsaleStatus
{
	function OnIBlockElementSetPropertyValues($element_id, $iblock_id, $property_values)
	{
		if (ToLower(CIBlock::GetArrayByID($iblock_id, "CODE")) == "task")
		{
			$arProperty = CIBlockProperty::GetPropertyArray("STATUS_ID", $iblock_id);
			if ($arProperty)
			{
				$bAccess = false;
				if (!empty($property_values['STATUS_ID']))
				{
					$bAccess = true;
					$list = array(array('VALUE' => $property_values['STATUS_ID']));
				}
				else
					$list = $property_values[$arProperty['ORIG_ID']];
				foreach ($list as $value)
				{
					if (strlen($value['VALUE']) == 0)
						continue;

					if ($value['VALUE'] == 'F')
					{
						$dbElement = CIBlockElement::GetByID($element_id);
						if ($obElement = $dbElement->GetNextElement())
						{
							$arFields = $obElement->GetFields();
							$arProps = $obElement->GetProperties();
							if ($arProps['STATUS_ID']['VALUE'] != 'F' || $bAccess)
							{

								$profile_id = intval($arProps['PROFILE_ID']['VALUE']);
								$price = intval($arProps['PRICE']['VALUE']);
								if ($profile_id > 0 && $price > 0)
								{
									global $USER_FIELD_MANAGER;
									$comission = $USER_FIELD_MANAGER->GetUserFieldValue("IBLOCK_".$iblock_id."_SECTION", "UF_COMISSION", $arFields['IBLOCK_SECTION_ID']);
									return CAppforsaleUserAccount::Pay($profile_id, ($price/100*$comission), 0, false);
								}
							}
						}
						return true;
					}
				}
			}	
		}
	}
}
?>