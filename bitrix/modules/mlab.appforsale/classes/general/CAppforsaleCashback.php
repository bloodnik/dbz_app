<?php

use Bitrix\Main\Loader;

class CAppforsaleCashback {

	const CASHBACK_PERCENT = 30;

	const DAY_OF_MONTH = "26";
	const TIME = "00:00";

	static function startCashbackCrediting() {

		if (self::isTimeCome()) {

			$arUsersAccountSumm = self::getUsersAccountSumm();


			$userIDs = [];
			foreach ($arUsersAccountSumm as $userId => $budget) {
				$userCashback = round($budget * self::CASHBACK_PERCENT / 100, 2);

				$message = "На Ваш бонусный баланс зачислен ежемесячный кэшбэк в размере 30% от суммы основного счёта на 23:59 МСК 25 числа текущего месяца";

				// Транзакция бонусов
				if ($userCashback > 0) {
					// Начисляем кэшбек
					$bonusTransact = new CAppforsaleTaskBonusTransact($userId, $userCashback, 1, $message);
					$bonusTransact->addTransact();

					$userIDs[] = $userId;

					// Отправляем уведомление
					global $push_id;
					$push_id = 1;
					\CAppforsale::SendMessage(
						array(intval($userId)),
						array(
							'body'         => $message,
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => 'http://dom-bez-zabot.online/youdo/personal/?ITEM=account'
						)
					);
				}
			}

			self::addLog($userIDs);
		}

		return "CAppforsaleCashback::startCashbackCrediting();";
	}

	static function getUsersAccountSumm() {
		$arUsersAccountSumm = [];

		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array("CURRENT_BUDGET" => "DESC"),
			array(">CURRENT_BUDGET" => 0),
//			array(">CURRENT_BUDGET" => 0, "USER_ID" => [37865, 27, 40027, 14730, 2383]),
			false,
			false,
			array("*")
		);
		while ($arUserAccount = $dbUserAccount->Fetch()) {
			$arUsersAccountSumm[ $arUserAccount["USER_ID"] ] = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		return $arUsersAccountSumm;
	}

	static function isTimeCome() {
		$day         = date("d");
		$currentTime = date("H:i");

		if ($day == self::DAY_OF_MONTH && self::TIME == $currentTime) {
			return true;
		}

		return false;
	}

	static function addLog($usersId) {
		$entity_data_class = getHighloadEntityByName("DBZCashbackCreditsLog");
		$hlData            = array(
			"UF_DATE"    => new \Bitrix\Main\Type\DateTime(),
			"UF_USERS"    => $usersId,
		);

		try {
			$result = $entity_data_class::add($hlData);

			return $result->isSuccess();
		} catch (Exception $e) {
			return false;
		}
	}


}