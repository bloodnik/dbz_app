<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 24.07.2020
 * Time: 19:32
 * Project: test.dombezzabot.info
 */

abstract class AAppforsaleTariff {


	/**
	 * @var CAppforsaleTariffOption
	 */
	private $option;

	/**
	 * Потрачено с основного баланса
	 * @var
	 */
	private $cost;


	/**
	 * @var int
	 */
	private $userId;

	const COMISSION_TARIFF_ID = 232;
	const PACKAGE_TARIFF_ID = 233;
	const UNLIM_TARIFF_ID = 234;

	public function __construct($userId) {
		$this->setUserId($userId);
	}


	public function pay() {

		$userId = $this->getUserId();
		// Сумма оплаты
		$paySumm = $this->getOption()->getCost();

		if ($this->isTariffActive() && $this->isUnused()) {
			$currentTariff       = $this->getUserTariff();
			$currentTariffOption = CAppforsaleTariffOption::getById($currentTariff["TARIFF_ID"]);
			$currentCost         = $currentTariffOption->getCost();
			$paySumm             = $paySumm - $currentCost;
		}

		try {
			if ($paySumm >= 0) {
				$this->setCost(\CAppforsaleUserAccount::TariffPay($userId, $paySumm, "Оплата тарифа: " . $this->getOption()->getName()));

				// Начисляем кешбек за оплату из основного баланса
				$tariffCost = $this->getOption()->getCost();

				$balancePercent = ($this->getCost() * 100) / $tariffCost;
				if ($balancePercent == 100) {
					$cashbackPercent = 0.15;
				} elseif ($balancePercent >= 50 && $balancePercent <= 99) {
					$cashbackPercent = 0.1;
				} else {
					$cashbackPercent = 0.05;
				}
				$cashbackSumm = $this->getCost() * $cashbackPercent;
				if ($cashbackSumm > 0) {
					$bonusTransact = new CAppforsaleTaskBonusTransact($userId, abs($cashbackSumm), 1, "Кэшбек за активацию тарифа");
					$bonusTransact->addTransact();
				}

			} else {
				// Транзакция бонусов
				$bonusTransact = new CAppforsaleTaskBonusTransact($userId, abs($paySumm), 1, "Коррекция баланса в счёт смены тарифа");
				$bonusTransact->addTransact();
			}
		} catch (Exception $e) {
			throw new \Bitrix\Main\SystemException($e->getMessage());
		}

		return $paySumm;
	}

	public function setUserTariff() {
		$userId = $this->getUserId();

		\CTimeZone::Disable();

		$entity_data_class = getHighloadEntityByName("DBZUserTariffs");
		$hlData            = array(
			"UF_CHANGE_DATE"   => new \Bitrix\Main\Type\DateTime(),
			"UF_ACTIVATE_DATE" => new \Bitrix\Main\Type\DateTime(),
			"UF_ACTIVE"        => 1,
			"UF_ACTIVE_TO"     => $this->getOption()->getActiveTo(),
			"UF_USER"          => $userId,
			"UF_TASK_COUNT"    => $this->getOption()->getTaskCount(),
			"UF_TARIFF_TYPE"   => $this->getOption()->getTariffType(),
			"UF_COST"          => $this->getCost()
		);

		$recordId = null;
		if ( ! $recordId = self::isExistUserTariff()) {
			$recordId = $entity_data_class::add($hlData);
		} else {
			$entity_data_class::update($recordId, $hlData);
		}

		global $USER;
		if ($this->getOption()->getCost() > 0) {
			$entity_data_class = getHighloadEntityByName("DBZTariffAcceptLog");
			$hlData            = array(
				"UF_DATE"         => new \Bitrix\Main\Type\DateTime(),
				"UF_USER"         => $userId,
				"UF_TARIFF"       => $this->getOption()->getTariffType(),
				"UF_TARIFF_NAME"  => $this->getOption()->getName(),
				"UF_COST"         => $this->getCost(),
				"UF_TARIFF_PRICE" => $this->getOption()->getCost()
			);
			$entity_data_class::add($hlData);

		}


		\CTimeZone::Enable();

		\CAppforsale::SendMessage(
			array((int) $userId),
			array(
				'body'         => "Тариф \"" . $this->getOption()->getName() . "\" успешно применен",
				'click_action' => 'appforsale.exec'
			),
			array(
				'url' => 'http://dom-bez-zabot.online/youdo/personal/subscription/'
			)
		);

		return true;
	}

	/**
	 * @param $userId
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function isExistUserTariff() {
		$entity_data_class = getHighloadEntityByName("DBZUserTariffs");

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array(
				"UF_USER" => $this->getUserId(),
			),
		));

		while ($arData = $rsData->Fetch()) {
			return $arData['ID'];
		}

		return false;
	}

	public function getUserTariff() {
		if ( ! $this->isTariffActive()) {
			$defaultTariff = CAppforsaleTariffOption::getById(1);

			return array(
				"TARIFF_NAME" => $defaultTariff->getName(),
				"UF_ACTIVE"   => false
			);
		}


		$entity_data_class  = getHighloadEntityByName("DBZUserTariffs");
		$entityTariffsClass = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select"  => array(
				"*",
				"TARIFF_TYPE"       => "TARIFF.UF_TYPE",
				"TARIFF_NAME"       => "TARIFF.UF_NAME",
				"TARIFF_ID"         => "TARIFF.ID",
				"TARIFF_COST_FROM"  => "TARIFF.UF_COST_FROM",
				"TARIFF_COST_TO"    => "TARIFF.UF_COST_TO",
				"TARIFF_PRICE"      => "TARIFF.UF_PRICE",
				"TARIFF_TASK_COUNT" => "TARIFF.UF_TASK_COUNT"
			),
			"filter"  => array(
				"UF_USER" => $this->getUserId(),
			),
			'runtime' => array(
				'TARIFF' => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					$entityTariffsClass,
					[    // СМОТРИ ДОКУ ПО ORM: JOIN
						'=this.UF_TARIFF_TYPE' => 'ref.ID',
					],
					['join_type' => 'INNER']
				),
			),
		));

		while ($arData = $rsData->Fetch()) {
			$arData["UF_ACTIVE"] = ! ! $arData["UF_ACTIVE"];

			return $arData;
		}

		return false;
	}

	public function isTariffActive() {
		$entity_data_class  = getHighloadEntityByName("DBZUserTariffs");
		$entityTariffsClass = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select"      => array("ID"),
			"filter"      => array(
				"UF_USER"   => $this->getUserId(),
				"UF_ACTIVE" => 1,
				array(
					'LOGIC'          => 'OR',
					'TARIFF.UF_TYPE' => self::COMISSION_TARIFF_ID,
					">UF_TASK_COUNT" => 0,
					'>UF_ACTIVE_TO'  => new \Bitrix\Main\Type\DateTime(),
				)
			),
			'runtime'     => array(
				'TARIFF' => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					$entityTariffsClass,
					[    // СМОТРИ ДОКУ ПО ORM: JOIN
						'=this.UF_TARIFF_TYPE' => 'ref.ID',
					],
					['join_type' => 'INNER']
				),
			),
			'count_total' => true,
		));

		return ! ! $rsData->getCount();
	}

	/**
	 * Тариф еще не использован
	 *
	 * @return bool
	 */
	function isUnused() {
		CTimeZone::Disable();

		$currentTarriff = $this->getUserTariff();

		if ( ! isset($currentTarriff["TARIFF_TYPE"]) || $currentTarriff["TARIFF_TYPE"] == self::COMISSION_TARIFF_ID) {
			return true;
		}

		// Сравниваем количество использованных заявок
		$tariffOption = CAppforsaleTariffOption::getById($currentTarriff["TARIFF_ID"]);
		if ($tariffOption->getTaskCount() != $currentTarriff["UF_TASK_COUNT"]) {
			return false;
		}


		CTimeZone::Enable();

		return true;
	}

	public function isUnlimTariff() {
		$tariff = $this->getUserTariff();

		return $tariff["TARIFF_TYPE"] == self::UNLIM_TARIFF_ID;
	}

	public function isComisssionTariff() {
		$tariff = $this->getUserTariff();

		return $tariff["TARIFF_TYPE"] == self::COMISSION_TARIFF_ID || ! $this->isTariffActive();
	}

	public function isPackageTariff() {
		$tariff = $this->getUserTariff();

		return $tariff["TARIFF_TYPE"] == self::PACKAGE_TARIFF_ID;
	}

	public function decrementTaskCount() {
		$userId = $this->getUserId();

		$entity_data_class = getHighloadEntityByName("DBZUserTariffs");
		$rsData            = $entity_data_class::getList(array(
			"select" => array("ID", "UF_TASK_COUNT"),
			"filter" => array(
				"UF_USER" => $userId,
			),
		));


		while ($arData = $rsData->Fetch()) {
			if ($arData["UF_TASK_COUNT"] > 0) {

				$newTaskCount = intval($arData["UF_TASK_COUNT"] - 1);
				$hlData       = array(
					"UF_CHANGE_DATE" => new \Bitrix\Main\Type\DateTime(),
					"UF_TASK_COUNT"  => $newTaskCount,
					"UF_ACTIVE"      => $newTaskCount > 0 ? 1 : false
				);
				$entity_data_class::update($arData["ID"], $hlData);

				if ($newTaskCount == 0) {
					\CAppforsale::SendMessage(
						array((int) $this->getUserId()),
						array(
							'body'         => "Оплаченные тарифом заявки закончились. Пожалуйста, выберите новый тариф",
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => 'http://dom-bez-zabot.online/youdo/personal/subscription/'
						)
					);
				}

				return true;
			}
		}

		return false;
	}

	/**
	 * Деактивация по дате активности. Вызывается из Агентов
	 * @return array|bool|false|mixed
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function deactivateByAgent() {
		$entity_data_class = getHighloadEntityByName("DBZUserTariffs");

		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"filter" => array(
				"UF_ACTIVE" => 1,
				array(
					'LOGIC'         => 'AND',
					'>UF_ACTIVE_TO' => '01.01.2020 09:00:00',
					'<UF_ACTIVE_TO' => new \Bitrix\Main\Type\DateTime(),
				)
			)
		));

		while ($arData = $rsData->Fetch()) {

			$arData["UF_ACTIVE"]      = false;
			$arData["UF_ACTIVE_TO"]   = false;
			$arData["UF_CHANGE_DATE"] = new \Bitrix\Main\Type\DateTime();
			$userId                   = $arData["UF_USER"];
			$entity_data_class::update($arData["ID"], $arData);

			\CAppforsale::SendMessage(
				array((int) $userId),
				array(
					'body'         => "Срок действия выбраннго тарифа истек. Пожалуйста выберите новый тариф",
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => 'http://dom-bez-zabot.online/youdo/personal/subscription/'
				)
			);
		}

		return "CAppforsaleTariff::deactivateByAgent();";
	}

	/**
	 * @param $cityId - идентификатор города
	 *
	 * Возвращает массив ID пользователей активного безлимитного тарифа
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getUnlimUsersByCity($cityId) {
		if (empty($cityId)) {
			return array();
		}

		$entity_data_class  = getHighloadEntityByName("DBZUserTariffs");
		$entityTariffsClass = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select"  => array("UF_USER"),
			"filter"  => array(
				"UF_ACTIVE"      => 1,
				"TARIFF.UF_TYPE" => self::UNLIM_TARIFF_ID,
				"USER.UF_CITY"   => $cityId
			),
			'runtime' => array(
				'TARIFF' => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					$entityTariffsClass,
					[
						'=this.UF_TARIFF_TYPE' => 'ref.ID',
					],
					['join_type' => 'INNER']
				),
				'USER'   => new \Bitrix\Main\Entity\ReferenceField(
					'ELEMENT',
					\Bitrix\Main\UserTable::class,
					[
						'=this.UF_USER' => 'ref.ID',
					],
					['join_type' => 'INNER']
				),
			),
		));

		$userIds = [];
		while ($arData = $rsData->Fetch()) {
			$userIds[] = $arData["UF_USER"];

		}

		return $userIds;
	}

	/**
	 * @return CAppforsaleTariffOption
	 */
	public function getOption() {
		return $this->option;
	}

	/**
	 * @param CAppforsaleTariffOption $option
	 */
	public function setOption($option) {
		$this->option = $option;
	}


	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @param mixed $userId
	 */
	public function setUserId($userId) {
		$this->userId = $userId;
	}

	/**
	 * @return mixed
	 */
	public function getCost() {
		return $this->cost;
	}

	/**
	 * @param mixed $cost
	 */
	public function setCost($cost) {
		$this->cost = $cost;
	}


}