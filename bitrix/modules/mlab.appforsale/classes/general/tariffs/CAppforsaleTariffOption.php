<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 24.07.2020
 * Time: 20:46
 * Project: test.dombezzabot.info
 */

class CAppforsaleTariffOption {
	private $taskCount;
	private $tariffType;
	private $cost;
	private $name;
	private $activeTo;

	public static $tariffNames = array(
		"232" => "Комиссия",
		"233" => "Пакеты заявок со скидкой",
		"234" => "Безлимит",
	);

	public function __construct($tariffType, $name, $taskCount, $cost, $activeTo) {
		$this->setName($name);
		$this->setTaskCount($taskCount);
		$this->setCost($cost);
		$this->setActiveTo($activeTo);
		$this->setTariffType($tariffType);
	}

	public static function getById($tariffId) {
		global $DB;

		$entity_data_class = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"filter" => array("ID" => $tariffId)
		));

		while ($arData = $rsData->Fetch()) {

			$activeTo = false;
			if ( ! empty($arData["UF_ACTIVE_DAYS"])) {
				\CTimeZone::Disable();

				$date     = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());
				$stmp     = MakeTimeStamp($date, "DD.MM.YYYY HH:MI:SS");
				$arrAdd   = array(
					"DD"   => $arData["UF_ACTIVE_DAYS"],
					"MM"   => 0,
					"YYYY" => 0,
					"HH"   => 0,
					"MI"   => 0,
					"SS"   => 0,
				);
				$stmp     = AddToTimeStamp($arrAdd, $stmp);
				$activeTo = date("d.m.Y H:i:s", $stmp);

				\CTimeZone::Enable();
			}

			return new CAppforsaleTariffOption($arData["ID"], $arData["UF_NAME"], $arData["UF_TASK_COUNT"], $arData["UF_PRICE"], $activeTo);
		}

		return false;
	}

	public static function getAvailableOptions() {

		$entity_data_class = getHighloadEntityByName("DBZTariffs");

		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"order" => array("UF_SORT" => "ASC"),

		));

		$arResult = [];
		while ($arData = $rsData->Fetch()) {
			$arData["TARIFF_NAME"] = self::$tariffNames[ $arData["UF_TYPE"] ];
			$arData["UF_DESCRIPTION"] = htmlspecialchars($arData["UF_DESCRIPTION"]);
			$arResult[] = $arData;
		}

		return $arResult;
	}


	/**
	 * @return mixed
	 */
	public function getTaskCount() {
		return $this->taskCount;
	}

	/**
	 * @param mixed $taskCount
	 */
	public function setTaskCount($taskCount) {
		$this->taskCount = $taskCount;
	}

	/**
	 * @return mixed
	 */
	public function getCost() {
		return $this->cost;
	}

	/**
	 * @param mixed $cost
	 */
	public function setCost($cost) {
		$this->cost = $cost;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getActiveTo() {
		return $this->activeTo;
	}

	/**
	 * @param mixed $activeTo
	 */
	public function setActiveTo($activeTo) {
		$this->activeTo = $activeTo;
	}

	/**
	 * @return mixed
	 */
	public function getTariffType() {
		return $this->tariffType;
	}

	/**
	 * @param mixed $tariffType
	 */
	public function setTariffType($tariffType) {
		$this->tariffType = $tariffType;
	}


}