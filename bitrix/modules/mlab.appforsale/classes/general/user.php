<?
IncludeModuleLangFile(__FILE__);

define("APPFORSALE_TIME_LOCK_USER", 600);
$GLOBALS["APPFORSALE_USER_ACCOUNT"] = array();

class CAllAppforsaleUserAccount {

	function CheckFields($ACTION, &$arFields, $ID = 0) {
		if ((is_set($arFields, "USER_ID") || $ACTION == "ADD") && intval($arFields["USER_ID"]) <= 0) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("SKGU_EMPTY_USER_ID"), "EMPTY_USER_ID");

			return false;
		}

		if (is_set($arFields, "CURRENT_BUDGET") || $ACTION == "ADD") {
			$arFields["CURRENT_BUDGET"] = str_replace(",", ".", $arFields["CURRENT_BUDGET"]);
			$arFields["CURRENT_BUDGET"] = doubleval($arFields["CURRENT_BUDGET"]);
		}

		if ((is_set($arFields, "LOCKED") || $ACTION == "ADD") && $arFields["LOCKED"] != "Y") {
			$arFields["LOCKED"] = "N";
		}

		if (is_set($arFields, "USER_ID")) {
			$dbUser = CUser::GetByID($arFields["USER_ID"]);
			if ( ! $dbUser->Fetch()) {
				$GLOBALS["APPLICATION"]->ThrowException(str_replace("#ID#", $arFields["USER_ID"], GetMessage("SKGU_NO_USER")), "ERROR_NO_USER_ID");

				return false;
			}
		}

		return true;
	}

	function Delete($ID) {
		global $DB;

		$ID = (int) $ID;
		if ($ID <= 0) {
			return false;
		}

		$arOldUserAccount = CAppforsaleUserAccount::GetByID($ID);

		$dbTrans = CAppforsaleUserTransact::GetList(
			array(),
			array("USER_ID" => $arOldUserAccount["USER_ID"]),
			false,
			false,
			array("ID", "USER_ID")
		);
		while ($arTrans = $dbTrans->Fetch()) {
			CAppforsaleUserTransact::Delete($arTrans["ID"]);
		}

		unset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ]);
		unset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE1_" . $arOldUserAccount["USER_ID"] . "_" ]);

		$res = $DB->Query("DELETE FROM appforsale_user_account WHERE ID = " . $ID . " ", true);

		return $res;
	}


	function Lock($userID) {
		global $DB, $APPLICATION;

		$userID = (int) $userID;
		if ($userID <= 0) {
			return false;
		}

		CTimeZone::Disable();
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			),
			false,
			false,
			array(
				"ID",
				"LOCKED",
				"DATE_LOCKED"
			)
		);
		CTimeZone::Enable();
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$dateLocked = 0;
			if ($arUserAccount["LOCKED"] == "Y") {
				if ( ! ($dateLocked = MakeTimeStamp($arUserAccount["DATE_LOCKED"], CSite::GetDateFormat("FULL", SITE_ID)))) {
					$dateLocked = mktime(0, 0, 0, 1, 1, 1990);
				}
			}

			if (defined("APPFORSALE_TIME_LOCK_USER") && intval(SALE_TIME_LOCK_USER) > 0) {
				$timeLockUser = intval(SALE_TIME_LOCK_USER);
			} else {
				$timeLockUser = 10 * 60;
			}

			if (($arUserAccount["LOCKED"] != "Y")
			    || (($arUserAccount["LOCKED"] == "Y") && ((time() - $dateLocked) > $timeLockUser))) {
				$arFields = array(
					"LOCKED"       => "Y",
					"=DATE_LOCKED" => $DB->GetNowFunction()
				);
				if (CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields)) {
					return true;
				} else {
					return false;
				}
			} else {
				$APPLICATION->ThrowException(GetMessage("SKGU_ACCOUNT_LOCKED"), "ACCOUNT_LOCKED");

				return false;
			}
		} else {
			$arFields = array(
				"USER_ID"        => $userID,
				"CURRENT_BUDGET" => 0.0,
				"LOCKED"         => "Y",
				"=DATE_LOCKED"   => $DB->GetNowFunction()
			);
			if (CAppforsaleUserAccount::Add($arFields)) {
				return true;
			} else {
				return false;
			}
		}
	}

	function UnLock($userID) {
		$userID = (int) $userID;
		if ($userID <= 0) {
			return false;
		}

		CTimeZone::Disable();
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			),
			false,
			false,
			array(
				"ID",
				"LOCKED",
				"DATE_LOCKED"
			)
		);
		CTimeZone::Enable();
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			if ($arUserAccount["LOCKED"] == "Y") {
				$arFields = array(
					"LOCKED"      => "N",
					"DATE_LOCKED" => false
				);
				if (CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields)) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			$arFields = array(
				"USER_ID"        => $userID,
				"CURRENT_BUDGET" => 0.0,
				"LOCKED"         => "N",
				"DATE_LOCKED"    => false
			);
			if (CAppforsaleUserAccount::Add($arFields)) {
				return true;
			} else {
				return false;
			}
		}
	}

	function Pay($userID, $paySum, $orderID = 0, $useCC = true, $paymentId = null) {
		global $DB, $APPLICATION, $USER;

		$errorCode = "";

		$userID = (int) $userID;
		if ($userID <= 0) {
			$APPLICATION->ThrowException(GetMessage("SKGU_EMPTY_USER_ID"), "EMPTY_USER_ID");

			return false;
		}

		$paySum = str_replace(",", ".", $paySum);
		$paySum = (float) $paySum;

		// Уменшаем сумму списания баланса на сумму бонусов
		$bonus     = new CAppforsaleTaskBonusAccount($userID);
		$bonusSumm = $bonus->calcWriteOfSumm($paySum);
		$paySum    = $paySum - $bonusSumm;


		$orderID   = (int) $orderID;
		$paymentId = (int) $paymentId;

		$useCC = ($useCC ? true : false);

		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			$APPLICATION->ThrowException(GetMessage("SKGU_ERROR_LOCK"), "ACCOUNT_NOT_LOCKED");

			return false;
		}

		$currentBudget = 0.0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		$withdrawSum = 0;
//   		if (($currentBudget < $paySum) && $useCC)
//   		{
//   			$payOverdraft = $paySum - $currentBudget;

//   			$bPayed = false;
// 		}

		if ($withdrawSum + $currentBudget >= $paySum || ! $useCC) {
			if ($arUserAccount) {
				$arFields = array(
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);
			} else {
				$arFields = array(
					"USER_ID"        => $userID,
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Add($arFields);
			}

			$tariff     = new CAppforsaleTariff($USER->GetID());
			$userTariff = $tariff->getUserTariff();

			$note = "Оплата заказа № " . $orderID . " по тарифу: Стандарт, при примененном тарифе " . $userTariff["TARIFF_NAME"] . ".";
			if($bonusSumm > 0) {
				$note .= " Оплата бонусами - " . $bonusSumm;
			}

			$arFields = array(
				"USER_ID"       => $userID,
				"TRANSACT_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
				"AMOUNT"        => $paySum,
				"DEBIT"         => "N",
				"ORDER_ID"      => ($orderID > 0 ? $orderID : false),
				"PAYMENT_ID"    => ($paymentId > 0 ? $paymentId : false),
				"DESCRIPTION"   => $note,
				"EMPLOYEE_ID"   => ($USER->IsAuthorized() ? $USER->GetID() : false)
			);
			CTimeZone::Disable();
			CAppforsaleUserTransact::Add($arFields);

			// Транзакция бонусов
			if ($bonusSumm > 0) {
				$bonusTransact = new CAppforsaleTaskBonusTransact($userID, $bonusSumm, 0, "Оплата заказа № " . $orderID);
				$bonusTransact->addTransact();
			}
			CTimeZone::Enable();

			CAppforsaleUserAccount::UnLock($userID);



			return true;
		}

		CAppforsaleUserAccount::UnLock($userID);
		$APPLICATION->ThrowException(GetMessage("SKGU_NO_ENOUGH"), "CANT_PAY");

		return false;
	}

	// Оплата подписки
	function SubscriptionPay($userID, $paySum) {
		global $DB, $APPLICATION, $USER;

		$userID = (int) $userID;
		if ($userID <= 0) {
			throw new \Bitrix\Main\SystemException("Пользователь не определен");
		}

		$paySum = str_replace(",", ".", $paySum);
		$paySum = (float) $paySum;


		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			throw new \Bitrix\Main\SystemException("Счет не может быть заблокирован");
		}

		$currentBudget = 0.0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		$withdrawSum = 0;

		if ($withdrawSum + $currentBudget >= $paySum) {
			if ($arUserAccount) {
				$arFields = array(
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);
			} else {
				$arFields = array(
					"USER_ID"        => $userID,
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Add($arFields);
			}

			$arFields = array(
				"USER_ID"       => $userID,
				"TRANSACT_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
				"AMOUNT"        => $paySum,
				"DEBIT"         => "N",
				"DESCRIPTION"   => "SUBSCRIPTION_PAY",
				"NOTES"         => "Списание абонентской платы",
				"EMPLOYEE_ID"   => ($USER->IsAuthorized() ? $USER->GetID() : false)
			);
			CTimeZone::Disable();
			CAppforsaleUserTransact::Add($arFields);
			CTimeZone::Enable();

			CAppforsaleUserAccount::UnLock($userID);

			return true;
		}
		CAppforsaleUserAccount::UnLock($userID);
		throw new \Bitrix\Main\SystemException("На счете недостаточно средств для проведения оплаты");

	}


	// Оплата процентов по завершении заявки
	function OrderPay($userID, $paySum, $orderID, $note = "") {
		global $DB, $APPLICATION, $USER;

		if (empty($note)) {
			$note = "Автоматическое списание комиссии за завершение заявки №" . $orderID;
		}

		$userID = (int) $userID;
		if ($userID <= 0) {
			throw new \Bitrix\Main\SystemException("Пользователь не определен");
		}

		$paySum = str_replace(",", ".", $paySum);
		$paySum = (float) $paySum;

		// Уменшаем сумму списания баланса на сумму бонусов
		$bonus     = new CAppforsaleTaskBonusAccount($userID);
		$bonusSumm = $bonus->calcWriteOfSumm($paySum);
		$paySum    = (float) $paySum - $bonusSumm;

		if($bonusSumm > 0) {
			$note .= ". Оплата бонусами - " . $bonusSumm;
		}


		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			throw new \Bitrix\Main\SystemException("Счет не может быть заблокирован");
		}

		$currentBudget = 0.0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		$withdrawSum = 0;

		if ($arUserAccount) {
			$arFields = array(
				"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
			);
			CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);
		} else {
			$arFields = array(
				"USER_ID"        => $userID,
				"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
			);
			CAppforsaleUserAccount::Add($arFields);
		}

		$arFields = array(
			"USER_ID"       => $userID,
			"TRANSACT_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
			"AMOUNT"        => $paySum,
			"DEBIT"         => "N",
			"DESCRIPTION"   => "COMISSION_PAY",
			"NOTES"         => $note,
			"ORDER_ID"      => ($orderID > 0 ? $orderID : false)
		);
		CTimeZone::Disable();
		CAppforsaleUserTransact::Add($arFields);

		// Транзакция бонусов
		if ($bonusSumm > 0) {
			$bonusTransact = new CAppforsaleTaskBonusTransact($userID, $bonusSumm, 0, $note);
			$bonusTransact->addTransact();
		}
		CTimeZone::Enable();

		CAppforsaleUserAccount::UnLock($userID);

		return true;
	}

	// Оплата тарифа
	public static function TariffPay($userID, $paySum, $note = "") {
		global $DB;

		if (empty($note)) {
			$note = "Оплата тарифа";
		}

		$userID = (int) $userID;
		if ($userID <= 0) {
			throw new \Bitrix\Main\SystemException("Пользователь не определен");
		}

		$paySum = str_replace(",", ".", $paySum);
		$paySum = (float) $paySum;


		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			throw new \Bitrix\Main\SystemException("Счет не может быть заблокирован");
		}

		$currentBudget = 0.0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		$withdrawSum = 0;


		// Уменшаем сумму списания баланса на сумму бонусов
		$bonus     = new CAppforsaleTaskBonusAccount($userID);
		$bonusSumm = $bonus->calcWriteOfSumm($paySum);
		$paySum    = $paySum - $bonusSumm;

		if($bonusSumm > 0) {
			$note .= ". Оплата бонусами - " . $bonusSumm;
		}

		if ($withdrawSum + $currentBudget >= $paySum) {

			if ($arUserAccount) {
				$arFields = array(
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);
			} else {
				$arFields = array(
					"USER_ID"        => $userID,
					"CURRENT_BUDGET" => ($withdrawSum + $currentBudget - $paySum)
				);
				CAppforsaleUserAccount::Add($arFields);
			}

			$arFields = array(
				"USER_ID"       => $userID,
				"TRANSACT_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
				"AMOUNT"        => $paySum,
				"DEBIT"         => "N",
				"DESCRIPTION"   => "TARIFF_PAY",
				"NOTES"         => $note
			);
			CTimeZone::Disable();
			CAppforsaleUserTransact::Add($arFields);

			// Транзакция бонусов
			if ($bonusSumm > 0) {
				$bonusTransact = new CAppforsaleTaskBonusTransact($userID, $bonusSumm, 0, $note);
				$bonusTransact->addTransact();
			}
			CTimeZone::Enable();


			CAppforsaleUserAccount::UnLock($userID);

			return $paySum;
		}
		CAppforsaleUserAccount::UnLock($userID);

		$needSumm = $paySum - ($withdrawSum + $currentBudget);
		throw new \Bitrix\Main\SystemException("На счете недостаточно средств для проведения оплаты. Необходимо пополинить счет на " . $needSumm . " руб.");
	}

	function Withdraw($userID, $paySum, $orderID = 0) {
		global $DB, $APPLICATION, $USER;

		$errorCode = "";

		$userID = (int) $userID;
		if ($userID <= 0) {
			$APPLICATION->ThrowException(GetMessage("SKGU_EMPTYID"), "EMPTY_USER_ID");

			return false;
		}

		$paySum = str_replace(",", ".", $paySum);
		$paySum = (float) $paySum;
		if ($paySum <= 0) {
			$APPLICATION->ThrowException(GetMessage("SKGU_EMPTY_SUM"), "EMPTY_SUM");

			return false;
		}

		$orderID = (int) $orderID;

		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			$APPLICATION->ThrowException(GetMessage("SKGU_ACCOUNT_NOT_LOCKED"), "ACCOUNT_NOT_LOCKED");

			return false;
		}

		$currentBudget = 0.0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = (float) $arUserAccount["CURRENT_BUDGET"];

			if ($currentBudget > 0) {
				$withdrawSum = $paySum;
				if ($withdrawSum > $currentBudget) {
					$withdrawSum = $currentBudget;
				}

				$arFields = array(
					"CURRENT_BUDGET" => ($currentBudget - $withdrawSum)
				);
				CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);

				$arFields = array(
					"USER_ID"       => $userID,
					"TRANSACT_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
					"AMOUNT"        => $withdrawSum,
					"DEBIT"         => "N",
					"ORDER_ID"      => ($orderID > 0 ? $orderID : false),
					"DESCRIPTION"   => "ORDER_PAY",
					"EMPLOYEE_ID"   => ($USER->IsAuthorized() ? $USER->GetID() : false)
				);
				CTimeZone::Disable();
				CAppforsaleUserTransact::Add($arFields);
				CTimeZone::Enable();

				CAppforsaleUserAccount::UnLock($userID);

				return $withdrawSum;
			}
		}

		CAppforsaleUserAccount::UnLock($userID);

		return false;
	}

	function UpdateAccount($userID, $sum, $description = "", $orderID = 0, $notes = "", $paymentId = null) {
		global $DB, $APPLICATION, $USER;

		$userID = (int) $userID;
		if ($userID <= 0) {
			$APPLICATION->ThrowException(GetMessage("SKGU_EMPTYID"), "EMPTY_USER_ID");

			return false;
		}
		$dbUser = CUser::GetByID($userID);
		if ( ! $dbUser->Fetch()) {
			$APPLICATION->ThrowException(str_replace("#ID#", $userID, GetMessage("SKGU_NO_USER")), "ERROR_NO_USER_ID");

			return false;
		}

		$sum = (float) str_replace(",", ".", $sum);


		$orderID   = (int) $orderID;
		$paymentId = (int) $paymentId;
		if ( ! CAppforsaleUserAccount::Lock($userID)) {
			$APPLICATION->ThrowException(GetMessage("SKGU_ACCOUNT_NOT_WORK"), "ACCOUNT_NOT_LOCKED");

			return false;
		}

		$currentBudget = 0.0000;

		$result = false;

		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array("USER_ID" => $userID)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = floatval($arUserAccount["CURRENT_BUDGET"]);
			$arFields      = array(
				"CURRENT_BUDGET" => $arUserAccount["CURRENT_BUDGET"] + $sum
			);
			$result        = CAppforsaleUserAccount::Update($arUserAccount["ID"], $arFields);
		} else {
			$currentBudget = floatval($sum);
			$arFields      = array(
				"USER_ID"        => $userID,
				"CURRENT_BUDGET" => $sum,
				"CURRENCY"       => $currency,
				"LOCKED"         => "Y",
				"=DATE_LOCKED"   => $DB->GetNowFunction()
			);
			$result        = CAppforsaleUserAccount::Add($arFields);
		}

		if ($result) {
			if (isset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ])) {
				unset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ]);
			}

			$arFields = array(
				"USER_ID"        => $userID,
				"TRANSACT_DATE"  => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID))),
				"CURRENT_BUDGET" => $currentBudget,
				"AMOUNT"         => ($sum > 0 ? $sum : -$sum),
				"DEBIT"          => ($sum > 0 ? "Y" : "N"),
				"ORDER_ID"       => ($orderID > 0 ? $orderID : false),
				"PAYMENT_ID"     => ($paymentId > 0 ? $paymentId : false),
				"DESCRIPTION"    => ((strlen($description) > 0) ? $description : null),
				"NOTES"          => ((strlen($notes) > 0) ? $notes : false),
				"EMPLOYEE_ID"    => ($USER->IsAuthorized() ? $USER->GetID() : false)
			);
			CTimeZone::Disable();
			CAppforsaleUserTransact::Add($arFields);
			CTimeZone::Enable();
		}

		CAppforsaleUserAccount::UnLock($userID);

		if ($result) {
			global $push_id;
			$push_id = 6;
			CAppforsale::SendMessage(
				array($userID),
				array(
					'body'         => GetMessage('NOTIFICATION_PAY_BODY', array('#SUM#' => AppforsaleFormatCurrency($sum), '#BUDGET#' => AppforsaleFormatCurrency(($currentBudget + $sum)))),
					'icon'         => 'ic_stat_name',
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => 'http://' . $_SERVER['SERVER_NAME'] . '/youdo/personal/?ITEM=account'
				)
			);
		}

		return $result;
	}

	public static function getCurrentUserBudget($userId) {
		global $APPLICATION;

		$userID = (int) $userId;
		if ($userID <= 0) {
			$APPLICATION->ThrowException(GetMessage("SKGU_EMPTY_USER_ID"), "EMPTY_USER_ID");

			return false;
		}

		$currentBudget = 0;

		// Check current user account budget
		$dbUserAccount = CAppforsaleUserAccount::GetList(
			array(),
			array(
				"USER_ID" => $userID
			)
		);
		if ($arUserAccount = $dbUserAccount->Fetch()) {
			$currentBudget = roundEx((float) $arUserAccount["CURRENT_BUDGET"], APPFORSALE_VALUE_PRECISION);
		}

		return $currentBudget;

	}


}

?>