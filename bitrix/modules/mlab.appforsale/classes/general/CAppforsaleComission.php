<?php

use Bitrix\Main\Loader;

class CAppforsaleComission {

	// USER FIELDS
	const WAIT_COMISSION = 'WAIT_COMISSION';
	const USER_TOTAL_WORKS_SUMM_FIELD = 'UF_TOTAL_WORKS_SUMM';

	// IBLOCKS
	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	const B24_CLOSE_STAGE = '18';

	// Граница стоимости
	const COST_BORDER = 10000;

	// Максимальная сумма списания
	const MAX_COMISSION_SUMM = 2000;

	//	Максимальная сумма первого списания
	const MAX_FIRST_COMISSION_SUM = 500;

	/**
	 * Списание первой части комиссии
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function writeOfFirstComission() {
		global $DB;
		Loader::includeModule("iblock");

		\CTimeZone::Disable();

		$time     = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()); // получаем текущую дату в формате текущего сайта
		$stmp     = MakeTimeStamp($time, "DD.MM.YYYY HH:MI:SS"); //Конвертируем время из строки в Unix-формат.
		$stmpFrom = AddToTimeStamp(array("DD" => -1), $stmp);
		$stmpTo   = AddToTimeStamp(array("HH" => -1), $stmp);
		$dateFrom = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $stmpFrom);
		$dateTo   = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $stmpTo);

		$arSelect = array("*", "PROPERTY_WORK_COST", "PROPERTY_STATUS_ID", "PROPERTY_COST", "PROPERTY_PROFILE_ID");
		$arFilter = array(
			"IBLOCK_ID"                => self::TASKS_IBLOCK_ID,
			"ACTIVE"                   => "Y",
			"DATE_MODIFY_FROM"         => $dateFrom,
			"DATE_MODIFY_TO"           => $dateTo,
			"PROPERTY_STATUS_ID"       => "P",
			"!PROPERTY_WAIT_COMISSION" => false,
			"PROPERTY_HAS_PROBLEM"     => false,
			"PROPERTY_NO_COMISSION"    => false
		);

		$res = CIBlockElement::GetList(array("timestamp_x" => "asc"), $arFilter, false, array("nPageSize" => 20), $arSelect);
		$res->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');

		self::writeOff($res);

		\CTimeZone::Enable();
	}

	/**
	 * @param  $res
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function writeOff($res) {
		Loader::includeModule("iblock");

		while ($arTask = $res->GetNextElement()) {
			$arFields = $arTask->GetFields();

			$totalComissionSumm = self::getTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"]);
			$balance            = self::MAX_FIRST_COMISSION_SUM - $totalComissionSumm; // Остаток комиссии

			$workCost     = $arFields["PROPERTY_WORK_COST_VALUE"];
			$cost         = $arFields["PROPERTY_COST_VALUE"];
			$withdrawSumm = ($workCost * 0.1) - $cost;
			$withdrawSumm = $withdrawSumm + $totalComissionSumm <= self::MAX_FIRST_COMISSION_SUM ? $withdrawSumm : $balance;

			if (intval($withdrawSumm) > 0 && $arFields["PROPERTY_PROFILE_ID_VALUE"] > 0) {

				if ($arFields["PROPERTY_WORK_COST_VALUE"] <= self::COST_BORDER) {
					$push_message = 'Списание остатка от 10% комиссии по заявке ' . $arFields['ID'] . ' в размере ' . $withdrawSumm . ' рублей. Если заявка не актуальна обратитесь за возвратом, возврат осуществляется в течение трёх рабочих дней';
				} else {
					$push_message = 'Списание первой части остатка от 10% комиссии по заявке ' . $arFields['ID'] . ' в размере ' . $withdrawSumm . ' рублей. Полное списание по заявке будет произведено через 14 дней. Если заявка не актуальна обратитесь за возвратом, возврат осуществляется в течение трёх рабочих дней';
				}

				// Снимаем комиссию у мастера в размере (Стоимость работы * 0,1) - Стоимость принятия заявки
				if ($withdrawSumm > 0) {
					\CAppforsaleUserAccount::OrderPay($arFields["PROPERTY_PROFILE_ID_VALUE"], $withdrawSumm, $arFields["ID"], $push_message);
				}

				// Снятие признака "Требуется списание"
				CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('WAIT_COMISSION' => false));

				// Записываем информацию о сумме комиссии в лог
				CAppforsaleComission::addTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"], $withdrawSumm);

				sleep(3); // задержка в 3 секунды, чтобы не угодить в блокировку Б24
				changeDealStage($arFields['ID'], self::B24_CLOSE_STAGE);

				// Пуш
				if ($withdrawSumm > 0) {
					global $push_id;
					$push_id = 1;
					\CAppforsale::SendMessage(
						array((int) $arFields["PROPERTY_PROFILE_ID_VALUE"]),
						array(
							'body'         => $push_message,
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => 'http://dom-bez-zabot.online/youdo/tasks-executor/' . $arFields['DETAIL_PAGE_URL'],
							'id'  => $arFields['ID']
						)
					);
				}
			} else {
				// Снятие признака "Требуется списание"
				CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('WAIT_COMISSION' => false));
			}
		}
	}


	/**
	 * Списание остатка комисии и закрытие заявки
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function writeOfLastComissionAndCloseTask($B24_CLOSE_STAGE = '19') {
		global $DB;
		$TASKS_IBLOCK_ID = 4;
		$B24_CLOSE_STAGE = '19';
		\Bitrix\Main\Loader::includeModule("iblock");

		// Вычисляем дату -14 дней от текущей даты
		$time             = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time()); // получаем текущую дату в формате текущего сайта
		$stmp             = MakeTimeStamp($time, "DD.MM.YYYY"); //Конвертируем время из строки в Unix-формат.
		$stmp             = AddToTimeStamp(array("DD" => -14), $stmp); //Добавляет к дате в Unix-формате заданный интервал времени. Возвращает новую дату также в Unix-формате.
		$lastModifiedDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), $stmp);

		$arSelect = array("*", "PROPERTY_WORK_COST", "PROPERTY_STATUS_ID", "PROPERTY_COST", "PROPERTY_PROFILE_ID", "PROPERTY_NO_COMISSION");
		$arFilter = array(
			"IBLOCK_ID"            => $TASKS_IBLOCK_ID,
			"ACTIVE"               => "Y",
			"DATE_MODIFY_FROM"     => "01.01.2017",
			"DATE_MODIFY_TO"       => $lastModifiedDate,
			"PROPERTY_STATUS_ID"   => "P",
			">PROPERTY_WORK_COST"  => self::COST_BORDER,
			"!PROPERTY_PROFILE_ID" => false,
			"PROPERTY_HAS_PROBLEM" => false
		);
		$res      = CIBlockElement::GetList(array("timestamp_x" => "asc"), $arFilter, false, array("nPageSize" => 10), $arSelect);
		$res->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');

		while ($arTask = $res->GetNextElement()) {
			$arFields = $arTask->GetFields();

			$noComission = ! ! $arFields["PROPERTY_NO_COMISSION_VALUE"];

			if ( ! $noComission) {

				$totalComissionSumm = CAppforsaleComission::getTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"]);

				$workCost     = $arFields["PROPERTY_WORK_COST_VALUE"];
				$cost         = $arFields["PROPERTY_COST_VALUE"];
				$withdrawSumm = ($workCost * 0.1) - $cost;
				$withdrawSumm = $withdrawSumm - $totalComissionSumm;
				$withdrawSumm = $withdrawSumm <= self::MAX_COMISSION_SUMM ? $withdrawSumm : self::MAX_COMISSION_SUMM; //Максимум 2000 рублей

				$totalComissionSumm = $totalComissionSumm + $withdrawSumm + $cost; // Общая сумма комиссий


				if (intval($withdrawSumm) >= 0 && $arFields["PROPERTY_PROFILE_ID_VALUE"] > 0) {

					// Снимаем комиссию у мастера в размере (Стоимость работы * 0,1) - Стоимость принятия заявки
					if ($withdrawSumm > 0) {
						\CAppforsaleUserAccount::OrderPay($arFields["PROPERTY_PROFILE_ID_VALUE"], $withdrawSumm, $arFields["ID"], "Автоматическое списание комиссии за завершение заявки №" . $arFields["ID"] . " Убедительно просим Вас погасить задолженность за выполненную работу в течение трех банковских дней");
					}

					// Записываем информацию о сумме комиссии в лог
					CAppforsaleComission::addTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"], $withdrawSumm);

					// Пуш
					if ($withdrawSumm > 0) {
						global $push_id;
						$push_id = 1;
						\CAppforsale::SendMessage(
							array((int) $arFields["PROPERTY_PROFILE_ID_VALUE"]),
							array(
								'body'         => 'Списание второй части остатка от 10% комиссии по заявке ' . $arFields['ID'] . ' в размере  ' . $withdrawSumm . ' рублей. Общий размер комиссии составил ' . $totalComissionSumm . ' рублей',
								'click_action' => 'appforsale.exec'
							),
							array(
								'url' => 'http://dom-bez-zabot.online/youdo/tasks-executor/' . $arFields['DETAIL_PAGE_URL'],
								'id'  => $arFields['ID']
							)
						);
					}
				}
			}

			// Установка статуса заявки "Исполнено"
			CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('STATUS_ID' => 'D'));
			CIBlock::clearIblockTagCache($TASKS_IBLOCK_ID);

			//Смена стадии сделки на "-10% Автомат 7 дней"
			changeDealStage($arFields['ID'], $B24_CLOSE_STAGE);
		}
	}

	/**
	 * Списание остатка комисии и закрытие конкретной заявки
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function writeOfComissionOfTask($taskId, $B24_CLOSE_STAGE = "9") {
		global $DB;
		$TASKS_IBLOCK_ID = 4;
		\Bitrix\Main\Loader::includeModule("iblock");

		if (empty($taskId)) {
			return;
		}


		$arSelect = array("*", "PROPERTY_WORK_COST", "PROPERTY_STATUS_ID", "PROPERTY_COST", "PROPERTY_PROFILE_ID", "PROPERTY_NO_COMISSION");
		$arFilter = array("IBLOCK_ID" => $TASKS_IBLOCK_ID, "ACTIVE" => "Y", "ID" => $taskId, "PROPERTY_STATUS_ID" => "P", "!PROPERTY_PROFILE_ID" => false, "PROPERTY_HAS_PROBLEM" => false, ">PROPERTY_COST" => 0);
		$res      = CIBlockElement::GetList(array("timestamp_x" => "asc"), $arFilter, false, array(), $arSelect);
		$res->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');

		while ($arTask = $res->GetNextElement()) {
			$arFields = $arTask->GetFields();

			// Свойство "Не списывать комсссию"
			$noComission = ! ! $arFields["PROPERTY_NO_COMISSION_VALUE"];

			if ( ! $noComission) {
				$totalComissionSumm = CAppforsaleComission::getTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"]);

				$workCost     = $arFields["PROPERTY_WORK_COST_VALUE"];
				$cost         = $arFields["PROPERTY_COST_VALUE"];
				$withdrawSumm = ($workCost * 0.1) - $cost;
				$withdrawSumm = $withdrawSumm - $totalComissionSumm;
				$withdrawSumm = $withdrawSumm <= self::MAX_COMISSION_SUMM ? $withdrawSumm : self::MAX_COMISSION_SUMM; //Максимум 2000 рублей

				$totalComissionSumm = $totalComissionSumm + $withdrawSumm + $cost; // Общая сумма комиссий

				if (intval($withdrawSumm) >= 0 && $arFields["PROPERTY_PROFILE_ID_VALUE"] > 0) {

					// Снимаем комиссию у мастера в размере (Стоимость работы * 0,1) - Стоимость принятия заявки
					if ($withdrawSumm > 0) {
						\CAppforsaleUserAccount::OrderPay($arFields["PROPERTY_PROFILE_ID_VALUE"], $withdrawSumm, $arFields["ID"], "Автоматическое списание комиссии за завершение заявки №" . $arFields["ID"] . " Убедительно просим Вас погасить задолженность за выполненную работу в течение трех банковских дней");
					}

					// Записываем информацию о сумме комиссии в лог
					CAppforsaleComission::addTotalComissionSumm($arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["ID"], $withdrawSumm);

					// Установка статуса заявки "Исполнено"
					CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('STATUS_ID' => 'D'));
					CIBlock::clearIblockTagCache($TASKS_IBLOCK_ID);

					// Пуш
					if ($withdrawSumm > 0) {
						global $push_id;
						$push_id = 1;
						\CAppforsale::SendMessage(
							array((int) $arFields["PROPERTY_PROFILE_ID_VALUE"]),
							array(
								'body'         => 'Списание 10% комиссии по заявке ' . $arFields['ID'] . ' в размере  ' . $withdrawSumm . ' рублей. Общий размер комиссии составил ' . $totalComissionSumm . ' рублей',
								'click_action' => 'appforsale.exec'
							),
							array(
								'url' => 'http://dom-bez-zabot.online/youdo/tasks-executor/' . $arFields['DETAIL_PAGE_URL'],
								'id'  => $arFields['ID']
							)
						);
					}
				}
			}

			// Установка статуса заявки "Исполнено"
			CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('STATUS_ID' => 'D'));
			CIBlock::clearIblockTagCache($TASKS_IBLOCK_ID);

			//Смена стадии сделки"
			changeDealStage($arFields['ID'], $B24_CLOSE_STAGE);

			self::addUserTotalWorksSumm((int) $arFields["PROPERTY_PROFILE_ID_VALUE"], $arFields["PROPERTY_WORK_COST_VALUE"]);
		}
	}


	/**
	 * Общая сумма списания по заявке
	 *
	 * @param $userId
	 * @param $taskId
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getTotalComissionSumm($userId, $taskId) {
		$entity_data_class    = getHighloadEntity(4);
		$arFilter             = array(
			"UF_USER" => $userId,
			"UF_TASK" => $taskId
		);
		$rsData               = $entity_data_class::getList(array(
			"select" => array("*"),
			"order"  => array("ID" => "ASC"),
			"filter" => $arFilter
		));
		$arTotalComissionSumm = 0;
		while ($arData = $rsData->Fetch()) {
			$arTotalComissionSumm = $arTotalComissionSumm + $arData["UF_SUM"];
		}

		return $arTotalComissionSumm;
	}

	/**
	 * Добавление записи лога
	 *
	 * @param $userId
	 * @param $taskId
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function addTotalComissionSumm($userId, $taskId, $summ) {
		$entity_data_class = getHighloadEntity(4);

		// Массив полей для добавления
		$hlData = array(
			"UF_DATE" => new \Bitrix\Main\Type\DateTime(),
			"UF_USER" => $userId,
			"UF_TASK" => $taskId,
			"UF_SUM"  => $summ,
		);
		$entity_data_class::add($hlData);
	}


	public static function getUserTotalWorksSumm($userId = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$summ = 0;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$summ = $arUser[ self::USER_TOTAL_WORKS_SUMM_FIELD ];
		}

		return $summ;
	}

	public static function addUserTotalWorksSumm($userId = 0, $value = 0) {
		global $APPLICATION;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$summ = self::getUserTotalWorksSumm($userId);


		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::USER_TOTAL_WORKS_SUMM_FIELD => intval($summ + $value)));

		return true;
	}
}