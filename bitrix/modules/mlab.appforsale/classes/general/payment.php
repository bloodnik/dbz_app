<?

class CAppforsalePayment {
	function bonus($amount, $userId = 0) {
		$bonus_balance      = intval(COption::GetOptionInt('mlab.appforsale', 'bonus_balance', 0));
		$bonus_type_balance = COption::GetOptionInt('mlab.appforsale', 'bonus_type_balance', 0);

		$bonusSumm = 0;
		if ($bonus_balance > 0) {
			if ($bonus_type_balance == 0) {
				$bonusSumm = $bonus_balance;
			} elseif ($bonus_type_balance == 1) {
				$bonusSumm = $amount / 100 * $bonus_balance;
			}
		}

		if(intval($userId) > 0) {
			CAppforsalePayment::addBonus($userId, $bonusSumm);
		}

		return $amount;
	}

	function checkParent($user_id) {
		global $USER_FIELD_MANAGER;
		$UF = $USER_FIELD_MANAGER->GetUserFields('USER', $user_id);
		if (array_key_exists('UF_PARENT_ID', $UF) && array_key_exists('UF_PARENT_PAY', $UF)) {
			$parent_id  = $UF['UF_PARENT_ID']['VALUE'];
			$parent_pay = $UF['UF_PARENT_PAY']['VALUE'];
			if (intval($parent_id) > 0 && $parent_pay != 1) {
				$invite_sum = COption::GetOptionInt('mlab.appforsale', "invite_sum", 150);
				if (CAppforsaleUserAccount::UpdateAccount($user_id, $invite_sum, 'OUT_CHARGE_OFF', 0, 'parent_id: ' . $parent_id) &&
				    CAppforsaleUserAccount::UpdateAccount($parent_id, $invite_sum, 'OUT_CHARGE_OFF', 0, 'user_id: ' . $user_id)) {
					$USER_FIELD_MANAGER->Update('USER', $user_id, array(
						'UF_PARENT_PAY' => 'Y'
					));
				}
			}
		}
	}

	function processRequest(Bitrix\Main\Request $request) {
		if ($request->get('notification_type')) {
			if ($request->get('label') && $request->get('sha1_hash') == sha1($request->get('notification_type') . '&' . $request->get('operation_id') . '&' . $request->get('amount') . '&' . $request->get('currency') . '&' . $request->get('datetime') . '&' . $request->get('sender') . '&' . $request->get('codepro') . '&' . COption::GetOptionString('mlab.appforsale', 'notification_secret', '') . '&' . $request->get('label'))) {
				if ( ! CAppforsaleUserAccount::UpdateAccount($request->get('label'), CAppforsalePayment::bonus($request->get('withdraw_amount')), 'OUT_CHARGE_OFF', 0, 'operation_id: ' . $request->get('operation_id'))) {
					CHTTP::SetStatus('400 Bad Request');
				} else {
					CAppforsalePayment::checkParent($request->get('label'));
				}
			} else {
				CHTTP::SetStatus('400 Bad Request');
			}
		} else {
			$performedDatetime = date("Y-m-d") . "T" . date("H:i:s") . ".000" . date("P");

			echo '<?xml version="1.0" encoding="UTF-8" ?>';
			if ($request->get("md5") == strtoupper(md5($request->get("action") . ';' . $request->get("orderSumAmount") . ';' . $request->get("orderSumCurrencyPaycash") . ';' . $request->get("orderSumBankPaycash") . ';' . $request->get("shopId") . ';' . $request->get("invoiceId") . ';' . $request->get("customerNumber") . ';' . COption::GetOptionString('mlab.appforsale', "shopPassword", "")))) {
				switch ($request->get("action")) {
					case "checkOrder":
						echo '<checkOrderResponse performedDatetime="' . $performedDatetime . '" code="0" invoiceId="' . $request->get("invoiceId") . '" shopId="' . $request->get("shopId") . '" />';
						break;
					case "paymentAviso":
						if (CAppforsaleUserAccount::UpdateAccount($request->get("customerNumber"), CAppforsalePayment::bonus($request->get("orderSumAmount"), $request->get("customerNumber")), "OUT_CHARGE_OFF", 0, "invoiceId: " . $request->get("invoiceId"))) {
							CAppforsalePayment::checkParent($request->get("customerNumber"));
							echo '<paymentAvisoResponse performedDatetime="' . $performedDatetime . '" code="0" invoiceId="' . $request->get("invoiceId") . '" shopId="' . $request->get("shopId") . '" />';
						} else {
							global $APPLICATION;
							$ex = $APPLICATION->GetException();
							echo '<paymentAvisoResponse performedDatetime="' . $performedDatetime . '" code="1" message="' . $ex->GetString() . '" />';
						}
						break;
				}
			} else {
				echo '<' . $request->get("action") . 'Response performedDatetime="' . $performedDatetime . '" code="1" message="Значение параметра md5 не совпадает с результатом расчета хэш-функции" />';
			}
		}
	}

	function addBonus($userID, $bonusSumm) {
		try {
			if ($bonusSumm > 0) {
				// Транзакция бонусов
				$bonusTransact = new CAppforsaleTaskBonusTransact($userID, $bonusSumm, 1, "Кэшбэк за пополнение баланса");
				$bonusTransact->addTransact();
			}
		} catch (Exception $e) {
			AddMessage2Log($e->getMessage(), 'ADD_BONUS_F_ERROR');
		}
	}
}

?>