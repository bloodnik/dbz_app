<?php

use Bitrix\Main\Loader;

class CAppforsaleProfile {

	// USER FIELDS
	const INTRO_SHOWED_CODE = 'UF_INTRO_SHOWN';
	const RETURNS_PERCENT_CODE = 'UF_RETURNS_PERCENT';
	const FIRST_TASK_CODE = 'UF_FIRST_TASK';
	const LAST_MODERATE_CODE = 'UF_LAST_MODERATE';
	const WELCOME_PUSH_CODE = 'UF_WELCOME_PUSH';

	// IBLOCKS
	const PROFILE_IBLOCK_ID = 2;
	const TASKS_IBLOCK_ID = 4;

	/**
	 * Возвращает состояние флага "Иснтрукция была показана"
	 *
	 * @param int $userId
	 *
	 * @return bool Статус показа инструкции для пользователя
	 */
	public static function isIntroShown() {
		global $APPLICATION, $USER;

		$userId = $USER->GetID();
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$introShowed = false;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$introShowed = $arUser[ self::INTRO_SHOWED_CODE ];
		}

		return ! ! $introShowed;
	}

	public static function setIntroShown($value = 1) {
		global $APPLICATION, $USER;

		$userId = $USER->GetID();
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::INTRO_SHOWED_CODE => $value));

		return true;
	}

	/**
	 * Возвращает состояние флага "Доступна первая заявка"
	 *
	 * @param int $userId
	 *
	 * @return bool Статус наличия певрой заявки
	 */
	public static function hasFirstTask() {
		global $APPLICATION, $USER;

		$userId = $USER->GetID();
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$hasFirstTask = false;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$hasFirstTask = $arUser[ self::FIRST_TASK_CODE ];
		}

		return ! ! $hasFirstTask;
	}

	public static function setFirstTask($userId, $value = 1) {
		global $APPLICATION;

		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::FIRST_TASK_CODE => $value));

		return true;
	}

	public static function setWelcomePush($userId, $value = 1) {
		global $APPLICATION;

		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::WELCOME_PUSH_CODE => $value));

		return true;
	}


	public function GetByUserID($userID) {
		$userID = (int) $userID;
		if ($userID <= 0) {
			return false;
		}


		$rsProfile = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, 'CREATED_BY' => $userID));
		if ($arProfile = $rsProfile->getNext()) {
			return $arProfile;
		}

		return false;
	}

	public function GetById($profileId) {
		$rsProfile = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, 'ID' => $profileId));
		if ($arProfile = $rsProfile->getNext()) {
			return $arProfile;
		}

		return false;
	}

	public static function GetByCityAndDSection($cityId, $sectionsIds) {
		Loader::includeModule('iblock');

		$arSelect1 = array("*");
		$arFilter1 = array(
			"IBLOCK_ID"             => self::PROFILE_IBLOCK_ID,
			"ACTIVE"                => "Y",
			"PROPERTY_PROFILE_WORK" => $sectionsIds,
			"PROPERTY_CITY"         => $cityId,
			"PROPERTY_BLACK_LIST"   => false,
			"PROPERTY_DEACTIVATED"  => false
		);
		$res       = CIBlockElement::GetList(array(), $arFilter1, false, array(), $arSelect1);
		$partners  = [];
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$partners[] = $arFields["ID"];
		}

		return $partners;
	}


	public static function isActive($userID) {
		$userID = (int) $userID;
		if ($userID <= 0) {
			return false;
		}

		$arProfile = self::GetByUserID($userID);

		return $arProfile && $arProfile['ACTIVE'] == 'Y' ? true : false;
	}

	/**
	 * Автоматическая модерация профилей мастеров по прошествии времени
	 * @return string
	 */
	public static function moderateByAgent() {
		global $DB, $USER;
		if ( ! $USER->GetID()) {
			$USER = new CUser();
			$USER->Authorize(1);
		}

		$WAITING_MINUTS = -2;

		Loader::includeModule("iblock");

		\CTimeZone::Disable();

		$time     = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()); // получаем текущую дату в формате текущего сайта
		$stmp     = MakeTimeStamp($time, "DD.MM.YYYY HH:MI:SS"); //Конвертируем время из строки в Unix-формат.
		$stmpFrom = AddToTimeStamp(array("DD" => -1), $stmp);
		$stmpTo   = AddToTimeStamp(array("MI" => $WAITING_MINUTS), $stmp);
		$dateFrom = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $stmpFrom);
		$dateTo   = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $stmpTo);


		$rsProfile = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::PROFILE_IBLOCK_ID, "ACTIVE" => "N", "DATE_MODIFY_FROM" => $dateFrom, "DATE_MODIFY_TO" => $dateTo), false, array("nPageSize" => 5));
		while ($arProfile = $rsProfile->getNext()) {
			$obEl = new CIBlockElement();

			$boolResult = $obEl->Update($arProfile["ID"], array('ACTIVE' => 'Y', 'MODIFIED_BY' => 1));
			CIBlockElement::SetPropertyValuesEx($arProfile["ID"], false, array('AUTO_MODERATE' => array("VALUE" => 36)));
		}

		\CTimeZone::Enable();

		return "CAppforsaleProfile::moderateByAgent();";
	}

	public static function setLastModeratateDate($userId) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");

			return false;
		}

		\CTimeZone::Disable();

		$date = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());
		$stmp = MakeTimeStamp($date, "DD.MM.YYYY HH:MI:SS");

		$ob = new CUserTypeManager;
		$ob->Update("USER", $userId, array(self::LAST_MODERATE_CODE => date("d.m.Y H:i:s", $stmp)));

		\CTimeZone::Enable();

		return true;
	}

	/**
	 * @param int $userId
	 *
	 * @return String  Дата последней модерации
	 */
	public static function getLastModerateDate($userId = 0) {
		global $APPLICATION, $DB;

		$userId = (int) $userId;
		if ($userId <= 0) {
			$APPLICATION->ThrowException("Не заполнено поле \"Пользователь\"");
		}

		$date = null;

		$rsUser = \CUser::GetByID($userId);
		if ($arUser = $rsUser->Fetch()) {
			$ts = MakeTimeStamp($arUser[ self::LAST_MODERATE_CODE ], "DD.MM.YYYY HH:MI");

			if ($ts) {
				$date = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), $ts);
			}
		}

		return $date;
	}

}