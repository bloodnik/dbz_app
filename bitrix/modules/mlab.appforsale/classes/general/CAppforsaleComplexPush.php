<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Grid\Declension;

class CAppforsaleComplexPush {

	/**
	 * Рассылка по невостребованным заявкам
	 *
	 * @return string
	 */
	static function sendUnclaimedPush() {
		Loader::includeModule('iblock');

		// Все заявки
		$arFilter                     = array(
			"IBLOCK_ID"      => 4,
			"ACTIVE"         => "Y",
			"!PROPERTY_CITY" => false
		);
		$arFilter["DATE_MODIFY_FROM"] = DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
		$arFilter["DATE_MODIFY_TO"]   = DateTime::createFromPhp(new \DateTime(date('Y-m-d')));

		$res            = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_CITY"), array(), array());
		$arAllCityTasks = [];
		while ($arTask = $res->GetNext()) {
			$arAllCityTasks[ $arTask["PROPERTY_CITY_VALUE"] ]["TOTAL"] = $arTask["CNT"];
		}


		// Открытые заявки
		$arFilter                     = array(
			"IBLOCK_ID"          => 4,
			"ACTIVE"             => "Y",
			"!PROPERTY_CITY"     => false,
			"PROPERTY_STATUS_ID" => "N"
		);
		$arFilter["DATE_MODIFY_FROM"] = DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
		$arFilter["DATE_MODIFY_TO"]   = DateTime::createFromPhp(new \DateTime(date('Y-m-d')));

		$res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_CITY"), array(), array());
		while ($arTask = $res->GetNext()) {
			$arAllCityTasks[ $arTask["PROPERTY_CITY_VALUE"] ]["OPEN"] = $arTask["CNT"];
		}

		foreach ($arAllCityTasks as $cityId => $arAllCityTask) {

			if (intval($arAllCityTask["OPEN"]) <= 1) {
				continue;
			}

			$openPercent = round($arAllCityTask["OPEN"] * 100 / $arAllCityTask["TOTAL"]);

			if ($openPercent >= 35) {
				// Пользователи
				$arFilter    = array("UF_CITY" => $cityId);
				$rsUsers     = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
				$arCityUsers = [];
				while ($arUser = $rsUsers->Fetch()) {
					$arCityUsers[] = $arUser["ID"];
				}

				$taskDeclension = new Declension('заявка', 'заявки', 'заявок');
				$message        = "Уважаемые мастера! За последнюю неделю по вашему городу осталось невостребованно " . $arAllCityTask["OPEN"] . " " . $taskDeclension->get($arAllCityTask["OPEN"]) . ". Проявляйте активность и зарабатывайте вместе с нами";

				self::sendPush($arCityUsers, $message);
			}
		}

		return "CAppforsaleComplexPush::sendUnclaimedPush();";
	}

	/**
	 * Рассылка новым зарегистрированным местерам
	 *
	 * @return string
	 */
	static function sendNewUsersPush() {
		$arFilter  = array(
			"!UF_CITY"        => false,
			"DATE_REGISTER_1" => DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-4 days')))),
			"DATE_REGISTER_2" => DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-1 days')))),
			"UF_WELCOME_PUSH" => false
		);
		$rsUsers   = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
		$arUsersId = [];
		while ($arUser = $rsUsers->Fetch()) {
			$arUsersId[] = $arUser["ID"];
			CAppforsaleProfile::setWelcomePush($arUser["ID"], 1);
		}

		$message = "Уважаемый мастер! Мы рады что Вы присоединились к команде профессионалов сервиса подбора мастеров 'Дом Без Забот'. <br>
					Мы даём возможность мастерам широкого профиля деятельности получать обработанные обращения заказчиков и зарабатывать на их выполнении. <br>
					Заказчики ждут от Вас быстрого отклика и профессионального исполнения заказов. <br>
					У Вас есть возможность в любое время принять любую интересующую заявку по Вашему профилю. <br>
					В случае если принятая Вами заявка оказалась не актуальной, у Вас всегда есть возможность оформить запрос на возврат комиссии, после чего служба контроля качества проведёт детальное рассмотрение по Вашему обращению и примет соответствующее решение. <br>
					Перед началом работы внимательно изучите инструкцию для мастеров и варианты принятия заявок с комиссией от 1% до 10%. <br>
					С нашим сервисом у Вас есть возможность обсуждать итоговую стоимость работ на месте с заказчиком. <br>
					Расчеты по оплате Вашей работы происходят напрямую с заказчиком. <br>
					Несмотря на сложную эпидемиологическую ситуацию в России и мире в целом мы продолжаем поддерживать частных мастеров заказами и выгодными условиями сотрудничества.";

		self::sendPush($arUsersId, $message);

		return "CAppforsaleComplexPush::sendNewUsersPush();";
	}


	/**
	 * Рассылка по упущенным деньгам мастеров
	 *
	 * @return string
	 */
	static function sendLostMoneyPush() {
		Loader::includeModule('iblock');

		// Открытые заявки
		$arFilter                     = array(
			"IBLOCK_ID"          => 4,
			"ACTIVE"             => "Y",
			"!PROPERTY_CITY"     => false,
			"PROPERTY_STATUS_ID" => "N"
		);
		$arFilter["DATE_MODIFY_FROM"] = DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
		$arFilter["DATE_MODIFY_TO"]   = DateTime::createFromPhp(new \DateTime(date('Y-m-d')));

		$res = CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_CITY"), array(), array());
		while ($arTask = $res->GetNext()) {
			$arAllCityTasks[ $arTask["PROPERTY_CITY_VALUE"] ]["OPEN"] = $arTask["CNT"];
		}

		// Стоимость заявок
		$arFilter                     = array(
			"IBLOCK_ID"          => 4,
			"ACTIVE"             => "Y",
			"!PROPERTY_CITY"     => false,
			"PROPERTY_STATUS_ID" => "N"
		);
		$arFilter["DATE_MODIFY_FROM"] = DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
		$arFilter["DATE_MODIFY_TO"]   = DateTime::createFromPhp(new \DateTime(date('Y-m-d')));

		$arCityLostMoney = [];
		$res             = CIBlockElement::GetList(array(), $arFilter, false, array(), array("PROPERTY_WORK_COST", "PROPERTY_CITY"));
		while ($arTask = $res->GetNext()) {
			$arCityLostMoney[ $arTask["PROPERTY_CITY_VALUE"] ][] = $arTask["PROPERTY_WORK_COST_VALUE"];
		}

		foreach ($arAllCityTasks as $cityId => $arAllCityTask) {

			if (intval($arAllCityTask["OPEN"]) <= 1) {
				continue;
			}

			$cityLostMonyeSumm = array_sum($arCityLostMoney[ $cityId ]);

			if ($cityLostMonyeSumm >= 50000) {
				// Пользователи
				$arFilter    = array("UF_CITY" => $cityId);
				$rsUsers     = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
				$arCityUsers = [];
				while ($arUser = $rsUsers->Fetch()) {
					$arCityUsers[] = $arUser["ID"];
				}

				$taskDeclension  = new Declension('заявка', 'заявки', 'заявок');
				$moneyDeclension = new Declension('рубль', 'рубля', 'рублей');
				$message         = "Уважаемые мастера! За эту неделю Вы не заработали " . $cityLostMonyeSumm . " " . $moneyDeclension->get($cityLostMonyeSumm) . " не приняв " . $arAllCityTask["OPEN"] . " " . $taskDeclension->get($arAllCityTask["OPEN"]);

				PR($message);

//				self::sendPush($arCityUsers, $message);
			}
		}

		return "CAppforsaleComplexPush::sendLostMoneyPush();";
	}

	static function sendPush($arUserIds, $message) {
		if ( ! empty($arUserIds) && strlen($message) > 0) {
			global $push_id;
			$push_id = 1;
			\CAppforsale::SendMessage(
				array_unique($arUserIds),
				array(
					'body'         => $message,
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => 'http://dom-bez-zabot.online/youdo/'
				)
			);
		}

	}


}