<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\ExpressionField;

class CAppforsaleReturnsRating {

	// USER FIELDS
	const RETURNS_PERCENT_CODE = 'UF_RETURNS_PERCENT';

	private $userId;

	/**
	 * CAppforsaleBonusReturns constructor.
	 *
	 * @param $userId
	 */
	public function __construct($userId) {
		$this->userId = $userId;
	}

	public function setReturnsRating() {

		$currentRating = $this->getReturnsRating();
		$newRating     = $this->calcCurrentReturnsRating();

		if ($currentRating < 35 && $newRating >= 35) {
			\CAppforsale::SendMessage(
				array($this->getUserId()),
				array(
					'body'         => "Вы превысили % возвратов. Возможность запроса возварта отключена до снижения 30%. Снизить процент можно одним из описанных в инструкции для мастеров способом",
					'click_action' => 'appforsale.exec'
				),
				array(
					'url' => 'http://dom-bez-zabot.online/youdo/personal/subscription/'
				)
			);
		}

		$ob = new CUserTypeManager;
		$ob->Update("USER", $this->getUserId(), array(self::RETURNS_PERCENT_CODE => $newRating));

		return true;
	}

	/**
	 * @param int $userId
	 *
	 * @return String  Рейтинг возвратов
	 */
	public function getReturnsRating() {
		$rsUser        = \CUser::GetByID($this->getUserId());
		$returnsRating = 0;
		if ($arUser = $rsUser->Fetch()) {
			$returnsRating = $arUser[ self::RETURNS_PERCENT_CODE ];
		}

		return ! empty($returnsRating) ? $returnsRating : 0;
	}

	public function calcCurrentReturnsRating() {
		global $DB, $APPLICATION;
		Loader::includeModule('iblock');

		// Считаем 30 дней активности
		$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");
		$rsData            = $entity_data_class::getList(array(
			"select"  => array("UF_DATE_SERVICE", "CNT"),
			"filter"  => [
				"UF_USER" => $this->getUserId()
			],
			"order"   => ["UF_DATE_SERVICE" => "DESC"],
			"group"   => ["UF_DATE_SERVICE"],
			'runtime' => array(
				new ExpressionField('CNT', 'COUNT(*)')
			)
		));

		$daysCounter = 1;
		$startDate   = null;
		$endDate     = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
		while ($arData = $rsData->Fetch()) {
			$daysCounter++;
			if ($daysCounter > 30) {
				break;
			}
			$startDate = $arData["UF_DATE_SERVICE"]->toString();
		}


		// Первые 5 заявок
		$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");
		$rsData            = $entity_data_class::getList(array(
			"select" => array("ID", "UF_TASK"),
			"order"  => array("UF_DATE_SERVICE" => "ASC"),
			"filter" => [
				"UF_USER" => $this->getUserId(),
			],
			"limit"  => 5
		));
		$excludeIds        = [];
		while ($arData = $rsData->Fetch()) {
			$excludeIds[] = $arData["UF_TASK"];
		}


		// Общее количество заявок за период
		$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");
		$rsData            = $entity_data_class::getList(array(
			"select"      => array("ID", "UF_TASK"),
			"filter"      => [
				"UF_USER"           => $this->getUserId(),
				">=UF_DATE_SERVICE" => $startDate,
				"<=UF_DATE_SERVICE" => $endDate,
				"!UF_TASK"          => $excludeIds
			],
			'count_total' => true,
		));
		$totalTasks        = $rsData->getCount();
		$includeIds        = [];
		while ($arData = $rsData->Fetch()) {
			$includeIds[] = $arData["UF_TASK"];
		}


		// Количество возвратов
		$entity_data_class = getHighloadEntityByName("DBZBonusReturns");
		$rsData            = $entity_data_class::getList(array(
			"select"      => array("*"),
			"filter"      => [
				"UF_USER"  => $this->getUserId(),
				"UF_TYPE"  => [CAppforsaleBonusReturns::getTypeId("refuse"), CAppforsaleBonusReturns::getTypeId("partial")],
				">UF_DATE" => $startDate,
				"UF_TASK" => $includeIds
			],
			'count_total' => true,
		));

		$returnsCount = $rsData->getCount();

		if ($returnsCount > 0 && $totalTasks > 0) {
			$returnRating = round($returnsCount * 100 / $totalTasks);
		} else {
			$returnRating = 0;
		}

		return $returnRating;
	}


	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}


}