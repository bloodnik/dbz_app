<?php

use Bitrix\Main\Loader;

class CAppforsaleTaskBonusTransact {

	private $userId;
	private $summ;
	private $debit;
	private $note;

	/**
	 * CAppforsaleTaskBonusTransact constructor.
	 *
	 * @param $userId
	 * @param $summ
	 * @param $debit
	 * @param $note
	 */
	public function __construct($userId, $summ, $debit, $note) {
		$this->userId = $userId;
		$this->summ   = $summ;
		$this->debit  = $debit;
		$this->note   = $note;
	}

	function addTransact() {
		// !!! Не выводить в выходной поток сообщения (echo, PR()). Функция используется при начислении кэшбека после пополнения баланса через YandexMoney

		$entity_data_class = getHighloadEntityByName("DBZBonusTransact");
		$hlData            = array(
			"UF_DATE"  => new \Bitrix\Main\Type\DateTime(),
			"UF_USER"  => intval($this->getUserId()),
			"UF_SUMM"  => round($this->getSumm(), 2),
			"UF_DEBIT" => $this->getDebit(),
			"UF_NOTE"  => $this->getNote(),
		);

		try {
			$result = $entity_data_class::add($hlData);

			return $result->isSuccess();
		} catch (Exception $e) {
			return false;
		}
	}

	public static function onAfterAdd(\Bitrix\Main\Entity\Event $event) {
		$id       = $event->getParameter("id");
		$arFields = $event->getParameter("fields");

		$bonusAccount = new CAppforsaleTaskBonusAccount($arFields["UF_USER"]);
		$bonusAccount->setSumm($arFields["UF_SUMM"]);
		$bonusAccount->setDebit($arFields["UF_DEBIT"]);
		$bonusAccount->changeSumm();
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @return mixed
	 */
	public function getSumm() {
		return $this->summ;
	}

	/**
	 * @return mixed
	 */
	public function getDebit() {
		return $this->debit;
	}

	/**
	 * @return mixed
	 */
	public function getNote() {
		return $this->note;
	}


}