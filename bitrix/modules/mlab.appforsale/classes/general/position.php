<? 
class CAppforsalePosition
{
    public static function onCurrentPosition(Bitrix\Main\Event $event)
    { 
        global $USER, $USER_FIELD_MANAGER;
        if ($USER->IsAuthorized())
        {
            $position = $event->getParameter("ENTITY");
            
            $arFields = [
                "UF_LATITUDE" => $position->getCoords()->getLatitude(),
                "UF_LONGITUDE" => $position->getCoords()->getLongitude()
            ];
            
            if ($USER_FIELD_MANAGER->CheckFields("USER", $USER->GetID(), $arFields))
            {
                $USER_FIELD_MANAGER->Update("USER", $USER->GetID(), $arFields);
            }
            
            if (CModule::IncludeModule('iblock'))
            {
                $dbProfile = CIBlockElement::GetList(
                    [],
                    [
                        "IBLOCK_CODE" => "profile",
                        "ACTIVE" => "Y",
                        "CREATED_BY" => $USER->GetID()
                    ],
                    false,
                    false,
                    [
                        "ID",
                        "IBLOCK_ID"
                    ]
                );
                while($arProfile = $dbProfile->GetNext())
                {
                    CIBlockElement::SetPropertyValuesEx(
                        (int) $arProfile["ID"],
                        $arProfile["IBLOCK_ID"],
                        [
                            "GEO" => $arFields["UF_LATITUDE"].','.$arFields["UF_LONGITUDE"]
                            
                        ]
                    );
                }
            }
        }
    }
}
?>