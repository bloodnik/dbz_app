<?php

use Bitrix\Main\Loader;

class CAppforsaleBonusReturns {

	// USER FIELDS
	const UF_TYPE_ID = 105;

	private $taskId;
	private $userId;
	private $returnType;
	private $summ;
	private $deailId;

	/**
	 * CAppforsaleBonusReturns constructor.
	 *
	 * @param $userId
	 */
	public function __construct($taskId, $userId, $returnType, $summ, $deailId) {
		$this->taskId     = $taskId;
		$this->userId     = $userId;
		$this->returnType = $returnType;
		$this->summ       = $summ;
		$this->deailId    = $deailId;
	}

	function add() {
		$entity_data_class = getHighloadEntityByName("DBZBonusReturns");
		$hlData            = array(
			"UF_DATE"    => new \Bitrix\Main\Type\DateTime(),
			"UF_USER"    => intval($this->getUserId()),
			"UF_TASK"    => $this->getTaskId(),
			"UF_TYPE"    => self::getTypeId($this->getReturnType()),
			"UF_SUMM"    => round($this->getSumm(), 2),
			"UF_DEAL_ID" => $this->getDeailId()
		);

		try {
			$result = $entity_data_class::add($hlData);

			return $result->isSuccess();
		} catch (Exception $e) {
			return false;
		}
	}


	static function getTypeId($xmlId) {
		$typeId = null;

		$UserField = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => self::UF_TYPE_ID, "XML_ID" => $xmlId));
		if ($arUserField = $UserField->GetNext()) {
			$typeId = $arUserField["ID"];
		}

		return $typeId;
	}

	/**
	 * @return mixed
	 */
	public function getDeailId() {
		return $this->deailId;
	}


	/**
	 * @return mixed
	 */
	public function getReturnType() {
		return $this->returnType;
	}

	/**
	 * @return mixed
	 */
	public function getSumm() {
		return $this->summ;
	}


	/**
	 * @return mixed
	 */
	public function getTaskId() {
		return $this->taskId;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}


}