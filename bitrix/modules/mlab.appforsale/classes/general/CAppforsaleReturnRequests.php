<?php

use Bitrix\Main\Loader;

class CAppforsaleReturnRequests {

	// USER FIELDS
	const UF_TYPE_ID = 102;

	private $taskId;
	private $userId;
	private $dealId;
	private $requestType;
	private $comment;

	/**
	 * CAppforsaleReturnRequests constructor.
	 *
	 * @param $userId
	 */
	public function __construct($taskId, $dealId, $userId, $requestType, $comment) {
		$this->taskId      = $taskId;
		$this->dealId      = $dealId;
		$this->userId      = $userId;
		$this->requestType = $requestType;
		$this->comment = $comment;
	}


	function add() {
		$entity_data_class = getHighloadEntityByName("DBZBonusReturnRequests");
		$hlData            = array(
			"UF_DATE"    => new \Bitrix\Main\Type\DateTime(),
			"UF_USER"    => intval($this->getUserId()),
			"UF_TASK"    => $this->getTaskId(),
			"UF_DEAL_ID" => $this->getDealId(),
			"UF_TYPE"    => self::getTypeId($this->getRequestType()),
			"UF_COMMENT" => $this->getComment()
		);

		try {
			$result = $entity_data_class::add($hlData);

			return $result->isSuccess();
		} catch (Exception $e) {
			return false;
		}
	}

	static function getTypeId($xmlId) {
		$typeId = null;

		$UserField = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => self::UF_TYPE_ID, "XML_ID" => $xmlId));
		if ($arUserField = $UserField->GetNext()) {
			$typeId = $arUserField["ID"];
		}

		return $typeId;
	}

	/**
	 * @return mixed
	 */
	public function getComment() {
		return $this->comment;
	}


	/**
	 * @return mixed
	 */
	public function getDealId() {
		return $this->dealId;
	}

	/**
	 * @return mixed
	 */
	public function getTaskId() {
		return $this->taskId;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @return mixed
	 */
	public function getRequestType() {
		return $this->requestType;
	}


}