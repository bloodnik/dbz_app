<?php

use Bitrix\Main\Loader;

class CAppforsaleTaskBonus {

	// USER FIELDS
	const UF_CLOSED_TASKS = 'UF_CLOSED_TASKS';
	const BONUS_COST = 1000;

	private $userId;
	private $newClosedTaskCount;

	/**
	 * CAppforsaleTaskBonus constructor.
	 *
	 * @param $userId
	 */
	public function __construct($userId) {
		$this->userId = $userId;
	}


	/**
	 * Возвращает количество успешно закрытых заявок мастером
	 *
	 * @param int $userId
	 *
	 * @return int - Количество закрытых заявок
	 */
	public function getUserClosedTaskCount() {
		$getUserCosedTaskCount = 0;

		$rsUser = \CUser::GetByID($this->userId);
		if ($arUser = $rsUser->Fetch()) {
			$getUserCosedTaskCount = $arUser[ self::UF_CLOSED_TASKS ];
		}

		return $getUserCosedTaskCount;
	}

	/**
	 * Увеличивает количество закрытых заявок мастером
	 *
	 * @param $userId - id пользователя
	 *
	 * @return int
	 */
	public function incrementClosedTaskCount() {
		$currentClosedTaskCount = self::getUserClosedTaskCount();

		if ($currentClosedTaskCount >= 0) {
			$this->newClosedTaskCount = ++$currentClosedTaskCount;

			$ob = new CUserTypeManager;
			if ($ob->Update("USER", $this->userId, array(self::UF_CLOSED_TASKS => $this->newClosedTaskCount))) {
				if ($this->newClosedTaskCount % 20 == 0) {
					PR($this->newClosedTaskCount);
					$this->chargeBonus();
					$this->sendPush();
				}
			}
		}

		return $currentClosedTaskCount;
	}

	private function chargeBonus() {
		try {
			$note = "Поздравляем Вас за качественно выполненные заявки, Вам начислен бонус в размере " . self::BONUS_COST . " рублей. Спасибо за сотрудничество";

			// Транзакция бонусов
			CTimeZone::Disable();
			$bonusTransact = new CAppforsaleTaskBonusTransact($this->userId, self::BONUS_COST, 1, $note);
			$bonusTransact->addTransact();
			CTimeZone::Enable();

		} catch (Exception $e) {
			throw new \Bitrix\Main\SystemException($e->getMessage());
		}
	}


	private function sendPush() {
		global $push_id;
		$push_id = 1;
		\CAppforsale::SendMessage(
			array((int) $this->userId),
			array(
				'body'         => "Ваш счёт пополнен по программе бонусов за выполнение заявок. Вы можете потратить бонусы на платные услуги сервиса. <br> Количество выполенных заявок - " . $this->newClosedTaskCount,
				'click_action' => 'appforsale.exec'
			),
			array(
				'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/'
			)
		);
	}

}