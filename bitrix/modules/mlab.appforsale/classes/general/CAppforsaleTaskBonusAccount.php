<?php

use Bitrix\Main\Loader;

class CAppforsaleTaskBonusAccount {

	// Процент разделеления платежа
	const BONUS_SPLIT_PERCENT = 100;

	private $userId;
	private $summ;
	private $debit;

	/**
	 * CAppforsaleTaskBonusTransact constructor.
	 *
	 * @param $userId
	 * @param $summ
	 */
	public function __construct($userId) {
		$this->userId = $userId;
	}


	/**
	 * Возвращет бонусный аккаунт мастера
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function getAccount() {
		return $this->isExistUserAccount();
	}

	public function calcWriteOfSumm($paySumm) {
		$userBonusAccount = $this->getAccount();

		$bonusSumm = intval($userBonusAccount["UF_SUMM"]);

		$writeOfBonusSumm = 0;
		if ($userBonusAccount && $bonusSumm > 0) {
			$needSumm = $paySumm * (self::BONUS_SPLIT_PERCENT / 100);

			$writeOfBonusSumm = $bonusSumm >= $needSumm ? $needSumm : $bonusSumm;
		}

		return $writeOfBonusSumm;
	}

	function changeSumm() {

		if ($this->getSumm() === null && $this->getDebit() === null) {
			return false;
		}

		$entity_data_class = getHighloadEntityByName("DBZBonusAccount");
		$hlData            = array(
			"UF_USER" => $this->getUserId(),
			"UF_SUMM" => $this->getDebit() ? $this->getSumm() : 0,
		);

		if ( ! $record = self::isExistUserAccount()) {
			$entity_data_class::add($hlData);
		} else {
			$currentSumm       = $record["UF_SUMM"];
			$newSumm           = $this->getDebit() ? $currentSumm + $this->getSumm() : $currentSumm - $this->getSumm();
			$hlData["UF_SUMM"] = $newSumm >= 0 ? $newSumm : 0;

			$entity_data_class::update($record["ID"], $hlData);
		}
	}


	/**
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function isExistUserAccount() {
		$entity_data_class = getHighloadEntityByName("DBZBonusAccount");

		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"filter" => array(
				"UF_USER" => $this->getUserId(),
			),
		));

		while ($arData = $rsData->Fetch()) {
			return $arData;
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @return mixed
	 */
	public function getSumm() {
		return $this->summ;
	}

	/**
	 * @param mixed $summ
	 */
	public function setSumm($summ) {
		$this->summ = $summ;
	}

	/**
	 * @param mixed $debit
	 */
	public function setDebit($debit) {
		$this->debit = $debit;
	}

	/**
	 * @return mixed
	 */
	public function getDebit() {
		return $this->debit;
	}


}