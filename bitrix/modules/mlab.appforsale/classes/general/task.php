<?

class CAppforsaleTask {
	protected static $bDisallow = false;
	const TASKS_IBLOCK_ID = 4;

	function getFormatName($iblock_id, $iblock_section_id) {
		global $USER_FIELD_MANAGER;

		$dbNav      = CIBlockSection::GetNavChain($iblock_id, $iblock_section_id, array('ID'));
		$arNavChain = array();
		while ($arNav = $dbNav->Fetch()) {
			$arNavChain[] = $arNav;
		}

		$arNavChain = array_reverse($arNavChain);

		foreach ($arNavChain as $arNav) {
			$format_name = $USER_FIELD_MANAGER->GetUserFieldValue(
				'IBLOCK_' . $iblock_id . '_SECTION',
				'UF_FORMAT_NAME',
				$arNav['ID']
			);
			if ( ! empty($format_name)) {
				return str_replace('PROPERTY_', '', $format_name);
			}
		}

		return '';
	}

	function OnAfterIBlockElementUpdate($arFields) {
		CAppforsaleTask::OnAfterIBlockElementAdd($arFields);
	}

	function OnAfterIBlockElementAdd($arFields) {
		if (self::$bDisallow) {
			return;
		}

		if (ToUpper(CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'TASK' ||
		    ToUpper(CIBlock::GetArrayByID($arFields['IBLOCK_ID'], 'CODE')) == 'PROFILE') {
			$dbElement = CIBlockElement::GetByID($arFields['ID']);
			if ($obElement = $dbElement->GetNextElement()) {
				$arFields     = $obElement->GetFields();
				$arProperties = $obElement->GetProperties();
				$format_name  = self::getFormatName($arFields['IBLOCK_ID'], $arFields['IBLOCK_SECTION_ID']);
				if ( ! empty($format_name)) {
					$replace_pairs = array();
					preg_match_all("/#([a-z0-9_]+)#/i", $format_name, $matches);
					foreach ($matches[1] as $code) {
						if (array_key_exists($code, $arProperties)) {
							$arProperties[ $code ]              = CIBlockFormatProperties::GetDisplayValue($arFields, $arProperties[ $code ], "");
							$replace_pairs[ '#' . $code . '#' ] = strip_tags(is_array($arProperties[ $code ]['DISPLAY_VALUE']) ? implode(', ', $arProperties[ $code ]['DISPLAY_VALUE']) : $arProperties[ $code ]['DISPLAY_VALUE']);
						} else {
							$replace_pairs[ '#' . $code . '#' ] = '';
						}
					}

					$name = strtr($format_name, $replace_pairs);
					$name = trim($name, ' ,');

					if ($arFields['NAME'] != $name) {
						self::$bDisallow = true;
						$el              = new CIBlockElement;
						$el->Update($arFields['ID'], array('NAME' => strtr($format_name, $replace_pairs)));
						self::$bDisallow = false;
					}
				}
			}
		}
	}

	/**
	 * Закрытие просроченной открытой заявки
	 */
	public static function closeExpiredTask() {
		global $DB;
		$TASKS_IBLOCK_ID = 4;
		$B24_CLOSE_STAGE = '18';
		\Bitrix\Main\Loader::includeModule("iblock");

		// Вычисляем дату -14 дней от текущей даты
		$time             = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time()); // получаем текущую дату в формате текущего сайта
		$stmp             = MakeTimeStamp($time, "DD.MM.YYYY"); //Конвертируем время из строки в Unix-формат.
		$stmp             = AddToTimeStamp(array("DD" => -14), $stmp); //Добавляет к дате в Unix-формате заданный интервал времени. Возвращает новую дату также в Unix-формате.
		$lastModifiedDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), $stmp);

		$arSelect = array("*", "PROPERTY_WORK_COST", "PROPERTY_STATUS_ID", "PROPERTY_COST", "PROPERTY_PROFILE_ID");
		$arFilter = array("IBLOCK_ID" => $TASKS_IBLOCK_ID, "ACTIVE" => "Y", "DATE_MODIFY_FROM" => "01.01.2017", "DATE_MODIFY_TO" => $lastModifiedDate, "PROPERTY_STATUS_ID" => "P", ">PROPERTY_WORK_COST" => 3000);
		$res      = CIBlockElement::GetList(array("timestamp_x" => "asc"), $arFilter, false, array("nPageSize" => 10), $arSelect);
		$res->SetUrlTemplates('#SECTION_CODE_PATH#/#ELEMENT_ID#/');

		while ($arTask = $res->GetNextElement()) {
			$arFields = $arTask->GetFields();

			$workCost     = $arFields["PROPERTY_WORK_COST_VALUE"];
			$cost         = $arFields["PROPERTY_COST_VALUE"];
			$withdrawSumm = ($workCost * 0.1) - $cost;
			$withdrawSumm = $withdrawSumm <= 2000 ? $withdrawSumm : 2000; //Максимум 2000 рублей

			if (intval($withdrawSumm) >= 0 && $arFields["PROPERTY_PROFILE_ID_VALUE"] > 0) {

				// Снимаем комиссию у мастера в размере (Стоимость работы * 0,1) - Стоимость принятия заявки
				if ($withdrawSumm > 0) {
					\CAppforsaleUserAccount::OrderPay($arFields["PROPERTY_PROFILE_ID_VALUE"], $withdrawSumm, $arFields["ID"]);
				}

				// Установка статуса заявки "Исполнено"
				CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array('STATUS_ID' => 'D'));
				CIBlock::clearIblockTagCache($TASKS_IBLOCK_ID);

				//Смена стадии сделки на "-10% Автомат 7 дней"
				sleep(3); // задержка в 3 секунды, чтобы не угодить в блокировку Б24
				changeDealStage($arFields['ID'], $B24_CLOSE_STAGE);

				// Пуш
				if ($withdrawSumm > 0) {
					global $push_id;
					$push_id = 1;
					\CAppforsale::SendMessage(
						array((int) $arFields["PROPERTY_PROFILE_ID_VALUE"]),
						array(
							'body'         => 'С Вас списана комиссия в счёт оплаты заявки (№ ' . $arFields['ID'] . ') в размере 10% от стоимости работ указанной в заявке за минусом комиссии за принятие заявки',
							'click_action' => 'appforsale.exec'
						),
						array(
							'url' => 'http://dom-bez-zabot.online/youdo/tasks-executor/' . $arFields['DETAIL_PAGE_URL'],
							'id'  => $arFields['ID']
						)
					);
				}

			}
		}
	}


	public static function GetByExternalId($externalId) {
		$externalId = (int) $externalId;
		if ($externalId <= 0) {
			return false;
		}


		$rsProfile = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, 'XML_ID' => $externalId), false, false, array("*", "PROPERTY_STATUS_ID", "PROPERTY_PROFILE_ID"));
		if ($arProfile = $rsProfile->getNext()) {
			return $arProfile;
		}

		return false;
	}

	public static function GetTaskStatus($arTaskIds) {
		if (empty($arTaskIds)) {
			return false;
		}

		$arStatusValues = [
			"N" => "Открыто",
			"P" => "В работе",
			"D" => "Исполнено",
			"F" => "Закрыто",
		];

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $arTaskIds), false, false, array("ID", "PROPERTY_STATUS_ID"));


		$arStatus = [];
		while ($arTask = $rsTask->getNext()) {
			$arStatus[ $arTask["ID"] ]["CODE"] = $arTask["PROPERTY_STATUS_ID_VALUE"];
			$arStatus[ $arTask["ID"] ]["NAME"] = $arStatusValues[ $arTask["PROPERTY_STATUS_ID_VALUE"] ];
		}

		return $arStatus;
	}

	/**
	 * @param $taskId
	 *
	 * Возвращает стоимость заявка по ID
	 *
	 * @return bool|int
	 */
	public static function GetTaskCost($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("ID", "PROPERTY_COST", "PROPERTY_FIRST_TASK"));

		while ($arTask = $rsTask->getNext()) {
			if ( ! empty($arTask["PROPERTY_FIRST_TASK_VALUE"])) {
				return intval($arTask["PROPERTY_COST_VALUE"]) / 2;
			} else {
				return intval($arTask["PROPERTY_COST_VALUE"]);
			}
		}

		return 0;
	}

	/**
	 * @param $taskId
	 *
	 * Возвращает заявку по ID
	 *
	 * @return bool|array
	 */
	public static function GetTaskById($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("*", "PROPERTY_*"));

		$arTask = [];
		while ($ob = $rsTask->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$arProps           = $ob->GetProperties();
			$arFields["PROPS"] = $arProps;
			$arTask            = $arFields;
		}

		return $arTask;
	}

	/**
	 * @param $taskId
	 *
	 * Возвращает стоимость заявка по ID
	 *
	 * @return bool|int
	 */
	public static function GetPartialRefundSum($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("ID", "PROPERTY_WORK_COST", "PROPERTY_COST", "PROPERTY_FIRST_TASK"));

		$refundSumm = 0;
		while ($arTask = $rsTask->getNext()) {

			$workCost = intval($arTask["PROPERTY_WORK_COST_VALUE"]);

			if ( ! empty($arTask["PROPERTY_FIRST_TASK_VALUE"])) {
				$cost = intval($arTask["PROPERTY_COST_VALUE"]) / 2;
			} else {
				$cost = intval($arTask["PROPERTY_COST_VALUE"]);
			}

			if ($workCost > 50000) {
				$refundSumm = $cost - 450;
			} elseif ($workCost > 30000 && $workCost <= 50000) {
				$refundSumm = $cost - 300;
			} elseif ($workCost > 20000 && $workCost <= 30000) {
				$refundSumm = $cost - 250;
			} elseif ($workCost > 15000 && $workCost <= 20000) {
				$refundSumm = $cost - 200;
			} elseif ($workCost > 10000 && $workCost <= 15000) {
				$refundSumm = $cost - 150;
			} elseif ($workCost > 5000 && $workCost <= 10000) {
				$refundSumm = $cost - 100;
			} elseif ($workCost <= 5000) {
				$refundSumm = $cost - 50;
			}
		}

		return $refundSumm;
	}

	/**
	 * @param $taskId
	 *
	 * Возвращает ID мастера принявшего заявку
	 *
	 * @return bool|int
	 */
	public static function GetProfileId($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("ID", "PROPERTY_PROFILE_ID"));

		while ($arTask = $rsTask->getNext()) {
			return intval($arTask["PROPERTY_PROFILE_ID_VALUE"]);
		}

		return 0;
	}

	/**
	 * @param $taskId
	 *
	 * Возвращает стоимость работ по заявке с ID
	 *
	 * @return bool|int
	 */
	public static function GetTaskWorkCost($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("ID", "PROPERTY_WORK_COST"));

		while ($arTask = $rsTask->getNext()) {
			return intval($arTask["PROPERTY_WORK_COST_VALUE"]);
		}

		return 0;
	}

	/**
	 * @param $taskId
	 *
	 * Заявка принята по тарифу
	 *
	 * @return bool|int
	 */
	public static function byTariff($taskId) {
		if (empty($taskId)) {
			return false;
		}

		$rsTask = CIBlockElement::GetList(array(), array('IBLOCK_ID' => self::TASKS_IBLOCK_ID, "ID" => $taskId), false, false, array("ID", "PROPERTY_NO_COMISSION"));

		while ($arTask = $rsTask->getNext()) {
			return ! empty($arTask["PROPERTY_NO_COMISSION_VALUE"]);
		}

		return false;
	}


	public static function chargeBackTaskCostToBonus($taskId) {
		$arTask = self::GetTaskById($taskId);

		if ($arTask["PROPS"]["STATUS_ID"]["VALUE"] == "P") {
			$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");

			$rsData = $entity_data_class::getList(array(
				"select" => array("*"),
				"filter" => array(
					"UF_TASK" => $arTask["ID"],
					"UF_USER" => $arTask["PROPS"]["PROFILE_ID"]["VALUE"]
				),
			));

			if ($arData = $rsData->Fetch()) {
				$taskCost = $arData["UF_COST"];

				if ($taskCost > 0 && $arData["UF_BY_TARIFF"] == 0) {
					// Транзакция бонусов
					$bonusTransact = new CAppforsaleTaskBonusTransact($arTask["PROPS"]["PROFILE_ID"]["VALUE"], $taskCost, 1, "Возвтрат средств за отмену заявки");
					$bonusTransact->addTransact();

					AddMessage2Log("За заявку {$taskId} возвращены средства на бонусный счет)", 'CHARGEBACK_TASK_COST');
				}
			}

		}
	}

}

?>