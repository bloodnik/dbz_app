<?php

use Bitrix\Main\Loader;

class CAppforsaleTaskAcceptLog {


	private $userId;
	private $taskId;
	private $byTariff;
	private $isFirstTask;

	/**
	 * CAppforsaleTaskAcceptLog constructor.
	 *
	 * @param $userId
	 * @param $taskId
	 * @param $isFirstTask
	 */
	public function __construct($userId, $taskId, $isFirstTask) {
		$this->userId      = $userId;
		$this->taskId      = $taskId;
		$this->isFirstTask = $isFirstTask;
	}


	function add() {

		$arTask = $this->getTaskData();

		if ( ! empty($arTask)) {
			$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");
			$hlData            = array(
				"UF_DATE_SERVICE" => new \Bitrix\Main\Type\DateTime(),
				"UF_DATE"         => new \Bitrix\Main\Type\DateTime(),
				"UF_USER"         => intval($this->getUserId()),
				"UF_TASK"         => intval($this->getTaskId()),
				"UF_FIRST_TASK"   => intval($this->getIsFirstTask()),
				"UF_BY_TARIFF"    => empty($arTask["PROPS"]["NO_COMISSION"]["VALUE"]) ? 0 : 1,
				"UF_WORK_COST"    => $arTask["PROPS"]["WORK_COST"]["VALUE"],
				"UF_COST"         => $arTask["PROPS"]["COST"]["VALUE"],
				"UF_CITY"         => $arTask["PROPS"]["CITY"]["VALUE"],
				"UF_DEAL_ID"      => $arTask["XML_ID"]
//				"UF_TASK_INFO" => json_encode($arTask),
			);

			try {
				$result = $entity_data_class::add($hlData);

				return $result->isSuccess();
			} catch (Exception $e) {
				return false;
			}
		}

	}

	function getTaskData() {
		return CAppforsaleTask::GetTaskById($this->getTaskId());
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @return mixed
	 */
	public function getTaskId() {
		return $this->taskId;
	}

	/**
	 * @return mixed
	 */
	public function getIsFirstTask() {
		return $this->isFirstTask;
	}


}