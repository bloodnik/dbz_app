<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;

class CAppforsalePushSchedule {

	const UF_DAYS_FIELD_ID = 80;

	private $userId;


	static function getPushList() {
		$entity_data_class = getHighloadEntityByName("DBZPushSchedule");
		$arFilter          = self::setFilter();

		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"filter" => $arFilter,
		));

		$arMessages = [];
		while ($arData = $rsData->Fetch()) {
			$currentTime = self::getCurrentTime();
			$timeDiff    = $currentTime - $arData["UF_TIME_FROM"];

			if ( ! empty($arData["UF_ONCE"]) && $timeDiff === 0) {
				$arMessages[] = $arData["UF_MESSAGE"];
			} elseif (empty($arData["UF_ONCE"]) && ! empty($arData["UF_REGULARITY"]) && $timeDiff % $arData["UF_REGULARITY"] === 0) {
				$arMessages[] = $arData["UF_MESSAGE"];
			}
		}
		self::sendPush($arMessages);

		return "CAppforsalePushSchedule::getPushList();";
	}

	static function setFilter() {

		$arFilter = [];

		$arFilter["UF_ACTIVE"]      = 1;
		$arFilter["UF_DAYS"]        = self::getWeekDayId();
		$arFilter["<=UF_TIME_FROM"] = self::getCurrentTime();
		$arFilter[]                 = array(
			'LOGIC'        => 'OR',
			'>=UF_TIME_TO' => self::getCurrentTime(),
			"UF_TIME_TO"   => false,
		);

		return $arFilter;
	}

	static function getWeekDayId() {
		$weekDayId = null;

		$UserField = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => self::UF_DAYS_FIELD_ID, "XML_ID" => date("N")));
		if ($arUserField = $UserField->GetNext()) {
			$weekDayId = $arUserField["ID"];
		}

		return $weekDayId;
	}

	static function getCurrentTime() {
		return date('H') + 2;
	}

	static function sendPush($arMessages = []) {
		if ( ! empty($arMessages)) {
			$arUsersId = self::getUsers();
			foreach ($arMessages as $arMessage) {
				\CAppforsale::SendMessage(
					array_unique($arUsersId),
					array(
						'body'         => $arMessage,
						'click_action' => 'appforsale.exec'
					),
					array(
						'url' => 'http://dom-bez-zabot.online/youdo/'
					)
				);
			}
		}
	}

	static function getUsers() {
		$filter  = Array("!GROUPS_ID" => Array(5), "ACTIVE" => 'Y');

		$arUsersId = [];

		$rsUsers = CUser::GetList(($by = "id"), ($order = "asc"), $filter);
		while ($arUser = $rsUsers->Fetch()) {
			$arUsersId[] = $arUser['ID'];
		}

//		[37865, 2383, 1, 40027, 27]
		return $arUsersId;
	}

}