<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mlab.appforsale/classes/general/user.php");

class CAppforsaleUserAccount extends CAllAppforsaleUserAccount {
	function GetByID($ID) {
		global $DB;

		$ID = (int) $ID;
		if ($ID <= 0) {
			return false;
		}

		if (isset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ]) && is_array($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ]) && is_set($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ], "ID")) {
			return $GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ];
		} else {
			$strSql =
				"SELECT UA.ID, UA.USER_ID, UA.CURRENT_BUDGET, UA.NOTES, UA.LOCKED, " .
				"	" . $DB->DateToCharFunction("UA.TIMESTAMP_X", "FULL") . " as TIMESTAMP_X, " .
				"	" . $DB->DateToCharFunction("UA.DATE_LOCKED", "FULL") . " as DATE_LOCKED " .
				"FROM appforsale_user_account UA " .
				"WHERE UA.ID = " . $ID . " ";

			$dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($arUserAccount = $dbUserAccount->Fetch()) {
				$GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ] = $arUserAccount;

				return $arUserAccount;
			}
		}

		return false;
	}

	function GetByUserID($userID) {
		global $DB;

		$userID = (int) $userID;
		if ($userID <= 0) {
			return false;
		}

		if (isset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ]) && is_array($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ]) && is_set($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ], "ID")) {
			return $GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ];
		} else {
			$strSql =
				"SELECT UA.ID, UA.USER_ID, UA.CURRENT_BUDGET, UA.NOTES, UA.LOCKED, " .
				"	" . $DB->DateToCharFunction("UA.TIMESTAMP_X", "FULL") . " as TIMESTAMP_X, " .
				"	" . $DB->DateToCharFunction("UA.DATE_LOCKED", "FULL") . " as DATE_LOCKED " .
				"FROM appforsale_user_account UA " .
				"WHERE UA.USER_ID = " . $userID . " ";

			$dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($arUserAccount = $dbUserAccount->Fetch()) {
				$GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $userID . "_" ] = $arUserAccount;

				return $arUserAccount;
			}
		}

		return false;
	}

	function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array()) {
		global $DB;

		if (empty($arSelectFields)) {
			$arSelectFields = array("ID", "USER_ID", "CURRENT_BUDGET", "CURRENCY", "LOCKED", "NOTES", "TIMESTAMP_X", "DATE_LOCKED");
		}

		// FIELDS -->
		$arFields = array(
			"ID"             => array("FIELD" => "UA.ID", "TYPE" => "int"),
			"USER_ID"        => array("FIELD" => "UA.USER_ID", "TYPE" => "int"),
			"CURRENT_BUDGET" => array("FIELD" => "UA.CURRENT_BUDGET", "TYPE" => "double"),
			"LOCKED"         => array("FIELD" => "UA.LOCKED", "TYPE" => "char"),
			"NOTES"          => array("FIELD" => "UA.NOTES", "TYPE" => "string"),
			"TIMESTAMP_X"    => array("FIELD" => "UA.TIMESTAMP_X", "TYPE" => "datetime"),
			"DATE_LOCKED"    => array("FIELD" => "UA.DATE_LOCKED", "TYPE" => "datetime"),
//			"CHARGE_OFF"     => array("FIELD" => "SUM(T.AMOUNT)", "TYPE" => "int", "FROM" => "INNER JOIN appforsale_user_transact T ON (T.USER_ID = UA.USER_ID)", "WHERE" => array("DESCRIPTION" => "OUT_CHARGE_OFF")),
			"USER_LOGIN"     => array("FIELD" => "U.LOGIN", "TYPE" => "string", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)"),
			"USER_ACTIVE"    => array("FIELD" => "U.ACTIVE", "TYPE" => "char", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)"),
			"USER_NAME"      => array("FIELD" => "U.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)"),
			"USER_LAST_NAME" => array("FIELD" => "U.LAST_NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)"),
			"USER_EMAIL"     => array("FIELD" => "U.EMAIL", "TYPE" => "string", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)"),
			"USER_CITY_ID"   => array("FIELD" => "UTS.UF_CITY", "TYPE" => "string", "FROM" => "INNER JOIN b_uts_user UTS ON (UA.USER_ID = UTS.VALUE_ID)"),
			"USER_CITY_NAME" => array("FIELD" => "IB.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_iblock_element IB ON (UTS.UF_CITY = IB.ID)"),
			"USER_USER"      => array("FIELD" => "U.LOGIN,U.NAME,U.LAST_NAME,U.EMAIL,U.ID", "WHERE_ONLY" => "Y", "TYPE" => "string", "FROM" => "INNER JOIN b_user U ON (UA.USER_ID = U.ID)")
		);
		// <-- FIELDS




		$arSqls = CAppforsale::PrepareSql($arFields, $arOrder, $arFilter, $arGroupBy, $arSelectFields);

		$arSqls["SELECT"] = str_replace("%%_DISTINCT_%%", "", $arSqls["SELECT"]);

		if (empty($arGroupBy) && is_array($arGroupBy)) {
			$strSql =
				"SELECT " . $arSqls["SELECT"] . " " .
				"FROM appforsale_user_account UA " .
				"	" . $arSqls["FROM"] . " ";
			if (strlen($arSqls["WHERE"]) > 0) {
				$strSql .= "WHERE " . $arSqls["WHERE"] . " ";
			}
			if (strlen($arSqls["GROUPBY"]) > 0) {
				$strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
			}

			//echo "!1!=".htmlspecialcharsbx($strSql)."<br>";

			$dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			if ($arRes = $dbRes->Fetch()) {
				return $arRes["CNT"];
			} else {
				return false;
			}
		}

		$strSql =
			"SELECT " . $arSqls["SELECT"] . " " .
			"FROM appforsale_user_account UA " .
			"	" . $arSqls["FROM"] . " ";
		if (strlen($arSqls["WHERE"]) > 0) {
			$strSql .= "WHERE " . $arSqls["WHERE"] . " ";
		}
		if (strlen($arSqls["GROUPBY"]) > 0) {
			$strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
		}
		if (strlen($arSqls["ORDERBY"]) > 0) {
			$strSql .= "ORDER BY " . $arSqls["ORDERBY"] . " ";
		}

		if (is_array($arNavStartParams) && intval($arNavStartParams["nTopCount"]) <= 0) {
			$strSql_tmp =
				"SELECT COUNT('x') as CNT " .
				"FROM appforsale_user_account UA " .
				"	" . $arSqls["FROM"] . " ";
			if (strlen($arSqls["WHERE"]) > 0) {
				$strSql_tmp .= "WHERE " . $arSqls["WHERE"] . " ";
			}
			if (strlen($arSqls["GROUPBY"]) > 0) {
				$strSql_tmp .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
			}

			//echo "!2.1!=".htmlspecialcharsbx($strSql_tmp)."<br>";

			$dbRes = $DB->Query($strSql_tmp, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			$cnt   = 0;
			if (strlen($arSqls["GROUPBY"]) <= 0) {
				if ($arRes = $dbRes->Fetch()) {
					$cnt = $arRes["CNT"];
				}
			} else {
				// FOR MYSQL!!! ANOTHER CODE FOR ORACLE
				$cnt = $dbRes->SelectedRowsCount();
			}

			$dbRes = new CDBResult();

			//echo "!2.2!=".htmlspecialcharsbx($strSql)."<br>";

			$dbRes->NavQuery($strSql, $cnt, $arNavStartParams);
		} else {
			if (is_array($arNavStartParams) && intval($arNavStartParams["nTopCount"]) > 0) {
				$strSql .= "LIMIT " . intval($arNavStartParams["nTopCount"]);
			}

			//echo "!3!=".htmlspecialcharsbx($strSql)."<br>";

			$dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		}

		return $dbRes;
	}

	function Add($arFields) {
		global $DB;

		$arFields1 = array();
		foreach ($arFields as $key => $value) {
			if (substr($key, 0, 1) == "=") {
				$arFields1[ substr($key, 1) ] = $value;
				unset($arFields[ $key ]);
			}
		}

		if ( ! CAppforsaleUserAccount::CheckFields("ADD", $arFields, 0)) {
			return false;
		}

		$arInsert = $DB->PrepareInsert("appforsale_user_account", $arFields);

		foreach ($arFields1 as $key => $value) {
			if (strlen($arInsert[0]) > 0) {
				$arInsert[0] .= ", ";
			}

			$arInsert[0] .= $key;

			if (strlen($arInsert[1]) > 0) {
				$arInsert[1] .= ", ";
			}

			$arInsert[1] .= $value;
		}

		$strSql = "INSERT INTO appforsale_user_account(" . $arInsert[0] . ") VALUES (" . $arInsert[1] . ")";
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		$ID = (int) $DB->LastID();

		return $ID;
	}

	function Update($ID, $arFields) {
		global $DB;

		$ID = (int) $ID;
		if ($ID <= 0) {
			return false;
		}

		$arFields1 = array();
		foreach ($arFields as $key => $value) {
			if (substr($key, 0, 1) == "=") {
				$arFields1[ substr($key, 1) ] = $value;
				unset($arFields[ $key ]);
			}
		}

		if ( ! CAppforsaleUserAccount::CheckFields("UPDATE", $arFields, $ID)) {
			return false;
		}

		$arOldUserAccount = CAppforsaleUserAccount::GetByID($ID);

		$strUpdate = $DB->PrepareUpdate("appforsale_user_account", $arFields);

		foreach ($arFields1 as $key => $value) {
			if (strlen($strUpdate) > 0) {
				$strUpdate .= ", ";
			}

			$strUpdate .= $key . "=" . $value . " ";
		}

		$strSql = "UPDATE appforsale_user_account SET " . $strUpdate . " WHERE ID = " . $ID . " ";
		$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		unset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $ID ]);
		unset($GLOBALS["APPFORSALE_USER_ACCOUNT"][ "APPFORSALE_USER_ACCOUNT_CACHE_" . $arOldUserAccount["USER_ID"] . "_" ]);

		return $ID;
	}
}

?>