<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle("Сервис поиска исполнителей");
?>
	<div class="header justify-content-center d-flex align-items-center">
		<div class="card p-5">
			<div class="afs-video-title">
				Освободим Вас от забот о Вашем Доме<br>
			</div>
			<div class="afs-video-text">
				Наши специалисты справятся с любыми проблемами<br>
			</div>
			<div class="text-center">
				<a class="btn btn-primary btn-lg" href="/youdo/tasks-new/">Заказать услугу</a>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="container">
			<? $APPLICATION->IncludeComponent(
				"mlab:appforsale.task.new.sections",
				"top",
				Array(
					"CACHE_TIME"                  => "36000000",
					"CACHE_TYPE"                  => "A",
					"COMPONENT_TEMPLATE"          => ".default",
					"DETAIL_URL"                  => "/youdo/tasks-new/#SECTION_CODE_PATH#/form/",
					"FORM_MESS_BTN_ADD"           => "Сохранить",
					"FORM_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
					"FORM_PROPERTY_CODE"          => array(0 => "", 1 => "",),
					"FORM_PROPERTY_CODE_REQUIRED" => array(0 => "", 1 => "",),
					"IBLOCK_ID"                   => "4",
					"IBLOCK_TYPE"                 => "tasks",
					"SECTIONS_FIELD_CODE"         => array(0 => "", 1 => "",),
					"SECTION_URL"                 => "/youdo/tasks-new/#SECTION_CODE_PATH#/",
					"SEF_URL_TEMPLATES"           => array("sections" => "#SECTION_CODE_PATH#/", "form" => "#SECTION_CODE_PATH#/form/",)
				)
			); ?>
		</div>
	</div>
	<div class="wrap" style="background: #f3f3f3">
		<div class="container">
			<h2 class="text-center">Как это работает</h2>
			<? $APPLICATION->IncludeComponent(
				"mlab:appforsale.promo",
				".default",
				Array(
					"CACHE_TIME"         => "36000000",
					"CACHE_TYPE"         => "A",
					"COMPONENT_TEMPLATE" => ".default",
					"FIELD_CODE"         => array(0 => "", 1 => "PREVIEW_PICTURE", 2 => "",),
					"IBLOCK_ID"          => "8",
					"IBLOCK_TYPE"        => "rules",
					"SORT_FIELD"         => "sort",
					"SORT_FIELD2"        => "id",
					"SORT_ORDER"         => "asc",
					"SORT_ORDER2"        => "asc"
				)
			); ?>
			<div class="text-center">
				<a class="btn btn-primary btn-lg" href="/youdo/tasks-new/">Разместить задание</a>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="container">
			<h2 class="text-center">Основные преимущества</h2>
			<? $APPLICATION->IncludeComponent(
				"mlab:appforsale.promo",
				".default",
				Array(
					"CACHE_TIME"         => "36000000",
					"CACHE_TYPE"         => "A",
					"COMPONENT_TEMPLATE" => ".default",
					"FIELD_CODE"         => array(0 => "", 1 => "PREVIEW_PICTURE", 2 => "",),
					"IBLOCK_ID"          => "7",
					"IBLOCK_TYPE"        => "advantage",
					"SORT_FIELD"         => "sort",
					"SORT_FIELD2"        => "id",
					"SORT_ORDER"         => "asc",
					"SORT_ORDER2"        => "asc"
				)
			); ?>
		</div>
	</div>
	<div class="wrap" style="background: #f3f3f3">
		<div class="container">
			<? $APPLICATION->IncludeComponent(
				"mlab:appforsale.download",
				".default",
				array(
					"COMPONENT_TEMPLATE" => ".default",
					"MESS_DOWNLOAD_NAME" => "Твой дом в надежных руках",
					"MESS_DOWNLOAD_DESC" => "Скачайте приложение и пользуйтесь нашими услугами, где бы вы ни находились.",
					"LINK_APPLE"         => "https://itunes.apple.com/ru/app/домбеззабот/id1440513938?mt=8",
					"LINK_GOOGLE"        => "/dombezzabot.apk"
				),
				false
			); ?>
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					© <?=date('Y') . ' Сервис поиска исполнителей'?>
				</div>
				<div class="col-md-6 text-right">
					<a href="/youdo/personal/?ITEM=agreement" style="color: #fff">
						Соглашение </a>
				</div>
			</div>
		</div>
	</div>
	<br><?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>