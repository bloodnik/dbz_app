<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('ДомБезЗабот');
?>
<? if ($USER->IsAuthorized()): ?>

	<? if ( ! CSite::InGroup([5])): // Если исполнитель?>
		<?
		$APPLICATION->IncludeComponent(
			'siril:notification.list',
			'',
			array(
				'CACHE_TIME' => '36000000',
				'CACHE_TYPE' => 'A',
			),
			false
		);
		?>

	<? elseif (CSite::InGroup([5])): ?>
		<? echo '<script>app.loadPageStart({url: "/youdo/executors/", title: ""})</script>'; ?>
	<? endif; ?>
<? else: ?>
	<? $APPLICATION->IncludeComponent(
		"appforsale:login",
		"",
		array(),
		false
	); ?>
<? endif; ?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>