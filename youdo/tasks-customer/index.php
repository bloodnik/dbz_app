<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('Мои заказы');

if ($USER->IsAuthorized()):
	if ( CSite::InGroup([5])) :
		$APPLICATION->IncludeComponent(
			"mlab:appforsale.meneger",
			".default",
			array(
				"CACHE_TIME"                        => "3600",
				"CACHE_TYPE"                        => "A",
				"ELEMENT_SORT_FIELD"                => "date_create",
				"ELEMENT_SORT_FIELD2"               => "id",
				"ELEMENT_SORT_ORDER"                => "desc",
				"ELEMENT_SORT_ORDER2"               => "desc",
				"IBLOCK_ID"                         => "4",
				"IBLOCK_TYPE"                       => "tasks",
				"OFFER_IBLOCK_ID"                   => "3",
				"OFFER_IBLOCK_TYPE"                 => "offers",
				"SEF_FOLDER"                        => "/youdo/tasks-customer/",
				"SEF_MODE"                          => "Y",
				"OFFER_FORM_PROPERTY_CODE"          => array(
					0 => "3",
					1 => "",
				),
				"OFFER_FORM_PROPERTY_CODE_REQUIRED" => array(
					0 => "3",
					1 => "",
				),
				"OFFER_PROPERTY_CODE"               => array(
					0 => "3",
					1 => "",
				),
				"COMPONENT_TEMPLATE"                => ".default",
				"OWNER"                             => "N",
				"LIST_FIELD_CODE"                   => array(
					0 => "",
					1 => "DATE_CREATE",
					2 => "",
				),
				"LIST_PROPERTY_CODE"                => array(
					0 => "28",
					1 => "29",
					2 => "43",
					3 => "59",
					4 => "",
				),
				"DETAIL_FIELD_CODE"                 => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PROPERTY_CODE"              => array(
					0 => "",
					1 => "",
				),
				"OFFER_FIELD_CODE"                  => array(
					0 => "",
					1 => "",
				),
				"OFFER_MESS_BTN_ADD"                => "Отправить предложение",
				"DETAIL_MESS_CUSTOMER"              => "Заказчик этого задания",
				"DETAIL_MESS_CLAIM"                 => "Пожаловаться на задание",
				"DETAIL_MESS_YOUR_OFFER"            => "Ваше предложение",
				"OFFER_FORM_FIELD_CODE"             => array(
					0 => "",
					1 => "",
				),
				"OFFER_FORM_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
				"OFFER_FORM_MESS_BTN_ADD"           => "Взять заказ",
				"USE_FILTER"                        => "Y",
				"USE_SORT"                          => "N",
				"USE_MAP"                           => "Y",
				"CNT"                               => "N",
				"FILTER_SORT_FIELD"                 => "timestamp_x",
				"FILTER_SORT_ORDER"                 => "desc",
				"FILTER_SORT_FIELD2"                => "id",
				"FILTER_SORT_ORDER2"                => "desc",
				"SEF_URL_TEMPLATES"                 => array(
					"list"   => "",
					"detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
					"offer"  => "#SECTION_CODE_PATH#/#ELEMENT_ID#/offer/",
				)
			),
			false
		);
	endif;
endif;

?>

<? /*$APPLICATION->IncludeComponent(
	"mlab:appforsale.task.customer", 
	".default", 
	array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"ELEMENT_SORT_FIELD" => "timestamp_x",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "tasks",
		"OFFER_IBLOCK_ID" => "3",
		"OFFER_IBLOCK_TYPE" => "offers",
		"OFFER_PROPERTY_CODE" => array(
			0 => "3",
			1 => "",
		),
		"SEF_FOLDER" => "/youdo/tasks-customer/",
		"SEF_MODE" => "Y",
		"COMMENT_IBLOCK_TYPE" => "comments",
		"COMMENT_IBLOCK_ID" => "5",
		"COMMENT_PROPERTY_CODE" => array(
			0 => "22",
			1 => "23",
			2 => "",
		),
		"COMMENT_PROPERTY_CODE_REQUIRED" => array(
			0 => "22",
			1 => "",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"USE_SWITCH" => "N",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFER_SORT_FIELD" => "timestamp_x",
		"OFFER_SORT_ORDER" => "desc",
		"OFFER_SORT_FIELD2" => "id",
		"OFFER_SORT_ORDER2" => "desc",
		"OFFER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFER_MESS_OFFER" => "Предложения",
		"OFFER_MESS_BTN_REJECT" => "Отклонить",
		"OFFER_MESS_BTN_CONFIRM" => "Подтвердить",
		"OFFER_MESS_BTN_CANCEL" => "Отменить",
		"OFFER_MESS_BTN_DONE" => "Выполнено",
		"OFFER_MESS_BTN_COMMENT" => "Оставить отзыв",
		"COMMENT_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
		"COMMENT_MESS_BTN_ADD" => "Сохранить",
		"FORM_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"FORM_PROPERTY_CODE_REQUIRED" => array(
			0 => "",
			1 => "",
		),
		"FORM_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
		"FORM_MESS_BTN_ADD" => "Сохранить",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
			"comment" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/comment/",
		)
	),
	false
);*/ ?>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>