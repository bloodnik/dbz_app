<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('ДомБезЗабот');

global $USER;
global $APPLICATION;

$userLogin = $USER->GetLogin();


?>
<? if ($USER->IsAuthorized()): ?>


	<style>
		.block-page.jumbotron {
			height: 100%;
			margin: 0;
			position: absolute;
			z-index: 999999;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			background: #00B94A;
			color: #fff;
		}
	</style>


	<div class="jumbotron block-page text-center">
		<h2 class="hero-title">Данное приложение больше не поддерживается</h2>

		<p>
			Просим установить новое приложение
		</p>
		<div>
			<div class="afs-download-badge">
				<a href="https://apps.apple.com/ru/app/id1558631394"><img src="/bitrix/components/mlab/appforsale.download/templates/.default/images/badge-download-on-the-app-store-ru.png"></a>
				<a href="https://play.google.com/store/apps/details?id=info.dombezzabot.worker"><img src="/bitrix/components/mlab/appforsale.download/templates/.default/images/google-play-badge.png"></a>
			</div>
		</div>
		<br>
		<p>Ваш номер телефона для авторизации в новом приложении: <strong><?=$userLogin?></strong></p>

		<p>Ваш баланс и текущий активный тариф будут перенесены автоматически</p>

	</div>

<? endif; ?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>