<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};

\Bitrix\Main\Loader::includeModule("iblock");

$fromDate = $_GET["dateStart"] ? $_GET["dateStart"] : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-90 days'))))->format("Y-m-d");
$toDate   = $_GET["dateEnd"] ? $_GET["dateEnd"] : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-30 days'))))->format("Y-m-d");

//Профили
$arProfiles = [];
$arUserIds  = [];

$arSelect        = array("ID", "NAME", "CREATED_BY", "PROPERTY_RESPONSIBLE", "PROPERTY_CALL_DATE");
$arFilterProfile = array(
	"IBLOCK_ID"             => 2,
	">=PROPERTY_CALL_DATE"  => $fromDate . " 00:00:00",
	"<=PROPERTY_CALL_DATE"  => $toDate . " 23:59:59",
	"!PROPERTY_RESPONSIBLE" => false,
	"ACTIVE"                => "Y"
);

$arManagers = [];
$resProfile = CIBlockElement::GetList(array("PROPERTY_RESPONSIBLE" => "ASC", "PROPERTY_CALL_DATE" => "ASC"), $arFilterProfile, false, array(), $arSelect);
while ($arProfile = $resProfile->GetNext()) {
//	PR($arProfile);
	$master                                                   = [];
	$master["id"]                                             = $arProfile["CREATED_BY"];
	$master["dateFrom"]                                       = DateTime::createFromText($arProfile["PROPERTY_CALL_DATE_VALUE"]);
	$master["dateTo"]                                         = DateTime::createFromText($arProfile["PROPERTY_CALL_DATE_VALUE"])->add('30 days');
	$arManagers[ $arProfile['PROPERTY_RESPONSIBLE_VALUE'] ][] = $master;
}

//$rsUsers = CUser::GetList(($by="id"), ($order="asc"), array("ID" => implode(" | ", array_keys($arManagers))));
//while($arUser = $rsUsers->Fetch()) {
//	PR($arUser);
//}

$arReport = [];
foreach ($arManagers as $arManager => $arMasters) {
//	PR($arMasters);

	$arTotalMastersSum = 0;
	foreach ($arMasters as $arMaster) {
		$arTransactFilter = [
			"USER_ID"         => $arMaster["id"],
			"%DESCRIPTION"    => array("TARIFF_PAY", "Оплата заказа"),
			"DEBIT"           => "N",
			">=TRANSACT_DATE" => $arMaster["dateFrom"],
			"<=TRANSACT_DATE" => $arMaster["dateTo"]

		];
		$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
		while ($arTransaction = $res->GetNext()) {
			$arTotalMastersSum = $arTotalMastersSum + $arTransaction["AMOUNT"];
		}
	}

	$dbUser = CUser::GetByID($arManager);
	$arUser = $dbUser->Fetch();

	$arReport[] = array(
		"MANAGER"       => $arUser,
		"SUMM"          => $arTotalMastersSum,
		"MASTERS_COUNT" => count($arMasters)
	);


}

//PR($arReport);

$arHeaders = [
	'index',
	["key" => "fio", "label" => "ФИО", "sortable" => true],
	["key" => "masters", "label" => "Количество мастеров", "sortable" => true],
	["key" => "amount", "label" => "Потраченная сумма мастеров", "sortable" => true],
];

$arItems = [];
foreach ($arReport as $item) {
	$arItems[] = array(
		"fio"     => $item["MANAGER"]["NAME"],
		"masters" => $item["MASTERS_COUNT"],
		"amount"  => $item["SUMM"]
	);
}

?>
	<div id="reportTable">
		<br>
		<form method="get" action="<?=POST_FORM_ACTION_URI?>">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Период с:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-60 days'))?>" name="dateStart">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Период по:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d', strtotime('-30 day'))?>" name="dateEnd">
					</div>
				</div>
				<div class="col">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>

		<b-table
				class="table table-striped"
				:items="items"
				:fields="fields"
				responsive="sm"
		>

			<!-- A virtual column -->
			<template v-slot:cell(index)="data">
				{{ data.index + 1 }}
			</template>

		</b-table>


	</div>


	<script>
		var reportTable = new Vue({
			el: '#reportTable',
			data() {
				return {
					fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
					items: <?=CUtil::PhpToJSObject($arItems)?>
				}
			},

			methods: {
				sortDate(a, b, key) {
					if (key !== 'last_activity') {
						// `key` is not the field that is a date.
						// Let b-table handle the sorting for other columns
						// returning null or false will tell b-table to fall back to it's
						// internal sort compare routine for fields keys other than `myDateField`
						return null // or false
					}
					aDate = moment(a[key], 'DD.MM.YYYY hh:mm:ss')
					bDate = moment(b[key], 'DD.MM.YYYY hh:mm:ss')
					if (aDate.isValid && bDate.isValid) {
						if (aDate < bDate) {
							return -1
						} else if (aDate > bDate) {
							return 1
						} else {
							return 0
						}
					}
					return null
				}
			}
		})
	</script>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>