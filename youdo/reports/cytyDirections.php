<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};
\Bitrix\Main\Loader::includeModule("iblock");


// Города
$arCities = [];
$arSelect = array("ID", "NAME");
$arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ] = $arCity["NAME"];
}

//Направления
$rsSections = CIBlockSection::GetList(array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID), false, array("UF_FEE_PRICE"));
$arSections = [];
while ($arSection = $rsSections->GetNext()) {
	if ($arSection['IBLOCK_SECTION_ID']) {
		continue;
	}
	$arItem                         = array();
	$arItem['ID']                   = $arSection['ID'];
	$arItem['NAME']                 = $arSection['NAME'];
	$arItem['IBLOCK_SECTION_ID']    = $arSection['IBLOCK_SECTION_ID'];
	$arItem['UF_FEE_PRICE']         = $arSection['UF_FEE_PRICE'];
	$arSections[ $arSection['ID'] ] = $arItem;
}

// Пользователи
$arFilter  = array("UF_CITY" => $_GET["city"]);
$rsUsers   = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
$arUsers   = [];
$arIdUsers = [];
while ($arUser = $rsUsers->Fetch()) {
	$user               = [];
	$user["FIO"]        = $arUser["LAST_NAME"] . ' ' . $arUser["NAME"] . '  ' . $arUser["SECOND_NAME"];
	$user["LAST_LOGIN"] = $arUser["LAST_LOGIN"];
	$arUsers[]          = $user;
	$arIdUsers[]        = $arUser["ID"];
}

//Профили
$arProfiles      = [];
$arSelect        = array("ID", "IBLOCK_ID", "NAME", "CREATED_BY");
$arFilterProfile = array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "CREATED_USER_ID" => $arIdUsers);
$resProfile      = CIBlockElement::GetList(array(), $arFilterProfile, false, array(), $arSelect);
while ($obProfile = $resProfile->GetNextElement()) {
	$arProfile                                       = $obProfile->GetFields();
	$arProfileProps                                  = $obProfile->GetProperties();
	$arProfiles[ $arProfile["CREATED_BY"] ]          = $arProfile;
	$arProfiles[ $arProfile["CREATED_BY"] ]["WORKS"] = $arProfileProps["PROFILE_WORK"]["VALUE"];
}

// Заявки
$arTasks                      = [];
$arSelect                     = array("ID", "NAME", "PROPERTY_CITY", "PROPERTY_STATUS_ID");
$arFilter                     = array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "SECTION_ID" => array_keys($arSections), "PROPERTY_CITY" => $_GET['city']);
$arFilter["DATE_MODIFY_FROM"] = $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
$arFilter["DATE_MODIFY_TO"]   = $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day'))));
$res                          = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arTask = $res->GetNext()) {
	$arTasks[ $arTask["PROPERTY_STATUS_ID_VALUE"] ][] = $arTask["ID"];
}


//Log
$arCityPayments     = [];
$arCityUniqUsers    = [];
$arCityTransactions = [];
$arCityTariffBuy    = [];
$arCityComissionBuy = [];

$entity_data_class = getHighloadEntity(2);
$rsData            = $entity_data_class::getList(array(
	"select" => array("*"),
	"order"  => array("ID" => "ASC"),
	"filter" => array(
		"UF_USER"   => $arIdUsers,
		">=UF_DATE" => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days')))),
		"<=UF_DATE" => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day')))),
	)
));

while ($arData = $rsData->Fetch()) {
	$arCityPayments[]                      = $arData["UF_COST"];
	$arCityUniqUsers[ $arData["UF_USER"] ] = 1;
}

//Транзакции
$arTransactFilter = [
	"USER_ID"         => $arIdUsers,
	"%DESCRIPTION"    => array("COMISSION_PAY", "TARIFF_PAY", "Оплата заказа"),
	"DEBIT"           => "N",
	">=TRANSACT_DATE" => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days')))),
	"<=TRANSACT_DATE" => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day')))),
];
$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
while ($arTransaction = $res->GetNext()) {
	$arCityTransaction[] = $arTransaction["AMOUNT"];
	if ($arTransaction["DESCRIPTION"] == "TARIFF_PAY") {
		$arCityTariffBuy[] = $arTransaction["AMOUNT"];
	} else {
		$arCityComissionBuy[] = $arTransaction["AMOUNT"];
	}
}

$arReport["CITY"]         = $arCities[ $_GET['city'] ];
$arReport["CITY_ID"]      = $_GET['city'];
$arReport["TOTAL_USERS"]  = count($arUsers);
$arReport["ACTIVE_USERS"] = count(array_filter($arUsers, function($user) {
	$dateStart = $_GET["dateStart"] ? strtotime($_GET["dateStart"]) : strtotime(date('-7 days'));
	$dateEnd   = $_GET["dateEnd"] ? strtotime($_GET["dateEnd"]) : strtotime(date('Y-m-d'));

	return (MakeTimeStamp($user["LAST_LOGIN"]) >= $dateStart) && (MakeTimeStamp($user["LAST_LOGIN"]) <= $dateEnd);
}));

$arSectionsReport = [];
$aDirectionTasksStatus        = [];
$aDirectionTasks              = [];
foreach ($arSections as $arSection) {
	$arSectionsReport[ $arSection["ID"] ]["USERS"] = count(array_filter($arProfiles, function($profile) use ($arSection) {
		return in_array($arSection["ID"], $profile["WORKS"]);
	}));

	//Подписки для направлений
	$entity_data_class = getHighloadEntity(2);
	$rsData            = $entity_data_class::getList(array(
		"select" => array("*"),
		"order"  => array("ID" => "ASC"),
		"filter" => array(
			"UF_USER"       => $arIdUsers,
			"UF_DIRECTIONS" => $arSection["ID"],
			">=UF_DATE"     => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days')))),
			"<=UF_DATE"     => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day')))),
		)
	));
	while ($arData = $rsData->Fetch()) {
		$arDirectionPayment[ $arSection["ID"] ]["COST"][] = $arSections[ $arSection["ID"] ]["UF_FEE_PRICE"] * $arData["UF_DAYS"];
		$arDirectionUniqUsers[ $arSection["ID"] ][ $arData["UF_USER"] ] = 1;
	}
	$arDirectionPaymentsSum[ $arSection["ID"] ] = array_sum($arDirectionPayment[ $arSection["ID"] ]["COST"]);

	// Заявки для направлений
	$arSelect                     = array("ID", "NAME", "PROPERTY_CITY", "PROPERTY_STATUS_ID");
	$arFilter                     = array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "SECTION_ID" => $arSection["ID"], "PROPERTY_CITY" => $_GET['city']);
	$arFilter["DATE_MODIFY_FROM"] = $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
	$arFilter["DATE_MODIFY_TO"]   = $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day'))));
	$res                          = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
	while ($arTask = $res->GetNext()) {
		$aDirectionTasksStatus[ $arSection["ID"] ][ $arTask["PROPERTY_STATUS_ID_VALUE"] ][] = $arTask["ID"];
		$aDirectionTasks[ $arSection["ID"] ][]                                              = $arTask["ID"];
	}
}

//Транзакции по направлениям
foreach ($aDirectionTasks as $key => $aDirectionTask) {

	$arTransactFilter = [
		"ORDER_ID"     => $aDirectionTask,
		"%DESCRIPTION" => array("COMISSION_PAY", "TARIFF_PAY", "Оплата заказа"),
		"DEBIT"        => "N"
	];

	$res = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "ORDER_ID", "TRANSACT_DATE", "USER_ID"));
	while ($arTransaction = $res->GetNext()) {

		$arDirectionTransaction[ $key ][] = $arTransaction["AMOUNT"];
		if ($arTransaction["DESCRIPTION"] == "TARIFF_PAY") {
			$arDirectionTariffBuy[ $key ][] = $arTransaction["AMOUNT"];
		} else {
			$arDirectionComissionBuy[ $key ][] = $arTransaction["AMOUNT"];
		}
	}
}

$arHeaders = [
	["key" => "city", "label" => "Город", "sortable" => true],
	["key" => "userCount", "label" => "Количество пользователей", "sortable" => true],
	["key" => "activeUsers", "label" => "Активные", "sortable" => true],
	["key" => "subscribeSumm", "label" => "Сумма подписок", "sortable" => true],
	["key" => "cityBuySum", "label" => "Всего потрачено", "sortable" => true],
	["key" => "tariffSum", "label" => "На тарифы", "sortable" => true],
	["key" => "comissionSum", "label" => "Комиссия 10%", "sortable" => true],
	["key" => "uniqSubscribes", "label" => "Уникальных подписок", "sortable" => true],
	["key" => "Pworks", "label" => "Выполняется заявок", "sortable" => true],
	["key" => "Dworks", "label" => "Исполнено заявок", "sortable" => true],
	["key" => "Fworks", "label" => "Закрыто для предлоежний заявок", "sortable" => true],
	["key" => "Nworks", "label" => "Всего заявок", "sortable" => true],
	["key" => "allWorks", "label" => "Открыто заявок", "sortable" => true],
];

$totalSum     = 0;
$activeSum    = 0;
$paymentsSum  = 0;
$cityBuySum   = 0;
$tariffSum    = 0;
$comissionSum = 0;
$usersSum     = 0;

$cityPayments = array_sum($arCityPayments);
$cityBuySum   = array_sum($arCityTransaction);
$tariffSum    = array_sum($arCityTariffBuy);
$comissionSum = array_sum($arCityComissionBuy);
$uniqueUsers  = count($arCityUniqUsers);

$sumN = count($arTasks["N"]);
$sumP = count($arTasks["P"]);
$sumD = count($arTasks["D"]);
$sumF = count($arTasks["F"]);

$arItems   = [];
$arItems[] = [
	"city"           => $arReport["CITY"],
	"userCount"      => $arReport["TOTAL_USERS"],
	"activeUsers"    => $arReport["ACTIVE_USERS"],
	"subscribeSumm"  => $cityPayments > 0 ? round($cityPayments) : 0,
	"cityBuySum"     => $cityBuySum > 0 ? round($cityBuySum) : 0,
	"tariffSum"      => $tariffSum > 0 ? round($tariffSum) : 0,
	"comissionSum"   => $comissionSum > 0 ? round($comissionSum) : 0,
	"uniqSubscribes" => $uniqueUsers,
	"Pworks"         => $sumP,
	"Dworks"         => $sumD,
	"Fworks"         => $sumF,
	"Nworks"         => $sumN + $sumP + $sumD + $sumF,
	"allWorks"       => $sumN,
];

foreach ($arSections as $arSection) {
	$allTransactions = array_sum($arDirectionTransaction[$arSection["ID"]]);
	$tariffTransactions = array_sum($arDirectionTariffBuy[$arSection["ID"]]);
	$comissionTransactions = array_sum($arDirectionComissionBuy[$arSection["ID"]]);

	$sumN = count($aDirectionTasksStatus[$arSection["ID"]]["N"]);
	$sumP = count($aDirectionTasksStatus[$arSection["ID"]]["P"]);
	$sumD = count($aDirectionTasksStatus[$arSection["ID"]]["D"]);
	$sumF = count($aDirectionTasksStatus[$arSection["ID"]]["F"]);

	$arItems[] = [
		"city"           => $arSection["NAME"],
		"userCount"      => $arSectionsReport[$arSection["ID"]]["USERS"],
		"activeUsers"    => "-",
		"subscribeSumm"  => $arDirectionPaymentsSum[$arSection["ID"]] > 0 ? round($arDirectionPaymentsSum[$arSection["ID"]]) : 0,
		"cityBuySum"     => $allTransactions > 0 ? round($allTransactions) : 0,
		"tariffSum"      => $tariffTransactions > 0 ? round($tariffTransactions) : 0,
		"comissionSum"   => $comissionTransactions > 0 ? round($comissionTransactions) : 0,
		"uniqSubscribes" => count($arDirectionUniqUsers[$arSection["ID"]]),
		"Pworks"         => $sumP,
		"Dworks"         => $sumD,
		"Fworks"         => $sumF,
		"Nworks"         => $sumN + $sumP + $sumD + $sumF,
		"allWorks"       => $sumN,
	];
}


?>
	<div id="reportTable">
		<br>
		<form method="get" action="<?=POST_FORM_ACTION_URI?>">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Период с:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-7 days'))?>" name="dateStart">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Период по:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d', strtotime('+1 day'))?>" name="dateEnd">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Город</label>
						<select name="city" class="form-control">
							<? foreach ($arCities as $key => $arCity): ?>
								<option <? if ($_GET["city"] == $key): ?>selected<? endif; ?> value="<?=$key?>"><?=$arCity?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>

		<b-table
				class="table table-striped"
				:items="items"
				:fields="fields"
				:sort-by.sync="sortBy"
				:sort-desc.sync="sortDesc"
				responsive="sm"
		>
		</b-table>


	</div>


	<script>
        var reportTable = new Vue({
            el: '#reportTable',
            data() {
                return {
                    sortBy: 'userCount',
                    sortDesc: true,
                    fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
                    items: <?=CUtil::PhpToJSObject($arItems)?>,
                    totals: <?=CUtil::PhpToJSObject($arTotals)?>
                }
            }
        })
	</script>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>