<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отчеты");

global $USER;
if ( ! $USER->IsAuthorized()) {
	ShowError("Необходима авторизация");

	return;
};


?>

	<ul>
<!--		<li><a href="subscribePayments.php">Платежи по подпискам</a></li>-->
		<li><a href="activeUsers.php">Активные пользователи</a></li>
<!--		<li><a href="subscribeLog.php">Лог подписок за период</a></li>-->
<!--		<li><a href="tasksReport.php">Отчет по заявкам</a></li>-->
		<li><a href="masters.php?&city=6726">Отчет по мастерам</a></li>
		<li><a href="cytyDirections.php?&city=6726">Отчет по направлениям</a></li>
		<li><a href="registerReport.php?&year=<?=date("Y")?>">Динамика регистрации мастеров</a></li>
		<li><a href="userAccounts.php">Счета пользователей</a></li>
		<li><a href="taskAcceptReport.php">Отчет по количеству принятий заявки</a></li>
		<li><a href="managers.php">Отчет по менеджерам</a></li>
	</ul>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>