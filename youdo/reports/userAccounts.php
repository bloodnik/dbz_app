<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};

$connection = Bitrix\Main\Application::getConnection();
$sqlHelper  = $connection->getSqlHelper();

$year = htmlspecialchars(isset($_GET["year"])) ? $_GET["year"] : date("Y");

$dateFrom = $_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-7 days'));
$dateEnd  = $_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d', strtotime('+1 day'));


$sql = "
SELECT ua.USER_ID, IFNULL(uts.UF_RETURNS_PERCENT, 0) as UF_RETURNS_PERCENT, CONCAT(u.NAME, ' ', u.LAST_NAME) as FIO, u.LOGIN as PHONE, u.LAST_LOGIN as LAST_LOGIN,  ib.NAME as CITY, ib.ID as CITY_ID, round(ua.CURRENT_BUDGET, 2) CURRENT_BUDGET, count(utTotal.ID) TRANSACTIONS,
			IFNULL((SELECT round(sum(ut.AMOUNT), 2) FROM appforsale_user_transact ut WHERE ut.USER_ID = ua.USER_ID and ut.DESCRIPTION = 'OUT_CHARGE_OFF' and ut.TRANSACT_DATE between STR_TO_DATE('" . $sqlHelper->forSql($dateFrom) . "', '%Y-%m-%d') and STR_TO_DATE('" . $sqlHelper->forSql($dateEnd) . "', '%Y-%m-%d') GROUP BY ut.USER_ID), 0) as CHARGE_OFF,
			IFNULL((SELECT round(sum(ut.AMOUNT), 2) FROM appforsale_user_transact ut WHERE ut.USER_ID = ua.USER_ID and ut.DESCRIPTION = 'TARIFF_PAY' and ut.TRANSACT_DATE between STR_TO_DATE('" . $sqlHelper->forSql($dateFrom) . "', '%Y-%m-%d') and STR_TO_DATE('" . $sqlHelper->forSql($dateEnd) . "', '%Y-%m-%d') GROUP BY ut.USER_ID), 0) as TARIFF_PAY,
			IFNULL((SELECT ba.UF_SUMM FROM dbz_bonus_account ba WHERE ba.UF_USER = ua.USER_ID), 0) as BONUS_ACCOUNT
	FROM appforsale_user_account  ua
	LEFT JOIN appforsale_user_transact utTotal ON (utTotal.USER_ID = ua.USER_ID)
	LEFT JOIN b_user u ON (u.ID = ua.USER_ID)
	LEFT JOIN b_uts_user uts ON (u.ID = uts.VALUE_ID)
	LEFT JOIN b_iblock_element ib ON (ib.ID = uts.UF_CITY)
	WHERE 
	  uts.UF_CITY > 0
	  and u.LAST_LOGIN between STR_TO_DATE('" . $sqlHelper->forSql($dateFrom) . "', '%Y-%m-%d') and STR_TO_DATE('" . $sqlHelper->forSql($dateEnd) . "', '%Y-%m-%d')
	GROUP BY utTotal.USER_ID
";


// Города
$arCities = [];
$arSelect = array("ID", "NAME");
$arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[] = array(
		'text'  => $arCity["NAME"],
		'value' => $arCity["ID"],
	);
}


$arHeaders = [
	["key" => "user_id", "label" => "ID пользователя", "sortable" => true],
	["key" => "fio", "label" => "Пользователь", "sortable" => true],
	["key" => "phone", "label" => "Телефон", "sortable" => true],
	["key" => "auth_date", "label" => "Дата авторизации", "sortable" => true],
	["key" => "rating_idx", "label" => "Индкес эффективности", "sortable" => true],
	["key" => "city", "label" => "Город", "sortable" => true],
	["key" => "bonus_budget", "label" => "Бонусный баланс", "sortable" => true],
	["key" => "budget", "label" => "Сумма на счете", "sortable" => true],
	["key" => "transactions", "label" => "Транзакции", "sortable" => true],
	["key" => "charge_off", "label" => "Сумма пополнений", "sortable" => true],
	["key" => "tariff_pay", "label" => "На тарифы", "sortable" => true],
	["key" => "return_rating", "label" => "Рейтинг возвратов", "sortable" => true],
];

$arTotals    = [];
$recordset   = $connection->query($sql);
$arCityUsers = [];
while ($record = $recordset->fetch()) {
	$arTotals["summ"]       = $arTotals["summ"] + $record["CURRENT_BUDGET"];
	$arTotals["bonus"]      = $arTotals["bonus"] + $record["BONUS_ACCOUNT"];
	$arTotals["charge_off"] = $arTotals["charge_off"] + $record["CHARGE_OFF"];
	$arTotals["tariff_pay"] = $arTotals["tariff_pay"] + $record["TARIFF_PAY"];

	$rating_idx = 0;
	if ($record["TRANSACTIONS"] > 0) {
		$rating_idx = $record["CHARGE_OFF"] / $record["TRANSACTIONS"];
	}

	$arItems[] = [
		"user_id_num"   => $record["USER_ID"],
		"user_id"       => "<a href='/bitrix/admin/user_edit.php?ID=" . $record["USER_ID"] . "&lang=ru' target='_blank'>" . $record["USER_ID"] . "</a>",
		"fio"           => $record["FIO"],
		"phone"         => $record["PHONE"],
		"auth_date"     => $record["LAST_LOGIN"],
		"rating_idx"    => round($rating_idx, 4),
		"city"          => $record["CITY"],
		"city_id"       => $record["CITY_ID"],
		"bonus_budget"  => $record["BONUS_ACCOUNT"],
		"budget"        => $record["CURRENT_BUDGET"],
		"transactions"  => $record["TRANSACTIONS"],
		"charge_off"    => $record["CHARGE_OFF"],
		"tariff_pay"    => $record["TARIFF_PAY"],
		"return_rating" => $record["UF_RETURNS_PERCENT"] . "%",
	];
}


?>
<div id="reportTable">
	<b-row class="my-3 ">
		<b-col lg="6">
			<b-form class="" method="get" action="<?=POST_FORM_ACTION_URI?>">
				<b-form-group
						label="Период"
						label-cols-sm="3"
						label-align-sm="left"
						label-size="sm"
						label-for="filterInput"
						class="mb-0"
				>
					<b-input-group size="sm">
						<b-form-input placeholder="Период с " type="date" value="<?=$dateFrom?>" name="dateStart"></b-form-input>
						<b-form-input placeholder="Период по" type="date" value="<?=$dateEnd?>" name="dateEnd"></b-form-input>
						<b-input-group-append>
							<b-button type="submit">Сформировать</b-button>
						</b-input-group-append>
					</b-input-group>
				</b-form-group>
			</b-form>
		</b-col>
	</b-row>

	<b-row>
		<b-col lg="6" class="my-1">


			<b-form-group
					label="Фильтр"
					label-cols-sm="3"
					label-align-sm="left"
					label-size="sm"
					label-for="filterInput"
					class="mb-0"
			>
				<b-input-group size="sm">
					<b-form-input
							v-model="filter"
							type="search"
							id="filterInput"
							placeholder="Поиск по ФИО"
					></b-form-input>
					<b-form-select v-model="filterCity" :options="cities">
						<template #first>
							<b-form-select-option :value="null" disabled>-- Выберите город --</b-form-select-option>
						</template>
					</b-form-select>
					<b-input-group-append>
						<b-button :disabled="!filter" @click="filter = ''; filterCity = ''">Очистить</b-button>
					</b-input-group-append>
				</b-input-group>
			</b-form-group>
		</b-col>


		<b-pagination
				class="m-0"
				v-model="currentPage"
				:total-rows="rows"
				:per-page="perPage"
				aria-controls="my-table"
				first-number
				last-number
		></b-pagination>

	</b-row>

	<b-table
			class="table table-striped"
			:items="filteredItems"
			:fields="fields"
			:sort-by.sync="sortBy"
			:sort-desc.sync="sortDesc"
			responsive="sm"
			:per-page="perPage"
			:current-page="currentPage"
	>
		<template #cell(user_id)="data">
			<span v-html="data.value"></span>
		</template>

		<template v-slot:custom-foot>
			<tr>
				<th class="bg-dark text-white" colspan="6">Итоги:</th>
				<th class="bg-dark text-white">{{totals.bonus.toFixed(2)}}</th>
				<th class="bg-dark text-white">{{totals.summ.toFixed(2)}}</th>
				<th class="bg-dark text-white"></th>
				<th class="bg-dark text-white">{{totals.charge_off.toFixed(2)}}</th>
				<th class="bg-dark text-white">{{totals.tariff_pay.toFixed(2)}}</th>
				<th class="bg-dark text-white"></th>
			</tr>
		</template>
	</b-table>
</div>


<script>
	var reportTable = new Vue({
		el: '#reportTable',
		computed: {
			filteredItems() {
				let items = this.items;

				if (this.filter.length > 0 && items && items.length) {
					items = this.items.filter((item) => item.fio.toUpperCase().includes(this.filter.toUpperCase()) || item.user_id_num.includes(this.filter) || item.phone.includes(this.filter) )
				}

				if (this.filterCity && this.items && this.items.length) {
					items = items.filter((item) => item.city_id === this.filterCity)
				}

				this.rows = items.length
				return items;
			},

			totals() {
				return this.filteredItems.reduce(function (totals, current) {
					return {
						summ: totals.summ + parseFloat(current.budget),
						bonus: totals.bonus + parseFloat(current.bonus_budget),
						charge_off: totals.charge_off + parseFloat(current.charge_off),
						tariff_pay: totals.tariff_pay + parseFloat(current.tariff_pay)
					}
				}, {summ: 0, charge_off: 0, tariff_pay: 0, bonus: 0})
			}
		},
		data() {
			return {
				filter: "",
				filterCity: null,
				perPage: 100,
				currentPage: 1,
				sortBy: 'charge_off',
				sortDesc: true,
				rows: null,
				fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
				items: <?=CUtil::PhpToJSObject($arItems)?>,
				cities: <?=CUtil::PhpToJSObject($arCities)?>
			}
		},
	})
</script>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
