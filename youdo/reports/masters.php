<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};

\Bitrix\Main\Loader::includeModule("iblock");

//Профили
$arProfiles      = [];
$arUserIds       = [];
$arSelect        = array("ID", "NAME", "CREATED_BY");
$arFilterProfile = array("IBLOCK_ID" => 2, "ACTIVE" => "Y");
if ($_GET["city"]) {
	$arFilterProfile["PROPERTY_CITY"] = $_GET["city"];
}
$resProfile = CIBlockElement::GetList(array(), $arFilterProfile, false, array(), $arSelect);
while ($arProfile = $resProfile->GetNext()) {

	$arProfiles[ $arProfile["CREATED_BY"] ] = $arProfile;
	$arUserIds[]                            = $arProfile["CREATED_BY"];
}

//Пользователи
$arFilter = array("ID" => $arUserIds);
$rsUsers  = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
$arUsers  = [];
while ($arUser = $rsUsers->Fetch()) {
	$user                     = [];
	$user["LOGIN"]            = $arUser["LOGIN"];
	$user["LAST_LOGIN"]       = $arUser["LAST_LOGIN"];
	$arUsers[ $arUser["ID"] ] = $user;
}

// Заявки
$arSelect                     = array("ID", "NAME", "PROPERTY_PROFILE_ID", "PROPERTY_CITY", "PROPERTY_STATUS_ID");
$arFilter                     = array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "PROPERTY_PROFILE_ID" => $arUserIds);
$arFilter["DATE_MODIFY_FROM"] = $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
$arFilter["DATE_MODIFY_TO"]   = $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day'))));
$res                          = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

while ($arTask = $res->GetNext()) {
	$arProfiles[ $arTask["PROPERTY_PROFILE_ID_VALUE"] ]["TASKS"][ $arTask["PROPERTY_STATUS_ID_VALUE"] ][] = $arTask["ID"];
}

// Транзакции
$arTransactFilter = [
	"USER_ID"         => $arUserIds,
	"%DESCRIPTION"    => array("TARIFF_PAY", "Оплата заказа"),
	"DEBIT"           => "N",
	">=TRANSACT_DATE" => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days')))),
	"<=TRANSACT_DATE" => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day')))),

];
$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
while ($arTransaction = $res->GetNext()) {
	$arProfiles[ $arTransaction["USER_ID"] ]["TRANSACTIONS"]["ALL"][] = $arTransaction["AMOUNT"];
	if ($arTransaction["DESCRIPTION"] == "TARIFF_PAY") {
		$arProfiles[ $arTransaction["USER_ID"] ]["TRANSACTIONS"]["TARIFF"][] = $arTransaction["AMOUNT"];
	}
}

$arTransactFilter = [
	"USER_ID"         => $arUserIds,
	"%DESCRIPTION"    => array("OUT_CHARGE_OFF"),
	"DEBIT"           => "Y",
	">=TRANSACT_DATE" => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days')))),
	"<=TRANSACT_DATE" => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day')))),

];
$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
while ($arTransaction = $res->GetNext()) {
	if ($arTransaction["DESCRIPTION"] == "OUT_CHARGE_OFF") {
		$arProfiles[ $arTransaction["USER_ID"] ]["TRANSACTIONS"]["CHARGE_OFF"][] = $arTransaction["AMOUNT"];
	}
}


// Города
$arCities = [];
$arSelect = array("ID", "NAME");
$arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ] = $arCity["NAME"];
}


$arHeaders = [
	'index',
	["key" => "fio", "label" => "ФИО", "sortable" => true],
	["key" => "phone", "label" => "Телефон", "sortable" => true],
	["key" => "last_activity", "label" => "Последняя авторизация", "sortable" => true],
	["key" => "chargeOff", "label" => "Сумма пополнений", "sortable" => true],
	["key" => "Pworks", "label" => "Выполняется заявок", "sortable" => true],
	["key" => "Dworks", "label" => "Исполнено заявок", "sortable" => true],
//	["key" => "Fworks", "label" => "Закрыто для предлоежний заявок", "sortable" => true],
	["key" => "Nworks", "label" => "Всего заявок", "sortable" => true],
	["key" => "tariffSum", "label" => "На тарифы", "sortable" => true],
	["key" => "profileSum", "label" => "Всего потрачено", "sortable" => true],
];

$profileSum    = 0;
$tariffSum     = 0;
$chargeOffSumm = 0;

$totalN = 0;
$totalP = 0;
$totalD = 0;
$totalF = 0;
//
$totalProfileSum = 0;
$totalTariffSum  = 0;
$totalChargeOff  = 0;

$i = 1;
foreach ($arProfiles as $key => $arProfile):
	$tariffSum        = array_sum($arProfile["TRANSACTIONS"]["TARIFF"]);
	$profileSum       = array_sum($arProfile["TRANSACTIONS"]["ALL"]);
	$profileChargeOff = array_sum($arProfile["TRANSACTIONS"]["CHARGE_OFF"]);

	$sumP = count($arProfile['TASKS']["P"]);
	$sumD = count($arProfile['TASKS']["D"]);
	$sumF = count($arProfile['TASKS']["F"]);
	$sumN = $sumP + $sumD + $sumF;

	// Итоги

	$totalP = $totalP + $sumP;
	$totalD = $totalD + $sumD;
	$totalF = $totalF + $sumF;
	$totalN = $totalN + $sumN;

	$totalTariffSum  = $totalTariffSum + $tariffSum;
	$totalProfileSum = $profileSum + $totalProfileSum;
	$totalChargeOff  = $totalChargeOff + $profileChargeOff;


	$arItems[] = [
		"fio"           => $arProfile["NAME"],
		"phone"         => $arUsers[ $arProfile["CREATED_BY"] ]["LOGIN"],
		"last_activity" => $arUsers[ $arProfile["CREATED_BY"] ]["LAST_LOGIN"],
		"chargeOff"     => $profileChargeOff > 0 ? $profileChargeOff : 0,
		"Pworks"        => $sumP,
		"Dworks"        => $sumD,
		"Fworks"        => $sumF,
		"Nworks"        => $sumN,
		"tariffSum"     => $tariffSum > 0 ? round($tariffSum) : 0,
		"profileSum"    => $profileSum > 0 ? round($profileSum) : 0,
	];

	$i++;
endforeach;

$arTotals = [
	"totalChargeOff"  => $totalChargeOff,
	"totalP"          => $totalP,
	"totalD"          => $totalD,
	"totalN"          => $totalN,
	"totalTariffSum"  => round($totalTariffSum),
	"totalProfileSum" => round($totalProfileSum),
]

?>
	<div id="reportTable">
		<br>
		<form method="get" action="<?=POST_FORM_ACTION_URI?>">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Период с:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-7 days'))?>" name="dateStart">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Период по:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d', strtotime('+1 day'))?>" name="dateEnd">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Город</label>
						<select name="city" class="form-control">
							<option value="">Все</option>
							<? foreach ($arCities as $key => $arCity): ?>
								<option <? if ($_GET["city"] == $key): ?>selected<? endif; ?> value="<?=$key?>"><?=$arCity?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>

		<b-table
				class="table table-striped"
				:items="items"
				:fields="fields"
				:sort-by.sync="sortBy"
				:sort-desc.sync="sortDesc"
				:sort-compare="sortDate"
				responsive="sm"
		>

			<!-- A virtual column -->
			<template v-slot:cell(index)="data">
				{{ data.index + 1 }}
			</template>

			<template v-slot:custom-foot>
				<!-- You can customize this however you want, this is just as an example -->
				<tr>
					<th class="bg-dark text-white" colspan="4">Итоги:</th>
					<th class="bg-dark text-white" v-for="(total, key) in totals" :key="key">
						{{ total }}
					</th>
				</tr>
			</template>

		</b-table>


	</div>


	<script>
		var reportTable = new Vue({
			el: '#reportTable',
			data() {
				return {
					sortBy: 'last_activity',
					sortDesc: true,
					fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
					items: <?=CUtil::PhpToJSObject($arItems)?>,
					totals: <?=CUtil::PhpToJSObject($arTotals)?>
				}
			},

			methods: {
				sortDate(a, b, key) {
					if (key !== 'last_activity') {
						// `key` is not the field that is a date.
						// Let b-table handle the sorting for other columns
						// returning null or false will tell b-table to fall back to it's
						// internal sort compare routine for fields keys other than `myDateField`
						return null // or false
					}
					aDate = moment(a[key], 'DD.MM.YYYY hh:mm:ss')
					bDate = moment(b[key], 'DD.MM.YYYY hh:mm:ss')
					if (aDate.isValid && bDate.isValid) {
						if (aDate < bDate) {
							return -1
						} else if (aDate > bDate) {
							return 1
						} else {
							return 0
						}
					}
					return null
				}
			}
		})
	</script>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>