<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};
\Bitrix\Main\Loader::includeModule("iblock");


// Города
$arCities = [];
$arSelect = array("ID", "NAME");
$arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ]["NAME"] = $arCity["NAME"];
}

// Пользователи
$arFilter    = array("!UF_CITY" => -1);
$rsUsers     = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
$arCityUsers = [];
while ($arUser = $rsUsers->Fetch()) {
	$arCityUsers[ $arUser["UF_CITY"] ][] = $arUser["ID"];
}


//Log подписок
$arCityPayments = [];

foreach ($arCityUsers as $cityId => $arCityUser) {
	$entity_data_class = getHighloadEntity(2);

	$arSubsFilter      = array("UF_USER"   => $arCityUser);
	if ( ! empty($_GET["dateStart"])) {
		$arSubsFilter[">=UF_DATE"] = DateTime::createFromPhp(new \DateTime($_GET["dateStart"]));
	}

	if ( ! empty($_GET["dateEnd"])) {
		$arSubsFilter["<=UF_DATE"] = DateTime::createFromPhp(new \DateTime($_GET["dateEnd"]));
	}

	$rsData = $entity_data_class::getList(array(
		"select" => array("*"),
		"order"  => array("ID" => "ASC"),
		"filter" => $arSubsFilter
	));

	while ($arData = $rsData->Fetch()) {
		$arCityPayments[ $cityId ][] = $arData["UF_COST"];
	}
}

// Заявки
$arTasks  = [];
$arSelect = array("ID", "NAME", "PROPERTY_CITY", "PROPERTY_STATUS_ID");
$arFilter = array(
	"IBLOCK_ID" => 4,
	"ACTIVE"    => "Y",
);
if ( ! empty($_GET["dateStart"])) {
	$arFilter["DATE_MODIFY_FROM"] = DateTime::createFromPhp(new \DateTime($_GET["dateStart"]));
}

if ( ! empty($_GET["dateEnd"])) {
	$arFilter["DATE_MODIFY_TO"] = DateTime::createFromPhp(new \DateTime($_GET["dateEnd"]));
}


$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arTask = $res->GetNext()) {
	$arCities[ $arTask["PROPERTY_CITY_VALUE"] ][ $arTask["PROPERTY_STATUS_ID_VALUE"] ][] = $arTask["ID"];
}

?>

	<form method="get" action="<?=POST_FORM_ACTION_URI?>">
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label for="">Период с:</label>
					<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-7 days'))?>" name="dateStart">
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="">Период по:</label>
					<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d')?>" name="dateEnd">
				</div>
			</div>
			<div class="col">
				<button class="mt-4 btn btn-primary">Сформировать</button>
			</div>
		</div>

	</form>
	<br>

	<table class="table table-striped" width="100%" border="1" cellpadding="5" cellspacing="0">
		<tr align="center">
			<th>Город</th>
			<th>Открыто</th>
			<th>Выполняется</th>
			<th>Исполнено</th>
			<th>Закрыто для предложений</th>
			<th>Всего</th>
			<th>Сумма подписок</th>
		</tr>
		<?
		$totalN          = 0;
		$totalP          = 0;
		$totalD          = 0;
		$totalF          = 0;
		$totalSubscribes = 0
		?>
		<? foreach ($arCities as $key => $arCity): ?>
			<?
			$cityPayments = array_sum($arCityPayments[ $key ]);
			$sumN         = count($arCity["N"]);
			$sumP         = count($arCity["P"]);
			$sumD         = count($arCity["D"]);
			$sumF         = count($arCity["F"]);
			?>
			<tr>
				<td><?=$arCity["NAME"]?></td>
				<td align="right"><?=$sumN?> </td>
				<td align="right"><?=$sumP?></td>
				<td align="right"><?=$sumD?></td>
				<td align="right"><?=$sumF?></td>
				<td align="right"><?=$sumN + $sumP + $sumD + $sumF?></td>
				<td align="right"><?=$cityPayments?></td>
			</tr>
			<?
			$totalN          = $totalN + $sumN;
			$totalP          = $totalP + $sumP;
			$totalD          = $totalD + $sumD;
			$totalF          = $totalF + $sumF;
			$totalSubscribes = $totalSubscribes + $cityPayments;
			$total           = $total + ($sumN + $sumP + $sumD + $sumF);
			?>
		<? endforeach; ?>
		<tr align="right">
			<th>Итого:</th>
			<th><?=$totalN?></th>
			<th><?=$totalP?></th>
			<th><?=$totalD?></th>
			<th><?=$totalF?></th>
			<th><?=$total?></th>
			<th><?=$totalSubscribes?></th>
		</tr>

	</table>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>