<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};
\Bitrix\Main\Loader::includeModule("iblock");

$dateStart = $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('-7 days'))));
$dateEnd   = $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d', strtotime('+1 day'))));

//Направления
$rsSections = CIBlockSection::GetList(array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID), false, array("UF_FEE_PRICE"));
$arSections = [];
while ($arSection = $rsSections->GetNext()) {
	if ($arSection['IBLOCK_SECTION_ID']) {
		continue;
	}

	$arItem                         = array();
	$arItem['ID']                   = $arSection['ID'];
	$arItem['NAME']                 = $arSection['NAME'];
	$arItem['IBLOCK_SECTION_ID']    = $arSection['IBLOCK_SECTION_ID'];
	$arSections[ $arSection['ID'] ] = $arItem;

}


// Пользователи по направлениям
$arDirectionUsers = [];
$arSelect         = array("ID", "NAME", "CREATED_BY");
$arFilter         = array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $_GET["direction"]);
$res              = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arProfile = $res->GetNext()) {
	$arDirectionUsers[] = $arProfile["CREATED_BY"];
}

// Города
$arCities = [];
$arSelect = array("ID", "NAME");
$arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ] = $arCity["NAME"];
}


// Заявки
$arTasks                      = [];
$arSelect                     = array("ID", "NAME", "PROPERTY_CITY", "PROPERTY_STATUS_ID");
$arFilter                     = array("IBLOCK_ID" => 4, "ACTIVE" => "Y", "SECTION_ID" => $_GET["direction"] ? $_GET["direction"] : array_keys($arSections));
$arFilter["DATE_MODIFY_FROM"] = $dateStart;
$arFilter["DATE_MODIFY_TO"]   = $dateEnd;
$res                          = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while ($arTask = $res->GetNext()) {
	$arTasks[ $arTask["PROPERTY_CITY_VALUE"] ][ $arTask["PROPERTY_STATUS_ID_VALUE"] ][] = $arTask["ID"];
}

// Пользователи
$arFilter = array("!UF_CITY" => -1);
if (isset($_GET["direction"]) && strlen($_GET["direction"]) > 0) {
	$arFilter["ID"] = implode('|', $arDirectionUsers);
}

$rsUsers     = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
$arUsers     = [];
$arCityUsers = [];
while ($arUser = $rsUsers->Fetch()) {
	$user                                = [];
	$user["FIO"]                         = $arUser["LAST_NAME"] . ' ' . $arUser["NAME"] . '  ' . $arUser["SECOND_NAME"];
	$user["LAST_LOGIN"]                  = $arUser["LAST_LOGIN"];
	$user["DATE_REGISTER"]               = $arUser["DATE_REGISTER"];
	$arUsers[ $arUser["UF_CITY"] ][]     = $user;
	$arCityUsers[ $arUser["UF_CITY"] ][] = $arUser["ID"];
}


//Log
$arCityChargeOff    = [];
$arCityTransactions = [];
$arCityTariffBuy    = [];
$arCityComissionBuy = [];
$arMasterTariffs    = [];
foreach ($arCityUsers as $cityId => $arCityUser) {


	$arTransactFilter = [
		"USER_ID"         => $arCityUser,
		"%DESCRIPTION"    => array("TARIFF_PAY", "Оплата заказа", "OUT_CHARGE_OFF"),
		"DEBIT"           => "N",
		">=TRANSACT_DATE" => $dateStart,
		"<=TRANSACT_DATE" => $dateEnd,

	];
	$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
	while ($arTransaction = $res->GetNext()) {
		$arCityTransaction[ $cityId ][] = $arTransaction["AMOUNT"];
		if ($arTransaction["DESCRIPTION"] == "TARIFF_PAY") {
			$arCityTariffBuy[ $cityId ][] = $arTransaction["AMOUNT"];
		} else {
			$arCityComissionBuy[ $cityId ][] = $arTransaction["AMOUNT"];
		}
	}

	$arTransactFilter = [
		"USER_ID"         => $arCityUser,
		"%DESCRIPTION"    => array("OUT_CHARGE_OFF"),
		"DEBIT"           => "Y",
		">=TRANSACT_DATE" => $dateStart,
		"<=TRANSACT_DATE" => $dateEnd,

	];
	$res              = CAppforsaleUserTransact::GetList(array(), $arTransactFilter, false, false, array("DESCRIPTION", "AMOUNT", "TRANSACT_DATE", "USER_ID"));
	while ($arTransaction = $res->GetNext()) {
		if ($arTransaction["DESCRIPTION"] == "OUT_CHARGE_OFF") {
			$arCityChargeOff[ $cityId ][] = $arTransaction["AMOUNT"];
		}
	}

	//Тарифы мастеров
	$entity_data_class = getHighloadEntityByName("DBZUserTariffs");
	$rsData            = $entity_data_class::getList(array(
		"select" => array("*"),
		"filter" => array("UF_ACTIVE" => 1, "UF_USER" => $arCityUser),
	));

	while ($arData = $rsData->Fetch()) {
		$arMasterTariffs[ $cityId ]["tariff_{$arData["UF_TARIFF_TYPE"]}"][] = 1;
	}
}

//Тарифы
$entity_data_class = getHighloadEntityByName("DBZTariffs");
$rsData            = $entity_data_class::getList(array(
	"select" => array("*"),
));

$arTariffs = [];
while ($arData = $rsData->Fetch()) {
	$arTariffs[] = $arData;
}


$arReport = [];

foreach ($arCities as $key => $arCity) {
	$report                = [];
	$report["CITY"]        = $arCity;
	$report["CITY_ID"]     = $key;
	$report["TOTAL_USERS"] = count($arUsers[ $key ]);

	$report["ACTIVE_USERS"]      = count(array_filter($arUsers[ $key ], function($user) use ($dateEnd, $dateStart) {
		$dateStart = strtotime($dateStart);
		$dateEnd   = strtotime($dateEnd);

		return (MakeTimeStamp($user["LAST_LOGIN"]) >= $dateStart) && (MakeTimeStamp($user["LAST_LOGIN"]) <= $dateEnd);
	}));
	$report["REGSITER_IN_MONTH"] = count(array_filter($arUsers[ $key ], function($user) use ($dateStart, $dateEnd) {
		$dateStart = strtotime($dateStart);
		$dateEnd   = strtotime($dateEnd);

		return (MakeTimeStamp($user["DATE_REGISTER"]) >= $dateStart) && (MakeTimeStamp($user["DATE_REGISTER"]) <= $dateEnd);
	}));

	$arReport[] = $report;
}

//Принятые заявки
$entity_data_class = getHighloadEntityByName("DBZTaskAcceptLog");
$rsData            = $entity_data_class::getList(array(
	"select"  => array("UF_CITY", "CNT"),
	"filter"  => array(
		"UF_BY_TARIFF"      => 1,
		">=UF_DATE_SERVICE" => $dateStart,
		"<=UF_DATE_SERVICE" => $dateEnd,
	),
	'runtime' => [
		new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)')
	],
	'group'   => ['UF_CITY']
));

$arStandartAccept = [];
while ($arData = $rsData->Fetch()) {
	$arStandartAccept[ $arData["UF_CITY"] ] = $arData["CNT"];
}


uasort($arReport, function($first, $second) {
	if ($first["TOTAL_USERS"] == $second["TOTAL_USERS"]) {
		return 0;
	}

	return ($first["TOTAL_USERS"] < $second["TOTAL_USERS"]) ? 1 : -1;
});

$arHeaders = [
	["key" => "city", "label" => "Город", "sortable" => true],
	["key" => "cityIndex", "label" => "Индекс города", "sortable" => true],
	["key" => "userCount", "label" => "Количество пользователей", "sortable" => true],
	["key" => "activeUsers", "label" => "Активные", "sortable" => true],
	["key" => "registerInPeriod", "label" => "Регистраций за период", "sortable" => true],
	["key" => "chargeOff", "label" => "Сумма пополнений", "sortable" => true],
	["key" => "tariffSum", "label" => "На тарифы", "sortable" => true],
	["key" => "Nworks", "label" => "Открыто заявок", "sortable" => true],
	["key" => "allWorks", "label" => "Всего заявок", "sortable" => true],
	["key" => "standartAcceptCount", "label" => "Принято по Стандарту", "sortable" => true],
	["key" => "totalTariffs", "label" => "Всего активных тарифов", "sortable" => true]
];

foreach ($arTariffs as $arTariff) {
	if ($arTariff["ID"] == 1) {
		continue;
	}
	$arHeaders[] = ["key" => "tariff_{$arTariff["ID"]}", "label" => "Тариф {$arTariff["UF_NAME"]}", "sortable" => true];
}



$totalUsersCount = 0;
$activeSum       = 0;
$registerSum     = 0;
$chargeOffSumm   = 0;
$cityBuySum      = 0;
$tariffSum       = 0;
$comissionSum    = 0;
$usersSum        = 0;

$totalN  = 0;
$totalP  = 0;
$totalD  = 0;
$totalF  = 0;
$totalE  = 0;
$arItems = [];

$totalCityBuySum          = 0;
$totalTariffSum           = 0;
$totalComissionSum        = 0;
$totalTariffsCount        = [];
$totalStandartAcceptCount = 0;
$totalTotalTariffsCount   = 0;

foreach ($arReport as $key => $arCity):
	$cityChargeOff = array_sum($arCityChargeOff[ $arCity["CITY_ID"] ]);
	$cityBuySum    = array_sum($arCityTransaction[ $arCity["CITY_ID"] ]);
	$tariffSum     = array_sum($arCityTariffBuy[ $arCity["CITY_ID"] ]);
	$comissionSum  = array_sum($arCityComissionBuy[ $arCity["CITY_ID"] ]);

	$sumN = count($arTasks[ $arCity["CITY_ID"] ]["N"]);
	$sumP = count($arTasks[ $arCity["CITY_ID"] ]["P"]);
	$sumD = count($arTasks[ $arCity["CITY_ID"] ]["D"]);
	$sumF = count($arTasks[ $arCity["CITY_ID"] ]["F"]);
	$sumE = count($arTasks[ $arCity["CITY_ID"] ]["E"]);

	// Итоги
	$totalUsersCount = $totalUsersCount + $arCity["TOTAL_USERS"];
	$activeSum       = $activeSum + $arCity["ACTIVE_USERS"];
	$registerSum     = $registerSum + $arCity["REGSITER_IN_MONTH"];
	$chargeOffSumm   = $chargeOffSumm + $cityChargeOff;
	$totalN          = $totalN + $sumN;
	$totalP          = $totalP + $sumP;
	$totalD          = $totalD + $sumD;
	$totalF          = $totalF + $sumF;
	$totalE          = $totalE + $sumE;
	$total           = $total + ($sumN + $sumP + $sumD + $sumF);

	$totalCityBuySum   = $totalCityBuySum + $cityBuySum;
	$totalTariffSum    = $totalTariffSum + $tariffSum;
	$totalComissionSum = $totalComissionSum + $comissionSum;

	$cityIndex = 0;
	if ($sumN + $sumP + $sumD + $sumF > 0) {
		$cityIndex = round($cityChargeOff / ($sumN + $sumP + $sumD + $sumF));
	}

	$standartAcceptCount      = $arStandartAccept[ $arCity["CITY_ID"] ] ?? 0;
	$totalStandartAcceptCount = $totalStandartAcceptCount + $standartAcceptCount;

	$arTariffFields = [];
	$totalTariffs   = 0;
	foreach ($arTariffs as $arTariff) {
		if ($arTariff["ID"] == 1) {
			continue;
		}

		$tariffCount                                   = count($arMasterTariffs[ $arCity["CITY_ID"] ]["tariff_{$arTariff["ID"]}"]);
		$arTariffFields["tariff_{$arTariff["ID"]}"]    = $tariffCount;
		$totalTariffsCount["tariff_{$arTariff["ID"]}"] = intval($totalTariffsCount["tariff_{$arTariff["ID"]}"]) + $tariffCount;
		$totalTariffs                                  = $totalTariffs + $tariffCount;
	}
	$totalTotalTariffsCount = $totalTotalTariffsCount + $totalTariffs;


	$arItem = [
		"city"                => $arCity["CITY"],
		"cityIndex"           => $cityIndex,
		"userCount"           => $arCity["TOTAL_USERS"],
		"activeUsers"         => $arCity["ACTIVE_USERS"],
		"registerInPeriod"    => $arCity["REGSITER_IN_MONTH"],
		"chargeOff"           => $cityChargeOff > 0 ? round($cityChargeOff) : 0,
		"cityBuySum"          => $cityBuySum > 0 ? round($cityBuySum) : 0,
		"tariffSum"           => $tariffSum > 0 ? round($tariffSum) : 0,
		"comissionSum"        => $comissionSum > 0 ? round($comissionSum) : 0,
		"Pworks"              => $sumP,
		"Dworks"              => $sumD,
		"Fworks"              => $sumF,
		"Eworks"              => $sumE,
		"Nworks"              => $sumN,
		"allWorks"            => $sumN + $sumP + $sumD + $sumF,
		"standartAcceptCount" => $standartAcceptCount,
		"totalTariffs"        => $totalTariffs,
	];

	$arItems[] = array_merge($arItem, $arTariffFields);

endforeach;

$arTotals = [
	"totalUsersCount"               => $totalUsersCount,
	"activeSum"                     => $activeSum,
	"registerSum"                   => $registerSum,
	"chargeOffSumm"                 => round($chargeOffSumm),
	"totalTariffSum"                => round($totalTariffSum),
	"totalN"                        => $totalN,
	"total"                         => $total,
	"totalStandartAcceptCount"      => $totalStandartAcceptCount,
	"totalTotalStandartAcceptCount" => $totalTotalTariffsCount,
];
foreach ($arTariffs as $arTariff) {
	if ($arTariff["ID"] == 1) {
		continue;
	}
	$arTotals["tariff_{$arTariff["ID"]}"] = $totalTariffsCount["tariff_{$arTariff["ID"]}"];
}

?>
	<div id="reportTable">
		<br>
		<form method="get" action="<?=POST_FORM_ACTION_URI?>">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Период с:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d', strtotime('-7 days'))?>" name="dateStart">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Период по:</label>
						<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d', strtotime('+1 day'))?>" name="dateEnd">
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="">Направление:</label>
						<select name="direction" class="form-control">
							<option value="">Все</option>
							<? foreach ($arSections as $arSection): ?>
								<option <? if ($_GET["direction"] == $arSection["ID"]): ?>selected<? endif; ?> value="<?=$arSection["ID"]?>"><?=$arSection["NAME"]?></option>
							<? endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>

		<b-table
				class="table table-striped"
				:items="items"
				:fields="fields"
				:sort-by.sync="sortBy"
				:sort-desc.sync="sortDesc"
				responsive="sm"
		>
			<template #thead-top>
				<tr>
					<th class="bg-dark text-white" colspan="2">Итоги:</th>
					<th class="bg-dark text-white" v-for="(total, key) in totals" :key="key">
						{{ total }}
					</th>
				</tr>
			</template>
			<template v-slot:custom-foot>
				<!-- You can customize this however you want, this is just as an example -->
				<tr>
					<th class="bg-dark text-white" colspan="2">Итоги:</th>
					<th class="bg-dark text-white" v-for="(total, key) in totals" :key="key">
						{{ total }}
					</th>
				</tr>
			</template>

		</b-table>


	</div>


	<script>
		var reportTable = new Vue({
			el: '#reportTable',
			data() {
				return {
					sortBy: 'userCount',
					sortDesc: true,
					fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
					items: <?=CUtil::PhpToJSObject($arItems)?>,
					totals: <?=CUtil::PhpToJSObject($arTotals)?>
				}
			}
		})
	</script>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>