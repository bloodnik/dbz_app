<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>

<?

use Bitrix\Main\Type\DateTime;

global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};
\Bitrix\Main\Loader::includeModule("iblock");


// Города
$arCities = [];
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y");
$res      = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while ($arCity = $res->GetNext()) {
	$arCities[ $arCity["ID"] ] = $arCity["NAME"];
}


// Пользователи
if (isset($_GET["direction"]) && strlen($_GET["direction"]) > 0) {
	$arFilter["ID"] = implode('|', $arDirectionUsers);
}
if (isset($_GET["city"]) && strlen($_GET["city"]) > 0) {
	$arFilter["UF_CITY"] = $_GET["city"];
}

$rsUsers     = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, array("SELECT" => array("UF_CITY")));
$arCityUsers = [];
while ($arUser = $rsUsers->Fetch()) {
	$arCityUsers[ $arUser["ID"] ] = $arUser["LAST_NAME"] . ' ' . $arUser["NAME"] . ' ' . $arUser["SECOND_NAME"];
}


//Направления
$rsSections = CIBlockSection::GetList(Array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID), false, array("UF_FEE_PRICE"));
$arSections = [];
while ($arSection = $rsSections->GetNext()) {
	if ($arSection['IBLOCK_SECTION_ID']) {
		continue;
	}
	$arSections[ $arSection['ID'] ] = $arSection['NAME'];

}

//Log
$entity_data_class = getHighloadEntity(2);
$arFilter          = array(
	"UF_USER"       => array_keys($arCityUsers),
	">=UF_DATE"     => $_GET["dateStart"] ? DateTime::createFromPhp(new \DateTime($_GET["dateStart"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d'))),
	"<=UF_DATE"     => $_GET["dateEnd"] ? DateTime::createFromPhp(new \DateTime($_GET["dateEnd"])) : DateTime::createFromPhp(new \DateTime(date('Y-m-d'))),
	"UF_DIRECTIONS" => $_GET["direction"] ? $_GET["direction"] : array_keys($arSections)
);

$rsData            = $entity_data_class::getList(array(
	"select" => array("*"),
	"order"  => array("ID" => "ASC"),
	"filter" => $arFilter
));
$arLogs            = [];
while ($arData = $rsData->Fetch()) {
	$arLogs[] = $arData;
}

//
//
//// Пользователи по направлениям
//$arDirectionUsers = [];
//$arSelect         = Array("ID", "NAME", "CREATED_BY");
//$arFilter         = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $_GET["direction"]);
//$res              = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
//while ($arProfile = $res->GetNext()) {
//	$arDirectionUsers[] = $arProfile["CREATED_BY"];
//}
//


//$arReport = [];
//
//foreach ($arCities as $key => $arCity) {
//	$report                 = [];
//	$report["CITY"]         = $arCity;
//	$report["TOTAL_USERS"]  = count($arUsers[ $key ]);
//	$report["ACTIVE_USERS"] = count(array_filter($arUsers[ $key ], function($user) {
//		return MakeTimeStamp($user["LAST_LOGIN"]) >= strtotime('-3 days');
//	}));
//
//	$arReport[] = $report;
//}
//
//uasort($arReport, function($first, $second) {
//	if ($first["TOTAL_USERS"] == $second["TOTAL_USERS"]) {
//		return 0;
//	}
//
//	return ($first["TOTAL_USERS"] < $second["TOTAL_USERS"]) ? 1 : -1;
//});
?>
	<br>
	<form method="get" action="<?=POST_FORM_ACTION_URI?>">
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label for="">Период с:</label>
					<input class="form-control" type="date" value="<?=$_GET["dateStart"] ? $_GET["dateStart"] : date('Y-m-d')?>" name="dateStart">
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="">Период по:</label>
					<input class="form-control" type="date" value="<?=$_GET["dateEnd"] ? $_GET["dateEnd"] : date('Y-m-d')?>" name="dateEnd">
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="">Направление</label>
					<select name="direction" class="form-control">
						<option value="">Все</option>
						<? foreach ($arSections as $key => $arSection): ?>
							<option <? if ($_GET["direction"] == $key): ?>selected<? endif; ?> value="<?=$key?>"><?=$arSection?></option>
						<? endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label for="">Город</label>
					<select name="city" class="form-control">
						<option value="">Все</option>
						<? foreach ($arCities as $key => $arCity): ?>
							<option <? if ($_GET["city"] == $key): ?>selected<? endif; ?> value="<?=$key?>"><?=$arCity?></option>
						<? endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</div>

	</form>

	<table class="table table-striped" width="100%" border="1" cellpadding="5" cellspacing="0">
		<tr align="center">
			<th>№</th>
			<th>Пользователь</th>
			<th>Дата</th>
			<th>Количество дней</th>
			<th>Стоимость подписки</th>
			<th>Направления деятельности</th>
		</tr>
		<?
		$totalDays = 0;
		$totalSum  = 0;
		?>
		<? foreach ($arLogs as $key => $arLog): ?>
			<tr>
				<td><?=$key + 1?></td>
				<td><?=$arCityUsers[ $arLog["UF_USER"] ]?></td>
				<td><?=$arLog["UF_DATE"]?></td>
				<td align="right"><?=$arLog["UF_DAYS"]?> </td>
				<td align="right"><?=$arLog["UF_COST"]?></td>
				<td>
					<ul class="list-unstyled">
						<? foreach ($arLog["UF_DIRECTIONS"] as $direction): ?>
							<li><?=$arSections[ $direction ]?></li>
						<? endforeach; ?>
					</ul>
				</td>
			</tr>
			<?
			$totalDays = $totalDays + $arLog["UF_DAYS"];
			$totalSum  = $totalSum + $arLog["UF_COST"];
			?>
		<? endforeach; ?>
		<tr align="right">
			<th>Итого:</th>
			<th></th>
			<th></th>
			<th><?=$totalDays?></th>
			<th><?=$totalSum?></th>
			<th></th>
		</tr>

	</table>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>