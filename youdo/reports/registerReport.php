<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Type\DateTime; ?>
<?
global $USER;
if ( ! $USER->IsAdmin()) {
	ShowError("Необходима авторизация");

	return;
};

$connection = Bitrix\Main\Application::getConnection();
$sqlHelper  = $connection->getSqlHelper();

$year = htmlspecialchars(isset($_GET["year"])) ? $_GET["year"] : date("Y");

$sql = "
SELECT uts.UF_CITY city_id, ib.NAME city_name, DATE_FORMAT(u.DATE_REGISTER, '%m') month_register, count(u.ID) count 
	FROM b_user u 
	LEFT JOIN b_uts_user uts ON (u.ID = uts.VALUE_ID)
	LEFT JOIN b_iblock_element ib ON (ib.ID = uts.UF_CITY) 
	WHERE DATE_REGISTER >= STR_TO_DATE('01.01." . $sqlHelper->forSql($year, 4) . "', '%d.%m.%Y')
	 AND DATE_REGISTER <= STR_TO_DATE('31.12." . $sqlHelper->forSql($year, 4) . "', '%d.%m.%Y')
	 AND uts.UF_CITY > 0
 	GROUP BY uts.UF_CITY, ib.NAME, DATE_FORMAT(u.DATE_REGISTER, '%m')
";


$sql2 = "
	SELECT uts.UF_CITY city_id, ib.NAME city_name,  DATE_FORMAT(t.TRANSACT_DATE, '%m') month_register, description,  round(sum(t.AMOUNT)) total
	FROM appforsale_user_transact t 	
	LEFT JOIN b_uts_user uts ON (t.USER_ID = uts.VALUE_ID)
	LEFT JOIN b_iblock_element ib ON (ib.ID = uts.UF_CITY) 
	WHERE TRANSACT_DATE >= STR_TO_DATE('01.01." . $sqlHelper->forSql($year, 4) . "', '%d.%m.%Y')
	 AND TRANSACT_DATE <= STR_TO_DATE('31.12." . $sqlHelper->forSql($year, 4) . "', '%d.%m.%Y')
	 AND (DESCRIPTION = 'OUT_CHARGE_OFF' OR DESCRIPTION = 'TARIFF_PAY')
	 AND uts.UF_CITY > 0
	GROUP BY uts.UF_CITY, ib.NAME, DESCRIPTION, DATE_FORMAT(t.TRANSACT_DATE, '%m')
";

$recordset   = $connection->query($sql2);
$arTransacts = [];
while ($record = $recordset->fetch()) {
	$arTransacts[ $record["city_name"] ][ $record["description"] ][ intval($record["month_register"]) ] = $record["total"];
}

$recordset   = $connection->query($sql);
$arCityUsers = [];
while ($record = $recordset->fetch()) {
	$arCityUsers[ $record["city_name"] ][ intval($record["month_register"]) ] = $record["count"];
}

foreach ($arCityUsers as $key => $arCityUser) {

	$r = -2;
	$p = -1;
	$t = 0;
	for ($i = 1; $i <= 12; $i++) {
		$chargeOff = $arTransacts[ $key ]["OUT_CHARGE_OFF"][ $i ] ?? 0;
		$tariff    = $arTransacts[ $key ]["TARIFF_PAY"][ $i ] ?? 0;

		$r = $r + 3;
		$p = $p + 3;
		$t = $t + 3;

		$arCityUsers[ $key ][ $r ] = $arCityUser[ $i ] ?? 0;
		$arCityUsers[ $key ][ $p ] = $chargeOff ?? 0;
		$arCityUsers[ $key ][ $t ] = $tariff ?? 0;
	}

	$arCityUsers[ $key ][0] = $key;
	ksort($arCityUsers[ $key ]);
}

//PR($arCityUsers);

$arHeaders = [
	["key" => "city", "label" => "Город", "sortable" => true],
	["key" => "total_r", "label" => "Итого регистраций", "sortable" => true],
	["key" => "jan_r", "label" => "Январь рег", "sortable" => true],
	["key" => "jan_p", "label" => "Январь пополн", "sortable" => true],
	["key" => "jan_t", "label" => "Январь тариф", "sortable" => true],
	["key" => "feb_r", "label" => "Февраль рег", "sortable" => true],
	["key" => "feb_p", "label" => "Февраль пополн", "sortable" => true],
	["key" => "feb_t", "label" => "Февраль тариф", "sortable" => true],
	["key" => "mar_r", "label" => "Март рег", "sortable" => true],
	["key" => "mar_p", "label" => "Март пополн", "sortable" => true],
	["key" => "mar_t", "label" => "Март тариф", "sortable" => true],
	["key" => "apr_r", "label" => "Апрель рег", "sortable" => true],
	["key" => "apr_p", "label" => "Апрель пополн", "sortable" => true],
	["key" => "apr_t", "label" => "Апрель тариф", "sortable" => true],
	["key" => "may_r", "label" => "Май рег", "sortable" => true],
	["key" => "may_p", "label" => "Май пополн", "sortable" => true],
	["key" => "may_t", "label" => "Май тариф", "sortable" => true],
	["key" => "jun_r", "label" => "Июнь рег", "sortable" => true],
	["key" => "jun_p", "label" => "Июнь пополн", "sortable" => true],
	["key" => "jun_t", "label" => "Июнь тариф", "sortable" => true],
	["key" => "jul_r", "label" => "Июль рег", "sortable" => true],
	["key" => "jul_p", "label" => "Июль пополн", "sortable" => true],
	["key" => "jul_t", "label" => "Июль тариф", "sortable" => true],
	["key" => "aug_r", "label" => "Август рег", "sortable" => true],
	["key" => "aug_p", "label" => "Август пополн", "sortable" => true],
	["key" => "aug_t", "label" => "Август тариф", "sortable" => true],
	["key" => "sep_r", "label" => "Сентябрь рег", "sortable" => true],
	["key" => "sep_p", "label" => "Сентябрь пополн", "sortable" => true],
	["key" => "sep_t", "label" => "Сентябрь тариф", "sortable" => true],
	["key" => "oct_r", "label" => "Октябрь рег", "sortable" => true],
	["key" => "oct_p", "label" => "Октябрь пополн", "sortable" => true],
	["key" => "oct_t", "label" => "Октябрь тариф", "sortable" => true],
	["key" => "nov_r", "label" => "Ноябрь рег", "sortable" => true],
	["key" => "nov_p", "label" => "Ноябрь пополн", "sortable" => true],
	["key" => "nov_t", "label" => "Ноябрь тариф", "sortable" => true],
	["key" => "dec_r", "label" => "Декабрь рег", "sortable" => true],
	["key" => "dec_p", "label" => "Декабрь пополн", "sortable" => true],
	["key" => "dec_t", "label" => "Декабрь тариф", "sortable" => true],
];

$arTotals = [];
foreach ($arCityUsers as $arCityUser) {

	$total_r = array_intersect_key($arCityUser, array_flip(array(1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34)));

	$arTotals["alltotal_r"] = $arTotals["alltotal_r"] + array_sum($total_r);
	$arTotals["totaljan_r"] = $arTotals["totaljan_r"] + $arCityUser[1];
	$arTotals["totaljan_p"] = $arTotals["totaljan_p"] + $arCityUser[2];
	$arTotals["totaljan_t"] = $arTotals["totaljan_t"] + $arCityUser[3];
	$arTotals["totalfeb_r"] = $arTotals["totalfeb_r"] + $arCityUser[4];
	$arTotals["totalfeb_p"] = $arTotals["totalfeb_p"] + $arCityUser[5];
	$arTotals["totalfeb_t"] = $arTotals["totalfeb_t"] + $arCityUser[6];
	$arTotals["totalmar_r"] = $arTotals["totalmar_r"] + $arCityUser[7];
	$arTotals["totalmar_p"] = $arTotals["totalmar_p"] + $arCityUser[8];
	$arTotals["totalmar_t"] = $arTotals["totalmar_t"] + $arCityUser[9];
	$arTotals["totalapr_r"] = $arTotals["totalapr_r"] + $arCityUser[10];
	$arTotals["totalapr_p"] = $arTotals["totalapr_p"] + $arCityUser[11];
	$arTotals["totalapr_t"] = $arTotals["totalapr_t"] + $arCityUser[12];
	$arTotals["totalmay_r"] = $arTotals["totalmay_r"] + $arCityUser[13];
	$arTotals["totalmay_p"] = $arTotals["totalmay_p"] + $arCityUser[14];
	$arTotals["totalmay_t"] = $arTotals["totalmay_t"] + $arCityUser[15];
	$arTotals["totaljun_r"] = $arTotals["totaljun_r"] + $arCityUser[16];
	$arTotals["totaljun_p"] = $arTotals["totaljun_p"] + $arCityUser[17];
	$arTotals["totaljun_t"] = $arTotals["totaljun_t"] + $arCityUser[18];
	$arTotals["totaljul_r"] = $arTotals["totaljul_r"] + $arCityUser[19];
	$arTotals["totaljul_p"] = $arTotals["totaljul_p"] + $arCityUser[20];
	$arTotals["totaljul_t"] = $arTotals["totaljul_t"] + $arCityUser[21];
	$arTotals["totalaug_r"] = $arTotals["totalaug_r"] + $arCityUser[22];
	$arTotals["totalaug_p"] = $arTotals["totalaug_p"] + $arCityUser[23];
	$arTotals["totalaug_t"] = $arTotals["totalaug_t"] + $arCityUser[24];
	$arTotals["totalsep_r"] = $arTotals["totalsep_r"] + $arCityUser[25];
	$arTotals["totalsep_p"] = $arTotals["totalsep_p"] + $arCityUser[26];
	$arTotals["totalsep_t"] = $arTotals["totalsep_t"] + $arCityUser[27];
	$arTotals["totaloct_r"] = $arTotals["totaloct_r"] + $arCityUser[28];
	$arTotals["totaloct_p"] = $arTotals["totaloct_p"] + $arCityUser[29];
	$arTotals["totaloct_t"] = $arTotals["totaloct_t"] + $arCityUser[30];
	$arTotals["totalnov_r"] = $arTotals["totalnov_r"] + $arCityUser[31];
	$arTotals["totalnov_p"] = $arTotals["totalnov_p"] + $arCityUser[32];
	$arTotals["totalnov_t"] = $arTotals["totalnov_t"] + $arCityUser[33];
	$arTotals["totaldec_r"] = $arTotals["totaldec_r"] + $arCityUser[34];
	$arTotals["totaldec_p"] = $arTotals["totaldec_p"] + $arCityUser[35];
	$arTotals["totaldec_t"] = $arTotals["totaldec_t"] + $arCityUser[36];


	$arItems[] = [
		"city"    => $arCityUser[0],
		"total_r" => array_sum($total_r),
		"jan_r"   => $arCityUser[1],
		"jan_p"   => $arCityUser[2],
		"jan_t"   => $arCityUser[3],
		"feb_r"   => $arCityUser[4],
		"feb_p"   => $arCityUser[5],
		"feb_t"   => $arCityUser[6],
		"mar_r"   => $arCityUser[7],
		"mar_p"   => $arCityUser[8],
		"mar_t"   => $arCityUser[9],
		"apr_r"   => $arCityUser[10],
		"apr_p"   => $arCityUser[11],
		"apr_t"   => $arCityUser[12],
		"may_r"   => $arCityUser[13],
		"may_p"   => $arCityUser[14],
		"may_t"   => $arCityUser[15],
		"jun_r"   => $arCityUser[16],
		"jun_p"   => $arCityUser[17],
		"jun_t"   => $arCityUser[18],
		"jul_r"   => $arCityUser[19],
		"jul_p"   => $arCityUser[20],
		"jul_t"   => $arCityUser[21],
		"aug_r"   => $arCityUser[22],
		"aug_p"   => $arCityUser[23],
		"aug_t"   => $arCityUser[24],
		"sep_r"   => $arCityUser[25],
		"sep_p"   => $arCityUser[26],
		"sep_t"   => $arCityUser[27],
		"oct_r"   => $arCityUser[28],
		"oct_p"   => $arCityUser[29],
		"oct_t"   => $arCityUser[30],
		"nov_r"   => $arCityUser[31],
		"nov_p"   => $arCityUser[32],
		"nov_t"   => $arCityUser[33],
		"dec_r"   => $arCityUser[34],
		"dec_p"   => $arCityUser[35],
		"dec_t"   => $arCityUser[36],
	];
}


?>
	<div id="reportTable">
		<br>
		<form method="get" action="<?=POST_FORM_ACTION_URI?>">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Год:</label>
						<select class="form-control" name="year" id="year">
							<option value="2018" <? if ($year == 2018): ?> selected <? endif; ?>>2018</option>
							<option value="2019" <? if ($year == 2019): ?> selected <? endif; ?>>2019</option>
							<option value="2020" <? if ($year == 2020): ?> selected <? endif; ?>>2020</option>
							<option value="2021" <? if ($year == 2021): ?> selected <? endif; ?>>2021</option>
							<option value="2022" <? if ($year == 2022): ?> selected <? endif; ?>>2022</option>
						</select>
					</div>
				</div>
				<div class="col">
					<button class="mt-4 btn btn-primary">Сформировать</button>
				</div>
			</div>
		</form>
		<b-table
				class="table table-striped"
				:items="items"
				:fields="fields"
				:sort-by.sync="sortBy"
				:sort-desc.sync="sortDesc"
				responsive="sm"
		>
			<template v-slot:custom-foot>
				<!-- You can customize this however you want, this is just as an example -->
				<tr>
					<th class="bg-dark text-white">Итоги:</th>
					<th class="bg-dark text-white" v-for="(total, key) in totals" :key="key">
						{{ total }}
					</th>
				</tr>
			</template>
		</b-table>
	</div>


	<script>
		var reportTable = new Vue({
			el: '#reportTable',
			data() {
				return {
					sortBy: 'userCount',
					sortDesc: true,
					fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
					items: <?=CUtil::PhpToJSObject($arItems)?>,
					totals: <?=CUtil::PhpToJSObject($arTotals)?>
				}
			}
		})
	</script>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>