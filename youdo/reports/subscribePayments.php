<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>
<?
global $USER;
if ( ! $USER->IsAuthorized()) {
	ShowError("Необходима авторизация");

	return;
};


$arSort   = array('SORT' => 'ASC', 'ID' => 'DESC');
$arFilter = array('ACTIVE' => 'Y', 'DESCRIPTION' => 'SUBSCRIPTION_PAY');
$arSelect = array('*');

$rsTransactions = CAppforsaleUserTransact::GetList($arSort, $arFilter, false, false, $arSelect);

$arUsers         = [];
$arMonthPayments = [];
$currentDayPayments = [];
while ($arTransaction = $rsTransactions->Fetch()) {
	// Платежи по датам
	$monthDate = new DateTime($arTransaction["TRANSACT_DATE"]); // текущая транзакции
	$curDay    = $monthDate->format('j');
	if ($curDay != 1) {
		$monthDate->modify(sprintf('-%d day', $curDay - 1)); // начало месяца
	}
	$arMonthPayments[ $monthDate->format("d.m.Y") ][] = $arTransaction['AMOUNT'];

	// Текущие сутки
	if(ConvertDateTime($arTransaction["TRANSACT_DATE"], "d.m.Y") == date("d.m.Y")) {
		$currentDayPayments[] = $arTransaction['AMOUNT'];
	}

	$arUsers[ $arTransaction['USER_ID'] ]['FIO']        = $arTransaction['USER_NAME'] . ' ' . $arTransaction['USER_LAST_NAME'];
	$arUsers[ $arTransaction['USER_ID'] ]['PAYMENTS'][] = $arTransaction['AMOUNT'];
}


foreach ($arUsers as $key => $arItem) {
	$arUsers[ $key ]['SUM'] = array_sum($arItem['PAYMENTS']);
}

$arSum = array_map(function($value) {
	return $value['SUM'];
}, $arUsers);

?>
	<br>

	<table class="table table-striped" width="100%" border="1" cellpadding="5" cellspacing="0">
		<tr>
			<th>#</th>
			<th>Имя</th>
			<th>Сумма</th>
		</tr>
		<? foreach ($arUsers as $key => $arUser): ?>
			<tr>
				<td><?=$key?></td>
				<td><?=$arUser['FIO']?></td>
				<td align="right"><?=$arUser['SUM']?></td>
			</tr>
		<? endforeach; ?>
		<tr style=font-weight:bold;">
			<td>Итого</td>
			<td align="right" colspan="2"><?=array_sum($arSum)?></td>
		</tr>
	</table>

	<table class="table table-striped" width="100%">
		<tr>
			<th>Месяц</th>
			<th>Сумма</th>
		</tr>
		<? foreach ($arMonthPayments as $key => $monthPayment): ?>
			<tr>
				<td><?=$key?></td>
				<td><?=array_sum($monthPayment)?></td>
			</tr>
		<? endforeach; ?>
		<tr>
			<td>Текущие сутки</td>
			<td ><?=array_sum($currentDayPayments)?></td>
		</tr>
	</table>



<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>