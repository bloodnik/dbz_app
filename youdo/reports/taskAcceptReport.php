<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>
<?

\Bitrix\Main\Loader::includeModule("iblock");

$connection = Bitrix\Main\Application::getConnection();
$sqlHelper  = $connection->getSqlHelper();

$sql = "
SELECT DATE_FORMAT(l.UF_DATE_SERVICE, '%Y-%m-01') mtn, DATE_FORMAT(l.UF_DATE, '%H:00') time, DATE_FORMAT(l.UF_DATE_SERVICE, '%d.%m.%Y') day, DATE_FORMAT(l.UF_DATE_SERVICE, '%a %d.%m.%Y') dayName, l.ID  FROM dbz_task_accept_log l
WHERE l.UF_DATE_SERVICE between STR_TO_DATE('01.12.2020', '%d.%m.%Y') and STR_TO_DATE('30.12.2020', '%d.%m.%Y') 
ORDER BY l.UF_DATE, time
";


$recordset = $connection->query($sql);
$arReport  = [];
while ($record = $recordset->fetch()) {
	$arReport[ $record["day"] ][ $record["time"] ][] = $record["ID"];
}

$arBody = [];
$arHeaders = [
	["key" => "time", "label" => "Время", "sortable" => false],
];
foreach ($arReport as $day => $days) {
	$arHeaders[] = ["key" => $day, "label" => $day, "align" => "center", "sortable" => false];
	foreach ($days as $time => $items) {
		$arBody[ $time ][ $day ] = count($items);
	}
}
$arHeaders[] = ["key" => "total", "label" => "Итого", "align" => "center", "sortable" => false];

ksort($arBody);
$arItems  = [];
$arTotals = [];
foreach ($arBody as $time => $arItem) {
	$item         = [];
	$item["time"] = $time;
	$timeTotal    = 0;
	foreach ($arItem as $day => $count) {
		$item[ $day ] = $count;
		$timeTotal    = $timeTotal + $count;

		$arTotals[ $day ] = $arTotals[ $day ] + $count;
	}
	$item["total"]        = $timeTotal;
	$arTotals["allTotal"] = $arTotals["allTotal"] + $timeTotal;
	$arItems[]            = $item;
}

ksort($arTotals);
?>


	<div id="reportTable">
		<br>
		<div style="width: 100%; overflow-y: scroll ">
			<b-table
					class="table table-striped"
					:items="items"
					:fields="fields"
					responsive="sm"
					bordered
					tbody-tr-class="text-center"
			>

				<template v-slot:custom-foot>
					<!-- You can customize this however you want, this is just as an example -->
					<tr>
						<th class="bg-dark text-white">Итоги:</th>
						<th class="bg-dark text-white text-center" v-for="(total, key) in totals" :key="key">
							{{ total }}
						</th>
					</tr>
				</template>
			</b-table>

		</div>
	</div>


	<script>
		var reportTable = new Vue({
			el: '#reportTable',
			data() {
				return {
					fields: <?=CUtil::PhpToJSObject($arHeaders)?>,
					items: <?=CUtil::PhpToJSObject($arItems)?>,
					totals: <?=CUtil::PhpToJSObject($arTotals)?>
				}
			}
		})
	</script>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>