<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?$APPLICATION->IncludeComponent(
	"mlab:appforsale.subscription", 
	".default", 
	array(
		"IBLOCK_ID" => "2",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "profiles",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/youdo/subscription/",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"form" => "#ELEMENT_ID#/",
		)
	),
	false
);?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>