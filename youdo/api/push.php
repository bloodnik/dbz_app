<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if ( ! empty($_GET['text'])) {

	CModule::IncludeModule("iblock");
	CModule::IncludeModule("mlab.appforsale");

	$el      = new CIBlockElement;
	$newpush = Array(
		"MODIFIED_BY"       => $USER->GetID(),
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"         => 10,
		"NAME"              => $_GET['text'],
		"ACTIVE"            => "Y",
	);
	$el->Add($newpush);

	/*$arSelect = Array("CREATED_BY");
	$arFilter = Array("IBLOCK_ID"=>2);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNext())
		{
			$USERS[] = $ob['CREATED_BY'];
		}	
	*/

	global $USER;
	$filter  = Array("!GROUPS_ID" => Array(5), "ACTIVE" => 'Y');

	if(! empty($_GET['city'])){
		$filter["UF_CITY"] = $_GET['city'];
	}

	$rsUsers = CUser::GetList(($by = "id"), ($order = "asc"), $filter);
	while ($arUser = $rsUsers->Fetch()) {
		$USERS[] = $arUser['ID'];
	}

	global $push_id;
	\CAppforsale::SendMessage(
		array_unique($USERS),
		array(
			'body'         => $_GET['text'],
			'icon'         => 'ic_stat_name',
			'click_action' => 'appforsale.exec'
		),
		array(
			'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/'
		)
	);

}
?>