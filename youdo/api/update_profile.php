<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//ajax
//if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
//     || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	return;
//}

if ( ! check_bitrix_sessid()) {
	return;
}

$request = Application::getInstance()->getContext()->getRequest();
$server  = Application::getInstance()->getContext()->getServer();

$iblockId = 2;

// Profile
$PROFILE_WORK = htmlspecialcharsbx($request->getPost('PROFILE_WORK'));

$arResult = array(
	'hasError' => true,
	'msg'      => "Ошибка отправки сообщения",
);

global $USER;

if ( ! $USER->IsAuthorized()) {
	$arResult['msg'] = "Необходимо авторизоваться";
	echo json_encode($arResult);
	die();
}


if ( ! $request->isPost()
     || ! ($PROFILE_WORK)
     || ! Loader::includeModule('iblock')
) {

	if ( ! ($PROFILE_WORK)) {
		$arResult['msg'] = "ОШИБКА: Заполните все обязательные поля.";
	}

	echo json_encode($arResult);
	die();
}


// Проверка на существование профиля
$arFilterProfile = Array("IBLOCK_ID" => $iblockId, "CREATED_BY" => $USER->GetId());
$arSelect        = Array("ID");
$res             = CIBlockElement::GetList(Array(), $arFilterProfile, false, false, $arSelect);
if ($arProfile = $res->GetNext()) {

	if ($arProfile["ID"] > 0) {

		// Обновление профиля профиля
		try {
			CIBlockElement::SetPropertyValuesEx($arProfile["ID"], $iblockId, array('PROFILE_WORK' => explode(",", $PROFILE_WORK)));
			$arResult['hasError'] = false;
			$arResult['msg']      = "Изменения сохранены";
		} catch (\Bitrix\Main\SystemException $exception) {
			$arResult['msg'] = $el->LAST_ERROR;
			echo json_encode($arResult);
			die();
		}
	}
}


$arResult = array(
	'hasError' => false,
	'msg'      => "Спасибо! Изменения сохранены",
);

echo json_encode($arResult);
die();


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

