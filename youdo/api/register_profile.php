<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PROFILE_IBLOCK_ID", 2);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if ( ! check_bitrix_sessid()) {
	return;
}

$request = Application::getInstance()->getContext()->getRequest();
$server  = Application::getInstance()->getContext()->getServer();

$LAST_NAME   = htmlspecialcharsbx($request->getPost('LAST_NAME'));
$NAME        = htmlspecialcharsbx($request->getPost('NAME'));
$SECOND_NAME = htmlspecialcharsbx($request->getPost('SECOND_NAME'));
$BIRTHDAY    = htmlspecialcharsbx($request->getPost('BIRTHDAY'));
$CITY         = htmlspecialcharsbx($request->getPost('CITY'));
$ADDRESS      = htmlspecialcharsbx($request->getPost('ADDRESS'));
$EMAIL        = htmlspecialcharsbx($request->getPost('EMAIL'));
$PROFILE_WORK = htmlspecialcharsbx($request->getPost('PROFILE_WORK'));

$arResult = array(
	'hasError' => true,
	'msg'      => "Ошибка отправки сообщения",
);

global $USER;

if ( ! $USER->IsAuthorized()) {
	$arResult['msg'] = "Необходимо авторизоваться";
	echo json_encode($arResult);
	die();
}


if ( ! $request->isPost()
     || ! ($LAST_NAME && $NAME && $BIRTHDAY && $CITY && $ADDRESS && $PROFILE_WORK)
     || ! Loader::includeModule('iblock')
) {

	if ( ! ($LAST_NAME && $NAME && $BIRTHDAY && $CITY && $ADDRESS && $PROFILE_WORK)) {
		$arResult['msg'] = "ОШИБКА: Заполните все обязательные поля.";
	}


	echo json_encode($arResult);
	die();
}

$arPROPS = array(
	'LAST_NAME'     => $LAST_NAME,
	'NAME'          => $NAME,
	'SECOND_NAME'   => $SECOND_NAME,
	'BIRTHDAY'      => $BIRTHDAY,
	'CITY'          => $CITY,
	'ADDRESS'       => $ADDRESS,
	'EMAIL'         => $EMAIL,
	'PROFILE_WORK'  => explode(",", $PROFILE_WORK)
);

$arFields = array(
	"NAME"              => $LAST_NAME . " " . $NAME . " " . $SECOND_NAME,
	"IBLOCK_SECTION_ID" => 478,
	"IBLOCK_ID"         => PROFILE_IBLOCK_ID,
	'ACTIVE'            => 'N',
	'PROPERTY_VALUES'   => $arPROPS,
);


// Проверка на существование профиля
$arItems = Bitrix\Iblock\Elements\ElementProfileTable::getList(array(
	'select' => array('ID', "BLACK_LIST_" => "BLACK_LIST"),
	'filter' => array('IBLOCK_ID' => PROFILE_IBLOCK_ID, "CREATED_BY" => $USER->GetID()),
	'limit'  => 1,
))->fetchAll();


$el     = new \CIBlockElement();
$itemId = null;

if (count($arItems) > 0) {
	//	Обновление профиля
	$arItem = array_shift($arItems);

	// Оставляем в черном списке, если до этого был отмечен
	if(!empty($arItem["BLACK_LIST_VALUE"])) {
		$arFields['PROPERTY_VALUES']["BLACK_LIST"] = array("VALUE" => $arItem["BLACK_LIST_VALUE"]);
	}

	$arFields['MODIFIED_BY'] = $USER->GetID();
	if ($el->Update($arItem["ID"], $arFields)) {
		$itemId               = $arItem["ID"];
		$arResult['hasError'] = false;
		$arResult['msg']      = "Профиль отправлен на модерацию";
	} else {
		$arResult['msg'] = $el->LAST_ERROR;
		echo json_encode($arResult);
		die();
	}
} else {
	// Добавление профиля
	if ($itemId = $el->Add($arFields)) {
		$arResult['hasError'] = false;
		$arResult['msg']      = "Профиль отправлен на модерацию";
	}

	if ( ! $itemId) {
		$arResult['msg'] = $el->LAST_ERROR;
		echo json_encode($arResult);
		die();
	}
}

$arResult = array(
	'hasError' => false,
	'msg'      => "Спасибо! Ваш профиль отправлен на модерацию",
);

echo json_encode($arResult);
die();


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

