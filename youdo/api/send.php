<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

//if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
//     || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	AddMessage2Log("Ошибка попытка прямого вызова скрипта send.api. USER_ID: " . $USER->GetID(), "AJAX_ERROR");
//
//	return;
//}


CModule::IncludeModule("iblock");
CModule::IncludeModule("mlab.appforsale");

$TASK_ID = $_GET['task'];
$USER_ID = $_GET['user'];
$FREE    = $_GET['free'];

$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();

if ( ! $USER->IsAuthorized()) {
	AddMessage2Log("Ошибка попытка вызова скрипта send.api без авторизации", "AUTH_ERROR");

	return;
}

$userTariff     = new CAppforsaleTariff($USER->GetID());
$isTakeByTariff = ($userTariff->isPackageTariff() || $userTariff->isUnlimTariff()) && ! ! $FREE;
$hasFirstTask   = CAppforsaleProfile::hasFirstTask();

if ( ! $userTariff->isTariffActive() && ! $hasFirstTask) {
	echo 'no_tariff';
	die();
}

$dbPropsFree = CIBlockElement::GetProperty(
	4,
	$TASK_ID,
	array(),
	array(
		'CODE' => 'COST'
	)
);

$offer_price = 0;
if ($arPropsFree = $dbPropsFree->Fetch()) {

	if ($arPropsFree['VALUE'] == 0 || $isTakeByTariff) {
		$free = true;
	} else {
		$free        = false;
		$offer_price = $arPropsFree['VALUE'];
		if ($hasFirstTask) {
			$offer_price = $offer_price / 2;
		}
	}
}


$bSuccess = true;
$DB->StartTransaction();
try {
	$dbPropsExecutor = CIBlockElement::GetProperty(4, $TASK_ID, array(), array('CODE' => 'PROFILE_ID'));

	if ($arPropsExecutor = $dbPropsExecutor->Fetch()) {
		if ( ! empty($arPropsExecutor['VALUE'])) {
			throw new Exception('executor');
		}
	}

	if ($free == false) {
		$arUserAccount = \CAppforsaleUserAccount::GetByUserID($USER->GetID());
		if ($arUserAccount) {
			$currentBudget = (float) $arUserAccount['CURRENT_BUDGET'];
		}

		$bonus      = new CAppforsaleTaskBonusAccount($USER->GetID());
		$bonusSumm  = $bonus->calcWriteOfSumm($offer_price);
		$need_price = (float) $offer_price - $bonusSumm;

		if ($currentBudget >= $need_price) {
			\CAppforsaleUserAccount::Pay($USER_ID, $offer_price, $TASK_ID);
			$done = true;
		} else {
			throw new Exception('false');
		}
	} else {
		$done = true;
	}
	if ($done == true) {

		$arFieldsOffer = array(
			"ACTIVE"          => "Y",
			"IBLOCK_ID"       => 3,
			"NAME"            => "Отклик на задание " . $TASK_ID,
			"PROPERTY_VALUES" => array(
				"TASK_ID" => $TASK_ID,
			)
		);
		$oElementOffer = new CIBlockElement();
		$oElementOffer->Add($arFieldsOffer);

		$dbPropsExecutor = CIBlockElement::GetProperty(4, $TASK_ID, array(), array('CODE' => 'PROFILE_ID'));
		if ($arPropsExecutor = $dbPropsExecutor->Fetch()) {
			if ( ! empty($arPropsExecutor['VALUE'])) {
				throw new Exception('executor');
			}
		}

		if( !isDealAvailableInCloud($TASK_ID) ) {
			echo 'executor';
			die();
		}

		try {
			CIBlockElement::SetPropertyValuesEx($TASK_ID, false, array(
				'PROFILE_ID'     => $USER_ID,
				'STATUS_ID'      => 'P',
				'EXECUTOR_NAME'  => $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'],
				"NO_COMISSION"   => array(
					"VALUE" => $isTakeByTariff ? 35 : false
				),
				"HAS_PROBLEM"    => array("VALUE" => false),
				"PROBLEM_TYPE"    => "",
				"WAIT_COMISSION" => array(
					"VALUE" => $isTakeByTariff || $hasFirstTask ? false : 33
				),
				"FIRST_TASK"     => array(
					"VALUE" => $hasFirstTask ? 37 : false
				),
			));

			// Чтобы изменить дату изменения
			$el = new CIBlockElement();
			$el->Update($TASK_ID, array('TIMESTAMP_X' => true));
			CIBlock::clearIblockTagCache(4);


			//Смена стадии сделки на "Заявка в работе"
			// 24 - В работе по тарифу | 8 - Заявка в работе
			changeDealStage($TASK_ID, $isTakeByTariff ? '25' : '8');


			if ($isTakeByTariff && $arPropsFree['VALUE'] > 0) {
				$userTariff->decrementTaskCount();

				// Пересчет рейтинга возвратов
				$rating = new CAppforsaleReturnsRating($USER->GetID());
				$rating->setReturnsRating();
			}
			if ($hasFirstTask) {
				CAppforsaleProfile::setFirstTask($USER->GetID(), 0);
			}

			// Добавляем запись в лог принятий заявок
			try {
				$taskAcceptLog = new CAppforsaleTaskAcceptLog($USER->GetID(), $TASK_ID, $hasFirstTask ? 1 : 0);
				$taskAcceptLog->add();
			} catch (Exception $e) {

			}
			echo 'true';
		} catch (Exception $e) {
			AddMessage2Log("Ошибка принятия заявки № {$TASK_ID} Исполнителем {$arUser['LAST_NAME']} {$arUser['NAME']}. \n Ошибка {$e->getMessage()} ", "WORK_ERROR");
		}

	} else {
		throw new Exception('false');
	}

	$DB->Commit();
} catch (Exception $ex) {
	$DB->Rollback();
	$bSuccess = false;
	echo $ex->getMessage();
	AddMessage2Log("Ошибка " . $ex->getMessage(), "SEND.PHP_ERROR");
}
?>