<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PARTNERS_IBLOCK_ID", 2);
define("CITIES_IBLOCK_ID", 1);
define("TASK_IBLOCK_ID", 4);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//ajax
//if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
//     || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	return;
//}

$request = Application::getInstance()->getContext()->getRequest();

$requestType = $request->get('type');
if (isset($requestType) && strlen($requestType) > 0) {
	Loader::includeModule('iblock');

	// Список партнеров
	if ($requestType === 'partners') {
		$sectionsId = $request->get('sectionId');
		$cityId     = $request->get('cityId');
		$partners   = [];
		if (isset($sectionsId) && strlen($sectionsId) > 0) {

			$arSelect1 = array("*");
			$arFilter1 = array("IBLOCK_ID" => PARTNERS_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_PROFILE_WORK" => $sectionsId, "PROPERTY_CITY" => $cityId);
			$res       = CIBlockElement::GetList(array(), $arFilter1, false, array(), $arSelect1);
			while ($ob = $res->GetNextElement()) {
				$arFields            = $ob->GetFields();
				$arItem['ID']        = $arFields['ID'];
				$arItem['NAME']      = $arFields['NAME'];
				$arItem['IS_ONLINE'] = false; //CUser::IsOnLine($arFields['CREATED_BY']) ? true : false;
				$partners[]          = $arItem;
			}
		}
		echo json_encode($partners);
	}

	if ($requestType === 'partners2') {
		$sectionsId = $request->get('sectionId');
		$cityId     = $request->get('cityId');
		$partners   = [];
		if (isset($sectionsId) && strlen($sectionsId) > 0) {

			$sections = explode(",", $sectionsId);
			//$unlimUsers = CAppforsaleTariff::getUnlimUsersByCity($cityId);

			$arSelect1 = array("*");
			$arFilter1 = array(
				"IBLOCK_ID"             => PARTNERS_IBLOCK_ID,
				"ACTIVE"                => "Y",
				"PROPERTY_PROFILE_WORK" => $sections,
				"PROPERTY_CITY"         => $cityId,
				"PROPERTY_BLACK_LIST"   => false
			);
			$res       = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter1, false, array(), $arSelect1);
			while ($ob = $res->GetNextElement()) {
				$arFields            = $ob->GetFields();
				$arItem['ID']        = $arFields['ID'];
				$arItem['NAME']      = $arFields['NAME'];
				$arItem['IS_ONLINE'] = false; //CUser::IsOnLine($arFields['CREATED_BY']) ? true : false;

				$partners[] = $arItem;
			}
		}
		echo json_encode($partners);
	}

	// Список городов
	if ($requestType === 'cities') {
		function getCities() {
			$cities    = [];
			$arSelect1 = array("*");
			$arFilter1 = array("IBLOCK_ID" => CITIES_IBLOCK_ID, "ACTIVE" => "Y");
			$res       = CIBlockElement::GetList(array(), $arFilter1, false, array(), $arSelect1);
			while ($ob = $res->GetNextElement()) {
				$arFields       = $ob->GetFields();
				$arItem['ID']   = $arFields['ID'];
				$arItem['NAME'] = $arFields['NAME'];
				$cities[]       = $arItem;
			}

			return $cities;
		}

		$cachedCities = returnResultCache(604800, 'dbzCitiesCacheId', 'getCities');

		echo json_encode($cachedCities);
	}

	// Список видов деятельности
	if ($requestType === 'direction') {
		function getDirections() {
			$rsSections = CIBlockSection::GetList(array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID, "ACTIVE" => "Y"), false, array("UF_FEE_PRICE"));
			$arSections = [];
			while ($arSection = $rsSections->GetNext()) {

				// Проверяем, платный ли родитель
				$paydParent = false;
				if ( ! empty($arSection['IBLOCK_SECTION_ID'])) {
					$rsParentSections = CIBlockSection::GetList(array("left_margin" => "asc"), array("IBLOCK_ID" => TASK_IBLOCK_ID, "ID" => $arSection['IBLOCK_SECTION_ID'], "ACTIVE" => "Y"), false, array("ID", "UF_FEE_PRICE"));
					while ($arParentSection = $rsParentSections->GetNext()) {
						$paydParent = intval($arParentSection['UF_FEE_PRICE']) > 0;
					}
				}

				$arItem                      = array();
				$arItem['ID']                = $arSection['ID'];
				$arItem['NAME']              = $arSection['NAME'];
				$arItem['IBLOCK_SECTION_ID'] = $arSection['IBLOCK_SECTION_ID'];
				$arItem['CHILDREN']          = [];
				$arItem['FEE_PRICE']         = intval($arSection['UF_FEE_PRICE']);
				$arItem['IS_PAYD']           = intval($arSection['UF_FEE_PRICE']) > 0 || $paydParent;
				$arSections[]                = $arItem;

			}

			return $arSections;
		}

		$arCachedDirections = returnResultCache(1, 'DBZDirectionsId6', 'getDirections');

		echo json_encode($arCachedDirections);
	}
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

