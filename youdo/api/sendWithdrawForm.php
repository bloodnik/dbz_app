<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//ajax
//if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($_SERVER['HTTP_X_REQUESTED_WITH'])
//     || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	return;
//}


$request = Application::getInstance()->getContext()->getRequest();
$server  = Application::getInstance()->getContext()->getServer();


// Profile
$EMAIL = htmlspecialcharsbx($request->get('EMAIL'));

$arResult = array(
	'hasError' => true,
	'msg'      => "Ошибка отправки сообщения",
);

global $USER;

if ( ! $USER->IsAuthorized()) {
	$arResult['msg'] = "Необходимо авторизоваться";
	echo json_encode($arResult);
	die();
}


if ( ! ($EMAIL)
) {

	$arResult['msg'] = "ОШИБКА: Заполните все обязательные поля.";

	echo json_encode($arResult);
	die();
}
$arEventFields = array(
	"EMAIL_TO" => $EMAIL
);

CEvent::Send("WITHDRAW_FORM", "s1", $arEventFields);

$arResult = array(
	'hasError' => false,
	'msg'      => "Форма отправлена на почту",
);

echo json_encode($arResult);
die();


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

