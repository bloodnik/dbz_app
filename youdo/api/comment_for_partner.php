<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");
CModule::IncludeModule("mlab.appforsale");

$TASK_ID = $_GET['task'];
$PROFILE = $_GET['profile'];
$COMMENT = $_GET['comment'];

CIBlockElement::SetPropertyValuesEx($TASK_ID, false, array('COMMENT_FOR_PARTNER' => $COMMENT));

$arSelect = Array('*', 'PROPERTY_*');
$arFilter = Array("IBLOCK_ID" => 4, "ID" => $TASK_ID);
$dbTask   = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
if ($arTask = $dbTask->GetNextElement()) {
	$arFieldsTask = $arTask->GetFields();
	$arPropsTask  = $arTask->GetProperties();

	global $push_id;
	$push_id = 4;
	\CAppforsale::SendMessage(
		array((int) $PROFILE),
		array(
			'body'         => 'Заявка "' . $arFieldsTask['NAME'] . '" не принята',
			'click_action' => 'appforsale.exec'
		),
		array(
			'url' => (\CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/youdo/tasks-executor/' . $arFieldsTask['DETAIL_PAGE_URL']
		)
	);
}
?>