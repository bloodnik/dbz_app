<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");
CModule::IncludeModule("mlab.appforsale");

$bSuccess = true;

$TASK_ID = $_GET['task'];
$USER_ID = $_GET['user'];

$DB->StartTransaction();

$arSelect = Array('IBLOCK_SECTION_ID', 'PROPERTY_PROFILE_ID');
$arFilter = Array("IBLOCK_ID" => 4, "ID" => $TASK_ID);
$dbTask   = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
if ($arTask = $dbTask->GetNext()) {
	$SECTION_ID = $arTask['IBLOCK_SECTION_ID'];
	if ( ! empty($arTask['PROPERTY_PROFILE_ID_VALUE'])) {
		$DB->Rollback();
		die('executor');
	}
}

$dbSection = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => 4, 'ID' => $SECTION_ID), false, array('UF_*'));
while ($arSection = $dbSection->GetNext()) {
	$offer_price = 0;
	if ($arSection['UF_OFFER_PRICE'] > 0) {
		$offer_price = $arSection['UF_OFFER_PRICE'];
	}
}

$dbPropsFree = CIBlockElement::GetProperty(4, $TASK_ID, array(), array('CODE' => 'FREE'));
if ($arPropsFree = $dbPropsFree->Fetch()) {
	if ($arPropsFree['VALUE_XML_ID'] == 'y') {
		$free = true;
	} else {
		$free = false;
	}
}

if ($free == false) {
	$arUserAccount = \CAppforsaleUserAccount::GetByUserID($USER->GetID());
	if ($arUserAccount) {
		$currentBudget = (float) $arUserAccount['CURRENT_BUDGET'];
	}
	if ($currentBudget >= $offer_price) {
		\CAppforsaleUserAccount::Pay($USER_ID, $offer_price, 'PAY_ORDER', $TASK_ID, false);
		$done = true;
	} else {
		$done = false;
	}
} else {
	$done = true;
}

if ($done == true) {
	$arFieldsOffer = array(
		"ACTIVE"          => "Y",
		"IBLOCK_ID"       => 3,
		"NAME"            => "Отклик на задание " . $TASK_ID,
		"PROPERTY_VALUES" => array(
			"TASK_ID" => $TASK_ID,
		)
	);
	$oElementOffer = new CIBlockElement();
	$oElementOffer->Add($arFieldsOffer);

	CIBlockElement::SetPropertyValuesEx($TASK_ID, false, array('PROFILE_ID' => $USER_ID, 'STATUS_ID' => 'P'));
	CIBlock::clearIblockTagCache(4);
	echo 'true';
	$DB->Commit();
} else {
	echo 'false';
	$DB->Rollback();
}
?>