<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.02.2019
 * Time: 15:48
 * Project: dombezzabot_dev
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
define("PARTNERS_IBLOCK_ID", 2);
define("CITIES_IBLOCK_ID", 1);
define("TASK_IBLOCK_ID", 4);

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$request = Application::getInstance()->getContext()->getRequest();

$token = $request->get('token');
$type  = $request->get('type');
if (isset($token) && strlen($token) > 0) {

	$phone        = $request->get('ph');
	$responseData = [];

	// Список партнеров
	if ($token === "migrateToNewApp2021") {

		if ($type == 'checkUser') {
			global $USER;
			$rsUser = $USER->GetByLogin($phone);
			if ($arUser = $rsUser->Fetch()) {
				// Баланс мастера
				$budget = CAppforsaleUserAccount::getCurrentUserBudget($arUser["ID"]);

				// Бонусный баланс
				$obBonus = new CAppforsaleTaskBonusAccount($arUser["ID"]);
				$arBonus = $obBonus->getAccount();

				$responseData = [
					"NAME" => $arUser["NAME"],
					"LAST_NAME" => $arUser["LAST_NAME"],
					"SECOND_NAME" => $arUser["SECOND_NAME"],
					"BUDGET" => $budget,
					"BONUS"  => round(floatval($arBonus["UF_SUMM"]), 2),
				];

				// Тариф
				$obTariff = new CAppforsaleTariff($arUser["ID"]);
				$arTariff = $obTariff->isTariffActive() ? $obTariff->getUserTariff() : false;

				$responseData["TARIFF"] = $arTariff ? [
					"NAME"       => $arTariff["TARIFF_NAME"],
					"TASK_COUNT" => $arTariff["UF_TASK_COUNT"],
					"ACTIVE_TO"  => $arTariff["UF_ACTIVE_TO"] ? $arTariff["UF_ACTIVE_TO"]->toString() : ''
				] : [];
			}
			echo json_encode($responseData);
		}

		if ($type == 'deactivateUser') {

			global $USER;
			$rsUser = $USER->GetByLogin($phone);
			if ($arUser = $rsUser->Fetch()) {
				// Баланс мастера
				$budget = CAppforsaleUserAccount::getCurrentUserBudget($arUser["ID"]);

				// Бонусный баланс
				$obBonus = new CAppforsaleTaskBonusAccount($arUser["ID"]);
				$arBonus = $obBonus->getAccount();

				$responseData = [
					"NAME" => $arUser["NAME"],
					"LAST_NAME" => $arUser["LAST_NAME"],
					"SECOND_NAME" => $arUser["SECOND_NAME"],
					"BUDGET" => $budget,
					"BONUS"  => round(floatval($arBonus["UF_SUMM"]), 2),
				];

				// Тариф
				$obTariff = new CAppforsaleTariff($arUser["ID"]);
				$arTariff = $obTariff->isTariffActive() ? $obTariff->getUserTariff() : false;

				$responseData["TARIFF"] = $arTariff ? [
					"NAME"       => $arTariff["TARIFF_NAME"],
					"TASK_COUNT" => $arTariff["UF_TASK_COUNT"],
					"ACTIVE_TO"  => $arTariff["UF_ACTIVE_TO"] ? $arTariff["UF_ACTIVE_TO"]->toString() : '',
				] : [];

				$tmpUser = new CUser;
				$tmpUser->Update($arUser["ID"], [
					"UF_DEACTIVATED"      => 1,
					"ADMIN_NOTES" => "Автоблокировка",
				]);


				Loader::includeModule('iblock');

				$arFilterProfile = array("IBLOCK_ID" => 2, "CREATED_BY" => $arUser["ID"]);
				$arSelect        = array("ID");
				$res             = CIBlockElement::GetList(array(), $arFilterProfile, false, false, $arSelect);
				if ($arProfile = $res->GetNext()) {
					if ($arProfile["ID"] > 0) {
						// Обновление профиля профиля
						try {
							CIBlockElement::SetPropertyValuesEx($arProfile["ID"], $iblockId, array('DEACTIVATED' => array("VALUE" => 38)));
						} catch (\Bitrix\Main\SystemException $exception) {
						}
					}
				}

			}

			echo json_encode($responseData);
		}


	}

}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

