<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('Мои заказы');
?>

<?
CModule::IncludeModule('iblock');
$arSelectProfile = Array("ID");
$arFilterProfile = Array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "CREATED_BY" => $USER->GetID());
$resProfile      = CIBlockElement::GetList(Array(), $arFilterProfile, false, Array(), $arSelectProfile);
while ($obProfile = $resProfile->GetNextElement()) {
	$arFieldsProfile = $obProfile->GetFields();
	$PROFILE         = $arFieldsProfile['ID'];
}
?>
<? if ( ! empty($PROFILE)): ?>
	<?
	$APPLICATION->IncludeComponent(
		"mlab:appforsale.task",
		".default",
		array(
			"CACHE_TIME"                        => "36000000",
			"CACHE_TYPE"                        => "A",
			"ELEMENT_SORT_FIELD"                => "timestamp_x",
			"ELEMENT_SORT_FIELD2"               => "id",
			"ELEMENT_SORT_ORDER"                => "desc",
			"ELEMENT_SORT_ORDER2"               => "desc",
			"IBLOCK_ID"                         => "4",
			"IBLOCK_TYPE"                       => "tasks",
			"OFFER_IBLOCK_ID"                   => "3",
			"OFFER_IBLOCK_TYPE"                 => "offers",
			"SEF_FOLDER"                        => "/youdo/tasks/",
			"SEF_MODE"                          => "Y",
			"OFFER_FORM_PROPERTY_CODE"          => array(
				0 => "3",
				1 => "",
			),
			"OFFER_FORM_PROPERTY_CODE_REQUIRED" => array(
				0 => "3",
				1 => "",
			),
			"OFFER_PROPERTY_CODE"               => array(
				0 => "3",
				1 => "",
			),
			"COMPONENT_TEMPLATE"                => ".default",
			"OWNER"                             => "N",
			"LIST_FIELD_CODE"                   => array(
				0 => "",
				1 => "",
			),
			"LIST_PROPERTY_CODE"                => array(
				0 => "",
				1 => "",
			),
			"DETAIL_FIELD_CODE"                 => array(
				0 => "",
				1 => "",
			),
			"DETAIL_PROPERTY_CODE"              => array(
				0 => "",
				1 => "",
			),
			"OFFER_FIELD_CODE"                  => array(
				0 => "",
				1 => "",
			),
			"OFFER_MESS_BTN_ADD"                => "Отправить предложение",
			"DETAIL_MESS_CUSTOMER"              => "Заказчик этого задания",
			"DETAIL_MESS_CLAIM"                 => "Пожаловаться на задание",
			"DETAIL_MESS_YOUR_OFFER"            => "Ваше предложение",
			"OFFER_FORM_FIELD_CODE"             => array(
				0 => "",
				1 => "",
			),
			"OFFER_FORM_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
			"OFFER_FORM_MESS_BTN_ADD"           => "Взять заказ",
			"USE_FILTER"                        => "Y",
			"USE_SORT"                          => "N",
			"USE_MAP"                           => "N",
			"CNT"                               => "N",
			"FILTER_SORT_FIELD"                 => "timestamp_x",
			"FILTER_SORT_ORDER"                 => "desc",
			"FILTER_SORT_FIELD2"                => "id",
			"FILTER_SORT_ORDER2"                => "desc",
			"SEF_URL_TEMPLATES"                 => array(
				"list"   => "",
				"detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
				"offer"  => "#SECTION_CODE_PATH#/#ELEMENT_ID#/offer/",
			)
		),
		false
	);
	?>
<? else: ?>
	<? echo '<script>app.loadPageStart({url: "/youdo/personal/register/", title: "Регистрация профиля"})</script>'; ?>
<? endif; ?>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>