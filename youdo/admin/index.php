<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Администрирование");

global $USER;
if ( ! $USER->IsAuthorized()) {
	ShowError("Необходима авторизация");

	return;
};


?>

	<ul class="mt-5">
		<li><a href="deactivate.php">Деактивация тарифов</a></li>
	</ul>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>