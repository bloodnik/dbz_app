<?

use Bitrix\Main\Loader;

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>
<?

$mobileApp = \Mlab\Appforsale\MobileApp::getInstance();
$platform  = $mobileApp->getPlatform();

PR($platform);

if (($platform == 'android' || isUserDeactivated()) && $APPLICATION->GetCurPage() != '/youdo/blockApp.php') {
	LocalRedirect("/youdo/blockApp.php");
}


require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>