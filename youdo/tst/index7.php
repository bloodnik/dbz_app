<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>

<?

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;

global $USER, $DB;

Loader::includeModule('mlab.appforsale');
Loader::includeModule('iblock');


// Считаем 30 дней активности
$arFilter    = array(
	"IBLOCK_ID"           => 4,
	"ACTIVE"              => "Y",
	"PROPERTY_PROFILE_ID" => $_GET["master"]
);
$res         = CIBlockElement::GetList(array("CREATED_DATE" => "DESC"), $arFilter, array("CREATED_DATE"), array(), array());
$startDate   = null;
$endDate     = null;
$daysCounter = 1;
$endDate     = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
while ($arTask = $res->GetNext()) {
	$daysCounter++;
	if ($daysCounter > 30) {
		continue;
	}
	$startDate = $DB->FormatDate($arTask["CREATED_DATE"], "YYYY.MM.DD", "DD.MM.YYYY");


}

PR("Начало периода - " . $startDate);
PR("Конец периода периода - " . $endDate);


// Общее количество заявок за период
$arFilter                     = array(
	"IBLOCK_ID"           => 4,
	"ACTIVE"              => "Y",
	"PROPERTY_PROFILE_ID" => $_GET["master"]
);
$arFilter["DATE_MODIFY_FROM"] = $startDate;
$arFilter["DATE_MODIFY_TO"]   = $endDate;
$res                          = CIBlockElement::GetList(array("CREATED_DATE" => "DESC"), $arFilter, false, array(), array("CNT"));
$totalTasks                   = $res->SelectedRowsCount();

PR("Всего заявок за период - " . $totalTasks);

// Количество возвратов
$entity_data_class = getHighloadEntityByName("DBZBonusReturns");
$rsData            = $entity_data_class::getList(array(
	"select"      => array("*"),
	"filter"      => [
		"UF_USER"  => $_GET["master"],
		"UF_TYPE"  => [CAppforsaleBonusReturns::getTypeId("full"), CAppforsaleBonusReturns::getTypeId("partial")],
		">UF_DATE" => $startDate
	],
	'count_total' => true,
));

$returnsCount = $rsData->getCount();

PR("Всего возвратов за период - " . $returnsCount);

PR("Процент возвратов за период - " . round($returnsCount * 100 / $totalTasks) . "%");


?>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>