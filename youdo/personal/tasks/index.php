<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Заказы');
?>
<?
$APPLICATION->IncludeComponent(
	"appforsale:personal.tasks", 
	"", 
	array(
		"IBLOCK_ID" => "4",
		"PROFILE_IBLOCK_ID" => "2",
		"OFFER_IBLOCK_ID" => "3"
	),
	false
);
?>
<? 
if ($_SESSION['mode'] != 1)
{
	$_SESSION['mode'] = 1;
	echo '<script>app.onCustomEvent("OnAfterUserUpdate")</script>';	
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>