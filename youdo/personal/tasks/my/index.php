<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Мои заявки');
?><?$APPLICATION->IncludeComponent(
	"mlab:appforsale.task.executor", 
	".default", 
	array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"ELEMENT_SORT_FIELD" => "timestamp_x",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "tasks",
		"OFFER_IBLOCK_ID" => "3",
		"OFFER_IBLOCK_TYPE" => "offers",
		"SEF_FOLDER" => "/youdo/tasks-executor/",
		"SEF_MODE" => "Y",
		"COMMENT_IBLOCK_TYPE" => "comments",
		"COMMENT_IBLOCK_ID" => "5",
		"COMMENT_PROPERTY_CODE" => array(
			0 => "22",
			1 => "23",
			2 => "",
		),
		"COMMENT_PROPERTY_CODE_REQUIRED" => array(
			0 => "22",
			1 => "",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"USE_SWITCH" => "N",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "43",
			1 => "",
		),
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "24",
			1 => "25",
			2 => "27",
			3 => "28",
			4 => "29",
			5 => "61",
			6 => "71",
			7 => "85",
			8 => "",
		),
		"OFFER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_MESS_CUSTOMER" => "Заказчик этого задания",
		"COMMENT_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
		"COMMENT_MESS_BTN_ADD" => "Сохранить",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
			"comment" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/comment/",
		)
	),
	false
);?><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>