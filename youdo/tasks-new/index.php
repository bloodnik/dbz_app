<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('Создать задание');

LocalRedirect("/youdo");
?>


<? $APPLICATION->IncludeComponent(
	"mlab:appforsale.task.new",
	".default",
	array(
		"CACHE_TIME"                  => "36000000",
		"CACHE_TYPE"                  => "A",
		"IBLOCK_ID"                   => "4",
		"IBLOCK_TYPE"                 => "tasks",
		"SECTIONS_FIELD_CODE"         => array(
			0 => "",
			1 => "",
		),
		"SEF_FOLDER"                  => "/youdo/tasks-new/",
		"SEF_MODE"                    => "Y",
		"COMPONENT_TEMPLATE"          => ".default",
		"FORM_PROPERTY_CODE"          => array(
			0 => "",
			1 => "",
		),
		"FORM_PROPERTY_CODE_REQUIRED" => array(
			0 => "",
			1 => "",
		),
		"FORM_MESS_PROPERTY_VALUE_NA" => "(не установлено)",
		"FORM_MESS_BTN_ADD"           => "Отправить партнерам",
		"SEF_URL_TEMPLATES"           => array(
			"sections" => "#SECTION_CODE_PATH#/",
			"form"     => "#SECTION_CODE_PATH#/form/",
		)
	),
	false
); ?><?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>