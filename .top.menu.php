<?

use Mlab\Appforsale\MobileApp;

$app = MobileApp::getInstance();

try {
	$platform = $app->getPlatform();
} catch (Exception $exception) {
	$platform = 'ios';
}

$newAppUrl = "https://yandex.ru/";
if ($platform == 'android') {
	$newAppUrl = "https://play.google.com/store/apps/details?id=ru.yandex.mail&hl=ru&gl=US";
}


$aMenuLinks = array(
	array(
		"Создать задание",
		"/youdo/tasks-new/",
		array(),
		array("img" => "/youdo/images/menu_task_add.png"),
		"CSite::InGroup([5])"
	),
	array(
		"Исполнители",
		"/youdo/executors/",
		array(),
		array("img" => "/youdo/images/menu_users.png"),
		"CSite::InGroup([5])"
	),
	array(
		"Новые заявки",
		"/youdo/notification/",
		array(),
		array("img" => "/youdo/images/icons/menu_notifications.svg"),
		""
	),
	array(
		"История заявок",
		"/youdo/tasks-customer/",
		array("/youdo/tasks-executor/"),
		array("img" => "/youdo/images/icons/menu_history.svg"),
		"CSite::InGroup([5])"
	),
	array(
		"История заявок",
		"/youdo/tasks-executor/",
		array("youdo/tasks-customer/"),
		array("img" => "/youdo/images/icons/menu_history.svg"),
		"!CSite::InGroup([5])"
	),
	array(
		"Баланс",
		"/youdo/personal/?ITEM=account",
		array(),
		array("img" => "/youdo/images/icons/menu_balance.svg", "title" => "Баланс"),
		""
	),
	array(
		"Выбрать направления",
		"/youdo/personal/subscription/#direction",
		array(),
		array("img" => "/youdo/images/icons/menu_subscribe.svg", "title" => "Выбрать направления"),
		""
	),
	array(
		"Бонусы/Тарифы",
		"/youdo/personal/subscription/",
		array(),
		array("img" => "/youdo/images/icons/menu_subscribe.svg", "title" => "Бонусы/Тарифы"),
		""
	),
	array(
		"Настройки",
		"/youdo/personal/",
		array(),
		array("class" => "hidden-md hidden-lg", "img" => "/youdo/images/icons/menu_settings.svg", "title" => "Профиль"),
		""
	),
	array(
		"Справка для мастеров",
		"/youdo/app_intro/",
		array(),
		array("class" => "hidden-md hidden-lg", "img" => "/youdo/images/icons/menu_info.svg", "title" => "Справка для мастеров"),
		""
	),
	array(
		"Админ",
		"/youdo/admin/",
		array(),
		array("img" => "/youdo/images/icons/menu_subscribe.svg", "title" => "Админ"),
		"CSite::InGroup([1])"
	),
	array(
		"Тест",
		"/youdo/tst/",
		array(),
		array("class" => "hidden-md hidden-lg", "img" => "/youdo/images/menu_setting.png", "title" => "Тест"),
		"CSite::InGroup([1])"
	),
//	array(
//		"Перейти в новое приложение",
//		$newAppUrl,
//		array(),
//		array("class" => "hidden-md hidden-lg", "img" => "/youdo/images/menu_setting.png", "title" => "Тест"),
//		"CSite::InGroup([1])"
//	),
//	Array(
//		"Бонусы",
//		"/youdo/bonus/",
//		Array(),
//		Array("class"=>"hidden-md hidden-lg", "img"=>"/youdo/images/menu_setting.png", "title"=>"Тест"),
//		"CSite::InGroup([1])"
//	)
);
?>